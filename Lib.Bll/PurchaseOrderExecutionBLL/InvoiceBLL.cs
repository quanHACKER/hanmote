﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.PurchaseOrderExecution;
using Lib.IDAL.PurchaseOrderExecution;
using Lib.SqlServerDAL.PurchaseOrderExecution;

namespace Lib.Bll.PurchaseOrderExecutionBLL
{
    public class InvoiceBLL
    {
        InvoiceIDAL invoiceDAL = DALFactory.DALFactoryHelper
            .CreateNewInstance<InvoiceIDAL>("PurchaseOrderExecution.InvoiceDAL");
        /// <summary>
        /// 增加发票信息
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addInvoice(Invoice invoice, List<Invoice_Item> itemList) {
            return invoiceDAL.addInvoice(invoice, itemList);
        }

        /// <summary>
        /// 删除发票信息
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public int deleteInvoice(string invoiceID) {
            return invoiceDAL.deleteInvoice(invoiceID);
        }

        /// <summary>
        /// 更新发票信息
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateInvoice(Invoice invoice, List<Invoice_Item> itemList) {
            return invoiceDAL.updateInvoice(invoice, itemList);
        }

        /// <summary>
        /// 查询发票信息
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public Invoice getInvoice(string invoiceID) {
            return invoiceDAL.getInvoice(invoiceID);
        }

        /// <summary>
        /// 查询满足条件的发票
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Invoice> getInvoiceListByConditions(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime)
        {
            return invoiceDAL.getInvoiceListByConditions(conditions, beginTime, endTime);
        }

        /// <summary>
        /// 查询发票项
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public List<Invoice_Item> getInvoiceItemByInvoiceID(string invoiceID) {
            return invoiceDAL.getInvoiceItemByInvoiceID(invoiceID);
        }
    }
}
