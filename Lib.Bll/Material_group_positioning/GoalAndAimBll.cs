﻿using DALFactory;
using Lib.IDAL.Material_group_positioning;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.Material_group_positioning
{
    public class GoalAndAimBll
    {

        
        private static readonly GoalAndAimIDAL idal = DALFactoryHelper.CreateNewInstance<GoalAndAimIDAL>("Material_group_positioning.GoalAndAimDAL");


        //维护性物料组

        //资产性物料组


        #region 生产性物料组

        public List<String> getPrdId()
        {
            List<String> list = new List<string>();
            DataTable dt =idal.GetPrdId();

            for (int i = 0; i < dt.Rows.Count; i++) {
                list.Add(dt.Rows[i][0].ToString()); 
            }
            return list;
        }
      
         
        /**
         * 通过产品id查询产品名称
         **/
        public String getPrdName(string text)
        {
           
            DataTable dt = idal.GetPrdName(text);
            return dt.Rows[0][0].ToString();
        }
        //获取组成产品的物料信息
        public DataTable getMaterialInfo(string text)
        {
            DataTable dt = idal.getMaterialInfo(text);
            return dt;
        }

        public int insertGoalInfo(prdGoalModel prdGoalModel)
        {
          return  idal.insertGoalInfo(prdGoalModel);
            
        }
        #endregion

    }
}
