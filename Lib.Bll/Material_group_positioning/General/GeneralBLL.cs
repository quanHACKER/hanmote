﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.Material_group_positioning;
using DALFactory;
using System.Data;
//using Lib.Model.Material_group_positioning.GN;
namespace Lib.Bll.Material_group_positioning.General
{
    public class GeneralBLL
    {
        private static readonly GeneralIDAL idal = DALFactoryHelper.CreateNewInstance<GeneralIDAL>("Material_group_positioning.GeneralDAL");
        public DataTable loaddata(string id)
        {
            return idal.loaddata(id);
        }
        public DataTable loaditemdata(string id)
        {
            return idal.loaditemdata(id);
        }
        public DataTable loadgeneralitemdata(string id)
        {
            return idal.loadgeneralitemdata(id);
        }
        public DataTable searchdata(string id, string ORGID, string ORGName, string MGTID,string MGTName)
        {
            return idal.searchdata(id, ORGID, ORGName, MGTID,MGTName);
        }
        public DataTable searchitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName)
        {
            return idal.searchitemdata(id, ORGID, ORGName, MGTID, MGTName);
        }
        public DataTable searchgeneralitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName)
        {
            return idal.searchgeneralitemdata(id, ORGID, ORGName, MGTID, MGTName);
        }
        public List<String> getALLMtGroup()
        {
            return idal.getALLMtGroup();
        }
        public List<String> getALLorg()//获取所有采购组织
        {
            return idal.getALLorg();
        }
        public List<String> getModel()//获取所有采购组织
        {
            return idal.getModel();
        }
        public List<String> getALLorgID()//获取所有采购组织的ID
        {
            return idal.getALLorgID();
        }
        
        public List<String> getALLitemorgID()//获取所有item采购组织的ID
        {
            return idal.getALLitemorgID();
        }
        public List<String> getALLgeneralitemorgID()//获取所有item采购组织的ID
        {
            return idal.getALLgeneralitemorgID();
        }

        public void SaveGoal(string orgID, string orgName, string  name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.SaveGoal(orgID,orgName,name_MtGroup,code_MtGroup,G1,Z1);
        }
        public void SaveitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.SaveitemGoal(orgID, orgName, name_MtGroup, code_MtGroup, G1, Z1);
        }
        public void SavegeneralitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.SavegeneralitemGoal(orgID, orgName, name_MtGroup, code_MtGroup, G1, Z1);
        }


        public void SaveModel(string name, string area, string aim, string goal)
        {
            idal.SaveModel(name,area,aim,goal);
        }
        public void UpdateMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.UpdateMtGroup(name_MtGroup, code_MtGroup, G1, Z1);
        }
        public void UpdateitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.UpdateitemMtGroup(name_MtGroup, code_MtGroup, G1, Z1);
        }
        public void UpdategeneralitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)
        {
            idal.UpdategeneralitemMtGroup(name_MtGroup, code_MtGroup, G1, Z1);
        }


        public void deletedata(string b, string G1, string Z1)
        {
            idal.deletedata( b,G1, Z1);
        }

        public void deleteitemdata(string G1, string Z1)
        {
            idal.deleteitemdata(G1, Z1);
        }
        public void deletegeneralitemdata(string G1, string Z1)
        {
            idal.deletegeneralitemdata(G1, Z1);
        }
    }

}
