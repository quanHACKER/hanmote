﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using DALFactory;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SPR_SPCompareRadarCharBLL
    {
        private static readonly SPR_SPCompareRadarChartIDAL sPR_SPCompareRadarChartIDAL = DALFactoryHelper.CreateNewInstance<SPR_SPCompareRadarChartIDAL>("SPR_SPCompareRadarCharDAL");

        /// <summary>
        /// 查询供应商列表
        /// 条件
        /// 采购组织ID、年度、时段
        /// </summary>
        /// <param name="selectSupplierConditionSettings"></param>
        /// <returns></returns>
        public DataTable getAllSupplierInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            return sPR_SPCompareRadarChartIDAL.getAllSupplierInfo(selectSupplierConditionSettings);
        }
        
        /// <summary>
        /// 查询供应商评分结果
        /// </summary>
        /// <param name="conditionvalue"></param>
        /// <returns></returns>
        public DataTable getSupplierEvaluationScore(SPCompareRadarChartConditionValue conditionvalue)
        {
            return sPR_SPCompareRadarChartIDAL.getSupplierEvaluationScore(conditionvalue);
        }
    }
}
