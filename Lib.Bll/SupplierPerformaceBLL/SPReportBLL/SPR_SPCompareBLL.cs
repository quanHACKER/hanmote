﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using DALFactory;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SPR_SPCompareBLL
    {
        private static readonly SPR_SPCompareIDAL sPR_SPCompareIDAL = DALFactoryHelper.CreateNewInstance<SPR_SPCompareIDAL>("SPR_SPCompareDAL");

        /// <summary>
        /// 获取所有采购组织信息
        /// </summary>
        /// <returns></returns>
        public DataTable getAllBuyerOrgInfo()
        {
            return sPR_SPCompareIDAL.getAllBuyerOrgInfo();
        }

        /// <summary>
        /// 根据采购组织编码查找该采购组织下的供应商名称
        /// </summary>
        /// <param name="purchaseOrgId">采购组织编码</param>
        /// <returns>一个DataTable类型的数据</returns>
        public DataTable getAllSupplierInfoByPurchaseOrgId(string purchaseOrgId)
        {
            return sPR_SPCompareIDAL.getAllSupplierInfoByPurchaseOrgId(purchaseOrgId);
        }

        /// <summary>
        /// 查询物料信息
        /// </summary>
        /// <param name="selectMaterialConditionSettings">查询条件</param>
        /// <returns></returns>
        public DataTable getAllMaterialInfoBySelectMaterialConditon(SelectMaterialConditionSettings selectMaterialConditionSettings)
        {
            return sPR_SPCompareIDAL.getAllMaterialInfoBySelectMaterialConditon(selectMaterialConditionSettings);
        }


        

        /// <summary>
        /// 获得物料评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        public DataTable getMaterialEvaluatedScore(SPCompareConditionValue sPCompareConditionValue)
        {
            return sPR_SPCompareIDAL.getMaterialEvaluatedScore(sPCompareConditionValue);
        }

        /// <summary>
        /// 获得供应商评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        public DataTable getSupplierEvaluatedScore(SPCompareConditionValue sPCompareConditionValue)
        {
            return sPR_SPCompareIDAL.getSupplierEvaluatedScore(sPCompareConditionValue);
        }

        /// <summary>
        /// 得到供应商和物料评分比较结果
        /// </summary>
        /// <returns></returns>
        public DataTable getSupplierMaterialScoreCompareResult(SPCompareConditionValue sPCompareConditionValue)
        {
            //得到供应商评分结果
            DataTable dtSupplierScore = this.getSupplierEvaluatedScore(sPCompareConditionValue);
            //得到物料评分结果
            DataTable dtMaterialScore = this.getMaterialEvaluatedScore(sPCompareConditionValue);

            //创建一个DataTable
            DataTable dtResult = new DataTable("ScoreResult");
            //给DataTable添加列
            DataColumn project = dtResult.Columns.Add("Project", typeof(string));
            DataColumn supplierScore = dtResult.Columns.Add("SupplierScore", typeof(string));
            DataColumn materialScore = dtResult.Columns.Add("MaterialScore", typeof(string));
            //给DataTable添加行
            DataRow dtSupplierScoreCurrentRow = dtSupplierScore.Rows[0];  //供应商评分 ，查询出来的数据只有一行
            DataRow dtMaterialScoreCurrentRow = dtMaterialScore.Rows[0]; //物料评分 ，查询出来的数据只有一行
            dtResult.Rows.Add(new object[] { "总体评估", Convert.ToDouble(dtSupplierScoreCurrentRow["Total_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["Total_Score"] });
            dtResult.Rows.Add(new object[] { "", "", "" });
            dtResult.Rows.Add(new object[] { "01 价格", Convert.ToDouble(dtSupplierScoreCurrentRow["Price_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["Price_Score"] });
            dtResult.Rows.Add(new object[] { "01 价格水平", Convert.ToDouble(dtSupplierScoreCurrentRow["PriceLevel_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["PriceLevel_Score"] });
            dtResult.Rows.Add(new object[] { "02 价格历史", Convert.ToDouble(dtSupplierScoreCurrentRow["PriceHistory_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["PriceHistory_Score"] });
            dtResult.Rows.Add(new object[] { "", "", "" });
            dtResult.Rows.Add(new object[] { "02 质量", Convert.ToDouble(dtSupplierScoreCurrentRow["Quality_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["Quality_Score"] });
            dtResult.Rows.Add(new object[] { "01 收货", Convert.ToDouble(dtSupplierScoreCurrentRow["GoodReceipt_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["GoodReceipt_Score"] });
            dtResult.Rows.Add(new object[] { "02 质量审计", Convert.ToDouble(dtSupplierScoreCurrentRow["QualityAudit_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["QualityAudit_Score"] });
            dtResult.Rows.Add(new object[] { "", "", "" });
            dtResult.Rows.Add(new object[] { "03 抱怨/拒绝水平", Convert.ToDouble(dtSupplierScoreCurrentRow["ComplaintAndReject_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["ComplaintAndReject_Score"] });
            dtResult.Rows.Add(new object[] { "01 交货", Convert.ToDouble(dtSupplierScoreCurrentRow["Delivery_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["Delivery_Score"] });
            dtResult.Rows.Add(new object[] { "01 按时交货的表现", Convert.ToDouble(dtSupplierScoreCurrentRow["OnTimeDelivery_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["OnTimeDelivery_Score"] });
            dtResult.Rows.Add(new object[] { "02 确认日期", Convert.ToDouble(dtSupplierScoreCurrentRow["ConfirmDate_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["ConfirmDate_Score"] });
            dtResult.Rows.Add(new object[] { "03 数量可靠性", Convert.ToDouble(dtSupplierScoreCurrentRow["QuantityReliability_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["QuantityReliability_Score"] });
            dtResult.Rows.Add(new object[] { "04 对装运须知的遵守", Convert.ToDouble(dtSupplierScoreCurrentRow["Shipment_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["Shipment_Score"] });
            dtResult.Rows.Add(new object[] { "", "", "" });
            dtResult.Rows.Add(new object[] { "04 一般服务/支持", Convert.ToDouble(dtSupplierScoreCurrentRow["GeneralServiceAndSupport_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["GeneralServiceAndSupport_Score"] });
            dtResult.Rows.Add(new object[] { "", "", "" });
            dtResult.Rows.Add(new object[] { "05 外部服务", Convert.ToDouble(dtSupplierScoreCurrentRow["ExternalService_Score"]).ToString("0.00"), dtMaterialScoreCurrentRow["ExternalService_Score"] });

            return dtResult;
        }


        /// <summary>
        /// 取得评估人和评估时间
        /// </summary>
        /// <param name="sPCompareConditionValue"></param>
        /// <returns></returns>
        public string[] getEvaluationCreatorAndTime(SPCompareConditionValue sPCompareConditionValue)
        {
            //得到物料评分结果
            DataTable dtMaterialScore = this.getMaterialEvaluatedScore(sPCompareConditionValue);
            //创建一个长度为2的字符串数组
            string[] evaluationCreatorAndTime = new string[2];
            evaluationCreatorAndTime[0] = dtMaterialScore.Rows[0]["Creator_Name"].ToString();
            evaluationCreatorAndTime[1] = Convert.ToDateTime(dtMaterialScore.Rows[0]["Evaluation_Time"]).ToString("yyyy-MM-dd");
            return evaluationCreatorAndTime;
        }

    }
}
