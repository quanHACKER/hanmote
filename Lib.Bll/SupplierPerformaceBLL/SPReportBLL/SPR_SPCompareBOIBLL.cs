﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.IDAL.SupplierPerformaceIDAL;
using DALFactory;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SPR_SPCompareBOIBLL
    {
        private static readonly SPR_SPCompareBOIIDAL sPR_SPCompareBOIIDAL = DALFactoryHelper.CreateNewInstance<SPR_SPCompareBOIIDAL>("SPR_SPCompareBOIDAL");

        /// <summary>
        /// 获取所有采购组织信息
        /// </summary>
        /// <returns></returns>
        public DataTable getAllBuyerOrgInfo()
        {
            return sPR_SPCompareBOIIDAL.getAllBuyerOrgInfo();
        }

        /// <summary>
        /// 取得物料组信息
        /// </summary>
        /// <param name="selectSupplierConditionSettings">查询条件</param>
        /// <returns></returns>
        public DataTable getSupplierIndustryInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            return sPR_SPCompareBOIIDAL.getSupplierIndustryInfo(selectSupplierConditionSettings);
        }

        /// <summary>
        /// 基于行业的供应商评估比较
        /// </summary>
        /// <param name="spcCondition">查询条件</param>
        /// <returns></returns>
        public DataTable getEvaluationSupplierIScoreBaseIndustry(SPCompareBaseOnBOIConditionValue condtionValue)
        {
            return sPR_SPCompareBOIIDAL.getEvaluationSupplierIScoreBaseIndustry(condtionValue);
        }
    }
}
