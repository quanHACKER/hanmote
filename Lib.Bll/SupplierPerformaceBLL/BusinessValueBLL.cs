﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using System.Windows.Forms;
using Lib.IDAL.SupplierPerformaceIDAL;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class BusinessValueBLL
    {
        private static readonly BusinessValueIDAL businessValueBLL = DALFactoryHelper.CreateNewInstance<BusinessValueIDAL>("SupplierPerformaceDAL.BusinessValueDAL");

        #region 业务价值

        /// <summary>
        /// 查询 采购方的采购业务额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal queryPurchasingTurnover(String supplierID, String year)
        {
            DataTable turnoverList = businessValueBLL.queryPurchasingTurnover(supplierID,year);
            if (turnoverList.Rows.Count > 0)
            {
                Decimal sum = 0M;
                foreach (DataRow row in turnoverList.Rows)
                {
                    try
                    {
                        sum = sum + Convert.ToDecimal(row[0]);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return sum;
            }
            else
            {
                //返回-1表示当前暂时无数据
                return -1;
            }

        }

        /// <summary>
        /// 查询供应商全年营业额
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal querySupplierTurnover(String supplierID, String year)
        {
            DataTable turnoverList = businessValueBLL.querySupplierTurnover(supplierID, year);
            if (turnoverList.Rows.Count > 0)
            {
                Decimal turnover = 0M;
                foreach (DataRow row in turnoverList.Rows)
                {
                    try
                    {
                        turnover = turnover + Convert.ToDecimal(row[0]);
                    }
                    catch
                    {
                        return -1;
                    }
                    
                }
                return turnover;
            }
            else
            {
                //返回负数表示没有数据
                return -1M;
            }
        }

        /// <summary>
        /// 定位公司业务价值，返回H\M\L\N
        /// </summary>
        /// <param name="portion"></param>
        /// <returns></returns>
        public String positionCompanyPortion(Decimal portion)
        {
            Double portionScore = Convert.ToDouble(portion);
            if (portionScore >= 0.15)
            {
                return "H";
            }
            else if ((portionScore < 0.15) && (portionScore >= 0.05))
            {
                return "M";
            }
            else if ((portionScore < 0.05) && (portionScore >= 0.008))
            {
                return "L";
            }
            else
            {
                return "N";
            }
        }

        /// <summary>
        /// 定位公司吸引力水平，返回H\M\L\N
        /// </summary>
        /// <param name="appeal"></param>
        /// <returns></returns>
        public String positionAppealLevel(Decimal appeal)
        {
            Double appealScore = Convert.ToDouble(appeal);

            if (appealScore >= 15)
            {
                return "H";
            }
            else if ((appealScore < 15) && (appealScore >= 10))
            {
                return "M";
            }
            else if ((appealScore < 10) && (appealScore >= 5))
            {
                return "L";
            }
            else
            {
                return "N";
            }
        }

        /// <summary>
        /// 供应商定位
        /// </summary>
        /// <param name="turnover"></param>
        /// <param name="appeal"></param>
        /// <returns></returns>
        public String positionSupplier(String turnoverRst, String appealRst)
        {
            String rst = "";
            if ((turnoverRst == "N") || (turnoverRst == "L"))
            {
                if ((appealRst == "H") || (appealRst == "M"))
                {
                    rst = "发展型";
                }
                else if ((appealRst == "L") || (appealRst == "N"))
                {
                    rst = "边缘型";
                }
                else
                {
                    return "";
                }
            }
            else if ((turnoverRst == "M") || (turnoverRst == "H"))
            {
                if ((appealRst == "H") || (appealRst == "M"))
                {
                    rst = "核心型";
                }
                else if ((appealRst == "L") || (appealRst == "N"))
                {
                    rst = "盘剥型";
                }
                else
                {
                    return "";
                }
            }
            return rst;

        }

        /// <summary>
        /// 保存业务价值相关字段 保存到数据表Supplier_Business_Value中
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveBusinessValue(List<Object> savePara)
        {
            int lines = businessValueBLL.saveBusinessValue(savePara);
            return lines;
        }

        /// <summary>
        /// 保存供应商营业额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <param name="turnover"></param>
        /// <returns></returns>
        public int saveSupplierTurnover(String supplierID,String year,String turnover)
        {
            int lines = businessValueBLL.saveBusinessValue(supplierID,year,turnover);
            return lines;
        }

        #endregion
    }
}
