﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using System.Windows.Forms;
using Lib.IDAL.SupplierPerformaceIDAL;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class LPSBLL
    {
        private static readonly LPSIDAL lpsIDAL = DALFactoryHelper.CreateNewInstance<LPSIDAL>("SupplierPerformaceDAL.LPSDAL");

        #region 低效能供应商警告
        /// <summary>
        /// 查询进入低效能供应商管理流程的供应商
        /// </summary>
        /// <param name="scoreThresholdList"></param>
        /// <param name="timePeriod"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryLowPerformanceSupplier(List<Decimal> scoreThresholdList, String timePeriod, String year)
        {
            DataTable LPSByScoreTable = lpsIDAL.queryLPSByScore(scoreThresholdList, timePeriod);
            DataTable LPSByClassificationTable = lpsIDAL.queryLPSByClassification(year);

            //合并两个Datatable

            //拷贝LPSByScoreTablede结构和数据
            DataTable rst = LPSByScoreTable.Copy();
            //添加LPSByClassificationTable的数据
            foreach (DataRow dr in LPSByClassificationTable.Rows)
            {
                rst.ImportRow(dr);
            }

            //过滤rst中的重复数据
            DataView myDataView = new DataView(rst);
            String[] strColumns = { "Supplier_ID", "Supplier_Name" };
            rst = myDataView.ToTable(true, strColumns);

            return rst;
        }

        /// <summary>
        /// 查询是否已经存在LPS记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public bool queryRecordOrNot(String supplierID)
        {
            bool rst = lpsIDAL.queryRecordOrNot(supplierID);
            return rst;
        }

        /// <summary>
        /// 记录LPS流程状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="LPSState"></param>
        /// <param name="recordTime"></param>
        /// <param name="recordName"></param>
        /// <returns></returns>
        public int saveLPSState(String supplierID, String LPSState, DateTime recordTime, String recordName)
        {
            int lines = lpsIDAL.saveLPSState(supplierID, LPSState, recordTime, recordName);
            return lines;
        }

        /// <summary>
        /// 查询低效能供应商管理流程中的所有供应商
        /// </summary>
        /// <returns></returns>
        public DataTable querySupplier()
        {
            DataTable dt = lpsIDAL.queryLPSSupplier();
            return dt;
        }

        /// <summary>
        /// 查询LPS流程状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public String queryLPSDegree(String supplierID)
        {
            return (lpsIDAL.queryLPSDegree(supplierID));
        }

        /// <summary>
        /// 保存关键指数及陈述
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="kpiList"></param>
        /// <param name="statement"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int saveKPIStatement(String supplierID, List<String> kpiList, String statement, DateTime time, String name)
        {
            int lines = lpsIDAL.saveKPIStatement(supplierID,kpiList,statement,time,name);
            return lines;
        }
        #endregion

        #region LPS三级流程

        /// <summary>
        /// 判断供应商是否合格
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="timePeriod"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="scoreThreshodList"></param>
        /// <returns></returns>
        public int judgeQualifiedOrNot(String supplierID, String timePeriod, DateTime startTime, DateTime endTime, List<Decimal> scoreThreshodList)
        {
            DataTable scoreRst = lpsIDAL.querySupplierPerformance(supplierID, timePeriod, startTime, endTime);

            try
            {
                //价格分数
                if (Convert.ToDecimal(scoreRst.Rows[0][0]) < scoreThreshodList[0])
                {
                    return 0;
                }
                //质量分数
                else if (Convert.ToDecimal(scoreRst.Rows[0][1]) < scoreThreshodList[1])
                {
                    return 0;
                }
                //交货分数
                else if (Convert.ToDecimal(scoreRst.Rows[0][2]) < scoreThreshodList[2])
                {
                    return 0;
                }
                //一般服务/支持分数
                else if (Convert.ToDecimal(scoreRst.Rows[0][3]) < scoreThreshodList[3])
                {
                    return 0;
                }
                //总分
                else if (Convert.ToDecimal(scoreRst.Rows[0][4]) < scoreThreshodList[4])
                {
                    return 0;
                }
            }
            catch
            {
                return 2;
            }

            return 1;
        }

        /// <summary>
        /// 退出LPS流程，即删除该供应商在Low_Performance_Supplier中的记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int deleteLPSRecord(String supplierID)
        {
            int lines = lpsIDAL.deleteLPSRecord(supplierID);
            return lines;
        }

        /// <summary>
        /// 更新LPS状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int updateLPSState(String supplierID, String state)
        {
            int lines = lpsIDAL.updateLPSState(supplierID, state);
            return lines;
        }

        /// <summary>
        /// 添加至淘汰供应商列表
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="outIdentity"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int saveLPSOutList(String supplierID, String outIdentity, DateTime time, String name)
        {
            int lines = lpsIDAL.saveLPSOutList(supplierID, outIdentity, time, name);
            return lines;
        }

        /// <summary>
        /// 检查在数据表LPS_Out_List中是否有该供应商的记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int checkLPSOutList(String supplierID)
        {
            int count = lpsIDAL.checkLPSOutList(supplierID);
            return count;
        }

        #endregion

        #region 淘汰供应商

        /// <summary>
        /// 查询待淘汰供应商列表
        /// </summary>
        /// <returns></returns>
        public DataTable queryOutSupplier()
        {
            DataTable rst;
            rst = lpsIDAL.queryOutSupplier();
            return rst;
        }

        /// <summary>
        /// 淘汰供应商（删除供应商在LPS_Out_List数据表中的记录）
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int deleteLPSupplier(String supplierID)
        {
            int lines = lpsIDAL.deleteLPSupplier(supplierID);
            return lines;
        }

        /// <summary>
        /// 在Source_List数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int updateSupplierOutInSourceList(String supplierID)
        {
            int lines = lpsIDAL.updateSupplierOutInSourceList(supplierID);
            return lines;
        }

        /// <summary>
        /// 在Supplier_Base数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int updateSupplierOutInSupplierBase(String supplierID)
        {
            int lines = lpsIDAL.updateSupplierOutInSupplierBase(supplierID);
            return lines;
        }

        /// <summary>
        /// 在Eliminate_Supplier_Record数据表中添加供应商淘汰记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int insertEliminateSupplierRecord(String supplierID, String time, String name)
        {
            int lines = lpsIDAL.insertEliminateSupplierRecord(supplierID,time,name);
            return lines;
        }

        #endregion

    }
}
