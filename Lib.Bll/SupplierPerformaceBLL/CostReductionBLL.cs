﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using System.Windows.Forms;
using Lib.IDAL.SupplierPerformaceIDAL;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class CostReductionBLL
    {
        private static readonly CostReductionIDAL costReductionIDAL = DALFactoryHelper.CreateNewInstance<CostReductionIDAL>("SupplierPerformaceDAL.CostReductionDAL");

        /// <summary>
        /// 计算采购成本降低额（管制品种）
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public Decimal calculateControledCostReduction(String supplierID, String materialID,String year,String month)
        {
            Decimal amountThisMonth = costReductionIDAL.queryPurchaseAmountThisMonth(supplierID, materialID, year, month);
            Decimal marketPriceLastYear = costReductionIDAL.queryMarketPriceLastYear(materialID, year);
            Decimal purchasePriceLastYearEnd = costReductionIDAL.queryPurchasePriceLastYearEnd(supplierID, materialID, year);
            Decimal marketPriceThisMonth = costReductionIDAL.queryMarketPriceThisMonth(materialID, year, month);
            Decimal purchasePriceThisMonth = costReductionIDAL.queryPurchasePriceThisMonth(supplierID, materialID, year, month);
            Decimal costReduction;

            try
            {
                costReduction = amountThisMonth * ((purchasePriceThisMonth = marketPriceThisMonth) - (purchasePriceLastYearEnd - marketPriceLastYear));
                return costReduction;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 计算 采购成本降低率（市场管制）
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal calculateControledCostReductionRate(String supplierID, String materialID, String year, String month)
        {
            Decimal marketPriceLastYear = costReductionIDAL.queryMarketPriceLastYear(materialID, year);
            Decimal purchasePriceLastYearEnd = costReductionIDAL.queryPurchasePriceLastYearEnd(supplierID, materialID, year);
            Decimal marketPriceThisMonth = costReductionIDAL.queryMarketPriceThisMonth(materialID, year, month);
            Decimal purchasePriceThisMonth = costReductionIDAL.queryPurchasePriceThisMonth(supplierID, materialID, year, month);
            Decimal costReductionRate;

            try
            {
                costReductionRate = ((purchasePriceThisMonth - marketPriceThisMonth) - (purchasePriceLastYearEnd - marketPriceLastYear))/purchasePriceLastYearEnd;
                return costReductionRate;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 计算采购成本降低额（非市场管制）
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal calculateUncontroledCostReduction(String supplierID, String materialID, String year, String month)
        {
            Decimal marketPriceLastYear = costReductionIDAL.queryMarketPriceLastYear(materialID, year);
            Decimal purchasePriceLastYearEnd = costReductionIDAL.queryPurchasePriceLastYearEnd(supplierID, materialID, year);
            Decimal purchasePriceThisMonth = costReductionIDAL.queryPurchasePriceThisMonth(supplierID, materialID, year, month);
            Decimal costReduction;

            try
            {
                costReduction = (purchasePriceThisMonth - marketPriceLastYear) * purchasePriceLastYearEnd;
                return costReduction;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 计算采购成本降低率（市场管制品种）
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal calculateUncontroledCostReductionRate(String supplierID, String materialID, String year, String month)
        {
            Decimal marketPriceLastYear = costReductionIDAL.queryMarketPriceLastYear(materialID, year);
            Decimal purchasePriceThisMonth = costReductionIDAL.queryPurchasePriceThisMonth(supplierID, materialID, year, month);
            Decimal costReductionRate;

            try
            {
                costReductionRate = (purchasePriceThisMonth-marketPriceLastYear)/marketPriceLastYear;
                return costReductionRate;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 判断是否为市场管制品种
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public bool queryControledOrNot(String materialID)
        {
            bool rst = costReductionIDAL.queryControledOrNot(materialID);
            return rst;
        }

        /// <summary>
        /// 保存降成本计算的记录
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveCostReduction(List<Object> savePara)
        {
            int lines = costReductionIDAL.saveCostReduction(savePara);
            return lines;
        }

        /// <summary>
        /// 查询符合时间条件的所有降成本记录
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public DataTable queryCostReductionResult(String year, String month)
        {
            return (costReductionIDAL.queryCostReductionResult(year, month));
        }
    }
}
