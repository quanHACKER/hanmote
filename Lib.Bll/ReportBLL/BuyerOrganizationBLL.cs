﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using DALFactory;

namespace Lib.Bll
{
    public class BuyerOrganizationBLL
    {
        public static readonly BuyerOrganizationIDAL buyerOrgIDAL = DALFactoryHelper.CreateNewInstance<BuyerOrganizationIDAL>("BuyerOrganizationDAL");

        public DataTable GetBuyerOrganizationName()
        {
            return buyerOrgIDAL.GetAllBuyerOrganizationName();
        }


    }
}
