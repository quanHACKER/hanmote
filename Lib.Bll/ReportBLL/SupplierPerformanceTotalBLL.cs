﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using DALFactory;

namespace Lib.Bll
{
    public class SupplierPerformanceTotalBLL
    {
        /// <summary>
        /// 取得该对象的实例
        /// </summary>
        public static readonly SupplierPerformanceTotalIDAL sPerIDAL = DALFactoryHelper.CreateNewInstance<SupplierPerformanceTotalIDAL>("SupplierPerformanceTotalDAL");

        public DataTable GetSupplierPerformanceTotal(string purchaseOrg, string materialGroup)
        {
            return sPerIDAL.GetSupplierPerformanceTotal(purchaseOrg, materialGroup);
        }
    }
}
