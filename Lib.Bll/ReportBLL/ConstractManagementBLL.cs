﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using Lib.SqlIDAL;

namespace Lib.Bll
{
     public class ConstractManagementBLL
    {
         public static readonly ConstractManagementIDAL constractManagementIDAL = DALFactoryHelper.CreateNewInstance<ConstractManagementIDAL>("ConstractManagementDAL");
         public DataTable GetConstractInfo(string purchaseOrg, string supplierNameOrID, string constractNameOrID)
        {
            return constractManagementIDAL.GetConstractInfo(purchaseOrg, supplierNameOrID, constractNameOrID);
        }
    }
}
