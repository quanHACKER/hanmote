﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using DALFactory;

namespace Lib.Bll
{
    public class MaterialPriceBLL
    {
        public static readonly MaterialPriceIDAL materialPriceIDAL = DALFactoryHelper.CreateNewInstance<MaterialPriceIDAL>("MaterialPriceDAL");
        public DataTable GetMaterialDetailPrice(string purchaseOrg, string supplierNameOrID)
        {
            return materialPriceIDAL.GetMaterialDetailPrice(purchaseOrg, supplierNameOrID);        
        }
    }
}
