﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.CatalogPurchase;
using Lib.IDAL.CatalogManageIDAL;
using DALFactory;

namespace Lib.Bll.CatalogManageBLL
{
    public class CatalogMaterialBLL
    {
        CatalogMaterialIDAL catalogMaterialDAL = DALFactoryHelper
            .CreateNewInstance<CatalogMaterialIDAL>(
            "CatalogManageDAL.CatalogMaterialDAL");

        /// <summary>
        /// 添加新物料目录
        /// </summary>
        /// <param name="catalogMaterial"></param>
        /// <returns></returns>
        public int addNewCatalogMaterial(CatalogMaterial catalogMaterial) {
            return catalogMaterialDAL.addNewCatalogMaterial(catalogMaterial);
        }

        /// <summary>
        /// 通过物料编号、供应商编号查询指定商品信息
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public CatalogMaterial getCatalogMaterial(string materialID, string supplierID) {
            return catalogMaterialDAL.getCatalogMaterial(materialID, supplierID);
        }

        /// <summary>
        /// 通过目录采购的物料编号查询商品信息
        /// </summary>
        /// <param name="materialCatalogID"></param>
        /// <returns></returns>
        public CatalogMaterial getCatalogMaterial(string materialCatalogID)
        {
            return catalogMaterialDAL.getCatalogMaterial(materialCatalogID);
        }

        /// <summary>
        /// 获得所有的符合条件的商品信息
        /// </summary>
        /// <param name="materialType"></param>
        /// <param name="supplierID"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public DataTable getCatalogMaterialDataTable(string materialType, string supplierID, string[] keywords){

            return catalogMaterialDAL.getCatalogMaterialDataTable(materialType, supplierID, keywords);
        }

        /// <summary>
        /// 获取自助采购物料表中所有的物料类型
        /// </summary>
        /// <returns>物料类型集合</returns>
        public List<String> getAllMaterialTypes()
        {
            return catalogMaterialDAL.getAllMaterialTypes();
        }

        public List<string> getAllSuppliers()
        {
            return catalogMaterialDAL.getAllSuppliers();
        }
    }
}
