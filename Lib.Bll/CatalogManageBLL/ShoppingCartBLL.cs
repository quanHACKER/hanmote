﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.CatalogPurchase;
using Lib.IDAL.CatalogManageIDAL;
using DALFactory;

namespace Lib.Bll.CatalogManageBLL
{
    public class ShoppingCartBLL
    {
        private ShoppingCartIDAL shoppingCartDAL = DALFactoryHelper
            .CreateNewInstance<ShoppingCartIDAL>(
            "CatalogManageDAL.ShoppingCartDAL");

        /// <summary>
        /// 根据购物车编号和物料编号获取购物车项目信息
        /// </summary>
        /// <param name="shoppingCartID"></param>
        /// <param name="materialCatalogID"></param>
        /// <returns></returns>
        public ShoppingCartItem getShoppingCartItem(string shoppingCartID, string materialCatalogID) {
            return shoppingCartDAL.getShoppingCartItem(shoppingCartID, materialCatalogID);
        }

        /// <summary>
        /// 根据购物车编号获取所有项目内容
        /// </summary>
        /// <param name="shoppingCartID"></param>
        /// <returns></returns>
        public List<ShoppingCartItem> getShoppingCartItems(string shoppingCartID) {
            return shoppingCartDAL.getShoppingCartItems(shoppingCartID);
        }

        /// <summary>
        /// 加入购物车
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int addToShoppingCart(ShoppingCartItem item) {
            return shoppingCartDAL.addToShoppingCart(item);
        }

        /// <summary>
        /// 更新购物车
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int updateShoppingCart(ShoppingCartItem item) {
            return shoppingCartDAL.updateShoppingCart(item);
        }

        /// <summary>
        /// 删除购物车内容
        /// </summary>
        /// <param name="materialCatalogID"></param>
        /// <param name="shoppingCartID"></param>
        /// <returns></returns>
        public int deleteShoppingCartItem(string materialCatalogID, string shoppingCartID) {
            return shoppingCartDAL.deleteShoppingCartItem(materialCatalogID, shoppingCartID);
        }

        /// <summary>
        /// 获取所有的预算列表
        /// </summary>
        /// <returns></returns>
        public List<string> getPurchaseBudgetList() {
            return shoppingCartDAL.getPurchaseBudgetList();
        }
    }
}
