﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.StockIDAL;
using System.Data;

namespace Lib.Bll.StockBLL
{
    public class MoveHandleBLL
    {
        public static readonly MoveHandleIDAL movehandleidal = DALFactoryHelper.CreateNewInstance<MoveHandleIDAL>("StockDAL.MoveHandleDAL");

        //维护Inventory（入库）
        public void updateInventoryStockIn(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
            string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            movehandleidal.updateInventoryStockIn(Material_ID, Factory_ID, Stock_ID, Inventory_Type, Inventory_State, Evaluate_Type, Batch_ID, Count, Material_Group, SKU);
        }

        //维护收货评分记录（采购入库）
        public void receiveProcess(string stockInNoteID, string orderID, string supplierID, string materialID, double receiveCount, double countInOrder, DateTime stockInDate, string Material_Type)
        {
            movehandleidal.receiveProcess(stockInNoteID, orderID, supplierID, materialID, receiveCount, countInOrder, stockInDate, Material_Type);
        }

        //暂时没有用到
        public void receiveCheck(string stockInNoteID, string supplierID, string materialID)
        {
            movehandleidal.receiveCheck(stockInNoteID, supplierID, materialID);
        }
        //暂时没有用到
        public void nonRawMaterialReceive(string stockInNoteID, string supplierID, string materialID)
        {
            movehandleidal.nonRawMaterialReceive(stockInNoteID, supplierID, materialID);
        }



        /// <summary>
        /// 维护到期提醒表（入库）
        /// </summary>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Material_ID"></param>
        /// <param name="batch_ID"></param>
        /// <param name="Manufacture_Date"></param>
        public void updRemind(string Factory_ID, string Stock_ID, string Material_ID, string batch_ID, DateTime Manufacture_Date)
        {
            movehandleidal.updRemind(Factory_ID, Stock_ID, Material_ID, batch_ID, Manufacture_Date);
        }


        /// <summary>
        /// 维护安全库存（入库）
        /// </summary>
        public void upSaftyInventory(string factoryid, string materialid)
        {
            movehandleidal.upSaftyInventory(factoryid, materialid);
        }


        /// <summary>
        /// 维护移动平均价（采购入库）
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="avgprice"></param>
        /// <param name="count"></param>
        public void moveAvgPrice(string materialid, float avgprice, float count)
        {
            movehandleidal.moveAvgPrice(materialid, avgprice, count);
        }

        public void moveAvgPrice(string materialid, float avgprice, float count, string factoryid)
        {
            movehandleidal.moveAvgPrice(materialid, avgprice,  count, factoryid);
        }

        /// <summary>
        /// 维护Inventory （出库）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        public void updateInventoryStockOut(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
          string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {

            movehandleidal.updateInventoryStockOut(Material_ID, Factory_ID, Stock_ID, Inventory_Type, Inventory_State, Evaluate_Type, Batch_ID, Count, Material_Group, SKU);
        }


        /// <summary>
        /// 维护Inventory （转移）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        public void updateInventoryStockTransfer(string Material_ID, string Factory_ID, string Stock_ID, string factoryid, string stockid, string Inventory_Type,
          string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            movehandleidal.updateInventoryStockTransfer(Material_ID, Factory_ID, Stock_ID, factoryid, stockid, Inventory_Type, Inventory_State, Evaluate_Type, Batch_ID, Count, Material_Group, SKU);
        }

        public void updateInventoryStockStateTransfer(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
        string Inventory_State, string newState, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            movehandleidal.updateInventoryStockStateTransfer(Material_ID,Factory_ID,Stock_ID, Inventory_Type,Inventory_State,newState, Evaluate_Type, Batch_ID,Count, Material_Group,SKU);        }

        /// <summary>
        /// 账期管理
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="factoryid"></param>
        /// <param name="stockid"></param>
        /// <param name="stockinDate"></param>
        public void InventoryInfo(string materialid, string factoryid, string stockid, string Inventory_Type, string Batch_ID, string Inventory_State, string Evaluate_Type, string Material_Group, float count, float price, DateTime stockinDate,string SKU)
        {
            movehandleidal.InventoryInfo( materialid,   factoryid,  stockid, Inventory_Type,  Batch_ID,   Inventory_State,   Evaluate_Type,  Material_Group,  count,  price, stockinDate, SKU);
        }
    }
}
