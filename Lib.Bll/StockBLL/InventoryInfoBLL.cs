﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using DALFactory;
using Lib.Model.StockModel;

namespace Lib.Bll.StockBLL
{
    public class InventoryInfoBLL
    {
        public static readonly InventoryInfoIDAL inventoryInfoIDAL = DALFactoryHelper.CreateNewInstance<InventoryInfoIDAL>("InventoryInfoDAL");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialIdOrName"></param>
        /// <param name="factoryIdOrName"></param>
        /// <param name="inventoryState"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        public List<InventoryExtendModel> findInventoryInfo(string materialIdOrName, string factoryIdOrName, string inventoryState, string stock)
        {
            string tmaterialIdOrName = null; ;
            string tFactoryIdOrName = null;
            string tInventoryState = null;
            string tStock = null;

            if (!("物料编码/物料名称/双击选择".Equals(materialIdOrName)))
            {
                tmaterialIdOrName = materialIdOrName;
            }
            if (!("工厂编码/工厂名称/双击选择".Equals(factoryIdOrName)))
            {
                tFactoryIdOrName = factoryIdOrName;
            }
            if (!("全部物料".Equals(inventoryState)))
            {
                tInventoryState = inventoryState;
            }
            else
            {
                tInventoryState = null;
            }
            if (!("库存编码/库存名称/双击选择".Equals(stock)))
            {
                tStock = stock;
            }
            return inventoryInfoIDAL.findInventoryInfo(tmaterialIdOrName, tFactoryIdOrName, tInventoryState, tStock);
        }

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string stockId, string batchId, string factoryId)
        {
            return inventoryInfoIDAL.findInventoryInfoByMaterialId(materialId,inventoryState, stockId, batchId, factoryId);
        }

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string batchId)
        {
            return inventoryInfoIDAL.findInventoryInfoByMaterialId(materialId, inventoryState, batchId);
        }
    }
}
