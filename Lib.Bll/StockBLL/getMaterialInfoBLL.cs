﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using DALFactory;
using System.Data;


namespace Lib.Bll.StockBLL
{
    
    public class getMaterialInfoBLL
    {
        public static readonly getMaterialInfoIDAL getmaterialinfodal = DALFactoryHelper.CreateNewInstance<getMaterialInfoIDAL>("StockDAL.getMaterialInfoDAL");


        /// <summary>
        /// 由物料编码读出物料名
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialName(string mid)
        {
            return getmaterialinfodal.getMaterialName(mid);
        }

         /// <summary>
       /// 由物料编号获得物料类型
       /// </summary>
       /// <param name="materialid"></param>
       /// <returns></returns>
        public string getMaterialType(string materialid)
        {
            return getmaterialinfodal.getMaterialType(materialid); 
        }

         /// <summary>
        /// 由物料编号获得物料单位
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMeasurement(string materialid)
        {
            return getmaterialinfodal.getMeasurement(materialid);
        }

              /// <summary>
        /// 由物料编号获得物料组
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialGroup(string materialid)
        {

            return getmaterialinfodal.getMaterialGroup(materialid);
        }

         /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable getMaterialID()
        {
            return getmaterialinfodal.getMaterialID();
        }

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        public DataTable getFactoryID() 
        {
            return getmaterialinfodal.getFactoryID();
        }

           /// <summary>
        /// 获取所有库存地ID
        /// </summary>
        /// <returns></returns>
        public DataTable getStockID()
        {
            return getmaterialinfodal.getStockID();
        }

              /// <summary>
        /// 获取所有库存盘点凭证号
        /// </summary>
        /// <returns></returns>
        public DataTable getStocktakingDocuid()
        {
            return getmaterialinfodal.getStocktakingDocuid();
        }

        /// <summary>
        /// 获取所有库存盘点凭证号(循环盘点)  
        /// </summary>
        /// <returns></returns>
        public DataTable getloopStocktakingDocuid()
        {
            return getmaterialinfodal.getloopStocktakingDocuid(); 
        }
        /// <summary>
        /// 获取全部批次号
        /// </summary>
        /// <returns></returns>
        public DataTable getbatchid()
        {
            return getmaterialinfodal.getbatchid();
        }

        /// <summary>
        /// 获取质检结果
        /// </summary>
        /// <returns></returns>
        public DataTable getCheckResult()
        {
            return getmaterialinfodal.getCheckResult();
        }

        /// <summary>
        /// 获取质检分数
        /// </summary>
        /// <returns></returns>
        public DataTable getCheckScore(string checkresult)
        {
            return getmaterialinfodal.getCheckScore(checkresult);
        }
    }
}
