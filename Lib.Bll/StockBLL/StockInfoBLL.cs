﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class StockInfoBLL
    {
        public static readonly StockInfoIDAL stockInfoIDAL = DALFactoryHelper.CreateNewInstance<StockInfoIDAL>("StockInfoDAL");

        /// <summary>
        /// 根据工厂ID和库存id查询仓库信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId,string stockIdOrName)
        {
            string tStockIdOrName = null;
            if (!("仓库编码/仓库名称".Equals(stockIdOrName)))
            {
                tStockIdOrName = stockIdOrName;
            }

            return stockInfoIDAL.findStockInfoByFactoryId(factoryId, tStockIdOrName);
        }

        /// <summary>
        /// 根据工厂ID查询仓库信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId)
        {
            return stockInfoIDAL.findStockInfoByFactoryId(factoryId);
        }

        /// <summary>
        /// 根据
        /// </summary>
        /// <param name="stockId"></param>
        /// <returns></returns>
        public StockInfoExtendModel getStockInfoByStockId(string stockId)
        {
            DataTable dt = stockInfoIDAL.getStockInfoByStockId(stockId);
            StockInfoExtendModel stockInfoModel = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                stockInfoModel = new StockInfoExtendModel();
                DataRow currentRow = dt.Rows[0];
                stockInfoModel.ID = currentRow["ID"].ToString();
                stockInfoModel.Stock_ID = currentRow["Stock_ID"].ToString();
                stockInfoModel.Stock_Name = currentRow["Stock_Name"].ToString();
                stockInfoModel.Address = currentRow["Address"].ToString();
                stockInfoModel.Factory_ID = currentRow["Factory_ID"].ToString();
            }
            return stockInfoModel;
        }
    }
}
