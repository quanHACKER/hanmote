﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DALFactory;
using Lib.IDAL.StockIDAL.StockReport;

namespace Lib.Bll.StockBLL.StockReport
{
    public class StockOverviewBLL
    {
        public static readonly StockOverviewIDAL sov = DALFactoryHelper.CreateNewInstance<StockOverviewIDAL>("StockDAL.StockReportDAL.StockOverviewDAL");

         /// <summary>
        /// 根据用户指定的要求筛选出符合要求的数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
       public  DataTable selectInfo(DataTable dt)
        {
            return sov.selectInfo(dt);
        }
    }
}
