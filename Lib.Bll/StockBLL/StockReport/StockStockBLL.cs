﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class StockStockBLL
    {
        private static readonly StockStockIDAL stockstock = DALFactoryHelper.CreateNewInstance<StockStockIDAL>("StockDAL.StockStockDAL");

        public DataTable showStock(DataTable dt0)
        {
            return stockstock.showStock(dt0);
        }
    }
}
