﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DALFactory;
using Lib.IDAL.StockIDAL.StockReport;

namespace Lib.Bll.StockBLL.StockReport
{
    public class FactoryStockBLL
    {
        private static readonly FactoryStockIDAL factorystock = DALFactoryHelper.CreateNewInstance<FactoryStockIDAL>("StockDAL.StockReportDAL.FactoryStockDAL");

        public DataTable showStock(DataTable dt0)
        {
            return factorystock.showStock(dt0);
        }
    }
}
