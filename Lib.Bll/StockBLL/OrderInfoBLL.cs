﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class OrderInfoBLL
    {
        public static readonly OrderInfoIDAL orderInfoIDAL = DALFactoryHelper.CreateNewInstance<OrderInfoIDAL>("OrderInfoDAL");

        /// <summary>
        /// 根据采购订单编号查找订单信息
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        public OrderInfoExtendModel getOrderinfoByPOId(string poId)
        {
            DataTable dt = orderInfoIDAL.getOrderinfoByPOId(poId);
            OrderInfoExtendModel orderInfoExtendModel = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                orderInfoExtendModel = new OrderInfoExtendModel();
                DataRow currentRow = dt.Rows[0];
                orderInfoExtendModel.Order_ID = currentRow["Order_ID"].ToString();
                orderInfoExtendModel.Order_Type = currentRow["Order_Type"].ToString();
                orderInfoExtendModel.Supplier_ID = currentRow["Supplier_ID"].ToString();
                orderInfoExtendModel.SupplierName = currentRow["Supplier_Name"].ToString();
                orderInfoExtendModel.Address = currentRow["Address"].ToString();
                orderInfoExtendModel.Zip_Code = currentRow["Zip_Code"].ToString();
            }
            return orderInfoExtendModel;
        }

        /// <summary>
        /// 根据采购订单编号查找订单物料详情
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        public List<OrderItemExtendModel> getOrderItemByPOId(string poId)
        {
            DataTable dt = orderInfoIDAL.getOrderItemByPOId(poId);
            List<OrderItemExtendModel> orderItemList = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                orderItemList = new List<OrderItemExtendModel>();
                OrderItemExtendModel orderItemExtendModel = null;
                //遍历datatable
                foreach (DataRow currentRow in dt.Rows)
                {
                    orderItemExtendModel = new OrderItemExtendModel();
                    orderItemExtendModel.Order_Id = currentRow["Order_ID"].ToString();
                    orderItemExtendModel.Material_ID= currentRow["Mterial_ID"].ToString();
                    orderItemExtendModel.Material_Name = currentRow["Material_Name"].ToString();
                    orderItemExtendModel.Factory_ID = currentRow["Factory_ID"].ToString();
                    orderItemExtendModel.Stock_ID = currentRow["Stock_ID"].ToString();
                    orderItemExtendModel.Material_Group = currentRow["Material_Group"].ToString();
                    orderItemExtendModel.Net_Price = Convert.ToDouble(currentRow["net_Price"]);
                    orderItemExtendModel.Number = Convert.ToInt32(currentRow["Number"]);
                    orderItemExtendModel.Unit = currentRow["Unit"].ToString();
                    orderItemExtendModel.Total_price = Convert.ToDouble(currentRow["Total_price"]);
                    orderItemExtendModel.Batch_ID = currentRow["Batch_ID"].ToString();
                    orderItemExtendModel.PR_ID = currentRow["PR_ID"].ToString();
                    orderItemExtendModel.Delivery_Time = Convert.ToDateTime(currentRow["Delivery_Time"]);
                    orderItemExtendModel.Info_Type = currentRow["Info_Type"].ToString();
                    orderItemExtendModel.Info_Number = currentRow["Info_Number"].ToString();
                    orderItemExtendModel.Purchase_ID = currentRow["Purchase_ID"].ToString();
                    orderItemList.Add(orderItemExtendModel);
                }
            }
            return orderItemList;
        }

    }
}
