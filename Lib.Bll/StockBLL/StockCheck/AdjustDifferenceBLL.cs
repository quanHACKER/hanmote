﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using DALFactory;
using System.Data;
using System.Data.SqlClient;

namespace Lib.Bll.StockBLL
{
    public  class AdjustDifferenceBLL
    {
        private static readonly AdjustDifferenceIDAL adjdifidal = DALFactory.DALFactoryHelper.CreateNewInstance<AdjustDifferenceIDAL>("StockDAL.AdjustDifferenceDAL");

        public bool genAdjustDocument(DataTable dt)
        {
            return adjdifidal.genAdjustDocument(dt);
        }

             /// <summary>
        /// 由凭证编号选择差异调整物料基本信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable ShowDifferenceAdjustcommonDAL(string documentid)
        {
            return adjdifidal.ShowDifferenceAdjustcommonDAL(documentid);
        }

            /// <summary>
        /// 由凭证编号选择差异调整物料基本信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable ShowDifferenceAdjustDAL(string documentid)
        {
            return adjdifidal.ShowDifferenceAdjustDAL(documentid);
        }


        /// <summary>
        /// 判断凭证是否可冲销
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public bool canceled(string documentid)
        {
            return adjdifidal.canceled(documentid);
        }

        /// <summary>
        /// 更新cancel字段
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public bool updatecancel(string documentid)
        {
            return adjdifidal.updatecancel(documentid);
        }

         /// <summary>
        /// 产生取消凭证
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool genCancelDocument(DataTable dt)
        {
            return adjdifidal.genCancelDocument(dt);
        }

        /// <summary>
        /// 更新Inventory表数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool updateInventory(DataTable dt)
        {
            return adjdifidal.updateInventory(dt);
        }
    }
}
