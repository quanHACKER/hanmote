﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class CheckResultBLL
    {
        public static readonly CheckResultIDAL checkresulridal = DALFactoryHelper.CreateNewInstance<CheckResultIDAL>("StockDAL.CheckResultDAL");

        /// <summary>
        /// 根据凭证ID读出Material_Name,batchID,Inventorystate,stocktakingcount,SKU,ZC 
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public DataTable loadTable(string documentID)
        {
            return checkresulridal.loadTable(documentID);
        }

         /// <summary>
        /// 根据凭证ID读出工厂、库存地ID
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public DataTable selectbasicInfo(string documentID)
        {
            return checkresulridal.selectbasicInfo(documentID);
        }

                /// <summary>
        /// 用户输入盘点结果，点击确定后保存于数据库中
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool inputcount(DataTable dt)
        {
            return checkresulridal.inputcount(dt);
        }

    }
}
