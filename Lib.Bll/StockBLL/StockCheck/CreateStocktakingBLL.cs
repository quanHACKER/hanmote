﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using DALFactory;
using System.Data;
using System.Data.SqlClient;


namespace Lib.Bll.StockBLL
{
    public class CreateStocktakingBLL
    {
        public static readonly CreateStocktakingIDAL cstbll = DALFactoryHelper.CreateNewInstance<CreateStocktakingIDAL>("StockDAL.CreateStocktakingDAL");

        /// <summary>
        /// 选择符合用户指定条件的物料（集中创建）
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <param name="FactoryID"></param>
        /// <param name="StockID"></param>
        /// <param name="EvaluateType"></param>
        /// <param name="MaterialGroup"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="b3"></param>
        /// <returns></returns>
        public DataTable selectMaterial(string MaterialID, string FactoryID, string StockID, string EvaluateType, string MaterialGroup, bool b1, bool b2, bool b3)
        {
            return cstbll.selectMaterial(MaterialID, FactoryID, StockID, EvaluateType, MaterialGroup, b1, b2, b3);

        }

        /// <summary>
        /// 产生盘点物料凭证（集中创建和单个创建共用）
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="i"></param>
        /// <param name="dtime2"></param>
        /// <param name="referID"></param>
        /// <param name="stockID"></param>
        /// <returns></returns>
        public bool createDocument(DataTable dt, DateTime dtime2, string referID, string stockID)
        {
            return cstbll.createDocument(dt, dtime2, referID, stockID);
        }

        /// <summary>
        /// 选择满足用户指定条件的物料(单个创建)
        /// </summary>
        /// <returns></returns>
        public DataTable selectMaterialSingle(DataTable dt)
        {
            return cstbll.selectMaterialSingle(dt);
        }


        /// <summary>
        /// 根据物料编号确定物料名称
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialname(string materialid)
        {
            return cstbll.getMaterialname(materialid);
        }

        /// <summary>
        /// 盘点冻结
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        public bool freezingstock(string stocktakingDocuID)
        {
            return cstbll.freezingstock(stocktakingDocuID);
        }

        /// <summary>
        /// 盘点解冻
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        public bool defreezingstock(string stocktakingDocuID)
        {
            return cstbll.defreezingstock(stocktakingDocuID);
        }

        /// <summary>
        /// 选择物料编号（单个创建）
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        public DataTable selectMaterial(string factory, string stock)
        {
            return cstbll.selectMaterial(factory, stock);
        }
    }
}
