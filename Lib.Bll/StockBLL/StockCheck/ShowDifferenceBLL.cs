﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DALFactory;
using Lib.IDAL.StockIDAL;


namespace Lib.Bll.StockBLL
{
    public class ShowDifferenceBLL
    {
        public static readonly ShowDifferenceIDAL sdfidal = DALFactoryHelper.CreateNewInstance<ShowDifferenceIDAL>("StockDAL.ShowDifferenceDAL");

        /// <summary>
        /// 选择符合用户指定条件的物料
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <param name="FactoryID"></param>
        /// <param name="StockID"></param>
        /// <param name="EvaluateType"></param>
        /// <param name="MaterialGroup"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="b3"></param>
        /// <returns></returns>
        public DataTable selectInfo(DataTable dt)
        {
            return sdfidal.selectInfo(dt);

        }
    }
}
