﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class StockReceivingBLL
    {
        public static readonly StockMaterialInfoIDAL stockMaterialInfoIDAL = DALFactoryHelper.CreateNewInstance<StockMaterialInfoIDAL>("StockMaterialInfoDAL");

        public static readonly StockSupplierInfoIDAL stockSupplierInfoIDAL = DALFactoryHelper.CreateNewInstance<StockSupplierInfoIDAL>("StockSupplierInfoDAL");
        /// <summary>
        /// 根据订单id来查找与该订单关联的物料信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public OrderMatReceivingModel findOrderMatInfoById(string orderId)
        {
            return stockMaterialInfoIDAL.findOrderMatInfoById(orderId);
        }

        /// <summary>
        /// 根据订单ID来查找与该订单号关联的供应商详细信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public OrderSupReceivingModel findOrderSupInfoById(string orderId)
        {
            return stockSupplierInfoIDAL.findOrderSupInfoById(orderId);
        }
    }
}
