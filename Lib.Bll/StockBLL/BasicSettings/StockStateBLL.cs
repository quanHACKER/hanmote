﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class StockStateBLL
    {
        public static readonly StockStateIDAL stockStateIDAL = DALFactoryHelper.CreateNewInstance<StockStateIDAL>("StockStateDAL");

        /// <summary>
        /// 新增一个库存类型
        /// </summary>
        /// <param name="stockTypeModel">库存类型模板</param>
        /// <returns></returns>
        public int insertStockState(StockStateModel stockStateModel)
        {
            return stockStateIDAL.insertStockState(stockStateModel);
        }

        /// <summary>
        /// 根据类型Id修改库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int updateStockStateByStateId(StockStateModel stockStateModel)
        {
            return stockStateIDAL.updateStockStateByStateId(stockStateModel);
        }

        /// <summary>
        /// 根据类型Id删除库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int deleteStockStateByStateId(int typeId)
        {
            return stockStateIDAL.deleteStockStateByStateId(typeId);
        }

        /// <summary>
        /// 根据类型Id恢复库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int restoreStockStateByStateId(int typeId)
        {
            return stockStateIDAL.restoreStockStateByStateId(typeId);
        }

        /// <summary>
        /// 获得最新获得的TypeId
        /// </summary>
        /// <returns></returns>
        public int getLatestStateId() 
        {
            return stockStateIDAL.getLatestStateId();
        }

        /// <summary>
        /// 获得所有的库存类型
        /// </summary>
        /// <returns></returns>
        public List<StockStateModel> getAllStockState()
        {
            //声明一个只存放StockTypeModel类型的集合
            List<StockStateModel> stockStateModelList = new List<StockStateModel>();
            //获得查询结果
            DataTable dt = stockStateIDAL.getAllStockState();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    StockStateModel stockStateModel = new StockStateModel();
                    stockStateModel.Id = Convert.ToInt32(dr["Id"].ToString());
                    stockStateModel.State = dr["State"].ToString();
                    stockStateModel.Valid = Convert.ToInt32(dr["Valid"].ToString());
                    stockStateModelList.Add(stockStateModel);
                }
                return stockStateModelList;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得所有有效的库存类型
        /// 此方法只取有效的库存类型名称
        /// </summary>
        /// <returns></returns>
        public List<string> getValidStockStateName()
        {
            //获取查询结果
            DataTable dt = stockStateIDAL.getValidStockState();
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> stockStateNameList = new List<string>();
                string stockStateName;
                foreach (DataRow dr in dt.Rows)
                {
                    stockStateName = dr["State"].ToString();
                    //将查到的库存类型数据依次添加到集合中 
                    stockStateNameList.Add(stockStateName);
                }
                return stockStateNameList;
            }
            else
            {
                return null;
            }
        }

    }
}
