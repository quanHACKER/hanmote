﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class StockTypeBLL
    {
        public static readonly StockTypeIDAL stockTypeIDAL = DALFactoryHelper.CreateNewInstance<StockTypeIDAL>("StockTypeDAL");

        /// <summary>
        /// 新增一个库存类型
        /// </summary>
        /// <param name="stockTypeModel">库存类型模板</param>
        /// <returns></returns>
        public int insertStockType(StockTypeModel stockTypeModel)
        {
            return stockTypeIDAL.insertStockType(stockTypeModel);
        }

        /// <summary>
        /// 根据类型Id修改库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int updateStockTypeByTypeId(StockTypeModel stockTypeModel)
        {
            return stockTypeIDAL.updateStockTypeByTypeId(stockTypeModel);
        }

        /// <summary>
        /// 根据类型Id删除库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int deleteStockTypeByTypeId(int typeId)
        {
            return stockTypeIDAL.deleteStockTypeByTypeId(typeId);
        }

        /// <summary>
        /// 根据类型Id恢复库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int restoreStockTypeByTypeId(int typeId)
        {
            return stockTypeIDAL.restoreStockTypeByTypeId(typeId);
        }

        /// <summary>
        /// 获得最新获得的TypeId
        /// </summary>
        /// <returns></returns>
        public int getLatestTypeId() 
        {
            return stockTypeIDAL.getLatestTypeId();
        }

        /// <summary>
        /// 获得所有的库存类型
        /// </summary>
        /// <returns></returns>
        public List<StockTypeModel> getAllStockType()
        {
            //获得查询结果
            DataTable dt = stockTypeIDAL.getAllStockType();
            if (dt != null && dt.Rows.Count > 0)
            {
                //声明一个只存放StockTypeModel类型的集合
                List<StockTypeModel> stockTypeModelList = new List<StockTypeModel>();
                foreach (DataRow dr in dt.Rows)
                {
                    StockTypeModel stockTypeModel = new StockTypeModel();
                    stockTypeModel.Id = Convert.ToInt32(dr["Id"].ToString());
                    stockTypeModel.Type = dr["Type"].ToString();
                    stockTypeModel.Valid = Convert.ToInt32(dr["Valid"].ToString());
                    stockTypeModelList.Add(stockTypeModel);
                }
                return stockTypeModelList;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得所有有效的库存类型
        /// 此方法只取有效的库存类型名称
        /// </summary>
        /// <returns></returns>
        public List<string> getValidStockTypeName()
        {
            //获取查询结果
            DataTable dt = stockTypeIDAL.getValidStockType();
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> stockTypeNameList = new List<string>();
                string stockTypeName;
                foreach (DataRow dr in dt.Rows)
                {
                    stockTypeName = dr["Type"].ToString();
                    //将查到的库存类型数据依次添加到集合中 
                    stockTypeNameList.Add(stockTypeName);
                }
                return stockTypeNameList;
            }
            else
            {
                return null;
            }
        }

    }
}
