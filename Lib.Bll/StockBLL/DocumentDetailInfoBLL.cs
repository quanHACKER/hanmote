﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.IDAL.StockIDAL;
using DALFactory;
using Lib.Model.StockModel;

namespace Lib.Bll.StockBLL
{
    public class DocumentDetailInfoBLL
    {
        public static readonly DocumentDetailInfoIDAL documentDetailInfoIDAL = DALFactoryHelper.CreateNewInstance<DocumentDetailInfoIDAL>("DocumentDetailInfoDAL");

        public int insertDocumentDetailInfo(List<DocumentDetailInfoModel> documentDetailInfoModelList)
        {
            return documentDetailInfoIDAL.insertDocumentDetailInfo(documentDetailInfoModelList);
        }

        /// <summary>
        /// 根据采购订单编号查找订单物料详情
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        public List<DocumentDetailInfoModelExtend> getMDocItemByDocId(string mDocId)
        {
            DataTable dt = documentDetailInfoIDAL.getMDocItemByDocId(mDocId);
            List<DocumentDetailInfoModelExtend> mDocItemList = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                mDocItemList = new List<DocumentDetailInfoModelExtend>();
                DocumentDetailInfoModelExtend mDocNewExtendModel = null;
                //遍历datatable
                foreach (DataRow currentRow in dt.Rows)
                {
                    mDocNewExtendModel = new DocumentDetailInfoModelExtend();
                    mDocNewExtendModel.StockInDocument_ID = currentRow["StockInDocument_ID"].ToString();
                    mDocNewExtendModel.SMaterial_ID = currentRow["SMaterial_ID"].ToString();
                    mDocNewExtendModel.Order_ID = currentRow["Order_ID"].ToString();
                    mDocNewExtendModel.Delivery_ID = currentRow["Delivery_ID"].ToString();
                    mDocNewExtendModel.ReceiptNote_ID = currentRow["ReceiptNote_ID"].ToString();
                    mDocNewExtendModel.SFactory_ID = currentRow["SFactory_ID"].ToString();
                    mDocNewExtendModel.SStock_ID = currentRow["SStock_ID"].ToString();
                    mDocNewExtendModel.Supplier_ID = currentRow["Supplier_ID"].ToString();
                    mDocNewExtendModel.Inventory_Type = currentRow["Inventory_Type"].ToString();
                    mDocNewExtendModel.Receive_Count = Convert.ToInt32(currentRow["Receive_Count"]);
                    mDocNewExtendModel.Order_Count = Convert.ToInt32(currentRow["Order_Count"]);
                    mDocNewExtendModel.UnitPrice = (float)Convert.ToDouble(currentRow["UnitPrice"]);
                    mDocNewExtendModel.Input_Count = Convert.ToInt32(currentRow["Input_Count"]);
                    mDocNewExtendModel.SKU_Count = Convert.ToInt32(currentRow["SKU_Count"]);
                    mDocNewExtendModel.Notes_Count = Convert.ToInt32(currentRow["Notes_Count"]);
                    mDocNewExtendModel.Batch_ID = currentRow["Batch_ID"].ToString();
                    mDocNewExtendModel.SupplierMaterial_ID = currentRow["SupplierMaterial_ID"].ToString();
                    mDocNewExtendModel.Material_Group = currentRow["Material_Group"].ToString();
                    mDocNewExtendModel.Evaluate_Type = currentRow["Evaluate_Type"].ToString();
                    mDocNewExtendModel.Tolerance = Convert.ToInt32(currentRow["Tolerance"]);
                    mDocNewExtendModel.Loading_Place = currentRow["Loading_Place"].ToString();
                    mDocNewExtendModel.Receiving_Place = currentRow["Receiving_Place"].ToString();
                    mDocNewExtendModel.TMaterial_ID = currentRow["TMaterial_ID"].ToString();
                    mDocNewExtendModel.TFactory_ID = currentRow["TFactory_ID"].ToString();
                    mDocNewExtendModel.TStock_ID = currentRow["TStock_ID"].ToString();
                    mDocNewExtendModel.TMaterial_Name = currentRow["TMaterial_Name"].ToString();
                    mDocNewExtendModel.TFactory_Name = currentRow["TFactory_Name"].ToString();
                    mDocNewExtendModel.TStock_Name = currentRow["TStock_Name"].ToString();
                    mDocNewExtendModel.SpecialStock = currentRow["SpecialStock"].ToString();
                    mDocNewExtendModel.ExplainText = currentRow["ExplainText"].ToString();
                    mDocNewExtendModel.StorehouseUnit = currentRow["StorehouseUnit"].ToString();
                    mDocNewExtendModel.Storehouse_ID = currentRow["Storehouse_ID"].ToString();
                    mDocNewExtendModel.Unit = currentRow["Unit"].ToString();
                    mDocItemList.Add(mDocNewExtendModel);
                }
            }
            return mDocItemList;
        }
    }
}
