﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.FileManage;
using Lib.Model.FileManage;
using Lib.SqlServerDAL.FileManageDAL;

namespace Lib.Bll.FileManageBLL
{
    public class FileBLL
    {
        FileInfoIDAL fileInfoDAL = DALFactory.DALFactoryHelper
            .CreateNewInstance<FileInfoIDAL>("FileManageDAL.FileInfoDAL");
        FileAttachmentInfoIDAL fileAttachmentInfoDAL = DALFactory.DALFactoryHelper
            .CreateNewInstance<FileAttachmentInfoIDAL>("FileManageDAL.FileAttachmentInfoDAL");

        /// <summary>
        /// 获取文件信息
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public File_Info getFileInfo(string fileID) {
            return fileInfoDAL.getFileInfo(fileID);
        }

        /// <summary>
        /// 条件化获取文件列表
        /// </summary>
        /// <param name="conditions">条件词典</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public List<File_Info> getFileInfoListByConditions(Dictionary<string, string> conditions,
            DateTime beginTime, DateTime endTime) {

            return fileInfoDAL.getFileInfoListByConditions(conditions, beginTime, endTime);
        }

        /// <summary>
        /// 添加文件信息(主体信息+附件信息)
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="fileAttachmentList"></param>
        /// <returns></returns>
        public int addFileInfo(File_Info fileInfo, List<File_Attachment_Info> fileAttachmentList) {
            return fileInfoDAL.addFileInfo(fileInfo, fileAttachmentList);
        }

        /// <summary>
        /// 更新文件信息(主体信息+附件信息)
        /// </summary>
        /// <param name="fileInfo">主体信息</param>
        /// <param name="fileAttachmentList">附件信息</param>
        /// <returns></returns>
        public int updateFileInfo(File_Info fileInfo, List<File_Attachment_Info> fileAttachmentList) {
            return fileInfoDAL.updateFileInfo(fileInfo, fileAttachmentList);
        }

        /// <summary>
        /// 删除文件信息
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public int deleteInfo(string fileID) {
            return fileInfoDAL.deleteFileInfo(fileID);
        }

        /// <summary>
        /// 获取文件附件列表
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public List<File_Attachment_Info> getFileAttachmentList(string fileID) {
            return fileAttachmentInfoDAL.getFileAttachmentList(fileID);
        }

        /// <summary>
        /// 删除一个附件数据库信息
        /// </summary>
        /// <param name="fileID">文件编号</param>
        /// <param name="attachmentName">附件名称</param>
        /// <returns>受影响行数</returns>
        public int deleteFileAttachment(string fileID, string attachmentName) {

            return fileAttachmentInfoDAL.deleteFileAttachment(fileID, attachmentName);
        }
    }
}
