﻿using DALFactory;
using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.SystemConfig.UserManage
{   /// <summary>
    /// author：hezl
    /// </summary>
    public class CompanyUserBLL
    {
        CompanyUserIDAL companyUserIDAL = DALFactoryHelper.CreateNewInstance<CompanyUserIDAL>("SystemConfig.CompanyUserDAL");
        /// <summary>
        /// 得到某公司旗下所有信息
        /// </summary>
        /// <param name="compangID"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable getCompanyUserList(String compangID, int pageNum, int frontNum)
        {
            return companyUserIDAL.findAllCompanyuUserList(compangID, pageNum, frontNum);
        }
        /// <summary>
        /// 
        /// 条件查询
        /// </summary>
        /// <param name="compangID"></param>
        /// <param name="Username"></param>
        /// <param name="Login_Name"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable getCompanyListByCondition(String companyName, int pageNum, int frontNum)
        {
            return companyUserIDAL.findAllCompanyListByCondition(companyName, pageNum, frontNum);
        }
        /// <summary>
        /// add userInfo
        /// </summary>
        /// <param name="model"></param>
        public void addCompanyUserInfo(CompanyUserModel model)
        {
            companyUserIDAL.addCompanyUserInfo(model);
        }

        public int calculateUserNumber()
        {
            DataTable userInforTable = companyUserIDAL.calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }

        /// <summary>
        /// 
        /// 编辑用户信息
        /// </summary>
        /// <param name="companyUserModel"></param>
        public void editCompanyUserInfo(CompanyUserModel companyUserModel)
        {
            companyUserIDAL.editCompanyUserIndo(companyUserModel);
        }
        

        public void delCompanyUserInfo(String Supplier_Id ,String user_ID)    
        {
            companyUserIDAL.delCompanyUserInfo(Supplier_Id,user_ID);
        }


    }
}
