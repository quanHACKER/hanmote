﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class UserGroupDetailInforBLL
    {
        BaseGroupIDAL baseGroupIDAL = DALFactoryHelper.CreateNewInstance<BaseGroupIDAL>("SystemConfig.BaseGroupDAL");

        /// <summary>
        /// 获取可用最大用户组编号
        /// </summary>
        /// <returns></returns>
        public String getBiggestGroupId()
        {
            String today = DateTime.Now.ToString("yyyyMMdd");
            DataTable groupTable = baseGroupIDAL.queryBiggestGroupID(today);
            if (groupTable == null)
            {
                throw new Exception("生成用户组编号失败！");
            }

            if (groupTable.Rows.Count > 0 && !groupTable.Rows[0][0].Equals(DBNull.Value))
            {
                String currentId = Convert.ToString(groupTable.Rows[0][0]);
                int seq = Convert.ToInt32(currentId.Substring(8, 5));
                String newSeq = Convert.ToString(seq + 1);
                if (seq >= 1 && seq < 9)
                    return today + "0000" + newSeq;
                else if (seq >= 9 && seq < 99)
                    return today + "000" + newSeq;
                else if (seq >= 99 && seq < 999)
                    return today + "00" + newSeq;
                else if (seq >= 999 && seq < 9999)
                    return today + "0" + newSeq;
                else if (seq >= 9999 && seq < 99999)
                    return today + newSeq;
                else
                    throw new Exception("今日可用编号已达到上限，请择日重试！");
            }
            else
            {
                return today + "00001";
            }
        }

        /// <summary>
        /// 插入新的用户组
        /// </summary>
        /// <param name="groupModel"></param>
        public void insertNewGroup(BaseGroupModel groupModel)
        {
            baseGroupIDAL.insertNewGroup(groupModel);
        }

        /// <summary>
        /// 查询用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        public void queryGroupInforById(BaseGroupModel groupModel)
        {
            DataTable groupTable = baseGroupIDAL.queryGroupInforById(groupModel);
            if (groupTable != null && groupTable.Rows.Count > 0)
            {
                groupModel.Group_Name = Convert.ToString(groupTable.Rows[0]["Group_Name"]);
                groupModel.Description = Convert.ToString(groupTable.Rows[0]["Description"]);
                groupModel.Generate_Time = Convert.ToString(groupTable.Rows[0]["Generate_Time"]);
                groupModel.Generate_User_ID = Convert.ToString(groupTable.Rows[0]["Generate_User_ID"]);
                groupModel.Generate_Username = Convert.ToString(groupTable.Rows[0]["Generate_Username"]);
                groupModel.Modify_Time = Convert.ToString(groupTable.Rows[0]["Modify_Time"]);
                groupModel.Modify_User_ID = Convert.ToString(groupTable.Rows[0]["Modify_User_ID"]);
                groupModel.Modify_Username = Convert.ToString(groupTable.Rows[0]["Modify_Username"]);
            }
            else
            {
                throw new Exception("查询用户组信息失败！");
            }
        }

        /// <summary>
        /// 更新用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        public void updateGroupInforById(BaseGroupModel groupModel)
        {
            baseGroupIDAL.updateGroupInforById(groupModel);
        }
    }
}
