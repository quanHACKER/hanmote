﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using DALFactory;
using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class UserRoleManageBLL
    {
        BaseRoleIDAL baseRole = DALFactoryHelper.CreateNewInstance<BaseRoleIDAL>("SystemConfig.BaseRoleDAL");
        JoinUserRoleIDAL joinUserRole = DALFactoryHelper.CreateNewInstance<JoinUserRoleIDAL>("SystemConfig.JoinUserRoleDAL");

        /// <summary>
        /// 添加角色给用户，即写入用户和角色的联系
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="roleModel"></param>
        public void addRoleToUser(JoinUserRoleModel userRoleModel)
        {
            DataTable userRoleTable = joinUserRole.checkUserRoleLinkExist(userRoleModel);
            if (userRoleTable != null && userRoleTable.Rows.Count <= 0)
            {
                joinUserRole.addRoleToUser(userRoleModel);
            }
        }

        /// <summary>
        /// 从用户收回角色，即移除用户和角色间联系
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="roleModel"></param>
        public void removeRoleFromUser(JoinUserRoleModel userRoleModel)
        {
            joinUserRole.removeRoleFromUser(userRoleModel);
        }

        /// <summary>
        /// 插入角色
        /// </summary>
        /// <param name="roleModel"></param>
        public void insertRole(BaseRoleModel roleModel)
        {
            baseRole.insertRole(roleModel);
        }

        /// <summary>
        /// 查询所有角色信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllRoleInfor()
        {
            return baseRole.queryAllRoleInfor();
        }

        /// <summary>
        /// 查询当前角色及所有子角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public DataTable queryOffspringInfor(BaseRoleModel roleModel)
        {
            return baseRole.queryOffspringInfor(roleModel);
        }

        /// <summary>
        /// 根据角色编号查询其关联的用户
        /// </summary>
        /// <returns></returns>
        public DataTable queryUserInforByRoleId(BaseRoleModel roleModel)
        {
            return baseRole.queryUserInforByRoleId(roleModel);
        }

        /// <summary>
        /// 删除某角色和用户的关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void deleteUserRoleLinkByRoleId(JoinUserRoleModel userRoleModel)
        {
            baseRole.deleteUserRoleLinkByRoleId(userRoleModel);
        }

        /// <summary>
        /// 删除数据库中当前节点及所有子节点并更新受影响节点的左右值
        /// </summary>
        /// <param name="roleModel"></param>
        public void deleteOffspringOrganizationAndUpdateNodes(BaseRoleModel roleModel)
        {
            baseRole.deleteOffspringOrganizationAndUpdateNodes(roleModel);
        }

        /// <summary>
        /// 插入用户和角色关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void insertUserRoleLink(JoinUserRoleModel userRoleModel)
        {
            baseRole.insertUserRoleLink(userRoleModel);
        }

    }
}
