﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class RoleListBLL
    {
        JoinUserRoleIDAL joinUserRoleIDAL = DALFactoryHelper.CreateNewInstance<JoinUserRoleIDAL>("SystemConfig.JoinUserRoleDAL");

        /// <summary>
        /// 查询用户所具有的角色信息
        /// </summary>
        /// <param name="joinUserRoleModel"></param>
        /// <returns></returns>
        public DataTable queryRoleByUserId(JoinUserRoleModel joinUserRoleModel)
        {
            return joinUserRoleIDAL.queryRoleByUserId(joinUserRoleModel);
        }


    }
}
