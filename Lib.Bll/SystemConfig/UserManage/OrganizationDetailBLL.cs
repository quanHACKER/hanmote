﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class OrganizationDetailBLL
    {
        BaseOrganizationIDAL organizationDAL = DALFactory.DALFactoryHelper.CreateNewInstance<BaseOrganizationDAL>("SystemConfig.BaseOrganizationDAL");

        /// <summary>
        /// 获取当前可用的最大组织机构编号
        /// </summary>
        /// <returns></returns>
        public String getUserfulOrganizationID()
        {
            String today = DateTime.Now.ToString("yyyyMMdd");
            DataTable orgTable = organizationDAL.queryBiggestOrganizationID(today);
            if (orgTable != null && orgTable.Rows.Count > 0 && !orgTable.Rows[0][0].Equals(DBNull.Value))
            {
                String organizationID = Convert.ToString(orgTable.Rows[0][0]);
                int sequence = Convert.ToInt32(organizationID.Substring(8,5));
                if(sequence >= 1 && sequence < 9)
                {
                    return today + "0000" + Convert.ToString(sequence + 1);
                }
                else if(sequence >= 9 && sequence < 99)
                {
                    return today + "000" + Convert.ToString(sequence + 1);
                }
                else if (sequence > 99 && sequence < 999)
                {
                    return today + "00" + Convert.ToString(sequence + 1);
                }
                else if (sequence >= 999 && sequence < 9999)
                {
                    return today + "0" + Convert.ToString(sequence + 1);
                }
                else
                {
                    throw new Exception("当日组织编号已分配完，请改天再试！ ");
                }
            }
            else
            {
                return today + "00001";
            }
        }


    }
}
