﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using DALFactory;
using Lib.Model.SystemConfig;

namespace Lib.Bll.SystemConfig.UserManage
{
    /// <summary>
    /// 用户信息BLL
    /// </summary>
    public class UserDetailInforBLL
    {
        private BaseUserIDAL baseUser = DALFactory.DALFactoryHelper.CreateNewInstance<BaseUserIDAL>("SystemConfig.BaseUserDAL");
        private CompanyUserIDAL companyUser = DALFactory.DALFactoryHelper.CreateNewInstance<CompanyUserIDAL>("SystemConfig.CompanyUserDAL");
        /// <summary>
        /// 获取一个可用的顺序用户编号
        /// </summary>
        /// <returns>可用的顺序用户编号</returns>
        public String getValidUserID()
        {
            String today = DateTime.Now.ToLocalTime().ToString("yyyyMMdd");     //获取当前日期
            DataTable userIdTable = baseUser.queryBiggestUserID(today);
            if (userIdTable == null)
            {
                throw new Exception("获取用户编号失败！");
            }

            if (userIdTable.Rows.Count > 0 && !userIdTable.Rows[0][0].Equals(DBNull.Value))
            {
                String currentId = Convert.ToString(userIdTable.Rows[0][0]);
                int seq = Convert.ToInt32(currentId.Substring(8,5));
                String newSeq = Convert.ToString(seq + 1);
                if (seq >= 1 && seq < 9)
                    return today + "0000" + newSeq;
                else if (seq >= 9 && seq < 99)
                    return today + "000" + newSeq;
                else if (seq >= 99 && seq < 999)
                    return today + "00" + newSeq;
                else if (seq >= 999 && seq < 9999)
                    return today + "0" + newSeq;
                else if (seq >= 9999 && seq < 99999)
                    return today + newSeq;
                else
                    throw new Exception("今日可用编号已达到上限，请择日重试！");
            }
            else
            {
                return today + "00001";
            }
        }

        /// <summary>
        /// 创建一个新用户
        /// </summary>
        /// <param name="newUserModel">插入的用户的Model</param>
        public void insertNewUser(CompanyUserModel baseUserModel)
        {
            companyUser.addCompanyUserInfo(baseUserModel);
        }

        /// <summary>
        /// 查询用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        public void queryUserInforById(BaseUserModel baseUserModel)
        {
            DataTable userInforTable = baseUser.queryUserInformationById(baseUserModel);
            if (userInforTable != null && userInforTable.Rows.Count > 0)
            {
                DataRow row = userInforTable.Rows[0];
                baseUserModel.Login_Name = Convert.ToString(row["Login_Name"]);
                baseUserModel.Login_Password = Convert.ToString(row["Login_Password"]);
                baseUserModel.Username = Convert.ToString(row["Username"]);
                baseUserModel.Gender = Convert.ToInt32(row["Gender"]);
                baseUserModel.Birthday = Convert.ToString(row["Birthday"]);
                baseUserModel.Birth_Place = Convert.ToString(row["Birth_Place"]);
                baseUserModel.ID_Number = Convert.ToString(row["ID_Number"]);
                baseUserModel.Mobile = Convert.ToString(row["Mobile"]);
                baseUserModel.Email = Convert.ToString(row["Email"]);
                baseUserModel.User_Description = Convert.ToString(row["User_Description"]);
                baseUserModel.Question = Convert.ToString(row["Question"]);
                baseUserModel.Answer_Question = Convert.ToString(row["Answer_Question"]);
                baseUserModel.Enabled = Convert.ToInt32(row["Enabled"]);
                baseUserModel.Manager_Flag = Convert.ToInt32(row["Manager_Flag"]);
                baseUserModel.Generate_Time = Convert.ToString(row["Generate_Time"]);
                baseUserModel.Generate_User_ID = Convert.ToString(row["Generate_User_ID"]);
                baseUserModel.Generate_Username = Convert.ToString(row["Generate_Username"]);
                baseUserModel.Modify_Time = Convert.ToString(row["Modify_Time"]);
                baseUserModel.Modify_User_ID = Convert.ToString(row["Modify_User_ID"]);
                baseUserModel.Modify_Username = Convert.ToString(row["Modify_Username"]);
            }
        }

        /// <summary>
        /// 更新用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        public void updateUserInfor(CompanyUserModel baseUserModel)
        {
            companyUser.editCompanyUserIndo(baseUserModel);
           // companyUser.updateUserInformation(baseUserModel);
        }


    }
}
