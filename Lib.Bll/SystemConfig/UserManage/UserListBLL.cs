﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class UserListBLL
    {
        BaseUserIDAL baseUserIDAL = DALFactoryHelper.CreateNewInstance<BaseUserIDAL>("SystemConfig.BaseUserDAL");

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public DataTable getUserList(int pageNum, int frontNum)
        {
            return baseUserIDAL.queryAllUserInformation(pageNum, frontNum);
        }

        /// <summary>
        /// 统计用户记录数目
        /// </summary>
        /// <returns></returns>
        public int calculateUserNumber()
        {
            DataTable userInforTable = baseUserIDAL.calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }

        public DataTable getSysUserList(int v1, int v2)
        {
            return baseUserIDAL.getSysUserList(v1,v2);
        }

        public DataTable getUserListBySearchCondition(BaseUserModel userModel)
        {
            return baseUserIDAL.queryUserInformationBySearchCondition(userModel);
        }

        public DataTable getAuthorNodes(string user_ID)
        {
            return baseUserIDAL.getAuthorNodes(user_ID);
        }
    }
}
