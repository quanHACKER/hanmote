﻿using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class SystemUserOrganicRelationshipBLL
    {
        SystemUserOrganicRelationshipIDAL systemUserOrganicRelationshipIDAL = DALFactory.DALFactoryHelper.CreateNewInstance<SystemUserOrganicRelationshipIDAL>("SystemConfig.SystemUserOrganicRelationshipDAL");
        /// <summary>
        /// 删除/激活
        /// </summary>
        /// <param name="userID"></param>
        public void delOrActivitiUser(string userID, int a)
        {
            systemUserOrganicRelationshipIDAL.delOrActivitiUser(userID,a);
        }
        /// <summary>
        /// 查找所有信息
        /// </summary>
        /// <returns></returns>
        public DataTable findAllSystemUserOrganicRelationshipInfo(int pageNum ,int front,int type)
        {
            return systemUserOrganicRelationshipIDAL.findAllSystemUserOrganicRelationshipInfo(pageNum,front,type);
        }

        public DataTable findAllPurGroupName()
        {
            return systemUserOrganicRelationshipIDAL.findAllPurGroupName();
        }

        public DataTable getUserInfoByRoleName(String role)
        {
            return systemUserOrganicRelationshipIDAL.getUserInfoByRoleName(role);
        }

        public DataTable findAllMtGroupName()
        {
            return systemUserOrganicRelationshipIDAL.findAllMtGroupName();
        }

        public DataTable getInfoByRoleUserGroup()
        {
            return systemUserOrganicRelationshipIDAL.getInfoByRoleUserGroup();
        }
        

        public DataTable findAllMtGroupNameByPurGroupName(string name,string userID,int type)
        {
            return systemUserOrganicRelationshipIDAL.findAllMtGroupNameByPurGroupName(name, userID,type);
         
        }

        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable findAllSystemUserOrganicRelationshipInfoByCondition(SystemUserOrganicRelationshipModel model, int pageNum, int front)
        {
            return systemUserOrganicRelationshipIDAL.findAllSystemUserOrganicRelationshipInfoByCondition(model, pageNum, front);
        }

        public void insertNewUser(SystemUserOrganicRelationshipModel model)
        {
             systemUserOrganicRelationshipIDAL.insertNewUser(model);
        }

        /// <summary>
        /// 总数
        /// </summary>
        /// <returns></returns>
        public int calculateUserNumber()
        {
            DataTable userInforTable = systemUserOrganicRelationshipIDAL.calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }

        public DataTable getMainEvaulatorSelectdMtGroupAndPurGroupByUserID(string userID, string purGroupName)
        {
            return systemUserOrganicRelationshipIDAL.getMainEvaulatorSelectdMtGroupAndPurGroupByUserID(userID, purGroupName );
        }

        public DataTable findAllSystemUserOrganicRelationshipInfoByCondition(string userName, string departName,int type)
        {
            return systemUserOrganicRelationshipIDAL.findAllSystemUserOrganicRelationshipInfoByCondition(userName, departName,type);
        }

        public void insertHanmote_User_MtGroupName(string userID, string name, string purName,int type)
        {
            systemUserOrganicRelationshipIDAL.insertHanmote_User_MtGroupName(userID, name,purName,type);
        }

        public void updateUserInfor(SystemUserOrganicRelationshipModel model)
        {
            systemUserOrganicRelationshipIDAL.updateUserInfor(model);
        }

        public void updateHanmote_User_MtGroupName(string sysUser_ID, string v,string purName,int type)
        {
            systemUserOrganicRelationshipIDAL.updateHanmote_User_MtGroupName(sysUser_ID, v,purName,type);
        }
        /// <summary>
        /// 1 删除前有的评估员身份 2 删除前有的主审身份
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <param name="a"></param>
        public void deleteHamote_User_MtGroup(string sysUser_ID,int a)
        {
            systemUserOrganicRelationshipIDAL.deleteHamote_User_MtGroup(sysUser_ID,a);
        }
        public void insertHanmoteBaseUser_EvalCode(string v, string code,string evalDescription)
        {
            systemUserOrganicRelationshipIDAL.insertHanmoteBaseUser_EvalCode(v, code, evalDescription);
        }
       public  void deleteHanmoteBaseUser_EvalCode(string v, string code)
        {
            systemUserOrganicRelationshipIDAL.deleteHanmoteBaseUser_EvalCode(v, code);
        }
        /// <summary>
        /// getCompanyName
        /// </summary>
        /// <returns></returns>
        public DataTable getCompanyNameSQL()
        {
            return  systemUserOrganicRelationshipIDAL.getCompanyNameSQL();
        }
        /// <summary>
        /// 得到职务名
        /// </summary>
        /// <returns></returns>
        public DataTable getRoleNameSQL(string comanyID)
        {
            return systemUserOrganicRelationshipIDAL.getRoleNameSQL(comanyID);
        }
        /// <summary>
        /// 得到部门
        /// </summary>
        /// <returns></returns>
        public DataTable getOrganicNameSQL(string comanyID)
        {
            return systemUserOrganicRelationshipIDAL.getOrganicNameSQL(comanyID);
        }

        public void queryUserInforById(SystemUserOrganicRelationshipModel baseUserModel)
        {
            DataTable userInforTable = systemUserOrganicRelationshipIDAL.queryUserInforById(baseUserModel.sysUser_ID);
            if (userInforTable != null && userInforTable.Rows.Count > 0)
            {
                DataRow row = userInforTable.Rows[0];
                baseUserModel.Login_Name = Convert.ToString(row["Login_Name"]);
                baseUserModel.Login_Password = Convert.ToString(row["Login_Password"]);
                baseUserModel.Username = Convert.ToString(row["Username"]);
                baseUserModel.Gender = Convert.ToInt32(row["Gender"]);
                baseUserModel.Birthday = Convert.ToString(row["Birthday"]);
                baseUserModel.Birth_Place = Convert.ToString(row["Birth_Place"]);
                baseUserModel.Mobile = Convert.ToString(row["Mobile"]);
                baseUserModel.Email = Convert.ToString(row["Email"]);
                baseUserModel.User_Description = Convert.ToString(row["User_Description"]);
                baseUserModel.Enabled = Convert.ToInt32(row["Enabled"]);
                baseUserModel.Manager_Flag = Convert.ToInt32(row["Manager_Flag"]);
                baseUserModel.compangClass = Convert.ToString(row["companyID"]);
                baseUserModel.jobPosition = Convert.ToString(row["Role_ID"]);
                baseUserModel.departName = Convert.ToString(row["Depart_Name"]);
            }
        }

        public void deleteSysUserInfoByUserId(string userId)
        {
            systemUserOrganicRelationshipIDAL.deleteSysUserInfoByUserId(userId);
        }
        /// <summary>
        /// 得到物料组名字
        /// </summary>
        /// <returns></returns>
        public DataTable getMtGroupName()
        {
            return systemUserOrganicRelationshipIDAL.getMtGroupName();
        }

        public string getUpUserNameById(string sysUser_ID)
        {
            return systemUserOrganicRelationshipIDAL.getUpUserNameById(sysUser_ID);
        }

        public string getMtGroupName(string sysUser_ID)
        {
            return systemUserOrganicRelationshipIDAL.getMtGroupName(sysUser_ID);
        }

        public string getEvalCodeByUserId(string sysUser_ID)
        {
            return systemUserOrganicRelationshipIDAL.getEvalCodeByUserId(sysUser_ID);
        }

        public string getPIDByUserId(string sysUser_ID)
        {
            return systemUserOrganicRelationshipIDAL.getPIDByUserId(sysUser_ID);
        }

        public string getMtMethodGroupName(string sysUser_ID)
        {
            return systemUserOrganicRelationshipIDAL.getMtMethodGroupName(sysUser_ID);
        }
    }
}
