﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.DataBackupRestoreManage
{
    public class BackupDatabaseBLL
    {
        DatabaseBackupRecordIDAL backupIDAL = DALFactoryHelper.CreateNewInstance<DatabaseBackupRecordIDAL>("SystemConfig.DatabaseBackupRecordDAL");

        /// <summary>
        /// 查询全部备份记录
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllBackupRecords()
        {
            return backupIDAL.queryAllBackupRecord();
        }

        /// <summary>
        /// 插入新的备份记录到数据库
        /// </summary>
        /// <param name="recordModel"></param>
        public void insertBackupRecord(DatabaseBackupRecordModel recordModel)
        {
            backupIDAL.insertBackupRecord(recordModel);
        }
    }
}
