﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.DataBackupRestoreManage
{
    public class RestoreDatabaseBLL
    {
        DatabaseRestoreRecordIDAL restoreIDAL = DALFactoryHelper.CreateNewInstance<DatabaseRestoreRecordIDAL>("SystemConfig.DatabaseRestoreRecordDAL");
        DatabaseBackupRecordIDAL backupIDAL = DALFactoryHelper.CreateNewInstance<DatabaseBackupRecordIDAL>("SystemConfig.DatabaseBackupRecordDAL");

        /// <summary>
        /// 查询所有的备份记录
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllBackupRecords()
        {
            return backupIDAL.queryAllBackupRecord();
        }

        /// <summary>
        /// 插入新的恢复记录到数据库
        /// </summary>
        /// <param name="recordModel"></param>
        public void insertRestoreRecord(DatabaseRestoreRecordModel recordModel)
        {
            restoreIDAL.insertRestoreRecord(recordModel);
        }


    }
}
