﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Bll.SystemConfig.PermissionManage;
using Lib.Model.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.IDAL.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.PermissionManage
{
    /// <summary>
    /// 功能级权限校验通用验证类
    /// </summary>
    public class FunctionLevelPermissionValidateBLL
    {
        private JoinUserFunctionPermissionObjectIDAL joinUserFunction = DALFactoryHelper.CreateNewInstance<JoinUserFunctionPermissionObjectIDAL>("SystemConfig.JoinUserFunctionPermissionObjectDAL");
        private JoinRoleFunctionPermissionObjectIDAL joinRoleFunction = DALFactoryHelper.CreateNewInstance<JoinRoleFunctionPermissionObjectIDAL>("SystemConfig.JoinRoleFunctionPermissionObjectDAL");
        private JoinGroupFunctionPermissionObjectIDAL joinGroupFunction = DALFactoryHelper.CreateNewInstance<JoinGroupFunctionPermissionObjectIDAL>("SystemConfig.JoinGroupFunctionPermissionObjectDAL");

        /// <summary>
        /// 点击菜单进入窗口前权限验证
        /// </summary>
        /// <param name="baseUserModel">当前用户Model</param>
        /// <param name="menuName">点击菜单name</param>
        /// <returns></returns>
        public bool canEnterWindow(BaseUserModel baseUserModel,String menuName)
        {
            BaseFunctionPermissionObjectModel basePermissionModel = new BaseFunctionPermissionObjectModel();
            basePermissionModel.Menu_Name = menuName;

            DataTable baseUserPermissionTable = null;
            DataTable joinRolePermissionTable = null;
            DataTable joinGroupPermissionTable = null;

            try
            {
                //查询用户直接权限项
                baseUserPermissionTable = joinUserFunction.queryUserFunctionByID(baseUserModel, basePermissionModel);
                //查询所在用户组权限项
                joinGroupPermissionTable = joinGroupFunction.queryPermissionFromGroup(baseUserModel, basePermissionModel);
                //查询所具有的角色的所有权限项
                joinRolePermissionTable = joinRoleFunction.queryPermissionFromRoleOfMenuName(baseUserModel, basePermissionModel);

                baseUserPermissionTable.Merge(joinGroupPermissionTable);
                baseUserPermissionTable.Merge(joinRolePermissionTable);

                //在该菜单对应的窗口中具有至少一项操作的权限，则允许进入窗口
                if (baseUserPermissionTable.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw new Exception("系统异常，验证权限失败！");
            }
        }

        /// <summary>
        /// 窗口内进行某操作前权限验证
        /// </summary>
        /// <param name="baseUserModel">当前用户Model</param>
        /// <param name="permissionId">权限对象ID</param>
        /// <returns></returns>
        public bool canOperateInWindow(BaseUserModel baseUserModel,int permissionId)
        {
            BaseFunctionPermissionObjectModel basePermissionModel = new BaseFunctionPermissionObjectModel();
            basePermissionModel.Permission_ID = permissionId;

            DataTable baseUserPermissionTable = null;
            DataTable joinRolePermissionTable = null;
            DataTable joinGroupPermissionTable = null;

            try
            {
                //查询用户是否具有直接授权
                baseUserPermissionTable = joinUserFunction.queryUserFunctionPermissionById(baseUserModel, permissionId);
                //查询用户所在的组的是否具有该权限
                joinGroupPermissionTable = joinGroupFunction.queryPermissionByIdFormGroup(baseUserModel, permissionId);
                //查询用户所具有的角色是否具有该权限
                joinRolePermissionTable = joinRoleFunction.queryPermissionFromRoleOfPermissionID(baseUserModel, basePermissionModel);

                baseUserPermissionTable.Merge(joinGroupPermissionTable);
                baseUserPermissionTable.Merge(joinRolePermissionTable);

                if(baseUserPermissionTable.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw new Exception("系统异常，验证权限失败！");
            }
        }

    }
}
