﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceItemMaterialBLL
    {
        SourceItemMaterialIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceItemMaterialIDAL>(
           "SourcingManage.SourcingManagement.SourceItemMaterialDAL");

        public int addSourceItemMaterial(SourceItemMaterial sim)
        {
            return idal.addSourceItemMaterial(sim);
        }

        #region 为创建合同服务
        public List<SourceItemMaterial> getSourceItemMaterialByFK(string contractID)
        {
            return idal.getSourceItemMaterialByFK(contractID);
        }
        #endregion
    }
}
