﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceSupplierBLL
    {
        SourceSupplierIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceSupplierIDAL>(
            "SourcingManage.SourcingManagement.SourceSupplierDAL");

        /// <summary>
        /// 添加寻源对应的供应商信息
        /// </summary>
        /// <param name="ss"></param>
        /// <returns></returns>
        public int addSourceSupplier(SourceSupplier ss)
        {
            return idal.addSourceSupplier(ss);
        }

        public List<SourceSupplier> findSuppliersBySId(string sourceId)
        {
            return idal.findSuppliersBySId(sourceId);
        }

        public List<SourceSupplier> getBidSupplier()
        {
            return idal.getBidSupplier();
        }

        public List<SourceSupplier> getBidSuppliersByBidId(string p)
        {
            return idal.getBidSuppliersByBidId(p);
        }

        public List<SourceSupplier> getAvailableSuppliersByBId(string bidId)
        {
            return idal.getAvailableSuppliersByBId(bidId);
        }

        public List<SourceSupplier> getBidsBySupplierId(string sid)
        {
            return idal.getBidsBySupplierId(sid);
        }

        public int updateTransState(string bid, string sid, int res)
        {
            return idal.updateTransState(bid,sid,res);
        }

        public SourceSupplier getSupplierByBidState(string bidId)
        {
            return idal.getSupplierByBidState(bidId);
        }

        public string countTaskNumberByUserId(string user_ID)
        {
            return idal.countTaskNumberByUserId(user_ID);
        }
    }
}
