﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidCertificateBLL
    {
        BidCertificateIDAL idal =
           DALFactoryHelper.CreateNewInstance<BidCertificateIDAL>(
           "SourcingManage.SourcingManagement.BidCertificateDAL");

        //添加招标凭证
        public int addBidCertificate(BidCertificate bc)
        {
            return idal.addBidCertificate(bc);
        }

        public BidCertificate getBidCertificateByBidId(string bidId)
        {
            return idal.getBidCertificateByBidId(bidId);
        }
    }
}
