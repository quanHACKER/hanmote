﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceBidBLL
    {

        SourceBidIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceBidIDAL>(
           "SourcingManage.SourcingManagement.SourceBidDAL");

        /// <summary>
        /// 添加招标
        /// </summary>
        /// <param name="bid"></param>
        public int addBid(SourceBid bid)
        {
            return idal.addBid(bid);
        }



        public List<SourceBid> getBids()
        {
           return  idal.getBids();
        }

        public SourceBid findBidByBidId(string bidId)
        {
            return idal.findBidByBidId(bidId);
        }

        public int updateBidState(SourceBid bid)
        {
            return idal.updateBidState(bid);
        }

        public List<SourceBid> getSourceBidsByState(int state)
        {
            return idal.getSourceBidsByState(state);
        }

        public int updateBidReleaseTime(string bidId)
        {
            return idal.updateBidReleaseTime(bidId);
        }
    }
}
