﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceSupplierMaterialBLL
    {
        SourceSupplierMaterialIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceSupplierMaterialIDAL>(
            "SourcingManage.SourcingManagement.SourceSupplierMaterialDAL");

        public int addSupplierMaterial(SourceSupplierMaterial ssm)
        {
            return idal.addSupplierMaterial(ssm);
        }

        public List<SourceSupplierMaterial> findSSMBySId(string sourceId)
        {
            return idal.findSSMBySId(sourceId);
        }

        public List<SourceSupplierMaterial> getSSMBySuIdMId(string bidId, string materialId)
        {
            return idal.getSSMBySuIdMId(bidId, materialId);
        }

        public List<SourceSupplierMaterial> getSSMBySIdSuId(string sourceId, string supplierId)
        {
            return idal.getSSMBySIdSuId(sourceId, supplierId);
        }

        public SourceSupplierMaterial getSSMBySIdSIdMId(string bidId, string supplierId, string materialId)
        {
            return idal.getSSMBySIdSIdMId(bidId, supplierId, materialId);
        }
    }
}
