﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceListBLL
    {
        SourceListIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceListIDAL>(
           "SourcingManage.SourcingManagement.SourceListDAL");

        //添加货源清单
        public int addSourceList(SourceList sl)
        {
            
            return idal.addSourceList(sl);
        }

        //根据物料编号获取货源清单
        public List<SourceList> getSourceList(string materialId,string factoryId)
        {
            return idal.getSourceList(materialId, factoryId);
        }


        public List<SourceList> getSourceList(string materialId, string factoryId,string startTime, string endTime)
        {
            return idal.getSourceList(materialId, factoryId,startTime,endTime);
        }

        public int updateSourceList(SourceList sourceList)
        {
            return idal.updateSourceList(sourceList);
        }

        public SourceList getSourceListBySId(string sid)
        {
            return idal.getSourceListBySId(sid);
        }

    }
}
