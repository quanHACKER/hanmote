﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidMaterialBLL
    {
        BidMaterialIDAL idal = DALFactoryHelper.CreateNewInstance<BidMaterialIDAL>("SourcingManage.SourcingManagement.BidMaterialDAL");

        //添加BidMaterial
        public int addBidMaterial(BidMaterial bm)
        {
            return idal.addBidMaterial(bm);
        }
    }
}
