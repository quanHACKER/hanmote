﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class Source_ListBLL
    {
        Source_ListDAL source_ListDAL = new Source_ListDAL();
        InquiryDAL inquiryDAL = new InquiryDAL();

        /// <summary>
        /// 保存新的询价单
        /// </summary>
        /// <param name="enquiry"></param>
        /// <returns></returns>
        public int addNewSource_List(List<Source_List> sourceList)
        {
            return source_ListDAL.addNewSource_List(sourceList);
        }

        /// <summary>
        /// 保存新的询价单表头
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewInquiry_Table(Inquiry_Table inquiry_Table)
        {
            return inquiryDAL.addNewInquiry_Table(inquiry_Table);
        }

        /// <summary>
        /// 查询单号是否存在
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public bool isInquiry_TableExist(string id)
        {
            return inquiryDAL.isInquiry_TableExist(id);
        }

        /// <summary>
        /// 更改表头信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateInquiry_Table(Inquiry_Table inquiry_Table)
        {
            return inquiryDAL.updateInquiry_Table(inquiry_Table);
        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public Inquiry_Table findInquiryTable(Dictionary<string, string> conditions, String table)
        {
            return source_ListDAL.findInquiryTable(conditions, table);
        }

        /// <summary>
        /// 根据询价单号查询已存在的询价单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Source_List> findSource(Dictionary<string, string> conditions)
        {
            return source_ListDAL.findSource(conditions);
        }

        /// <summary>
        /// 更改报价信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateEnquiryOffer(List<Enquiry> enquiryList, Inquiry_Table inquiry)
        {
            return source_ListDAL.updateEnquiryOffer(enquiryList, inquiry);
        }

        /// <summary>
        /// 查询报价单某状态数量
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int enquiryCount(Enquiry enquiry)
        {
            return source_ListDAL.enquiryCount(enquiry);
        }
    }
}
