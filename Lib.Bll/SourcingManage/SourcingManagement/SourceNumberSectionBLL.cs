﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceNumberSectionBLL
    {
        SourceNumberSectionIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceNumberSectionIDAL>(
            "SourcingManage.SourcingManagement.SourceNumberSectionDAL");

        public List<SourceNumberSection> getSouNumSecsBySouSupMaId(string sourceId,string supplierId,string materialId)
        {
            return idal.getSouNumSecsBySouSupMaId(sourceId, supplierId, materialId);
        }

    }
}
