﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidPartnerBLL
    {
        BidPartnerIDAL idal =
          DALFactoryHelper.CreateNewInstance<BidPartnerIDAL>(
          "SourcingManage.SourcingManagement.BidPartnerDAL");


        public int addBidPartner(BidPartner bp)
        {
            return idal.addBidPartner(bp);
        }

        public BidPartner findBidPartnerByBidId(string bidId)
        {
            return idal.findBidPartnerByBidId(bidId);
        }
    }
}
