﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
   public class BidWeightScoreBLL
    {
       BidWeightScoreIDAL idal =
          DALFactoryHelper.CreateNewInstance<BidWeightScoreIDAL>(
          "SourcingManage.SourcingManagement.BidWeightScoreDAL");

       public int addBidWeightScore(BidWeightScore bws)
       {
           return idal.addBidWeightScore(bws);
       }

       public List<BidWeightScore> findWeightScoresByBidId(string bidId)
       {
           return idal.findWeightScoresByBidId(bidId);
       }

    }
}
