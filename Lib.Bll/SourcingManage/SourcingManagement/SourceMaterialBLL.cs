﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceMaterialBLL
    {
        SourceMaterialIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceMaterialIDAL>(
            "SourcingManage.SourcingManagement.SourceMaterialDAL");

        /// <summary>
        /// 添加寻源包含的所有物料
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int addSourceMaterials(SourceMaterial sm)
        {
            return idal.addSourceMaterials(sm);
        }

        public List<SourceMaterial> findMaterialsBySId(string sourceId)
        {
            return idal.findMaterialsBySId(sourceId);
        }

        public SourceMaterial findSMBySIdMId(string sourceId,string materialId)
        {
            return idal.findSMBySIdMId(sourceId,materialId);
        }

        public List<SourceMaterial> findSourceMaterialsBySourceId(string p)
        {
            return idal.findSourceMaterialsBySourceId(p);
        }

        public List<SourceMaterial> findSourcesByMaterialId(string materialId)
        {
            return idal.findSourcesByMaterialId(materialId);
        }
    }
}
