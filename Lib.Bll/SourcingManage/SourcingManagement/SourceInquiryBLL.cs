﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceInquiryBLL
    {
        SourceInquiryIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceInquiryIDAL>(
           "SourcingManage.SourcingManagement.SourceInquiryDAL");

        public int addNewInquiry_Table(SourceInquiry inquiry_Table)
        {
            return idal.addNewInquiry_Table(inquiry_Table);
        }

        public int updateInquiryState(String inquiry_id, String state)
        {
            return idal.updateInquiryState(inquiry_id, state);
        }

        public List<SourceInquiry> findSourcesByState(int state)
        {
            return idal.findSourcesByState(state);
        }

        public SourceInquiry findSourceById(string sourceId)
        {
            return idal.findSourceById(sourceId);
        }

        public int updateSourceState(string sourceId, int newState)
        {
            return idal.updateSourceState(sourceId, newState);
        }
    }
}
