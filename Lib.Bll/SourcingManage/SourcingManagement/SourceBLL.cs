﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceBLL
    {
        SourceIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceIDAL>(
            "SourcingManage.SourcingManagement.SourceDAL");

        /// <summary>
        /// 添加寻源
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int addSource(Source source)
        {
            return idal.addSource(source);
        }

        /// <summary>
        /// 查找所有指定类型的寻源记录
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public List<Source> findAllSources(string item)
        {
            return idal.findAllSources(item);
        }

        /// <summary>
        /// 更改指定寻源编号的寻源单路径
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="newState"></param>
        /// <returns></returns>
        public int updateSourcePath(string sourceId, string path)
        {
            return idal.updateSourcePath(sourceId, path);
        }

        /// <summary>
        /// 根据寻源编号查找寻源单
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public Source findSourceById(string sourceId)
        {
            return idal.findSourceById(sourceId);
        }
    }
}
