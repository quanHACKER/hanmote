﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceTimeSectionBLL
    {
        SourceTimeSectionIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceTimeSectionIDAL>(
            "SourcingManage.SourcingManagement.SourceTimeSectionDAL");

        public List<SourceTimeSection>  getSouTmsBySouSupMaId(string sourceId,string supplierId,string materialId)
        {
            return idal.getSouTmsBySouSupMaId(sourceId, supplierId, materialId);
        }

    }
}
