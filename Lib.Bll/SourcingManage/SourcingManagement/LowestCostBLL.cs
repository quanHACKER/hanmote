﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class LowestCostBLL
    {
        LowestCostIDAL idal =
           DALFactoryHelper.CreateNewInstance<LowestCostIDAL>(
           "SourcingManage.SourcingManagement.LowestCostDAL");

        public int addLowestCost(LowestCost ls)
        {
            return idal.addLowestCost(ls);
        }

        public List<LowestCost> getAllLowestBySId(string sid)
        {
            return idal.getAllLowestBySId(sid);
        }
    }

}
