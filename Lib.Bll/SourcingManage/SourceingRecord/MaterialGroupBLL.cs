﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;


namespace Lib.Bll.SourcingManage.SourceingRecord
{
    public class MaterialGroupBLL
    {
        MaterialGroupIDAL idal =
           DALFactoryHelper.CreateNewInstance<MaterialGroupIDAL>(
           "SourcingManage.SourceingRecord.MaterialGroupDAL");

        public List<MaterialGroup> getAllMaterialGroup()
        {
            return idal.getAllMaterialGroup();
        }
    }
}
