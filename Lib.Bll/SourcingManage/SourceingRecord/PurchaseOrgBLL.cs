﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;


namespace Lib.Bll.SourcingManage.SourceingRecord
{
    public class PurchaseOrgBLL
    {
        PurchaseOrgIDAL idal =
           DALFactoryHelper.CreateNewInstance<PurchaseOrgIDAL>(
           "SourcingManage.SourceingRecord.PurchaseOrgDAL");

        public List<PurchaseOrg> getAllPurchaseOrg()
        {
            return idal.getAllPurchaseOrg();
        }
    }
}
