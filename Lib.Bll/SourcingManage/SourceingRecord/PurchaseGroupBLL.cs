﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;


namespace Lib.Bll.SourcingManage.SourceingRecord
{
    public class PurchaseGroupBLL
    {
        PurchaseGroupIDAL idal =
           DALFactoryHelper.CreateNewInstance<PurchaseGroupIDAL>(
           "SourcingManage.SourceingRecord.PurchaseGroupDAL");

        public List<PurchaseGroup> getAllPurchaseGroup()
        {
            return idal.getAllPurchaseGroup();
        }
    }
}
