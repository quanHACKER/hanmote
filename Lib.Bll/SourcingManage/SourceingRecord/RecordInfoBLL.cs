﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;

namespace Lib.Bll.SourcingManage.SourceingRecord
{
    public class RecordInfoBLL
    {
        RecordInfoIDAL idal =
           DALFactoryHelper.CreateNewInstance<RecordInfoIDAL>(
           "SourcingManage.SourceingRecord.RecordInfoDAL");

        public int addRecordInfo(RecordInfo ri)
        {
            return idal.addRecordInfo(ri);
        }

        public List<RecordInfo> getAllRecordInfos()
        {
            return idal.getAllRecordInfos();
        }


        public List<RecordInfo> getRecordsByMaterialId(string materialId)
        {
            return idal.getRecordsByMaterialId(materialId);
        }
    }
}
