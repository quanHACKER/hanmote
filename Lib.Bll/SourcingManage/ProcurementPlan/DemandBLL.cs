﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.ProcurementPlan
{
    public class DemandBLL
    {
        DemandIDAL demandDAL =
            DALFactoryHelper.CreateNewInstance<DemandIDAL>(
            "SourcingManage.ProcurementPlan.Summary_DemandDAL");


        /// <summary>
        /// 添加新的采购需求
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewStandard(Summary_Demand summary_Demand)
        {
            return demandDAL.addNewStandard(summary_Demand);
        }

        /// <summary>
        /// 修改标准需求计划
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int editNewStandard(Summary_Demand summary_Demand)
        {
            return demandDAL.editNewStandard(summary_Demand);
        }

        /// <summary>
        /// 查询是否已存在相同的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public Boolean SameStandardByDemand_ID(string demand_ID)
        {
            return demandDAL.SameStandardByDemand_ID(demand_ID);
        }

        /// <summary>
        /// 查询已存在的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID(String itemName)
        {
            return demandDAL.FindStandardDemand_ID(itemName);
        }

        /// <summary>
        /// 查询最新的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID()
        {
            return demandDAL.FindStandardDemand_ID();
        }

        /// <summary>
        /// 查询需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable StandardByWLBH(String item1, String item2, String item3)
        {
            return demandDAL.StandardByWLBH(item1, item2, item3);
        }

        /// <summary>
        /// 更改采购单号状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changeState(String state, String demand_ID)
        {
            return demandDAL.changeState(state, demand_ID);
        }

        /// <summary>
        /// 更改采购单号状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changePrice(String price, String demand_ID)
        {
            return demandDAL.changePrice(price, demand_ID);
        }

        /// <summary>
        /// 查询物料编号对应的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterial(String itemName, String table)
        {
            return demandDAL.FindMaterial(itemName, table);
        }

        /// <summary>
        /// 查询已存在的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterialItems(String itemName, String table)
        {
            return demandDAL.FindMaterialItems(itemName, table);
        }
        public List<String> FindAddItems(String id, String idName, String itemName, String table)
        {
            return demandDAL.FindAddItems(id, idName, itemName, table);
        }

        /// <summary>
        /// 根据需要加载items至combobox
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable AddItemsToCombobox(String itemName, String table)
        {
            return demandDAL.AddItemsToCombobox(itemName, table);
        }

        /// <summary>
        /// 新建条件类型
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewPrice_Condition(String table,String id, String Condition, String Price_Level, String Rounding_Rule, Boolean Sign, float Value_Rate)
        {
            return demandDAL.addNewPrice_Condition(table,id,Condition,Price_Level,Rounding_Rule,Sign,Value_Rate);
        }
    }
}
