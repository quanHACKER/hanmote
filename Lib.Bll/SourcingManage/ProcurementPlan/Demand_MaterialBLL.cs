﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;

namespace Lib.Bll.SourcingManage.ProcurementPlan
{
    public class Demand_MaterialBLL
    {
        Demand_MaterialIDAL idal =
            DALFactoryHelper.CreateNewInstance<Demand_MaterialIDAL>(
            "SourcingManage.ProcurementPlan.Demand_MaterialDAL");

        /// <summary>
        /// 根据需求单号，物料编号查找需求-物料记录
        /// </summary>
        /// <param name="demandId"></param>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public Demand_Material findDemandMaterial(string demandId, string materialId)
        {
            return idal.findDemandMaterial(demandId,materialId);
        }

        /// <summary>
        /// 添加需求-物料关系
        /// </summary>
        /// <param name="demandMaterial"></param>
        /// <returns></returns>
        public int addSummaryMaterials(Demand_Material demandMaterial)
        {
            return idal.addSummaryMaterials(demandMaterial);
        }

        /// <summary>
        /// 根据需求单号，查找该需求单号包含的物料
        /// </summary>
        /// <param name="demandid"></param>
        /// <returns></returns>
        public List<Demand_Material> findDemandMaterials(string demandid)
        {
            return idal.findDemandMaterials(demandid);
        }

        /// <summary>
        /// 根据需求单号和物料编号删除物料
        /// </summary>
        /// <returns></returns>
        public int deleteDemandMaterial(string demandId,string materialId)
        {
            return idal.deleteDemandMaterial(demandId, materialId);
        }

        /// <summary>
        /// 查找还未汇总的需求单号
        /// </summary>
        /// <returns></returns>
        public List<Demand_Material> findDemandMaterialNonAggregate(List<string> demandIDs)
        {
            return idal.findDemandMaterialNonAggregate(demandIDs);
        }

        /// <summary>
        /// 更新需求-物料的汇总编号
        /// </summary>
        /// <param name="am"></param>
        /// <returns></returns>
        public int updateDemandMaterialAggregateID(Aggregate_Material am,List<string> demandIDs)
        {
            return idal.updateDemandMaterialAggregateID(am,demandIDs);
        }

        public int updateSummaryMaterials(Demand_Material demand_material,string materialId)
        {
            return idal.updateSummaryMaterials(demand_material,materialId);
        }
    }
}
