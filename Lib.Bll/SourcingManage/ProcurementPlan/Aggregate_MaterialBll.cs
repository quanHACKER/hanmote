﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;

namespace Lib.Bll.SourcingManage.ProcurementPlan
{
    public class Aggregate_MaterialBLL
    {
        Aggregate_MaterialIDAL idal =
            DALFactoryHelper.CreateNewInstance<Aggregate_MaterialIDAL>(
            "SourcingManage.ProcurementPlan.Aggregate_MaterialDAL");

        /// <summary>
        /// 插入汇总记录
        /// </summary>
        /// <param name="am"></param>
        /// <returns></returns>
        public int addAggregateMaterial(Aggregate_Material am)
        {
            return idal.addAggregateMaterial(am);
        }

        /// <summary>
        /// 获取所有汇总信息
        /// </summary>
        /// <returns></returns>
        public List<Aggregate_Material> getAllAggregates()
        {
            return idal.getAllAggregates();
        }
       
    }
}

