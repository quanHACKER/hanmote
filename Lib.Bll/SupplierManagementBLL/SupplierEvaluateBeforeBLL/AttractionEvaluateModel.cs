﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using Lib.Common;

namespace Lib.Bll
{
    /// <summary>
    /// 供应商吸引力评价模型
    /// </summary>
    public class AttractionEvaluateModel
    {
        #region   计算总分
        /// <summary>
        /// 计算总得分
        /// </summary>
        /// <param name="list">各项指标的分数</param>
        /// <returns>总得分</returns>
        public static double calculateTotalScore(List<double> list)
        { 
            double result = 0.0;
            if (list == null || list.Count == 0)
            {
                throw new ArgumentNullException();
            }
            foreach (double d in list)
            {
                result += d;
            }
            return result;
        }
        #endregion

        #region  映射总得分和PIP等级

        /// <summary>
        /// 通过得分获取PIP等级
        /// </summary>
        /// <param name="value">总得分</param>
        /// <returns>PIP等级</returns>
        public static string mappingTotalScorePIP(double value)
        {
            string[] level = PIPLevel.pipMap;

            double[] nodes = {1,2,3,4};
            int maxLevel = level.Length -1;
            int curlevel = 0;

            while(curlevel <= maxLevel)
            {
                if (value < nodes[curlevel])
                    return level[curlevel];
                else
                    curlevel++;
            }

            return level[maxLevel];


        }
        #endregion 

        #region  感知模型
        /// <summary>
        /// 通过吸引力水平和业务机制来得到感知类型
        /// </summary>
        /// <param name="index1">吸引力水平</param>
        /// <param name="index2">业务价值</param>
        /// <returns>感知类型</returns>
        public static string mappingApperceiveType(string index1,string index2)
        {

            if (index1.Equals(PIPLevel.High))
            {
                if (index2.Equals(PIPLevel.High))
                {
                    return ApperceiveType.CORE;
                }
                else if (index2.Equals(PIPLevel.Middle))
                {
                    return ApperceiveType.CORE;
                }
                else if (index2.Equals(PIPLevel.Low))
                {
                    return ApperceiveType.DEVELOPMENT;
                }
                else
                {
                    return ApperceiveType.DEVELOPMENT;
                }
            }
            else if (index1.Equals(PIPLevel.Middle))
            {
                if (index2.Equals(PIPLevel.High))
                {
                    return ApperceiveType.CORE;
                }
                else if (index2.Equals(PIPLevel.Middle))
                {
                    return ApperceiveType.CORE;
                }
                else if (index2.Equals(PIPLevel.Low))
                {
                    return ApperceiveType.DEVELOPMENT;
                }
                else
                {
                    return ApperceiveType.DEVELOPMENT;
                }
            }
            else if (index1.Equals(PIPLevel.Low))
            {
                if (index2.Equals(PIPLevel.High))
                {
                    return ApperceiveType.EXPLOIT;
                }
                else if (index2.Equals(PIPLevel.Middle))
                {
                    return ApperceiveType.EXPLOIT;
                }
                else if (index2.Equals(PIPLevel.Low))
                {
                    return ApperceiveType.EDGE;
                }
                else
                {
                    return ApperceiveType.EDGE;
                }
            }
            else
            {
                if (index2.Equals(PIPLevel.High))
                {
                    return ApperceiveType.EXPLOIT;
                }
                else if (index2.Equals(PIPLevel.Middle))
                {
                    return ApperceiveType.EXPLOIT;
                }
                else if (index2.Equals(PIPLevel.Low))
                {
                    return ApperceiveType.EDGE;
                }
                else
                {
                    return ApperceiveType.EDGE;
                }
            }
        }
        #endregion 
    }
}
