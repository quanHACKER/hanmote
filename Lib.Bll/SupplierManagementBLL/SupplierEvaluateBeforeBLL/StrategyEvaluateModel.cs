﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;

namespace Lib.Bll
{
    /// <summary>
    /// 供应商策略模型类
    /// </summary>
    public class StrategyEvaluateModel
    {
        #region  供应商策略对应关系
        /// <summary>
        /// 通过物料品项和感知类型确定供应商策略类型
        /// </summary>
        /// <param name="index1">感知类型</param>
        /// <param name="index2">物料品项</param>
        /// <returns>供应商的策略类型</returns>
        public static string mappingStrategyType(string index1,string index2)
        {
            //核心
            if (index1.Equals(ApperceiveType.CORE))
            {
                if (index2.Equals(MaterialItemType.KEY))
                {
                    return StrategyType.STRATEGIC_COOPERATION;
                }
                else if (index2.Equals(MaterialItemType.BOTTLENECK))
                {
                    return StrategyType.COOPERATION;
                }
                else if(index2.Equals(MaterialItemType.LEVER))
                {
                    return StrategyType.COOPERATION;
                }
                else
                {
                    return StrategyType.TRANSACTION;
                }
            }//发展
            else if (index1.Equals(ApperceiveType.DEVELOPMENT))
            {
                if (index2.Equals(MaterialItemType.KEY))
                {
                    return StrategyType.COOPERATION;
                }
                else if (index2.Equals(MaterialItemType.BOTTLENECK))
                {
                    return StrategyType.COOPERATION;
                }
                else if (index2.Equals(MaterialItemType.LEVER))
                {
                    return StrategyType.TRANSACTION;
                }
                else
                {
                    return StrategyType.TRANSACTION;
                }
            }//盘剥
            else if (index1.Equals(ApperceiveType.EXPLOIT))
            {
                if(index2.Equals(MaterialItemType.KEY))
                {
                    return StrategyType.COOPERATION;
                }
                else if (index2.Equals(MaterialItemType.BOTTLENECK))
                {
                    return StrategyType.TRANSACTION;
                }
                else if (index2.Equals(MaterialItemType.LEVER))
                {
                    return StrategyType.TRANSACTION;
                }
                else
                {
                    return StrategyType.TRANSACTION;
                }
            }//边缘
            else
            {
                return StrategyType.TRANSACTION;
            }
        }
        #endregion  

     

    }
}
