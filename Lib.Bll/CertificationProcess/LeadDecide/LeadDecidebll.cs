﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlServerDAL.CertificationProcess;
using System.Windows.Forms;

namespace Lib.Bll.CertificationProcess.LeadDecide
{
    public class LeadDecidebll
    {
        LeaderDecide leader = new LeaderDecide();
       public  DataTable getDecideSuppInfo(string ID) {
           
            return leader.GetSupplier(ID); 

        }

        public DataTable SelectAgree(string SupplierId) {

            return leader.GetAgree(SupplierId);
        }

        public DataTable QueryConditionbll(string purchase, string thingGroup, string status, string country, string name,int flag,string ID)
        {
           return leader.QueryCondition(purchase, thingGroup, status, country, name,flag,ID);
        }

        public DataTable selectDecideStaus(string supplierId)
        {
            return leader.selectDecideStaus(supplierId);
        }

        public DataTable SelectById(string id)
        {
            return leader.SelectSuppById(id);
        }

        public DataTable getScore(string id,string flag)
        {
            return leader.getScore(id, flag);
        }

        public int  refuseSupplier(string supplierId, string user_ID,string suggession)
        {
             return leader.refuseSupplier(supplierId, user_ID, suggession);
        }

        /// <summary>
        /// 插入领导决策意见
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="Agree"></param>
        /// <returns></returns>
        public int InsertAgreeInfo(String SupplierId,int Agree) {

            return leader.InsertagreeInfo(SupplierId, Agree);
        }
        /// <summary>
        /// 选择已经准入的供应商
        /// </summary>
        /// <returns></returns>
        public DataTable SelectPermission(string ID,int pageSize,int pageIndex,out int total)
        {

            return leader.SelectPerimissionedSup(ID, pageSize,pageIndex,out total);
        }
        //查询主审意见
        public DataTable queryMainAdvice(int tagFour,string supplierID)
        {
            return leader.queryMainAdvice(tagFour,supplierID);
        }
        //查询审批过程
        public DataTable getDecideInfoBySupplierIdAndUserId(string user_ID, string SupplierId)
        {
            return leader.getDecideInfoBySupplierIdAndUserId(user_ID, SupplierId);
        }

        public DataTable getCGreenValueAndGLowValue()
        {
           return  leader.getCGreenValueAndGLowValue();
        }

        public void updateLevelInfo(string level,string supplierId)
        {
            leader.updateLevelInfo(level,supplierId);
        }

        public DataTable getSuggesstion(string user_ID, string supplierID)
        {
            return leader.getSuggesstion(user_ID, supplierID);
        }

        public DataTable getIsAgree(string user_ID, string supplierID)
        {
           return leader.getIsAgree(user_ID, supplierID);
        }

        //插入上层领导决策信息
        public int insertDecideInfo(string supplierId, string suggession, string user_ID, int step, int statuEnd,string isAgree)
        {
            return leader.insertDecideInfo(supplierId,suggession, user_ID,step,statuEnd, isAgree);
        }

        //插入终审信息
        public int insertDecideInfoSubmitEnd(string user_ID,int backTag,string IsAgree,string supplierId, string suggession)
        {
            return leader.insertDecideInfoSubmitEnd(user_ID, backTag,IsAgree,supplierId, suggession);
        }
        //反馈操作
        public int upDataDecideInfo(string user_ID, int backTag, string isAgree,string supplierId)
        {
            return leader.insertDecideInfoSubmitEnd(user_ID, backTag, isAgree, supplierId,"");
        }

        /// <summary>
        /// 更新SR_Info
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="user_ID"></param>
        /// <param name="isAgree"></param>
        /// <returns></returns>
        public int upDataSupplierTag(string supplierId, string user_ID, string isAgree)
        {
            return leader.upDataSupplierTag(supplierId, user_ID, isAgree);
        }

        public void updateConditionToPermission(string supplierID)
        {
            leader.updateConditionToPermission(supplierID);
        }
    }
}
