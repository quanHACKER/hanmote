﻿using DALFactory;
using Lib.IDAL.CertificationProcess.LocationalEvaluation;
using Lib.Model.CertificationProcess.LocationEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.CertificationProcess.LocationalEvaluation
{
    /// <summary>
    /// author：hezl
    /// </summary>
    public class LocationalEvaluationBLL
    {
        LocationalEvaluationIDAL locationalEvaluationIDAL = DALFactoryHelper.CreateNewInstance<LocationalEvaluationIDAL>("CertificationProcess.LocationEvaluation.LocationalEvaluationDAL");

        /// <summary>
        /// 得到所有信息
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllLocalCompanyList(string userId,int pageNum, int frontNum,out int totalSize)
        {
            return locationalEvaluationIDAL.findAllCompanyList(userId,pageNum, frontNum,out totalSize);
        }

        public void setSupplierLevelValueABySupplierID(string companyID)
        {
            locationalEvaluationIDAL.setSupplierLevelValueABySupplierID(companyID);
        }

        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="locationalEvaluationModel"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findCompanyListByCondition(string userId, LocationalEvaluationSupperModel locationalEvaluationModel, int pageNum, int frontNum)
        {
            return locationalEvaluationIDAL.findAllCompanyListByCondition(userId,locationalEvaluationModel, pageNum, frontNum); 
        }
        /// <summary>
        /// 总数
        /// </summar>
        /// <returns></returns>
        public int calculateUserNumber()
        {
            DataTable userInforTable = locationalEvaluationIDAL.calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }
        /// <summary>
        /// 得到需要改善的供应商
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public DataTable findAllImprovementReportSupplier(string user_ID, int pageSize, int pageIndex,out int pageCount)
        {
            return locationalEvaluationIDAL.findAllImprovementReportSupplier(user_ID, pageSize, pageIndex,out pageCount);
        }

        public DataTable findAllCertificatedLocalCompanyList(String userId,int pageSize, int v)
        {
            return locationalEvaluationIDAL.findAllCertificatedLocalCompanyList(userId,pageSize, v);
        }
        /// <summary>
        /// insertHanmoteDecisionTable
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        public void insertHanmoteDecisionTable(string id, string companyID, string v, string suggestion)
        {
            locationalEvaluationIDAL.insertHanmoteDecisionTable(id,companyID,v,suggestion);
        }

        public void updateTagSuperSRInfoByCompanyID(string companyID)
        {
            locationalEvaluationIDAL.updateTagSuperSRInfoByCompanyID(companyID);
        }
        /// <summary>
        /// 条件查找 改善供应商
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <param name="nationalName"></param>
        /// <param name="companyNam"></param>
        /// <returns></returns>
        public DataTable findAllImprovementReportSupplierByCondition(string user_ID, int pageSize, int v, string nationalName, string companyNam,string isProvement)
        {
            return locationalEvaluationIDAL.findAllImprovementReportSupplierByCondition(user_ID,pageSize,v,nationalName,companyNam,isProvement);
        }

        public void updateTagRefuseSRInfoByCompanyID(string iD)
        {
            locationalEvaluationIDAL.updateTagRefuseSRInfoByCompanyID(iD);
        }

        /// <summary>
        /// findSimplePersonEvalListByCondition
        /// </summary>
        /// <param name="searchCondition"></param>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public DataTable findSimplePersonEvalListByCondition(string userId,LocationalEvaluationSupperModel searchCondition, int pageSize, int v)
        {
            return locationalEvaluationIDAL.findSimplePersonEvalListByCondition(userId,searchCondition, pageSize, v);
        }

        public DataTable findCompanyCertificatedListByCondition(string userId,LocationalEvaluationSupperModel searchCondition, int pageSize, int v)
        {
            return locationalEvaluationIDAL.findCompanyCertificatedListByCondition(userId,searchCondition);
        }

        public string getisSubmitImprovementReport(string companyID)
        {
            return locationalEvaluationIDAL.getISSubmitImprovementReportBySupplierId(companyID);
        }

        public string getPidByCurUserID(string user_ID)
        {
            return locationalEvaluationIDAL.getPidByCurUserID(user_ID);
        }
    }
}
