﻿using DALFactory;
using Lib.IDAL.MDIDAL.GoalValueAndLowValue;
using Lib.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.MDBll
{
    public class GoalValueAndLowValueBll
    {
        private GoalValueAndLowValueModel gl = new GoalValueAndLowValueModel();
        private static readonly GoalValueAndLowValueIDAL idal = DALFactoryHelper.CreateNewInstance<GoalValueAndLowValueIDAL>("MDDAL.GoalValueAndLowValue.GoalValueAndLowValueDAL");
        /// <summary>
        /// 获取目标值和最低值
        /// </summary>
        /// <returns></returns>
       public  GoalValueAndLowValueModel getGoalsAndVlaue() {

            DataTable dt = idal.getGoalAndLow();
            gl.CGreenValue = dt.Rows[0]["CGoalValue"].ToString();
            gl.CLowValue = dt.Rows[0]["CLowValue"].ToString();
            gl.PGreenValue = dt.Rows[0]["PGoalValue"].ToString();
            gl.PLowValue = dt.Rows[0]["LowValue"].ToString();
            return gl;
        }
        /// <summary>
        /// 新建目标值和最低值
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool newGoalsAndLowValue(GoalValueAndLowValueModel model ) {
            return idal.newGoalAndLowValue(model);
        }



    }
}
