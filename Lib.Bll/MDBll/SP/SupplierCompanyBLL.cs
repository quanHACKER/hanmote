﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using DALFactory;
using Lib.Model.MD.SP;

namespace Lib.Bll.MDBll.SP
{
   public class SupplierCompanyBLL
    {
       private static readonly SupplierCompanyIDAL idal = DALFactoryHelper.CreateNewInstance<SupplierCompanyIDAL>("MDDAL.SP.SupplierCompanyDAL");
       /// <summary>
       /// 获取供应商的公司代码级别数据
       /// </summary>
       /// <param name="supplierid"></param>
       /// <param name="companyid"></param>
       /// <returns></returns>
     public  SupplierCompanyCode GetSupplierCompanyInformation(string supplierid, string companyid)
       {
           return idal.GetSupplierCompanyInformation(supplierid, companyid); 
       }
       /// <summary>
       /// 更新供应商的公司代码级别数据
       /// </summary>
       /// <param name="spcpy"></param>
       /// <returns></returns>
      public bool UpdateSupplierCompanyInformation(SupplierCompanyCode spcpy)
       {
           return idal.UpdateSupplierCompanyInformation(spcpy);
       }
    }
}
