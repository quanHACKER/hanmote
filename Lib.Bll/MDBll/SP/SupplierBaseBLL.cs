﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using DALFactory;
using Lib.Model.MD.SP;
using System.Data;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace Lib.Bll.MDBll.SP
{
   public class SupplierBaseBLL
    {
        private static readonly SupplierBaseIDAL idal = DALFactoryHelper.CreateNewInstance<SupplierBaseIDAL>("MDDAL.SP.SupplierBaseDAL");
       /// <summary>
       /// 通过供应商编码获取供应商无组织机构级别数据
       /// </summary>
       /// <param name="SupplierID"></param>
       /// <returns></returns>
      public  SupplierBase GetSupplierBasicInformation(string SupplierID)
        {
            return idal.GetSupplierBasicInformation(SupplierID);
        }
       /// <summary>
       /// 更新供应商无组织机构级别数据
       /// </summary>
       /// <param name="spb"></param>
       /// <returns></returns>
      public  bool UpdateSupplierBasicInformation(SupplierBase spb)
        {
            try
            {
                return idal.UpdateSupplierBasicInformation(spb);
            }
            catch (DBException ex)
            {
                throw new BllException("更新数据失败！请稍后再试", ex);
            }
            
        }

        /// <summary>
        /// 分页查询供应商基础信息数据
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public DataTable FindSupplierInforByPage(int pageSize , int pageIndex)
        {
            try
            {
                return idal.GetSupplierInforByPage(pageSize, pageIndex);
            }
            catch(DBException ex)
            {
                throw new BllException("分页查询供应商数据出现异常！",ex);
            }
        }

        /// <summary>
        /// 查询表中的总记录数
        /// </summary>
        /// <returns></returns>
        public int TotalCount()
        {
            DataTable dt = idal.GetTotalCount();
            String totalCount = dt.Rows[0][0].ToString();
            return Convert.ToInt32(totalCount);
        }

      /// <summary>
      /// 获取所有供应商编码
      /// </summary>
      /// <returns></returns>
      public List<string> GetAllSupplierID()
      {
          return idal.GetAllSupplierID();
      }
       /// <summary>
       /// 获取所有供应商基本信息
       /// </summary>
       /// <returns></returns>
      public DataTable GetAllSupplierBasicInformation()
      {
          return idal.GetAllSupplierBasicInformation();
      }
	  //获取所有供应商信息
	  public List<SupplierBase> GetAllSupplierBase()
      {
          return idal.GetAllSupplierBase();
      }
	  
      public List<SupplierBase> getSupplierBaseByMG(string materialGroup)
      {
          return idal.getSupplierBaseByMG(materialGroup);
      }

    }
}
