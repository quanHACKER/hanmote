﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using DALFactory;
using System.Data;
namespace Lib.Bll
{
   public class BatchFeatureBLL
    {
       private static readonly BatchFeatureIDAL idal = DALFactoryHelper.CreateNewInstance<BatchFeatureIDAL>("MDDAL.Batch.BatchDAL");
       public List<string> GetAllFixedFeatures()
       {
           return idal.GetAllFixedFeatures();
       }
       public bool InsertBatchFeatures(string BatchID, string name, string value)
       {
           return idal.InsertBatchFeatures(BatchID, name, value);
       }
       public List<string> GetAllValue(string name)
       {
           return idal.GetAllValue(name);
       }
       public DataTable GetFeaturesByID(string ID)
       {
           return idal.GetFeaturesByID(ID);
       }
       /// <summary>
       /// 批次特性是否重复
       /// </summary>
       /// <param name="row"></param>
       /// <returns></returns>
       public bool RepeatedBatchFeatureInformation(string batchid, string featurename)
       {
           return idal.RepeatedBatchFeatureInformation(batchid,featurename);
       }
       /// <summary>
       /// 判断批次号是否重复
       /// </summary>
       /// <param name="BatchID"></param>
       /// <returns></returns>
      public bool RepeatedBatch(string BatchID)
      {
          return idal.RepeatedBatch(BatchID);
      }
    }
}
