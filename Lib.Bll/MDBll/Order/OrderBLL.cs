﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.Order;
using Lib.Model.MD.Order;

namespace Lib.Bll.MDBll.Order
{
   public class OrderBLL
    {
        OrderIDAL idal = DALFactory.DALFactoryHelper.CreateNewInstance<OrderIDAL>("Order.OrderDAL");
       /// <summary>
       /// orderinfo
       /// </summary>
       /// <param name="odif"></param>
       /// <param name="list"></param>
       /// <returns></returns>
       public bool InsertOrderInfo(OrderInfo odif, List<OrderItem> list)
        {
            return idal.InsertOrderInfo(odif, list);
        }
       public List<PR_Supplier> GetAllPrSupplierRelation(bool finishedmark)
       {
           return idal.GetAllPrSupplierRelation(finishedmark);
       }
       public List<InfoRecord> GetInforecordByMaterilSuplier(List<MaterialSupplier> list)
       {
           return idal.GetInforecordByMaterilSuplier(list);
       }
    }
}
