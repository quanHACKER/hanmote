﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Lib.Bll.MDBll.General
{
   public class CmbAutoSize
    {
        /// <summary>
        /// 报表下拉框自适应
        /// </summary>
        /// <param name="cmb">下拉框</param>
        /// <param name="form">包含下拉框的窗口，一般传“this”即可 </param>
        public void CbbAutoSize(ComboBox cmb, Form form)
        {
            int maxSize = 0;
            int ext = 0; //滚动条宽度
            if (cmb.Items.Count > cmb.MaxDropDownItems)
            {
                ext = 16;
            }
            System.Drawing.Graphics g = form.CreateGraphics();
            for (int i = 0; i < cmb.Items.Count; i++)
            {
                cmb.SelectedIndex = i;
                SizeF size = g.MeasureString(cmb.Text, cmb.Font);
                if (maxSize < (int)size.Width)
                {
                    maxSize = (int)size.Width + ext;
                }
            }
            cmb.DropDownWidth = cmb.Width;
            if (cmb.DropDownWidth < maxSize)
            {
                cmb.DropDownWidth = maxSize;
            }
            cmb.SelectedIndex = -1;
        }
    }
}
