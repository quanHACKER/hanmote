﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using Lib.IDAL.MDIDAL.Gerneral.Material_Type;

namespace Lib.Bll.MDBll.General.Material_Type
{
   public class MTTypeCBLL
    {
        private static readonly MTTypeCIDAL idal = DALFactoryHelper.CreateNewInstance<MTTypeCIDAL>("MDDAL.General.Material_Type.MTTypeCDAL");
        //获取所有物料类型
      public  DataTable GetAllLittleClassfy()
        {
            return idal.GetAllLittleClassfy();
        }
         //获取所有小分类名称
      public List<string> GetAllBigClassfyName()
        {
            return idal.GetAllBigClassfyName();
        }
        //新建物料类型信息
      public bool NewLittleClassfy(string lID, string lname, string bname)
        {
            return idal.NewLittleClassfy(lID, lname, bname);
        }
        //删除选定物料类型
      public bool DeleteLittleClassfy(string ID)
        {
            return idal.DeleteLittleClassfy(ID);
        }
      public List<string> GetAllTypeName()
      {
          return idal.GetAllTypeName();
      }
    }
    }

