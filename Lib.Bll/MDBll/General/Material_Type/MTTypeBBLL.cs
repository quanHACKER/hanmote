﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.Gerneral.Material_Type;
using DALFactory;
using System.Data;

namespace Lib.Bll.MDBll.General.Material_Type
{
   public class MTTypeBBLL
    {
       private static readonly MTTypeBIDAL idal = DALFactoryHelper.CreateNewInstance<MTTypeBIDAL>("MDDAL.General.Material_Type.MTTypeBDAL");
        /// <summary>
       /// 获取所有二级分类
        /// </summary>
        /// <returns></returns>

      public  DataTable GetAllLittleClassfy()
        {
            return idal.GetAllLittleClassfy();
        }
         /// <summary>
      /// 获取所有一级分类名称
         /// </summary>
         /// <returns></returns>
      public List<string> GetAllBigClassfyName()
        {
            return idal.GetAllBigClassfyName();
        }
        /// <summary>
      /// 新建二级分类信息
        /// </summary>
        /// <param name="lID"></param>
        /// <param name="lname"></param>
        /// <param name="bname"></param>
        /// <returns></returns>
      public bool NewLittleClassfy(string lID, string lname, string bname)
        {
            return idal.NewLittleClassfy(lID, lname, bname);
        }
        /// <summary>
      /// 删除选定二级分类
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
      public bool DeleteLittleClassfy(string ID)
        {
            return idal.DeleteLittleClassfy(ID);
        }
       /// <summary>
      /// 通过一级分类获取所属二级分类名称
       /// </summary>
       /// <param name="name"></param>
       /// <returns></returns>
     public List<string> GetLittleClassfyByBig(string name)
      {
          return idal.GetLittleClassfyByBig(name);
      }

        public List<string> GetAllMtGroupName()
        {
            return idal.GetAllMtGroupName();
        }

        public List<string> GetAllfiled()
        {
            return idal.GetAllfiled();
        }
    }
}
