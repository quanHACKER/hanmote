﻿using DALFactory;
using Lib.IDAL.MDIDAL.MT;
using Lib.SqlServerDAL.MDDAL.CertiFileDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.MDBll.CertiFileBll
{
    public class CertiFileBll
    {
        CertiFileTypeInfoIDAL idal = DALFactoryHelper.CreateNewInstance<CertiFileTypeInfoIDAL>("MDDAL.CertiFileDAL.CertiFileDAL");
        public DataTable getCertiFileTypeInfo()
        {

            return idal.GetCertiFileInfo();

        }

        public int delCertiFileTypeInfo(string id)
        {
            return idal.GetCertiFileInfo(id);
        }
    }
}
