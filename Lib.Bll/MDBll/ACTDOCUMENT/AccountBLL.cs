﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using DALFactory;
using System.Data;

namespace Lib.Bll
{
    public class AccountBLL
    {
        private static readonly AccumentIDAL idal = DALFactoryHelper.CreateNewInstance<AccumentIDAL>("AccountDAL");
        public List<string> GetCompanyCode()
        {
            return idal.GetCompanyCode();
        }
        public List<string> GetCurrencyType()
        {
            return idal.GetCurrencyType();
        }
        public List<string> GetEvaluationClass()
        {
            return idal.GetEvaluationClass();
        }
        public List<string> GetAllSupplier()
        {
            return idal.GetAllSupplier();
        }
        public string GetSuppplierDescription(string SupplierID)
        {
            return idal.GetSuppplierDescription(SupplierID);
        }
        public List<string> GetTaxCode()
        {
            return idal.GetTaxCode();
            }
        public string GetTaxDescription(string TaxID)
        {
            return idal.GetTaxDescription(TaxID);
        }
        public List<string> GetAllFactory()
        {
            return idal.GetAllFactory();
        }
        public List<string> GetAllStock(string FactoryID)
        {
            return idal.GetAllStock(FactoryID);
        }
        public List<string> GetAllAccount()
        {
            return idal.GetAllAccount();
        }
        //判断借方总计和贷方总计是否相等
        public bool IsEqual(string  a, string b)
        {
            if (a == b)
                return true;
            else
                return false;
        }
       public bool DebitRawMaterial(DateTime time,string number,float dmoney,float cmoney,string factory,string stock,string type)
        {

            return idal.DebitRawMaterial(time, number, dmoney, cmoney, factory, stock, type);

        }
       public bool DebitServiceClass(DateTime time, string number, float money, string type)
       {
           return idal.DebitServiceClass(time, number, money, type);
       }
       public bool DebitFinishedProduct(DateTime time, string number, float money, string factory, string stock, string type)
       {
           return idal.DebitFinishedProduct(time, number, money, factory, stock, type);

       }
      //public bool CreditRawMaterial(DateTime time, string number, float money, string factory, string stock, string type)
      // {
      //     return idal.CreditRawMaterial(time, time, number, money, factory, stock, type);
      // }
       public bool CreditAccountsPayable(DateTime time, string number, float money)
       {
           return idal.CreditAccountsPayable(time, number, money);
       }
       //将会计凭证项目信息写入数据表
       public bool WriteAccountQuery(string code, int rownumber, string supplierID, string AccountID, float debit, float credit, string taxcode)
       {
           return idal.WriteAccountQuery(code, rownumber, supplierID, AccountID, debit, credit, taxcode);
       }

        public bool WriteAccountFrount(string code, DateTime time, string type, string ccode)
       {
           return idal.WriteAccountFrount(code, time, type, ccode);
       }
       public DataTable ReadAccountFrount(string code)
       {
           return idal.ReadAccountFrount(code);
       }
       public DataTable ReadAccountInner(string code)
       {
           return idal.ReadAccountInner(code);
       }
        /// <summary>
        /// 获取所有财务凭证编码
        /// </summary>
        /// <returns></returns>
       public List<string> GetAllVocherID()
       {
           return idal.GetAllVocherID();
       }
        /// <summary>
        /// 获取所有会计凭证详细信息
        /// </summary>
        /// <returns></returns>
       public DataTable GetALLAccountInformation()
       {
           return idal.GetALLAccountInformation();
       }
        /// <summary>
        /// 获取所有税码详细信息
        /// </summary>
        /// <returns></returns>
       public DataTable GetAllTaxCodeInformation()
       {
           return idal.GetAllTaxCodeInformation();
       }
        /// <summary>
        /// 通过税码获取税率
        /// </summary>
        /// <param name="TaxCode"></param>
        /// <returns></returns>
       public DataTable GetTaxByTaxCode(string TaxCode)
       {
           return idal.GetTaxByTaxCode(TaxCode);
       }
        /// <summary>
        /// 获取所有科目名称
        /// </summary>
        /// <returns></returns>
       public List<string> GetAllAccountName()
       {
           return idal.GetAllAccountName();
       }
    }
}
