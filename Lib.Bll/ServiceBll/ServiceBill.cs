﻿using Lib.IDAL.ServiceIDAL;
using Lib.Model.ServiceEvaluation;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Bll.ServiceBll
{
    public class ServiceBill : ServiceIDAL
    {
        private ServiceSqlIDAL serviceSqlIDAL=new ServiceSqlIDAL();


        /// <summary>
        /// 获取供应商编号
        /// 
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public Dictionary<string, string> getSupplierId(string loginId)
        {
            Dictionary<string, string> supplierMap = new Dictionary<string, string>();

            DataTable dt= serviceSqlIDAL.getSupplierId(loginId);
            if (dt.Rows.Count >0) {
                foreach (DataRow dataRow in dt.Rows)
                {
                    supplierMap.Add(dataRow["供应商编号"].ToString(), dataRow["供应商名称"].ToString());

                }

            }
          
            return supplierMap;
        }

        public List<string> getGenMateName(string text1, string text2)
        {
            return serviceSqlIDAL.getGenMateName(text1, text2);
        }

        public List<string> getGenMateGroupName(string text)
        {
            return serviceSqlIDAL.getGenMateGroupName(text);
        }

        public List<string> getGenSupplierName()
        {
            return serviceSqlIDAL.getGenSupplierName();
        }

        public List<string> getMateName(string supplierName, string mateGroupName)
        {
            return serviceSqlIDAL.getMateName(supplierName, mateGroupName);
        }

        public DataTable getExServiceData(string supplierName, string MtGroupName, string mtName,string startTime,string endTime)
        {
            return serviceSqlIDAL.getExServiceData(supplierName, MtGroupName, mtName, startTime, endTime);
        }

        public List<string> getMateGroupName(string supplierName)
        {
            return serviceSqlIDAL.getMateGroupName(supplierName);
        }

        public DataTable getGenServiceData(string supplierName, string mtGroupName, string mtName, string startTime, string endTime)
        {
            return serviceSqlIDAL.getGenServiceData(supplierName, mtGroupName, mtName, startTime, endTime);
        }

        public List<string> getSupplierName()
        {
            return serviceSqlIDAL.getSupplierName();
        }

        public DataTable getGeneServiceRate(string mtGroupId)
        {
            return serviceSqlIDAL.getGeneServiceRate(mtGroupId);
        }

        public DataTable getExServiceRate(string mtGroupId)
        {
            return serviceSqlIDAL.getExServiceRate(mtGroupId);
        }

        public DataTable getServiceRate()
        {
            return serviceSqlIDAL.getServiceRate();
        }

        public DataTable getAllMtGroupidAndName()
        {
            return serviceSqlIDAL.getAllMtGroupidAndName();
        }

        public DataTable getServiceRate(string MTGName)
        {
            return serviceSqlIDAL.getServiceRate(MTGName);
        }

        public bool insertServiceRate(DataGridView mtGroupRate)
        {
            return serviceSqlIDAL.insertServiceRate(mtGroupRate);
        }

        public DataTable getGenServiceData()
        {
            return serviceSqlIDAL.getGenServiceData();
        }

        public DataTable getExServiceData()
        {
            return  serviceSqlIDAL.getExServiceData();
        }

        public Boolean insertServiceModel(string sQs, string sTs, string modelName)
        {
            return serviceSqlIDAL.insertServiceModel(sQs,sTs, modelName);
        }

        public bool insertGenServiceModel(string innos, string res, string uSs, string modelName)
        {
            return serviceSqlIDAL.insertGenServiceModel(innos, res, uSs, modelName);
        }

        public List<String> getGenServiceModelName()
        {
            return serviceSqlIDAL.getGenServiceModelName();
        }

        public DataTable getGenServiceModel(string Name)
        {
            return serviceSqlIDAL.getGenServiceModel(Name);
        }

        public Boolean insertServiceData(ServiceModel serviceData)
        {
            return serviceSqlIDAL.insertServiceData(serviceData);
        }

        public Boolean insertGenServiceData(GeneralServiceModle serviceData)
        {
            return serviceSqlIDAL.insertGenServiceData(serviceData);
        }

        public List<String> getServiceModelName()
        {
              return serviceSqlIDAL.getServiceModelName();
        }

        public DataTable getServiceModel(string Name)
        {
            return serviceSqlIDAL.getServiceModel(Name);
        }
        /// <summary>
        /// 获取物料编号
        /// </summary>
        /// <param name="MtGroupId"></param>
        /// <returns></returns>
        public Dictionary<string, string> getMtidAndName(string MtGroupId)
        {
            Dictionary<string, string> mtMap = new Dictionary<string, string>();

            DataTable dt = serviceSqlIDAL.getMtidAndName(MtGroupId);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dataRow in dt.Rows)
                {
                    mtMap.Add(dataRow["物料编号"].ToString(), dataRow["物料名称"].ToString());

                }

            }

            return mtMap;
        }
        //获取物料组编号
        public Dictionary<string, string> getMtGroupidAndName(string supplierId)
        {
            Dictionary<string, string> mtMap = new Dictionary<string, string>();

            DataTable dt = serviceSqlIDAL.getMtGroupidAndName(supplierId);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dataRow in dt.Rows)
                {
                    mtMap.Add(dataRow["物料组编号"].ToString(), dataRow["物料组名称"].ToString());

                }

            }

            return mtMap;
        }
    }
}
