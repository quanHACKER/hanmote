﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;


namespace Lib.Common
{
    [Serializable]
    public class Config
    {
        #region 系统常用参数设置
        //类库链接定义
        public static string DBDAL = "Lib.SqlServerDAL";
        //数据库链接
        public static string MMDBConnStr = ConfigurationManager.AppSettings["serverAddress"];
        //ftp地址读取
        public static string ftpServerIp = ConfigurationManager.AppSettings["ftpServerIP"];
        //ftp用户名
        public static string ftpServerUserID = ConfigurationManager.AppSettings["ftpServerUserID"];
        //ftp密码
        public static string ftpServerPsw = ConfigurationManager.AppSettings["ftpServerPsw"];
        #endregion
    }
}