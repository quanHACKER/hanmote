﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// 
    /// </summary>
    public class SignUtil
    {
        public const string EndSign = "\r\n";
        public const string MiddleSign = ",";//中间空两个字符
        public const string TimeSign = "yyyy-MM-dd hh-mm-ss";
        public const string SqlDateFormat = "yyyy-MM-dd hh:mm:ss";

    }
}
