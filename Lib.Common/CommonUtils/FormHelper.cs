﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// 控件帮助类
    /// </summary>
    public class FormHelper
    {
        /// <summary>
        /// 给ComboBox绑定数据
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="obj"></param>
        /// <param name="displayMember"></param>
        /// <param name="valueMember"></param>
        public static void combox_bind(ComboBox cb, object obj, string displayMember, string valueMember)
        {
            cb.DisplayMember = displayMember;
            cb.ValueMember = valueMember;
            cb.DataSource = obj;
        }

        public void formatDate(DateTimePicker dtp, string dateformat)
        {
            dtp.Format = DateTimePickerFormat.Custom;
            dtp.CustomFormat = dateformat;


        }

        /// <summary>
        /// 只能输入数字和小数点
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="dict_temp"></param>
        public void onlyNumber(TextBox tb, string key, Dictionary<string, string> dict_temp)
        {
            //string pattern = @"^[0-9]+[\.]?\d*$";
            string pattern = @"^([0-9]+[\.]?\d*)?$";     //可以支持一次全删除数字
            Match m = Regex.Match(tb.Text, pattern);
            if (!m.Success)
            {
                MessageUtil.ShowError("请输入数字");
                tb.Text = dict_temp[key];
                tb.SelectionStart = dict_temp[key].Length;

            }
            else
            {
                dict_temp[key] = tb.Text;
            }

        }

        /// <summary>
        /// 只能输入数字和小数点
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="dict_temp"></param>
        //public void onlyNumber(ComboBox cbb, string key, Dictionary<string, string> dict_temp)
        //{
        //    //string pattern = @"^[0-9]+[\.]?\d*$";
        //    string pattern = @"^([0-9]+[\.]?\d*)?$";     //可以支持一次全删除数字
        //    Match m = Regex.Match(cbb.Text, pattern);
        //    if (!m.Success)
        //    {
        //        MessageUtil.ShowError("请输入数字或小数点");
        //        cbb.Text = dict_temp[key];
        //        cbb.SelectionStart = dict_temp[key].Length;

        //    }
        //    else
        //    {
        //        dict_temp[key] = cbb.Text;
        //    }

        //}
        /// <summary>
        /// 只能输入数字和小数点
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="dict_temp"></param>
        public void onlyNumber(ComboBox cbb, string key, Dictionary<string, string> dict_temp)
        {
            //string pattern = @"^[0-9]+[\.]?\d*$";
            string pattern = @"^([0-9]+[\.]?\d*)?$";     //可以支持一次全删除数字
            Match m = Regex.Match(cbb.Text, pattern);
            if (!m.Success)
            {
                MessageUtil.ShowError("请输入数字");
                cbb.Text = dict_temp[key];
                cbb.SelectionStart = dict_temp[key].Length;

            }
            else
            {
                dict_temp[key] = cbb.Text;
            }

        }
        public void onlyNumber(DataGridViewCell cell, string key, Dictionary<string, string> dict_temp)
        {
            //string pattern = @"^[0-9]+[\.]?\d*$";
            string pattern = @"^([0-9]+[\.]?\d*)?$";     //可以支持一次全删除数字
            Match m = Regex.Match(cell.Value.ToString(), pattern);
            if (!m.Success)
            {
                MessageUtil.ShowError("请输入数字");
                cell.Value = dict_temp[key];
                cell.Value = "";

            }
            else
            {
                dict_temp[key] = cell.Value.ToString();
            }

        }
        /// <summary>
        /// 只能输入数字
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="key"></param>
        /// <param name="dict_temp"></param>
        public void onlyNumber2(TextBox tb, string key, Dictionary<string, string> dict_temp)
        {
            string pattern = @"^[0-9]+\d*$";
            
            Match m = Regex.Match(tb.Text, pattern);
            if (!m.Success)
            {
                tb.Text = dict_temp[key];
                tb.SelectionStart = dict_temp[key].Length;

            }
            else
            {
                dict_temp[key] = tb.Text;
            }

        }
        /// <summary>
        /// 验证邮政编码是否符合规范
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="key"></param>
        /// <param name="dict_temp"></param>
        public void ZipCode(ComboBox cbb)
        {
            if (cbb.Text != "")
            {
                var ss = cbb.Text;
                var re = @"^\d{6}$";
                Match m = Regex.Match(ss, re);
                if (!m.Success)
                {
                    MessageUtil.ShowError("请输入正确的邮政编码");
                    cbb.Text = null;
                    cbb.Focus();
                }
            }


        }
        /// <summary>
        /// 验证手机号码是否符合规范
        /// </summary>
        /// <param name="cbb"></param>
        public void telephone(ComboBox cbb)
        {
            if (cbb.Text != "")
            {
                var ss = cbb.Text;
                var re = @"^\d{11}$";
                Match m = Regex.Match(ss, re);
                if (!m.Success)
                {
                    MessageUtil.ShowError("请输入正确的手机号");
                    cbb.Text = null;
                    cbb.Focus();
                }
            }


        }
        /// <summary>
        /// 验证邮箱是否符合规范
        /// </summary>
        /// <param name="tbx"></param>
        public void Email(TextBox tbx)
        {
            if (tbx.Text != "")
            {
                var ss = tbx.Text;
                var re = @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$";
                Match m = Regex.Match(ss, re);
                if (!m.Success)
                {
                    MessageUtil.ShowError("请输入正确的邮箱");
                    tbx.Text = null;
                    tbx.Focus();
                }
            }


        }


        public void onlyNumberNegtive(TextBox tb, string key, Dictionary<string, string> dict_temp)
        {
            //string pattern = @"^[\-0-9]+[\.]?\d*$";
            //string pattern = @"^\-?[0-9]+(\.[0-9]+)?$"; 
            //string pattern = @"^\-?[0-9]+[\.]?\d*$"; 
            string pattern = @"^\-?([0-9]+[\.]?\d*)?$"; 
            Match m = Regex.Match(tb.Text, pattern);
            if (!m.Success)
            {
                tb.Text = dict_temp[key];
                tb.SelectionStart = dict_temp[key].Length;
            }
            else
            {
                dict_temp[key] = tb.Text;
            }

        }

        public void onlyNumberWord(TextBox tb, string key, Dictionary<string, string> dict_temp)
        {
            string pattern = @"^[A-Za-z0-9]+$";
            Match m = Regex.Match(tb.Text, pattern);
            if (!m.Success)
            {
                tb.Text = dict_temp[key];
                tb.SelectionStart = dict_temp[key].Length;

            }
            else
            {
                dict_temp[key] = tb.Text;
            }

        }





        /// <summary>
        /// 求三个数的平均值
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns>平均值</returns>
        public string ComputeAverage(string a, string b, string c, Dictionary<string, string> dict_temp)
        {
            float average = 0;
            float sum = ComputeSum(a, b, c, dict_temp);
            average = (float)(sum * 1.0) / 3;
            //dict_temp[averageStr] = average.ToString();
            return average.ToString();
        }

        /// <summary>
        /// 求三个数的和
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns>和</returns>
        public float ComputeSum(string a, string b, string c, Dictionary<string, string> dict_temp)
        {
            float sum = 0;

            sum = (float)(Convert.ToDouble(dict_temp[a]) + Convert.ToDouble(dict_temp[b]) + Convert.ToDouble(dict_temp[c]));
            //dict_temp[sumStr] = sum.ToString();
            return sum;
        }

        /// <summary>
        /// 控制日期
        /// </summary>
        /// <param name="beginDate"></param>
        /// <param name="formatStr"></param>
        public void ControlDate(DateTimePicker beginDate, string formatStr)
        {
            DateTime bd = DateTime.Parse(beginDate.Text);

            if (bd > DateTime.Now)
            {

                beginDate.Value = DateTime.Parse(DateTime.Now.ToString(formatStr));
            }
        }

        /// <summary>
        /// 隐藏所有的窗体
        /// </summary>
        /// <param name="ls"></param>
        public void closeAllFrm(List<Form> ls)
        {
            foreach (Form frm in ls)
            {
                frm.Hide();
            }
        }



        //求占比
        public string ComputePercent(string a, string b, Dictionary<string, string> dict_temp)
        {
            double result = 0;
            result = ((Convert.ToDouble(dict_temp[b])) / (Convert.ToDouble(dict_temp[a]))) * 100;
            return result.ToString("0.000");
        }
    }
}
