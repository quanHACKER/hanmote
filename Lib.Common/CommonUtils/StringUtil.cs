﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common.CommonUtils
{
    public class StringUtil
    {
        /// <summary>
        /// 把文件大小转换为合适的string
        /// </summary>
        /// <param name="size">字节数</param>
        /// <returns></returns>
        public static string convertFileSizeToString(double size)
        {
            if (size < 1024.0)
                return String.Format("{0:0.00}", size) + " B";

            size /= 1024.0;
            if (size < 1024.0)
                return String.Format("{0:0.00}", size) + " KB";

            size /= 1024.0;
            if (size < 1024.0)
                return String.Format("{0:0.00}", size) + " MB";

            size /= 1024.0;
            if (size < 1024.0)
                return String.Format("{0:0.00}", size) + " GB";

            size /= 1024.0;
            return String.Format("{0:0.00}", size) + " TB";
        }
    }
}
