﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// 数学有关的工具类
    /// </summary>
    public class MathUtil
    {
        //最小误差
        private const double MINERROR = 0.0000001;
        #region  double数是否和零相等
        /// <summary>
        /// double数是否和零相等
        /// </summary>
        /// <param name="value">double数</param>
        /// <returns></returns>
        public static bool equalsZero(double value)   
        {
            //value与0.0之差小于最小误差   返回true
            if (Math.Abs(value - 0.0) < MINERROR)
                return true;
            return false;
        }
        #endregion

        #region 两个double数是否相等
        /// <summary>
        /// 两个double数是否相等
        /// </summary>
        /// <param name="d1">第一个double</param>
        /// <param name="d2">第二个double</param>
        /// <returns></returns>
        public static bool equalsDouble(double d1,double d2)
        {
            //如果d1-d2的绝对值小于最小误差 返回true
            if (Math.Abs(d1 - d2) < MINERROR)
                return true;
            return false;
        }
        #endregion 
 
        //#region 大于等于某数
        ///// <summary>
        ///// 判断double1是否大于等于double2
        ///// </summary>
        ///// <param name="d1"></param>
        ///// <param name="d2"></param>
        ///// <returns></returns>
        //public static bool equalsOrGreater(double d1, double d2)
        //{
        //    if (d1 < d2)
        //        return false;
        //    return true;
        //}
        //#endregion 

        //#region  小于等于某数
        ///// <summary>
        ///// 判断double1是否小于等于double2
        ///// </summary>
        ///// <param name="d1"></param>
        ///// <param name="d2"></param>
        ///// <returns></returns>
        //public static bool equalsOrLess(double d1, double d2)
        //{
        //    if (d1 > d2)
        //        return false;
        //    return true;
        //}
        //#endregion 



        #region 除法
        /// <summary>
        /// double的除法
        /// </summary>
        /// <param name="d1">第一个double</param>
        /// <param name="d2">第二个double</param>
        /// <returns></returns>
        public static double division(double d1, double d2)
        {
            if (Double.IsInfinity(d1 / d2))
                throw new DivideByZeroException();
            return d1 / d2;
        }
        #endregion 

    }
}
