﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// 提示框的工具类
    /// </summary>
    public class MessageUtil
    {
        /// <summary>
        /// 提示一般的信息
        /// </summary>
        /// <param name="message">提示信息</param>
        /// <returns></returns>
        public static DialogResult ShowTips(string message)
        {
            return MessageBox.Show(message,"提示信息",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        /// <summary>
        /// 错误信息的提示
        /// </summary>
        /// <param name="message">错误信息</param>
        /// <returns></returns>
        public static DialogResult ShowError(string message)
        {
            return MessageBox.Show(message,"错误信息",MessageBoxButtons.OK,MessageBoxIcon.Error);
        }
        
        /// <summary>
        /// 警告信息的提示
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <returns></returns>
        public static DialogResult ShowWarning(string message)
        { 
            return MessageBox.Show(message,"警告信息",MessageBoxButtons.OK,MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 提示询问用户信息，并显示错误标志
        /// </summary>
        /// <param name="message">错误信息</param>
        /// <returns></returns>
        public static DialogResult ShowYesNoAndError(string message)
        {
            return MessageBox.Show(message,"错误信息",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
        }
        /// <summary>
        /// 提示询问用户信息，并显示提示标志
        /// </summary>
        /// <param name="message">提示信息</param>
        /// <returns></returns>
        public static DialogResult ShowYesNoAndTips(string message)
        {
            return MessageBox.Show(message,"提示信息",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
        }

        /// <summary>
        /// 提示询问用户信息，并显示警告标志
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <returns></returns>
        public static DialogResult ShowYesNoAndWarning(string message)
        {
            return MessageBox.Show(message, "警告信息", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 提示用户确认信息，并显示疑问标志
        /// </summary>
        /// <param name="message">确认信息</param>
        /// <returns></returns>
        public static DialogResult ShowOKCancelAndQuestion(string message) {
            return MessageBox.Show(message, "确认信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        }

        /// <summary>
        /// 提示询问用户信息，并显示提示标志
        /// </summary>
        /// <param name="message">错误信息</param>
        public static DialogResult ShowYesNoCancelAndTips(string message)
        {
            return MessageBox.Show(message, "提示信息", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
        }
    }
}
