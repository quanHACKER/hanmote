﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Cells;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    public class ExcelUtil
    {
        private static readonly Workbook workbook = new Workbook(); 
        private static string OledbConnString="Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
        public static void Open(string fileName)
        {
            if (workbook != null)
            {
                workbook.Open(fileName);
            }
            else
            {
                throw new ArgumentNullException("workbook");
            }
        }

        public static void ReadAllCells()
        {
            int totalNum = workbook.Worksheets.Count;
            for (int i = 0; i < totalNum; i++)
            { 
                
            }
            Cells cells = workbook.Worksheets[0].Cells;
            for (int i = 0; i < cells.MaxDataRow + 1; i++)
            {
                for (int j = 0; j < cells.MaxDataColumn + 1; j++)
                {
                    string s = cells[i, j].StringValue.Trim();
                }
            }
        }

        public static void ReadCellInWorksheetByNum(int num)
        {
            int totalNum = workbook.Worksheets.Count;
            if (num > totalNum || num < 0)
            {
                throw new Exception("不能读取worksheet");
            }
            Cells cells = workbook.Worksheets[num].Cells;
            for (int i = 0; i < cells.MaxDataColumn + 1; i++)
            {
                for (int j = 0; j < cells.MaxDataColumn + 1; j++)
                {
                    string s = cells[i, j].StringValue;
                }
            }
        }

        public static DataTable  ReadCellValueToDataTable(string fileName,string sheetName)
        {
            Open(fileName);
            DataTable dt = null;
            int totalNum = workbook.Worksheets.Count;
            for (int i = 0; i < totalNum; i++)
            {
                Worksheet worksheet = workbook.Worksheets[i];
                if (worksheet.Name.Equals(sheetName))
                {
                    Cells cells = worksheet.Cells;
                    dt = cells.ExportDataTableAsString(0, 0, cells.MaxDataRow + 1, cells.MaxColumn, true);
                    break;
                }
            }

            return dt;
        }

        public static string ReadCellValueToString(string fileName, string sheetName,string cellName)
        {
            Open(fileName);
            string s = null;
            int totalNum = workbook.Worksheets.Count;
            bool flag = false;
            for (int i = 0; i < totalNum; i++)
            {
                Worksheet worksheet = workbook.Worksheets[i];
                if (worksheet.Name.Equals(sheetName))
                {
                    Cells cells = worksheet.Cells;
                    for (int j = cells.MaxDataRow; j >= 0; j--)
                    {
                        for (int k = 0; k < cells.MaxDataColumn + 1; k++)
                        {
                            if (!string.IsNullOrWhiteSpace(cells[j, k].StringValue) && cells[j, k].StringValue.Contains(cellName))
                            {
                                s = cells[j+1, k].StringValue;
                                flag = true;
                                break;
                            }
                        }
                        if (flag) break;
                    }
                }
                if (flag) break;
            }

            return s;
        }


        public static DataTable GetExcelTable(string path)
        {
            try
            {
                //获取excel数据  
                DataTable dt1 = new DataTable();
                if (!(path.ToLower().IndexOf(".xlsx") < 0))
                {
                    OledbConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
                }
                string strConn = string.Format(OledbConnString, path);
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable dt = conn.GetSchema("Tables");
                //判断excel的sheet页数量，查询第1页  
                if (dt.Rows.Count > 0)
                {
                    string selSqlStr = string.Format("select * from [{0}]", dt.Rows[0]["TABLE_NAME"]);
                    OleDbDataAdapter oleDa = new OleDbDataAdapter(selSqlStr, conn);
                    oleDa.Fill(dt1);
                }
                conn.Close();

                return dt1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel转换DataTable出错：" + ex.Message);
                return null;
            }
        }






    }
}
