﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Lib.Common;

namespace Lib.Common.CommonUtils
{
    public class LogHelper
    {
        //存放日志文件的文件夹
        private static string logFilePath = "\\log\\";
        private static string directoryName = AppDomain.CurrentDomain.BaseDirectory + logFilePath;


        /// <summary>
        /// 写入基本登录和操作数据库的日志信息
        /// </summary>
        /// <param name="e"></param>
        public static void WriteMySqlLog(string loginfo)
        {
            Console.WriteLine(directoryName);
            StringBuilder sb = new StringBuilder();
            string dir = directoryName;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            string time = DateTime.Now.ToString(SignUtil.TimeSign);
            //写入信息到日志文件
            sb.Append(loginfo);
            //获取路径下的所有文件名，这个文件名是绝对路径
            string[] fileNames = Directory.GetFiles(dir);
            //用于存储最后修改时间和文件路径
            Dictionary<double, string> dic = new Dictionary<double, string>();
            //用于存储最后修改时间，以便进行排序
            List<double> lds = new List<double>();
            //将文件信息添加到dic和lds中
            foreach (string fileName in fileNames)
            {
                FileInfo finfo = new FileInfo(fileName);
                DateTime lastTime = finfo.LastWriteTime;
                TimeSpan sp = lastTime.Subtract(new DateTime(1970, 1, 1));
                dic.Add(sp.TotalMilliseconds, fileName);
                lds.Add(sp.TotalMilliseconds);

            }
            string valPath = "";
            //如果是第一次，路径中没有任何的文件
            if (lds.Count == 0)
            {
                valPath = dir + time + ".txt";
            }
            else
            {
                //如果只有一个文件，就不需要进行排序了
                if (lds.Count > 1)
                {
                    //由大到小进行排序
                    lds.Sort(delegate(double a, double b)
                    {
                        return -a.CompareTo(b);//从小到大排序，如果你想从大到小排序，改成：return -a.CompareTo(b);

                    });

                }
                //文件路径就是最后修改时间最大的那个
                valPath = dic[lds[0]];
                FileInfo fi = new FileInfo(valPath);
                //如果文件大小超过了1k，则重新建文件
                if (fi.Length > 1024 * 1024 * 1024)
                {
                    valPath = dir + time + ".txt";

                }
            }
            //将异常信息写入到文件中
            using (StreamWriter sw = new StreamWriter(valPath, true, Encoding.UTF8))
            {
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
            }

        }

        /// <summary>
        /// 写入异常日志信息
        /// </summary>
        /// <param name="e"></param>
        public static void WriteExceptionLog(Exception e)
        {
            Console.WriteLine(directoryName);
            StringBuilder sb = new StringBuilder();
            string dir = directoryName;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            //文件名称
            string time = DateTime.Now.ToString(SignUtil.TimeSign);
            //将异常信息写入其中
            if (e != null)
            {
                ConvertToNotNull(sb, "出现应用程序未处理的异常：" + DateTime.Now.ToString(SignUtil.SqlDateFormat));
                //获取异常信息
                ConvertToNotNull(sb, "异常消息：" + e.Message);
                //获取当前实例的运行时类型
                ConvertToNotNull(sb, "异常类型：" + e.GetType());
                //获取或设置导致错误的应用程序或对象的名称
                ConvertToNotNull(sb, "应用程序线程错误：" + e.Source);
                //获取引发当前异常的方法
                ConvertToNotNull(sb, "引发当前异常的方法：" + e.TargetSite);
                //获取调用堆栈上直接桢的字符串表示形式
                ConvertToNotNull(sb, "堆栈信息:" + e.StackTrace);
            }
            //首先获得这个路径下的所有文件名，这个文件名是绝对路径
            string[] fileNames = Directory.GetFiles(dir);
            //用于存储最后修改时间和文件路径
            Dictionary<double, string> dic = new Dictionary<double, string>();
            //用于存储最后修改时间，以便进行排序
            List<double> lds = new List<double>();
            //将文件信息添加到dic和lds中
            foreach (string fileName in fileNames)
            {
                FileInfo finfo = new FileInfo(fileName);
                DateTime lastTime = finfo.LastWriteTime;
                TimeSpan sp = lastTime.Subtract(new DateTime(1970, 1, 1));
                dic.Add(sp.TotalMilliseconds, fileName);
                lds.Add(sp.TotalMilliseconds);

            }
            string valPath = "";
            //如果是第一次，路径中没有任何的文件
            if (lds.Count == 0)
            {
                valPath = dir + time + ".txt";
            }
            else
            {
                //如果只有一个文件，就不需要进行排序了
                if (lds.Count > 1)
                {
                    //由大到小进行排序
                    lds.Sort(delegate(double a, double b)
                    {
                        return -a.CompareTo(b);//从小到大排序，如果你想从大到小排序，改成：return -a.CompareTo(b);

                    });

                }
                //文件路径就是最后修改时间最大的那个
                valPath = dic[lds[0]];
                FileInfo fi = new FileInfo(valPath);
                //如果文件大小超过了1k，则重新建文件
                if (fi.Length > 1024 * 1024 * 1024)
                {
                    valPath = dir + time + ".txt";

                }


            }
            //将异常信息写入到文件中
            using (StreamWriter sw = new StreamWriter(valPath, true, Encoding.UTF8))
            {
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
            }
        }

        public static void ConvertToNotNull(StringBuilder sb, object o)
        {
            if (o == null) return;
            else sb.Append(o + SignUtil.EndSign);
        }
    }
}
