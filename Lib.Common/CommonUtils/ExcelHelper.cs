﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Cells;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// excxel帮助工具类
    /// </summary>
    public class ExcelHelper
    {
        private string fileName;
        private Workbook workBook;
        private List<Workbook> workBooks;
        private Worksheets workSheets;


        public ExcelHelper() { }
        public ExcelHelper(string fileName) 
        {
            this.fileName = fileName;
        }



        private void openExcel() {
            workBook = new Workbook();
            if(string.IsNullOrWhiteSpace(fileName))
            {
                workBook.Open(fileName);
            }
        }

        #region 读取模块

        public void readExcel()
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                Workbook tcWorkBook = new Workbook();
                tcWorkBook.Open(fileName);
                //工作簿
                Worksheets tcWorkSheets = tcWorkBook.Worksheets;
                Worksheet tcWorkSheet;
                Cells tcCells;
                //索引行号
                //int tcRow = 0;
                //索引列号
                //int tcColumn = 0;
                Range tcRange;
                string sExcelValue = "";
                Cell tcCell;
                //遍历工作簿
                for (int i = 0; i < tcWorkSheets.Count; i++)
                {
                    tcWorkSheet = tcWorkSheets[i];
                    tcCells = tcWorkSheet.Cells;
                    //以索引的方式遍历工作表
                    //tcCell = tcCells[tcRow, tcColumn];
                    tcCell = tcCells["a2"];
                    string tcSheetName = tcWorkSheets[i].Name;
                    try
                    {
                        sExcelValue = tcCell.StringValue;    //可以获取空值
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("工作表 \"" + tcSheetName + "\" a2单元格不存在或者已经被合并：" + ex.Message);
                        //ex.StackTrace();
                    }
                    if (tcCell.IsMerged)
                    {
                        tcRange = tcCell.GetMergedRange();
                        //MessageBox.Show("工作表 \"" + tcSheetName + "\" a2单元格合并了 " + tcRange.RowCount.ToString() + " 行。");
                        //MessageBox.Show("工作表 \"" + tcSheetName + "\" a2单元格合并了 " + tcRange.ColumnCount.ToString() + " 列。");
                    }
                    else
                    {
                        //MessageBox.Show("工作表 \"" + tcSheetName + "\" a2单元格没有被合并。");
                    }
                }
            }
        }

        /// <summary>
        /// 根据索引获取worksheet工作簿(从0开始)
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Worksheet getWorksheetById(int index,Worksheets workSheets)
        {
            int count = workSheets != null ? workSheets.Count : 0;
            if (index > count || index < 0)
            {
                return null;
            }
            return workSheets[index];
        }
       

        #endregion

    }
}
