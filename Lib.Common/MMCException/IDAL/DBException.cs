﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common.MMCException.IDAL
{
    public class DBException : ApplicationException
    {
        private String msg;
        private Exception innerException;
        //空参构造函数
        public DBException() { }

        //带有一个字符串参数的构造函数
        public DBException(String msg) : base(msg)
        {
            this.Msg = msg;
        }

        //带有一个字符串参数和一个内部异常参数的构造参数
        public DBException(String msg , Exception innerException) : base(msg)
        {
            this.innerException = innerException;
            this.Msg = msg;
        }

        public string Msg { get => msg; set => msg = value; }
    }
}
