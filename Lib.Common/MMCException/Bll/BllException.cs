﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common.MMCException.Bll
{
    public class BllException : ApplicationException
    {
        private String msg;
        private Exception innerException;

        //空参数构造函数
        public BllException() { }

        //带有一个字符串参数的构造函数
        public BllException(String msg) : base(msg)
        {
            this.Msg = msg;
        }

        //带有一个字符串参数和一个内部异常参数的构造函数
        public BllException(String msg , Exception innerException) : base(msg)
        {
            this.Msg = msg;
            this.innerException = innerException;
        }

        public string Msg { get => msg; set => msg = value; }
    }
}
