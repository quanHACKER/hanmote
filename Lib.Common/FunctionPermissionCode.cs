﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common
{
    /// <summary>
    /// 功能权限编码类，每个Form对应一个类，每个类中列出当前Form可能的所有操作对应的编码
    /// Permission_ID以一个32位int值表示，0~7位标志窗口内操作、8~23位标志窗口、24~31位标志大模块
    /// <12位菜单码><12位窗口码><8位窗口内操作码>
    /// </summary>
    public class FunctionPermissionCode
    {
        public class ContractExecutionMonitorCode
        {
            public const int permission1 = 0x08000000;
        }
    }
}
