﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// 气泡图选择条件模版
    /// </summary>
    public class SPCompareBubbleChartConditionValue
    {
        private SelectSupplierConditionSettings selectSupplierConditionSettings;

        /// <summary>
        /// 设置查询供应商信息的条件类
        /// 包装类
        /// 此处不用继承方式
        /// </summary>
        public SelectSupplierConditionSettings SelectSupplierConditionSettings
        {
            get { return selectSupplierConditionSettings; }
            set { selectSupplierConditionSettings = value; }
        }

        private Dictionary<string, string> savaSelectedSupplierMap;

        /// <summary>
        /// 保存所选择的供应商信息
        /// 供应商Id
        /// 供应商名称
        /// </summary>
        public Dictionary<string, string> SavaSelectedSupplierMap
        {
            get { return savaSelectedSupplierMap; }
            set { savaSelectedSupplierMap = value; }
        }
    }
}
