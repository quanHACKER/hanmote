﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// 查询供应商信息条件设置类
    /// 后者物料组信息条件设置类
    /// </summary>
    public class SelectSupplierConditionSettings
    {
        private string purchaseId;

        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string PurchaseId
        {
            get { return purchaseId; }
            set { purchaseId = value; }
        }

        private string purchaseName;

        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string PurchaseName
        {
            get { return purchaseName; }
            set { purchaseName = value; }
        }

        private string year;

        /// <summary>
        /// 评估周期
        /// 年度
        /// </summary>
        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        private string month;

        /// <summary>
        /// 评估周期
        /// 时段
        /// </summary>
        public string Month
        {
            get { return month; }
            set { month = value; }
        }

    }
}
