﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// 查询物料信息条件设置类
    /// </summary>
    public class SelectMaterialConditionSettings
    {

        private SelectSupplierConditionSettings selectSupplierConditionSettings;

        /// <summary>
        /// 设置查询供应商信息的条件类
        /// 包装类
        /// 此处不用继承方式
        /// </summary>
        public SelectSupplierConditionSettings SelectSupplierConditionSettings
        {
            get { return selectSupplierConditionSettings; }
            set { selectSupplierConditionSettings = value; }
        }

        private string supplierId;
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }
    }
}
