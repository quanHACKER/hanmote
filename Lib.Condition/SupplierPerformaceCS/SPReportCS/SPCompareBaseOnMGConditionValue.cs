﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// /// <summary>
    /// 基于物料组的供应商绩效比较界面
    /// 保存该界面的值
    /// 作为查询条件
    /// </summary>
    /// </summary>
    public class SPCompareBaseOnMGConditionValue
    {
        private SelectSupplierConditionSettings selectSupplierConditionSettings;

        /// <summary>
        /// 设置查询供应商信息的条件类
        /// 包装类
        /// 此处不用继承方式
        /// </summary>
        public SelectSupplierConditionSettings SelectSupplierConditionSettings
        {
            get { return selectSupplierConditionSettings; }
            set { selectSupplierConditionSettings = value; }
        }

        private string materialGroupId;

        /// <summary>
        /// 物料组Id
        /// </summary>
        public string MaterialGroupId
        {
            get { return materialGroupId; }
            set { materialGroupId = value; }
        }

        private string materialGroupName;

        /// <summary>
        /// 物料组名称
        /// </summary>
        public string MaterialGroupName
        {
            get { return materialGroupName; }
            set { materialGroupName = value; }
        }
    }
}
