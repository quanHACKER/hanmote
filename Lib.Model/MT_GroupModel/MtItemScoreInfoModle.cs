﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
    public class MtItemScoreInfoModle
    {
        /// <summary>
        /// 打分子项
        /// </summary>
        private string id;
        private string aim;
        private string global;
        private string influenScore;
        private string riskScore;
        private string influenLevel;
        private string riskLevel;
        private string riskText;

        public string Global { get => global; set => global = value; }
        public string Id { get => id; set => id = value; }
        public string Aim { get => aim; set => aim = value; }
        public string RiskScore { get => riskScore; set => riskScore = value; }
      
        public string RiskLevel { get => riskLevel; set => riskLevel = value; }
        public string InfluenScore { get => influenScore; set => influenScore = value; }
        public string InfluenLevel { get => influenLevel; set => influenLevel = value; }
        public string RiskText { get => riskText; set => riskText = value; }
    }
}
