﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
   public  class PosResultInfoModel
    {
        #region 定位结果模型
        private String purOrgName;//采购组织名称
        private String mtName;//物料组名称
        private String influAndRiskScore;//影响/供应风险得分
        private String cost;//支出占比
        private String posResult;//定位结果

        public string PurOrgName { get => purOrgName; set => purOrgName = value; }
        public string MtName { get => mtName; set => mtName = value; }
        public string InfluAndRiskScore { get => influAndRiskScore; set => influAndRiskScore = value; }
        public string Cost { get => cost; set => cost = value; }
        public string PosResult { get => posResult; set => posResult = value; }
        #endregion
    }
}
