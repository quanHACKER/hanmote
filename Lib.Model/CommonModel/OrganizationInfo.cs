﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 组织信息   供应商准入时
    /// </summary>
    public class OrganizationInfo :BaseEntity
    {
        public string SupplierChineseName { get; set; }

        public string SupplierEnglishName { get; set; }

        public string CompanyProperty { get; set; }

        public string CompanyUsedName { get; set; }

        public string CompanyRegisterCountry { get; set; }

        public string CompanyWebsite { get; set; }

        public string CompanyCode { get; set; }

        public DateTime CompanyRegisterDate{ get; set; }

        public string CompanyRepresent{ get; set; }

        public int IsListed { get; set; }

        public string StockCode { get; set; }

        public int IsExport { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string DoorNum { get; set; }

        public string ZipCode { get; set; }

        public int Id { get; set; }

        public List<OrgContacts> orgContacts { get; set; }
    }
}
