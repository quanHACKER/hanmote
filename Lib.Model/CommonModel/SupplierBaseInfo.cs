﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    public class SupplierBaseInfo
    {
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        /// <summary>
        /// 物料品项 A/B
        /// </summary>
        public string MaterialItems { get; set; }

        /// <summary>string
        /// 物料组
        /// </summary>
        public string MaterialGroup { get; set; }
    }
}
