﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.FileManage
{
    public class File_Info
    {
        /// <summary>
        /// 文件编号
        /// </summary>
        public string File_ID { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public string File_Type { get; set; }

        /// <summary>
        /// 供应商可见等级
        /// </summary>
        public string Supplier_Visible_Level { get; set; }

        /// <summary>
        /// 创建者编号
        /// </summary>
        public string Creator_ID { get; set; }

        /// <summary>
        /// 创建者类型
        /// </summary>
        public string Creator_Type { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime Modify_Time { get; set; }
    }
}
