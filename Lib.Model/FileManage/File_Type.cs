﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.FileManage
{
    public class File_Type
    {
        /// <summary>
        /// 文件类型名称
        /// </summary>
        public string File_Type_Name { get; set; }

        /// <summary>
        /// 文件类型描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 创建者编号
        /// </summary>
        public string Creator_ID { get; set; }

        /// <summary>
        /// 创建者类型
        /// </summary>
        public string Creator_Type { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time { get; set; }
    }
}
