﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.FileManage
{
    public enum PermissionType
    {
        None = 0,
        Read = 1,
        ReadWrite = 2,
        ReadWriteDelete = 3
    }
}
