﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
  public  class InfoRecord
    {
      /// <summary>
      /// 信息记录编码
      /// </summary>
        public string Record_ID { get; set; }
      /// <summary>
      /// 物料编码
      /// </summary>
        public string Material_ID { get; set; }
      /// <summary>
      /// 物料名称
      /// </summary>
        public string Material_Name { get; set; }
      /// <summary>
      /// 供应商编码
      /// </summary>
        public String Supplier_ID { get; set; }
      /// <summary>
      /// 供应商名称
      /// </summary>
        public string Supplier_Name { get; set; }
      /// <summary>
      /// 净价
      /// </summary>
        public float NetPrice { get; set; }
      /// <summary>
      /// 工厂编码
      /// </summary>
        public string Factory_ID { get; set; }
      /// <summary>
      /// 库存地编码
      /// </summary>
        public string Stock_ID { get; set; }
      /// <summary>
      /// 价格确定编号
      /// </summary>
        public string Price_Determine { get; set; }


    }
}
