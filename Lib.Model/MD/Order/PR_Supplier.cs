﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
  public  class PR_Supplier
    {
      /// <summary>
      /// 采购申请编码
      /// </summary>
        public string PR_ID { get; set; }
      /// <summary>
      /// 物料编码
      /// </summary>
        public string Material_ID { get; set; }
      /// <summary>
      /// 供应商编码
      /// </summary>
        public string Supplier_ID { get; set; }
      /// <summary>
      /// 供应数量
      /// </summary>
        public long Number { get; set; }
      /// <summary>
      /// 完成状态
      /// </summary>
        public bool Finished_Mark { get; set; }
    }
}
