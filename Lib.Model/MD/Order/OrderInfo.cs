﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
   public class OrderInfo
    {
        /// <summary>
        /// 订单编码
        /// </summary>
        public string Order_ID { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public string Order_Type { get; set; }
       
       /// <summary>
       /// 公司代码
       /// </summary>
        public string Company_Code { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string Purchase_Organization  { get; set; }
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Supplier_ID { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time { get; set; }
        /// <summary>
        /// 交货时间
        /// </summary>
        public DateTime Delivery_Ttime{ get; set; }
        /// <summary>
        /// 交货地点
        /// </summary>
        public string Delivery_Points { get; set; }
        /// <summary>
        /// 交货方式
        /// </summary>
        public string Delivery_Type { get; set; }
        /// <summary>
        /// 总价值
        /// </summary>
        public float Total_Value { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string Order_Status { get; set; }
        /// <summary>
        /// 已接收的价值
        /// </summary>
        public float Value_Received { get; set; }
        /// <summary>
        /// 已付款项
        /// </summary>
        public float Value_Paid { get; set; }
       /// <summary>
       /// 总数量
       /// </summary>
        public int Total_Number { get; set; }
    }
}
