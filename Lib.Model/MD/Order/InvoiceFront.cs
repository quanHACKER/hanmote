﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
   public class InvoiceFront
    {
       /// <summary>
       /// 发票代码
       /// </summary>
        public string Invoice_Code { get; set; }
       /// <summary>
       /// 发票号码
       /// </summary>
        public string Invoice_Number { get; set; }
       /// <summary>
       /// 订单类型
       /// </summary>
        public string Certificate_Type { get; set; }
       /// <summary>
       /// 凭证编号
       /// </summary>
        public string Certificate_Code { get; set; }
       /// <summary>
       /// 开票日期
       /// </summary>
        public DateTime Invoice_Makeout_Time { get; set; }
       /// <summary>
       /// 创建日期
       /// </summary>
        public DateTime Create_Time { get; set; }
       /// <summary>
       /// 付款方式
       /// </summary>
        public string Payment_Type { get; set; }
       /// <summary>
       /// 发票总金额
       /// </summary>
        public string sum { get; set; }
       /// <summary>
       /// 货币单位
       /// </summary>
        public string Currency { get; set; }
       /// <summary>
       /// 总数量
       /// </summary>
        public int Sum_Number { get; set; }

    }
}
