﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
  public  class MaterialDocumentMM
    {
      /// <summary>
      /// 凭证编码
      /// </summary>
        public string StockDocumentId { get; set; }
      /// <summary>
      /// 订单号
      /// </summary>
        public string Order_ID { get; set; }
      /// <summary>
        /// 提货单号
      /// </summary>
        public string Delivery_ID { get; set; }
      /// <summary>
      /// 交货单号
      /// </summary>
        public string ReceiptNote_ID { get; set; }
      /// <summary>
        /// 记账日期
      /// </summary>
        public DateTime Posting_Date { get; set; }
      /// <summary>
        /// 凭证日期
      /// </summary>
        public DateTime Document_Date { get; set; }
      /// <summary>
        /// 收货人
      /// </summary>
        public string StockManager { get; set; }
      /// <summary>
        /// 移动类型
      /// </summary>
        public string Move_Type { get; set; }
      /// <summary>
      /// 是否冲销
      /// </summary>
        public bool Reversed { get; set; }
      /// <summary>
        /// 收货总数量
      /// </summary>
        public int Total_Number { get; set; }
      /// <summary>
        /// 收货总价值
      /// </summary>
        public float Total_Value { get; set; }
    }
}
