﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
   public class SupplierCompanyCode
    {
       /// <summary>
       /// 标识符（流水码）
       /// </summary>
        public int ID { get; set; }
       /// <summary>
        /// 供应商编码(主键）
       /// </summary>
        public string Supplier_ID { get; set; }
       /// <summary>
        /// 供应商名称
       /// </summary>
        public string Supplier_Name { get; set; }
       /// <summary>
        /// 公司代码(主键)
       /// </summary>
        public string Company_ID { get; set; }
       /// <summary>
        /// 公司名称
       /// </summary>
        public string Company_Name { get; set; }
       /// <summary>
        /// 统驭科目
       /// </summary>
        public string Controlling_Accounts { get; set; }
       /// <summary>
        /// 总部
       /// </summary>
        public string Head_Office { get; set; }
       /// <summary>
        /// 权限组
       /// </summary>
        public string Permissions_Set { get; set; }
       /// <summary>
        /// 少数标志
       /// </summary>
        public string Few_Marks { get; set; }
       /// <summary>
        /// 排序码
       /// </summary>
        public string Sort_Code { get; set; }
       /// <summary>
        /// 现金管理组
       /// </summary>
        public string CashManagement_Group { get; set; }
       /// <summary>
        /// 批准组
       /// </summary>
        public string Approval_Group { get; set; }
       /// <summary>
       /// 证书日期
       /// </summary>
        public DateTime Certificate_Date { get; set; }
       /// <summary>
        /// 利息计算标志
       /// </summary>
        public string InterestCalculated_Logo { get; set; }
       /// <summary>
        /// 计息周期
       /// </summary>
        public string Interest_Cycle { get; set; }
       /// <summary>
        /// 上次计算日期
       /// </summary>
        public DateTime LastCalculation_Date { get; set; }
       /// <summary>
        /// 上次利息运行
       /// </summary>
        public DateTime Last_Runtime { get; set; }
       /// <summary>
        /// 预扣税款代码
       /// </summary>
        public string Withholding_TaxCode { get; set; }
       /// <summary>
        /// 折扣所得税国家
       /// </summary>
        public string Income_Country { get; set; }
       /// <summary>
        /// 收受人类型
       /// </summary>
        public string Receiptor_Type { get; set; }
       /// <summary>
        /// 免税号
       /// </summary>
        public string Duty_Free { get; set; }
       /// <summary>
        /// 有效结束日
       /// </summary>
        public DateTime Effective_EndDate { get; set; }
       /// <summary>
        /// 免税的权限
       /// </summary>
        public string DutyFree_Access { get; set; }
       /// <summary>
        /// 先前的账户号码
       /// </summary>
        public string Previous_Account { get; set; }
       /// <summary>
        /// 人事编号
       /// </summary>
        public string Personnel_Number { get; set; }
       
       
    }
}
