﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
   public class Supplier_Feature
    {
       /// <summary>
       /// 供应商编码
       /// </summary>
        public string  Supplier_ID { get; set; }
       /// <summary>
       /// 特征类编码
       /// </summary>
        public string Class_ID { get; set; }
       /// <summary>
       /// 特征类名称
       /// </summary>
        public string Class_Name { get; set; }
       /// <summary>
       /// 特性编码
       /// </summary>
        public string Feature_ID { get; set; }
       /// <summary>
       /// 特性名称
       /// </summary>
        public string Feature_Name { get; set; }
       /// <summary>
       /// 特性值
       /// </summary>
        public string Feature_Value { get; set; }

    }
}
