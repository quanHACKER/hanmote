﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class MaterialBase
    {
       /// <summary>
       /// 标识符
       /// </summary>
        public int ID { get; set; }
       /// <summary>
       /// 物料编码
       /// </summary>
        public string Material_ID { get; set; }
       /// <summary>
       /// 物料名称（短文本）
       /// </summary>
        public string Material_Name { get; set;}
       /// <summary>
       /// 物料组
       /// </summary>
        public string Material_Group { get; set; }
       /// <summary>
       /// 物料大分类
       /// </summary>
        public string Material_Type  { get; set; }
       /// <summary>
       /// 物料二级分类
       /// </summary>
        public string Type_Name { get; set; }
       /// <summary>
       /// 物料用途（三级分类）
       /// </summary>
        public string Purpose { get; set; }
       /// <summary>
       /// 计量单位
       /// </summary>
        public string Measurement { get; set; }
       /// <summary>
       /// 工业标识
       /// </summary>
        public string In_Identification { get; set; }
       /// <summary>
       /// 材料标准
       /// </summary>
        public string Material_Standard { get; set; }
       /// <summary>
       /// 跨工厂物料状态
       /// </summary>
        public string Material_Status { get; set; }
       /// <summary>
       /// 物料状态描述
       /// </summary>
        public String Status_Information { get; set; }
       /// <summary>
       /// 产品特殊要求
       /// </summary>
        public string Special_Requirements { get; set; }
       /// <summary>
       /// 特征码（物料编码前十位）
       /// </summary>
        public string Condition_Code { get; set; }
       /// <summary>
       /// 温度条件
       /// </summary>
        public string Temperature_Condition { get; set; }
       /// <summary>
       /// 集装箱需求
       /// </summary>
        public string Container_Demand { get; set; }
       /// <summary>
       /// 标号类型
       /// </summary>
        public string Label_Type { get; set; }
       /// <summary>
       /// 存储条件
       /// </summary>
        public string Storage_Condition { get; set; }
       /// <summary>
       /// 危险物料号
       /// </summary>
        public string Dangerous_MTNumber { get; set; }
       /// <summary>
       /// 收货单据数
       /// </summary>
        public int Document_Number { get; set; }
       /// <summary>
       /// 最小剩余货架寿命
       /// </summary>
        public string MIN_SLlife { get; set; }
       /// <summary>
       /// 总货架寿命
       /// </summary>
        public string Total_SLlife { get; set; }
       /// <summary>
       /// SLED的期间标识
       /// </summary>
        public string SLED_Identify { get; set; }
       /// <summary>
       /// SLED的舍入规则
       /// </summary>
        public string SLED_Rule { get; set; }
       /// <summary>
       /// 仓储百分比
       /// </summary>
        public string Storge_Percentage { get; set; }
       /// <summary>
       /// 毛重
       /// </summary>
        public decimal Gross_Weight { get; set; }
       /// <summary>
       /// 净重
       /// </summary>
        public decimal Net_Weight { get; set; }
       /// <summary>
       /// 重量单位
       /// </summary>
        public string Weight_Unit { get; set; }
       /// <summary>
       /// 体积单位
       /// </summary>
        public string Volume_Unit { get; set; }
       /// <summary>
       /// 大小/量纲
       /// </summary>
        public string Dimension { get; set; }
       /// <summary>
       /// 折扣资格
       /// </summary>
        public string Discount_Qualifications { get; set; }
       /// <summary>
       /// 价值代码
       /// </summary>
        public string Value_Code { get; set; }
       /// <summary>
       /// 产品组
       /// </summary>
        public string Division { get; set; }
       /// <summary>
       /// MRP组
       /// </summary>
        public string MRP_Group { get; set; }
       /// <summary>
       /// ABC标识
       /// </summary>
        public String ABC_Identify { get; set; }
       /// <summary>
       /// 订单单位
       /// </summary>
        public string Order_Unit { get; set; }
       /// <summary>
       /// 可变单位
       /// </summary>
        public string Variable_Unit { get; set; }
       /// <summary>
       /// 有效起始期
       /// </summary>
        public DateTime EI_Period { get; set; }
       /// <summary>
       /// 批次管理标识
       /// </summary>
        public bool Batch_Mark { get; set; }
       /// <summary>
       /// MPN标识
       /// </summary>
        public bool MPN_Mark { get; set; }
       /// <summary>
       /// 市场管制标识
       /// </summary>
        public bool Market_Regulation_Identity { get; set; }
       /// <summary>
       /// 物料级别
       /// </summary>
        public string Material_Level { get; set; }
       /// <summary>
       /// 物料特性类
       /// </summary>
        public string Material_Class { get; set; }
        /// <summary>
        /// 生产性物料标识
        /// </summary>
        public bool Material_Purchase_Type { get; set; }

        /// <summary>
        /// 最小交货数量
        /// </summary>
        public string minDeliveryNum { get; set; }



    }
}
