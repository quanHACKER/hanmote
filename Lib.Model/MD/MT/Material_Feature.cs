﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class Material_Feature
    {
       /// <summary>
       /// 物料编码
       /// </summary>
        public string Material_ID { get; set; }
       /// <summary>
       /// 特征类编码
       /// </summary>
        public string Class_ID { get; set; }
       /// <summary>
       /// 特性编码
       /// </summary>
        public string Feature_ID { get; set; }
       /// <summary>
       /// 特性描述
       /// </summary>
        public string Feature_Name { get; set; }
       /// <summary>
       /// 特性值
       /// </summary>
        public string Feature_Value { get; set; }
    }
}
