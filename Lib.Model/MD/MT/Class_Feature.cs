﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class Class_Feature
    {
       /// <summary>
       /// 特征类编码
       /// </summary>
        public string Class_ID { get; set; }
       /// <summary>
       /// 特征类描述
       /// </summary>
        public string Class_Name { get; set; }
       /// <summary>
       /// 特性编码
       /// </summary>
        public string Feature_ID { get; set; }
       /// <summary>
       /// 特性描述
       /// </summary>
        public string Feature_Name { get; set; }
    }
}
