﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class Material_Class
    {
       /// <summary>
       /// 物料编码
       /// </summary>
        public string Material_ID { get; set; }
       /// <summary>
       /// 物料名称
       /// </summary>
        public string Material_Name { get; set; }
       /// <summary>
       /// 特征类编码
       /// </summary>
        public string Class_ID { get; set; }
       /// <summary>
       /// 特征类描述
       /// </summary>
        public string Class_Name { get; set; }
    }
}
