﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD
{
   /**
    * 证书类型类
    */
    public class CertifileModel
    {
        private string certiFileTypeId;
        private string certiFileTypeName;
        private string certiFileStorePath;

        public string CertiFileStorePath { get => certiFileStorePath; set => certiFileStorePath = value; }
        public string CertiFileTypeId { get => certiFileTypeId; set => certiFileTypeId = value; }
        public string CertiFileTypeName { get => certiFileTypeName; set => certiFileTypeName = value; }
    }
}
