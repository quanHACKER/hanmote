﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD
{
    /// <summary>
    /// 主标准
    /// </summary>
    public class MainEvalFactor
    {
        #region 属性
        //代码
        private string code;
        //名称
        private string desc;
        #endregion


        #region 方法
        public string Code { get => code; set => code = value; }
        public string Desc { get => desc; set => desc = value; }
        #endregion
    }
}
