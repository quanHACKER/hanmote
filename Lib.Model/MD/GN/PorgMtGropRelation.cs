﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.GN
{
    public class PorgMtGropRelation
    {
        /// <summary>
        /// 标识符
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string Porg_ID { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string Porg_Name { get; set; }
        /// <summary>
        /// 物料组编码
        /// </summary>
        public string MtGroup_ID { get; set; }
        /// <summary>
        /// 物料组名称
        /// </summary>
        public string MtGroup_Name { get; set; }
    }
}
