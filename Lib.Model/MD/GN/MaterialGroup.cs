﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.GN
{
   public class MaterialGroup
    {
      /// <summary>
      /// 标识符
      /// </summary>
        public int ID { get; set; }
       /// <summary>
       /// 物料组名称
       /// </summary>
        public string Material_Group { get; set; }
       /// <summary>
       /// 物料组描述
       /// </summary>
        public string Description { get; set; }
       /// <summary>
       /// 工厂编号
       /// </summary>
        public string Factory_ID { get; set; }
    }
}
