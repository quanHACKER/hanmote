﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD
{
   public class PaymentClause
    {
        /// <summary>
        /// 付款条件编码
        /// </summary>
        public string Clause_ID { get; set; }
        /// <summary>
        /// 付快条件描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 第一次折扣
        /// </summary>
        public float First_Discount { get; set; }
        /// <summary>
        /// 第一次折扣天数
        /// </summary>
        public int First_Date { get; set; }
        /// <summary>
        /// 第二次折扣
        /// </summary>
        public float Second_Discount { get; set; }
        /// <summary>
        /// 第二次折扣天数
        /// </summary>
        public int Second_Date { get; set; }
        /// <summary>
        /// 付清天数
        /// </summary>
        public int Final_Date { get; set; }

    }
}
