﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.GN
{
   public class Factory
    {
       /// <summary>
       /// 工厂编码
       /// </summary>Test
        public string  FactoryID { get; set; }
       /// <summary>
       /// 工厂名称
       /// </summary>
        public string FactoryName { get; set; }
       /// <summary>
       /// 工厂所属公司代码
       /// </summary>
        public string CompanyCode { get; set; }
    }
}
