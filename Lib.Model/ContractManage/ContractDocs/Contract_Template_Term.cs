﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template_Term
    {
        /// <summary>
        /// 排列序号
        /// </summary>
        public int Order_Number;

        /// <summary>
        /// 条款Sample的ID
        /// </summary>
        public string Term_Sample_ID;

        /// <summary>
        /// 条款所属章节ID
        /// </summary>
        public string Section_ID;

        /// <summary>
        /// 说明
        /// </summary>
        public string Detail;

        /// <summary>
        /// 自定义注释
        /// </summary>
        public string Annotation;
    }
}
