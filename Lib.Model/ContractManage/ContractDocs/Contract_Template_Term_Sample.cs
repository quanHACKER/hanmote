﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template_Term_Sample
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name;
        
        /// <summary>
        /// 所属类别
        /// </summary>
        public string Class;

        /// <summary>
        /// 对应的word文件地址
        /// </summary>
        public string FileName;

        /// <summary>
        /// 详细说明
        /// </summary>
        public string Detail;
    }
}
