﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    /// <summary>
    ///合同模板类 
    /// </summary>
    public class ContractTemplate
    {
        /// <summary>
        /// 合同模板编号
        /// </summary>
        private string contractTemplateID;

        public string ContractTemplateID
        {
            get { return contractTemplateID; }
            set { contractTemplateID = value; }
        }

        private string createrID;
        /// <summary>
        /// 合同模板创建者ID
        /// </summary>
        public string CreaterID
        {
            get { return createrID; }
            set { createrID = value; }
        }

        /// <summary>
        /// 合同文本
        /// </summary>
        private string contractTemplateContext;

        public string ContractTemplateContext
        {
            get { return contractTemplateContext; }
            set { contractTemplateContext = value; }
        }

        /// <summary>
        /// 合同名称
        /// </summary>
        private string contractTemplateName;

        public string ContractTemplateName
        {
            get { return contractTemplateName; }
            set { contractTemplateName = value; }
        }

        /// <summary>
        /// 合同Ins？？？
        /// </summary>
        private string contractTemplateIns;

        public string ContractTemplateIns
        {
            get { return contractTemplateIns; }
            set { contractTemplateIns = value; }
        }

        /// <summary>
        /// 合同模板状态：已审核，未审核
        /// </summary>
        private bool contractTemplateState;

        public bool ContractTemplateState
        {
            get { return contractTemplateState; }
            set { contractTemplateState = value; }
        }

        private DateTime createTime;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }

        /// <summary>
        /// 开始时间
        /// </summary>
        private DateTime beginTime;

        public DateTime BeginTime
        {
            get { return beginTime; }
            set { beginTime = value; }
        }

        /// <summary>
        /// 结束时间
        /// </summary>
        private DateTime endTime;

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
    }
}
