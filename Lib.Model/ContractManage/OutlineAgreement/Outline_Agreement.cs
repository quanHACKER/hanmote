﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Outline_Agreement
    {
        private string id;
        /// <summary>
        /// 框架协议编号
        /// </summary>
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string supplier_Id;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_Id
        {
            get { return supplier_Id; }
            set { supplier_Id = value; }
        }

        private string type;
        /// <summary>
        /// 框架协议类型
        /// </summary>
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        private string purchase_Group;
        /// <summary>
        /// 采购组
        /// </summary>
        public string Purchase_Group
        {
            get { return purchase_Group; }
            set { purchase_Group = value; }
        }

        private string purchase_Org;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string Purchase_Org
        {
            get { return purchase_Org; }
            set { purchase_Org = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string creater_Id;
        /// <summary>
        /// 创建者编号
        /// </summary>
        public string Creater_Id
        {
            get { return creater_Id; }
            set { creater_Id = value; }
        }

        private string status;
        /// <summary>
        /// 框架协议状态
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string factory_Id;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_Id
        {
            get { return factory_Id; }
            set { factory_Id = value; }
        }

        private string stock_Id;
        /// <summary>
        /// 库存地编号
        /// </summary>
        public string Stock_Id
        {
            get { return stock_Id; }
            set { stock_Id = value; }
        }

        private DateTime begin_Time;
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime Begin_Time
        {
            get { return begin_Time; }
            set { begin_Time = value; }
        }

        private DateTime end_Time;
        /// <summary>
        /// 截止时间
        /// </summary>
        public DateTime End_Time
        {
            get { return end_Time; }
            set { end_Time = value; }
        }

        private string trade_Clause;
        /// <summary>
        /// 交付条件
        /// </summary>
        public string Trade_Clause
        {
            get { return trade_Clause; }
            set { trade_Clause = value; }
        }

        private string trade_Text;
        /// <summary>
        /// 交付条件文本
        /// </summary>
        public string Trade_Text
        {
            get { return trade_Text; }
            set { trade_Text = value; }
        }

        private string payment_Clause;
        /// <summary>
        /// 付款条件
        /// </summary>
        public string Payment_Clause
        {
            get { return payment_Clause; }
            set { payment_Clause = value; }
        }

        private int days1;
        public int Days1
        {
            get { return days1; }
            set { days1 = value; }
        }

        private double discount1;
        public double Discount1
        {
            get { return discount1; }
            set { discount1 = value; }
        }

        private int days2;
        public int Days2
        {
            get { return days2; }
            set { days2 = value; }
        }

        private double discount2;
        public double Discount2
        {
            get { return discount2; }
            set { discount2 = value; }
        }

        private int days3;
        public int Days3
        {
            get { return days3; }
            set { days3 = value; }
        }

        private double target_Value;
        /// <summary>
        /// 目标值
        /// </summary>
        public double Target_Value
        {
            get { return target_Value; }
            set { target_Value = value; }
        }

        private string currency_Type;
        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency_Type
        {
            get { return currency_Type; }
            set { currency_Type = value; }
        }

        private bool valid;
        /// <summary>
        /// 是否失效
        /// </summary>
        public bool Valid
        {
            get { return valid; }
            set { valid = value; }
        }
    }
}
