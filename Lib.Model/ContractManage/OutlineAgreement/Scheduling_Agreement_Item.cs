﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Scheduling_Agreement_Item
    {
        private string scheduling_Agreement_ID;
        /// <summary>
        /// 计划协议ID
        /// </summary>
        public string Scheduling_Agreement_ID
        {
            get { return scheduling_Agreement_ID; }
            set { scheduling_Agreement_ID = value; }
        }

        private int item_Num;
        /// <summary>
        /// 计划协议项编号
        /// </summary>
        public int Item_Num
        {
            get { return item_Num; }
            set { item_Num = value; }
        }

        private string i;
        /// <summary>
        /// 采购项目类型
        /// </summary>
        public string I
        {
            get { return i; }
            set { i = value; }
        }

        private string a;
        /// <summary>
        /// 
        /// </summary>
        public string A
        {
            get { return a; }
            set { a = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string short_Text;
        /// <summary>
        /// 短文本
        /// </summary>
        public string Short_Text
        {
            get { return short_Text; }
            set { short_Text = value; }
        }

        private double target_Quantity;
        /// <summary>
        /// 目标数量
        /// </summary>
        public double Target_Quantity
        {
            get { return target_Quantity; }
            set { target_Quantity = value; }
        }

        private string oUn;
        /// <summary>
        /// 目标数量单位
        /// </summary>
        public string OUn
        {
            get { return oUn; }
            set { oUn = value; }
        }

        private double net_Price;
        /// <summary>
        /// 净价
        /// </summary>
        public double Net_Price
        {
            get { return net_Price; }
            set { net_Price = value; }
        }

        private double every;
        /// <summary>
        /// 每
        /// </summary>
        public double Every
        {
            get { return every; }
            set { every = value; }
        }

        private string oPU;
        /// <summary>
        /// 净价计量单位
        /// </summary>
        public string OPU
        {
            get { return oPU; }
            set { oPU = value; }
        }

        private string material_Group;
        /// <summary>
        /// 物料组编号
        /// </summary>
        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        private string factory;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory
        {
            get { return factory; }
            set { factory = value; }
        }
    }
}
