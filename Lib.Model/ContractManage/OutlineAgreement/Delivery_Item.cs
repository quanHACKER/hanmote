﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Delivery_Item
    {
        private string scheduling_Agreement_ID;
        /// <summary>
        /// 计划协议ID
        /// </summary>
        public string Scheduling_Agreement_ID
        {
            get { return scheduling_Agreement_ID; }
            set { scheduling_Agreement_ID = value; }
        }

        private int material_Item_Num;
        /// <summary>
        /// 计划协议物料项编号
        /// </summary>
        public int Material_Item_Num
        {
            get { return material_Item_Num; }
            set { material_Item_Num = value; }
        }

        private int item_Num;
        /// <summary>
        /// 送货计划行编号
        /// </summary>
        public int Item_Num
        {
            get { return item_Num; }
            set { item_Num = value; }
        }

        private DateTime delivery_Time;
        /// <summary>
        /// 送货时间
        /// </summary>
        public DateTime Delivery_Time
        {
            get { return delivery_Time; }
            set { delivery_Time = value; }
        }

        private double delivery_Amount;
        /// <summary>
        /// 送货数量
        /// </summary>
        public double Delivery_Amount
        {
            get { return delivery_Amount; }
            set { delivery_Amount = value; }
        }

        private DateTime record_Delivery_Time;
        /// <summary>
        /// 统计送货时间
        /// </summary>
        public DateTime Record_Delivery_Time
        {
            get { return record_Delivery_Time; }
            set { record_Delivery_Time = value; }
        }

        private double accumulate_Delivery_Amount;
        /// <summary>
        /// 累积送货数量
        /// </summary>
        public double Accumulate_Delivery_Amount
        {
            get { return accumulate_Delivery_Amount; }
            set { accumulate_Delivery_Amount = value; }
        }

        private double lastTime_Accumulate_Amount;
        /// <summary>
        /// 上期累积数量
        /// </summary>
        public double LastTime_Accumulate_Amount
        {
            get { return lastTime_Accumulate_Amount; }
            set { lastTime_Accumulate_Amount = value; }
        }

        private double not_Delivery_Amount;
        /// <summary>
        /// 未清数量
        /// </summary>
        public double Not_Delivery_Amount
        {
            get { return not_Delivery_Amount; }
            set { not_Delivery_Amount = value; }
        }

        /// <summary>
        /// 状态(待收货、已收货)
        /// </summary>
        public string Status { get; set; }
    }
}
