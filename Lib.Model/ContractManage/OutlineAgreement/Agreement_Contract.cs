﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    /// <summary>
    /// 框架协议合同
    /// </summary>
    public class Agreement_Contract
    {
        private string agreement_Contract_ID;
        /// <summary>
        /// 协议合同ID
        /// 对应数据表中的ID
        /// </summary>
        public string Agreement_Contract_ID
        {
            get { return agreement_Contract_ID; }
            set { agreement_Contract_ID = value; }
        }

        private string agreement_Contract_Version;
        /// <summary>
        /// 合同版本号(初始值为1)
        /// 对应数据表中的Version
        /// </summary>
        public string Agreement_Contract_Version
        {
            get { return agreement_Contract_Version; }
            set { agreement_Contract_Version = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// 对应数据表中的Supplier_ID
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string agreement_Contract_Type;
        /// <summary>
        /// 协议合同类型(数量合同、价值合同)
        /// 对应数据表中的Type
        /// </summary>
        public string Agreement_Contract_Type
        {
            get { return agreement_Contract_Type; }
            set { agreement_Contract_Type = value; }
        }

        private string purchase_Group;
        /// <summary>
        /// 采购组
        /// 对应数据表中的Purchase_Group
        /// </summary>
        public string Purchase_Group
        {
            get { return purchase_Group; }
            set { purchase_Group = value; }
        }

        private string purchase_Organization;
        /// <summary>
        /// 采购组织
        /// 对应数据表中的Purchase_Org
        /// </summary>
        public string Purchase_Organization
        {
            get { return purchase_Organization; }
            set { purchase_Organization = value; }
        }

        private string creater_ID;
        /// <summary>
        /// 创建者ID
        /// 对应数据表中的Creater_ID
        /// </summary>
        public string Creater_ID
        {
            get { return creater_ID; }
            set { creater_ID = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 创建时间
        /// 对应数据表中的Create_Time
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string status;
        /// <summary>
        /// 协议状态(未审核、已审核、执行完成、已变更)
        /// 对应数据表中的Status
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string factory;
        /// <summary>
        /// 工厂编号
        /// 对应数据表中的Factory_ID
        /// </summary>
        public string Factory
        {
            get { return factory; }
            set { factory = value; }
        }

        private string storage_Location;
        /// <summary>
        /// 库存位置
        /// 对应数据表中的Stock_ID
        /// </summary>
        public string Storage_Location
        {
            get { return storage_Location; }
            set { storage_Location = value; }
        }

        private DateTime begin_Time;
        /// <summary>
        /// 有效开始时间
        /// 对应数据表中的Begin_Time
        /// </summary>
        public DateTime Begin_Time
        {
            get { return begin_Time; }
            set { begin_Time = value; }
        }

        private DateTime end_Time;
        /// <summary>
        /// 有效结束时间
        /// 对应数据表中的End_Time
        /// </summary>
        public DateTime End_Time
        {
            get { return end_Time; }
            set { end_Time = value; }
        }

        private string payment_Type;
        /// <summary>
        /// 付款条件
        /// 对应数据表中的Payment_Clause
        /// </summary>
        public string Payment_Type
        {
            get { return payment_Type; }
            set { payment_Type = value; }
        }

        private int days1;
        /// <summary>
        /// 天数1
        /// </summary>
        public int Days1
        {
            get { return days1; }
            set { days1 = value; }
        }

        private double discount_Rate1;
        /// <summary>
        /// 天数1前的折扣比
        /// </summary>
        public double Discount_Rate1
        {
            get { return discount_Rate1; }
            set { discount_Rate1 = value; }
        }

        private int days2;
        /// <summary>
        /// 天数2
        /// </summary>
        public int Days2
        {
            get { return days2; }
            set { days2 = value; }
        }

        private double discount_Rate2;
        /// <summary>
        /// 天数2前的折扣比
        /// </summary>
        public double Discount_Rate2
        {
            get { return discount_Rate2; }
            set { discount_Rate2 = value; }
        }

        private int days3;
        /// <summary>
        /// days3前付款净额
        /// </summary>
        public int Days3
        {
            get { return days3; }
            set { days3 = value; }
        }

        private string iT_Term_Code;
        /// <summary>
        /// 国贸术语编号
        /// 对应数据表中的Trade_Clause
        /// </summary>
        public string IT_Term_Code
        {
            get { return iT_Term_Code; }
            set { iT_Term_Code = value; }
        }

        private string iT_Term_Text;
        /// <summary>
        /// 国贸术语文本
        /// 对应数据表中的Trade_Text
        /// </summary>
        public string IT_Term_Text
        {
            get { return iT_Term_Text; }
            set { iT_Term_Text = value; }
        }

        private double target_Value;
        /// <summary>
        /// 目标值
        /// </summary>
        public double Target_Value
        {
            get { return target_Value; }
            set { target_Value = value; }
        }

        private string currency_Type;
        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency_Type
        {
            get { return currency_Type; }
            set { currency_Type = value; }
        }

        private double exchange_Rate;
        /// <summary>
        /// 汇率(暂时不考虑)
        /// </summary>
        public double Exchange_Rate
        {
            get { return exchange_Rate; }
            set { exchange_Rate = value; }
        }

        private int isValid;
        ///<summary>
        ///有效位
        ///</summary>
        public int IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }

        private double finished;
        ///<summary>
        ///已完成数量
        ///</summary>
        public double Finished
        {
            get { return finished; }
            set { finished = value; }
        }
    }
}
