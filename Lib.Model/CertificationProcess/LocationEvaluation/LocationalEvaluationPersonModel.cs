﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.CertificationProcess.LocationEvaluation
{
    public class LocationalEvaluationPersonModel
    {
        private String ID { get; set; }
        private String name { get; set; }
        private String email { get; set; }
        private String evalCode { get; set; }
        private String mobile { get; set; }
        private String telPhone { get; set; }
        private String address { get; set; }
        private String parentId { get; set; }
        private String password { get; set; }
        private String typeId { get; set; }
        public static  String companyID { get; set; }
        public static String companyName { get; set; }
        private String PurGroupName { get; set; }
    }
}
