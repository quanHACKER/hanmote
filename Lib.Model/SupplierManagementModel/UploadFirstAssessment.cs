﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 上传初评文档的实体类
    /// </summary>
    public class UploadFirstAssessment
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierID { get; set; }

        /// <summary>
        /// 是否上传初评文件
        /// </summary>
        public byte IsUploadedAssessment { get; set; }

        /// <summary>
        /// 上传初评文件的路径
        /// </summary>
        public string UploadedAssessmentPath { get; set; }

        /// <summary>
        /// 是否上传承诺书
        /// </summary>
        public byte IsUploadedCommitment { get; set; }

        /// <summary>
        /// 上传承诺书文件的路径
        /// </summary>
        public string UploadedCommitmentPath { get; set; }

        /// <summary>
        /// 上传初评结果的日期
        /// </summary>
        public DateTime UploadedAssessmentDatetime { get; set; }

        /// <summary>
        /// 上传承诺书的日期
        /// </summary>
        public DateTime UploadedCommitmentDatetime { get; set; }

    }
}
