﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 上传决策结果的实体类
    /// </summary>
    public class UploadDecesionResult
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierID { get; set; }

        /// <summary>
        /// 是否上传决策结果文件  0/1（没有/有）
        /// </summary>
        public byte IsUploadedDecesionResult { get; set; }

        /// <summary>
        /// 上传决策结果文件路径（服务器路径）
        /// </summary>
        public string UploadedDecesionResultPath { get; set; }

        /// <summary>
        /// 上传决策结果的日期
        /// </summary>
        public DateTime UploadedDecesionResultDatetime { get; set; }
    }
}
