﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 感知类型实体类
    /// </summary>
    public class ApperceiveType
    {
        /// <summary>
        /// 核心
        /// </summary>
        public const string CORE = "核心";
        
        /// <summary>
        /// 发展
        /// </summary>
        public const string DEVELOPMENT = "发展";

        /// <summary>
        /// 盘剥
        /// </summary>
        public const string EXPLOIT = "盘剥";

        /// <summary>
        /// 边缘
        /// </summary>
        public const string EDGE = "边缘";
    }
}
