﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class DecisionInfo
    {
        public string SupplierName { get; set; }

        public string DecisionResult { get; set; }

        public string Descriptions { get; set; }

        public DateTime DecisionTime { get; set; }
        /// <summary>
        /// 供应商状态
        /// </summary>
        public int Status { get; set; }
    }
}
