﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 物料品项类型
    /// </summary>
    public class MaterialItemType
    {
        /// <summary>
        /// 关键型
        /// </summary>
        public const string KEY = "关键型";
        
        /// <summary>
        /// 瓶颈型
        /// </summary>
        public const string BOTTLENECK = "瓶颈型";

        /// <summary>
        /// 杠杆型
        /// </summary>
        public const string LEVER = "杠杆型";

        /// <summary>
        /// 常规型
        /// </summary>
        public const string NORMAL = "常规型";

    }
}
