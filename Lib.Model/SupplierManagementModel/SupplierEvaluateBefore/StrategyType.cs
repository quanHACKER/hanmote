﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 供应商策略类型实体类
    /// </summary>
    public class StrategyType
    {
        /// <summary>
        /// 战略合作型
        /// </summary>
        public const string STRATEGIC_COOPERATION = "战略合作型";
        
        /// <summary>
        /// 协作型
        /// </summary>
        public const string COOPERATION = "协作型";

        /// <summary>
        /// 交易型
        /// </summary>
        public const string TRANSACTION = "交易型";
    }
}
