﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{

    /// <summary>
    /// PIP等级实体类
    /// </summary>
    public class PIPLevel
    {
        /// <summary>
        /// PIP等级的H
        /// </summary>
        public const string High = "H";
        /// <summary>
        /// PIP等级的M
        /// </summary>
        public const string Middle = "M";
        /// <summary>
        /// PIP等级的L
        /// </summary>
        public const string Low = "L";
        /// <summary>
        /// PIP等级的N
        /// </summary>
        public const string Normal = "N";

        public static readonly string[] pipMap = {"N","L","M","H"};

        private Dictionary<int, string> map = new Dictionary<int, string>();
    }
}
