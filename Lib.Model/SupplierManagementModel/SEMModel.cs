﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    public class SEMModel
    {
        public string  SupplierName { get; set; }

        public double TotalSocre { get; set; }

        public string Others { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }

        public string UploadDate { get; set; }

        public string Level { get; set; }

        public int Status { get; set; }

    }

    public class SEMLevel
    { 
        public const string A = "A";
        public const string B="B";
        public const string C="C";
    }
    public class SEMScore
    {
        public const double FirstLadderScore = 90.0;
        public const double SecondLadderScore = 60.0;
    }
}
