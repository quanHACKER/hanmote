﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PurchaseOrderExecution
{
    public class Invoice_Tolerance
    {
        /// <summary>
        /// 变量名称
        /// </summary>
        public string Param_Name { get; set; }

        /// <summary>
        /// 边界(下限 0、上限 1)
        /// </summary>
        public int Border { get; set; }

        /// <summary>
        /// 是否检测绝对值
        /// </summary>
        public bool AbValue_Is_Check { get; set; }

        /// <summary>
        /// 绝对值
        /// </summary>
        public double AbValue { get; set; }

        /// <summary>
        /// 是否检测百分比
        /// </summary>
        public bool Per_Is_Check { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public double Per { get; set; }
    }
}