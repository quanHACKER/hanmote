﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PurchaseOrderExecution
{
    public class Invoice_Item
    {
        /// <summary>
        /// 发票编号
        /// </summary>
        public string Invoice_ID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        public double Unit_Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// 总额
        /// </summary>
        public double Sum { get; set; }

        /// <summary>
        /// 货币
        /// </summary>
        public string Currency { get; set; }
    }
}
