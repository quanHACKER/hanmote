﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PurchaseOrderExecution
{
    public class Invoice
    {
        /// <summary>
        /// 发票编号
        /// </summary>
        public string Invoice_ID { get; set; }

        /// <summary>
        /// 发票代码
        /// </summary>
        public string Invoice_Code { get; set; }

        /// <summary>
        /// 发票号码
        /// </summary>
        public string Invoice_Number { get; set; }

        /// <summary>
        /// 凭证类型
        /// </summary>
        public string Certificate_Type { get; set; }

        /// <summary>
        /// 凭证编号
        /// </summary>
        public string Certificate_ID { get; set; }

        /// <summary>
        /// 开票日期(发票的开票日期)
        /// </summary>
        public DateTime Invoice_MakeOut_Time { get; set; }

        /// <summary>
        /// 创建日期(系统创建发票记录的日期)
        /// </summary>
        public DateTime Create_Time { get; set; }

        /// <summary>
        /// 收款方名称
        /// </summary>
        public string Payer_Name { get; set; }

        /// <summary>
        /// 收款方识别号
        /// </summary>
        public string Payer_Identify_Number { get; set; }

        /// <summary>
        /// 付款方名称
        /// </summary>
        public string Recipient_Name { get; set; }

        /// <summary>
        /// 付款方识别号
        /// </summary>
        public string Recipient_Identify_Number { get; set; }

        /// <summary>
        /// 付款条件
        /// </summary>
        public string Payment_Type { get; set; }

        /// <summary>
        /// 总金额
        /// </summary>
        public double Sum { get; set; }

        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 开票人
        /// </summary>
        public string Invoice_Maker { get; set; }

        /// <summary>
        /// 系统记录创建人
        /// </summary>
        public string Creator { get; set; }

        /// <summary>
        /// 发票状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Supplier_Name { get; set; }
    }
}
