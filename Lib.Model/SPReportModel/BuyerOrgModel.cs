﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SPReportModel
{
    public class BuyerOrgModel
    {
        private string buyer_Org;

        /// <summary>
        /// 采购组织编号
        /// </summary>
        public string Buyer_Org
        {
            get { return buyer_Org; }
            set { buyer_Org = value; }
        }

        private string buyer_Org_Name;

        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string Buyer_Org_Name
        {
            get { return buyer_Org_Name; }
            set { buyer_Org_Name = value; }
        }
    }
}
