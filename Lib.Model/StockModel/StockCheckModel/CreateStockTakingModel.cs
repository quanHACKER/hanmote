﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel.StockCheckModel
{
    public class CreateStockTakingModel
    {
        /// <summary>
        /// 盘点凭证编号
        /// </summary>
        public string stocktakingDocument_ID { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public string Material_ID { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public DateTime Document_Date { get; set; }
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID { get; set; }
        /// <summary>
        /// 库存地
        /// </summary>
        public string Stock_ID { get; set; }
        /// <summary>
        /// 评估类型
        /// </summary>
        public string Evaluate_Type { get; set; }
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Group { get; set; }
        /// <summary>
        /// 库存状态
        /// </summary>
        public string Inventory_State { get; set; }
        /// <summary>
        /// 库存类型
        /// </summary>
        public string Inventory_Type { get; set; }
        /// <summary>
        /// 批次
        /// </summary>
        public string batch_ID { get; set; }
        /// <summary>
        /// 计划盘点日期
        /// </summary>
        public DateTime planStocktaking_Date { get; set; }
        /// <summary>
        /// 盘点参考号
        /// </summary>
        public string stocktakingrefer_ID { get; set; }
        /// <summary>
        /// 盘点号
        /// </summary>
        public string stocktaking_ID { get; set; }
        /// <summary>
        /// 冻结过账
        /// </summary>
        public bool Freezingpost { get; set; }
        /// <summary>
        /// 冻结库存
        /// </summary>
        public bool Freezingsum { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public float StockCount { get; set; }
        /// <summary>
        /// 盘点差异
        /// </summary>
        public float Difference { get; set; }
        /// <summary>
        /// 盘点数量
        /// </summary>
        public float Stocktaking_count { get; set; }
        /// <summary>
        /// 盘点状态
        /// </summary>
        public string State { get; set; }

    }
}


