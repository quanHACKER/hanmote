﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class OrderSupReceivingModel
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        private string supplier_name;

        public string Supplier_name
        {
            get { return supplier_name; }
            set { supplier_name = value; }
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        private string supplier_ID;

        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }


        /// <summary>
        /// 供应商所在地址
        /// </summary>
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        private string company_ZipCode;

        public string Company_ZipCode
        {
            get { return company_ZipCode; }
            set { company_ZipCode = value; }
        }


        /*
        /// <summary>
        /// 联系人
        /// </summary>
        private string contact_Person;

        public string Contact_Person
        {
            get { return contact_Person; }
            set { contact_Person = value; }
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        private string contact_phone1;

        public string Contact_phone1
        {
            get { return contact_phone1; }
            set { contact_phone1 = value; }
        }
         * */
    }
}
