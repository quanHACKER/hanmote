﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class InventoryExtendModel:InventoryModel
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        private string material_Name;

        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        /// <summary>
        /// 工厂名称
        /// </summary>
        private string factory_Name;

        public string Factory_Name
        {
            get { return factory_Name; }
            set { factory_Name = value; }
        }

        /// <summary>
        /// 库存名称
        /// </summary>
        private string stock_Name;

        public string Stock_Name
        {
            get { return stock_Name; }
            set { stock_Name = value; }
        }
    }
}
