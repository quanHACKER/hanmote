﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class MaterialDocumentModel
    {
        /// <summary>
        /// 物料凭证ID
        /// </summary>
        private string stockInDocument_ID;

        public string StockInDocument_ID
        {
            get { return stockInDocument_ID; }
            set { stockInDocument_ID = value; }
        }

        /// <summary>
        /// 物料ID
        /// </summary>
        private string material_ID;

        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        /// <summary>
        /// 移动类型
        /// </summary>
        private string document_Type;

        public string Document_Type
        {
            get { return document_Type; }
            set { document_Type = value; }
        }

        /// <summary>
        /// 订单ID
        /// </summary>
        private string order_ID;

        public string Order_ID
        {
            get { return order_ID; }
            set { order_ID = value; }
        }

        /// <summary>
        /// 过账日期
        /// </summary>
        private DateTime posting_Date;

        public DateTime Posting_Date
        {
            get { return posting_Date; }
            set { posting_Date = value; }
        }

        /// <summary>
        /// 凭证日期
        /// </summary>
        private DateTime document_Date;

        public DateTime Document_Date
        {
            get { return document_Date; }
            set { document_Date = value; }
        }

        /// <summary>
        /// 送货单号
        /// </summary>
        private string deliveryNote_ID;

        public string DeliveryNote_ID
        {
            get { return deliveryNote_ID; }
            set { deliveryNote_ID = value; }
        }

        /// <summary>
        /// 提货单号
        /// </summary>
        private string receiptNote_ID;

        public string ReceiptNote_ID
        {
            get { return receiptNote_ID; }
            set { receiptNote_ID = value; }
        }

        /// <summary>
        /// 工厂编号
        /// </summary>
        private string factory_ID;

        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        /// <summary>
        /// 仓库地点
        /// </summary>
        private string stock_Place;

        public string Stock_Place
        {
            get { return stock_Place; }
            set { stock_Place = value; }
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        private string supplier_ID;

        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        /// <summary>
        /// 仓库管理人员
        /// </summary>
        private string stockManager;

        public string StockManager
        {
            get { return stockManager; }
            set { stockManager = value; }
        }

        /// <summary>
        /// 库存类型
        /// </summary>
        private string inventory_Type;

        public string Inventory_Type
        {
            get { return inventory_Type; }
            set { inventory_Type = value; }
        }

        /// <summary>
        /// 实收数量
        /// </summary>
        private int receive_count;

        public int Receive_count
        {
            get { return receive_count; }
            set { receive_count = value; }
        }

        /// <summary>
        /// 订单中的数量
        /// </summary>
        private int countInOrder;

        public int CountInOrder
        {
            get { return countInOrder; }
            set { countInOrder = value; }
        }

        /// <summary>
        /// 输入单位计的数量
        /// </summary>
        private int inputCount;

        public int InputCount
        {
            get { return inputCount; }
            set { inputCount = value; }
        }

        /// <summary>
        /// SKU中的数量
        /// </summary>
        private int sKUCount;

        public int SKUCount
        {
            get { return sKUCount; }
            set { sKUCount = value; }
        }

        /// <summary>
        /// 批次号
        /// </summary>
        private string batch_ID;

        public string Batch_ID
        {
            get { return batch_ID; }
            set { batch_ID = value; }
        }

        /// <summary>
        /// 供应商物料编号
        /// </summary>
        private string supplierMaterial_ID;

        public string SupplierMaterial_ID
        {
            get { return supplierMaterial_ID; }
            set { supplierMaterial_ID = value; }
        }

        /// <summary>
        /// 物料组
        /// </summary>
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        /// <summary>
        /// 评估类型
        /// </summary>
        private string evaluate_Type;

        public string Evaluate_Type
        {
            get { return evaluate_Type; }
            set { evaluate_Type = value; }
        }

        /// <summary>
        /// 容差
        /// </summary>
        private decimal tolerance;

        public decimal Tolerance
        {
            get { return tolerance; }
            set { tolerance = value; }
        }

        /// <summary>
        /// 卸货点
        /// </summary>
        private string loading_Place;

        public string Loading_Place
        {
            get { return loading_Place; }
            set { loading_Place = value; }
        }

        /// <summary>
        /// 收货方
        /// </summary>
        private string receiving_Place;

        public string Receiving_Place
        {
            get { return receiving_Place; }
            set { receiving_Place = value; }
        }

        /// <summary>
        /// 是否冲销过
        /// </summary>
        private bool reversred;

        public bool Reversred
        {
            get { return reversred; }
            set { reversred = value; }
        }

        /// <summary>
        /// 注释中的数量
        /// </summary>
        private int notesNumber;

        public int NotesNumber
        {
            get { return notesNumber; }
            set { notesNumber = value; }
        }

        /// <summary>
        /// 说明文本
        /// </summary>
        private string explainText;

        public string ExplainText
        {
            get { return explainText; }
            set { explainText = value; }
        }

        //目标物料id
        private string toMaterial_ID;

        public string ToMaterial_ID
        {
            get { return toMaterial_ID; }
            set { toMaterial_ID = value; }
        }

        //目标物料名称
        private string toMaterial_Name;

        public string ToMaterial_Name
        {
            get { return toMaterial_Name; }
            set { toMaterial_Name = value; }
        }

        //目标工厂id
        private string toFactory_ID;

        public string ToFactory_ID
        {
            get { return toFactory_ID; }
            set { toFactory_ID = value; }
        }

        //目标工厂名称
        private string toFactory_Name;

        public string ToFactory_Name
        {
            get { return toFactory_Name; }
            set { toFactory_Name = value; }
        }

        //目标库存id
        private string toStock_ID;

        public string ToStock_ID
        {
            get { return toStock_ID; }
            set { toStock_ID = value; }
        }

        //目标库存名称
        private string toStock_Name;

        public string ToStock_Name
        {
            get { return toStock_Name; }
            set { toStock_Name = value; }
        }

        //特殊库存
        private string specialStock;

        public string SpecialStock
        {
            get { return specialStock; }
            set { specialStock = value; }
        }
    }
}
