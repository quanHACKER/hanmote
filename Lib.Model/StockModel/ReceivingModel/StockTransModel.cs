﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class StockTransModel
    {
        /// <summary>
        /// 事务编号
        /// </summary>
        private string stockTrans_ID;

        public string StockTrans_ID
        {
            get { return stockTrans_ID; }
            set { stockTrans_ID = value; }
        }

        /// <summary>
        /// 事务名称
        /// </summary>
        private string stockTrans_Name;

        public string StockTrans_Name
        {
            get { return stockTrans_Name; }
            set { stockTrans_Name = value; }
        }

        /// <summary>
        /// 是否激活
        /// </summary>
        private Boolean stockTrans_Activate;

        public Boolean StockTrans_Activate
        {
            get { return stockTrans_Activate; }
            set { stockTrans_Activate = value; }
        }
    }
}
