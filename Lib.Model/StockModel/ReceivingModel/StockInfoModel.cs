﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class StockInfoModel
    {
        /// <summary>
        /// 流水码
        /// </summary>
        private string iD;

        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }

        /// <summary>
        /// 仓库编码
        /// </summary>
        private string stock_ID;

        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        private string stock_Name;

        public string Stock_Name
        {
            get { return stock_Name; }
            set { stock_Name = value; }
        }

        /// <summary>
        /// 地址
        /// </summary>
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        /// <summary>
        /// 所属工厂编码
        /// </summary>
        private string factory_ID;

        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }
    }
}
