﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class MaterialDocumentNewModel
    {
        //库存凭证ID
        private string stockInDocument_ID;
        /// <summary>
        /// 库存凭证ID
        /// </summary>
        public string StockInDocument_ID
        {
            get { return stockInDocument_ID; }
            set { stockInDocument_ID = value; }
        }

        private DateTime posting_Date;
        /// <summary>
        /// 过账时间
        /// </summary>
        public DateTime Posting_Date
        {
            get { return posting_Date; }
            set { posting_Date = value; }
        }

        private DateTime document_Date;
        /// <summary>
        /// 凭证时间
        /// </summary>
        public DateTime Document_Date
        {
            get { return document_Date; }
            set { document_Date = value; }
        }

        private string stockManager;
        /// <summary>
        /// 库存管理员
        /// </summary>
        public string StockManager
        {
            get { return stockManager; }
            set { stockManager = value; }
        }

        private string document_Type;
        /// <summary>
        /// 移动类型
        /// </summary>
        public string Document_Type
        {
            get { return document_Type; }
            set { document_Type = value; }
        }

        private Boolean reversred;
        /// <summary>
        /// 冲销字段
        /// </summary>
        public Boolean Reversred
        {
            get { return reversred; }
            set { reversred = value; }
        }

        private string order_ID;
        /// <summary>
        /// 订单号，可以用于存储转移单号等等
        /// </summary>
        public string Order_ID
        {
            get { return order_ID; }
            set { order_ID = value; }
        }

        private string delivery_ID;
        /// <summary>
        /// 送货单
        /// </summary>
        public string Delivery_ID
        {
            get { return delivery_ID; }
            set { delivery_ID = value; }
        }

        private string receiptNote_ID;
        /// <summary>
        /// 收货单号
        /// </summary>
        public string ReceiptNote_ID
        {
            get { return receiptNote_ID; }
            set { receiptNote_ID = value; }
        }

        private int total_Number;

        /// <summary>
        /// 此次收货总数量
        /// </summary>
        public int Total_Number
        {
            get { return total_Number; }
            set { total_Number = value; }
        }

        private float total_Value;

        /// <summary>
        /// 此次收货总价值
        /// </summary>
        public float Total_Value
        {
            get { return total_Value; }
            set { total_Value = value; }
        }


    }
}
