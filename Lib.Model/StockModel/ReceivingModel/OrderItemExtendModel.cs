﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class OrderItemExtendModel:OrderItemModel
    {
        //扩展字段
        private string material_Name;

        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

    }
}
