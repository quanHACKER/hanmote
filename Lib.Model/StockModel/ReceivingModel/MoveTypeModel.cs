﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class MoveTypeModel
    {
        /// <summary>
        /// 移动类型编号
        /// </summary>
        private string mT_ID;

        public string MT_ID
        {
            get { return mT_ID; }
            set { mT_ID = value; }
        }

        /// <summary>
        /// 移动类型意义
        /// </summary>
        private string mT_Name;

        public string MT_Name
        {
            get { return mT_Name; }
            set { mT_Name = value; }
        }

        /// <summary>
        /// 移动类型原因
        /// </summary>
        private string mT_Reason;

        public string MT_Reason
        {
            get { return mT_Reason; }
            set { mT_Reason = value; }
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        private Boolean mT_Valid;

        public Boolean MT_Valid
        {
            get { return mT_Valid; }
            set { mT_Valid = value; }
        }

        /// <summary>
        /// 移动类型事务分配
        /// </summary>
        private string mT_TransactionDtb;

        public string MT_TransactionDtb
        {
            get { return mT_TransactionDtb; }
            set { mT_TransactionDtb = value; }
        }
    }
}
