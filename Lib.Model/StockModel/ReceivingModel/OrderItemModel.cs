﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public  class OrderItemModel
    {
        private string order_Id;

        public string Order_Id
        {
            get { return order_Id; }
            set { order_Id = value; }
        }

        private string material_ID;

        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }
        private string factory_ID;

        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }
        private string stock_ID;

        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }
        private double net_Price;

        public double Net_Price
        {
            get { return net_Price; }
            set { net_Price = value; }
        }
        private int number;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }
        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }
        private double total_price;

        public double Total_price
        {
            get { return total_price; }
            set { total_price = value; }
        }
        private string batch_ID;

        public string Batch_ID
        {
            get { return batch_ID; }
            set { batch_ID = value; }
        }
        private string pR_ID;

        public string PR_ID
        {
            get { return pR_ID; }
            set { pR_ID = value; }
        }
        private DateTime delivery_Time;

        public DateTime Delivery_Time
        {
            get { return delivery_Time; }
            set { delivery_Time = value; }
        }
        private string info_Type;

        public string Info_Type
        {
            get { return info_Type; }
            set { info_Type = value; }
        }
        private string info_Number;

        public string Info_Number
        {
            get { return info_Number; }
            set { info_Number = value; }
        }
        private string purchase_ID;

        public string Purchase_ID
        {
            get { return purchase_ID; }
            set { purchase_ID = value; }
        }
    }
}
