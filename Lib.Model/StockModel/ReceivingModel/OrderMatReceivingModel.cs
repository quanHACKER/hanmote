﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class OrderMatReceivingModel
    {
        /// <summary>
        /// 物料编码
        /// </summary>
        private string material_ID;

        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        /// <summary>
        /// 物料名称
        /// </summary>
        private string material_Name;

        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        /// <summary>
        /// 采购订单数量
        /// </summary>
        private int material_Count;

        public int Material_Count
        {
            get { return material_Count; }
            set { material_Count = value; }
        }

        /// <summary>
        /// 物料组
        /// </summary>
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        private string measurement;

        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }
    }
}
