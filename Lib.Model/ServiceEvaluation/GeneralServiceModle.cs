﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceEvaluation
{
    public class GeneralServiceModle : ServiceModel
    {

        //工厂编号
        private string factoryId;
        //创新性得分
        private float innovationSco;
        //创新性占比
        private string InnoRate;
        //创新性理由
        private string InReason;
        //可靠性得分
        private float realizableSco;
        //可靠性占比
        private string ReRate;
        //可靠性理由
        private string reReson;
        //用户服务得分
        private float userServiceSco;
        //用户服务占比
        private String UserRate;
        //用户服务理由
        private string userSerReson;
        //一般服务编号
        private string genServiceId;

        public string FactoryId { get => factoryId; set => factoryId = value; }
        public float InnovationSco { get => innovationSco; set => innovationSco = value; }
        public float RealizableSco { get => realizableSco; set => realizableSco = value; }
        public float UserServiceSco { get => userServiceSco; set => userServiceSco = value; }
        public string GenServiceId { get => genServiceId; set => genServiceId = value; }

        public string ReReson { get => reReson; set => reReson = value; }
        public string UserSerReson { get => userSerReson; set => userSerReson = value; }
        public string InReason1 { get => InReason; set => InReason = value; }
        public string InnoRate1 { get => InnoRate; set => InnoRate = value; }
        public string ReRate1 { get => ReRate; set => ReRate = value; }
        public string UserRate1 { get => UserRate; set => UserRate = value; }
    }
}
