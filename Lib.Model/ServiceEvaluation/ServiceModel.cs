﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceEvaluation
{
    public class ServiceModel
    {
        //供应商编号
        private String supplierId;
        //供应商名称
        private String supplierName;
        //物料组名称
        private string mtGroupName;
        //物料名称
        private string mtName;
        //供应商所选物料编号
        private String mtID;
        //服务工单编号
        private String extServiceId;
        //服务地点
        private String servicePlace;
        //创建者
        private String creator;
        //服务及时性
        private DateTime serviceTime;
        //服务及时性得分
        private float serviceTimeScore;
        //服务及时性占比
        private string STRate;
        //服务及时性评分依据
        private String serviceTimeReason;
        //服务质量得分
        private float serviceQualityScroce;
        //服务质量占比
        private string SQRate;
        //服务质量理由
        private String serviceQualityReason;
        //综合得分
        private double serviceTotalScore;

        public string SupplierId { get => supplierId; set => supplierId = value; }
        public string MtID { get => mtID; set => mtID = value; }
        public string ExtServiceId { get => extServiceId; set => extServiceId = value; }
        public string ServicePlace { get => servicePlace; set => servicePlace = value; }
        public string Creator { get => creator; set => creator = value; }
        public DateTime ServiceTime { get => serviceTime; set => serviceTime = value; }
        public float ServiceTimeScore { get => serviceTimeScore; set => serviceTimeScore = value; }
        public string ServiceTimeReason { get => serviceTimeReason; set => serviceTimeReason = value; }
        public float ServiceQualityScroce { get => serviceQualityScroce; set => serviceQualityScroce = value; }
        public string ServiceQualityReason { get => serviceQualityReason; set => serviceQualityReason = value; }
        
        public string SupplierName { get => supplierName; set => supplierName = value; }
        public string STRate1 { get => STRate; set => STRate = value; }
        public string SQRate1 { get => SQRate; set => SQRate = value; }
        public double ServiceTotalScore { get => serviceTotalScore; set => serviceTotalScore = value; }
        public string MtGroupName { get => mtGroupName; set => mtGroupName = value; }
        public string MtName { get => mtName; set => mtName = value; }
    }
}
