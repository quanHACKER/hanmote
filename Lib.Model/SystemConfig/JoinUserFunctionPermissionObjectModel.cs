﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 用户、权限对象联系Model
    /// </summary>
    public class JoinUserFunctionPermissionObjectModel
    {
        /// <summary>
        /// 自增变量
        /// </summary>
        public int User_Permission_ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 权限对象编号
        /// </summary>
        public int Permission_ID { get; set; }
    }
}
