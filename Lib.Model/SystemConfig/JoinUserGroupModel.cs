﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 用户、组联系表
    /// </summary>
    public class JoinUserGroupModel
    {
        /// <summary>
        /// 自增标志
        /// </summary>
        public int User_Group_ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public String Username { get; set; }

        /// <summary>
        /// 组编号
        /// </summary>
        public String Group_ID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public String Group_Name { get; set; }
    }
}
