﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 用户角色联系表
    /// </summary>
    public class JoinUserRoleModel
    {
        /// <summary>
        /// 自增变量
        /// </summary>
        public int User_Role_ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public String Username { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        public String Role_ID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public String Role_Name { get; set; }
    }
}
