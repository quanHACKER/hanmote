﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 基本角色表
    /// </summary>
    public class BaseRoleModel
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        public String Role_ID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public String Role_Name { get; set; }

        /// <summary>
        /// 角色描述信息
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// 左值
        /// </summary>
        public int Left_Value { get; set; }

        /// <summary>
        /// 右值
        /// </summary>
        public int Right_Value { get; set; }

        /// <summary>
        /// 层次
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 角色建立时间
        /// </summary>
        public String Generate_Time { get; set; }

        /// <summary>
        /// 角色建立者编号
        /// </summary>
        public String Generate_User_ID { get; set; }

        /// <summary>
        /// 角色建立者名字
        /// </summary>
        public String Generate_Username { get; set; }

        /// <summary>
        /// 角色更新时间
        /// </summary>
        public String Modify_Time { get; set; }

        /// <summary>
        /// 角色更新者编号
        /// </summary>
        public String Modify_User_ID { get; set; }

        /// <summary>
        /// 角色更新者名称
        /// </summary>
        public String Modify_Username { get; set; }

        public String Compang_ID { get; set; }
    }
}
