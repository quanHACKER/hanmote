﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    public class BaseOrganizationModel
    {
        public String Organization_ID { get; set; }

        public String Organization_Name { get; set; }

        public int Left_Value { get; set; }

        public int Right_Value { get; set; }

        public int Level { get; set; }

        public String Generate_Time { get; set; }

        public String Generate_User_ID { get; set; }

        public String Generate_Username { get; set; }

        public String Modify_Time { get; set; }

        public String Modify_User_ID { get; set; }

        public String Modify_Username { get; set; }

        public String Description { get; set; }
    }
}
