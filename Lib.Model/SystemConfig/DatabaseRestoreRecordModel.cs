﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    public class DatabaseRestoreRecordModel
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string DB_Name { get; set; }

        /// <summary>
        /// 恢复时间
        /// </summary>
        public string Restore_Time { get; set; }

        /// <summary>
        /// 恢复操作人编号
        /// </summary>
        public int Restore_Operator { get; set; }

        /// <summary>
        /// 恢复方式
        /// </summary>
        public int Restore_Type { get; set; }

        /// <summary>
        /// 恢复使用的备份源（备份记录ID）
        /// </summary>
        public int Restore_Source { get; set; }
    }
}
