﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 角色、权限对象联系Model
    /// </summary>
    public class JoinRoleFunctionPermissionObjectModel
    {
        /// <summary>
        /// 自增变量
        /// </summary>
        public int Role_Permission_ID { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        public String Role_ID { get; set; }

        /// <summary>
        /// 权限对象编号
        /// </summary>
        public int Permission_ID { get; set; }
    }
}
