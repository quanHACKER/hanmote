﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 功能权限对象Model
    /// </summary>
    public class BaseFunctionPermissionObjectModel
    {
        /// <summary>
        /// 权限ID
        /// </summary>
        public int Permission_ID { get; set; }

        /// <summary>
        /// 菜单name
        /// </summary>
        public String Menu_Name { get; set; }

        /// <summary>
        /// 窗口name
        /// </summary>
        public String Window_Name { get; set; }

        /// <summary>
        /// 权限名
        /// </summary>
        public String Permission_Name { get; set; }

        /// <summary>
        /// 权限描述
        /// </summary>
        public String Description { get; set; }

    }
}
