﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 用户登录记录Model
    /// </summary>
    public class LoginRecordModel
    {
        /// <summary>
        /// 自增变量
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 登录用户名
        /// </summary>
        public String Login_Name { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public String Login_Time { get; set; }

        /// <summary>
        /// 登出时间
        /// </summary>
        public String Logout_Time { get; set; }

        /// <summary>
        /// 登录者主机IP
        /// </summary>
        public String Login_IP { get; set; }

        /// <summary>
        /// 登录者域名
        /// </summary>
        public String Login_Host { get; set; }

        /// <summary>
        /// 是否异常登出
        /// </summary>
        public int Exception_Flag { get; set; }
    }
}
