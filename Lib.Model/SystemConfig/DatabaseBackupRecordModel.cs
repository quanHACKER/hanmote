﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    public class DatabaseBackupRecordModel
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string DB_Name { get; set; }

        /// <summary>
        /// 备份时间
        /// </summary>
        public string Backup_Time { get; set; }

        /// <summary>
        /// 备份操作人编号
        /// </summary>
        public string Backup_Operator { get; set; }

        /// <summary>
        /// 备份存放路径
        /// </summary>
        public string Backup_Path { get; set; }

        /// <summary>
        /// 备份文件大小
        /// </summary>
        public Int64 Backup_Size { get; set; }

        /// <summary>
        /// 备份方式
        /// </summary>
        public int Backup_Type { get; set; }

        /// <summary>
        /// 备份状态
        /// </summary>
        public int Backup_Status { get; set; }
    }
}
