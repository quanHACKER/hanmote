﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class SystemUserOrganicRelationshipModel
    {
        public string compangClass { get; set; } //公司名称
        public string sysUser_ID { get; set; }//员工编号
        public string jobPosition { get; set; }//职位类型
        public string departName { get; set; }//部门名称
        public string sysUserName { get; set; }//姓名
        public string phoneNumber{ get; set; }//联系方式
        public int status { get; set; }//状态
                                         
        public int Enabled { get; set; }// 用户是否有效
                                        /// <summary>
                                        /// 用户编号
                                        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        public String Login_Name { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public String Login_Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public String Username { get; set; }

        /// <summary>
        /// 性别 0为男、1为女
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public String Birthday { get; set; }

        /// <summary>
        /// 籍贯
        /// </summary>
        public String Birth_Place { get; set; }

        /// <summary>
        /// 用户联系方式
        /// </summary>
        public String Mobile { get; set; }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// 用户备注
        /// </summary>
        public String User_Description { get; set; }
        /// <summary>
        /// 是否为管理员
        /// </summary>
        public int Manager_Flag { get; set; }

        public string pid { get; set; }
        public string Evalcode { get; set; }
    }
}
