﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 基本组Model
    /// </summary>
    public class BaseGroupModel
    {
        /// <summary>
        /// 组编号
        /// </summary>
        public String Group_ID { get; set; }

        /// <summary>
        /// 组名
        /// </summary>
        public String Group_Name { get; set; }

        /// <summary>
        /// 组描述信息
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// 组建立时间
        /// </summary>
        public String Generate_Time { get; set; }

        /// <summary>
        /// 组建立者编号
        /// </summary>
        public String Generate_User_ID { get; set; }

        /// <summary>
        /// 组建立者姓名
        /// </summary>
        public String Generate_Username { get; set; }

        /// <summary>
        /// 组信息更新时间
        /// </summary>
        public String Modify_Time { get; set; }

        /// <summary>
        /// 组更新者编号
        /// </summary>
        public String Modify_User_ID { get; set; }

        /// <summary>
        /// 组更新者姓名
        /// </summary>
        public String Modify_Username { get; set; }
    }
}
