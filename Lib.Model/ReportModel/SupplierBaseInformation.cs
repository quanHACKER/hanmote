﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 供应商基本信息VO操作类
    /// </summary>
    public class SupplierBaseInformation
    {
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Supplier_ID { get; set; }

        /// <summary>
        /// 供应商登记名称
        /// </summary>
        public string Supplier_LoginName { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Supplier_Name { get; set; }

        /// <summary>
        /// 供应商企业性质
        /// </summary>
        public string Enterprise { get; set; }

        /// <summary>
        /// 供应商所属国家
        /// </summary>
        public string Nation { get; set; }

        /// <summary>
        /// 供应商详细地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 登记时间
        /// </summary>
        public string Register_Time { get; set; }

        /// <summary>
        /// 法人代表
        /// </summary>
        public string Representative { get; set; }

        /// <summary>
        /// 供应商开户银行
        /// </summary>
        public string Bank_Name { get; set; }

        /// <summary>
        /// 供应商银行账号
        /// </summary>
        public string Bank_Account { get; set; }

        /// <summary>
        /// 是否是上市公司
        /// </summary>
        public string IsListedCompany { get; set; }

        /// <summary>
        /// 股票代码
        /// </summary>
        public string Stock_code { get; set; }

        /// <summary>
        /// 公司固话
        /// </summary>
        public string Payer_No { get; set; }

        /// <summary>
        /// 公司传真
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// 公司网址
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// 公司邮编
        /// </summary>
        public string Zip_Code { get; set; }
    }
}
