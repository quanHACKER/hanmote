﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class PRSupplier
    {
        private string pR_ID;

        public string PR_ID
        {
            get { return pR_ID; }
            set { pR_ID = value; }
        }

        private string material_ID;

        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string supplier_ID;

        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private int number;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }

        private bool finished_Mark;

        public bool Finished_Mark
        {
            get { return finished_Mark; }
            set { finished_Mark = value; }
        }
    }
}
