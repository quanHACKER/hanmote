﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class BidMaterial
    {
        //招标编号
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        //物料编号
        private string materialId;

        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        //物料名称
        private string materialName;

        public string MaterialName
        {
            get { return materialName; }
            set { materialName = value; }
        }

        //购买数量
        private int number;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }

        //单位
        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        //交货日期
        private DateTime deliveryTime;

        public DateTime DeliveryTime
        {
            get { return deliveryTime; }
            set { deliveryTime = value; }
        }


    }
}
