﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class ConditionType
    {
        private string conditionId;
        /// <summary>
        /// 条件编号
        /// </summary>
        public string ConditionId
        {
            get { return conditionId; }
            set { conditionId = value; }
        }

        private string conditionName;
        /// <summary>
        /// 条件名称
        /// </summary>
        public string ConditionName
        {
            get { return conditionName; }
            set { conditionName = value; }
        }

        private string conditionCategory;
        /// <summary>
        /// 定价类别
        /// </summary>
        public string ConditionCategory
        {
            get { return conditionCategory; }
            set { conditionCategory = value; }
        }

        private string calType;
        /// <summary>
        /// 计算类型
        /// </summary>
        public string CalType
        {
            get { return calType; }
            set { calType = value; }
        }


        private string roundType;
        /// <summary>
        /// 四舍五入
        /// </summary>
        public string RoundType
        {
            get { return roundType; }
            set { roundType = value; }
        }

        private string pn;
        /// <summary>
        /// 正负
        /// </summary>
        public string Pn
        {
            get { return pn; }
            set { pn = value; }
        }

        private string description;
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

    }
}
