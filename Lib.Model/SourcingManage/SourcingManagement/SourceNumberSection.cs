﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class SourceNumberSection
    {
        private string sourceId;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }

        private string supplierId;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string materialId;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private int startNumber;
        /// <summary>
        /// 开始数量
        /// </summary>
        public int StartNumber
        {
            get { return startNumber; }
            set { startNumber = value; }
        }

        private int endNumber;
        /// <summary>
        /// 结束数量
        /// </summary>
        public int EndNumber
        {
            get { return endNumber; }
            set { endNumber = value; }
        }

        private float num;
        /// <summary>
        /// 区间值
        /// </summary>
        public float Num
        {
            get { return num; }
            set { num = value; }
        }
    }
}
