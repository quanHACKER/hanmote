﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class BidPartner
    {
        //招标编号
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        //请求者编号
        private string requesterId;

        public string RequesterId
        {
            get { return requesterId; }
            set { requesterId = value; }
        }

        //请求者名称
        private string requesterName;

        public string RequesterName
        {
            get { return requesterName; }
            set { requesterName = value; }
        }

        //收货人编号
        private string receiverId;

        public string ReceiverId
        {
            get { return receiverId; }
            set { receiverId = value; }
        }

        //收货人名称
        private string receiverName;

        public string ReceiverName
        {
            get { return receiverName; }
            set { receiverName = value; }
        }

        //交货点编号
        private string dropPointId;

        public string DropPointId
        {
            get { return dropPointId; }
            set { dropPointId = value; }
        }

        //交货点名称
        private string dropPointName;

        public string DropPointName
        {
            get { return dropPointName; }
            set { dropPointName = value; }
        }

        //评审专家编号
        private string reviewerId;

        public string ReviewerId
        {
            get { return reviewerId; }
            set { reviewerId = value; }
        }

        //评审专家名称
        private string reviewerName;

        public string ReviewerName
        {
            get { return reviewerName; }
            set { reviewerName = value; }
        }

        //开标人编号
        private string callerId;

        public string CallerId
        {
            get { return callerId; }
            set { callerId = value; }
        }

        //开标人名称
        private string callerName;

        public string CallerName
        {
            get { return callerName; }
            set { callerName = value; }
        }

    }
}
