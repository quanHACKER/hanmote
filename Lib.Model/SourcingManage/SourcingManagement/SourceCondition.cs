﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class SourceCondition
    {
        private string sourceId;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }

        private string supplierId;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string materialId;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private string conditionId;
        /// <summary>
        /// 条件类型编号
        /// </summary>
        public string ConditionId
        {
            get { return conditionId; }
            set { conditionId = value; }
        }

        private float num;
        /// <summary>
        /// 条件类型值
        /// </summary>
        public float Num
        {
            get { return num; }
            set { num = value; }
        }
    }
}
