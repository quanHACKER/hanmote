﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class TransactCost
    {
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        private string supplierId;

        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string materialId;

        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private string materialName;

        public string MaterialName
        {
            get { return materialName; }
            set { materialName = value; }
        }

        private int count;

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        private float targetPrice;

        public float TargetPrice
        {
            get { return targetPrice; }
            set { targetPrice = value; }
        }

        private float targetTotal;

        public float TargetTotal
        {
            get { return targetTotal; }
            set { targetTotal = value; }
        }

        private float supplierPrice;

        public float SupplierPrice
        {
            get { return supplierPrice; }
            set { supplierPrice = value; }
        }

        private float supplierTotal;

        public float SupplierTotal
        {
            get { return supplierTotal; }
            set { supplierTotal = value; }
        }

        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
