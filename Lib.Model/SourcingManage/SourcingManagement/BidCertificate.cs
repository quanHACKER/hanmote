﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    //招标凭证
    public class BidCertificate
    {
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        private string shortBidText;

        public string ShortBidText
        {
            get { return shortBidText; }
            set { shortBidText = value; }
        }

        private string approvalComment;

        public string ApprovalComment
        {
            get { return approvalComment; }
            set { approvalComment = value; }
        }

        private string longBidText;

        public string LongBidText
        {
            get { return longBidText; }
            set { longBidText = value; }
        }

    }
}
