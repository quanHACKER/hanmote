﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class BidWeightScore
    {
        private string bidId;
        /// <summary>
        /// 招标编号
        /// </summary>
        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        private string weightScoreName;
        /// <summary>
        /// 评估标准
        /// </summary>
        public string WeightScoreName
        {
            get { return weightScoreName; }
            set { weightScoreName = value; }
        }

        private int weightScoreNum;
        /// <summary>
        /// 评估分
        /// </summary>
        public int WeightScoreNum
        {
            get { return weightScoreNum; }
            set { weightScoreNum = value; }
        }


    }
}
