﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 寻源选项类
    /// </summary>
    public class SourceItem
    {
        private string item_ID;
        /// <summary>
        /// 子项编号
        /// </summary>
        public string Item_ID
        {
            get { return item_ID; }
            set { item_ID = value; }
        }


        private string source_ID;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private int state;
        /// <summary>
        /// 状态
        /// </summary>
        public int State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
