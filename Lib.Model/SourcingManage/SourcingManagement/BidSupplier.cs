﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class BidSupplier
    {
        //招标编号
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        //供应商编号
        private string supplierId;

        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        //供应商名称
        private string supplierName;

        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }

        //供应商评级
        private string classificationResult;

        public string ClassificationResult
        {
            get { return classificationResult; }
            set { classificationResult = value; }
        }


        //联系人
        private string contact;

        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        //邮箱
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        //地址
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        //状态
        private int state;

        public int State
        {
            get { return state; }
            set { state = value; }
        }


    }
}
