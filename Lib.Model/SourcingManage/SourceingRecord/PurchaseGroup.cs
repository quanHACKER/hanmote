﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourceingRecord
{
    public class PurchaseGroup
    {
        private string purchaseGroupId;

        public string PurchaseGroupId
        {
            get { return purchaseGroupId; }
            set { purchaseGroupId = value; }
        }

        private string purchaseGroupName;

        public string PurchaseGroupName
        {
            get { return purchaseGroupName; }
            set { purchaseGroupName = value; }
        }

        private string materialType;

        public string MaterialType
        {
            get { return materialType; }
            set { materialType = value; }
        }

        private string telephone;

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        private string fax;

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }


    }
}
