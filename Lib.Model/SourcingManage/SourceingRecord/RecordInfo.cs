﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourceingRecord
{
    public class RecordInfo
    {
        private string recordInfoId;
        /// <summary>
        /// 信息记录编号
        /// </summary>
        public string RecordInfoId
        {
            get { return recordInfoId; }
            set { recordInfoId = value; }
        }

        private string supplierId;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string supplierName;
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }

        private string purchaseOrg;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string PurchaseOrg
        {
            get { return purchaseOrg; }
            set { purchaseOrg = value; }
        }

        private string purchaseGroup;
        /// <summary>
        /// 采购组
        /// </summary>
        public string PurchaseGroup
        {
            get { return purchaseGroup; }
            set { purchaseGroup = value; }
        }

        private string materialId;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private string materialName;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName
        {
            get { return materialName; }
            set { materialName = value; }
        }

        private string factoryId;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string FactoryId
        {
            get { return factoryId; }
            set { factoryId = value; }
        }

        private string stockId;
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string StockId
        {
            get { return stockId; }
            set { stockId = value; }
        }

        private int demandCount;
        /// <summary>
        /// 购买数量
        /// </summary>
        public int DemandCount
        {
            get { return demandCount; }
            set { demandCount = value; }
        }

        private float netPrice;
        /// <summary>
        /// 净价
        /// </summary>
        public float NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }

        private int fromType;
        /// <summary>
        /// 来源方式
        /// </summary>
        public int FromType
        {
            get { return fromType; }
            set { fromType = value; }
        }

        private string fromId;
        /// <summary>
        /// 来源编号
        /// </summary>
        public string FromId
        {
            get { return fromId; }
            set { fromId = value; }
        }

        private string paymentClause;
        /// <summary>
        /// 支付条款
        /// </summary>
        public string PaymentClause
        {
            get { return paymentClause; }
            set { paymentClause = value; }
        }

        private string tradeClause;
        /// <summary>
        /// 交货条件
        /// </summary>
        public string TradeClause
        {
            get { return tradeClause; }
            set { tradeClause = value; }
        }

        private string priceDetermineId;
        /// <summary>
        /// 价格来源编号
        /// </summary>
        public string PriceDetermineId
        {
            get { return priceDetermineId; }
            set { priceDetermineId = value; }
        }

        private string contactState;
        /// <summary>
        /// 框架协议状态
        /// </summary>
        public string ContactState
        {
            get { return contactState; }
            set { contactState = value; }
        }

        private DateTime creatTime;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatTime
        {
            get { return creatTime; }
            set { creatTime = value; }
        }

        private DateTime startTime;
        /// <summary>
        /// 有效期开始时间
        /// </summary>
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }


        private DateTime endTime;
        /// <summary>
        /// 有效期截止时间
        /// </summary>
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
    }
}
