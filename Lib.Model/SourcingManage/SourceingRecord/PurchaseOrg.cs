﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourceingRecord
{
    public class PurchaseOrg
    {
        private string purchaseOrgId;
        /// <summary>
        /// 采购组织编号
        /// </summary>
        public string PurchaseOrgId
        {
            get { return purchaseOrgId; }
            set { purchaseOrgId = value; }
        }

        private string purchaseOrgName;
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string PurchaseOrgName
        {
            get { return purchaseOrgName; }
            set { purchaseOrgName = value; }
        }

    }
}
