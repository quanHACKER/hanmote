﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourceingRecord
{
    public class MaterialGroup
    {
        private string materialGroupId;

        public string MaterialGroupId
        {
            get { return materialGroupId; }
            set { materialGroupId = value; }
        }

        private string materialGroupName;

        public string MaterialGroupName
        {
            get { return materialGroupName; }
            set { materialGroupName = value; }
        }
    }
}
