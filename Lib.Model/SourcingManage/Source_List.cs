﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage
{
    /// <summary>
    /// 货源清单类
    /// </summary>
    public class Source_List
    {
        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private DateTime effective_Date;
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime Effective_Date
        {
            get { return effective_Date; }
            set { effective_Date = value; }
        }

        private DateTime expiration_Date;
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime Expiration_Date
        {
            get { return expiration_Date; }
            set { expiration_Date = value; }
        }

        private DateTime create_Date;
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime Create_Date
        {
            get { return create_Date; }
            set { create_Date = value; }
        }

        private string department;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        private string buyer_Name;
        /// <summary>
        /// 采购组
        /// </summary>
        public string Buyer_Name
        {
            get { return buyer_Name; }
            set { buyer_Name = value; }
        }

        private string agreement_ID;
        /// <summary>
        /// 协议编号
        /// </summary>
        public string Agreement_ID
        {
            get { return agreement_ID; }
            set { agreement_ID = value; }
        }

        private string state;
        /// <summary>
        /// 货源状态
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private string spare1;
        /// <summary>
        /// 备用字段1
        /// </summary>
        public string Spare1
        {
            get { return spare1; }
            set { spare1 = value; }
        }

        private string spare2;
        /// <summary>
        /// 备用字段2
        /// </summary>
        public string Spare2
        {
            get { return spare2; }
            set { spare2 = value; }
        }

        private string spare3;
        /// <summary>
        /// 备用字段3
        /// </summary>
        public string Spare3
        {
            get { return spare3; }
            set { spare3 = value; }
        }

    }
}
