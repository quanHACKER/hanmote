﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage
{
    /// <summary>
    /// 询价单表头类
    /// </summary>
    public class Inquiry_Table
    {
        private string inquiry_ID;
        /// <summary>
        /// 寻源单号
        /// </summary>
        public string Inquiry_ID
        {
            get { return inquiry_ID; }
            set { inquiry_ID = value; }
        }

        private string inquiry_Name;
        /// <summary>
        /// 寻源名称
        /// </summary>
        public string Inquiry_Name
        {
            get { return inquiry_Name; }
            set { inquiry_Name = value; }
        }

        private string transaction_Type;
        /// <summary>
        /// 交易类型
        /// </summary>
        public string Transaction_Type
        {
            get { return transaction_Type; }
            set { transaction_Type = value; }
        }

        private string buyer_Name;
        /// <summary>
        /// 采购组
        /// </summary>
        public string Buyer_Name
        {
            get { return buyer_Name; }
            set { buyer_Name = value; }
        }

        private DateTime inquiry_Time;
        /// <summary>
        /// 询价日期
        /// </summary>
        public DateTime Inquiry_Time
        {
            get { return inquiry_Time; }
            set { inquiry_Time = value; }
        }

        private DateTime offer_Time;
        /// <summary>
        /// 报价日期
        /// </summary>
        public DateTime Offer_Time
        {
            get { return offer_Time; }
            set { offer_Time = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string state;
        /// <summary>
        /// 字段1
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private string inquiry_Path;
        /// <summary>
        /// 字段2
        /// </summary>
        public string Inquiry_Path
        {
            get { return inquiry_Path; }
            set { inquiry_Path = value; }
        }

        private string example3;
        /// <summary>
        /// 字段3
        /// </summary>
        public string Example3
        {
            get { return example3; }
            set { example3 = value; }
        }

    }
}
