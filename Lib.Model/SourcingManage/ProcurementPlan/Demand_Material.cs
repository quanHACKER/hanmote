﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.ProcurementPlan
{
    /// <summary>
    /// 需求计划对应的物料
    /// </summary>
    public class Demand_Material
    {
        private string demand_ID;
        /// <summary>
        /// 需求单号
        /// </summary>
        public string Demand_ID
        {
            get { return demand_ID; }
            set { demand_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private string material_Group;
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        private int demand_Count;
        /// <summary>
        /// 需求数量
        /// </summary>
        public int Demand_Count
        {
            get { return demand_Count; }
            set { demand_Count = value; }
        }

        private string measurement;
        /// <summary>
        /// 计量单位
        /// </summary>
        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }

        private string purchase_Price;
        /// <summary>
        /// 采购价格（不确定）
        /// </summary>
        public string Purchase_Price
        {
            get { return purchase_Price; }
            set { purchase_Price = value; }
        }

        private string stock_ID;
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }


        private string factory_ID;
        /// <summary>
        /// 工厂编号 
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private DateTime deliveryStartTime;
        /// <summary>
        /// 交货开始时间
        /// </summary>
        public DateTime DeliveryStartTime
        {
            get { return deliveryStartTime; }
            set { deliveryStartTime = value; }
        }

        private DateTime deliveryEndTime;
        /// <summary>
        /// 交货结束时间
        /// </summary>
        public DateTime DeliveryEndTime
        {
            get { return deliveryEndTime; }
            set { deliveryEndTime = value; }
        }

        

    }
}
