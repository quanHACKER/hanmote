﻿using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MMClient.supplyManage
{
    public partial class SupplyInfoForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SupplyInfoForm()
        {
            InitializeComponent();
        }

         List<String> getSupplierId() {
            List<String> supplierIdList = new List<string>();
            DataTable dt =DBHelper.ExecuteQueryDT("select distinct supplierId from TabSupplyInfo ");

            for (int i = 0; i < dt.Rows.Count; i++) {
                supplierIdList.Add(dt.Rows[i][0].ToString());
            }
            return supplierIdList;
        }

        private void SupplyInfoForm_Load(object sender, EventArgs e)
        {
            this.CB_supplierId.DataSource = getSupplierId();
        }

        private void CB_supplierId_SelectedValueChanged(object sender, EventArgs e)
        {
            this.dgv_SupplyInfo.DataSource = getSupplyInfo();
            this.LB_SuppName.Text = "供应商" + this.CB_supplierId.Text;
        }

        DataTable getSupplyInfo() {
            String sql = "select distinct mtGroupName,mtGroupId,mtId,mtName,ModifyTime from TabSupplyInfo where supplierId='" + this.CB_supplierId.Text+"' ";
            return DBHelper.ExecuteQueryDT(sql);

        }

    }
}
