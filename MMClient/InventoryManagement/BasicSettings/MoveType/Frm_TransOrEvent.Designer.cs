﻿namespace MMClient.InventoryManagement
{
    partial class Frm_TransOrEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Update = new System.Windows.Forms.Button();
            this.dgv_TransDetails = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.Col_TransId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TransName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TransActivate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_TransDetails)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(15, 45);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(61, 24);
            this.btn_Update.TabIndex = 10;
            this.btn_Update.Text = "激活";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // dgv_TransDetails
            // 
            this.dgv_TransDetails.AllowUserToAddRows = false;
            this.dgv_TransDetails.AllowUserToResizeColumns = false;
            this.dgv_TransDetails.AllowUserToResizeRows = false;
            this.dgv_TransDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_TransDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_TransDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_TransDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_TransDetails.ColumnHeadersHeight = 25;
            this.dgv_TransDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_TransDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_TransId,
            this.Col_TransName,
            this.Col_TransActivate});
            this.dgv_TransDetails.ContextMenuStrip = this.contextMenuStrip;
            this.dgv_TransDetails.EnableHeadersVisualStyles = false;
            this.dgv_TransDetails.Location = new System.Drawing.Point(15, 85);
            this.dgv_TransDetails.MultiSelect = false;
            this.dgv_TransDetails.Name = "dgv_TransDetails";
            this.dgv_TransDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_TransDetails.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_TransDetails.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_TransDetails.RowTemplate.Height = 23;
            this.dgv_TransDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_TransDetails.Size = new System.Drawing.Size(392, 384);
            this.dgv_TransDetails.TabIndex = 12;
            this.dgv_TransDetails.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_TransDetails_CellMouseEnter);
            this.dgv_TransDetails.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_TransDetails_CellMouseLeave);
            this.dgv_TransDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_TransDetails_RowPostPaint);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_Update});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(101, 26);
            // 
            // ToolStripMenuItem_Update
            // 
            this.ToolStripMenuItem_Update.Name = "ToolStripMenuItem_Update";
            this.ToolStripMenuItem_Update.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_Update.Text = "激活";
            this.ToolStripMenuItem_Update.Click += new System.EventHandler(this.ToolStripMenuItem_Update_Click);
            // 
            // Col_TransId
            // 
            this.Col_TransId.HeaderText = "事务编码";
            this.Col_TransId.Name = "Col_TransId";
            this.Col_TransId.ReadOnly = true;
            this.Col_TransId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TransName
            // 
            this.Col_TransName.HeaderText = "事务名称";
            this.Col_TransName.Name = "Col_TransName";
            this.Col_TransName.ReadOnly = true;
            this.Col_TransName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_TransName.Width = 150;
            // 
            // Col_TransActivate
            // 
            this.Col_TransActivate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col_TransActivate.HeaderText = "是否激活";
            this.Col_TransActivate.Name = "Col_TransActivate";
            this.Col_TransActivate.ReadOnly = true;
            this.Col_TransActivate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Frm_TransOrEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 510);
            this.Controls.Add(this.dgv_TransDetails);
            this.Controls.Add(this.btn_Update);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TransOrEvent";
            this.Text = "移动类型->事务/事件";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_TransDetails)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.DataGridView dgv_TransDetails;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Update;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TransId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TransName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Col_TransActivate;
    }
}