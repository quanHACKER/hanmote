﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Common.CommonUtils;
using Lib.Model.StockModel;

namespace MMClient.InventoryManagement
{
    public partial class Frm_FindMoveType : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        MoveTypeInfoBLL moveTypeInfoBLL = new MoveTypeInfoBLL();
        public Frm_FindMoveType()
        {
            InitializeComponent();
            this.dgv_MoveTypeDetails.TopLeftHeaderCell.Value = "行号";
            this.initialData();
        }

        /// <summary>
        /// 初始化时绑定数据;
        /// </summary>
        private void initialData()
        {
            try
            {
                List<MoveTypeExtendModel> moveTypeExtendModleList = moveTypeInfoBLL.findALLMoveTypeInfo();
                if (moveTypeExtendModleList != null)
                {
                    int i = 0;
                    foreach (MoveTypeExtendModel moveTypeExtendModel in moveTypeExtendModleList)
                    {
                        this.dgv_MoveTypeDetails.Rows.Add(1);
                        dgv_MoveTypeDetails.Rows[i].Cells["Col_MT_ID"].Value = moveTypeExtendModel.MT_ID;
                        dgv_MoveTypeDetails.Rows[i].Cells["Col_MT_Name"].Value = moveTypeExtendModel.MT_Name;
                        dgv_MoveTypeDetails.Rows[i].Cells["Col_MT_TransactionDtb"].Value = moveTypeExtendModel.MT_TransactionDtb;
                        i++;
                    }
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MoveTypeDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MoveTypeDetails_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_MoveTypeDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_MoveTypeDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }
        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MoveTypeDetails_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_MoveTypeDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_MoveTypeDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 新增移动类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_New_Click(object sender, EventArgs e)
        {
            new Frm_NewMoveType(this).ShowDialog();
        }

        public void updateDGV(MoveTypeModel moveTypeModel)
        {
            //给dgv添加一行，添加在最后
            this.dgv_MoveTypeDetails.Rows.Add(1);
            //获取dgv最后一行的位置，然后插入数据
            dgv_MoveTypeDetails.Rows[this.dgv_MoveTypeDetails.Rows.Count - 1].Cells["Col_MT_ID"].Value = moveTypeModel.MT_ID;
            dgv_MoveTypeDetails.Rows[this.dgv_MoveTypeDetails.Rows.Count - 1].Cells["Col_MT_Name"].Value = moveTypeModel.MT_Name;
            dgv_MoveTypeDetails.Rows[this.dgv_MoveTypeDetails.Rows.Count - 1].Cells["Col_MT_TransactionDtb"].Value = moveTypeModel.MT_TransactionDtb;
        }

        public void updateDGV(MoveTypeModel moveTypeModel, DataGridViewRow currentRow)
        {
            currentRow.Cells["Col_MT_Name"].Value = moveTypeModel.MT_Name;
            currentRow.Cells["Col_MT_TransactionDtb"].Value = moveTypeModel.MT_TransactionDtb;
        }

        /// <summary>
        /// 修改移动类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Update_Click(object sender, EventArgs e)
        {
            //获取当前鼠标选中移动类型的ID
            //string moveTypeId = dgv_MoveTypeDetails.CurrentRow.Cells["Col_MT_ID"].Value.ToString();
            new Frm_UpdateMoveType(this, dgv_MoveTypeDetails.CurrentRow).ShowDialog();
        }

        /// <summary>
        /// 删除移动类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("您确定删除吗？", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (DialogResult.OK == dr)
            {
                string moveTypeId = dgv_MoveTypeDetails.CurrentRow.Cells["Col_MT_ID"].Value.ToString();
                try
                {
                    //执行删除语句
                    int executeResult = moveTypeInfoBLL.deleteMoveTypeInfoById(moveTypeId);
                    if (executeResult > 0)
                    {
                        //数据删除成功显示消息
                        MessageBox.Show("数据删除成功", "信息提示");
                        //在不执行查询操作的情况下，在dgv中迅速显示;
                        this.dgv_MoveTypeDetails.Rows.Remove(this.dgv_MoveTypeDetails.CurrentRow);
                    }
                    else
                    {
                        MessageBox.Show("数据删除失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowError(ex.Message);
                }
            }
            else
            { 
                //点击取消按钮时发生
            }
        }

        /// <summary>
        /// 右击->新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            this.btn_New_Click(sender,e);
        }

        /// <summary>
        /// 右击->删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Delete_Click(object sender, EventArgs e)
        {
            this.btn_Delete_Click(sender,e);
        }

        /// <summary>
        /// 右击->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Update_Click(object sender, EventArgs e)
        {
            this.btn_Update_Click(sender,e);
        } 
    }
}
