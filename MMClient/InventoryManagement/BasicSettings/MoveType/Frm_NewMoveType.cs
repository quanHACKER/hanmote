﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.StockModel;
using Lib.Bll.StockBLL;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_NewMoveType : Form
    {

        MoveTypeInfoBLL moveTypeInfoBLL = new MoveTypeInfoBLL();
        Frm_FindMoveType frm_FindMoveType = new Frm_FindMoveType();
        StockTransInfoBLL stockTransInfoBLL = new StockTransInfoBLL();

        public Frm_NewMoveType()
            :this(null)
        {
            //无参构造函数
        }

        public Frm_NewMoveType(Frm_FindMoveType frm_FindMoveType)
        {
            InitializeComponent();
            this.frm_FindMoveType = frm_FindMoveType;
            this.initialData();
        }

        private void initialData()
        {
            try
            {
                List<StockTransExtendModel> stockTransExtendModelList = stockTransInfoBLL.findAllStockTransInfo();
                if (stockTransExtendModelList != null)
                {
                    foreach (StockTransExtendModel stockTransExtendModel in stockTransExtendModelList)
                    {
                        if (stockTransExtendModel.StockTrans_Activate)
                        {
                            string transOrEventName = stockTransExtendModel.StockTrans_ID + " " + stockTransExtendModel.StockTrans_Name;
                            this.cbb_TransactionDtb.Items.Add(transOrEventName);
                        }
                    }
                    this.cbb_TransactionDtb.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            MoveTypeModel moveTypeModel = new MoveTypeModel();
            string MT_ID = this.txt_MoveTypeId.Text.Trim();
            string MT_Name = this.txt_MoveTypeMeaning.Text.Trim();
            string MT_Reason = this.txt_MoveTypeReason.Text.Trim();
            Boolean MT_Valid = true;
            string MT_TransactionDtb = this.cbb_TransactionDtb.Text.Trim().Substring(0, 3);
            if ("".Equals(MT_ID) || "".Equals(MT_Name))
            {
                MessageBox.Show("请将必填信息填写完整", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                moveTypeModel.MT_ID = MT_ID;
                moveTypeModel.MT_Name = MT_Name;
                moveTypeModel.MT_Reason = MT_Reason;
                moveTypeModel.MT_Valid = MT_Valid;
                moveTypeModel.MT_TransactionDtb = MT_TransactionDtb;
                try
                {
                    //执行插入语句，并返回结果
                    int queryResult = moveTypeInfoBLL.insertNewMoveTypeInfo(moveTypeModel);
                    if (queryResult > 0)
                    {
                        //数据插入成功显示消息
                        MessageBox.Show("数据插入成功","信息提示");
                        //返回到父窗体的代码
                        this.frm_FindMoveType.updateDGV(moveTypeModel);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("数据插入失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowError(ex.Message);
                }
            }
        }
    }
}
