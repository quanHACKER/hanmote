﻿namespace MMClient.InventoryManagement
{
    partial class Frm_StockState
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_DisplayALL = new System.Windows.Forms.Button();
            this.dgv_StockStateDetails = new System.Windows.Forms.DataGridView();
            this.Col_StockState_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockState_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_New = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Update = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockStateDetails)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_DisplayALL
            // 
            this.btn_DisplayALL.Location = new System.Drawing.Point(213, 46);
            this.btn_DisplayALL.Name = "btn_DisplayALL";
            this.btn_DisplayALL.Size = new System.Drawing.Size(75, 23);
            this.btn_DisplayALL.TabIndex = 17;
            this.btn_DisplayALL.Text = "显示全部";
            this.btn_DisplayALL.UseVisualStyleBackColor = true;
            this.btn_DisplayALL.Click += new System.EventHandler(this.btn_DisplayALL_Click);
            // 
            // dgv_StockStateDetails
            // 
            this.dgv_StockStateDetails.AllowUserToAddRows = false;
            this.dgv_StockStateDetails.AllowUserToResizeColumns = false;
            this.dgv_StockStateDetails.AllowUserToResizeRows = false;
            this.dgv_StockStateDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_StockStateDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_StockStateDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_StockStateDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgv_StockStateDetails.ColumnHeadersHeight = 25;
            this.dgv_StockStateDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_StockStateDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_StockState_Id,
            this.Col_Flag,
            this.Col_StockState_Name});
            this.dgv_StockStateDetails.EnableHeadersVisualStyles = false;
            this.dgv_StockStateDetails.Location = new System.Drawing.Point(13, 92);
            this.dgv_StockStateDetails.MultiSelect = false;
            this.dgv_StockStateDetails.Name = "dgv_StockStateDetails";
            this.dgv_StockStateDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_StockStateDetails.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgv_StockStateDetails.RowTemplate.Height = 23;
            this.dgv_StockStateDetails.RowTemplate.ReadOnly = true;
            this.dgv_StockStateDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_StockStateDetails.Size = new System.Drawing.Size(549, 464);
            this.dgv_StockStateDetails.TabIndex = 16;
            this.dgv_StockStateDetails.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_StockStateDetails_CellMouseDown);
            this.dgv_StockStateDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_StockStateDetails_RowPostPaint);
            // 
            // Col_StockState_Id
            // 
            this.Col_StockState_Id.DataPropertyName = "Id";
            this.Col_StockState_Id.HeaderText = "编号";
            this.Col_StockState_Id.Name = "Col_StockState_Id";
            this.Col_StockState_Id.ReadOnly = true;
            this.Col_StockState_Id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockState_Id.ToolTipText = "编号";
            this.Col_StockState_Id.Width = 120;
            // 
            // Col_Flag
            // 
            this.Col_Flag.HeaderText = "是否有效";
            this.Col_Flag.Name = "Col_Flag";
            this.Col_Flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_StockState_Name
            // 
            this.Col_StockState_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col_StockState_Name.DataPropertyName = "Type";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Col_StockState_Name.DefaultCellStyle = dataGridViewCellStyle14;
            this.Col_StockState_Name.HeaderText = "名称";
            this.Col_StockState_Name.Name = "Col_StockState_Name";
            this.Col_StockState_Name.ReadOnly = true;
            this.Col_StockState_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockState_Name.ToolTipText = "名称";
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(81, 45);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(61, 24);
            this.btn_Update.TabIndex = 15;
            this.btn_Update.Text = "修改";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_New
            // 
            this.btn_New.Location = new System.Drawing.Point(16, 45);
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(61, 24);
            this.btn_New.TabIndex = 13;
            this.btn_New.Text = "新增";
            this.btn_New.UseVisualStyleBackColor = true;
            this.btn_New.Click += new System.EventHandler(this.btn_New_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(146, 45);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(61, 24);
            this.btn_Delete.TabIndex = 14;
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_New,
            this.ToolStripMenuItem_Delete,
            this.ToolStripMenuItem_Update});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(153, 92);
            // 
            // ToolStripMenuItem_New
            // 
            this.ToolStripMenuItem_New.Name = "ToolStripMenuItem_New";
            this.ToolStripMenuItem_New.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItem_New.Text = "新增";
            this.ToolStripMenuItem_New.Click += new System.EventHandler(this.ToolStripMenuItem_New_Click);
            // 
            // ToolStripMenuItem_Delete
            // 
            this.ToolStripMenuItem_Delete.Name = "ToolStripMenuItem_Delete";
            this.ToolStripMenuItem_Delete.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItem_Delete.Text = "删除";
            this.ToolStripMenuItem_Delete.Click += new System.EventHandler(this.ToolStripMenuItem_Delete_Click);
            // 
            // ToolStripMenuItem_Update
            // 
            this.ToolStripMenuItem_Update.Name = "ToolStripMenuItem_Update";
            this.ToolStripMenuItem_Update.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItem_Update.Text = "修改";
            this.ToolStripMenuItem_Update.Click += new System.EventHandler(this.ToolStripMenuItem_Update_Click);
            // 
            // Frm_StockState
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 599);
            this.Controls.Add(this.btn_DisplayALL);
            this.Controls.Add(this.dgv_StockStateDetails);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_New);
            this.Controls.Add(this.btn_Delete);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_StockState";
            this.Text = "库存状态设置";
            this.Load += new System.EventHandler(this.Frm_StockState_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockStateDetails)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_DisplayALL;
        private System.Windows.Forms.DataGridView dgv_StockStateDetails;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_New;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockState_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockState_Name;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Delete;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Update;
    }
}