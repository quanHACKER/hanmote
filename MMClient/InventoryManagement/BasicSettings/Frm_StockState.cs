﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;

namespace MMClient.InventoryManagement
{
    public partial class Frm_StockState : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        /// <summary>
        /// 操作库存类型的业务逻辑层
        /// </summary>
        StockStateBLL stockStateBLL = new StockStateBLL();

        public Frm_StockState()
        {
            InitializeComponent();

            this.dgv_StockStateDetails.TopLeftHeaderCell.Value = "序号";
        }

        /// <summary>
        /// 按钮->新建
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_New_Click(object sender, EventArgs e)
        {
            Frm_NewStockState frm_NewStockState = new Frm_NewStockState(1);
            DialogResult result = frm_NewStockState.ShowDialog();
            if (DialogResult.OK == result)
            {
                //存放库存类型的模板类
                StockStateModel stockStateModel = new StockStateModel();
                stockStateModel.Id = frm_NewStockState.StateId;
                stockStateModel.State = frm_NewStockState.StateName;
                stockStateModel.Valid = 1; //初次创建的库存类型都是有效的
                stockStateModel.Create_ts = Convert.ToDateTime(DateTime.Now.ToLongDateString()); //库存类型创建时间
                //取得插入数据库的行数
                int influnceRows = stockStateBLL.insertStockState(stockStateModel);
                if (influnceRows > 0)
                {
                    this.updateDGV(stockStateModel);
                    MessageUtil.ShowTips("库存类型新增了一条");
                }
                else
                {
                    MessageUtil.ShowError("出错了!");
                }
            }
        }

        // <summary>
        /// 按钮->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Update_Click(object sender, EventArgs e)
        {
            //获取当前行
            DataGridViewRow currentRow = this.dgv_StockStateDetails.CurrentRow;
            if (currentRow == null)
            {
                MessageUtil.ShowTips("亲，没有可修改的数据哟!");
                return;
            }
            //获取库存类型ID
            string strStateId = currentRow.Cells["Col_StockState_Id"].Value.ToString();
            int intTypeId = Convert.ToInt32(strStateId);

            Frm_NewStockState frm_NewStockState = new Frm_NewStockState(2, intTypeId);
            DialogResult result = frm_NewStockState.ShowDialog();
            if (DialogResult.OK == result)
            {
                //存放库存类型的模板类
                StockStateModel stockStateModel = new StockStateModel();
                stockStateModel.Id = intTypeId;
                stockStateModel.State = frm_NewStockState.StateName;
                stockStateModel.Update_ts = Convert.ToDateTime(DateTime.Now.ToLongDateString()); //库存类型创建时间
                //取得插入数据库的行数
                int influnceRows = stockStateBLL.updateStockStateByStateId(stockStateModel);
                if (influnceRows > 0)
                {
                    this.updateDGV(stockStateModel, currentRow);
                    MessageUtil.ShowTips("库存类型修改成功");
                }
                else
                {
                    MessageUtil.ShowError("出错了!");
                }
            }
        }

        /// <summary>
        /// 按钮->删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            //获取当前行
            DataGridViewRow currentRow = this.dgv_StockStateDetails.CurrentRow;
            if (currentRow == null)
            {
                MessageUtil.ShowTips("亲，没有可删除的数据哟!");
                return;
            }
            //获取有效性
            string strValid = currentRow.Cells["Col_Flag"].Value.ToString();
            if (strValid.Equals("有效"))
            {
                DialogResult result = MessageBox.Show(this, "您确定要删除库存类型吗？", "警告信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    //获取库存类型ID
                    string stateId = currentRow.Cells["Col_StockState_Id"].Value.ToString();
                    int influence = stockStateBLL.deleteStockStateByStateId(Convert.ToInt32(stateId));
                    if (influence > 0)
                    {
                        currentRow.Cells["Col_Flag"].Value = "失效";
                    }
                    else
                    {
                        MessageUtil.ShowError("出错啦!");
                    }
                }
            }
            else
            {
                DialogResult result = MessageBox.Show(this, "您确定要恢复库存类型吗？", "警告信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    //获取复杂工单号
                    string stateId = currentRow.Cells["Col_StockState_Id"].Value.ToString();
                    int influence = stockStateBLL.restoreStockStateByStateId(Convert.ToInt32(stateId));
                    if (influence > 0)
                    {
                        currentRow.Cells["Col_Flag"].Value = "有效";
                    }
                    else
                    {
                        MessageUtil.ShowError("出错啦!");
                    }
                }
            }
        }

        /// <summary>
        /// 按钮->显示全部
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DisplayALL_Click(object sender, EventArgs e)
        {
            this.iniDGVData();
        }

        /// <summary>
        /// dgv->右键->菜单栏
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockStateDetails_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = this.dgv_StockStateDetails.CurrentRow;
                //获取有效性
                string strValid = currentRow.Cells["Col_Flag"].Value.ToString();
                if (strValid.Equals("有效"))
                {
                    this.ToolStripMenuItem_Delete.Text = "删除";
                }
                else
                {
                    this.ToolStripMenuItem_Delete.Text = "恢复";
                }
                this.contextMenuStrip.Show(MousePosition.X, MousePosition.Y);
            }
        }

        /// <summary>
        /// dgv->添加行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockStateDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_StockState_Load(object sender, EventArgs e)
        {
            this.btn_DisplayALL_Click(sender, e);
        }

        /// <summary>
        /// 当前行右键->新建
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            this.btn_New_Click(sender, e);
        }

        /// <summary>
        /// 当前行右键->删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Delete_Click(object sender, EventArgs e)
        {
            this.btn_Delete_Click(sender, e);
        }

        /// <summary>
        /// 当前行右键->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Update_Click(object sender, EventArgs e)
        {
            this.btn_Update_Click(sender, e);
        }

        #region 公共调用方法
        /// <summary>
        /// 初始化datagridview中的数据
        /// </summary>
        private void iniDGVData()
        {
            //先清空DGV中的数据
            this.dgv_StockStateDetails.Rows.Clear();

            List<StockStateModel> stockStateModelList = stockStateBLL.getAllStockState();
            if (stockStateModelList != null)
            {
                int i = 0;
                foreach (StockStateModel stockStateModel in stockStateModelList)
                {
                    this.dgv_StockStateDetails.Rows.Add(1);
                    dgv_StockStateDetails.Rows[i].Cells["Col_StockState_Id"].Value = stockStateModel.Id;
                    if (1 == stockStateModel.Valid)
                    {
                        dgv_StockStateDetails.Rows[i].Cells["Col_Flag"].Value = "有效";
                    }
                    else
                    {
                        dgv_StockStateDetails.Rows[i].Cells["Col_Flag"].Value = "无效";
                    }

                    dgv_StockStateDetails.Rows[i].Cells["Col_StockState_Name"].Value = stockStateModel.State;
                    i++;
                }
            }
            else
            {
                //MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 插入数据的同时更新DGV
        /// </summary>
        /// <param name="stockTypeModel"></param>
        private void updateDGV(StockStateModel stockStateModel)
        {
            //给dgv添加一行，添加在最后
            this.dgv_StockStateDetails.Rows.Add(1);
            //获取dgv最后一行的位置，然后插入数据
            dgv_StockStateDetails.Rows[this.dgv_StockStateDetails.Rows.Count - 1].Cells["Col_StockState_Id"].Value = stockStateModel.Id;
            dgv_StockStateDetails.Rows[this.dgv_StockStateDetails.Rows.Count - 1].Cells["Col_Flag"].Value = "有效";
            dgv_StockStateDetails.Rows[this.dgv_StockStateDetails.Rows.Count - 1].Cells["Col_StockState_Name"].Value = stockStateModel.State;
        }

        /// <summary>
        /// 修改操作,同时更新当前选中行的数据
        /// </summary>
        /// <param name="moveTypeModel"></param>
        /// <param name="currentRow"></param>
        public void updateDGV(StockStateModel stockStateModel, DataGridViewRow currentRow)
        {
            //修改当前行数据
            currentRow.Cells["Col_StockState_Name"].Value = stockStateModel.State;
        }
        #endregion

    }
}
