﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Model;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_NewStockState : Form
    {
        /// <summary>
        /// 操作库存类型的业务逻辑层
        /// </summary>
        StockStateBLL stockStateBLL = new StockStateBLL();

        /// <summary>
        /// 类别
        /// catagory=1：表示是新建
        /// catagory=2：表示是修改
        /// </summary>
        private int catagory;

        /// <summary>
        /// 类别
        /// catagory=1：表示是新建
        /// catagory=2：表示是修改
        /// </summary>
        public int Catagory
        {
            get { return catagory; }
            set { catagory = value; }
        }

        /// <summary>
        /// 库存类型Id
        /// </summary>
        private int stateId;

        /// <summary>
        /// 库存类型Id
        /// </summary>
        public int StateId
        {
            get { return stateId; }
            set { stateId = value; }
        }

        /// <summary>
        /// 库存类型名称
        /// </summary>
        private string stateName;

        /// <summary>
        /// 库存类型名称
        /// </summary>
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }

        /// <summary>
        /// 基础编号
        /// </summary>
        public const int BASE_ID = 1000;

        /// <summary>
        /// 无参构造
        /// 初始化窗体空间
        /// </summary>
        public Frm_NewStockState()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 类别参数
        /// </summary>
        /// <param name="catagory">标识新建还是修改</param>
        public Frm_NewStockState(int catagory, int stateId = 1000)
            :this()
        {
            this.catagory = catagory;
            this.stateId = stateId;
            this.intiCurrentFormControls(catagory);
        }

        /// <summary>
        /// 根据类别来初始化不同界面
        /// </summary>
        /// <param name="catagory"></param>
        private void intiCurrentFormControls(int catagory)
        {
            //catagory=1：表示是新建
            if (1 == catagory) 
            {
                this.Text = "新建库存类型";
                int latestStateId = stockStateBLL.getLatestStateId();
                int newStateId = this.getStateId(latestStateId);
                this.StateId = newStateId;
                this.txt_StateId.Text = newStateId.ToString();
            }
            //catagory=2：表示是修改
            if (2 == catagory)
            {
                this.txt_StateId.Text = this.stateId.ToString();
                this.Text = "修改库存类型";
            }
        }

        /// <summary>
        /// 生成新的EID
        /// </summary>
        /// <param name="eid"></param>
        /// <returns></returns>
        private int getStateId(int stateId)
        {
            if (stateId == 0)
            {
                return BASE_ID + 1;
            }
            else
            {
                return stateId + 1;
            }
        }

        /// <summary>
        /// 点击确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (this.checkInput())
            {
                this.StateId = Convert.ToInt32(this.txt_StateId.Text.Trim());
                this.StateName = this.txt_Name.Text.Trim();
                this.DialogResult = DialogResult.OK;
            }
            else 
            {
                return;
            }
        }

        /// <summary>
        /// 验证输入的合法性
        /// </summary>
        /// <returns></returns>
        private bool checkInput()
        {
            //输入是否合法标识
            bool validity = false;
            //库存类型名称
            string sStateName = this.txt_Name.Text.Trim();
            //库存类型编号
            string sStateId = this.txt_StateId.Text.Trim();
            int a = sStateId.Length;
            //输入合法化检查
            if (sStateId.Length <= 0 || sStateName.Length <= 0)
            {
                validity = false;
                MessageUtil.ShowError("请检查输入的合法性!");
            }
            else
            {
                validity = true;
            }
            return validity;
        }


        /// <summary>
        /// 点击取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
