﻿namespace MMClient.InventoryManagement
{
    partial class Frm_QueryDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Query = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.gb_ProjectData = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.lb_MoveType = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.lb_Supplier = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.lb_Batch = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lb_StockAddress = new System.Windows.Forms.Label();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.lb_Material = new System.Windows.Forms.Label();
            this.gb_TableHead = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lb_VoucherDate = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gb_ProjectData.SuspendLayout();
            this.gb_TableHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(16, 45);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(61, 24);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "查询";
            this.btn_Query.UseVisualStyleBackColor = true;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(83, 45);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(61, 24);
            this.btn_Cancel.TabIndex = 1;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(150, 45);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(61, 24);
            this.btn_Clear.TabIndex = 2;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            // 
            // gb_ProjectData
            // 
            this.gb_ProjectData.Controls.Add(this.textBox12);
            this.gb_ProjectData.Controls.Add(this.textBox11);
            this.gb_ProjectData.Controls.Add(this.lb_MoveType);
            this.gb_ProjectData.Controls.Add(this.textBox10);
            this.gb_ProjectData.Controls.Add(this.textBox9);
            this.gb_ProjectData.Controls.Add(this.lb_Supplier);
            this.gb_ProjectData.Controls.Add(this.textBox8);
            this.gb_ProjectData.Controls.Add(this.textBox7);
            this.gb_ProjectData.Controls.Add(this.lb_Batch);
            this.gb_ProjectData.Controls.Add(this.textBox6);
            this.gb_ProjectData.Controls.Add(this.textBox5);
            this.gb_ProjectData.Controls.Add(this.textBox4);
            this.gb_ProjectData.Controls.Add(this.textBox3);
            this.gb_ProjectData.Controls.Add(this.label5);
            this.gb_ProjectData.Controls.Add(this.label4);
            this.gb_ProjectData.Controls.Add(this.label6);
            this.gb_ProjectData.Controls.Add(this.label3);
            this.gb_ProjectData.Controls.Add(this.label2);
            this.gb_ProjectData.Controls.Add(this.label1);
            this.gb_ProjectData.Controls.Add(this.textBox2);
            this.gb_ProjectData.Controls.Add(this.textBox1);
            this.gb_ProjectData.Controls.Add(this.lb_StockAddress);
            this.gb_ProjectData.Controls.Add(this.lb_Factory);
            this.gb_ProjectData.Controls.Add(this.lb_Material);
            this.gb_ProjectData.Location = new System.Drawing.Point(12, 94);
            this.gb_ProjectData.Name = "gb_ProjectData";
            this.gb_ProjectData.Size = new System.Drawing.Size(735, 225);
            this.gb_ProjectData.TabIndex = 3;
            this.gb_ProjectData.TabStop = false;
            this.gb_ProjectData.Text = "项目数据";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(383, 182);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(215, 21);
            this.textBox12.TabIndex = 18;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(111, 182);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(222, 21);
            this.textBox11.TabIndex = 17;
            // 
            // lb_MoveType
            // 
            this.lb_MoveType.AutoSize = true;
            this.lb_MoveType.Location = new System.Drawing.Point(18, 186);
            this.lb_MoveType.Name = "lb_MoveType";
            this.lb_MoveType.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveType.TabIndex = 16;
            this.lb_MoveType.Text = "移动类型";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(383, 152);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(215, 21);
            this.textBox10.TabIndex = 15;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(111, 151);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(222, 21);
            this.textBox9.TabIndex = 14;
            // 
            // lb_Supplier
            // 
            this.lb_Supplier.AutoSize = true;
            this.lb_Supplier.Location = new System.Drawing.Point(18, 157);
            this.lb_Supplier.Name = "lb_Supplier";
            this.lb_Supplier.Size = new System.Drawing.Size(41, 12);
            this.lb_Supplier.TabIndex = 13;
            this.lb_Supplier.Text = "供应商";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(383, 121);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(215, 21);
            this.textBox8.TabIndex = 12;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(111, 121);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(222, 21);
            this.textBox7.TabIndex = 11;
            // 
            // lb_Batch
            // 
            this.lb_Batch.AutoSize = true;
            this.lb_Batch.Location = new System.Drawing.Point(18, 126);
            this.lb_Batch.Name = "lb_Batch";
            this.lb_Batch.Size = new System.Drawing.Size(29, 12);
            this.lb_Batch.TabIndex = 10;
            this.lb_Batch.Text = "批次";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(383, 89);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(215, 21);
            this.textBox6.TabIndex = 9;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(111, 90);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(222, 21);
            this.textBox5.TabIndex = 8;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(383, 59);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(215, 21);
            this.textBox4.TabIndex = 7;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(111, 60);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(222, 21);
            this.textBox3.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(349, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "到";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(349, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "到";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(349, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "到";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(349, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "到";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(349, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "到";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(349, 31);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "到";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(383, 28);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(215, 21);
            this.textBox2.TabIndex = 4;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(111, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(222, 21);
            this.textBox1.TabIndex = 3;
            // 
            // lb_StockAddress
            // 
            this.lb_StockAddress.AutoSize = true;
            this.lb_StockAddress.Location = new System.Drawing.Point(16, 90);
            this.lb_StockAddress.Name = "lb_StockAddress";
            this.lb_StockAddress.Size = new System.Drawing.Size(53, 12);
            this.lb_StockAddress.TabIndex = 2;
            this.lb_StockAddress.Text = "库存地点";
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(16, 60);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(29, 12);
            this.lb_Factory.TabIndex = 1;
            this.lb_Factory.Text = "工厂";
            // 
            // lb_Material
            // 
            this.lb_Material.AutoSize = true;
            this.lb_Material.Location = new System.Drawing.Point(16, 31);
            this.lb_Material.Name = "lb_Material";
            this.lb_Material.Size = new System.Drawing.Size(29, 12);
            this.lb_Material.TabIndex = 0;
            this.lb_Material.Text = "物料";
            // 
            // gb_TableHead
            // 
            this.gb_TableHead.Controls.Add(this.dateTimePicker2);
            this.gb_TableHead.Controls.Add(this.dateTimePicker1);
            this.gb_TableHead.Controls.Add(this.lb_VoucherDate);
            this.gb_TableHead.Controls.Add(this.label7);
            this.gb_TableHead.Location = new System.Drawing.Point(12, 338);
            this.gb_TableHead.Name = "gb_TableHead";
            this.gb_TableHead.Size = new System.Drawing.Size(735, 107);
            this.gb_TableHead.TabIndex = 4;
            this.gb_TableHead.TabStop = false;
            this.gb_TableHead.Text = "表头数据";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(383, 31);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(215, 21);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(111, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(222, 21);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // lb_VoucherDate
            // 
            this.lb_VoucherDate.AutoSize = true;
            this.lb_VoucherDate.Location = new System.Drawing.Point(18, 37);
            this.lb_VoucherDate.Name = "lb_VoucherDate";
            this.lb_VoucherDate.Size = new System.Drawing.Size(53, 12);
            this.lb_VoucherDate.TabIndex = 0;
            this.lb_VoucherDate.Text = "记账日期";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(349, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "到";
            // 
            // Frm_QueryDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 480);
            this.Controls.Add(this.gb_TableHead);
            this.Controls.Add(this.gb_ProjectData);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_QueryDocument";
            this.Text = "物料凭证->查询";
            this.gb_ProjectData.ResumeLayout(false);
            this.gb_ProjectData.PerformLayout();
            this.gb_TableHead.ResumeLayout(false);
            this.gb_TableHead.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.GroupBox gb_ProjectData;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lb_StockAddress;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.Label lb_Material;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label lb_MoveType;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label lb_Supplier;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label lb_Batch;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gb_TableHead;
        private System.Windows.Forms.Label lb_VoucherDate;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label7;
    }
}