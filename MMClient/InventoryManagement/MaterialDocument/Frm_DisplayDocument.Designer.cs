﻿namespace MMClient.InventoryManagement
{
    partial class Frm_DisplayDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Year = new System.Windows.Forms.TextBox();
            this.txt_MaterialDocument = new System.Windows.Forms.TextBox();
            this.lb_Underline2 = new System.Windows.Forms.Label();
            this.lb_Underline1 = new System.Windows.Forms.Label();
            this.lb_Year = new System.Windows.Forms.Label();
            this.lb_MaterialDocument = new System.Windows.Forms.Label();
            this.btn_Comfirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.gb_Condition = new System.Windows.Forms.GroupBox();
            this.gb_Condition.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_Year
            // 
            this.txt_Year.Location = new System.Drawing.Point(173, 94);
            this.txt_Year.Name = "txt_Year";
            this.txt_Year.Size = new System.Drawing.Size(95, 21);
            this.txt_Year.TabIndex = 19;
            // 
            // txt_MaterialDocument
            // 
            this.txt_MaterialDocument.Location = new System.Drawing.Point(173, 31);
            this.txt_MaterialDocument.Name = "txt_MaterialDocument";
            this.txt_MaterialDocument.Size = new System.Drawing.Size(176, 21);
            this.txt_MaterialDocument.TabIndex = 18;
            // 
            // lb_Underline2
            // 
            this.lb_Underline2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline2.Location = new System.Drawing.Point(17, 112);
            this.lb_Underline2.Name = "lb_Underline2";
            this.lb_Underline2.Size = new System.Drawing.Size(170, 1);
            this.lb_Underline2.TabIndex = 17;
            // 
            // lb_Underline1
            // 
            this.lb_Underline1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline1.Location = new System.Drawing.Point(6, 49);
            this.lb_Underline1.Name = "lb_Underline1";
            this.lb_Underline1.Size = new System.Drawing.Size(170, 1);
            this.lb_Underline1.TabIndex = 16;
            // 
            // lb_Year
            // 
            this.lb_Year.AutoSize = true;
            this.lb_Year.Location = new System.Drawing.Point(6, 97);
            this.lb_Year.Name = "lb_Year";
            this.lb_Year.Size = new System.Drawing.Size(77, 12);
            this.lb_Year.TabIndex = 15;
            this.lb_Year.Text = "物料凭证年度";
            // 
            // lb_MaterialDocument
            // 
            this.lb_MaterialDocument.AutoSize = true;
            this.lb_MaterialDocument.Location = new System.Drawing.Point(6, 34);
            this.lb_MaterialDocument.Name = "lb_MaterialDocument";
            this.lb_MaterialDocument.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialDocument.TabIndex = 14;
            this.lb_MaterialDocument.Text = "物料凭证";
            // 
            // btn_Comfirm
            // 
            this.btn_Comfirm.Location = new System.Drawing.Point(28, 45);
            this.btn_Comfirm.Name = "btn_Comfirm";
            this.btn_Comfirm.Size = new System.Drawing.Size(61, 24);
            this.btn_Comfirm.TabIndex = 13;
            this.btn_Comfirm.Text = "确定";
            this.btn_Comfirm.UseVisualStyleBackColor = true;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(95, 45);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(61, 24);
            this.btn_Cancel.TabIndex = 28;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // gb_Condition
            // 
            this.gb_Condition.Controls.Add(this.lb_MaterialDocument);
            this.gb_Condition.Controls.Add(this.lb_Year);
            this.gb_Condition.Controls.Add(this.txt_Year);
            this.gb_Condition.Controls.Add(this.lb_Underline1);
            this.gb_Condition.Controls.Add(this.txt_MaterialDocument);
            this.gb_Condition.Controls.Add(this.lb_Underline2);
            this.gb_Condition.Location = new System.Drawing.Point(28, 88);
            this.gb_Condition.Name = "gb_Condition";
            this.gb_Condition.Size = new System.Drawing.Size(787, 145);
            this.gb_Condition.TabIndex = 29;
            this.gb_Condition.TabStop = false;
            // 
            // Frm_DisplayDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(878, 487);
            this.Controls.Add(this.gb_Condition);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Comfirm);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_DisplayDocument";
            this.Text = "物料凭证->显示";
            this.gb_Condition.ResumeLayout(false);
            this.gb_Condition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Year;
        private System.Windows.Forms.TextBox txt_MaterialDocument;
        private System.Windows.Forms.Label lb_Underline2;
        private System.Windows.Forms.Label lb_Underline1;
        private System.Windows.Forms.Label lb_Year;
        private System.Windows.Forms.Label lb_MaterialDocument;
        private System.Windows.Forms.Button btn_Comfirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.GroupBox gb_Condition;
    }
}