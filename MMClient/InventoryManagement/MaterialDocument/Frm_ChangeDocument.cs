﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ChangeDocument : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_ChangeDocument()
        {
            InitializeComponent();
        }

        private void Frm_ChangeDocument_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
