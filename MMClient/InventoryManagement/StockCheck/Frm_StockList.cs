﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_StockList : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_StockList()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (comboBox2.SelectedItem==null && comboBox3.SelectedItem ==null && comboBox4.SelectedItem==null && textBox4.Text == "")
            {
                MessageBox.Show("请至少填写一项数据库选择信息");
                return;
            }
         
           try
           {
               string materialid = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
               string factoryid = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
               string stockid = comboBox4.SelectedItem == null ? "" : comboBox4.SelectedItem.ToString().Trim(); 
                string batchid = textBox4.Text.Trim();
                string evaluatetype = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString();
                string materialgroup = textBox6.Text.Trim();

                
                bool unristrictstock = checkBox7.Checked;
                bool checkstock = checkBox6.Checked;
                bool freezestock = checkBox5.Checked;
                bool batch = checkBox2.Checked;
                bool nonzero = checkBox3.Checked;
                bool nonvalue = checkBox4.Checked;
                bool negative = checkBox1.Checked;

                DataTable dt = new DataTable("dt");
                dt.Columns.Add("materialid", typeof(string));
                dt.Columns.Add("factoryid", typeof(string));
                dt.Columns.Add("stockid", typeof(string));
                dt.Columns.Add("batchid", typeof(string));
                dt.Columns.Add("evaluatetype", typeof(string));
                dt.Columns.Add("materialgroup", typeof(string));
                dt.Columns.Add("unristrictstock", typeof(bool));
                dt.Columns.Add("checkstock", typeof(bool));
                dt.Columns.Add("freezestock", typeof(bool));
                dt.Columns.Add("batch", typeof(bool));
                dt.Columns.Add("nonzero", typeof(bool));
                dt.Columns.Add("nonvalue", typeof(bool));
                dt.Columns.Add("negative", typeof(bool));

                DataRow dr = dt.NewRow();
                dr["materialid"] = materialid;
                dr["factoryid"] = factoryid;
                dr["stockid"] = stockid;
                dr["batchid"] = batchid;
                dr["evaluatetype"] = evaluatetype;
                dr["materialgroup"] = materialgroup;
                dr["unristrictstock"] = unristrictstock;
                dr["checkstock"] = checkstock;
                dr["freezestock"] = freezestock;
                
                dr["batch"] = batch;
                dr["nonzero"] = nonzero;
                dr["nonvalue"] = nonvalue;
                dr["negative"] = negative;
                dt.Rows.Add(dr);
            
                StockListBLL stklstbll = new StockListBLL();
                DataTable info = stklstbll.stocklist(dt);
                if (info.Rows.Count == 0)
                {
                    MessageBox.Show("没有符合条件的物料，请检查输入是否正确");
                    return;
                }
                DataTable dt0 = stklstbll.changestyle(info, Convert.ToBoolean(checkBox4.Checked));

                Frm_ShowStockList frmshow = new Frm_ShowStockList(dt0);
                frmshow.Show();


           }
            catch
            {
                MessageBox.Show("查询出错，请检查输入是否正确");
            }   

        }

        private void Frm_StockList_Load(object sender, EventArgs e)
        {

            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox3, flist);
            comb.FuzzyQury(comboBox2, mlist);
            comb.FuzzyQury(comboBox4, slist);
        }
    }
}
