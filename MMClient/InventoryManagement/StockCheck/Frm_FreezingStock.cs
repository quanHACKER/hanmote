﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_FreezingStock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_FreezingStock()
        {
            InitializeComponent();
        }

        private void Frm_FreezingStock_Load(object sender, EventArgs e)
        {
            List<string> doculist = new List<string>();

            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getStocktakingDocuid();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doculist.Insert(i, dt.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, doculist);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s =comboBox1.SelectedItem==null?"":comboBox1.SelectedItem.ToString();
            if (s != "")
            {
                try
                {
                    CreateStocktakingBLL csd = new CreateStocktakingBLL();
                    if(csd.freezingstock(s))
                    MessageBox.Show("冻结成功");
                    else
                        MessageBox.Show("冻结失败，请检查凭证编号是否正确"); 

                }
                catch
                {
                    MessageBox.Show("冻结失败，请检查凭证编号是否正确"); 
                }
            }
            else {
                MessageBox.Show("冻结失败，请检查凭证编号是否正确");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString();
            if (s != "")
            {
                try
                {
                    CreateStocktakingBLL csd = new CreateStocktakingBLL();
                    if(csd.freezingstock(s))
                    MessageBox.Show("解除冻结成功");
                    else
                        MessageBox.Show("冻结失败，请检查凭证编号是否正确");

                }
                catch
                {
                    MessageBox.Show("冻结失败，请检查凭证编号是否正确");
                }
            }

            else
            {
                MessageBox.Show("解除冻结失败，请检查凭证编号是否正确");
            }
        }
    }
}
