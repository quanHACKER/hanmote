﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_InputResultDetail : Form
    {
        DataTable basicInfo;
        public Frm_InputResultDetail()
        {
            InitializeComponent();
        }

        public Frm_InputResultDetail(DataTable dt)
        {
            basicInfo = dt;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = dataGridView1.RowCount;
            dataGridView1.Visible = true;
            if (n > 0)
            {
                DataTable dt = new DataTable("dt");
                dt.Columns.Add("StocktakingDocumentID", typeof(string));
                dt.Columns.Add("StocktakingDate", typeof(DateTime));
                dt.Columns.Add("materialID", typeof(string));
                dt.Columns.Add("batchID", typeof(string));
                dt.Columns.Add("InventoryState", typeof(string));
                dt.Columns.Add("count", typeof(double));
                dt.Columns.Add("SKU", typeof(string));
                dt.Columns.Add("ZC", typeof(bool));
                dt.Columns.Add("accountyear", typeof(string));
                dt.Columns.Add("evaluatetype", typeof(string));
                dt.Columns.Add("materialgroup", typeof(string));
                dt.Columns.Add("inventorytype", typeof(string));
                for (int i = 0; i < n  ; i++)
                {


                    if (dataGridView1.Rows[i].Cells[7].Value.ToString() != "")
                    {
                        DataRow dr = dt.NewRow();
                        dr["StocktakingDocumentID"] = basicInfo.Rows[0][0].ToString();
                        dr["StocktakingDate"] = basicInfo.Rows[0][2].ToString();
                        dr["materialID"] = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        dr["batchID"] = dataGridView1.Rows[i].Cells[2].Value.ToString();
                        dr["InventoryState"] = dataGridView1.Rows[i].Cells[3].Value.ToString();
                        dr["count"] = Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value.ToString());
                        dr["SKU"] = dataGridView1.Rows[i].Cells[8].Value.ToString();
                        dr["ZC"] = Convert.ToBoolean(dataGridView1.Rows[i].Cells[9].Value.ToString());
                        dr["accountyear"] = basicInfo.Rows[0][1].ToString();
                        dr["evaluatetype"] = dataGridView1.Rows[i].Cells[4].Value.ToString();
                        dr["materialgroup"] = dataGridView1.Rows[i].Cells[5].Value.ToString();
                        dr["inventorytype"] = dataGridView1.Rows[i].Cells[6].Value.ToString();
                        dt.Rows.Add(dr);
                    }
                }
                CheckResultBLL ckrd = new CheckResultBLL();
                if (ckrd.inputcount(dt) == true)
                    MessageBox.Show("输入成功");
            }
            else
            {
                dataGridView1.Visible = false;
                MessageBox.Show("暂无符合条件的盘点数据");
            }
            this.Close();

        }

        /// <summary>
        /// 加载待输入盘点数量的物料表格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_InputResult1_Load(object sender, EventArgs e)
        {
            try
            {
                CheckResultBLL crb = new CheckResultBLL();
                DataTable DT = crb.selectbasicInfo(basicInfo.Rows[0][0].ToString());
                this.textBox1.Text = DT.Rows[0][0].ToString();
                this.textBox2.Text = DT.Rows[0][1].ToString();

                DataTable dt = crb.loadTable(basicInfo.Rows[0][0].ToString());
                this.dataGridView1.DataSource = dt;
            }
            catch
            {
                MessageBox.Show("输入失败，请检查凭证编号等信息是否填写正确");
                return;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}