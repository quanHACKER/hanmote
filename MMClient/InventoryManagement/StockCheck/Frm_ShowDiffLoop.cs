﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShowDiffLoop : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_ShowDiffLoop()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int documentid;
            try
            {
                documentid = Convert.ToInt32(comboBox1.SelectedItem.ToString().Trim());
            }
            catch
            {
                MessageBox.Show("输入格式不正确，请检查", "提示");
                return;
            }
            AdjustDifferenceBLL adb = new AdjustDifferenceBLL();
            DataTable dt0 = adb.ShowDifferenceAdjustcommonDAL(documentid.ToString());
            DataTable dt1 = adb.ShowDifferenceAdjustDAL(documentid.ToString());

            if (dt0.Rows.Count < 1 || dt1.Rows.Count < 1)
            {
                MessageBox.Show("暂无满足条件的结果，请检查输入是否正确", "提示");
                return;
            }
            this.textBox2.Text = Convert.ToDateTime(dt0.Rows[0][0].ToString()).Date.ToString().Trim();
            this.textBox3.Text = Convert.ToDateTime(dt0.Rows[0][1].ToString()).Date.ToString().Trim();
            this.textBox4.Text = dt0.Rows[0][2].ToString().Trim();

            this.tabControl1.Visible = true;
            this.tabControl1.Show();

            this.dataGridView1.DataSource = dt1;
            this.dataGridView1.Visible = true;
            this.dataGridView1.Show();

        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void Frm_ShowDiffLoop_Load(object sender, EventArgs e)
        {
            List<string> doculist = new List<string>();

            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getloopStocktakingDocuid();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doculist.Insert(i, dt.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, doculist);
        }

    }
}