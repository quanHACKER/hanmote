﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShowStockList : Form
    {
        DataTable basicinfo;
        public Frm_ShowStockList()
        {
            InitializeComponent();
        }

        public Frm_ShowStockList(DataTable dt)
        {
            basicinfo = dt;
            
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {

            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void Frm_ShowStockList_Load(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = basicinfo;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
