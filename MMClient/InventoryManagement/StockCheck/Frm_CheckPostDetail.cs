﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CheckPostDetail : Form
    {
        DataTable basicInfo;
        public Frm_CheckPostDetail()
        {
            InitializeComponent();
        }


        public Frm_CheckPostDetail(DataTable dt)
        {
            InitializeComponent();
            basicInfo = dt;
        }

        private void Frm_CheckPostDetail_Load(object sender, EventArgs e)
        {
            CheckPostBLL cpb = new CheckPostBLL();
            DataTable dt0 = cpb.selectInfo(basicInfo.Rows[0][0].ToString(), basicInfo.Rows[0][1].ToString());
            DataTable DT = cpb.getfactory(basicInfo.Rows[0][0].ToString());
            if (DT.Rows.Count > 0)
            {
                this.textBox1.Text = DT.Rows[0][0].ToString();
                this.textBox2.Text = DT.Rows[0][1].ToString();
            }

            else
            {
                MessageBox.Show("暂无满足条件的盘点数据，请检查凭证编号或者会计年度信息是否正确");
                return;
            }

            int n = dt0.Rows.Count;
            DataTable dt = new DataTable("dt");
            if (n > 0)
            {
                dt.Columns.Add("选择", typeof(bool));
                dt.Columns.Add("盘点凭证编码", typeof(string));
                dt.Columns.Add("物料编码", typeof(string));
                dt.Columns.Add("批次", typeof(string));
                dt.Columns.Add("库存状态", typeof(string));
                dt.Columns.Add("评估类型", typeof(string));
                dt.Columns.Add("物料组", typeof(string));
                dt.Columns.Add("库存类型", typeof(string));
                dt.Columns.Add("账面数量", typeof(double));
                dt.Columns.Add("盘点数量", typeof(double));
                dt.Columns.Add("差额", typeof(double));
                dt.Columns.Add("计量单位", typeof(string));
                dt.Columns.Add("原因", typeof(string));

                for (int i = 0; i < n; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["选择"] = false;
                    dr["盘点凭证编码"] = basicInfo.Rows[0][0].ToString();
                    dr["物料编码"] = dt0.Rows[i][0].ToString();
                    dr["批次"] = dt0.Rows[i][1].ToString();
                    dr["库存状态"] = dt0.Rows[i][2].ToString();
                    dr["账面数量"] = Convert.ToDouble(dt0.Rows[i][3].ToString());
                    dr["盘点数量"] = Convert.ToDouble(dt0.Rows[i][4].ToString());
                    dr["差额"] = Convert.ToDouble(dt0.Rows[i][5].ToString());
                    dr["计量单位"] = dt0.Rows[i][6].ToString();
                    dr["原因"] = dt0.Rows[i][7].ToString();
                    dr["评估类型"] = dt0.Rows[i][8].ToString();
                    dr["物料组"] = dt0.Rows[i][9].ToString();
                    dr["库存类型"] = dt0.Rows[i][10].ToString();
                    dt.Rows.Add(dr);
                }

                this.dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("暂无满足条件的盘点数据，请检查凭证编号或者会计年度信息是否正确");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = dataGridView1.RowCount;
            dataGridView1.Visible = true;
            if (n > 0)
            {
                DataTable dt = new DataTable("dt");
                dt.Columns.Add("选择", typeof(bool));
                dt.Columns.Add("盘点凭证编码", typeof(string));
                dt.Columns.Add("物料编码", typeof(string));
                dt.Columns.Add("批次", typeof(string));
                dt.Columns.Add("工厂", typeof(string));
                dt.Columns.Add("库存地", typeof(string));
                dt.Columns.Add("原因", typeof(string));
                dt.Columns.Add("库存状态", typeof(string));
                dt.Columns.Add("评估类型", typeof(string));
                dt.Columns.Add("物料组", typeof(string));
                dt.Columns.Add("库存类型", typeof(string));
                dt.Columns.Add("账面数量", typeof(string));
                dt.Columns.Add("盘点数量", typeof(string));
                dt.Columns.Add("差额", typeof(string));
                dt.Columns.Add("SKU", typeof(string));
                
                
                for (int i = 0; i < n ; i++)
                {

                    if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[0].Value.ToString()) == true)
                    {
                        DataRow dr = dt.NewRow();
                        dr["选择"] = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        dr["盘点凭证编码"] = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        dr["物料编码"] = dataGridView1.Rows[i].Cells[2].Value.ToString();
                        dr["批次"] = dataGridView1.Rows[i].Cells[3].Value.ToString();
                        dr["工厂"] = this.textBox1.Text;
                        dr["库存地"] = this.textBox2.Text;
                        dr["原因"] = dataGridView1.Rows[i].Cells[12].Value.ToString();
                        dr["库存状态"] = dataGridView1.Rows[i].Cells[4].Value.ToString();
                        dr["评估类型"] = dataGridView1.Rows[i].Cells[5].Value.ToString();
                        dr["物料组"] = dataGridView1.Rows[i].Cells[6].Value.ToString();
                        dr["库存类型"] = dataGridView1.Rows[i].Cells[7].Value.ToString();
                        dr["账面数量"] = dataGridView1.Rows[i].Cells[8].Value.ToString();
                        dr["盘点数量"] = dataGridView1.Rows[i].Cells[9].Value.ToString();
                        dr["差额"] = dataGridView1.Rows[i].Cells[10].Value.ToString();
                        dr["SKU"] = dataGridView1.Rows[i].Cells[11].Value.ToString();
                        
                        dt.Rows.Add(dr);
                    }
                }
                CheckPostBLL ckrd = new CheckPostBLL();
                ckrd.checkPost(dt);

            }
            else
            {
                dataGridView1.Visible = false;
                MessageBox.Show("暂无符合条件的盘点数据");
            }
            this.Close();

        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {

        }
    }
}
