﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CheckPost : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_CheckPost()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (comboBox1.SelectedItem == null|| textBox2.Text == "")
            {
                MessageBox.Show("请将盘点凭证信息填写完整");
                return;
            }

            try
            {
                int a = Convert.ToInt32(textBox2.Text.Trim());
            }
            catch
            {
                MessageBox.Show("请确认已输入的年份是否为2000-2500之间的数字!");
                return;
            }

            if (Convert.ToInt32(textBox2.Text.Trim()) < 2000 || Convert.ToInt32(textBox2.Text.Trim()) > 2500)
            {
                MessageBox.Show("请输入2000-2500之间的年份");
                return;
            }

            string documentid = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            string accountyear = this.textBox2.Text.Trim();
            DateTime postdate = dateTimePicker1.Value;

            DataTable dt = new DataTable("basicinfo");
            dt.Columns.Add("DocumentID", typeof(string));
            dt.Columns.Add("accountyear", typeof(string));
            dt.Columns.Add("postdate", typeof(DateTime));

            DataRow dr = dt.NewRow();
            dr["DocumentID"] = documentid;
            dr["accountyear"] = textBox2.Text;
            dr["postdate"] = dateTimePicker1.Value;
            
            dt.Rows.Add(dr);

            CheckPostBLL cpb = new CheckPostBLL();
            DataTable dt0 = cpb.selectInfo(dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString());


            if (dt0.Rows.Count > 0)
            {
                try
                {
                    Frm_CheckPostDetail frm_CheckPostDetail = new Frm_CheckPostDetail(dt);
                    frm_CheckPostDetail.Show();
                }
                catch
                {
                    MessageBox.Show("过账失败，请检查凭证信息是否填写正确");
                    return;
                }
            }
            else
            {
                MessageBox.Show("该凭证已过账或未填写盘点结果");
                return;
            }
        }

        private void Frm_CheckPost_Load(object sender, EventArgs e)
        {
            List<string> doculist = new List<string>();

            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getStocktakingDocuid();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doculist.Insert(i, dt.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, doculist);
        }
    }
}
