﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMClient.InventoryManagement
{
    public class CommonVariable
    {
        /// <summary>
        /// 所有库存类型名称集合
        /// </summary>
        public static List<string> stockTypeNameList;

        /// <summary>
        /// 所有库存状态名称集合
        /// </summary>
        public static List<string> stockStateNameList;

    }
}
