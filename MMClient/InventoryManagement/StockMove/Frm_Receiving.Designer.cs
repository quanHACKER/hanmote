﻿namespace MMClient.InventoryManagement
{
    partial class Frm_Receiving
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Check = new System.Windows.Forms.Button();
            this.btn_Post = new System.Windows.Forms.Button();
            this.gb_Content = new System.Windows.Forms.GroupBox();
            this.btn_MultiAdd = new System.Windows.Forms.Button();
            this.lb_MoveTypeDetails = new System.Windows.Forms.Label();
            this.lb_DocumentSig = new System.Windows.Forms.Label();
            this.cbb_GR = new System.Windows.Forms.ComboBox();
            this.btn_confirm = new System.Windows.Forms.Button();
            this.tc_Details = new System.Windows.Forms.TabControl();
            this.tp_TransferAccount = new System.Windows.Forms.TabPage();
            this.gb_End = new System.Windows.Forms.GroupBox();
            this.btn_ESSearch = new System.Windows.Forms.Button();
            this.btn_EFSearch = new System.Windows.Forms.Button();
            this.txt_EStockID = new System.Windows.Forms.TextBox();
            this.txt_EFactoryID = new System.Windows.Forms.TextBox();
            this.txt_EStockName = new System.Windows.Forms.TextBox();
            this.txt_EFactoryName = new System.Windows.Forms.TextBox();
            this.txt_EMaterialID = new System.Windows.Forms.TextBox();
            this.txt_EMaterialName = new System.Windows.Forms.TextBox();
            this.lb_EStock = new System.Windows.Forms.Label();
            this.lb_EFactory = new System.Windows.Forms.Label();
            this.lb_EMaterial = new System.Windows.Forms.Label();
            this.gb_Start = new System.Windows.Forms.GroupBox();
            this.btn_TFind = new System.Windows.Forms.Button();
            this.txt_TSpecialStock = new System.Windows.Forms.TextBox();
            this.txt_TStockID = new System.Windows.Forms.TextBox();
            this.txt_TFactoryID = new System.Windows.Forms.TextBox();
            this.txt_TStockName = new System.Windows.Forms.TextBox();
            this.txt_TFactoryName = new System.Windows.Forms.TextBox();
            this.txt_TMaterialID = new System.Windows.Forms.TextBox();
            this.txt_TMaterialName = new System.Windows.Forms.TextBox();
            this.lb_TSpecialStock = new System.Windows.Forms.Label();
            this.lb_TStock = new System.Windows.Forms.Label();
            this.lb_TFactory = new System.Windows.Forms.Label();
            this.lb_TMaterial = new System.Windows.Forms.Label();
            this.tp_Material = new System.Windows.Forms.TabPage();
            this.lb_EvluateTypeUnderline = new System.Windows.Forms.Label();
            this.cbb_EvaluateType = new System.Windows.Forms.ComboBox();
            this.lb_EvaluateType = new System.Windows.Forms.Label();
            this.txt_MaterialGroup = new System.Windows.Forms.TextBox();
            this.txt_SupplierMaterialID = new System.Windows.Forms.TextBox();
            this.txt_MaterialID = new System.Windows.Forms.TextBox();
            this.txt_MaterialName = new System.Windows.Forms.TextBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.lb_SupplierMaterialID = new System.Windows.Forms.Label();
            this.lb_Material = new System.Windows.Forms.Label();
            this.lb_MaterialUnderline3 = new System.Windows.Forms.Label();
            this.lb_MaterialUnderline2 = new System.Windows.Forms.Label();
            this.lb_MaterialUnderline1 = new System.Windows.Forms.Label();
            this.tp_Number = new System.Windows.Forms.TabPage();
            this.txt_InputMeasureUnit = new System.Windows.Forms.TextBox();
            this.txt_ContainerMeasureUnit = new System.Windows.Forms.TextBox();
            this.txt_ContainerNumber = new System.Windows.Forms.TextBox();
            this.txt_YSNumber = new System.Windows.Forms.TextBox();
            this.txt_OrderMeasureUnit = new System.Windows.Forms.TextBox();
            this.txt_OrderNumber = new System.Windows.Forms.TextBox();
            this.txt_DeliveryMeasureUnit = new System.Windows.Forms.TextBox();
            this.txt_DeliveryNumber = new System.Windows.Forms.TextBox();
            this.txt_SKUMeasureUnit = new System.Windows.Forms.TextBox();
            this.txt_SKUNumber = new System.Windows.Forms.TextBox();
            this.txt_InputNumber = new System.Windows.Forms.TextBox();
            this.lb_ContainerNumber = new System.Windows.Forms.Label();
            this.lb_YSNumber = new System.Windows.Forms.Label();
            this.lb_OrderNumber = new System.Windows.Forms.Label();
            this.lb_DeliveryNumber = new System.Windows.Forms.Label();
            this.lb_SKU = new System.Windows.Forms.Label();
            this.lb_InputNumber = new System.Windows.Forms.Label();
            this.lb_NuberUnderline1 = new System.Windows.Forms.Label();
            this.lb_NuberUnderline6 = new System.Windows.Forms.Label();
            this.lb_NuberUnderline5 = new System.Windows.Forms.Label();
            this.lb_NuberUnderline4 = new System.Windows.Forms.Label();
            this.lb_NuberUnderline3 = new System.Windows.Forms.Label();
            this.lb_NuberUnderline2 = new System.Windows.Forms.Label();
            this.tp_StockAddress = new System.Windows.Forms.TabPage();
            this.txt_StockAddressId = new System.Windows.Forms.TextBox();
            this.txt_StockAddressName = new System.Windows.Forms.TextBox();
            this.btn_SelectStock = new System.Windows.Forms.Button();
            this.cbb_StockType = new System.Windows.Forms.ComboBox();
            this.lb_StockType = new System.Windows.Forms.Label();
            this.txt_Context = new System.Windows.Forms.TextBox();
            this.txt_XHD = new System.Windows.Forms.TextBox();
            this.txt_SHF = new System.Windows.Forms.TextBox();
            this.txt_FactoryID = new System.Windows.Forms.TextBox();
            this.txt_FactoryName = new System.Windows.Forms.TextBox();
            this.btn_SelectFactory = new System.Windows.Forms.Button();
            this.lb_Underline7 = new System.Windows.Forms.Label();
            this.lb_Underline6 = new System.Windows.Forms.Label();
            this.lb_Underline5 = new System.Windows.Forms.Label();
            this.lb_Underline4 = new System.Windows.Forms.Label();
            this.lb_Underline3 = new System.Windows.Forms.Label();
            this.lb_Underline2 = new System.Windows.Forms.Label();
            this.lb_Context = new System.Windows.Forms.Label();
            this.lb_XHD = new System.Windows.Forms.Label();
            this.lb_SHF = new System.Windows.Forms.Label();
            this.lb_StockAddress = new System.Windows.Forms.Label();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.tp_Classification = new System.Windows.Forms.TabPage();
            this.btn_CreateBatch = new System.Windows.Forms.Button();
            this.txt_BatchId = new System.Windows.Forms.TextBox();
            this.lb_Batch = new System.Windows.Forms.Label();
            this.lb_BatchUnderline1 = new System.Windows.Forms.Label();
            this.dgv_MaterialInfo = new System.Windows.Forms.DataGridView();
            this.Col_Operation = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Col_MaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_OrderCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MeasureUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockGround = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Batch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_EvaluationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MoveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_ReceiptNoteID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_DeliveryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_SupplierID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_InputCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_SKUCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_NotesCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_SupplierMaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MaterialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Tolerance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_LodingPlace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_ReceivingPlace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_OrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TMaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TFactoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TFactoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TStockID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_TStockName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_SpecialStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_ExplainText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabc_info = new System.Windows.Forms.TabControl();
            this.tp_Ordinary = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_Underline13 = new System.Windows.Forms.Label();
            this.lb_Underline11 = new System.Windows.Forms.Label();
            this.lb_Underline12 = new System.Windows.Forms.Label();
            this.lb_Underline10 = new System.Windows.Forms.Label();
            this.lb_Underline9 = new System.Windows.Forms.Label();
            this.lb_Underline8 = new System.Windows.Forms.Label();
            this.txt_StockManager = new System.Windows.Forms.TextBox();
            this.txt_Supplier = new System.Windows.Forms.TextBox();
            this.lb_StockManager = new System.Windows.Forms.Label();
            this.lb_Supplier = new System.Windows.Forms.Label();
            this.txt_THBill = new System.Windows.Forms.TextBox();
            this.txt_JHBill = new System.Windows.Forms.TextBox();
            this.lb_THBill = new System.Windows.Forms.Label();
            this.lb_JHBill = new System.Windows.Forms.Label();
            this.dtp_AccountDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_VoucherDate = new System.Windows.Forms.DateTimePicker();
            this.lb_AccountDate = new System.Windows.Forms.Label();
            this.lb_VoucherDate = new System.Windows.Forms.Label();
            this.tp_Supplier = new System.Windows.Forms.TabPage();
            this.txt_SupplierAddress = new System.Windows.Forms.TextBox();
            this.txt_SupplierName = new System.Windows.Forms.TextBox();
            this.txt_ZipCode = new System.Windows.Forms.TextBox();
            this.txt_SupplierID = new System.Windows.Forms.TextBox();
            this.lb_ContactInfo = new System.Windows.Forms.Label();
            this.lb_SupplierName = new System.Windows.Forms.Label();
            this.lb_SupplierContacts = new System.Windows.Forms.Label();
            this.lb_SUnderline4 = new System.Windows.Forms.Label();
            this.lb_SUnderline2 = new System.Windows.Forms.Label();
            this.lb_SUnderline3 = new System.Windows.Forms.Label();
            this.lb_SupplierID = new System.Windows.Forms.Label();
            this.lb_SUnderline1 = new System.Windows.Forms.Label();
            this.lb_MoveType = new System.Windows.Forms.Label();
            this.txt_POID = new System.Windows.Forms.TextBox();
            this.cbb_SHType = new System.Windows.Forms.ComboBox();
            this.cbb_YDType = new System.Windows.Forms.ComboBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btn_Clear = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Content.SuspendLayout();
            this.tc_Details.SuspendLayout();
            this.tp_TransferAccount.SuspendLayout();
            this.gb_End.SuspendLayout();
            this.gb_Start.SuspendLayout();
            this.tp_Material.SuspendLayout();
            this.tp_Number.SuspendLayout();
            this.tp_StockAddress.SuspendLayout();
            this.tp_Classification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialInfo)).BeginInit();
            this.tabc_info.SuspendLayout();
            this.tp_Ordinary.SuspendLayout();
            this.tp_Supplier.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(27, 14);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(56, 24);
            this.btn_Save.TabIndex = 0;
            this.btn_Save.Text = "保存";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Check
            // 
            this.btn_Check.Location = new System.Drawing.Point(89, 14);
            this.btn_Check.Name = "btn_Check";
            this.btn_Check.Size = new System.Drawing.Size(56, 24);
            this.btn_Check.TabIndex = 1;
            this.btn_Check.Text = "检查";
            this.btn_Check.UseVisualStyleBackColor = true;
            this.btn_Check.Click += new System.EventHandler(this.btn_Check_Click);
            // 
            // btn_Post
            // 
            this.btn_Post.Location = new System.Drawing.Point(151, 14);
            this.btn_Post.Name = "btn_Post";
            this.btn_Post.Size = new System.Drawing.Size(56, 24);
            this.btn_Post.TabIndex = 2;
            this.btn_Post.Text = "过账";
            this.btn_Post.UseVisualStyleBackColor = true;
            this.btn_Post.Click += new System.EventHandler(this.btn_Post_Click);
            // 
            // gb_Content
            // 
            this.gb_Content.Controls.Add(this.btn_MultiAdd);
            this.gb_Content.Controls.Add(this.lb_MoveTypeDetails);
            this.gb_Content.Controls.Add(this.lb_DocumentSig);
            this.gb_Content.Controls.Add(this.cbb_GR);
            this.gb_Content.Controls.Add(this.btn_confirm);
            this.gb_Content.Controls.Add(this.tc_Details);
            this.gb_Content.Controls.Add(this.dgv_MaterialInfo);
            this.gb_Content.Controls.Add(this.tabc_info);
            this.gb_Content.Controls.Add(this.lb_MoveType);
            this.gb_Content.Controls.Add(this.txt_POID);
            this.gb_Content.Controls.Add(this.cbb_SHType);
            this.gb_Content.Controls.Add(this.cbb_YDType);
            this.gb_Content.Location = new System.Drawing.Point(13, 45);
            this.gb_Content.Name = "gb_Content";
            this.gb_Content.Size = new System.Drawing.Size(1140, 731);
            this.gb_Content.TabIndex = 3;
            this.gb_Content.TabStop = false;
            // 
            // btn_MultiAdd
            // 
            this.btn_MultiAdd.Location = new System.Drawing.Point(12, 389);
            this.btn_MultiAdd.Name = "btn_MultiAdd";
            this.btn_MultiAdd.Size = new System.Drawing.Size(105, 23);
            this.btn_MultiAdd.TabIndex = 15;
            this.btn_MultiAdd.Text = "批量添加";
            this.btn_MultiAdd.UseVisualStyleBackColor = true;
            this.btn_MultiAdd.Click += new System.EventHandler(this.btn_MultiAdd_Click);
            // 
            // lb_MoveTypeDetails
            // 
            this.lb_MoveTypeDetails.Location = new System.Drawing.Point(899, 27);
            this.lb_MoveTypeDetails.Name = "lb_MoveTypeDetails";
            this.lb_MoveTypeDetails.Size = new System.Drawing.Size(224, 18);
            this.lb_MoveTypeDetails.TabIndex = 14;
            // 
            // lb_DocumentSig
            // 
            this.lb_DocumentSig.AutoSize = true;
            this.lb_DocumentSig.Location = new System.Drawing.Point(281, 701);
            this.lb_DocumentSig.Name = "lb_DocumentSig";
            this.lb_DocumentSig.Size = new System.Drawing.Size(0, 12);
            this.lb_DocumentSig.TabIndex = 12;
            // 
            // cbb_GR
            // 
            this.cbb_GR.FormattingEnabled = true;
            this.cbb_GR.Items.AddRange(new object[] {
            "101",
            "102",
            "201"});
            this.cbb_GR.Location = new System.Drawing.Point(831, 22);
            this.cbb_GR.Name = "cbb_GR";
            this.cbb_GR.Size = new System.Drawing.Size(62, 20);
            this.cbb_GR.TabIndex = 11;
            this.toolTip.SetToolTip(this.cbb_GR, "移动类型为必填项");
            this.cbb_GR.SelectedIndexChanged += new System.EventHandler(this.cbb_GR_SelectedIndexChanged);
            // 
            // btn_confirm
            // 
            this.btn_confirm.Location = new System.Drawing.Point(671, 20);
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Size = new System.Drawing.Size(56, 24);
            this.btn_confirm.TabIndex = 10;
            this.btn_confirm.Text = "确定";
            this.btn_confirm.UseVisualStyleBackColor = true;
            this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
            // 
            // tc_Details
            // 
            this.tc_Details.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tc_Details.Controls.Add(this.tp_TransferAccount);
            this.tc_Details.Controls.Add(this.tp_Material);
            this.tc_Details.Controls.Add(this.tp_Number);
            this.tc_Details.Controls.Add(this.tp_StockAddress);
            this.tc_Details.Controls.Add(this.tp_Classification);
            this.tc_Details.ItemSize = new System.Drawing.Size(80, 20);
            this.tc_Details.Location = new System.Drawing.Point(14, 421);
            this.tc_Details.Name = "tc_Details";
            this.tc_Details.SelectedIndex = 0;
            this.tc_Details.Size = new System.Drawing.Size(1097, 271);
            this.tc_Details.TabIndex = 8;
            // 
            // tp_TransferAccount
            // 
            this.tp_TransferAccount.BackColor = System.Drawing.SystemColors.Control;
            this.tp_TransferAccount.Controls.Add(this.gb_End);
            this.tp_TransferAccount.Controls.Add(this.gb_Start);
            this.tp_TransferAccount.Location = new System.Drawing.Point(4, 24);
            this.tp_TransferAccount.Name = "tp_TransferAccount";
            this.tp_TransferAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tp_TransferAccount.Size = new System.Drawing.Size(1089, 243);
            this.tp_TransferAccount.TabIndex = 5;
            this.tp_TransferAccount.Text = "转移过账";
            // 
            // gb_End
            // 
            this.gb_End.Controls.Add(this.btn_ESSearch);
            this.gb_End.Controls.Add(this.btn_EFSearch);
            this.gb_End.Controls.Add(this.txt_EStockID);
            this.gb_End.Controls.Add(this.txt_EFactoryID);
            this.gb_End.Controls.Add(this.txt_EStockName);
            this.gb_End.Controls.Add(this.txt_EFactoryName);
            this.gb_End.Controls.Add(this.txt_EMaterialID);
            this.gb_End.Controls.Add(this.txt_EMaterialName);
            this.gb_End.Controls.Add(this.lb_EStock);
            this.gb_End.Controls.Add(this.lb_EFactory);
            this.gb_End.Controls.Add(this.lb_EMaterial);
            this.gb_End.Location = new System.Drawing.Point(501, 7);
            this.gb_End.Name = "gb_End";
            this.gb_End.Size = new System.Drawing.Size(539, 230);
            this.gb_End.TabIndex = 1;
            this.gb_End.TabStop = false;
            this.gb_End.Text = "目标";
            // 
            // btn_ESSearch
            // 
            this.btn_ESSearch.Location = new System.Drawing.Point(401, 125);
            this.btn_ESSearch.Name = "btn_ESSearch";
            this.btn_ESSearch.Size = new System.Drawing.Size(51, 23);
            this.btn_ESSearch.TabIndex = 20;
            this.btn_ESSearch.Text = "查找";
            this.btn_ESSearch.UseVisualStyleBackColor = true;
            this.btn_ESSearch.Click += new System.EventHandler(this.btn_ESSearch_Click);
            // 
            // btn_EFSearch
            // 
            this.btn_EFSearch.Location = new System.Drawing.Point(468, 87);
            this.btn_EFSearch.Name = "btn_EFSearch";
            this.btn_EFSearch.Size = new System.Drawing.Size(50, 23);
            this.btn_EFSearch.TabIndex = 19;
            this.btn_EFSearch.Text = "查找";
            this.btn_EFSearch.UseVisualStyleBackColor = true;
            this.btn_EFSearch.Click += new System.EventHandler(this.btn_EFSearch_Click);
            // 
            // txt_EStockID
            // 
            this.txt_EStockID.Location = new System.Drawing.Point(282, 127);
            this.txt_EStockID.Name = "txt_EStockID";
            this.txt_EStockID.Size = new System.Drawing.Size(113, 21);
            this.txt_EStockID.TabIndex = 18;
            this.txt_EStockID.TextChanged += new System.EventHandler(this.txt_EStockID_TextChanged);
            // 
            // txt_EFactoryID
            // 
            this.txt_EFactoryID.Location = new System.Drawing.Point(342, 89);
            this.txt_EFactoryID.Name = "txt_EFactoryID";
            this.txt_EFactoryID.Size = new System.Drawing.Size(119, 21);
            this.txt_EFactoryID.TabIndex = 17;
            this.txt_EFactoryID.TextChanged += new System.EventHandler(this.txt_EFactoryID_TextChanged);
            // 
            // txt_EStockName
            // 
            this.txt_EStockName.Location = new System.Drawing.Point(77, 127);
            this.txt_EStockName.Name = "txt_EStockName";
            this.txt_EStockName.Size = new System.Drawing.Size(202, 21);
            this.txt_EStockName.TabIndex = 16;
            this.txt_EStockName.TextChanged += new System.EventHandler(this.txt_EStockName_TextChanged);
            // 
            // txt_EFactoryName
            // 
            this.txt_EFactoryName.Location = new System.Drawing.Point(77, 89);
            this.txt_EFactoryName.Name = "txt_EFactoryName";
            this.txt_EFactoryName.Size = new System.Drawing.Size(262, 21);
            this.txt_EFactoryName.TabIndex = 15;
            this.txt_EFactoryName.TextChanged += new System.EventHandler(this.txt_EFactoryName_TextChanged);
            // 
            // txt_EMaterialID
            // 
            this.txt_EMaterialID.Location = new System.Drawing.Point(77, 54);
            this.txt_EMaterialID.Name = "txt_EMaterialID";
            this.txt_EMaterialID.Size = new System.Drawing.Size(175, 21);
            this.txt_EMaterialID.TabIndex = 14;
            this.txt_EMaterialID.TextChanged += new System.EventHandler(this.txt_EMaterialID_TextChanged);
            // 
            // txt_EMaterialName
            // 
            this.txt_EMaterialName.Location = new System.Drawing.Point(77, 27);
            this.txt_EMaterialName.Name = "txt_EMaterialName";
            this.txt_EMaterialName.Size = new System.Drawing.Size(278, 21);
            this.txt_EMaterialName.TabIndex = 13;
            this.txt_EMaterialName.TextChanged += new System.EventHandler(this.txt_EMaterialName_TextChanged);
            // 
            // lb_EStock
            // 
            this.lb_EStock.AutoSize = true;
            this.lb_EStock.Location = new System.Drawing.Point(7, 132);
            this.lb_EStock.Name = "lb_EStock";
            this.lb_EStock.Size = new System.Drawing.Size(41, 12);
            this.lb_EStock.TabIndex = 12;
            this.lb_EStock.Text = "库存地";
            // 
            // lb_EFactory
            // 
            this.lb_EFactory.AutoSize = true;
            this.lb_EFactory.Location = new System.Drawing.Point(8, 95);
            this.lb_EFactory.Name = "lb_EFactory";
            this.lb_EFactory.Size = new System.Drawing.Size(29, 12);
            this.lb_EFactory.TabIndex = 11;
            this.lb_EFactory.Text = "工厂";
            // 
            // lb_EMaterial
            // 
            this.lb_EMaterial.AutoSize = true;
            this.lb_EMaterial.Location = new System.Drawing.Point(6, 34);
            this.lb_EMaterial.Name = "lb_EMaterial";
            this.lb_EMaterial.Size = new System.Drawing.Size(29, 12);
            this.lb_EMaterial.TabIndex = 10;
            this.lb_EMaterial.Text = "物料";
            // 
            // gb_Start
            // 
            this.gb_Start.Controls.Add(this.btn_TFind);
            this.gb_Start.Controls.Add(this.txt_TSpecialStock);
            this.gb_Start.Controls.Add(this.txt_TStockID);
            this.gb_Start.Controls.Add(this.txt_TFactoryID);
            this.gb_Start.Controls.Add(this.txt_TStockName);
            this.gb_Start.Controls.Add(this.txt_TFactoryName);
            this.gb_Start.Controls.Add(this.txt_TMaterialID);
            this.gb_Start.Controls.Add(this.txt_TMaterialName);
            this.gb_Start.Controls.Add(this.lb_TSpecialStock);
            this.gb_Start.Controls.Add(this.lb_TStock);
            this.gb_Start.Controls.Add(this.lb_TFactory);
            this.gb_Start.Controls.Add(this.lb_TMaterial);
            this.gb_Start.Location = new System.Drawing.Point(6, 5);
            this.gb_Start.Name = "gb_Start";
            this.gb_Start.Size = new System.Drawing.Size(489, 232);
            this.gb_Start.TabIndex = 0;
            this.gb_Start.TabStop = false;
            this.gb_Start.Text = "从";
            // 
            // btn_TFind
            // 
            this.btn_TFind.Location = new System.Drawing.Point(263, 52);
            this.btn_TFind.Name = "btn_TFind";
            this.btn_TFind.Size = new System.Drawing.Size(60, 23);
            this.btn_TFind.TabIndex = 11;
            this.btn_TFind.Text = "查找";
            this.btn_TFind.UseVisualStyleBackColor = true;
            this.btn_TFind.Click += new System.EventHandler(this.btn_TFind_Click);
            // 
            // txt_TSpecialStock
            // 
            this.txt_TSpecialStock.Location = new System.Drawing.Point(82, 178);
            this.txt_TSpecialStock.Name = "txt_TSpecialStock";
            this.txt_TSpecialStock.Size = new System.Drawing.Size(88, 21);
            this.txt_TSpecialStock.TabIndex = 10;
            // 
            // txt_TStockID
            // 
            this.txt_TStockID.Location = new System.Drawing.Point(287, 127);
            this.txt_TStockID.Name = "txt_TStockID";
            this.txt_TStockID.Size = new System.Drawing.Size(113, 21);
            this.txt_TStockID.TabIndex = 9;
            // 
            // txt_TFactoryID
            // 
            this.txt_TFactoryID.Location = new System.Drawing.Point(347, 89);
            this.txt_TFactoryID.Name = "txt_TFactoryID";
            this.txt_TFactoryID.Size = new System.Drawing.Size(119, 21);
            this.txt_TFactoryID.TabIndex = 8;
            // 
            // txt_TStockName
            // 
            this.txt_TStockName.Location = new System.Drawing.Point(82, 127);
            this.txt_TStockName.Name = "txt_TStockName";
            this.txt_TStockName.Size = new System.Drawing.Size(202, 21);
            this.txt_TStockName.TabIndex = 7;
            // 
            // txt_TFactoryName
            // 
            this.txt_TFactoryName.Location = new System.Drawing.Point(82, 89);
            this.txt_TFactoryName.Name = "txt_TFactoryName";
            this.txt_TFactoryName.Size = new System.Drawing.Size(262, 21);
            this.txt_TFactoryName.TabIndex = 6;
            // 
            // txt_TMaterialID
            // 
            this.txt_TMaterialID.Location = new System.Drawing.Point(82, 54);
            this.txt_TMaterialID.Name = "txt_TMaterialID";
            this.txt_TMaterialID.Size = new System.Drawing.Size(175, 21);
            this.txt_TMaterialID.TabIndex = 5;
            // 
            // txt_TMaterialName
            // 
            this.txt_TMaterialName.Location = new System.Drawing.Point(82, 27);
            this.txt_TMaterialName.Name = "txt_TMaterialName";
            this.txt_TMaterialName.Size = new System.Drawing.Size(278, 21);
            this.txt_TMaterialName.TabIndex = 4;
            // 
            // lb_TSpecialStock
            // 
            this.lb_TSpecialStock.AutoSize = true;
            this.lb_TSpecialStock.Location = new System.Drawing.Point(13, 183);
            this.lb_TSpecialStock.Name = "lb_TSpecialStock";
            this.lb_TSpecialStock.Size = new System.Drawing.Size(53, 12);
            this.lb_TSpecialStock.TabIndex = 3;
            this.lb_TSpecialStock.Text = "特殊库存";
            // 
            // lb_TStock
            // 
            this.lb_TStock.AutoSize = true;
            this.lb_TStock.Location = new System.Drawing.Point(12, 132);
            this.lb_TStock.Name = "lb_TStock";
            this.lb_TStock.Size = new System.Drawing.Size(41, 12);
            this.lb_TStock.TabIndex = 2;
            this.lb_TStock.Text = "库存地";
            // 
            // lb_TFactory
            // 
            this.lb_TFactory.AutoSize = true;
            this.lb_TFactory.Location = new System.Drawing.Point(13, 95);
            this.lb_TFactory.Name = "lb_TFactory";
            this.lb_TFactory.Size = new System.Drawing.Size(29, 12);
            this.lb_TFactory.TabIndex = 1;
            this.lb_TFactory.Text = "工厂";
            // 
            // lb_TMaterial
            // 
            this.lb_TMaterial.AutoSize = true;
            this.lb_TMaterial.Location = new System.Drawing.Point(11, 34);
            this.lb_TMaterial.Name = "lb_TMaterial";
            this.lb_TMaterial.Size = new System.Drawing.Size(29, 12);
            this.lb_TMaterial.TabIndex = 0;
            this.lb_TMaterial.Text = "物料";
            // 
            // tp_Material
            // 
            this.tp_Material.BackColor = System.Drawing.SystemColors.Control;
            this.tp_Material.Controls.Add(this.lb_EvluateTypeUnderline);
            this.tp_Material.Controls.Add(this.cbb_EvaluateType);
            this.tp_Material.Controls.Add(this.lb_EvaluateType);
            this.tp_Material.Controls.Add(this.txt_MaterialGroup);
            this.tp_Material.Controls.Add(this.txt_SupplierMaterialID);
            this.tp_Material.Controls.Add(this.txt_MaterialID);
            this.tp_Material.Controls.Add(this.txt_MaterialName);
            this.tp_Material.Controls.Add(this.lb_MaterialGroup);
            this.tp_Material.Controls.Add(this.lb_SupplierMaterialID);
            this.tp_Material.Controls.Add(this.lb_Material);
            this.tp_Material.Controls.Add(this.lb_MaterialUnderline3);
            this.tp_Material.Controls.Add(this.lb_MaterialUnderline2);
            this.tp_Material.Controls.Add(this.lb_MaterialUnderline1);
            this.tp_Material.Location = new System.Drawing.Point(4, 24);
            this.tp_Material.Name = "tp_Material";
            this.tp_Material.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Material.Size = new System.Drawing.Size(1089, 243);
            this.tp_Material.TabIndex = 0;
            this.tp_Material.Text = "物料";
            // 
            // lb_EvluateTypeUnderline
            // 
            this.lb_EvluateTypeUnderline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_EvluateTypeUnderline.Location = new System.Drawing.Point(18, 190);
            this.lb_EvluateTypeUnderline.Name = "lb_EvluateTypeUnderline";
            this.lb_EvluateTypeUnderline.Size = new System.Drawing.Size(140, 1);
            this.lb_EvluateTypeUnderline.TabIndex = 19;
            // 
            // cbb_EvaluateType
            // 
            this.cbb_EvaluateType.FormattingEnabled = true;
            this.cbb_EvaluateType.Items.AddRange(new object[] {
            "原材料",
            "成品",
            "半成品",
            "非存储物料",
            "服务",
            "备件",
            "贸易商品经营供应",
            "虚拟件",
            "包装",
            "竞争产品",
            "生产资源/工具"});
            this.cbb_EvaluateType.Location = new System.Drawing.Point(156, 173);
            this.cbb_EvaluateType.Name = "cbb_EvaluateType";
            this.cbb_EvaluateType.Size = new System.Drawing.Size(268, 20);
            this.cbb_EvaluateType.TabIndex = 18;
            this.cbb_EvaluateType.SelectedIndexChanged += new System.EventHandler(this.cbb_EvaluateType_SelectedIndexChanged);
            // 
            // lb_EvaluateType
            // 
            this.lb_EvaluateType.AutoSize = true;
            this.lb_EvaluateType.Location = new System.Drawing.Point(16, 175);
            this.lb_EvaluateType.Name = "lb_EvaluateType";
            this.lb_EvaluateType.Size = new System.Drawing.Size(53, 12);
            this.lb_EvaluateType.TabIndex = 17;
            this.lb_EvaluateType.Text = "评估类型";
            // 
            // txt_MaterialGroup
            // 
            this.txt_MaterialGroup.Location = new System.Drawing.Point(157, 130);
            this.txt_MaterialGroup.Name = "txt_MaterialGroup";
            this.txt_MaterialGroup.ReadOnly = true;
            this.txt_MaterialGroup.Size = new System.Drawing.Size(267, 21);
            this.txt_MaterialGroup.TabIndex = 16;
            this.txt_MaterialGroup.TextChanged += new System.EventHandler(this.txt_MaterialGroup_TextChanged);
            // 
            // txt_SupplierMaterialID
            // 
            this.txt_SupplierMaterialID.Location = new System.Drawing.Point(157, 70);
            this.txt_SupplierMaterialID.Name = "txt_SupplierMaterialID";
            this.txt_SupplierMaterialID.Size = new System.Drawing.Size(332, 21);
            this.txt_SupplierMaterialID.TabIndex = 15;
            this.txt_SupplierMaterialID.TextChanged += new System.EventHandler(this.txt_SupplierMaterialID_TextChanged);
            // 
            // txt_MaterialID
            // 
            this.txt_MaterialID.Location = new System.Drawing.Point(492, 34);
            this.txt_MaterialID.Name = "txt_MaterialID";
            this.txt_MaterialID.ReadOnly = true;
            this.txt_MaterialID.Size = new System.Drawing.Size(190, 21);
            this.txt_MaterialID.TabIndex = 14;
            // 
            // txt_MaterialName
            // 
            this.txt_MaterialName.Location = new System.Drawing.Point(157, 34);
            this.txt_MaterialName.Name = "txt_MaterialName";
            this.txt_MaterialName.ReadOnly = true;
            this.txt_MaterialName.Size = new System.Drawing.Size(332, 21);
            this.txt_MaterialName.TabIndex = 13;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.Location = new System.Drawing.Point(17, 133);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(66, 12);
            this.lb_MaterialGroup.TabIndex = 12;
            this.lb_MaterialGroup.Text = "物料组";
            // 
            // lb_SupplierMaterialID
            // 
            this.lb_SupplierMaterialID.Location = new System.Drawing.Point(17, 73);
            this.lb_SupplierMaterialID.Name = "lb_SupplierMaterialID";
            this.lb_SupplierMaterialID.Size = new System.Drawing.Size(107, 12);
            this.lb_SupplierMaterialID.TabIndex = 11;
            this.lb_SupplierMaterialID.Text = "供应商物料编号";
            // 
            // lb_Material
            // 
            this.lb_Material.Location = new System.Drawing.Point(17, 37);
            this.lb_Material.Name = "lb_Material";
            this.lb_Material.Size = new System.Drawing.Size(60, 12);
            this.lb_Material.TabIndex = 10;
            this.lb_Material.Text = "物料";
            // 
            // lb_MaterialUnderline3
            // 
            this.lb_MaterialUnderline3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_MaterialUnderline3.Location = new System.Drawing.Point(18, 148);
            this.lb_MaterialUnderline3.Name = "lb_MaterialUnderline3";
            this.lb_MaterialUnderline3.Size = new System.Drawing.Size(140, 1);
            this.lb_MaterialUnderline3.TabIndex = 9;
            // 
            // lb_MaterialUnderline2
            // 
            this.lb_MaterialUnderline2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_MaterialUnderline2.Location = new System.Drawing.Point(18, 88);
            this.lb_MaterialUnderline2.Name = "lb_MaterialUnderline2";
            this.lb_MaterialUnderline2.Size = new System.Drawing.Size(140, 1);
            this.lb_MaterialUnderline2.TabIndex = 9;
            // 
            // lb_MaterialUnderline1
            // 
            this.lb_MaterialUnderline1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_MaterialUnderline1.Location = new System.Drawing.Point(18, 52);
            this.lb_MaterialUnderline1.Name = "lb_MaterialUnderline1";
            this.lb_MaterialUnderline1.Size = new System.Drawing.Size(140, 1);
            this.lb_MaterialUnderline1.TabIndex = 9;
            // 
            // tp_Number
            // 
            this.tp_Number.BackColor = System.Drawing.SystemColors.Control;
            this.tp_Number.Controls.Add(this.txt_InputMeasureUnit);
            this.tp_Number.Controls.Add(this.txt_ContainerMeasureUnit);
            this.tp_Number.Controls.Add(this.txt_ContainerNumber);
            this.tp_Number.Controls.Add(this.txt_YSNumber);
            this.tp_Number.Controls.Add(this.txt_OrderMeasureUnit);
            this.tp_Number.Controls.Add(this.txt_OrderNumber);
            this.tp_Number.Controls.Add(this.txt_DeliveryMeasureUnit);
            this.tp_Number.Controls.Add(this.txt_DeliveryNumber);
            this.tp_Number.Controls.Add(this.txt_SKUMeasureUnit);
            this.tp_Number.Controls.Add(this.txt_SKUNumber);
            this.tp_Number.Controls.Add(this.txt_InputNumber);
            this.tp_Number.Controls.Add(this.lb_ContainerNumber);
            this.tp_Number.Controls.Add(this.lb_YSNumber);
            this.tp_Number.Controls.Add(this.lb_OrderNumber);
            this.tp_Number.Controls.Add(this.lb_DeliveryNumber);
            this.tp_Number.Controls.Add(this.lb_SKU);
            this.tp_Number.Controls.Add(this.lb_InputNumber);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline1);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline6);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline5);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline4);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline3);
            this.tp_Number.Controls.Add(this.lb_NuberUnderline2);
            this.tp_Number.Location = new System.Drawing.Point(4, 24);
            this.tp_Number.Name = "tp_Number";
            this.tp_Number.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Number.Size = new System.Drawing.Size(1089, 243);
            this.tp_Number.TabIndex = 1;
            this.tp_Number.Text = "数量";
            // 
            // txt_InputMeasureUnit
            // 
            this.txt_InputMeasureUnit.Location = new System.Drawing.Point(361, 22);
            this.txt_InputMeasureUnit.Name = "txt_InputMeasureUnit";
            this.txt_InputMeasureUnit.ReadOnly = true;
            this.txt_InputMeasureUnit.Size = new System.Drawing.Size(56, 21);
            this.txt_InputMeasureUnit.TabIndex = 27;
            // 
            // txt_ContainerMeasureUnit
            // 
            this.txt_ContainerMeasureUnit.Location = new System.Drawing.Point(759, 188);
            this.txt_ContainerMeasureUnit.Name = "txt_ContainerMeasureUnit";
            this.txt_ContainerMeasureUnit.ReadOnly = true;
            this.txt_ContainerMeasureUnit.Size = new System.Drawing.Size(49, 21);
            this.txt_ContainerMeasureUnit.TabIndex = 26;
            // 
            // txt_ContainerNumber
            // 
            this.txt_ContainerNumber.Location = new System.Drawing.Point(626, 188);
            this.txt_ContainerNumber.Name = "txt_ContainerNumber";
            this.txt_ContainerNumber.ReadOnly = true;
            this.txt_ContainerNumber.Size = new System.Drawing.Size(131, 21);
            this.txt_ContainerNumber.TabIndex = 25;
            this.txt_ContainerNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ContainerNumber_KeyPress);
            // 
            // txt_YSNumber
            // 
            this.txt_YSNumber.Location = new System.Drawing.Point(197, 189);
            this.txt_YSNumber.Name = "txt_YSNumber";
            this.txt_YSNumber.ReadOnly = true;
            this.txt_YSNumber.Size = new System.Drawing.Size(162, 21);
            this.txt_YSNumber.TabIndex = 23;
            this.txt_YSNumber.TextChanged += new System.EventHandler(this.txt_YSNumber_TextChanged);
            // 
            // txt_OrderMeasureUnit
            // 
            this.txt_OrderMeasureUnit.Location = new System.Drawing.Point(361, 156);
            this.txt_OrderMeasureUnit.Name = "txt_OrderMeasureUnit";
            this.txt_OrderMeasureUnit.ReadOnly = true;
            this.txt_OrderMeasureUnit.Size = new System.Drawing.Size(56, 21);
            this.txt_OrderMeasureUnit.TabIndex = 22;
            // 
            // txt_OrderNumber
            // 
            this.txt_OrderNumber.Location = new System.Drawing.Point(197, 156);
            this.txt_OrderNumber.Name = "txt_OrderNumber";
            this.txt_OrderNumber.ReadOnly = true;
            this.txt_OrderNumber.Size = new System.Drawing.Size(162, 21);
            this.txt_OrderNumber.TabIndex = 21;
            this.txt_OrderNumber.TextChanged += new System.EventHandler(this.txt_OrderNumber_TextChanged);
            // 
            // txt_DeliveryMeasureUnit
            // 
            this.txt_DeliveryMeasureUnit.Location = new System.Drawing.Point(361, 105);
            this.txt_DeliveryMeasureUnit.Name = "txt_DeliveryMeasureUnit";
            this.txt_DeliveryMeasureUnit.ReadOnly = true;
            this.txt_DeliveryMeasureUnit.Size = new System.Drawing.Size(56, 21);
            this.txt_DeliveryMeasureUnit.TabIndex = 20;
            // 
            // txt_DeliveryNumber
            // 
            this.txt_DeliveryNumber.Location = new System.Drawing.Point(197, 105);
            this.txt_DeliveryNumber.Name = "txt_DeliveryNumber";
            this.txt_DeliveryNumber.Size = new System.Drawing.Size(162, 21);
            this.txt_DeliveryNumber.TabIndex = 19;
            this.txt_DeliveryNumber.TextChanged += new System.EventHandler(this.txt_DeliveryNumber_TextChanged);
            this.txt_DeliveryNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_DeliveryNumber_KeyPress);
            // 
            // txt_SKUMeasureUnit
            // 
            this.txt_SKUMeasureUnit.Location = new System.Drawing.Point(361, 54);
            this.txt_SKUMeasureUnit.Name = "txt_SKUMeasureUnit";
            this.txt_SKUMeasureUnit.ReadOnly = true;
            this.txt_SKUMeasureUnit.Size = new System.Drawing.Size(56, 21);
            this.txt_SKUMeasureUnit.TabIndex = 18;
            // 
            // txt_SKUNumber
            // 
            this.txt_SKUNumber.Location = new System.Drawing.Point(197, 54);
            this.txt_SKUNumber.Name = "txt_SKUNumber";
            this.txt_SKUNumber.ReadOnly = true;
            this.txt_SKUNumber.Size = new System.Drawing.Size(162, 21);
            this.txt_SKUNumber.TabIndex = 17;
            this.txt_SKUNumber.TextChanged += new System.EventHandler(this.txt_SKUNumber_TextChanged);
            // 
            // txt_InputNumber
            // 
            this.txt_InputNumber.Location = new System.Drawing.Point(197, 22);
            this.txt_InputNumber.Name = "txt_InputNumber";
            this.txt_InputNumber.Size = new System.Drawing.Size(162, 21);
            this.txt_InputNumber.TabIndex = 15;
            this.toolTip.SetToolTip(this.txt_InputNumber, "数量为必填项");
            this.txt_InputNumber.TextChanged += new System.EventHandler(this.txt_InputNumber_TextChanged);
            this.txt_InputNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_InputNumber_KeyPress);
            // 
            // lb_ContainerNumber
            // 
            this.lb_ContainerNumber.AutoSize = true;
            this.lb_ContainerNumber.Location = new System.Drawing.Point(491, 190);
            this.lb_ContainerNumber.Name = "lb_ContainerNumber";
            this.lb_ContainerNumber.Size = new System.Drawing.Size(41, 12);
            this.lb_ContainerNumber.TabIndex = 24;
            this.lb_ContainerNumber.Text = "库存量";
            // 
            // lb_YSNumber
            // 
            this.lb_YSNumber.AutoSize = true;
            this.lb_YSNumber.Location = new System.Drawing.Point(19, 190);
            this.lb_YSNumber.Name = "lb_YSNumber";
            this.lb_YSNumber.Size = new System.Drawing.Size(53, 12);
            this.lb_YSNumber.TabIndex = 14;
            this.lb_YSNumber.Text = "已收数量";
            // 
            // lb_OrderNumber
            // 
            this.lb_OrderNumber.AutoSize = true;
            this.lb_OrderNumber.Location = new System.Drawing.Point(19, 158);
            this.lb_OrderNumber.Name = "lb_OrderNumber";
            this.lb_OrderNumber.Size = new System.Drawing.Size(53, 12);
            this.lb_OrderNumber.TabIndex = 13;
            this.lb_OrderNumber.Text = "订货数量";
            // 
            // lb_DeliveryNumber
            // 
            this.lb_DeliveryNumber.AutoSize = true;
            this.lb_DeliveryNumber.Location = new System.Drawing.Point(19, 107);
            this.lb_DeliveryNumber.Name = "lb_DeliveryNumber";
            this.lb_DeliveryNumber.Size = new System.Drawing.Size(101, 12);
            this.lb_DeliveryNumber.TabIndex = 12;
            this.lb_DeliveryNumber.Text = "交货注释中的数量";
            // 
            // lb_SKU
            // 
            this.lb_SKU.AutoSize = true;
            this.lb_SKU.Location = new System.Drawing.Point(19, 57);
            this.lb_SKU.Name = "lb_SKU";
            this.lb_SKU.Size = new System.Drawing.Size(71, 12);
            this.lb_SKU.TabIndex = 11;
            this.lb_SKU.Text = "SKU中的数量";
            // 
            // lb_InputNumber
            // 
            this.lb_InputNumber.AutoSize = true;
            this.lb_InputNumber.Location = new System.Drawing.Point(19, 24);
            this.lb_InputNumber.Name = "lb_InputNumber";
            this.lb_InputNumber.Size = new System.Drawing.Size(113, 12);
            this.lb_InputNumber.TabIndex = 10;
            this.lb_InputNumber.Text = "已输入单位计的数量";
            // 
            // lb_NuberUnderline1
            // 
            this.lb_NuberUnderline1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline1.Location = new System.Drawing.Point(19, 40);
            this.lb_NuberUnderline1.Name = "lb_NuberUnderline1";
            this.lb_NuberUnderline1.Size = new System.Drawing.Size(180, 1);
            this.lb_NuberUnderline1.TabIndex = 9;
            // 
            // lb_NuberUnderline6
            // 
            this.lb_NuberUnderline6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline6.Location = new System.Drawing.Point(492, 206);
            this.lb_NuberUnderline6.Name = "lb_NuberUnderline6";
            this.lb_NuberUnderline6.Size = new System.Drawing.Size(140, 1);
            this.lb_NuberUnderline6.TabIndex = 9;
            // 
            // lb_NuberUnderline5
            // 
            this.lb_NuberUnderline5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline5.Location = new System.Drawing.Point(19, 206);
            this.lb_NuberUnderline5.Name = "lb_NuberUnderline5";
            this.lb_NuberUnderline5.Size = new System.Drawing.Size(180, 1);
            this.lb_NuberUnderline5.TabIndex = 9;
            // 
            // lb_NuberUnderline4
            // 
            this.lb_NuberUnderline4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline4.Location = new System.Drawing.Point(19, 173);
            this.lb_NuberUnderline4.Name = "lb_NuberUnderline4";
            this.lb_NuberUnderline4.Size = new System.Drawing.Size(180, 1);
            this.lb_NuberUnderline4.TabIndex = 9;
            // 
            // lb_NuberUnderline3
            // 
            this.lb_NuberUnderline3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline3.Location = new System.Drawing.Point(19, 123);
            this.lb_NuberUnderline3.Name = "lb_NuberUnderline3";
            this.lb_NuberUnderline3.Size = new System.Drawing.Size(180, 1);
            this.lb_NuberUnderline3.TabIndex = 9;
            // 
            // lb_NuberUnderline2
            // 
            this.lb_NuberUnderline2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_NuberUnderline2.Location = new System.Drawing.Point(19, 72);
            this.lb_NuberUnderline2.Name = "lb_NuberUnderline2";
            this.lb_NuberUnderline2.Size = new System.Drawing.Size(180, 1);
            this.lb_NuberUnderline2.TabIndex = 9;
            // 
            // tp_StockAddress
            // 
            this.tp_StockAddress.BackColor = System.Drawing.SystemColors.Control;
            this.tp_StockAddress.Controls.Add(this.txt_StockAddressId);
            this.tp_StockAddress.Controls.Add(this.txt_StockAddressName);
            this.tp_StockAddress.Controls.Add(this.btn_SelectStock);
            this.tp_StockAddress.Controls.Add(this.cbb_StockType);
            this.tp_StockAddress.Controls.Add(this.lb_StockType);
            this.tp_StockAddress.Controls.Add(this.txt_Context);
            this.tp_StockAddress.Controls.Add(this.txt_XHD);
            this.tp_StockAddress.Controls.Add(this.txt_SHF);
            this.tp_StockAddress.Controls.Add(this.txt_FactoryID);
            this.tp_StockAddress.Controls.Add(this.txt_FactoryName);
            this.tp_StockAddress.Controls.Add(this.btn_SelectFactory);
            this.tp_StockAddress.Controls.Add(this.lb_Underline7);
            this.tp_StockAddress.Controls.Add(this.lb_Underline6);
            this.tp_StockAddress.Controls.Add(this.lb_Underline5);
            this.tp_StockAddress.Controls.Add(this.lb_Underline4);
            this.tp_StockAddress.Controls.Add(this.lb_Underline3);
            this.tp_StockAddress.Controls.Add(this.lb_Underline2);
            this.tp_StockAddress.Controls.Add(this.lb_Context);
            this.tp_StockAddress.Controls.Add(this.lb_XHD);
            this.tp_StockAddress.Controls.Add(this.lb_SHF);
            this.tp_StockAddress.Controls.Add(this.lb_StockAddress);
            this.tp_StockAddress.Controls.Add(this.lb_Factory);
            this.tp_StockAddress.Location = new System.Drawing.Point(4, 24);
            this.tp_StockAddress.Name = "tp_StockAddress";
            this.tp_StockAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tp_StockAddress.Size = new System.Drawing.Size(1089, 243);
            this.tp_StockAddress.TabIndex = 2;
            this.tp_StockAddress.Text = "库存地";
            // 
            // txt_StockAddressId
            // 
            this.txt_StockAddressId.Location = new System.Drawing.Point(397, 51);
            this.txt_StockAddressId.Name = "txt_StockAddressId";
            this.txt_StockAddressId.Size = new System.Drawing.Size(70, 21);
            this.txt_StockAddressId.TabIndex = 19;
            // 
            // txt_StockAddressName
            // 
            this.txt_StockAddressName.Location = new System.Drawing.Point(156, 51);
            this.txt_StockAddressName.Name = "txt_StockAddressName";
            this.txt_StockAddressName.Size = new System.Drawing.Size(238, 21);
            this.txt_StockAddressName.TabIndex = 18;
            this.toolTip.SetToolTip(this.txt_StockAddressName, "工厂为必填项");
            // 
            // btn_SelectStock
            // 
            this.btn_SelectStock.Location = new System.Drawing.Point(472, 49);
            this.btn_SelectStock.Name = "btn_SelectStock";
            this.btn_SelectStock.Size = new System.Drawing.Size(48, 23);
            this.btn_SelectStock.TabIndex = 20;
            this.btn_SelectStock.Text = "查找";
            this.btn_SelectStock.UseVisualStyleBackColor = true;
            // 
            // cbb_StockType
            // 
            this.cbb_StockType.FormattingEnabled = true;
            this.cbb_StockType.Items.AddRange(new object[] {
            "",
            "非限制使用",
            "质量检查",
            "冻结的库存"});
            this.cbb_StockType.Location = new System.Drawing.Point(725, 18);
            this.cbb_StockType.Name = "cbb_StockType";
            this.cbb_StockType.Size = new System.Drawing.Size(162, 20);
            this.cbb_StockType.TabIndex = 17;
            this.toolTip.SetToolTip(this.cbb_StockType, "库存类型为必填项");
            this.cbb_StockType.SelectedIndexChanged += new System.EventHandler(this.cbb_StockType_SelectedIndexChanged);
            // 
            // lb_StockType
            // 
            this.lb_StockType.AutoSize = true;
            this.lb_StockType.Location = new System.Drawing.Point(589, 21);
            this.lb_StockType.Name = "lb_StockType";
            this.lb_StockType.Size = new System.Drawing.Size(53, 12);
            this.lb_StockType.TabIndex = 16;
            this.lb_StockType.Text = "库存类型";
            // 
            // txt_Context
            // 
            this.txt_Context.Location = new System.Drawing.Point(156, 162);
            this.txt_Context.Name = "txt_Context";
            this.txt_Context.Size = new System.Drawing.Size(567, 21);
            this.txt_Context.TabIndex = 15;
            this.txt_Context.TextChanged += new System.EventHandler(this.txt_Context_TextChanged);
            // 
            // txt_XHD
            // 
            this.txt_XHD.Location = new System.Drawing.Point(156, 113);
            this.txt_XHD.Name = "txt_XHD";
            this.txt_XHD.Size = new System.Drawing.Size(219, 21);
            this.txt_XHD.TabIndex = 14;
            this.txt_XHD.TextChanged += new System.EventHandler(this.txt_XHD_TextChanged);
            // 
            // txt_SHF
            // 
            this.txt_SHF.Location = new System.Drawing.Point(156, 83);
            this.txt_SHF.Name = "txt_SHF";
            this.txt_SHF.Size = new System.Drawing.Size(219, 21);
            this.txt_SHF.TabIndex = 13;
            this.txt_SHF.TextChanged += new System.EventHandler(this.txt_SHF_TextChanged);
            // 
            // txt_FactoryID
            // 
            this.txt_FactoryID.Location = new System.Drawing.Point(397, 19);
            this.txt_FactoryID.Name = "txt_FactoryID";
            this.txt_FactoryID.Size = new System.Drawing.Size(70, 21);
            this.txt_FactoryID.TabIndex = 10;
            // 
            // txt_FactoryName
            // 
            this.txt_FactoryName.Location = new System.Drawing.Point(156, 19);
            this.txt_FactoryName.Name = "txt_FactoryName";
            this.txt_FactoryName.Size = new System.Drawing.Size(238, 21);
            this.txt_FactoryName.TabIndex = 9;
            this.toolTip.SetToolTip(this.txt_FactoryName, "工厂为必填项");
            // 
            // btn_SelectFactory
            // 
            this.btn_SelectFactory.Location = new System.Drawing.Point(472, 17);
            this.btn_SelectFactory.Name = "btn_SelectFactory";
            this.btn_SelectFactory.Size = new System.Drawing.Size(48, 23);
            this.btn_SelectFactory.TabIndex = 11;
            this.btn_SelectFactory.Text = "查找";
            this.btn_SelectFactory.UseVisualStyleBackColor = true;
            this.btn_SelectFactory.Click += new System.EventHandler(this.btn_SelectFactory_Click);
            // 
            // lb_Underline7
            // 
            this.lb_Underline7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline7.Location = new System.Drawing.Point(589, 36);
            this.lb_Underline7.Name = "lb_Underline7";
            this.lb_Underline7.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline7.TabIndex = 8;
            // 
            // lb_Underline6
            // 
            this.lb_Underline6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline6.Location = new System.Drawing.Point(17, 181);
            this.lb_Underline6.Name = "lb_Underline6";
            this.lb_Underline6.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline6.TabIndex = 8;
            // 
            // lb_Underline5
            // 
            this.lb_Underline5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline5.Location = new System.Drawing.Point(17, 132);
            this.lb_Underline5.Name = "lb_Underline5";
            this.lb_Underline5.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline5.TabIndex = 8;
            // 
            // lb_Underline4
            // 
            this.lb_Underline4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline4.Location = new System.Drawing.Point(17, 102);
            this.lb_Underline4.Name = "lb_Underline4";
            this.lb_Underline4.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline4.TabIndex = 8;
            // 
            // lb_Underline3
            // 
            this.lb_Underline3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline3.Location = new System.Drawing.Point(17, 70);
            this.lb_Underline3.Name = "lb_Underline3";
            this.lb_Underline3.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline3.TabIndex = 8;
            // 
            // lb_Underline2
            // 
            this.lb_Underline2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline2.Location = new System.Drawing.Point(17, 38);
            this.lb_Underline2.Name = "lb_Underline2";
            this.lb_Underline2.Size = new System.Drawing.Size(140, 1);
            this.lb_Underline2.TabIndex = 8;
            // 
            // lb_Context
            // 
            this.lb_Context.AutoSize = true;
            this.lb_Context.Location = new System.Drawing.Point(19, 165);
            this.lb_Context.Name = "lb_Context";
            this.lb_Context.Size = new System.Drawing.Size(29, 12);
            this.lb_Context.TabIndex = 7;
            this.lb_Context.Text = "文本";
            // 
            // lb_XHD
            // 
            this.lb_XHD.AutoSize = true;
            this.lb_XHD.Location = new System.Drawing.Point(18, 116);
            this.lb_XHD.Name = "lb_XHD";
            this.lb_XHD.Size = new System.Drawing.Size(41, 12);
            this.lb_XHD.TabIndex = 6;
            this.lb_XHD.Text = "卸货点";
            // 
            // lb_SHF
            // 
            this.lb_SHF.AutoSize = true;
            this.lb_SHF.Location = new System.Drawing.Point(19, 85);
            this.lb_SHF.Name = "lb_SHF";
            this.lb_SHF.Size = new System.Drawing.Size(41, 12);
            this.lb_SHF.TabIndex = 5;
            this.lb_SHF.Text = "收货方";
            // 
            // lb_StockAddress
            // 
            this.lb_StockAddress.AutoSize = true;
            this.lb_StockAddress.Location = new System.Drawing.Point(17, 52);
            this.lb_StockAddress.Name = "lb_StockAddress";
            this.lb_StockAddress.Size = new System.Drawing.Size(53, 12);
            this.lb_StockAddress.TabIndex = 4;
            this.lb_StockAddress.Text = "库存地点";
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(17, 22);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(29, 12);
            this.lb_Factory.TabIndex = 3;
            this.lb_Factory.Text = "工厂";
            // 
            // tp_Classification
            // 
            this.tp_Classification.BackColor = System.Drawing.SystemColors.Control;
            this.tp_Classification.Controls.Add(this.btn_CreateBatch);
            this.tp_Classification.Controls.Add(this.txt_BatchId);
            this.tp_Classification.Controls.Add(this.lb_Batch);
            this.tp_Classification.Controls.Add(this.lb_BatchUnderline1);
            this.tp_Classification.Location = new System.Drawing.Point(4, 24);
            this.tp_Classification.Name = "tp_Classification";
            this.tp_Classification.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Classification.Size = new System.Drawing.Size(1089, 243);
            this.tp_Classification.TabIndex = 4;
            this.tp_Classification.Text = "分类";
            // 
            // btn_CreateBatch
            // 
            this.btn_CreateBatch.Location = new System.Drawing.Point(522, 25);
            this.btn_CreateBatch.Name = "btn_CreateBatch";
            this.btn_CreateBatch.Size = new System.Drawing.Size(139, 26);
            this.btn_CreateBatch.TabIndex = 12;
            this.btn_CreateBatch.Text = "建立批次";
            this.btn_CreateBatch.UseVisualStyleBackColor = true;
            this.btn_CreateBatch.Click += new System.EventHandler(this.btn_CreateBatch_Click);
            // 
            // txt_BatchId
            // 
            this.txt_BatchId.Location = new System.Drawing.Point(160, 27);
            this.txt_BatchId.Name = "txt_BatchId";
            this.txt_BatchId.Size = new System.Drawing.Size(169, 21);
            this.txt_BatchId.TabIndex = 11;
            this.txt_BatchId.TextChanged += new System.EventHandler(this.txt_BatchId_TextChanged);
            // 
            // lb_Batch
            // 
            this.lb_Batch.AutoSize = true;
            this.lb_Batch.Location = new System.Drawing.Point(21, 30);
            this.lb_Batch.Name = "lb_Batch";
            this.lb_Batch.Size = new System.Drawing.Size(29, 12);
            this.lb_Batch.TabIndex = 10;
            this.lb_Batch.Text = "批次";
            // 
            // lb_BatchUnderline1
            // 
            this.lb_BatchUnderline1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_BatchUnderline1.Location = new System.Drawing.Point(21, 45);
            this.lb_BatchUnderline1.Name = "lb_BatchUnderline1";
            this.lb_BatchUnderline1.Size = new System.Drawing.Size(140, 1);
            this.lb_BatchUnderline1.TabIndex = 9;
            // 
            // dgv_MaterialInfo
            // 
            this.dgv_MaterialInfo.AllowUserToDeleteRows = false;
            this.dgv_MaterialInfo.AllowUserToResizeColumns = false;
            this.dgv_MaterialInfo.AllowUserToResizeRows = false;
            this.dgv_MaterialInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_MaterialInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_MaterialInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_MaterialInfo.ColumnHeadersHeight = 25;
            this.dgv_MaterialInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_MaterialInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_Operation,
            this.Col_MaterialID,
            this.Col_MaterialName,
            this.Col_Number,
            this.Col_OrderCount,
            this.Col_MeasureUnit,
            this.Col_UnitPrice,
            this.Col_Factory,
            this.Col_StockGround,
            this.Col_Batch,
            this.Col_EvaluationType,
            this.Col_MoveType,
            this.Col_StockType,
            this.Col_ReceiptNoteID,
            this.Col_DeliveryID,
            this.Col_SupplierID,
            this.Col_InputCount,
            this.Col_SKUCount,
            this.Col_NotesCount,
            this.Col_SupplierMaterialID,
            this.Col_MaterialGroup,
            this.Col_Tolerance,
            this.Col_LodingPlace,
            this.Col_ReceivingPlace,
            this.Col_OrderID,
            this.Col_TMaterialID,
            this.Col_TMaterialName,
            this.Col_TFactoryID,
            this.Col_TFactoryName,
            this.Col_TStockID,
            this.Col_TStockName,
            this.Col_SpecialStock,
            this.Col_ExplainText});
            this.dgv_MaterialInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_MaterialInfo.EnableHeadersVisualStyles = false;
            this.dgv_MaterialInfo.Location = new System.Drawing.Point(14, 216);
            this.dgv_MaterialInfo.Name = "dgv_MaterialInfo";
            this.dgv_MaterialInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgv_MaterialInfo.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_MaterialInfo.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgv_MaterialInfo.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_MaterialInfo.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_MaterialInfo.RowTemplate.Height = 23;
            this.dgv_MaterialInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_MaterialInfo.Size = new System.Drawing.Size(1097, 167);
            this.dgv_MaterialInfo.TabIndex = 7;
            this.dgv_MaterialInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellClick);
            this.dgv_MaterialInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellContentClick);
            this.dgv_MaterialInfo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellDoubleClick);
            this.dgv_MaterialInfo.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellMouseEnter);
            this.dgv_MaterialInfo.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellMouseLeave);
            this.dgv_MaterialInfo.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellValueChanged);
            this.dgv_MaterialInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_MaterialInfo_RowPostPaint);
            // 
            // Col_Operation
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Col_Operation.DefaultCellStyle = dataGridViewCellStyle2;
            this.Col_Operation.HeaderText = "操作";
            this.Col_Operation.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Col_Operation.LinkColor = System.Drawing.Color.Blue;
            this.Col_Operation.Name = "Col_Operation";
            this.Col_Operation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Col_Operation.Text = "移除";
            this.Col_Operation.ToolTipText = "操作";
            this.Col_Operation.Width = 60;
            // 
            // Col_MaterialID
            // 
            this.Col_MaterialID.HeaderText = "物料编号";
            this.Col_MaterialID.Name = "Col_MaterialID";
            this.Col_MaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MaterialID.Width = 120;
            // 
            // Col_MaterialName
            // 
            this.Col_MaterialName.HeaderText = "物料名称";
            this.Col_MaterialName.Name = "Col_MaterialName";
            this.Col_MaterialName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MaterialName.Width = 80;
            // 
            // Col_Number
            // 
            this.Col_Number.HeaderText = "UCE的量";
            this.Col_Number.Name = "Col_Number";
            this.Col_Number.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_Number.ToolTipText = "实收数量";
            this.Col_Number.Width = 60;
            // 
            // Col_OrderCount
            // 
            this.Col_OrderCount.HeaderText = "订单中数量";
            this.Col_OrderCount.Name = "Col_OrderCount";
            this.Col_OrderCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_OrderCount.ToolTipText = "订单中数量";
            this.Col_OrderCount.Width = 80;
            // 
            // Col_MeasureUnit
            // 
            this.Col_MeasureUnit.HeaderText = "单位";
            this.Col_MeasureUnit.Name = "Col_MeasureUnit";
            this.Col_MeasureUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MeasureUnit.ToolTipText = "单位";
            this.Col_MeasureUnit.Width = 40;
            // 
            // Col_UnitPrice
            // 
            this.Col_UnitPrice.HeaderText = "单价";
            this.Col_UnitPrice.Name = "Col_UnitPrice";
            // 
            // Col_Factory
            // 
            this.Col_Factory.HeaderText = "工厂";
            this.Col_Factory.Name = "Col_Factory";
            this.Col_Factory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_Factory.Width = 80;
            // 
            // Col_StockGround
            // 
            this.Col_StockGround.HeaderText = "库存地";
            this.Col_StockGround.Name = "Col_StockGround";
            this.Col_StockGround.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockGround.Width = 80;
            // 
            // Col_Batch
            // 
            this.Col_Batch.HeaderText = "批次";
            this.Col_Batch.Name = "Col_Batch";
            this.Col_Batch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_Batch.Width = 80;
            // 
            // Col_EvaluationType
            // 
            this.Col_EvaluationType.HeaderText = "评估类型";
            this.Col_EvaluationType.Name = "Col_EvaluationType";
            this.Col_EvaluationType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_EvaluationType.Width = 80;
            // 
            // Col_MoveType
            // 
            this.Col_MoveType.HeaderText = "移动类型";
            this.Col_MoveType.Name = "Col_MoveType";
            this.Col_MoveType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MoveType.Width = 80;
            // 
            // Col_StockType
            // 
            this.Col_StockType.HeaderText = "库存类型";
            this.Col_StockType.Name = "Col_StockType";
            this.Col_StockType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockType.Width = 80;
            // 
            // Col_ReceiptNoteID
            // 
            this.Col_ReceiptNoteID.HeaderText = "提货单";
            this.Col_ReceiptNoteID.Name = "Col_ReceiptNoteID";
            this.Col_ReceiptNoteID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_ReceiptNoteID.Width = 80;
            // 
            // Col_DeliveryID
            // 
            this.Col_DeliveryID.HeaderText = "发货单";
            this.Col_DeliveryID.Name = "Col_DeliveryID";
            this.Col_DeliveryID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_DeliveryID.Width = 80;
            // 
            // Col_SupplierID
            // 
            this.Col_SupplierID.HeaderText = "供应商编码";
            this.Col_SupplierID.Name = "Col_SupplierID";
            this.Col_SupplierID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_SupplierID.Width = 80;
            // 
            // Col_InputCount
            // 
            this.Col_InputCount.HeaderText = "输入单位记的数量";
            this.Col_InputCount.Name = "Col_InputCount";
            this.Col_InputCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_SKUCount
            // 
            this.Col_SKUCount.HeaderText = "SKU中的数量";
            this.Col_SKUCount.Name = "Col_SKUCount";
            this.Col_SKUCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_NotesCount
            // 
            this.Col_NotesCount.HeaderText = "交货注释中的数量";
            this.Col_NotesCount.Name = "Col_NotesCount";
            this.Col_NotesCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_SupplierMaterialID
            // 
            this.Col_SupplierMaterialID.HeaderText = "供应商物料编号";
            this.Col_SupplierMaterialID.Name = "Col_SupplierMaterialID";
            this.Col_SupplierMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_MaterialGroup
            // 
            this.Col_MaterialGroup.HeaderText = "物料组";
            this.Col_MaterialGroup.Name = "Col_MaterialGroup";
            this.Col_MaterialGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MaterialGroup.Width = 80;
            // 
            // Col_Tolerance
            // 
            this.Col_Tolerance.HeaderText = "容差";
            this.Col_Tolerance.Name = "Col_Tolerance";
            this.Col_Tolerance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_Tolerance.Width = 80;
            // 
            // Col_LodingPlace
            // 
            this.Col_LodingPlace.HeaderText = "卸货点";
            this.Col_LodingPlace.Name = "Col_LodingPlace";
            this.Col_LodingPlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_LodingPlace.Width = 80;
            // 
            // Col_ReceivingPlace
            // 
            this.Col_ReceivingPlace.HeaderText = "收货方";
            this.Col_ReceivingPlace.Name = "Col_ReceivingPlace";
            this.Col_ReceivingPlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_ReceivingPlace.Width = 80;
            // 
            // Col_OrderID
            // 
            this.Col_OrderID.HeaderText = "订单号";
            this.Col_OrderID.Name = "Col_OrderID";
            this.Col_OrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_OrderID.Width = 80;
            // 
            // Col_TMaterialID
            // 
            this.Col_TMaterialID.HeaderText = "到物料编号";
            this.Col_TMaterialID.Name = "Col_TMaterialID";
            this.Col_TMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TMaterialName
            // 
            this.Col_TMaterialName.HeaderText = "到物料名称";
            this.Col_TMaterialName.Name = "Col_TMaterialName";
            this.Col_TMaterialName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TFactoryID
            // 
            this.Col_TFactoryID.HeaderText = "到工厂编号";
            this.Col_TFactoryID.Name = "Col_TFactoryID";
            this.Col_TFactoryID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TFactoryName
            // 
            this.Col_TFactoryName.HeaderText = "到工厂名称";
            this.Col_TFactoryName.Name = "Col_TFactoryName";
            this.Col_TFactoryName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TStockID
            // 
            this.Col_TStockID.HeaderText = "到库存编号";
            this.Col_TStockID.Name = "Col_TStockID";
            this.Col_TStockID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_TStockName
            // 
            this.Col_TStockName.HeaderText = "到库存名称";
            this.Col_TStockName.Name = "Col_TStockName";
            this.Col_TStockName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_SpecialStock
            // 
            this.Col_SpecialStock.HeaderText = "特殊库存";
            this.Col_SpecialStock.Name = "Col_SpecialStock";
            this.Col_SpecialStock.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_ExplainText
            // 
            this.Col_ExplainText.HeaderText = "文本";
            this.Col_ExplainText.Name = "Col_ExplainText";
            this.Col_ExplainText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabc_info
            // 
            this.tabc_info.Controls.Add(this.tp_Ordinary);
            this.tabc_info.Controls.Add(this.tp_Supplier);
            this.tabc_info.ItemSize = new System.Drawing.Size(80, 20);
            this.tabc_info.Location = new System.Drawing.Point(14, 69);
            this.tabc_info.Name = "tabc_info";
            this.tabc_info.SelectedIndex = 0;
            this.tabc_info.Size = new System.Drawing.Size(1097, 138);
            this.tabc_info.TabIndex = 6;
            // 
            // tp_Ordinary
            // 
            this.tp_Ordinary.BackColor = System.Drawing.SystemColors.Control;
            this.tp_Ordinary.Controls.Add(this.label2);
            this.tp_Ordinary.Controls.Add(this.lb_Underline13);
            this.tp_Ordinary.Controls.Add(this.lb_Underline11);
            this.tp_Ordinary.Controls.Add(this.lb_Underline12);
            this.tp_Ordinary.Controls.Add(this.lb_Underline10);
            this.tp_Ordinary.Controls.Add(this.lb_Underline9);
            this.tp_Ordinary.Controls.Add(this.lb_Underline8);
            this.tp_Ordinary.Controls.Add(this.txt_StockManager);
            this.tp_Ordinary.Controls.Add(this.txt_Supplier);
            this.tp_Ordinary.Controls.Add(this.lb_StockManager);
            this.tp_Ordinary.Controls.Add(this.lb_Supplier);
            this.tp_Ordinary.Controls.Add(this.txt_THBill);
            this.tp_Ordinary.Controls.Add(this.txt_JHBill);
            this.tp_Ordinary.Controls.Add(this.lb_THBill);
            this.tp_Ordinary.Controls.Add(this.lb_JHBill);
            this.tp_Ordinary.Controls.Add(this.dtp_AccountDate);
            this.tp_Ordinary.Controls.Add(this.dtp_VoucherDate);
            this.tp_Ordinary.Controls.Add(this.lb_AccountDate);
            this.tp_Ordinary.Controls.Add(this.lb_VoucherDate);
            this.tp_Ordinary.Location = new System.Drawing.Point(4, 24);
            this.tp_Ordinary.Name = "tp_Ordinary";
            this.tp_Ordinary.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Ordinary.Size = new System.Drawing.Size(1089, 110);
            this.tp_Ordinary.TabIndex = 0;
            this.tp_Ordinary.Text = "一般的";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(999, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 14;
            this.label2.Text = "*必填*";
            // 
            // lb_Underline13
            // 
            this.lb_Underline13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline13.Location = new System.Drawing.Point(653, 69);
            this.lb_Underline13.Name = "lb_Underline13";
            this.lb_Underline13.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline13.TabIndex = 12;
            // 
            // lb_Underline11
            // 
            this.lb_Underline11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline11.Location = new System.Drawing.Point(288, 69);
            this.lb_Underline11.Name = "lb_Underline11";
            this.lb_Underline11.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline11.TabIndex = 12;
            // 
            // lb_Underline12
            // 
            this.lb_Underline12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline12.Location = new System.Drawing.Point(653, 39);
            this.lb_Underline12.Name = "lb_Underline12";
            this.lb_Underline12.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline12.TabIndex = 12;
            // 
            // lb_Underline10
            // 
            this.lb_Underline10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline10.Location = new System.Drawing.Point(288, 39);
            this.lb_Underline10.Name = "lb_Underline10";
            this.lb_Underline10.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline10.TabIndex = 12;
            // 
            // lb_Underline9
            // 
            this.lb_Underline9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline9.Location = new System.Drawing.Point(19, 69);
            this.lb_Underline9.Name = "lb_Underline9";
            this.lb_Underline9.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline9.TabIndex = 12;
            // 
            // lb_Underline8
            // 
            this.lb_Underline8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_Underline8.Location = new System.Drawing.Point(19, 39);
            this.lb_Underline8.Name = "lb_Underline8";
            this.lb_Underline8.Size = new System.Drawing.Size(105, 1);
            this.lb_Underline8.TabIndex = 12;
            // 
            // txt_StockManager
            // 
            this.txt_StockManager.Location = new System.Drawing.Point(756, 51);
            this.txt_StockManager.Name = "txt_StockManager";
            this.txt_StockManager.Size = new System.Drawing.Size(236, 21);
            this.txt_StockManager.TabIndex = 11;
            this.toolTip.SetToolTip(this.txt_StockManager, "仓库管理员为必填项");
            // 
            // txt_Supplier
            // 
            this.txt_Supplier.Location = new System.Drawing.Point(756, 20);
            this.txt_Supplier.Name = "txt_Supplier";
            this.txt_Supplier.ReadOnly = true;
            this.txt_Supplier.Size = new System.Drawing.Size(236, 21);
            this.txt_Supplier.TabIndex = 10;
            // 
            // lb_StockManager
            // 
            this.lb_StockManager.AutoSize = true;
            this.lb_StockManager.Location = new System.Drawing.Point(652, 54);
            this.lb_StockManager.Name = "lb_StockManager";
            this.lb_StockManager.Size = new System.Drawing.Size(41, 12);
            this.lb_StockManager.TabIndex = 9;
            this.lb_StockManager.Text = "经手人";
            // 
            // lb_Supplier
            // 
            this.lb_Supplier.AutoSize = true;
            this.lb_Supplier.Location = new System.Drawing.Point(653, 23);
            this.lb_Supplier.Name = "lb_Supplier";
            this.lb_Supplier.Size = new System.Drawing.Size(53, 12);
            this.lb_Supplier.TabIndex = 8;
            this.lb_Supplier.Text = "供应名称";
            // 
            // txt_THBill
            // 
            this.txt_THBill.Location = new System.Drawing.Point(391, 51);
            this.txt_THBill.Name = "txt_THBill";
            this.txt_THBill.Size = new System.Drawing.Size(212, 21);
            this.txt_THBill.TabIndex = 7;
            // 
            // txt_JHBill
            // 
            this.txt_JHBill.Location = new System.Drawing.Point(391, 20);
            this.txt_JHBill.Name = "txt_JHBill";
            this.txt_JHBill.Size = new System.Drawing.Size(212, 21);
            this.txt_JHBill.TabIndex = 6;
            // 
            // lb_THBill
            // 
            this.lb_THBill.AutoSize = true;
            this.lb_THBill.Location = new System.Drawing.Point(282, 53);
            this.lb_THBill.Name = "lb_THBill";
            this.lb_THBill.Size = new System.Drawing.Size(47, 12);
            this.lb_THBill.TabIndex = 5;
            this.lb_THBill.Text = " 提货单";
            // 
            // lb_JHBill
            // 
            this.lb_JHBill.AutoSize = true;
            this.lb_JHBill.Location = new System.Drawing.Point(288, 23);
            this.lb_JHBill.Name = "lb_JHBill";
            this.lb_JHBill.Size = new System.Drawing.Size(41, 12);
            this.lb_JHBill.TabIndex = 4;
            this.lb_JHBill.Text = "交货单";
            // 
            // dtp_AccountDate
            // 
            this.dtp_AccountDate.Location = new System.Drawing.Point(122, 51);
            this.dtp_AccountDate.Name = "dtp_AccountDate";
            this.dtp_AccountDate.Size = new System.Drawing.Size(122, 21);
            this.dtp_AccountDate.TabIndex = 3;
            // 
            // dtp_VoucherDate
            // 
            this.dtp_VoucherDate.Location = new System.Drawing.Point(122, 20);
            this.dtp_VoucherDate.Name = "dtp_VoucherDate";
            this.dtp_VoucherDate.Size = new System.Drawing.Size(122, 21);
            this.dtp_VoucherDate.TabIndex = 2;
            // 
            // lb_AccountDate
            // 
            this.lb_AccountDate.AutoSize = true;
            this.lb_AccountDate.Location = new System.Drawing.Point(19, 53);
            this.lb_AccountDate.Name = "lb_AccountDate";
            this.lb_AccountDate.Size = new System.Drawing.Size(53, 12);
            this.lb_AccountDate.TabIndex = 1;
            this.lb_AccountDate.Text = "记账日期";
            // 
            // lb_VoucherDate
            // 
            this.lb_VoucherDate.AutoSize = true;
            this.lb_VoucherDate.Location = new System.Drawing.Point(19, 23);
            this.lb_VoucherDate.Name = "lb_VoucherDate";
            this.lb_VoucherDate.Size = new System.Drawing.Size(53, 12);
            this.lb_VoucherDate.TabIndex = 0;
            this.lb_VoucherDate.Text = "凭证日期";
            // 
            // tp_Supplier
            // 
            this.tp_Supplier.BackColor = System.Drawing.SystemColors.Control;
            this.tp_Supplier.Controls.Add(this.txt_SupplierAddress);
            this.tp_Supplier.Controls.Add(this.txt_SupplierName);
            this.tp_Supplier.Controls.Add(this.txt_ZipCode);
            this.tp_Supplier.Controls.Add(this.txt_SupplierID);
            this.tp_Supplier.Controls.Add(this.lb_ContactInfo);
            this.tp_Supplier.Controls.Add(this.lb_SupplierName);
            this.tp_Supplier.Controls.Add(this.lb_SupplierContacts);
            this.tp_Supplier.Controls.Add(this.lb_SUnderline4);
            this.tp_Supplier.Controls.Add(this.lb_SUnderline2);
            this.tp_Supplier.Controls.Add(this.lb_SUnderline3);
            this.tp_Supplier.Controls.Add(this.lb_SupplierID);
            this.tp_Supplier.Controls.Add(this.lb_SUnderline1);
            this.tp_Supplier.Location = new System.Drawing.Point(4, 24);
            this.tp_Supplier.Name = "tp_Supplier";
            this.tp_Supplier.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Supplier.Size = new System.Drawing.Size(1089, 110);
            this.tp_Supplier.TabIndex = 1;
            this.tp_Supplier.Text = "供应商";
            // 
            // txt_SupplierAddress
            // 
            this.txt_SupplierAddress.Location = new System.Drawing.Point(673, 60);
            this.txt_SupplierAddress.Name = "txt_SupplierAddress";
            this.txt_SupplierAddress.ReadOnly = true;
            this.txt_SupplierAddress.Size = new System.Drawing.Size(305, 21);
            this.txt_SupplierAddress.TabIndex = 18;
            // 
            // txt_SupplierName
            // 
            this.txt_SupplierName.Location = new System.Drawing.Point(673, 21);
            this.txt_SupplierName.Name = "txt_SupplierName";
            this.txt_SupplierName.ReadOnly = true;
            this.txt_SupplierName.Size = new System.Drawing.Size(305, 21);
            this.txt_SupplierName.TabIndex = 17;
            // 
            // txt_ZipCode
            // 
            this.txt_ZipCode.Location = new System.Drawing.Point(182, 60);
            this.txt_ZipCode.Name = "txt_ZipCode";
            this.txt_ZipCode.ReadOnly = true;
            this.txt_ZipCode.Size = new System.Drawing.Size(255, 21);
            this.txt_ZipCode.TabIndex = 16;
            // 
            // txt_SupplierID
            // 
            this.txt_SupplierID.Location = new System.Drawing.Point(182, 20);
            this.txt_SupplierID.Name = "txt_SupplierID";
            this.txt_SupplierID.ReadOnly = true;
            this.txt_SupplierID.Size = new System.Drawing.Size(255, 21);
            this.txt_SupplierID.TabIndex = 15;
            // 
            // lb_ContactInfo
            // 
            this.lb_ContactInfo.AutoSize = true;
            this.lb_ContactInfo.Location = new System.Drawing.Point(508, 63);
            this.lb_ContactInfo.Name = "lb_ContactInfo";
            this.lb_ContactInfo.Size = new System.Drawing.Size(29, 12);
            this.lb_ContactInfo.TabIndex = 14;
            this.lb_ContactInfo.Text = "地址";
            // 
            // lb_SupplierName
            // 
            this.lb_SupplierName.AutoSize = true;
            this.lb_SupplierName.Location = new System.Drawing.Point(507, 23);
            this.lb_SupplierName.Name = "lb_SupplierName";
            this.lb_SupplierName.Size = new System.Drawing.Size(65, 12);
            this.lb_SupplierName.TabIndex = 13;
            this.lb_SupplierName.Text = "供应商名称";
            // 
            // lb_SupplierContacts
            // 
            this.lb_SupplierContacts.AutoSize = true;
            this.lb_SupplierContacts.Location = new System.Drawing.Point(18, 63);
            this.lb_SupplierContacts.Name = "lb_SupplierContacts";
            this.lb_SupplierContacts.Size = new System.Drawing.Size(29, 12);
            this.lb_SupplierContacts.TabIndex = 12;
            this.lb_SupplierContacts.Text = "邮编";
            // 
            // lb_SUnderline4
            // 
            this.lb_SUnderline4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_SUnderline4.Location = new System.Drawing.Point(508, 77);
            this.lb_SUnderline4.Name = "lb_SUnderline4";
            this.lb_SUnderline4.Size = new System.Drawing.Size(166, 1);
            this.lb_SUnderline4.TabIndex = 10;
            // 
            // lb_SUnderline2
            // 
            this.lb_SUnderline2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_SUnderline2.Location = new System.Drawing.Point(18, 77);
            this.lb_SUnderline2.Name = "lb_SUnderline2";
            this.lb_SUnderline2.Size = new System.Drawing.Size(166, 1);
            this.lb_SUnderline2.TabIndex = 10;
            // 
            // lb_SUnderline3
            // 
            this.lb_SUnderline3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_SUnderline3.Location = new System.Drawing.Point(508, 38);
            this.lb_SUnderline3.Name = "lb_SUnderline3";
            this.lb_SUnderline3.Size = new System.Drawing.Size(166, 1);
            this.lb_SUnderline3.TabIndex = 1;
            // 
            // lb_SupplierID
            // 
            this.lb_SupplierID.AutoSize = true;
            this.lb_SupplierID.Location = new System.Drawing.Point(18, 23);
            this.lb_SupplierID.Name = "lb_SupplierID";
            this.lb_SupplierID.Size = new System.Drawing.Size(65, 12);
            this.lb_SupplierID.TabIndex = 11;
            this.lb_SupplierID.Text = "供应商代码";
            // 
            // lb_SUnderline1
            // 
            this.lb_SUnderline1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_SUnderline1.Location = new System.Drawing.Point(18, 38);
            this.lb_SUnderline1.Name = "lb_SUnderline1";
            this.lb_SUnderline1.Size = new System.Drawing.Size(166, 1);
            this.lb_SUnderline1.TabIndex = 10;
            // 
            // lb_MoveType
            // 
            this.lb_MoveType.AutoSize = true;
            this.lb_MoveType.Location = new System.Drawing.Point(772, 26);
            this.lb_MoveType.Name = "lb_MoveType";
            this.lb_MoveType.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveType.TabIndex = 4;
            this.lb_MoveType.Text = "移动类型";
            // 
            // txt_POID
            // 
            this.txt_POID.Location = new System.Drawing.Point(436, 21);
            this.txt_POID.Name = "txt_POID";
            this.txt_POID.Size = new System.Drawing.Size(171, 21);
            this.txt_POID.TabIndex = 2;
            // 
            // cbb_SHType
            // 
            this.cbb_SHType.FormattingEnabled = true;
            this.cbb_SHType.Items.AddRange(new object[] {
            "采购订单"});
            this.cbb_SHType.Location = new System.Drawing.Point(220, 21);
            this.cbb_SHType.Name = "cbb_SHType";
            this.cbb_SHType.Size = new System.Drawing.Size(180, 20);
            this.cbb_SHType.TabIndex = 1;
            this.cbb_SHType.SelectedIndexChanged += new System.EventHandler(this.cbb_SHType_SelectedIndexChanged);
            // 
            // cbb_YDType
            // 
            this.cbb_YDType.FormattingEnabled = true;
            this.cbb_YDType.Items.AddRange(new object[] {
            "收货",
            "退货交货",
            "取消",
            "显示",
            "发货",
            "转移过账",
            "出库",
            "入库",
            "报废"});
            this.cbb_YDType.Location = new System.Drawing.Point(14, 21);
            this.cbb_YDType.Name = "cbb_YDType";
            this.cbb_YDType.Size = new System.Drawing.Size(180, 20);
            this.cbb_YDType.TabIndex = 0;
            this.cbb_YDType.SelectedIndexChanged += new System.EventHandler(this.cbb_YDType_SelectedIndexChanged);
            // 
            // toolTip
            // 
            this.toolTip.AutomaticDelay = 2;
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 2;
            this.toolTip.ReshowDelay = 0;
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(214, 14);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(56, 24);
            this.btn_Clear.TabIndex = 4;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "数量";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "规格";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.ToolTipText = "单位";
            this.dataGridViewTextBoxColumn4.Width = 40;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "库存地";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 160;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "批次";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 120;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "评估类型";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "库存类型";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 105;
            // 
            // Frm_Receiving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 788);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.gb_Content);
            this.Controls.Add(this.btn_Post);
            this.Controls.Add(this.btn_Check);
            this.Controls.Add(this.btn_Save);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Receiving";
            this.Text = "收货";
            this.gb_Content.ResumeLayout(false);
            this.gb_Content.PerformLayout();
            this.tc_Details.ResumeLayout(false);
            this.tp_TransferAccount.ResumeLayout(false);
            this.gb_End.ResumeLayout(false);
            this.gb_End.PerformLayout();
            this.gb_Start.ResumeLayout(false);
            this.gb_Start.PerformLayout();
            this.tp_Material.ResumeLayout(false);
            this.tp_Material.PerformLayout();
            this.tp_Number.ResumeLayout(false);
            this.tp_Number.PerformLayout();
            this.tp_StockAddress.ResumeLayout(false);
            this.tp_StockAddress.PerformLayout();
            this.tp_Classification.ResumeLayout(false);
            this.tp_Classification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialInfo)).EndInit();
            this.tabc_info.ResumeLayout(false);
            this.tp_Ordinary.ResumeLayout(false);
            this.tp_Ordinary.PerformLayout();
            this.tp_Supplier.ResumeLayout(false);
            this.tp_Supplier.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Check;
        private System.Windows.Forms.Button btn_Post;
        private System.Windows.Forms.GroupBox gb_Content;
        private System.Windows.Forms.TabControl tabc_info;
        private System.Windows.Forms.TabPage tp_Ordinary;
        private System.Windows.Forms.TabPage tp_Supplier;
        private System.Windows.Forms.Label lb_MoveType;
        private System.Windows.Forms.TextBox txt_POID;
        private System.Windows.Forms.ComboBox cbb_SHType;
        private System.Windows.Forms.ComboBox cbb_YDType;
        private System.Windows.Forms.DateTimePicker dtp_AccountDate;
        private System.Windows.Forms.DateTimePicker dtp_VoucherDate;
        private System.Windows.Forms.Label lb_AccountDate;
        private System.Windows.Forms.Label lb_VoucherDate;
        private System.Windows.Forms.TextBox txt_StockManager;
        private System.Windows.Forms.TextBox txt_Supplier;
        private System.Windows.Forms.Label lb_StockManager;
        private System.Windows.Forms.Label lb_Supplier;
        private System.Windows.Forms.TextBox txt_THBill;
        private System.Windows.Forms.TextBox txt_JHBill;
        private System.Windows.Forms.Label lb_THBill;
        private System.Windows.Forms.Label lb_JHBill;
        private System.Windows.Forms.DataGridView dgv_MaterialInfo;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.Label lb_Underline13;
        private System.Windows.Forms.Label lb_Underline11;
        private System.Windows.Forms.Label lb_Underline12;
        private System.Windows.Forms.Label lb_Underline10;
        private System.Windows.Forms.Label lb_Underline9;
        private System.Windows.Forms.Label lb_Underline8;
        private System.Windows.Forms.Label lb_SUnderline1;
        private System.Windows.Forms.TextBox txt_SupplierAddress;
        private System.Windows.Forms.TextBox txt_SupplierName;
        private System.Windows.Forms.TextBox txt_ZipCode;
        private System.Windows.Forms.TextBox txt_SupplierID;
        private System.Windows.Forms.Label lb_ContactInfo;
        private System.Windows.Forms.Label lb_SupplierName;
        private System.Windows.Forms.Label lb_SupplierContacts;
        private System.Windows.Forms.Label lb_SUnderline4;
        private System.Windows.Forms.Label lb_SUnderline2;
        private System.Windows.Forms.Label lb_SUnderline3;
        private System.Windows.Forms.Label lb_SupplierID;
        private System.Windows.Forms.TabControl tc_Details;
        private System.Windows.Forms.TabPage tp_Material;
        private System.Windows.Forms.TextBox txt_MaterialGroup;
        private System.Windows.Forms.TextBox txt_SupplierMaterialID;
        private System.Windows.Forms.TextBox txt_MaterialID;
        private System.Windows.Forms.TextBox txt_MaterialName;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.Label lb_SupplierMaterialID;
        private System.Windows.Forms.Label lb_Material;
        private System.Windows.Forms.Label lb_MaterialUnderline3;
        private System.Windows.Forms.Label lb_MaterialUnderline2;
        private System.Windows.Forms.Label lb_MaterialUnderline1;
        private System.Windows.Forms.TabPage tp_Number;
        private System.Windows.Forms.TextBox txt_ContainerMeasureUnit;
        private System.Windows.Forms.TextBox txt_ContainerNumber;
        private System.Windows.Forms.TextBox txt_YSNumber;
        private System.Windows.Forms.TextBox txt_OrderMeasureUnit;
        private System.Windows.Forms.TextBox txt_OrderNumber;
        private System.Windows.Forms.TextBox txt_DeliveryMeasureUnit;
        private System.Windows.Forms.TextBox txt_DeliveryNumber;
        private System.Windows.Forms.TextBox txt_SKUMeasureUnit;
        private System.Windows.Forms.TextBox txt_SKUNumber;
        private System.Windows.Forms.TextBox txt_InputNumber;
        private System.Windows.Forms.Label lb_ContainerNumber;
        private System.Windows.Forms.Label lb_YSNumber;
        private System.Windows.Forms.Label lb_OrderNumber;
        private System.Windows.Forms.Label lb_DeliveryNumber;
        private System.Windows.Forms.Label lb_SKU;
        private System.Windows.Forms.Label lb_InputNumber;
        private System.Windows.Forms.Label lb_NuberUnderline1;
        private System.Windows.Forms.Label lb_NuberUnderline6;
        private System.Windows.Forms.Label lb_NuberUnderline5;
        private System.Windows.Forms.Label lb_NuberUnderline4;
        private System.Windows.Forms.Label lb_NuberUnderline3;
        private System.Windows.Forms.Label lb_NuberUnderline2;
        private System.Windows.Forms.TabPage tp_StockAddress;
        private System.Windows.Forms.ComboBox cbb_StockType;
        private System.Windows.Forms.Label lb_StockType;
        private System.Windows.Forms.TextBox txt_Context;
        private System.Windows.Forms.TextBox txt_XHD;
        private System.Windows.Forms.TextBox txt_SHF;
        private System.Windows.Forms.TextBox txt_FactoryID;
        private System.Windows.Forms.TextBox txt_FactoryName;
        private System.Windows.Forms.Button btn_SelectFactory;
        private System.Windows.Forms.Label lb_Underline7;
        private System.Windows.Forms.Label lb_Underline6;
        private System.Windows.Forms.Label lb_Underline5;
        private System.Windows.Forms.Label lb_Underline4;
        private System.Windows.Forms.Label lb_Underline3;
        private System.Windows.Forms.Label lb_Underline2;
        private System.Windows.Forms.Label lb_Context;
        private System.Windows.Forms.Label lb_XHD;
        private System.Windows.Forms.Label lb_SHF;
        private System.Windows.Forms.Label lb_StockAddress;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.TabPage tp_Classification;
        private System.Windows.Forms.Button btn_CreateBatch;
        private System.Windows.Forms.TextBox txt_BatchId;
        private System.Windows.Forms.Label lb_Batch;
        private System.Windows.Forms.Label lb_BatchUnderline1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.TextBox txt_InputMeasureUnit;
        private System.Windows.Forms.ComboBox cbb_GR;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label lb_EvaluateType;
        private System.Windows.Forms.Label lb_EvluateTypeUnderline;
        private System.Windows.Forms.ComboBox cbb_EvaluateType;
        private System.Windows.Forms.Label lb_DocumentSig;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tp_TransferAccount;
        private System.Windows.Forms.GroupBox gb_End;
        private System.Windows.Forms.GroupBox gb_Start;
        private System.Windows.Forms.TextBox txt_EStockID;
        private System.Windows.Forms.TextBox txt_EFactoryID;
        private System.Windows.Forms.TextBox txt_EStockName;
        private System.Windows.Forms.TextBox txt_EFactoryName;
        private System.Windows.Forms.TextBox txt_EMaterialID;
        private System.Windows.Forms.TextBox txt_EMaterialName;
        private System.Windows.Forms.Label lb_EStock;
        private System.Windows.Forms.Label lb_EFactory;
        private System.Windows.Forms.Label lb_EMaterial;
        private System.Windows.Forms.TextBox txt_TStockID;
        private System.Windows.Forms.TextBox txt_TFactoryID;
        private System.Windows.Forms.TextBox txt_TStockName;
        private System.Windows.Forms.TextBox txt_TFactoryName;
        private System.Windows.Forms.TextBox txt_TMaterialID;
        private System.Windows.Forms.TextBox txt_TMaterialName;
        private System.Windows.Forms.Label lb_TSpecialStock;
        private System.Windows.Forms.Label lb_TStock;
        private System.Windows.Forms.Label lb_TFactory;
        private System.Windows.Forms.Label lb_TMaterial;
        private System.Windows.Forms.TextBox txt_TSpecialStock;
        private System.Windows.Forms.Button btn_TFind;
        private System.Windows.Forms.Label lb_MoveTypeDetails;
        private System.Windows.Forms.Button btn_ESSearch;
        private System.Windows.Forms.Button btn_EFSearch;
        private System.Windows.Forms.Button btn_MultiAdd;
        private System.Windows.Forms.DataGridViewLinkColumn Col_Operation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_OrderCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MeasureUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_UnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockGround;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Batch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_EvaluationType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MoveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_ReceiptNoteID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_DeliveryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_SupplierID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_InputCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_SKUCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_NotesCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_SupplierMaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MaterialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Tolerance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_LodingPlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_ReceivingPlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_OrderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TMaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TFactoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TFactoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TStockID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_TStockName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_SpecialStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_ExplainText;
        private System.Windows.Forms.TextBox txt_StockAddressId;
        private System.Windows.Forms.TextBox txt_StockAddressName;
        private System.Windows.Forms.Button btn_SelectStock;
    }
}