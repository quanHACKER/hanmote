﻿namespace MMClient.InventoryManagement
{
    partial class Frm_SelectFactory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_QueryCondition = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lb_Unit = new System.Windows.Forms.Label();
            this.lb_Number = new System.Windows.Forms.Label();
            this.lb_SearchResult = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.dgv_FactoryInfo = new System.Windows.Forms.DataGridView();
            this.Factory_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_QueryCondition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FactoryInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_QueryCondition
            // 
            this.gb_QueryCondition.Controls.Add(this.button1);
            this.gb_QueryCondition.Controls.Add(this.lb_Unit);
            this.gb_QueryCondition.Controls.Add(this.lb_Number);
            this.gb_QueryCondition.Controls.Add(this.lb_SearchResult);
            this.gb_QueryCondition.Controls.Add(this.btn_Search);
            this.gb_QueryCondition.Controls.Add(this.textBox1);
            this.gb_QueryCondition.Controls.Add(this.lb_Factory);
            this.gb_QueryCondition.Location = new System.Drawing.Point(12, 12);
            this.gb_QueryCondition.Name = "gb_QueryCondition";
            this.gb_QueryCondition.Size = new System.Drawing.Size(945, 86);
            this.gb_QueryCondition.TabIndex = 0;
            this.gb_QueryCondition.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(362, 38);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 26);
            this.button1.TabIndex = 11;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.button1.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // lb_Unit
            // 
            this.lb_Unit.Location = new System.Drawing.Point(765, 47);
            this.lb_Unit.Name = "lb_Unit";
            this.lb_Unit.Size = new System.Drawing.Size(20, 12);
            this.lb_Unit.TabIndex = 10;
            this.lb_Unit.Text = "条";
            // 
            // lb_Number
            // 
            this.lb_Number.Location = new System.Drawing.Point(750, 48);
            this.lb_Number.Name = "lb_Number";
            this.lb_Number.Size = new System.Drawing.Size(15, 12);
            this.lb_Number.TabIndex = 9;
            this.lb_Number.Text = "0";
            // 
            // lb_SearchResult
            // 
            this.lb_SearchResult.AutoSize = true;
            this.lb_SearchResult.Location = new System.Drawing.Point(689, 47);
            this.lb_SearchResult.Name = "lb_SearchResult";
            this.lb_SearchResult.Size = new System.Drawing.Size(65, 12);
            this.lb_SearchResult.TabIndex = 8;
            this.lb_SearchResult.Text = "搜索结果：";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(550, 38);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(97, 26);
            this.btn_Search.TabIndex = 3;
            this.btn_Search.Text = "搜索";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(108, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(230, 21);
            this.textBox1.TabIndex = 1;
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(46, 44);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(41, 12);
            this.lb_Factory.TabIndex = 0;
            this.lb_Factory.Text = "工厂：";
            // 
            // dgv_FactoryInfo
            // 
            this.dgv_FactoryInfo.AllowUserToAddRows = false;
            this.dgv_FactoryInfo.AllowUserToResizeColumns = false;
            this.dgv_FactoryInfo.AllowUserToResizeRows = false;
            this.dgv_FactoryInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_FactoryInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_FactoryInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_FactoryInfo.ColumnHeadersHeight = 28;
            this.dgv_FactoryInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_FactoryInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Factory_ID,
            this.Factory_Name,
            this.Factory_Address});
            this.dgv_FactoryInfo.EnableHeadersVisualStyles = false;
            this.dgv_FactoryInfo.Location = new System.Drawing.Point(33, 119);
            this.dgv_FactoryInfo.Name = "dgv_FactoryInfo";
            this.dgv_FactoryInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_FactoryInfo.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_FactoryInfo.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgv_FactoryInfo.RowTemplate.Height = 23;
            this.dgv_FactoryInfo.RowTemplate.ReadOnly = true;
            this.dgv_FactoryInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_FactoryInfo.Size = new System.Drawing.Size(903, 352);
            this.dgv_FactoryInfo.TabIndex = 1;
            this.dgv_FactoryInfo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_FactoryInfo_CellDoubleClick);
            this.dgv_FactoryInfo.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_FactoryInfo_CellMouseEnter);
            this.dgv_FactoryInfo.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_FactoryInfo_CellMouseLeave);
            this.dgv_FactoryInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_FactoryInfo_RowPostPaint);
            // 
            // Factory_ID
            // 
            this.Factory_ID.HeaderText = "工厂编码";
            this.Factory_ID.Name = "Factory_ID";
            this.Factory_ID.Width = 200;
            // 
            // Factory_Name
            // 
            this.Factory_Name.HeaderText = "工厂名称";
            this.Factory_Name.Name = "Factory_Name";
            this.Factory_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Factory_Name.Width = 300;
            // 
            // Factory_Address
            // 
            this.Factory_Address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Factory_Address.HeaderText = "工厂地址";
            this.Factory_Address.Name = "Factory_Address";
            this.Factory_Address.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Frm_SelectFactory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 485);
            this.Controls.Add(this.dgv_FactoryInfo);
            this.Controls.Add(this.gb_QueryCondition);
            this.Name = "Frm_SelectFactory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "请选择工厂";
            this.gb_QueryCondition.ResumeLayout(false);
            this.gb_QueryCondition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FactoryInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_QueryCondition;
        private System.Windows.Forms.DataGridView dgv_FactoryInfo;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Label lb_Unit;
        private System.Windows.Forms.Label lb_Number;
        private System.Windows.Forms.Label lb_SearchResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_Address;
        private System.Windows.Forms.Button button1;
    }
}