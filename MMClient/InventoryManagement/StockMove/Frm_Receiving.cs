﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.MD.MT;
using System.Reflection;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using MMClient.MD.FI;
using MMClient.InventoryManagement;

namespace MMClient.InventoryManagement
{
    public partial class Frm_Receiving : Form
    {
        StockReceivingBLL stockReceivingBLL = new StockReceivingBLL();
        MaterialDocumentNewBLL materialDocumentNewBLL = new MaterialDocumentNewBLL();
        DocumentDetailInfoBLL documentDetailInfoBLL = new DocumentDetailInfoBLL();
        MoveTypeInfoBLL moveTypeInfoBLL = new MoveTypeInfoBLL();
        InventoryInfoBLL inventoryInfoBLL = new InventoryInfoBLL();
        getMaterialInfoBLL getMaterialBLL = new getMaterialInfoBLL();
        StockInfoBLL stockInfoBLL = new StockInfoBLL();
        StockFactoryInfoBLL stockFactoryInfoBLL = new StockFactoryInfoBLL();
        //Frm_StockMove frm_StockMove = null;
        UserUI frm_UserUI = null;

        //查询订单相关的BLL
        OrderInfoBLL orderInfoBLL = null;

        /// <summary>
        /// 判断是否点击了“检查”按钮，初始值为false
        /// </summary>
        private bool checkFlag = false;
        private int code = 0;
        private string materialDocumentID = "";
        //什么一个dgv的行变量，用于存放当前行
        DataGridViewRow currenDGVRow = null;

        /// <summary>
        /// 存放当前行
        /// </summary>
        DataGridViewRow currentRow = null;

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public Frm_Receiving()
        {
            InitializeComponent();
            this.dgv_MaterialInfo.Rows.Add(5);
            this.cbb_YDType.SelectedIndex = 0;
            this.cbb_SHType.SelectedIndex = 0;
            this.dgv_MaterialInfo.TopLeftHeaderCell.Value = "行号";
            this.cbb_StockType.SelectedIndex = 0;
            this.lb_MoveTypeDetails.Text = "";


            Type type = this.dgv_MaterialInfo.GetType();
            PropertyInfo pInfo = type.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pInfo.SetValue(this.dgv_MaterialInfo, true, null);

        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="frm_UserUI">母窗体</param>
        public Frm_Receiving(UserUI frm_UserUI)
            : this()
        {
            this.frm_UserUI = frm_UserUI;
        }

        /// <summary>
        /// 点击按钮选择采购订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelPurOrder_Click(object sender, EventArgs e)
        {
            new Frm_SelPurOrder().ShowDialog();
        }
        
        
        /// <summary>
        /// 输入凭证号后，点击确定，加载相应凭证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_confirm_Click(object sender, EventArgs e)
        {
            //try
            //{
                //获取凭证编号文本
                string documentID = "";

                #region 收货-采购订单

                if ((0 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //获取凭证编号文本
                    documentID = this.txt_POID.Text.Trim();
                    //判断是否输入了凭证编号
                    if (documentID == null || documentID.Length <= 0)
                    {
                        MessageUtil.ShowWarning("请输入相关凭证号...");
                        return;
                    }
                    orderInfoBLL = new OrderInfoBLL();  //获得BLL成连接
                    //获得订单头数据
                    OrderInfoExtendModel orderInfo = orderInfoBLL.getOrderinfoByPOId(documentID);
                    if (orderInfo != null)
                    {
                        //获得订单详细数据
                        List<OrderItemExtendModel> orderItemList = orderInfoBLL.getOrderItemByPOId(documentID);
                        //加载订单数据
                        this.loadOrderInfo(orderInfo, orderItemList);
                    }
                    else
                    {
                        MessageUtil.ShowWarning("没有相关订单信息...");
                    }
                }

                #endregion
                
                #region 取消-物料凭证

                if ((2 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //获取凭证编号文本
                    documentID = this.txt_POID.Text.Trim();
                    //判断是否输入了凭证编号
                    if (documentID == null || documentID.Length <= 0)
                    {
                        MessageUtil.ShowWarning("请输入相关凭证号...");
                        return;
                    }
                    orderInfoBLL = new OrderInfoBLL();  //获得BLL成连接
                    //获得订单头数据
                    OrderInfoExtendModel orderInfo = orderInfoBLL.getOrderinfoByPOId(documentID);
                    if (orderInfo != null)
                    {
                        //获得订单详细数据
                        List<OrderItemExtendModel> orderItemList = orderInfoBLL.getOrderItemByPOId(documentID);
                        //加载订单数据
                        this.loadOrderInfo(orderInfo, orderItemList);
                    }
                    else
                    {
                        MessageUtil.ShowWarning("没有相关订单信息...");
                    }
                }
                #endregion

                #region 显示-物料凭证

                if ((3 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //获取凭证编号文本
                    documentID = this.txt_POID.Text.Trim();
                    //判断是否输入了凭证编号
                    if (documentID == null || documentID.Length <= 0)
                    {
                        MessageUtil.ShowWarning("请输入相关凭证号...");
                        return;
                    }
                    //获得凭证头数据
                    MaterialDocumentNewExtendModel mDocModel = materialDocumentNewBLL.getMaterialNewDocument(documentID);
                    if (mDocModel != null)
                    {
                        //获得凭证详细数据
                        List<DocumentDetailInfoModelExtend> mDocItemList = documentDetailInfoBLL.getMDocItemByDocId(documentID);
                        //加载凭证数据
                        this.loadMDocInfo(mDocModel, mDocItemList);
                    }
                    else
                    {
                        MessageUtil.ShowWarning("没有相关订单信息...");
                    }
                }
                #endregion 

                #region 入库-物料凭证

                if ((7 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //获取凭证编号文本
                    documentID = this.txt_POID.Text.Trim();
                    //判断是否输入了凭证编号
                    if (documentID == null || documentID.Length <= 0)
                    {
                        MessageUtil.ShowWarning("请输入相关凭证号...");
                        return;
                    }
                    //获得凭证头数据
                    MaterialDocumentNewExtendModel mDocModel = materialDocumentNewBLL.getMaterialNewDocument(documentID);
                    if (mDocModel != null)
                    {
                        //获得订单详细数据
                        List<DocumentDetailInfoModelExtend> mDocItemList = documentDetailInfoBLL.getMDocItemByDocId(documentID);
                        //加载订单数据
                        this.loadMDocInfo(mDocModel, mDocItemList);
                    }
                    else
                    {
                        MessageUtil.ShowWarning("没有相关订单信息...");
                    }
                }
                #endregion 

            //}
           // catch (Exception ex)
            //{
            //    MessageUtil.ShowError(ex.Message);
            //}


            #region 以前写的代码
            /*
            try
            {
                if ((3 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex) && !("".Equals(this.txt_POID.Text)))
                {
                    //MessageBox.Show("hello world");
                    MaterialDocumentNewExtendModel materialDocumentExtendModel = new MaterialDocumentNewExtendModel();
                    materialDocumentExtendModel = materialDocumentBLL.findMaterialDocumentByDocumentId(this.txt_POID.Text.Trim());
                    if (materialDocumentExtendModel != null)
                    {
                        this.cbb_GR.Text = materialDocumentExtendModel.Document_Type;
                        //this.cbb_GR.Text = "222";
                        //一般的
                        this.dtp_VoucherDate.Value = materialDocumentExtendModel.Document_Date;
                        this.dtp_AccountDate.Value = materialDocumentExtendModel.Posting_Date;
                        this.txt_JHBill.Text = materialDocumentExtendModel.DeliveryNote_ID;
                        this.txt_THBill.Text = materialDocumentExtendModel.ReceiptNote_ID;
                        this.txt_Supplier.Text = materialDocumentExtendModel.Supplier_Name;
                        this.txt_StockManager.Text = materialDocumentExtendModel.StockManager;
                        //供应商
                        this.txt_SupplierID.Text = materialDocumentExtendModel.Supplier_ID;
                        this.txt_SupplierAddress.Text = materialDocumentExtendModel.Address;
                        this.txt_SupplierName.Text = materialDocumentExtendModel.Supplier_Name;
                        this.txt_ZipCode.Text = materialDocumentExtendModel.Company_ZipCode;
                        //联系人

                        //datagridview
                        DataGridViewRow currentRow = this.dgv_MaterialInfo.Rows[0];
                        currentRow.Cells["Col_MaterialID"].Value = materialDocumentExtendModel.Material_ID;
                        currentRow.Cells["Col_MaterialName"].Value = materialDocumentExtendModel.Material_Name;
                        currentRow.Cells["Col_OK"].Value = true;
                        currentRow.Cells["Col_OrderCount"].Value = materialDocumentExtendModel.Receive_count;
                        currentRow.Cells["Col_MeasureUnit"].Value = materialDocumentExtendModel.Measurement;
                        currentRow.Cells["Col_StockGround"].Value = materialDocumentExtendModel.Stock_Place;
                        currentRow.Cells["Col_Batch"].Value = materialDocumentExtendModel.Batch_ID;
                        currentRow.Cells["Col_EvaluationType"].Value = materialDocumentExtendModel.Evaluate_Type;
                        currentRow.Cells["Col_StockType"].Value = materialDocumentExtendModel.Inventory_Type;

                        //物料
                        this.txt_MaterialName.Text = materialDocumentExtendModel.Material_Name;
                        this.txt_MaterialID.Text = materialDocumentExtendModel.Material_ID;
                        this.txt_SupplierMaterialID.Text = materialDocumentExtendModel.SupplierMaterial_ID;
                        this.txt_MaterialGroup.Text = materialDocumentExtendModel.Material_Group;
                        this.cbb_EvaluateType.Text = materialDocumentExtendModel.Evaluate_Type;
                        //数量
                        this.txt_InputNumber.Text = materialDocumentExtendModel.InputCount.ToString();
                        this.txt_InputMeasureUnit.Text = materialDocumentExtendModel.Measurement;
                        this.txt_SKUNumber.Text = materialDocumentExtendModel.SKUCount.ToString();
                        this.txt_SKUMeasureUnit.Text = materialDocumentExtendModel.Measurement;
                        this.txt_DeliveryNumber.Text = materialDocumentExtendModel.NotesNumber.ToString();
                        this.txt_DeliveryMeasureUnit.Text = materialDocumentExtendModel.Measurement;
                        this.txt_OrderNumber.Text = materialDocumentExtendModel.CountInOrder.ToString();
                        this.txt_OrderMeasureUnit.Text = materialDocumentExtendModel.Measurement;
                        this.txt_YSNumber.Text = materialDocumentExtendModel.Receive_count.ToString();
                        this.txt_ContainerNumber.Text = materialDocumentExtendModel.Tolerance.ToString();
                        //何处
                        //this.txt_YDType.Text = "222";
                        this.txt_YDType.Text = materialDocumentExtendModel.Document_Type;
                        this.txt_FactoryName.Text = materialDocumentExtendModel.Factory_Name;
                        this.txt_FactoryID.Text = materialDocumentExtendModel.Factory_ID;
                        this.cbb_StockAddress.Text = materialDocumentExtendModel.Stock_Place;
                        this.txt_SHF.Text = materialDocumentExtendModel.Receiving_Place;
                        this.txt_XHD.Text = materialDocumentExtendModel.Loading_Place;
                        this.txt_Context.Text = materialDocumentExtendModel.ExplainText;
                        this.cbb_StockType.Text = materialDocumentExtendModel.Inventory_Type;

                        //分类
                        this.txt_BatchId.Text = materialDocumentExtendModel.Batch_ID;
                     

                    }
                    else
                    {
                        //MessageBox.Show(materialDocumentExtendModel.ToString());
                        MessageBox.Show("没有查询到物料凭证", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    
                }
                else
                {
                    this.txt_YDType.Text = this.cbb_GR.Text;
                    if (this.cbb_YDType.SelectedIndex == 0 && this.cbb_SHType.SelectedIndex == 0)
                    {
                        OrderMatReceivingModel orderMatReceivingModel = null;
                        OrderSupReceivingModel orderSupReceivingModel = null;

                        if (!("".Equals(this.txt_POID.Text.Trim())))
                        {
                            orderMatReceivingModel = stockReceivingBLL.findOrderMatInfoById(this.txt_POID.Text.Trim());
                            orderSupReceivingModel = stockReceivingBLL.findOrderSupInfoById(this.txt_POID.Text.Trim());
                            if (orderMatReceivingModel != null || orderSupReceivingModel != null)
                            {
                                //绑定供应商信息
                                this.txt_Supplier.Text = orderSupReceivingModel.Supplier_name;
                                this.txt_SupplierName.Text = orderSupReceivingModel.Supplier_name;
                                this.txt_SupplierID.Text = orderSupReceivingModel.Supplier_ID;
                                this.txt_SupplierAddress.Text = orderSupReceivingModel.Address;
                                this.txt_ZipCode.Text = orderSupReceivingModel.Company_ZipCode;
                                //this.txt_ContactInfo.Text = orderSupReceivingModel.Contact_phone1;

                                //绑定物料信息
                                this.txt_MaterialName.Text = orderMatReceivingModel.Material_Name;
                                this.txt_MaterialID.Text = orderMatReceivingModel.Material_ID;
                                this.txt_MaterialGroup.Text = orderMatReceivingModel.Material_Group;

                                //将订单中物料信息绑定到datagridview中
                                DataGridViewRow currentRow = this.dgv_MaterialInfo.Rows[0];
                                currentRow.Cells["Col_MaterialID"].Value = orderMatReceivingModel.Material_ID;
                                currentRow.Cells["Col_MaterialName"].Value = orderMatReceivingModel.Material_Name;
                                currentRow.Cells["Col_OrderCount"].Value = orderMatReceivingModel.Material_Count;
                                currentRow.Cells["Col_MeasureUnit"].Value = orderMatReceivingModel.Measurement;
                                currentRow.Cells["Col_Delete"].Value = "移除";

                                //更新“数量”选项卡中的信息
                                this.txt_InputMeasureUnit.Text = orderMatReceivingModel.Measurement;
                                this.txt_SKUMeasureUnit.Text = orderMatReceivingModel.Measurement;
                                this.txt_OrderMeasureUnit.Text = orderMatReceivingModel.Measurement;
                                this.txt_DeliveryMeasureUnit.Text = orderMatReceivingModel.Measurement;
                                this.txt_OrderNumber.Text = orderMatReceivingModel.Material_Count.ToString();
                            }
                            else
                            {
                                MessageBox.Show("请检查采购订单号是否正确！", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Question);
                                // MessageBox.Show(
                            }
                        }
                        else
                        {
                            MessageBox.Show("请填写采购订单号！", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Question);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
            * */

            #endregion
        }


        #region 其它代码

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_MaterialInfo.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_MaterialInfo.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }
        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_MaterialInfo.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_MaterialInfo.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 给dgv画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        /// <summary>
        /// 选择工厂
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectFactory_Click(object sender, EventArgs e)
        {
            new Frm_SelectFactory(this, 1).ShowDialog();
        }

        /// <summary>
        /// 设置库存地工厂的name和id
        /// </summary>
        /// <param name="factoryName"></param>
        /// <param name="factoryId"></param>
        public void setSFactoryNameAndId(string factoryName, string factoryId, int whereFactory)
        {
            if (whereFactory == 1)
            {
                this.txt_FactoryName.Text = factoryName;
                this.txt_FactoryID.Text = factoryId;
            }
            if (whereFactory == 2)
            {
                this.txt_EFactoryName.Text = factoryName;
                this.txt_EFactoryID.Text = factoryId;
            }
        }

        public void setStockSelection(List<StockInfoExtendModel> stockInfoList)
        {
  
        }

        /// <summary>
        /// 点击保存按钮，向数据库中插入物料凭证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Save_Click(object sender, EventArgs e)
        {
            //保存前检查代码
            if (checkFlag)
            {
              
            }
            #region 收货----采购订单
            if ((0 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveSHForPO();
            }
            
            #endregion

            #region  退货
            if ((1 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveTHForOther();
            }
            #endregion 

            #region  取消
            if ((2 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveCancelForMDoc();
            }
            #endregion 

            #region 转移过账
            if ((5 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveTransForOther();
            }
            
            #endregion

            #region 出库
            if ((6 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveCKForOther();
            }
            #endregion

            #region 入库
            if ((7 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveRKForMDoc();
            }
            #endregion

            #region 报废
            if ((8 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                this.saveBFForOther();
            }
            #endregion 


        }

        /// <summary>
        /// 收货----采购订单
        /// </summary>
        private void saveSHForPO()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //本次收货总数量
            int totalNumber = this.getTotalNumber(docDetailInfoList);
            //本次收货总数量价值
            float totalValue = this.getTotalValue(docDetailInfoList);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);
            materialDocumentNewModel.Total_Number = totalNumber;
            materialDocumentNewModel.Total_Value = totalValue;

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }

            //一个总数量和一个总价值
           
        }

        /// <summary>
        /// 取消-物料凭证
        /// </summary>
        private void saveCancelForMDoc()
        {

        }

        /// <summary>
        /// 转移过账-其它
        /// </summary>
        private void saveTransForOther()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }
        }

        /// <summary>
        /// 退货-其它
        /// </summary>
        private void saveTHForOther()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }
        }
        /// <summary>
        /// 报废-其它
        /// </summary>
        private void saveBFForOther()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }
        }
        /// <summary>
        /// 出库
        /// </summary>
        private void saveCKForOther()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }
        }

        /// <summary>
        /// 入库
        /// </summary>
        private void saveRKForMDoc()
        {
            string mDocId = this.generateMaterialDocumentID();
            //存放物料凭证详细信息 集合model
            List<DocumentDetailInfoModel> docDetailInfoList = getMaterialDocItemList(mDocId);

            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = this.getMaterialDocInfo(mDocId);

            //数据库插入操作
            int influenceMDocRow = materialDocumentNewBLL.insertMaterialNewDocument(materialDocumentNewModel);
            int influenceDocDRow = documentDetailInfoBLL.insertDocumentDetailInfo(docDetailInfoList);
            if (influenceDocDRow + influenceMDocRow > 1)
            {
                MessageBox.Show("凭证：" + mDocId + "，保存成功!");
            }
        }

        /// <summary>
        /// 获得MaterialDocInfo相关信息
        /// </summary>d
        /// <returns></returns>
        private MaterialDocumentNewModel getMaterialDocInfo(string mDocId)
        {
            //物料凭证信息 mode (此处重新设计过)
            MaterialDocumentNewModel materialDocumentNewModel = new MaterialDocumentNewModel();
            materialDocumentNewModel.StockInDocument_ID = mDocId;
            materialDocumentNewModel.Posting_Date = this.dtp_VoucherDate.Value;
            materialDocumentNewModel.Document_Date = this.dtp_AccountDate.Value;
            materialDocumentNewModel.StockManager = this.txt_StockManager.Text.Trim();
            materialDocumentNewModel.Document_Type = this.cbb_GR.Text.Trim();  //移动类型
            materialDocumentNewModel.Reversred = false;   //非冲销凭证
            materialDocumentNewModel.Order_ID = this.txt_POID.Text.Trim();
            materialDocumentNewModel.Delivery_ID = this.txt_JHBill.Text.Trim();
            materialDocumentNewModel.ReceiptNote_ID = this.txt_THBill.Text.Trim();

            //一个总数量和一个总价值

            return materialDocumentNewModel;
        }

        /// <summary>
        /// 获得凭证的物料项
        /// </summary>
        /// <returns></returns>
        private List<DocumentDetailInfoModel> getMaterialDocItemList(string mDocId)
        {
            List<DocumentDetailInfoModel> docDetailInfoList = new List<DocumentDetailInfoModel>();
            DocumentDetailInfoModel documentDetailInfoModel;
            if (this.dgv_MaterialInfo.Rows.Count > 0)   //dgv有数据时才进行扫描
            {
                //对dgv进行扫描
                foreach(DataGridViewRow currentDgvRow in this.dgv_MaterialInfo.Rows)
                {
                    if (currentDgvRow.Cells["Col_MaterialID"].Value != null && !"".Equals(currentDgvRow.Cells["Col_MaterialID"].Value.ToString().Trim()))
                    {
                        documentDetailInfoModel = new DocumentDetailInfoModel();
                        documentDetailInfoModel.StockInDocument_ID = mDocId;
                        documentDetailInfoModel.SMaterial_ID = currentDgvRow.Cells["Col_MaterialID"].Value == null ? "" : currentDgvRow.Cells["Col_MaterialID"].Value.ToString().Trim();
                        documentDetailInfoModel.Order_ID = this.txt_POID.Text.Trim();
                        documentDetailInfoModel.Delivery_ID = this.txt_JHBill.Text.Trim();
                        documentDetailInfoModel.ReceiptNote_ID = this.txt_THBill.Text.Trim();
                        documentDetailInfoModel.SFactory_ID = currentDgvRow.Cells["Col_Factory"].Value == null ? "" : currentDgvRow.Cells["Col_Factory"].Value.ToString().Trim();
                        documentDetailInfoModel.SStock_ID = currentDgvRow.Cells["Col_StockGround"].Value == null ? "" : currentDgvRow.Cells["Col_StockGround"].Value.ToString().Trim();
                        documentDetailInfoModel.Supplier_ID = this.txt_SupplierID.Text.Trim();
                        documentDetailInfoModel.Inventory_Type = currentDgvRow.Cells["Col_StockType"].Value == null ? "" : currentDgvRow.Cells["Col_StockType"].Value.ToString().Trim();
                        documentDetailInfoModel.Receive_Count = currentDgvRow.Cells["Col_Number"].Value == null ? 0 : Convert.ToInt32(currentDgvRow.Cells["Col_Number"].Value.ToString().Trim());
                        documentDetailInfoModel.Order_Count = currentDgvRow.Cells["Col_OrderCount"].Value == null ? 0 : Convert.ToInt32(currentDgvRow.Cells["Col_OrderCount"].Value.ToString().Trim());
                        documentDetailInfoModel.UnitPrice = currentDgvRow.Cells["Col_UnitPrice"].Value == null ? 0 :(float) Convert.ToDouble(currentDgvRow.Cells["Col_UnitPrice"].Value.ToString().Trim());
                        documentDetailInfoModel.Unit = currentDgvRow.Cells["Col_MeasureUnit"].Value == null ? "" : currentDgvRow.Cells["Col_MeasureUnit"].Value.ToString().Trim();
                        documentDetailInfoModel.Input_Count = currentDgvRow.Cells["Col_InputCount"].Value == null ? 0 :  Convert.ToInt32(currentDgvRow.Cells["Col_InputCount"].Value.ToString().Trim());
                        documentDetailInfoModel.SKU_Count = currentDgvRow.Cells["Col_SKUCount"].Value == null ? 0 :  Convert.ToInt32(currentDgvRow.Cells["Col_SKUCount"].Value.ToString().Trim());
                        documentDetailInfoModel.Notes_Count = currentDgvRow.Cells["Col_NotesCount"].Value == null ? 0 :  Convert.ToInt32(currentDgvRow.Cells["Col_NotesCount"].Value.ToString().Trim());
                        documentDetailInfoModel.Batch_ID = currentDgvRow.Cells["Col_Batch"].Value == null ? "" : currentDgvRow.Cells["Col_Batch"].Value.ToString().Trim();
                        documentDetailInfoModel.SupplierMaterial_ID = currentDgvRow.Cells["Col_SupplierMaterialID"].Value == null ? "" : currentDgvRow.Cells["Col_SupplierMaterialID"].Value.ToString().Trim();
                        documentDetailInfoModel.Material_Group = currentDgvRow.Cells["Col_MaterialGroup"].Value == null ? "" : currentDgvRow.Cells["Col_MaterialGroup"].Value.ToString().Trim();
                        documentDetailInfoModel.Evaluate_Type = currentDgvRow.Cells["Col_EvaluationType"].Value == null ? "" : currentDgvRow.Cells["Col_EvaluationType"].Value.ToString().Trim();
                        documentDetailInfoModel.Tolerance = currentDgvRow.Cells["Col_Tolerance"].Value == null ? 0 : Convert.ToInt32(currentDgvRow.Cells["Col_Tolerance"].Value.ToString().Trim());
                        documentDetailInfoModel.Loading_Place = currentDgvRow.Cells["Col_LodingPlace"].Value == null ? "" : currentDgvRow.Cells["Col_LodingPlace"].Value.ToString().Trim();
                        documentDetailInfoModel.Receiving_Place = currentDgvRow.Cells["Col_ReceivingPlace"].Value == null ? "" : currentDgvRow.Cells["Col_ReceivingPlace"].Value.ToString().Trim();
                        documentDetailInfoModel.TMaterial_ID = currentDgvRow.Cells["Col_TMaterialID"].Value == null ? "" : currentDgvRow.Cells["Col_TMaterialID"].Value.ToString().Trim();
                        documentDetailInfoModel.TFactory_ID = currentDgvRow.Cells["Col_TFactoryID"].Value == null ? "" : currentDgvRow.Cells["Col_TFactoryID"].Value.ToString().Trim();
                        documentDetailInfoModel.TStock_ID = currentDgvRow.Cells["Col_TStockID"].Value == null ? "" : currentDgvRow.Cells["Col_TStockID"].Value.ToString().Trim();
                        documentDetailInfoModel.TMaterial_Name = currentDgvRow.Cells["Col_TMaterialName"].Value == null ? "" : currentDgvRow.Cells["Col_TMaterialName"].Value.ToString().Trim();
                        documentDetailInfoModel.TFactory_Name = currentDgvRow.Cells["Col_TFactoryName"].Value == null ? "" : currentDgvRow.Cells["Col_TFactoryName"].Value.ToString().Trim();
                        documentDetailInfoModel.TStock_Name = currentDgvRow.Cells["Col_TStockName"].Value == null ? "" : currentDgvRow.Cells["Col_TStockName"].Value.ToString().Trim();
                        documentDetailInfoModel.SpecialStock = currentDgvRow.Cells["Col_SpecialStock"].Value == null ? "" : currentDgvRow.Cells["Col_SpecialStock"].Value.ToString().Trim();
                        documentDetailInfoModel.ExplainText = currentDgvRow.Cells["Col_ExplainText"].Value == null ? "" : currentDgvRow.Cells["Col_ExplainText"].Value.ToString().Trim();
                        //下面两个暂时不涉及
                        //documentDetailInfoModel.StorehouseUnit = currentDgvRow.Cells["Col_MaterialID"].Value == null ? "" : currentDgvRow.Cells["Col_MaterialID"].Value.ToString().Trim();
                        //documentDetailInfoModel.Storehouse_ID = currentDgvRow.Cells["Col_MaterialID"].Value == null ? "" : currentDgvRow.Cells["Col_MaterialID"].Value.ToString().Trim();
                        docDetailInfoList.Add(documentDetailInfoModel);

                    }
                }
            }
            return docDetailInfoList;
        }

        /// <summary>
        /// 本次收货总数量
        /// </summary>
        /// <param name="docDetailInfoList"></param>
        /// <returns></returns>
        private int getTotalNumber(List<DocumentDetailInfoModel> docDetailInfoList)
        {
            int number = 0;
            if (docDetailInfoList != null && docDetailInfoList.Count > 0)
            {
                foreach (DocumentDetailInfoModel docItem in docDetailInfoList)
                {
                    number += docItem.Receive_Count;
                }
            }
            return number;
        }

        /// <summary>
        /// 本次收货总价值
        /// </summary>
        /// <param name="docDetailInfoList"></param>
        /// <returns></returns>
        private float getTotalValue(List<DocumentDetailInfoModel> docDetailInfoList)
        {
            float value = 0;
            if (docDetailInfoList != null && docDetailInfoList.Count > 0)
            {
                foreach (DocumentDetailInfoModel docItem in docDetailInfoList)
                {
                    value += docItem.Receive_Count * docItem.UnitPrice;
                }
            }
            return value;
        }
        /// <summary>
        /// 生成物料凭证ID
        /// </summary>
        /// <returns></returns>
        private string generateMaterialDocumentID()
        {
            string year = DateTime.Now.ToString("yyyy");
            string month = DateTime.Now.ToString("MM");
            string day = DateTime.Now.ToString("dd");
            string hour = DateTime.Now.ToString("hh");
            string minute = DateTime.Now.Minute.ToString();
            string second = DateTime.Now.Second.ToString();
            //code++;
            //materialDocumentID = year + month + day + code.ToString().PadLeft(4, '0');
            materialDocumentID = year + month + day + minute + second;
            return materialDocumentID;
        }

        /// <summary>
        /// 点击检查按钮,是否漏输
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Check_Click(object sender, EventArgs e)
        {
            if ((5 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
                if (!("".Equals(this.txt_StockManager.Text.Trim())))
                {
                    this.checkFlag = true;
                }
                else
                {
                    MessageBox.Show("需要填写必填信息,带红色标记部分", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if ((0 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
            {
               
                    this.checkFlag = true;
                
            }
            if ((6 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex) && !("".Equals(this.cbb_GR.Text.Trim())))
            {
                if (!("".Equals(this.txt_StockManager.Text.Trim())))
                {
                    this.checkFlag = true;
                }
                else
                {
                    MessageBox.Show("需要填写必填信息,带红色标记部分", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// 当项目确定复选框选中时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_Confirm_CheckedChanged(object sender, EventArgs e)
        {
            this.lb_DocumentSig.Text = "凭证OK";
        }

        /// <summary>
        /// 已输入单位记的数量，只允许用户输入数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_InputNumber_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsNumber(e.KeyChar)) && e.KeyChar != (char)8)
            {
                //经过判断为数字，可以输入
                e.Handled = true;
            }
            else
            {
                //提示只能输入数字信息
            }
        }

        /// <summary>
        /// 交货注释中的数量，只允许用户输入数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_DeliveryNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.txt_InputNumber_KeyPress(sender, e);
        }

        /// <summary>
        /// 容器数，只允许用户输入数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ContainerNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.txt_InputNumber_KeyPress(sender, e);
        }

        /// <summary>
        /// 清楚该窗口内的所有内容
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            this.clearControlValue(this.gb_Content);
        }

        /// <summary>
        /// 清空容器内控件值
        /// </summary>
        private void clearControlValue(Control parContainer)
        {
            for (int index = 0; index < parContainer.Controls.Count; index++)
            {
                //如果本身也是容器类控件，则递归调用自己
                if (parContainer.Controls[index].HasChildren)
                {
                    clearControlValue(parContainer.Controls[index]);
                }
                else
                {
                    string controlName = parContainer.Controls[index].GetType().Name;
                    switch (controlName)
                    {
                        case "TextBox":
                            (parContainer.Controls[index] as TextBox).Text = "";
                            break;
                        case "DataGridView":
                            /*
                            for (int i = 0; i < (parContainer.Controls[index] as DataGridView).Rows.Count; i++)
                            {
                                (parContainer.Controls[index] as DataGridView).Rows[i].Cells.Clear();
                            }
                             * */
                            (parContainer.Controls[index] as DataGridView).Rows.Clear();
                            break;
                        case "ComboBox":
                            (parContainer.Controls[index] as ComboBox).Text = "";
                            //(parContainer.Controls[index] as ComboBox).Items.Clear();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 过账
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Post_Click(object sender, EventArgs e)
        {
            new actdocument().Show();
        }


        /// <summary>
        /// 当事务操作选项index变化时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_YDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.clearControlValue(this.gb_Content);
            this.cbb_SHType.Items.Clear();
            this.cbb_SHType.Text = "";

            this.cbb_GR.Items.Clear();
            this.cbb_GR.Text = "";
            int index = this.cbb_YDType.SelectedIndex;
            switch (index)
            {
                    //收货
                case 0:
                    //根据采购订单进行收货
                    this.cbb_SHType.Items.Add("采购订单");
                    //根据运输单进行收货
                    this.cbb_SHType.Items.Add("运输单");
                    //根据交货单进行收货
                    this.cbb_SHType.Items.Add("交货单");
                    //在刚采用此系统时，比如历史遗留的库存，我们可以根据"其他"来进行收货，将以前的库存信息输入到该系统中
                    this.cbb_SHType.Items.Add("其他");
                    //将移动类型名称置为空
                    this.lb_MoveTypeDetails.Text = "";
                    //加载收货相关的移动类型
                    this.bindMoveTypeData("收货");
                    break;
                    //退货
                case 1:
                    this.cbb_SHType.Items.Add("其他");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("退货交货");
                    break;
                    //取消
                case 2:
                    this.cbb_SHType.Items.Add("物料凭证");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("取消");
                    break;
                    //显示
                case 3:
                    this.cbb_SHType.Items.Add("物料凭证");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("显示");
                    break;
                    //发货
                case 4:
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("发货");
                    break;
                    //转移过账
                case 5:
                    this.cbb_SHType.Items.Add("其它");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("转移过账");
                    break;
                    //出库
                case 6:
                    this.cbb_SHType.Items.Add("其它");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("出库");
                    break;
                    //入库
                case 7:
                    this.cbb_SHType.Items.Add("物料凭证");
                    this.cbb_SHType.Items.Add("其它");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("入库");
                    break;
                    //报废
                case 8:
                    this.cbb_SHType.Items.Add("其它");
                    this.lb_MoveTypeDetails.Text = "";
                    this.bindMoveTypeData("报废");
                    break;
            }
        }

        /// <summary>
        /// 给移动类型绑定数据
        /// </summary>
        /// <param name="transName">事务/事件名称</param>
        private void bindMoveTypeData(string transName)
        {
            try
            {
                List<MoveTypeExtendModel> moveTypeExtendModleList = moveTypeInfoBLL.findALLMoveTypeInfoByTransName(transName);
                if (moveTypeExtendModleList != null)
                {
                    foreach (MoveTypeExtendModel moveTypeExtendModel in moveTypeExtendModleList)
                    {
                        this.cbb_GR.Items.Add(moveTypeExtendModel.MT_ID);
                    }
                }
                else
                {
                    //MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 当移动类型选择变化时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_GR_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(this.cbb_GR.Text);
            string selectedValue = this.cbb_GR.Text.Trim(); //获取当前移动类型编码
            try
            {
                MoveTypeExtendModel moveTypeExtendModel = moveTypeInfoBLL.findMoveTypeInfoByID(selectedValue);
                string moveTypeDetails = moveTypeExtendModel.MT_Name;
                this.lb_MoveTypeDetails.Text = moveTypeDetails;
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }

            //对分支判断语句
            switch (selectedValue)
            {
                case "321":
                    this.btn_EFSearch.Visible = false;
                    this.btn_ESSearch.Visible = false;
                    break;
                case "341":
                    this.btn_EFSearch.Visible = false;
                    this.btn_ESSearch.Visible = false;
                    break;
                case "349":
                    this.btn_EFSearch.Visible = false;
                    this.btn_ESSearch.Visible = false;
                    break;
                default:
                    this.btn_EFSearch.Visible = true;
                    this.btn_ESSearch.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// 当凭证选择改变时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_SHType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((0 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //MessageBox.Show("hello world");
                    //this.tp_TransferAccount.Hide();
                    this.txt_JHBill.ReadOnly = false;
                    this.txt_THBill.ReadOnly = false;
                    this.txt_SHF.ReadOnly = false;
                    this.txt_XHD.ReadOnly = false;
                    this.txt_POID.Visible = true;
                    this.btn_confirm.Visible = true;
                    this.dgv_MaterialInfo.Visible = true;
                    this.btn_MultiAdd.Visible = false;
                    this.tc_Details.Location = new Point(14, 421);
                    this.tp_TransferAccount.Parent = null; //隐藏"转移过账"选项卡
                    this.tp_Supplier.Parent = this.tabc_info;//显示"供应商"选项卡
                    this.tp_Classification.Parent = this.tc_Details;//显示"分类"选项卡
                    this.tp_StockAddress.Parent = this.tc_Details;//显示"库存地"选项卡

                }
                //退货
                else if ((1== this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = true;
                    this.txt_THBill.ReadOnly = true;
                    this.txt_SHF.ReadOnly = true;
                    this.txt_XHD.ReadOnly = true;
                    this.txt_POID.Visible = false;
                    this.btn_confirm.Visible = false;
                    this.btn_MultiAdd.Visible = true;
                    //this.tp_TransferAccount.Parent = this.tc_Details; //显示"转移过账"选项卡
                    this.tc_Details.SelectedIndex = 4;
                    this.tp_Supplier.Parent = null;//隐藏"供应商"选项卡
                    this.tp_Classification.Parent = null; //隐藏"分类"选项卡
                    //this.tp_StockAddress.Parent = null; //隐藏"库存地"选项卡
                    // MessageBox.Show("hello world");
                }
                else if ((2 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = false;
                    this.txt_THBill.ReadOnly = false;
                    this.txt_SHF.ReadOnly = false;
                    this.txt_XHD.ReadOnly = false;
                    this.txt_POID.Visible = true;
                    this.btn_confirm.Visible = true;
                    this.dgv_MaterialInfo.Visible = true;
                    this.btn_MultiAdd.Visible = false;
                    this.tp_Supplier.Parent = this.tabc_info;//显示"供应商"选项卡
                    this.tp_Classification.Parent = this.tc_Details;//显示"分类"选项卡
                    this.tp_StockAddress.Parent = this.tc_Details;//显示"库存地"选项卡
                }
                else if ((3 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = false;
                    this.txt_THBill.ReadOnly = false;
                    this.txt_SHF.ReadOnly = false;
                    this.txt_XHD.ReadOnly = false;
                    this.txt_POID.Visible = true;
                    this.btn_confirm.Visible = true;
                    this.dgv_MaterialInfo.Visible = true;
                    this.btn_MultiAdd.Visible = false;
                    this.tp_Supplier.Parent = this.tabc_info;//显示"供应商"选项卡
                    this.tp_Classification.Parent = this.tc_Details;//显示"分类"选项卡
                    this.tp_StockAddress.Parent = this.tc_Details;//显示"库存地"选项卡
                    this.tp_TransferAccount.Parent = this.tc_Details; //显示"转移过账"选项卡
                }
                //转移凭证
                else if ((5 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = true;
                    this.txt_THBill.ReadOnly = true;
                    this.txt_SHF.ReadOnly = true;
                    this.txt_XHD.ReadOnly = true;
                    this.txt_POID.Visible = false;
                    this.btn_confirm.Visible = false;
                    this.btn_MultiAdd.Visible = true;
                    //this.tp_TransferAccount.Parent = this.tc_Details; //显示"转移过账"选项卡
                    this.tc_Details.SelectedIndex = 4;
                    this.tp_Supplier.Parent = null;//隐藏"供应商"选项卡
                    this.tp_Classification.Parent = null; //隐藏"分类"选项卡
                    //this.tp_StockAddress.Parent = null; //隐藏"库存地"选项卡
                    // MessageBox.Show("hello world");
                }
                else if ((6 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = true;
                    this.txt_THBill.ReadOnly = true;
                    //this.txt_InputNumber.ReadOnly = true;
                    //this.txt_DeliveryNumber.ReadOnly = true;
                    this.txt_POID.Visible = false;
                    this.btn_confirm.Visible = false;
                    this.dgv_MaterialInfo.Visible = true;
                    this.btn_MultiAdd.Visible = true;
                    this.tc_Details.Location = new Point(14, 421);
                    //转移过账->从
                    this.txt_TMaterialID.ReadOnly = true;
                    this.txt_TMaterialName.ReadOnly = true;
                    this.txt_TStockID.ReadOnly = true;
                    this.txt_TStockName.ReadOnly = true;
                    this.txt_TFactoryID.ReadOnly = true;
                    this.txt_TFactoryName.ReadOnly = true;
                    this.btn_TFind.Visible = false;

                    //转移过账->目标
                    this.txt_EMaterialID.ReadOnly = true;
                    this.txt_EMaterialName.ReadOnly = true;
                    //this.dgv_MaterialInfo.Visible = false;
                    //this.tc_Details.Location = new Point(14, 230);
                    this.tp_TransferAccount.Parent = this.tc_Details; //显示"转移过账"选项卡
                    this.tc_Details.SelectedIndex = 4;
                    this.tp_Supplier.Parent = null;//隐藏"供应商"选项卡
                    this.tp_Classification.Parent = null; //隐藏"分类"选项卡
                    this.tp_StockAddress.Parent = null; //隐藏"库存地"选项卡
                    // MessageBox.Show("hello world");
                }
                else if ((7 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    //MessageBox.Show("hello world");
                    //this.tp_TransferAccount.Hide();
                    this.txt_POID.Visible = true;
                    this.btn_confirm.Visible = true;
                    this.dgv_MaterialInfo.Visible = true;
                    this.btn_MultiAdd.Visible = false;   //禁用批量添加按钮
                    this.tc_Details.Location = new Point(14, 421);
                    this.tp_TransferAccount.Parent = this.tc_Details; //隐藏"转移过账"选项卡
                    this.tp_Supplier.Parent = null; //隐藏"供应商"选项卡
                    this.tp_StockAddress.Parent = null; //隐藏"库存地"选项卡
                }
                //报废
                else if ((8 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                {
                    this.txt_JHBill.ReadOnly = true;
                    this.txt_THBill.ReadOnly = true;
                    this.txt_SHF.ReadOnly = true;
                    this.txt_XHD.ReadOnly = true;
                    this.txt_POID.Visible = false;
                    this.btn_confirm.Visible = false;
                    this.btn_MultiAdd.Visible = true;
                    //this.tp_TransferAccount.Parent = this.tc_Details; //显示"转移过账"选项卡
                    this.tc_Details.SelectedIndex = 4;
                    this.tp_Supplier.Parent = null;//隐藏"供应商"选项卡
                    this.tp_Classification.Parent = null; //隐藏"分类"选项卡
                    //this.tp_StockAddress.Parent = null; //隐藏"库存地"选项卡
                    // MessageBox.Show("hello world");
                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 查找物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_TFind_Click(object sender, EventArgs e)
        {
            new Frm_InventoryInfo(this, this.cbb_GR.Text, 1).ShowDialog();
        }

        /// <summary>
        /// "转移过账"选项卡中的绑定物料
        /// </summary>
        /// <param name="inventoryExtendModel"></param>
        /// <param name="moveTypeId"></param>
        public void stockTrans(InventoryExtendModel inventoryExtendModel, string moveTypeId)
        {
            if (moveTypeId.Equals("321"))
            {
                //从
                this.txt_TMaterialName.ReadOnly = true;
                this.txt_TMaterialID.ReadOnly = true;
                this.txt_TFactoryID.ReadOnly = true;
                this.txt_TFactoryName.ReadOnly = true;
                this.txt_TStockID.ReadOnly = true;
                this.txt_TStockName.ReadOnly = true;

                this.txt_TMaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_TMaterialID.Text = inventoryExtendModel.Material_ID;
                this.txt_TFactoryID.Text = inventoryExtendModel.Factory_ID;
                this.txt_TFactoryName.Text = inventoryExtendModel.Factory_Name;
                this.txt_TStockID.Text = inventoryExtendModel.Stock_ID;
                this.txt_TStockName.Text = inventoryExtendModel.Stock_Name;

                //目标
                this.txt_EMaterialName.ReadOnly = true;
                this.txt_EMaterialID.ReadOnly = true;
                this.txt_EFactoryName.ReadOnly = true;
                this.txt_EFactoryID.ReadOnly = true;
                this.txt_EStockID.ReadOnly = true;
                this.txt_EStockName.ReadOnly = true;

                this.txt_EMaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_EMaterialID.Text = inventoryExtendModel.Material_ID;
                this.txt_EFactoryName.Text = inventoryExtendModel.Factory_Name;
                this.txt_EFactoryID.Text = inventoryExtendModel.Factory_ID;
                this.txt_EStockID.Text = inventoryExtendModel.Stock_ID;
                this.txt_EStockName.Text = inventoryExtendModel.Stock_Name;

                //物料
                this.txt_MaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_MaterialID.Text = inventoryExtendModel.Material_ID;
                this.txt_MaterialGroup.Text = inventoryExtendModel.Material_Group;
                this.cbb_EvaluateType.Text = inventoryExtendModel.Evaluate_Type;

                //何处
                this.txt_SHF.ReadOnly = true;
                this.txt_XHD.ReadOnly = true;
                this.txt_FactoryName.Text = inventoryExtendModel.Factory_Name;
                this.txt_FactoryID.Text = inventoryExtendModel.Factory_ID;

                //数量
                this.txt_SupplierMaterialID.ReadOnly = true;
                this.txt_InputMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_SKUMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_DeliveryMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_ContainerMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_ContainerNumber.Text = inventoryExtendModel.Count.ToString();
            }
            else if (moveTypeId.Equals("301"))
            {
                //从
                this.txt_TMaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_TMaterialID.Text = inventoryExtendModel.Material_ID;
                this.txt_TFactoryID.Text = inventoryExtendModel.Factory_ID;
                this.txt_TFactoryName.Text = inventoryExtendModel.Factory_Name;
                this.txt_TStockID.Text = inventoryExtendModel.Stock_ID;
                this.txt_TStockName.Text = inventoryExtendModel.Stock_Name;

                //目标
                this.txt_EMaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_EMaterialID.Text = inventoryExtendModel.Material_ID;

                //物料
                this.txt_MaterialName.Text = inventoryExtendModel.Material_Name;
                this.txt_MaterialID.Text = inventoryExtendModel.Material_ID;
                this.txt_MaterialGroup.Text = inventoryExtendModel.Material_Group;
                this.cbb_EvaluateType.Text = inventoryExtendModel.Evaluate_Type;

                //何处
                this.txt_SHF.ReadOnly = true;
                this.txt_XHD.ReadOnly = true;
                this.cbb_StockType.Text = inventoryExtendModel.Inventory_State;
                this.txt_FactoryName.Text = inventoryExtendModel.Factory_Name;
                this.txt_FactoryID.Text = inventoryExtendModel.Factory_ID;

                //数量
                this.txt_SupplierMaterialID.ReadOnly = true;
                this.txt_InputMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_SKUMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_DeliveryMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_ContainerMeasureUnit.Text = inventoryExtendModel.SKU;
                this.txt_ContainerNumber.Text = inventoryExtendModel.Count.ToString();
            }
        }

        /// <summary>
        /// 查找工厂，库存转移
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_EFSearch_Click(object sender, EventArgs e)
        {
            new Frm_SelectFactory(this, 2).ShowDialog();
        }

        /// <summary>
        /// 查找库存地点，库存转移
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ESSearch_Click(object sender, EventArgs e)
        {
            if ("".Equals(this.txt_EFactoryID.Text.Trim()) || "".Equals(this.txt_EFactoryName.Text.Trim()))
            {
                MessageBox.Show("请先选择工厂", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                new Frm_SelectStock(this, this.txt_EFactoryID.Text.Trim()).ShowDialog();
            }
        }

        /// <summary>
        /// 设置"转移过账选项卡"中的仓库名称和ID
        /// </summary>
        /// <param name="stockName"></param>
        /// <param name="stockId"></param>
        public void setSStockIdAndName(string stockName, string stockId)
        {
            this.txt_EStockID.Text = stockId;
            this.txt_EStockName.Text = stockName;
        }

        /// <summary>
        /// 库存移动->出库->批量添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_MultiAdd_Click(object sender, EventArgs e)
        {
            new Frm_InventoryInfo(this, this.cbb_GR.Text, 2).ShowDialog();
        }
        /// <summary>
        /// 批量添加后的数据绑定到详细列表中
        /// </summary>
        /// <param name="inventoryExtendModelList"></param>
        public void multiAddBind(List<InventoryExtendModel> inventoryExtendModelList)
        {
            //情况dgv中所有的数据
            this.dgv_MaterialInfo.Rows.Clear();
            int i = 0;
            foreach (InventoryExtendModel inventoryExtendModel in inventoryExtendModelList)
            {
                dgv_MaterialInfo.Rows.Add(1);
                dgv_MaterialInfo.Rows[i].Cells["Col_MaterialID"].Value = inventoryExtendModel.Material_ID;
                dgv_MaterialInfo.Rows[i].Cells["Col_MaterialName"].Value = inventoryExtendModel.Material_Name;
                dgv_MaterialInfo.Rows[i].Cells["Col_Factory"].Value = inventoryExtendModel.Factory_ID;
                dgv_MaterialInfo.Rows[i].Cells["Col_StockGround"].Value = inventoryExtendModel.Stock_ID;
                dgv_MaterialInfo.Rows[i].Cells["Col_MeasureUnit"].Value = inventoryExtendModel.SKU;
                dgv_MaterialInfo.Rows[i].Cells["Col_Number"].Value = inventoryExtendModel.Count;
                dgv_MaterialInfo.Rows[i].Cells["Col_StockType"].Value = inventoryExtendModel.Inventory_State;
                dgv_MaterialInfo.Rows[i].Cells["Col_Batch"].Value = inventoryExtendModel.Batch_ID;
                dgv_MaterialInfo.Rows[i].Cells["Col_Operation"].Value = "删除";
                i++;
            }
        }
        /// <summary>
        /// 当点击单元格内容时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //点击操作在有效数据范围内
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (0 == e.ColumnIndex)
                {
                    DialogResult result = MessageBox.Show("您确定删除此条数据吗?", "温馨提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (DialogResult.OK == result)
                    {
                        //删除当前选中的行
                        this.dgv_MaterialInfo.Rows.RemoveAt(e.RowIndex);
                        this.dgv_MaterialInfo.Rows.Add(1);
                    }
                }
            }
        }

        /// <summary>
        /// 当点击任意的单元格时发生，目的就是每次点击一行时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
                #endregion


        #region 公共函数操作

        /// <summary>
        /// 加载订单信息
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <param name="orderItemList"></param>
        private void loadOrderInfo(OrderInfoExtendModel orderInfo, List<OrderItemExtendModel> orderItemList)
        {

            //加载订单的头信息
            if (orderInfo != null)
            {
                this.txt_Supplier.Text = orderInfo.SupplierName;
                this.txt_SupplierID.Text = orderInfo.Supplier_ID;
                this.txt_SupplierName.Text = orderInfo.SupplierName;
                this.txt_SupplierAddress.Text = orderInfo.Address;
                this.txt_ZipCode.Text = orderInfo.Zip_Code;
            }
            //加载订单详细数据项
            if (orderItemList != null && orderItemList.Count > 0)
            {
                OrderItemExtendModel orderItem;
                DataGridViewRow currentRow;
                //循环遍历加载订单数据项
                for (int i = 0; i < orderItemList.Count; i++)
                {
                    orderItem = orderItemList[i];
                    currentRow = this.dgv_MaterialInfo.Rows[i];
                    currentRow.Cells["Col_Operation"].Value = "删除";
                    currentRow.Cells["Col_MaterialID"].Value = orderItem.Material_ID;
                    currentRow.Cells["Col_MaterialName"].Value = orderItem.Material_Name;
                    currentRow.Cells["Col_OrderCount"].Value = orderItem.Number;
                    currentRow.Cells["Col_MeasureUnit"].Value = orderItem.Unit;
                    currentRow.Cells["Col_UnitPrice"].Value = orderItem.Net_Price;
                    currentRow.Cells["Col_Factory"].Value = orderItem.Factory_ID;
                    currentRow.Cells["Col_StockGround"].Value = orderItem.Stock_ID;
                    currentRow.Cells["Col_Batch"].Value = orderItem.Batch_ID;
                }
            }
        }

        /// <summary>
        /// 加载物料凭证数据
        /// </summary>
        /// <param name="mDocModel"></param>
        /// <param name="mDocItemList"></param>
        public void loadMDocInfo(MaterialDocumentNewExtendModel mDocModel, List<DocumentDetailInfoModelExtend> mDocItemList)
        {
            //加载订单的头信息
            if (mDocModel != null)
            {
                this.dtp_VoucherDate.Text = mDocModel.Document_Date.ToShortDateString();
                this.dtp_AccountDate.Text = mDocModel.Posting_Date.ToShortDateString();
                this.txt_JHBill.Text = mDocModel.ReceiptNote_ID;
                this.txt_THBill.Text = mDocModel.Delivery_ID;
                this.txt_StockManager.Text = mDocModel.StockManager;
            }
            //加载订单详细数据项
            if (mDocItemList != null && mDocItemList.Count > 0)
            {
                DocumentDetailInfoModelExtend mDocItem;
                DataGridViewRow currentRow;
                //循环遍历加载订单数据项
                for (int i = 0; i < mDocItemList.Count; i++)
                {
                    mDocItem = mDocItemList[i];
                    currentRow = this.dgv_MaterialInfo.Rows[i];
                    currentRow.Cells["Col_Operation"].Value = "删除";
                    currentRow.Cells["Col_MaterialID"].Value = mDocItem.SMaterial_ID;
                    //currentRow.Cells["Col_MaterialName"].Value = mDocItem;
                    currentRow.Cells["Col_Number"].Value = mDocItem.Input_Count;
                    currentRow.Cells["Col_OrderCount"].Value = mDocItem.Order_Count;
                    currentRow.Cells["Col_MeasureUnit"].Value = mDocItem.Unit;
                    currentRow.Cells["Col_UnitPrice"].Value = mDocItem.UnitPrice;
                    currentRow.Cells["Col_Factory"].Value = mDocItem.SFactory_ID;
                    currentRow.Cells["Col_StockGround"].Value = mDocItem.SStock_ID;
                    currentRow.Cells["Col_Batch"].Value = mDocItem.Batch_ID;
                    currentRow.Cells["Col_EvaluationType"].Value = mDocItem.Evaluate_Type;
                    currentRow.Cells["Col_StockType"].Value = mDocItem.Inventory_Type;
                    currentRow.Cells["Col_ReceiptNoteID"].Value = mDocItem.ReceiptNote_ID;
                    currentRow.Cells["Col_DeliveryID"].Value = mDocItem.Delivery_ID;
                    currentRow.Cells["Col_SupplierID"].Value = mDocItem.Supplier_ID;
                    currentRow.Cells["Col_InputCount"].Value = mDocItem.Input_Count;
                    currentRow.Cells["Col_SKUCount"].Value = mDocItem.SKU_Count;
                    currentRow.Cells["Col_NotesCount"].Value = mDocItem.Notes_Count;
                    currentRow.Cells["Col_SupplierMaterialID"].Value = mDocItem.SupplierMaterial_ID;
                    currentRow.Cells["Col_MaterialGroup"].Value = mDocItem.Material_Group;
                    currentRow.Cells["Col_Tolerance"].Value = mDocItem.Tolerance;
                    currentRow.Cells["Col_LodingPlace"].Value = mDocItem.Loading_Place;
                    currentRow.Cells["Col_ReceivingPlace"].Value = mDocItem.Receiving_Place;
                    currentRow.Cells["Col_OrderID"].Value = mDocItem.Order_ID;
                    currentRow.Cells["Col_TMaterialID"].Value = mDocItem.TMaterial_ID;
                    currentRow.Cells["Col_TMaterialName"].Value = mDocItem.TMaterial_Name;
                    currentRow.Cells["Col_TFactoryID"].Value = mDocItem.TFactory_ID;
                    currentRow.Cells["Col_TFactoryName"].Value = mDocItem.TFactory_Name;
                    currentRow.Cells["Col_TStockID"].Value = mDocItem.TStock_ID;
                    currentRow.Cells["Col_TStockName"].Value = mDocItem.TStock_Name;
                    currentRow.Cells["Col_SpecialStock"].Value = mDocItem.SpecialStock;
                    currentRow.Cells["Col_ExplainText"].Value = mDocItem.ExplainText;
                }
            }
        }
        #endregion 

        /// <summary>
        /// 当datagridview里面值变换时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                /*
                //获取当前行
                DataGridViewRow currentRow = this.dgv_MaterialInfo.CurrentRow;
                //判断该列的值是否为空
                if (currentRow.Cells["Col_MaterialID"].Value != null)
                {
                    //取得物料ID的长度，取的时候去掉空格
                    string materialId = currentRow.Cells["Col_MaterialID"].Value.ToString().Trim();
                    if (materialId.Length <= 0)
                    {
                        //若物料ID为空，则操作处内容也为空
                        currentRow.Cells["Col_Operation"].Value = "";
                    }
                    else
                    {
                        currentRow.Cells["Col_Operation"].Value = "   删除";
                    }
                }
                else
                {
                    currentRow.Cells["Col_Operation"].Value = "";
                }
                 * */
            }
        }

        StockFactoryModel stockFactoryInfo = null;
        StockInfoExtendModel stockInfo = null;

        /// <summary>
        /// 修改订单具体信息
        /// 双击DataGridView单元格时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
             //点击处要在第2列开始才有效
            if (e.RowIndex >= 0 && e.ColumnIndex > 0)
            {
                //获取当前行
                currentRow = this.dgv_MaterialInfo.CurrentRow;
                string materialId = "";
                //保证必须要有物料编号才能操作
                if (currentRow.Cells["Col_MaterialID"].Value != null && !"".Equals(currentRow.Cells["Col_MaterialID"].Value.ToString().Trim()))
                {
                    materialId = currentRow.Cells["Col_MaterialID"].Value.ToString().Trim();   //获得material编码
                    #region 收货-采购订单
                    if ((0 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))    //收货-采购订单
                    {
                        //详细-物料
                        this.txt_MaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);

                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        //详细-库存地
                        string factoryId = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.txt_FactoryID.Text = factoryId;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryId);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_FactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockId = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_StockAddressId.Text = stockId;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockId);
                        if (stockInfo != null)
                        {
                            this.txt_StockAddressName.Text = stockInfo.Stock_Name;
                        }
                        //详细-批次
                        this.txt_BatchId.Text = currentRow.Cells["Col_Batch"].Value.ToString();
                    }
                    #endregion

                    #region 取消-其他
                    if ((1 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))    //收货-采购订单
                    {
                        //详细-物料
                        this.txt_MaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //详细-库存地
                        string factoryIdT = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.cbb_StockType.Text = currentRow.Cells["Col_StockType"].Value.ToString();
                        this.txt_FactoryID.Text = factoryIdT;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdT);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_FactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdT = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_StockAddressId.Text = stockIdT;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdT);
                        if (stockInfo != null)
                        {
                            this.txt_StockAddressName.Text = stockInfo.Stock_Name;
                        }
                    }
                    #endregion 

                    #region 显示-物料凭证
                    if ((3== this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                    {
                        //详细-物料
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //转移过账
                        this.txt_TMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_TMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        //this.txt_EMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_EMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        string factoryIdCK = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.txt_TFactoryID.Text = factoryIdCK;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdCK);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_TFactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdCK = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_TStockID.Text = stockIdCK;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdCK);
                        if (stockInfo != null)
                        {
                            this.txt_TStockName.Text = stockInfo.Stock_Name;
                        }
                        this.txt_EMaterialName.Text = currentRow.Cells["Col_TMaterialID"].Value.ToString();
                        this.txt_EMaterialID.Text = currentRow.Cells["Col_TMaterialName"].Value.ToString();
                        this.txt_EFactoryName.Text = currentRow.Cells["Col_TFactoryName"].Value.ToString();
                        this.txt_EFactoryID.Text = currentRow.Cells["Col_TFactoryID"].Value.ToString();
                        this.txt_EStockName.Text = currentRow.Cells["Col_TStockName"].Value.ToString();
                        this.txt_EMaterialID.Text = currentRow.Cells["Col_TStockID"].Value.ToString();
                        this.txt_TSpecialStock.Text = currentRow.Cells["Col_SpecialStock"].Value.ToString();
                    }
                    #endregion
                    
                    #region 转移过账-其他
                    if ((5 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))    //收货-采购订单
                    {
                        //详细-物料
                        this.txt_MaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //详细-库存地
                        string factoryIdT = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.txt_FactoryID.Text = factoryIdT;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdT);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_FactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdT = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_StockAddressId.Text = stockIdT;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdT);
                        if (stockInfo != null)
                        {
                            this.txt_StockAddressName.Text = stockInfo.Stock_Name;
                        }
                    }
                    #endregion 

                    #region 出库-其它
                    if ((6 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                    {
                        //详细-物料
                        this.txt_MaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //转移过账
                        this.txt_TMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_TMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_EMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_EMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        string factoryIdCK = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.txt_TFactoryID.Text = factoryIdCK;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdCK);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_TFactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdCK = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_TStockID.Text = stockIdCK;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdCK);
                        if (stockInfo != null)
                        {
                            this.txt_TStockName.Text = stockInfo.Stock_Name;
                        }
                    }
                    #endregion

                    #region 入库-物料凭证
                    if ((7 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))
                    {
                        //详细-物料
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //转移过账
                        this.txt_TMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_TMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        //this.txt_EMaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        //this.txt_EMaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        string factoryIdCK = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.txt_TFactoryID.Text = factoryIdCK;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdCK);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_TFactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdCK = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_TStockID.Text = stockIdCK;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdCK);
                        if (stockInfo != null)
                        {
                            this.txt_TStockName.Text = stockInfo.Stock_Name;
                        }
                        this.txt_EMaterialName.Text = currentRow.Cells["Col_TMaterialID"].Value.ToString();
                        this.txt_EMaterialID.Text = currentRow.Cells["Col_TMaterialName"].Value.ToString();
                        this.txt_EFactoryName.Text = currentRow.Cells["Col_TFactoryName"].Value.ToString();
                        this.txt_EFactoryID.Text = currentRow.Cells["Col_TFactoryID"].Value.ToString();
                        this.txt_EStockName.Text = currentRow.Cells["Col_TStockName"].Value.ToString();
                        this.txt_EMaterialID.Text = currentRow.Cells["Col_TStockID"].Value.ToString();
                        this.txt_TSpecialStock.Text = currentRow.Cells["Col_SpecialStock"].Value.ToString();
                    }
                    #endregion

                    #region 报废-其他
                    if ((8 == this.cbb_YDType.SelectedIndex) && (0 == this.cbb_SHType.SelectedIndex))    //收货-采购订单
                    {
                        //详细-物料
                        this.txt_MaterialName.Text = currentRow.Cells["Col_MaterialID"].Value.ToString();
                        this.txt_MaterialID.Text = currentRow.Cells["Col_MaterialName"].Value.ToString();
                        this.txt_MaterialGroup.Text = this.getMaterialBLL.getMaterialGroup(materialId);
                        this.cbb_EvaluateType.Text = currentRow.Cells["Col_EvaluationType"].Value == null ? "" : currentRow.Cells["Col_EvaluationType"].Value.ToString().Trim();  //评估类型
                        //详细-数量
                        this.txt_InputMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        this.txt_SKUMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_DeliveryMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderMeasureUnit.Text = currentRow.Cells["Col_MeasureUnit"].Value.ToString();
                        //this.txt_OrderNumber.Text = currentRow.Cells["Col_OrderCount"].Value.ToString();
                        this.txt_InputNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        this.txt_SKUNumber.Text = currentRow.Cells["Col_Number"].Value == null ? "" : currentRow.Cells["Col_Number"].Value.ToString().Trim();
                        //详细-库存地
                        string factoryIdT = currentRow.Cells["Col_Factory"].Value.ToString(); //获取factoryId
                        this.cbb_StockType.Text = currentRow.Cells["Col_StockType"].Value.ToString();
                        this.txt_FactoryID.Text = factoryIdT;
                        stockFactoryInfo = stockFactoryInfoBLL.getFactoryInfoBy(factoryIdT);
                        if (stockFactoryInfo != null)
                        {
                            this.txt_FactoryName.Text = stockFactoryInfo.Factory_Name;
                        }
                        string stockIdT = currentRow.Cells["Col_StockGround"].Value.ToString(); //获取stockId
                        this.txt_StockAddressId.Text = stockIdT;
                        stockInfo = stockInfoBLL.getStockInfoByStockId(stockIdT);
                        if (stockInfo != null)
                        {
                            this.txt_StockAddressName.Text = stockInfo.Stock_Name;
                        }
                    }
                    #endregion 
                }
            }
        }

        #region 物料详细-数量

        /// <summary>
        /// 已输入单位计的数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_InputNumber_TextChanged(object sender, EventArgs e)
        {
            //SKU中的数量
            this.txt_SKUNumber.Text = this.txt_InputNumber.Text;
            //已收数量
            this.txt_YSNumber.Text = this.txt_InputNumber.Text;

            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_InputCount"].Value = this.txt_InputNumber.Text;
                currentRow.Cells["Col_Number"].Value = this.txt_InputNumber.Text;
            }
        }

        /// <summary>
        /// SKU中的数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SKUNumber_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_SKUCount"].Value = this.txt_SKUNumber.Text;
            }
        }

        /// <summary>
        /// 交货注释中的数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_DeliveryNumber_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_NotesCount"].Value = this.txt_DeliveryNumber.Text;
            }
        }

        /// <summary>
        /// 订单中的数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_OrderNumber_TextChanged(object sender, EventArgs e)
        {
                //暂时为空
        }

        /// <summary>
        /// 已收数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_YSNumber_TextChanged(object sender, EventArgs e)
        {
            //暂时为空，已收数量等同于SKU数量
        }

        #endregion

        #region 物料详细-物料

        /// <summary>
        /// 供应商物料编号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SupplierMaterialID_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_SupplierMaterialID"].Value = this.txt_SupplierMaterialID.Text;
            }
        }

        /// <summary>
        /// 物料组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_MaterialGroup_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_MaterialGroup"].Value = this.txt_MaterialGroup.Text;
            }
        }

        /// <summary>
        /// 评估类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_EvaluateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_EvaluationType"].Value = this.cbb_EvaluateType.Text;
            }
        }

        #endregion

        #region 物料详细-库存地
        /// <summary>
        /// 收货方
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SHF_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_NotesCount"].Value = this.txt_SHF.Text;
            }
        }

        /// <summary>
        /// 卸货点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_XHD_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_LodingPlace"].Value = this.txt_XHD.Text;
            }
        }

        /// <summary>
        /// 文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Context_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_ExplainText"].Value = this.txt_Context.Text;
            }
        }

        /// <summary>
        /// 库存类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_StockType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_StockType"].Value = this.cbb_StockType.Text;
            }
        }

        #endregion

        #region 物料详细-批次号

        /// <summary>
        /// 批次号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_BatchId_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_Batch"].Value = this.txt_BatchId.Text;
            }
        }

        #endregion

        #region 物料详细-转移过账
        /// <summary>
        /// 目标-物料名称
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EMaterialName_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TMaterialName"].Value = this.txt_EMaterialName.Text;
            }
        }

        /// <summary>
        /// 目标-物料ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EMaterialID_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TMaterialID"].Value = this.txt_EMaterialID.Text;
            }
        }

        /// <summary>
        /// 目标-工厂名称
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EFactoryName_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TFactoryName"].Value = this.txt_EFactoryName.Text;
            }
        }

        /// <summary>
        /// 目标-工厂ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EFactoryID_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TFactoryID"].Value = this.txt_EFactoryID.Text;
            }
        }

        /// <summary>
        /// 目标-库存名称
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EStockName_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TStockName"].Value = this.txt_EStockName.Text;
            }
        }

        /// <summary>
        /// 目标-库存ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EStockID_TextChanged(object sender, EventArgs e)
        {
            if (currentRow != null)   //有当前行
            {
                currentRow.Cells["Col_TStockID"].Value = this.txt_EStockID.Text;
            }
        }
        #endregion 

        private void btn_CreateBatch_Click(object sender, EventArgs e)
        {
            BatchFeature batchFeature = new BatchFeature();
            batchFeature.ShowDialog();
        }

    }
}
