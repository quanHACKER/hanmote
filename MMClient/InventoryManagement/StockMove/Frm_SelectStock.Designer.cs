﻿namespace MMClient.InventoryManagement
{
    partial class Frm_SelectStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Condition = new System.Windows.Forms.GroupBox();
            this.lb_Unit = new System.Windows.Forms.Label();
            this.lb_Number = new System.Windows.Forms.Label();
            this.lb_SearchResult = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txt_Stock = new System.Windows.Forms.TextBox();
            this.lb_Stock = new System.Windows.Forms.Label();
            this.txt_Factory = new System.Windows.Forms.TextBox();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.dgv_StockInfoDetail = new System.Windows.Forms.DataGridView();
            this.Col_StockID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Condition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockInfoDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_Condition
            // 
            this.gb_Condition.Controls.Add(this.lb_Unit);
            this.gb_Condition.Controls.Add(this.lb_Number);
            this.gb_Condition.Controls.Add(this.lb_SearchResult);
            this.gb_Condition.Controls.Add(this.btn_Search);
            this.gb_Condition.Controls.Add(this.txt_Stock);
            this.gb_Condition.Controls.Add(this.lb_Stock);
            this.gb_Condition.Controls.Add(this.txt_Factory);
            this.gb_Condition.Controls.Add(this.lb_Factory);
            this.gb_Condition.Location = new System.Drawing.Point(7, 0);
            this.gb_Condition.Name = "gb_Condition";
            this.gb_Condition.Size = new System.Drawing.Size(753, 81);
            this.gb_Condition.TabIndex = 0;
            this.gb_Condition.TabStop = false;
            // 
            // lb_Unit
            // 
            this.lb_Unit.Location = new System.Drawing.Point(705, 40);
            this.lb_Unit.Name = "lb_Unit";
            this.lb_Unit.Size = new System.Drawing.Size(20, 12);
            this.lb_Unit.TabIndex = 14;
            this.lb_Unit.Text = "条";
            // 
            // lb_Number
            // 
            this.lb_Number.Location = new System.Drawing.Point(690, 41);
            this.lb_Number.Name = "lb_Number";
            this.lb_Number.Size = new System.Drawing.Size(15, 12);
            this.lb_Number.TabIndex = 13;
            this.lb_Number.Text = "0";
            // 
            // lb_SearchResult
            // 
            this.lb_SearchResult.AutoSize = true;
            this.lb_SearchResult.Location = new System.Drawing.Point(629, 40);
            this.lb_SearchResult.Name = "lb_SearchResult";
            this.lb_SearchResult.Size = new System.Drawing.Size(65, 12);
            this.lb_SearchResult.TabIndex = 12;
            this.lb_SearchResult.Text = "搜索结果：";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(490, 31);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(97, 26);
            this.btn_Search.TabIndex = 11;
            this.btn_Search.Text = "搜索";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_Stock
            // 
            this.txt_Stock.Location = new System.Drawing.Point(280, 34);
            this.txt_Stock.Name = "txt_Stock";
            this.txt_Stock.Size = new System.Drawing.Size(165, 21);
            this.txt_Stock.TabIndex = 5;
            this.txt_Stock.Enter += new System.EventHandler(this.txt_Stock_Enter);
            this.txt_Stock.Leave += new System.EventHandler(this.txt_Stock_Leave);
            // 
            // lb_Stock
            // 
            this.lb_Stock.AutoSize = true;
            this.lb_Stock.Location = new System.Drawing.Point(233, 39);
            this.lb_Stock.Name = "lb_Stock";
            this.lb_Stock.Size = new System.Drawing.Size(41, 12);
            this.lb_Stock.TabIndex = 4;
            this.lb_Stock.Text = "仓库：";
            // 
            // txt_Factory
            // 
            this.txt_Factory.Location = new System.Drawing.Point(53, 35);
            this.txt_Factory.Name = "txt_Factory";
            this.txt_Factory.Size = new System.Drawing.Size(157, 21);
            this.txt_Factory.TabIndex = 3;
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(9, 39);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(41, 12);
            this.lb_Factory.TabIndex = 2;
            this.lb_Factory.Text = "工厂：";
            // 
            // dgv_StockInfoDetail
            // 
            this.dgv_StockInfoDetail.AllowUserToAddRows = false;
            this.dgv_StockInfoDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_StockInfoDetail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_StockInfoDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_StockInfoDetail.ColumnHeadersHeight = 28;
            this.dgv_StockInfoDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_StockInfoDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_StockID,
            this.Col_StockName,
            this.Col_Address,
            this.Col_Factory});
            this.dgv_StockInfoDetail.EnableHeadersVisualStyles = false;
            this.dgv_StockInfoDetail.Location = new System.Drawing.Point(7, 101);
            this.dgv_StockInfoDetail.Name = "dgv_StockInfoDetail";
            this.dgv_StockInfoDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_StockInfoDetail.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_StockInfoDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_StockInfoDetail.RowTemplate.Height = 23;
            this.dgv_StockInfoDetail.RowTemplate.ReadOnly = true;
            this.dgv_StockInfoDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_StockInfoDetail.Size = new System.Drawing.Size(752, 312);
            this.dgv_StockInfoDetail.TabIndex = 1;
            this.dgv_StockInfoDetail.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_StockInfoDetail_CellDoubleClick);
            this.dgv_StockInfoDetail.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_StockInfoDetail_CellMouseEnter);
            this.dgv_StockInfoDetail.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_StockInfoDetail_CellMouseLeave);
            this.dgv_StockInfoDetail.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_StockInfoDetail_RowPostPaint);
            // 
            // Col_StockID
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Col_StockID.DefaultCellStyle = dataGridViewCellStyle2;
            this.Col_StockID.HeaderText = "仓库编号";
            this.Col_StockID.Name = "Col_StockID";
            this.Col_StockID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockID.Width = 150;
            // 
            // Col_StockName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Col_StockName.DefaultCellStyle = dataGridViewCellStyle3;
            this.Col_StockName.HeaderText = "仓库名称";
            this.Col_StockName.Name = "Col_StockName";
            this.Col_StockName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockName.Width = 150;
            // 
            // Col_Address
            // 
            this.Col_Address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Col_Address.DefaultCellStyle = dataGridViewCellStyle4;
            this.Col_Address.HeaderText = "所在地址";
            this.Col_Address.Name = "Col_Address";
            this.Col_Address.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_Factory
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Col_Factory.DefaultCellStyle = dataGridViewCellStyle5;
            this.Col_Factory.HeaderText = "所属工厂";
            this.Col_Factory.Name = "Col_Factory";
            this.Col_Factory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_Factory.Width = 150;
            // 
            // Frm_SelectStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 425);
            this.Controls.Add(this.dgv_StockInfoDetail);
            this.Controls.Add(this.gb_Condition);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_SelectStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "请选择库存点";
            this.gb_Condition.ResumeLayout(false);
            this.gb_Condition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockInfoDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Condition;
        private System.Windows.Forms.TextBox txt_Factory;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.TextBox txt_Stock;
        private System.Windows.Forms.Label lb_Stock;
        private System.Windows.Forms.Label lb_Unit;
        private System.Windows.Forms.Label lb_Number;
        private System.Windows.Forms.Label lb_SearchResult;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.DataGridView dgv_StockInfoDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Factory;
    }
}