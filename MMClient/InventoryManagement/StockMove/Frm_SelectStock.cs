﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_SelectStock : Form
    {
        StockInfoBLL stockInfoBLL = new StockInfoBLL();
        Frm_Receiving frm_Receiving = null;

        /// <summary>
        /// 常量
        /// </summary>
        private readonly string STR_TXT_STOCK = "仓库编码/仓库名称";
        private string factory_Id = "";

        public Frm_SelectStock()
            :this(null,null)
        { 
            //空的构造方法
        }

        /// <summary>
        /// 有参构造方法
        /// </summary>
        /// <param name="factoryId"></param>
        public Frm_SelectStock(Frm_Receiving frm_Receiving,string factoryId)
        {
            InitializeComponent();
            this.factory_Id = factoryId;
            this.txt_Factory.Text = factoryId;
            this.frm_Receiving = frm_Receiving;
            this.dgv_StockInfoDetail.TopLeftHeaderCell.Value = "行号";

            //当窗体加载时，让其焦点在标签上
            this.lb_Factory.Select();
            this.txt_Stock.Text = this.STR_TXT_STOCK;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_Stock.ForeColor = System.Drawing.Color.LightGray;
        }

        /// <summary>
        /// 点击搜索按钮时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            //每次点击前清空dgv
            this.dgv_StockInfoDetail.Rows.Clear();
            try
            {
                List<StockInfoExtendModel> stockInfoList = stockInfoBLL.findStockInfoByFactoryId(this.factory_Id,this.txt_Stock.Text.Trim());
                int count = 0;
                if (stockInfoList != null)
                {
                    count = stockInfoList.Count;
                    int i = 0;
                    foreach (StockInfoExtendModel stockInfoExtendModel in stockInfoList)
                    {
                        this.dgv_StockInfoDetail.Rows.Add(1);
                        this.dgv_StockInfoDetail.Rows[i].Cells["Col_StockID"].Value = stockInfoExtendModel.Stock_ID;
                        this.dgv_StockInfoDetail.Rows[i].Cells["Col_StockName"].Value = stockInfoExtendModel.Stock_Name;
                        this.dgv_StockInfoDetail.Rows[i].Cells["Col_Address"].Value = stockInfoExtendModel.Address;
                        this.dgv_StockInfoDetail.Rows[i].Cells["Col_Factory"].Value = stockInfoExtendModel.Factory_ID;
                        i++;
                    }
                }
                else
                {
                    count = 0;
                    MessageBox.Show("没有找到相应的仓库","温馨提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                this.lb_Number.Text = count.ToString();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 双击单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockInfoDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //获得仓库名称
            string stockName = this.dgv_StockInfoDetail.CurrentRow.Cells["Col_StockName"].Value.ToString();
            //获得仓库ID
            string stockId = this.dgv_StockInfoDetail.CurrentRow.Cells["Col_StockID"].Value.ToString();

            //将值传回父窗体
            this.frm_Receiving.setSStockIdAndName(stockName, stockId);
            this.Close();
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockInfoDetail_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_StockInfoDetail.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_StockInfoDetail.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockInfoDetail_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_StockInfoDetail.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_StockInfoDetail.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockInfoDetail_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入"仓库"输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Stock_Enter(object sender, EventArgs e)
        {
            if (this.txt_Stock.Text.Trim().Equals(STR_TXT_STOCK))
            {
                //将textbox内容置空
                this.txt_Stock.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_Stock.ForeColor = System.Drawing.Color.Black;
            }
        }

        /// <summary>
        /// 当鼠标离开"仓库"输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Stock_Leave(object sender, EventArgs e)
        {
            if (this.txt_Stock.Text.Trim() == "")
            {
                this.txt_Stock.Text = STR_TXT_STOCK;
                this.txt_Stock.ForeColor = System.Drawing.Color.LightGray;
            }
        }
    }
}
