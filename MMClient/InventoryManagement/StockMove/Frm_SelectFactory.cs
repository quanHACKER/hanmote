﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_SelectFactory : Form
    {
        StockFactoryInfoBLL stockFactoryInfoBLL = new StockFactoryInfoBLL();
        StockInfoBLL stockInfoBLL = new StockInfoBLL();
        Frm_Receiving frm_Receiving = null;
        int whichFactory = 0;

        public Frm_SelectFactory()
            : this(null,0)
        {
            //空的构造方法
        }

        public Frm_SelectFactory(Frm_Receiving frm_Receiving, int whichFactory)
        {
            InitializeComponent();

            this.frm_Receiving = frm_Receiving;
            this.whichFactory = whichFactory;
            this.dgv_FactoryInfo.TopLeftHeaderCell.Value = "行号";
        }

        /// <summary>
        ///点击搜索按钮时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            //每次点击前清空dgv
            this.dgv_FactoryInfo.Rows.Clear();

            try
            {
                List<StockFactoryModel> stockFactoryList = stockFactoryInfoBLL.findAllFactoryInfoBy();
                int count = stockFactoryList.Count;
                int i = 0;
                foreach (StockFactoryModel stockFactoryModel in stockFactoryList)
                {
                    dgv_FactoryInfo.Rows.Add(1);
                    dgv_FactoryInfo.Rows[i].Cells["Factory_ID"].Value = stockFactoryModel.Factory_ID;
                    dgv_FactoryInfo.Rows[i].Cells["Factory_Name"].Value = stockFactoryModel.Factory_Name;
                    dgv_FactoryInfo.Rows[i].Cells["Factory_Address"].Value = "武汉光谷";
                    i++;
                }
                this.lb_Number.Text = count.ToString();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 双击单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_FactoryInfo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //获得工厂名称
                string factoryName = this.dgv_FactoryInfo.CurrentRow.Cells["Factory_Name"].Value.ToString();
                //获得工厂编码
                string factoryId = this.dgv_FactoryInfo.CurrentRow.Cells["Factory_ID"].Value.ToString();

                if (whichFactory == 1)
                {
                    List<StockInfoExtendModel> stockInfoList = stockInfoBLL.findStockInfoByFactoryId(factoryId);
                    //将值传回父窗体
                    this.frm_Receiving.setSFactoryNameAndId(factoryName, factoryId,whichFactory);
                    this.frm_Receiving.setStockSelection(stockInfoList);
                }
                if (whichFactory == 2)
                {
                    this.frm_Receiving.setSFactoryNameAndId(factoryName, factoryId, whichFactory);
                }
                //双击时关闭
                this.Close();
            }
            else
            {
                MessageBox.Show("请选择正确窗体！", "温馨提示", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_FactoryInfo_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_FactoryInfo_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_FactoryInfo.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_FactoryInfo.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_FactoryInfo_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_FactoryInfo.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_FactoryInfo.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }


        private void button1_MouseHover(object sender, EventArgs e)
        {
            //this.button1.BackgroundImage = MMClient.Properties.Resources.leftArrow2;
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            //this.button1.BackgroundImage = MMClient.Properties.Resources.leftArrow;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            //this.button1.BackgroundImage = MMClient.Properties.Resources.leftArrow;
        }
    }
}
