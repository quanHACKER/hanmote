﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL.StockReport;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_StockOverview : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_StockOverview()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("请填写物料编码");
                return;
            }

            string materialid = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            string factoryid = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            string stockid = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
            string batchid = comboBox5.SelectedItem == null ? "" : comboBox5.SelectedItem.ToString().Trim();
            bool specialstock = checkBox1.Checked;
            bool nonzero = checkBox2.Checked;
            bool company = checkBox3.Checked;
            bool factory = checkBox4.Checked;
            bool stock = checkBox5.Checked;
            bool batch = checkBox6.Checked;
            bool Inventorytype = checkBox7.Checked;

            DataTable dt = new DataTable("dt");
            dt.Columns.Add("materialid", typeof(string));
            dt.Columns.Add("factoryid", typeof(string));
            dt.Columns.Add("stockid", typeof(string));
            dt.Columns.Add("batchid", typeof(string));
            dt.Columns.Add("specialstock", typeof(bool));
            dt.Columns.Add("nonzero", typeof(bool));
            dt.Columns.Add("company", typeof(bool));
            dt.Columns.Add("factory", typeof(bool));
            dt.Columns.Add("stock", typeof(bool));
            dt.Columns.Add("batch", typeof(bool));
            dt.Columns.Add("Inventorytype", typeof(bool));

            DataRow dr = dt.NewRow();
            dr["materialid"] = materialid;
            dr["factoryid"] = factoryid;
            dr["stockid"] = stockid;
            dr["batchid"] = batchid;
            dr["specialstock"] = specialstock;
            dr["nonzero"] = nonzero;
            dr["company"] = company;
            dr["factory"] = factory;
            dr["stock"] = stock;
            dr["batch"] = batch;
            dr["Inventorytype"] = Inventorytype;
            dt.Rows.Add(dr);

            StockOverviewBLL sob = new StockOverviewBLL();
            DataTable dt0 = sob.selectInfo(dt);

            if (dt0 == null)
            {
                MessageBox.Show("暂无符合条件物料，请检查输入是否正确");
                return;
            }
            Frm_StockOverviewShow frm = new Frm_StockOverviewShow(dt0);
            frm.Show();


        }

        private void Frm_StockOverview_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            List<string> blist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            DataTable dt2 = gmb.getbatchid();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                blist.Insert(i, dt2.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox2, flist);
            comb.FuzzyQury(comboBox1, mlist);
            comb.FuzzyQury(comboBox3, slist);
            comb.FuzzyQury(comboBox5, blist);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = null;
            string materialid = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            if (materialid != "")
            {
                string materialname;
                getMaterialInfoBLL gmib = new getMaterialInfoBLL();
                try
                {
                    materialname = gmib.getMaterialName(materialid);
                }
                catch
                {
                    materialname = "";
                }
                this.textBox1.Text = materialname;
            }
        }
    }
}
