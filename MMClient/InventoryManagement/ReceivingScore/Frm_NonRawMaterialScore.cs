﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_NonRawMaterialScore : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        DateTimePicker dtp = new DateTimePicker();  //这里实例化一个DateTimePicker控件  
        Rectangle _Rectangle;
        public Frm_NonRawMaterialScore()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
            dataGridView1.Controls.Add(dtp);  //把时间控件加入DataGridView  
            dtp.Visible = false;  //先不让它显示  
            dtp.Format = DateTimePickerFormat.Custom;  //设置日期格式为2010-08-05  
            dtp.TextChanged += new EventHandler(dtp_TextChange); //为时间控件加入事件
        }

        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Text.ToString();  //时间控件选择时间时，就把时间赋给所在的单元格  
        }

         /***********当列的宽度变化时，时间控件先隐藏起来，不然单元格变大时间控件无法跟着变大哦***********/
        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;

        }

        /***********滚动条滚动时，单元格位置发生变化，也得隐藏时间控件，不然时间控件位置不动就乱了********/
        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }  


        private void button1_Click(object sender, EventArgs e)
        {
            ReceiveScoreDAL rsd = new ReceiveScoreDAL();
            int n = dataGridView1.RowCount;
            if (n > 0)
            {
                dataGridView1.Visible = true;
                int i;
                for (i = 0; i < n ; i++)
                {
                    if (dataGridView1.Rows[i].Cells[4].Value != DBNull.Value)
                    {
                        int m = Convert.ToInt32(dataGridView1.Rows[i].Cells[0].Value.ToString());
                        decimal score = Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        DateTime ct = Convert.ToDateTime(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        string cn = dataGridView1.Rows[i].Cells[6].Value.ToString();
                        rsd.updatetable3(m, score, ct, cn);
                    }
                }
                MessageBox.Show("评分成功");
            }

            else
            {
                MessageBox.Show("暂无待评分物料");
                dataGridView1.Visible = false;
            }
        }

        private void Frm_NonRawMaterialScore_Load(object sender, EventArgs e)
        {
            ReceiveScoreDAL rsd = new ReceiveScoreDAL();
            DataTable dt = rsd.filltable3();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
        }

        /****************单元格被单击，判断是否是放时间控件的那一列*******************/
        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {

                _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //得到所在单元格位置和大小  
                dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height); //把单元格大小赋给时间控件  
                dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件  
                dtp.Visible = true;  //可以显示控件了  
            }
            else
                dtp.Visible = false;
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
    }
}


