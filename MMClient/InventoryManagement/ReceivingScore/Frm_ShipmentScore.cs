﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShipmentScore : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        DateTimePicker dtp = new DateTimePicker();  //这里实例化一个DateTimePicker控件  
        Rectangle _Rectangle;

        public Frm_ShipmentScore()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
            dataGridView1.Controls.Add(dtp);  //把时间控件加入DataGridView  
            dtp.Visible = false;  //先不让它显示  
            dtp.Format = DateTimePickerFormat.Custom;  //设置日期格式为2010-08-05  
            dtp.TextChanged += new EventHandler(dtp_TextChange); //为时间控件加入事件
        }

        private void Frm_ShipmentScore_Load(object sender, EventArgs e)
        {
            ReceiveScoreDAL rsd = new ReceiveScoreDAL();

            DataTable dt1 = rsd.filltable2();
            if (dt1.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReceiveScoreDAL rsd = new ReceiveScoreDAL();
            int n = dataGridView1.RowCount;
            if (n >0)
            {
                dataGridView1.Visible = true;
                for (int i = 0; i < n ; i++)
                {
                    if (dataGridView1.Rows[i].Cells[4].Value != DBNull.Value)
                    {
                        decimal dcm = Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        string s = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        rsd.updatetable2(dcm, s);
                    }
                }
                MessageBox.Show("评分成功");

            }
            else
            {
                MessageBox.Show("暂无待评估供应商");
                dataGridView1.Visible = false;
            }
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {

                _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //得到所在单元格位置和大小  
                dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height); //把单元格大小赋给时间控件  
                dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件  
                dtp.Visible = true;  //可以显示控件了  
            }
            else
                dtp.Visible = false;
        }

        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Text.ToString();  //时间控件选择时间时，就把时间赋给所在的单元格  
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }

        private void Frm_ShipmentScore_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
