﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_SaftyStockSet : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_SaftyStockSet()
        {
            InitializeComponent();
        }

        private void SaftyInventoryForm_Load(object sender, EventArgs e)
        {
            SaftyInventoryDAL sid = new SaftyInventoryDAL();
            DataTable dt = sid.fillInput();
            if(dt.Rows.Count>0)
            dataGridView1.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
           

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SaftyInventoryDAL sid = new SaftyInventoryDAL();
            int n = dataGridView1.RowCount;
            if (n > 0)
            {
                dataGridView1.Visible = true;
                for (int i = 0; i < n ; i++)
                {
                    if (dataGridView1.Rows[i].Cells[2].Value != DBNull.Value)
                    {
                        float f = (float)Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value.ToString());
                        string s1 = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        string s2 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        sid.updateInputCount(s1, s2, f);
                    }
                }
                MessageBox.Show("设置成功");
            }
            else
            {
                MessageBox.Show("暂无待设置物料");
                dataGridView1.Visible = false;
            }
        }


    }
}
