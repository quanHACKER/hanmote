﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_SaftyStockComp : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_SaftyStockComp()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Frm_SaftyStockComp_Load(object sender, EventArgs e)
        {
            SaftyInventoryDAL sid = new SaftyInventoryDAL();
            
            DataTable dt1 = sid.fillCompute();
            dataGridView1.DataSource = dt1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaftyInventoryDAL sid = new SaftyInventoryDAL();
            int n = dataGridView1.RowCount;
            if (n >0)
            {
                dataGridView1.Visible = true;
                for (int i = 0; i < n ; i++)
                {
                    if (dataGridView1.Rows[i].Cells[2].Value != DBNull.Value && dataGridView1.Rows[i].Cells[3].Value != DBNull.Value && dataGridView1.Rows[i].Cells[4].Value != DBNull.Value)
                    {
                        dataGridView1.Visible = true;
                        float f1 = (float)Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value.ToString());
                        int f2 = (int)Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        int f3 = (int)Convert.ToDouble(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        string s1 = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        string s2 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        if (f2 < f3)
                        {
                            MessageBox.Show("缺货次数应不大于订货次数", "提示");
                            return;
                        }
                        double r = sid.computeSaftyFactor(f2, f3);
                        DataTable dtdemand = sid.selectDemand(s1, s2);
                        double mad = sid.computeMAD(dtdemand);

                        if (sid.madExist(s1, s2))
                            sid.updateComputeCount(s1, s2, mad, f3, r);
                        else
                        {
                            StringBuilder strbld = new StringBuilder("请先维护");
                            strbld.Append(dataGridView1.Rows[i].Cells[0].Value.ToString());
                            strbld.Append("工厂");
                            strbld.Append(dataGridView1.Rows[i].Cells[1].Value.ToString());
                            strbld.Append("物料需求信息");
                            
                            MessageBox.Show(strbld.ToString(),"提示");
                            return;
                        }
                    }

                }
                MessageBox.Show("计算成功");
            }
            else
            {
                MessageBox.Show("暂无待计算物料");
                dataGridView1.Visible = false;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
