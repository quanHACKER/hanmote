﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShowSaftyStock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_ShowSaftyStock()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string materialid = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString();
            string factoryid = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString();

            SaftyInventoryBLL sib = new SaftyInventoryBLL();
            DataTable dt = sib.showSaftyStock(factoryid,materialid);
            this.dataGridView1.DataSource = dt;


        }

        private void Frm_ShowSaftyStock_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            SaftyInventoryBLL gmb = new SaftyInventoryBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, flist);
            comb.FuzzyQury(comboBox2, mlist);
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }

        }
    }
}
