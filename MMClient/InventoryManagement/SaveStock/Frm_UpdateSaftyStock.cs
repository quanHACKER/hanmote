﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_UpdateSaftyStock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_UpdateSaftyStock()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string updatetype = this.comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString();
            if (updatetype == "")
            {
                MessageBox.Show("请选择更新类型");
                return;
            }
            else if (updatetype == "更新手动设置的安全库存")
            {
                this.label5.Visible = false;
                this.label6.Visible = false;
                this.label7.Visible = false;
                this.textBox2.Visible = false;
                this.textBox3.Visible = false;
            }
            else if (updatetype == "更新系统计算的安全库存")
            {
                this.label4.Visible = false;
                this.label5.Visible = true;
                this.label6.Visible = true;
                this.label7.Visible = true;
                this.textBox2.Visible = true;
                this.textBox3.Visible = true;
             }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string updatetype = this.comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            string factoryid = this.comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            string materialid = this.comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString();
            float count;
            double cycle;
            int ordertimes;
            int shorttimes;

            if (updatetype == "")
            {
                MessageBox.Show("请选择更新的类型","提示");
                return;
            }

            if (factoryid == "")
            {
                MessageBox.Show("请选择工厂编号", "提示");
                return;
            }

            if (materialid == "")
            {
                MessageBox.Show("请选择物料编号", "提示");
                return;
            }
            
            
            try
            {
                Convert.ToDouble(this.textBox1.Text.Trim());
            }
            catch
            {
                if (updatetype == "更新手动设置的安全库存")
                {
                    MessageBox.Show("安全库存请填数值型", "提示");
                    return;
                }
                else {
                    MessageBox.Show("供应周期请填数值型","提示");
                    return;
                }
            }

            if (updatetype == "更新系统计算的安全库存")
            {
                try
                {
                    Convert.ToInt32(this.textBox2.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("缺货次数请填整数", "提示");
                    return;
                }

                try
                {
                    Convert.ToInt32(this.textBox3.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("订货次数请填整数", "提示");
                    return;
                }
                cycle = Convert.ToDouble(textBox1.Text.Trim());
                shorttimes = Convert.ToInt32(textBox2.Text.Trim());
                ordertimes = Convert.ToInt32(textBox3.Text.Trim());

                if (shorttimes > ordertimes)
                {
                    MessageBox.Show("缺货次数应不大于订货次数", "提示");
                    return;
                }

                SaftyInventoryBLL sib = new SaftyInventoryBLL();
                DataTable dtdemand = sib.selectDemand(factoryid, materialid);

                double mad = sib.computeMAD(dtdemand);
                double r = sib.computeSaftyFactor(ordertimes, shorttimes);
                sib.updateComputeCount(factoryid, materialid, mad, cycle, r);
                MessageBox.Show("更新成功", "提示");
            }



            else if (updatetype == "更新手动设置的安全库存")
            {

                count = (float)Convert.ToDouble(textBox1.Text.Trim());

                SaftyInventoryBLL sib = new SaftyInventoryBLL();
                sib.updateInputCount(factoryid, materialid, count);
                MessageBox.Show("更新成功", "提示");

            }

            else {

                MessageBox.Show("输入有误请检查", "提示");
            }
           
        }

        private void Frm_UpdateSaftyStock_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            SaftyInventoryBLL gmb = new SaftyInventoryBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox2, flist);
            comb.FuzzyQury(comboBox3, mlist);
        }
    }
}
