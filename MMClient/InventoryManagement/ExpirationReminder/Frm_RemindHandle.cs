﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_RemindHandle : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_RemindHandle()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void Frm_RemindHandle_Load(object sender, EventArgs e)
        {
            remindDAL rd = new remindDAL();
            DataTable dt1 = rd.reminder();
            if (dt1.Rows.Count != 0)
            //MessageBox.Show("近期暂无要到期物料", "提示");
            //else
            {
                this.dataGridView1.DataSource = dt1;
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                {
                    dataGridView1.Rows[i].Cells[6].Value = DBNull.Value;
                }
                this.label1.Visible = false;
                this.label2.Visible = true;
                this.label3.Visible = true;
            }

            else
            {
                dataGridView1.Visible = false;
                button1.Visible = false;
                this.label1.Visible = true;
                this.label2.Visible = false;
                this.label3.Visible = false;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = dataGridView1.RowCount;
            if (n >0)
            {
                for (int i = 0; i < n ; i++)
                {
                            string factoryid ;;
                            string stockid ;
                            string materialid ;
                            string batchid ;
                            string prolongdays;
                    if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[0].Value.ToString().Trim()) == true)
                    {
                        try {
                            dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                            dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                            dataGridView1.Rows[i].Cells[3].Value.ToString().Trim();
                            dataGridView1.Rows[i].Cells[4].Value.ToString().Trim();
                        }
                        catch
                        {
                            MessageBox.Show("请将物料信息填写完整");
                        }

                        if (dataGridView1.Rows[i].Cells[6].Value != null || dataGridView1.Rows[i].Cells[6].Value.ToString().Trim() != "")
                        {
                            prolongdays = "";
                        
                        }
                        else
                        {
                            try
                            {
                                int days = Convert.ToInt32(dataGridView1.Rows[i].Cells[6].Value.ToString().Trim());
                            }
                            catch
                            {
                                MessageBox.Show("延长时间应填整数型值");
                            }
                        }

                        factoryid = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                        stockid = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                        materialid = dataGridView1.Rows[i].Cells[3].Value.ToString().Trim();
                        batchid = dataGridView1.Rows[i].Cells[4].Value.ToString().Trim();
                        prolongdays = dataGridView1.Rows[i].Cells[6].Value.ToString().Trim();
                        remindDAL rd = new remindDAL();
                        rd.handle(factoryid, stockid, materialid, batchid, prolongdays);
                    }
                }
                MessageBox.Show("处理成功");
               this.Close();
               //this.Show();
            }
            else
            {
               //MessageBox.Show("暂无待设置物料");
              //  dataGridView1.Visible = false;
            }

         
           
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
