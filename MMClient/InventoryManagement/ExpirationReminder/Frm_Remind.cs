﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_Remind : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        DateTimePicker dtp = new DateTimePicker();  //这里实例化一个DateTimePicker控件  
        Rectangle _Rectangle;

        public Frm_Remind()
        {
            InitializeComponent();

            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
            dataGridView1.Controls.Add(dtp);  //把时间控件加入DataGridView  
            dtp.Visible = false;  //先不让它显示  
            dtp.Format = DateTimePickerFormat.Custom;  //设置日期格式为2010-08-05  
            dtp.TextChanged += new EventHandler(dtp_TextChange); //为时间控件加入事件
        }


        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Text.ToString();  //时间控件选择时间时，就把时间赋给所在的单元格  
        }

        /****************单元格被单击，判断是否是放时间控件的那一列*******************/
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 4)
            {
                _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //得到所在单元格位置和大小  
                dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height); //把单元格大小赋给时间控件  
                dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件  
                dtp.Visible = true;  //可以显示控件了  
            }
            else
                dtp.Visible = false;
        }

        /***********当列的宽度变化时，时间控件先隐藏起来，不然单元格变大时间控件无法跟着变大哦***********/
        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;

        }

        /***********滚动条滚动时，单元格位置发生变化，也得隐藏时间控件，不然时间控件位置不动就乱了********/
        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }  



        private void remindForm_Load(object sender, EventArgs e)
        {
            remindDAL rd = new remindDAL();
            DataTable dt = rd.genTable();
            if (dt.Rows.Count > 0)
            {
                this.dataGridView1.DataSource = dt;
            }
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }



        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            int n = dataGridView1.RowCount;
            if (n > 0)
            {
                dataGridView1.Visible = true;
                for (int i = 0; i < n; i++)
                {
                    if (dataGridView1.Rows[i].Cells[5].Value != DBNull.Value)
                    {
                        dataGridView1.Visible = true;
                        remindDAL rd = new remindDAL();
                        string s0 = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        string s1 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        string s2 = dataGridView1.Rows[i].Cells[2].Value.ToString();
                        string s3 = dataGridView1.Rows[i].Cells[3].Value.ToString();
                        int s4 = Convert.ToInt32(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        rd.fillPreDay(s0, s1, s2, s3, s4);
                    }
                }
                MessageBox.Show("设置成功");
            }
            else
            {
                MessageBox.Show("暂无待设置物料");
                dataGridView1.Visible = false;
            }
            
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }


    }
}
