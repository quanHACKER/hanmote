﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement 
{
    public partial class Frm_AccountRemind : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_AccountRemind()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Frm_AccountRemind_Load(object sender, EventArgs e)
        {
            RemindBLL rmb = new RemindBLL();
            DataTable dt0 = rmb.accountManage();
            DateTime curtime = System.DateTime.Now;

            DataTable dt = new DataTable("dt");
                       
            dt.Columns.Add("物料编号", typeof(string));
            dt.Columns.Add("工厂编号", typeof(string));
            dt.Columns.Add("库存地", typeof(string));
            dt.Columns.Add("库存时间", typeof(int));

            if (dt0 != null)
            {
                for (int i = 0; i < dt0.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();

                    dr["物料编号"] = dt0.Rows[i][2];
                    dr["工厂编号"] = dt0.Rows[i][0];
                    dr["库存地"] = dt0.Rows[i][1];
                    dr["库存时间"] = (curtime - Convert.ToDateTime(dt0.Rows[i][3])).Days;

                    dt.Rows.Add(dr);
                }

                this.dataGridView1.DataSource = dt;
            }

        }
    }
}
