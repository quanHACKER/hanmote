﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Reflection;

namespace MMClient.InventoryManagement
{
    /// <summary>
    /// 与DataGridView有关的功用操作
    /// </summary>
    public class CommonMethod_AboutDGV
    {
        /// <summary>
        /// 减少DataGridView闪烁问题
        /// </summary>
        /// <param name="dgv">要操作的DataGridView名称</param>
        public static void cM_FlickerReduction(DataGridView dgv)
        {
            Type type = dgv.GetType();
            PropertyInfo pInfo = type.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pInfo.SetValue(dgv, true, null);
        }


    }
}
