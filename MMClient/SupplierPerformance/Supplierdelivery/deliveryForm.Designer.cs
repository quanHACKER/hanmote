﻿namespace MMClient.SupplierPerformance.Supplierdelivery
{
    partial class deliveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TabPage deliveryDate;
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.deliveryFormTab = new System.Windows.Forms.TabControl();
            this.OnTimeDelivery = new System.Windows.Forms.TabPage();
            this.dataReliable = new System.Windows.Forms.TabPage();
            this.deliveryknow = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            deliveryDate = new System.Windows.Forms.TabPage();
            deliveryDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.deliveryFormTab.SuspendLayout();
            this.deliveryknow.SuspendLayout();
            this.SuspendLayout();
            // 
            // deliveryDate
            // 
            deliveryDate.Controls.Add(this.panel1);
            deliveryDate.Location = new System.Drawing.Point(4, 22);
            deliveryDate.Name = "deliveryDate";
            deliveryDate.Padding = new System.Windows.Forms.Padding(3);
            deliveryDate.Size = new System.Drawing.Size(1224, 560);
            deliveryDate.TabIndex = 1;
            deliveryDate.Text = "确定交货日期";
            deliveryDate.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(48, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 0;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 38);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "supplierLabel";
            // 
            // deliveryFormTab
            // 
            this.deliveryFormTab.Controls.Add(this.OnTimeDelivery);
            this.deliveryFormTab.Controls.Add(deliveryDate);
            this.deliveryFormTab.Controls.Add(this.dataReliable);
            this.deliveryFormTab.Controls.Add(this.deliveryknow);
            this.deliveryFormTab.Location = new System.Drawing.Point(13, 13);
            this.deliveryFormTab.Name = "deliveryFormTab";
            this.deliveryFormTab.SelectedIndex = 0;
            this.deliveryFormTab.Size = new System.Drawing.Size(1232, 586);
            this.deliveryFormTab.TabIndex = 0;
            // 
            // OnTimeDelivery
            // 
            this.OnTimeDelivery.Location = new System.Drawing.Point(4, 22);
            this.OnTimeDelivery.Name = "OnTimeDelivery";
            this.OnTimeDelivery.Padding = new System.Windows.Forms.Padding(3);
            this.OnTimeDelivery.Size = new System.Drawing.Size(1224, 560);
            this.OnTimeDelivery.TabIndex = 0;
            this.OnTimeDelivery.Text = "及时交货表现";
            this.OnTimeDelivery.UseVisualStyleBackColor = true;
            // 
            // dataReliable
            // 
            this.dataReliable.Location = new System.Drawing.Point(4, 22);
            this.dataReliable.Name = "dataReliable";
            this.dataReliable.Padding = new System.Windows.Forms.Padding(3);
            this.dataReliable.Size = new System.Drawing.Size(1224, 560);
            this.dataReliable.TabIndex = 2;
            this.dataReliable.Text = "数据可靠性";
            this.dataReliable.UseVisualStyleBackColor = true;
            // 
            // deliveryknow
            // 
            this.deliveryknow.Controls.Add(this.label5);
            this.deliveryknow.Controls.Add(this.label4);
            this.deliveryknow.Controls.Add(this.label3);
            this.deliveryknow.Controls.Add(this.label2);
            this.deliveryknow.Location = new System.Drawing.Point(4, 22);
            this.deliveryknow.Name = "deliveryknow";
            this.deliveryknow.Padding = new System.Windows.Forms.Padding(3);
            this.deliveryknow.Size = new System.Drawing.Size(1224, 560);
            this.deliveryknow.TabIndex = 3;
            this.deliveryknow.Text = "装运须知的遵守";
            this.deliveryknow.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "供应商编号";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(561, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(196, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "物料组编号";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(368, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "label5";
            // 
            // deliveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 623);
            this.Controls.Add(this.deliveryFormTab);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "deliveryForm";
            this.Text = "交货评估窗口";
            deliveryDate.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.deliveryFormTab.ResumeLayout(false);
            this.deliveryknow.ResumeLayout(false);
            this.deliveryknow.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl deliveryFormTab;
        private System.Windows.Forms.TabPage OnTimeDelivery;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage dataReliable;
        private System.Windows.Forms.TabPage deliveryknow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}