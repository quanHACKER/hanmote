﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;

namespace MMClient.SupplierPerformance
{
    public partial class SystemProcessAuditForm : DockContent
    {
        List<Object> scoreList = new List<Object>();
        List<String> chapterInfo = new List<string>();//存放sheetNum和章节名称的对应关系
        //Decimal processAuditScore;
        //Decimal systemAuditScore;
        List<Object> savePSAuditPara = new List<object>();
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        String supplierID, materialID;
        DataTable supplierInfoTable, materialInfoTable;

        public SystemProcessAuditForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        /// <summary>
        /// 打开excel选择框
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        private List<Object> Import()
        {
            //打开excel选择框
            OpenFileDialog frm = new OpenFileDialog();
            frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
            if (frm.ShowDialog() == DialogResult.OK)
            {
                string excelName = frm.FileName;
                Workbook excel = new Workbook(excelName);
                scoreList = GetImportExcelRoute(excel);
            }
            return scoreList;
        }

        /// <summary>
        /// 读取excel
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        public List<Object> GetImportExcelRoute(Workbook excel)
        {
            String cellContent;
            Decimal score = 0;
            Decimal weightedCount = 0;
            String chapterInfoName = "";

            scoreList.Clear();
            chapterInfo.Clear();


            //循环读取每一张worksheet
            for (int sheetNum = 3; sheetNum <= 16; sheetNum++)
            {
                Worksheet sheet = excel.Worksheets[sheetNum];
                Cells cells = sheet.Cells;
                chapterInfoName = sheet.Name;
                
                int rowcount = cells.MaxRow;
                //int columncount = cells.MaxColumn;

                //每次循环之前将score和weightedCount置为0
                score = 0;
                weightedCount = 0;

                for (int j = 2; j <= rowcount; j++)
                {
                    for (int i = 2; i <= 6; i++)
                    {
                        //cells[j, i].StringValue.Trim()操作excel
                        cellContent = cells[j, i].StringValue.Trim();

                        //如果用户对该项无评分，则不计入分数
                        if ((i == 2) && (cellContent == "X"))
                        {
                            score = score + 0;
                            continue;
                        }
                        //若用户对该项的评分为3分，则分数加3，分母（weightedCount）加3
                        else if ((i == 3) && (cellContent == "X"))
                        {
                            score = score + 3;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为2分，则分数加2，分母（weightedCount）加3
                        else if ((i == 4) && (cellContent == "X"))
                        {
                            score = score + 2;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为1分，则分数加1，分母（weightedCount）加3
                        else if ((i == 5) && (cellContent == "X"))
                        {
                            score = score + 1;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为0分，则分数加0，分母（weightedCount）加3
                        else if ((i == 6) && (cellContent == "X"))
                        {
                            score = score + 0;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                    }
                }
                if (weightedCount == 0)
                {
                    scoreList.Add(null);
                }
                else
                {
                    score = (score / weightedCount) * 100;
                    scoreList.Add(score);
                }

                //if (chapterInfo.ContainsKey(sheetNum))
                //{
                //    chapterInfo[sheetNum] = chapterInfoName;
                //}
                //else
                //{
                //    chapterInfo.Add(sheetNum, chapterInfoName);
                //}
                chapterInfo.Add(chapterInfoName);
            }
            return scoreList;
        }

        /// <summary>
        /// 计算审核结果，并显示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_calculate_Click(object sender, EventArgs e)
        {
            scoreList.Clear();
            chapterInfo.Clear();

            scoreList = this.Import();

            //在dataGridView显示每一章节的分数
            for (int i = 0; i < 13; i++)
            {
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];
                
                //章节名称在dictonary类型的chapterInfo中保存时，其key为sheetNum，此处的i标识着scoreList的下标，因此需要i+3（因为excel的前三页不是打分页）
                row.Cells[0].Value = chapterInfo[i];

                if (i <= 5)
                {
                    row.Cells[1].Value = "过程审核";
                }
                else
                {
                    row.Cells[1].Value = "体系审核";
                }

                //分数
                if (scoreList[i] != null)
                {
                    row.Cells[2].Value = Math.Round(Convert.ToDecimal(scoreList[i]), 1);
                }
                else
                {
                    //让未评分的章节显示空，而不是0分
                    row.Cells[2].Value = scoreList[i];
                }
              
            }

            ////计算过程和体系审核的总分，并显示
            //int countPro = 0;
            //int countSys = 0;
            //for (int i = 0; i < 13; i++)
            //{
            //    if (i <= 5)
            //    {
            //        if (scoreList[i] != null)
            //        {
            //            processAuditScore = processAuditScore + Convert.ToDecimal(scoreList[i]);
            //            countPro++;
            //        }
                    
            //    }
            //    else
            //    {
            //        if (scoreList[i] != null)
            //        {
            //            systemAuditScore = systemAuditScore + Convert.ToDecimal(scoreList[i]);
            //            countSys++;
            //        }
            //    }
            //}

            //try
            //{
            //    processAuditScore = processAuditScore / countPro;
            //    systemAuditScore = systemAuditScore / countSys;
            //}
            //catch
            //{
            //    MessageBox.Show("请先完成过程/体系审核文件！");
            //    return;
            //}
            
            

            //textBox_processAudit.Text =  Math.Round(processAuditScore, 1).ToString();
            //textBox_systemAudit.Text = Math.Round(systemAuditScore, 1).ToString();
        }

        /// <summary>
        /// “保存”Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBox_materialID.Text.ToString()) || String.IsNullOrEmpty(comboBox_supplierID.Text.ToString()))
            {
                MessageBox.Show("请选择供应商代码和物料代码，若无对应的物料代码，请在主数据视图中维护！");
                return;
            }
            if (String.IsNullOrEmpty(textBox_Name.Text.ToString()))
            {
                MessageBox.Show("请输入维护人信息！");
                return;
            }
            if (scoreList.Count == 0 || chapterInfo.Count == 0)
            {
                MessageBox.Show("请计算各章节审核的分数！");
                return;
            }
            savePSAuditPara.Clear();
            savePSAuditPara.Add(comboBox_supplierID.Text.ToString());
            savePSAuditPara.Add(comboBox_materialID.Text.ToString());
            //chaptorInfo和scoreList作为参数单独传入
            savePSAuditPara.Add(Convert.ToDateTime(dateTimePicker1.Text));
            savePSAuditPara.Add(textBox_Name.Text.ToString());

            int lines = supplierPerformanceBLL.savePSAuditRecord(savePSAuditPara, chapterInfo, scoreList);
            if (lines > 0)
            {
                MessageBox.Show("过程/体系审核记录保存成功！");
            }
        }

        //供应商id
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();


            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //物料id
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// 下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownload_Click(object sender, EventArgs e)
        {//选择保存的文件夹
            //FolderBrowserDialog downloadFileFolder = new FolderBrowserDialog();
            //downloadFileFolder.Description = "请选择文件夹";
            ////允许用户在浏览的时候create new folder
            //downloadFileFolder.ShowNewFolderButton = false;

            //DialogResult result = downloadFileFolder.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    //格式: E://oil
            //    string selectedPath = downloadFileFolder.SelectedPath;
            //    selectedPath = selectedPath.Replace("\\", "/") + "/";

            //    List<string> remoteLocationList = new List<string>();
            //    //remoteLocationList.Add("FX/绩效评估模版/供应商体系过程审核模版.xls");
            //    try
            //    {
            //        remoteLocationList.Add("供应商绩效评估/模版/供应商体系过程审核模版.xls");
            //    }
            //    catch(Exception ex)
            //    {
            //        MessageBox.Show("文件下载失败！");
            //    }
                
            //    //remoteLocationList.Add("寻源文档/201604221650032016001/M20160422155901762.xls");
            //    //remoteLocationList.Add("文本文档/201601212057302016001/lqc_script.sql");
            //    Dictionary<string, string> downloadRLDic = new Dictionary<string, string>();
            //    foreach (string remoteLocation in remoteLocationList)
            //    {
            //        string[] arr = remoteLocation.Split('/');
            //        string fileName = arr[arr.Length - 1];
            //        string localLocation = selectedPath + fileName; 
            //        downloadRLDic.Add(remoteLocation, localLocation);
            //    }

            //    FTPTool ftp = FTPTool.getInstance();
            //    LinkedList<string> failList = ftp.download(downloadRLDic);
            //    if (failList.Count == 0)
            //    {
            //        MessageBox.Show("下载成功！");
            //    }
            //}
        }


        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "请选择文件";
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;
            string fileName = fileDialog.FileName;
            fileName = fileName.Replace("\\", "/");
            if (fileName.Equals(String.Empty))
                return;

            string safeFileName = fileDialog.SafeFileName;
            if (safeFileName.Length > 150)
            {
                MessageUtil.ShowError("文件名过长!");
                return;
            }

            String localFile = fileName;

            supplierID = comboBox_supplierID.Text.ToString();
            materialID = comboBox_materialID.Text.ToString();
            String remoteFile;

            //供应商ID为空时返回，不允许上传
            if (String.IsNullOrEmpty(supplierID))
            {
                MessageBox.Show("请选择供应商！");
                return;
            }

            string year = DateTime.Now.ToString("yyyy");
            string month = DateTime.Now.ToString("MM");
            string day = DateTime.Now.ToString("dd");

            if (String.IsNullOrEmpty(materialID))
            {
                remoteFile = "供应商绩效评估/资料/体系和过程审核/" + supplierID.ToString() + "/" + year + "-" + month + "-" + day + ".xlsx";
            }
            else
            {
                remoteFile = "供应商绩效评估/资料/体系和过程审核/" + supplierID.ToString() + "/" + materialID.ToString() + "/" + year + "-" + month + "-" + day + ".xlsx";
            }

            MessageBox.Show(remoteFile);
            FTPTool ftp = FTPTool.getInstance();
            bool failedList = ftp.upload(localFile, remoteFile);
            if (failedList)
            {
                MessageBox.Show("上传成功！");
            }
        }

    }
}
