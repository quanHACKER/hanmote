﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.DataRecord
{
    public partial class SupplierTurnoverForm : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        SupplierValueBLL supplierValueBLL = new SupplierValueBLL();
        DataTable supplierInfoTable;
        String assessDate = DateTime.Now.ToString();

        public SupplierTurnoverForm()
        {
            InitializeComponent();
            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;
        }


        /// <summary>
        /// 营业额保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, EventArgs e)
        {
            String supplierID = comboBox_supplierID.Text.ToString();
            String year = comboBox_year.Text.ToString();
            String turnover = textBox_turnover.Text.ToString();

            if (String.IsNullOrEmpty(supplierID) || String.IsNullOrEmpty(year) || String.IsNullOrEmpty(turnover))
            {
                MessageBox.Show("请输入完整信息！");
                return;
            }

            int lines = supplierValueBLL.saveSupplierTurnover(supplierID,year,turnover);
            if (lines > 0)
            {
                MessageBox.Show("保存成功！");
                return;
            }
            else
            {
                MessageBox.Show("保存失败！");
                return;
            }

        }
    }
}
