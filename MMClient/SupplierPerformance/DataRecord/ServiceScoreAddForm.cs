﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance
{
    public partial class ServiceScoreAddForm : DockContent
    {
        String supplierID;
        String materialID;
        String name;
        DateTime time;

        Decimal SScreativityScore, SSreliabilityScore, SSuserServiceScore;
        Decimal ESserviceQualityScore, ESservicePromptnessScore;

        DataTable supplierInfoTable, materialInfoTable;

        //将一般服务/支持相关数据录入数据库时，用一个list保存3项分数（之后作为参数传递）
        List<Object> saveSSPara = new List<object>();
        //将外部服务相关数据录入数据库时，用一个list保存2项分数（之后作为参数传递）
        List<Object> saveESPara = new List<object>();

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        
        /// <summary>
        ///加载用户维护服务相关分数的界面 
        /// </summary>
        public ServiceScoreAddForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        /// <summary>
        /// 保存一般服务支持和外部服务记录 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_saveServiceSupport_Click(object sender, EventArgs e)
        {
            #region 保存一般服务/支持分数
            supplierID = comboBox_supplierID.Text.ToString();
            materialID = comboBox_materialID.Text.ToString();
            time = Convert.ToDateTime(dateTimePicker1.Text);
            name = textBox_Name.Text.ToString();

            //判断用户输入信息是否完整
            if (String.IsNullOrEmpty(materialID) || String.IsNullOrEmpty(supplierID) || time == null || String.IsNullOrEmpty(name))
            {
                MessageBox.Show("请输入完整的信息！");
                return;
            }

            //判断填入的分数是否为0-100区间内的数字
            try
            {
                SScreativityScore = Convert.ToDecimal(textBox_SScreativity.Text);
                SSreliabilityScore = Convert.ToDecimal(textBox_SSreliability.Text);
                SSuserServiceScore = Convert.ToDecimal(textBox_SSuserService.Text);

                if (SScreativityScore < 0 || SScreativityScore > 100 || SSreliabilityScore < 0 || SSreliabilityScore > 100 || SSuserServiceScore < 0 || SSuserServiceScore > 100)
                {
                    MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请输入次标准的分数并检查已输入的分数是否为数字!");
                return;
            }

            //将数据表Service_Support中需写入的数据项依次添加到list类型的saveSSPara中
            saveSSPara.Clear();
            saveSSPara.Add(supplierID);
            saveSSPara.Add(materialID);
            saveSSPara.Add(SScreativityScore);
            saveSSPara.Add(SSreliabilityScore);
            saveSSPara.Add(SSuserServiceScore);
            saveSSPara.Add(time);
            saveSSPara.Add(name);

            //将一般服务/支持评分数据写入数据表
            int lines = supplierPerformanceBLL.saveServiceSupportRecord(saveSSPara);
            if (lines > 0)
            {
                MessageBox.Show("一般服务/支持数据录入成功！");
                return;
            }
            #endregion

        }

        /// <summary>
        /// 保存外部服务分数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_saveExternalService_Click(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            materialID = comboBox_materialID.Text.ToString();
            time = Convert.ToDateTime(dateTimePicker1.Text);
            name = textBox_Name.Text.ToString();

            //判断用户输入信息是否完整
            if (String.IsNullOrEmpty(materialID) || String.IsNullOrEmpty(supplierID) || time == null || String.IsNullOrEmpty(name))
            {
                MessageBox.Show("请输入完整的信息！");
                return;
            }

            //判断填入的分数是否为0-100区间内的数字
            try
            {
                ESserviceQualityScore = Convert.ToDecimal(textBox_ESserviceQuality.Text);
                ESservicePromptnessScore = Convert.ToDecimal(textBox_ESservicePromptness.Text);

                if (ESserviceQualityScore < 0 || ESserviceQualityScore > 100 || ESservicePromptnessScore < 0 || ESservicePromptnessScore > 100)
                {
                    MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请输入次标准的分数并检查已输入的分数是否为数字!");
                return;
            }

            //将数据表External_Service中需写入的数据项依次添加到list类型的saveESPara中
            saveESPara.Clear();
            saveESPara.Add(supplierID);
            saveESPara.Add(materialID);
            saveESPara.Add(ESserviceQualityScore);
            saveESPara.Add(ESservicePromptnessScore);
            saveESPara.Add(time);
            saveESPara.Add(name);

            //！将外部服务分数数据写入数据表
            int lines = supplierPerformanceBLL.saveExternalServiceRecord(saveESPara);
            if (lines > 0)
            {
                MessageBox.Show("外部服务数据录入成功！");
                return;
            }
        }

        /// <summary>
        /// comboBox 选择供应商ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// comboBox 选择物料id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }
    }
}
