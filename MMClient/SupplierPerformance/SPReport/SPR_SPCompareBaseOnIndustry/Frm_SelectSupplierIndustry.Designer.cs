﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SelectSupplierIndustry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pl_Title = new System.Windows.Forms.Panel();
            this.pb_Close = new System.Windows.Forms.PictureBox();
            this.lb_TitleText = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_SupplierIndustry = new System.Windows.Forms.DataGridView();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.sb_SupplierIndustryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sb_SupplierIndustryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pl_Title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierIndustry)).BeginInit();
            this.SuspendLayout();
            // 
            // pl_Title
            // 
            this.pl_Title.Controls.Add(this.pb_Close);
            this.pl_Title.Controls.Add(this.lb_TitleText);
            this.pl_Title.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.pl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pl_Title.Location = new System.Drawing.Point(1, 1);
            this.pl_Title.Margin = new System.Windows.Forms.Padding(0);
            this.pl_Title.Name = "pl_Title";
            this.pl_Title.Size = new System.Drawing.Size(374, 34);
            this.pl_Title.TabIndex = 10;
            this.pl_Title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseDown);
            this.pl_Title.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseMove);
            this.pl_Title.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseUp);
            // 
            // pb_Close
            // 
            this.pb_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Close.Image = global::MMClient.Properties.Resources.close;
            this.pb_Close.Location = new System.Drawing.Point(354, 10);
            this.pb_Close.Margin = new System.Windows.Forms.Padding(2);
            this.pb_Close.Name = "pb_Close";
            this.pb_Close.Size = new System.Drawing.Size(14, 13);
            this.pb_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Close.TabIndex = 1;
            this.pb_Close.TabStop = false;
            this.pb_Close.Click += new System.EventHandler(this.pb_Close_Click);
            // 
            // lb_TitleText
            // 
            this.lb_TitleText.AutoSize = true;
            this.lb_TitleText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_TitleText.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_TitleText.Location = new System.Drawing.Point(8, 10);
            this.lb_TitleText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_TitleText.Name = "lb_TitleText";
            this.lb_TitleText.Size = new System.Drawing.Size(135, 19);
            this.lb_TitleText.TabIndex = 0;
            this.lb_TitleText.Text = "选择供应商所属行业";
            this.lb_TitleText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseDown);
            this.lb_TitleText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseMove);
            this.lb_TitleText.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseUp);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dgv_SupplierIndustry);
            this.panel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel2.Location = new System.Drawing.Point(12, 54);
            this.panel2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(352, 224);
            this.panel2.TabIndex = 11;
            // 
            // dgv_SupplierIndustry
            // 
            this.dgv_SupplierIndustry.AllowUserToAddRows = false;
            this.dgv_SupplierIndustry.AllowUserToResizeColumns = false;
            this.dgv_SupplierIndustry.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierIndustry.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierIndustry.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierIndustry.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_SupplierIndustry.ColumnHeadersHeight = 21;
            this.dgv_SupplierIndustry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierIndustry.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sb_SupplierIndustryId,
            this.sb_SupplierIndustryName});
            this.dgv_SupplierIndustry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierIndustry.EnableHeadersVisualStyles = false;
            this.dgv_SupplierIndustry.Location = new System.Drawing.Point(0, 0);
            this.dgv_SupplierIndustry.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_SupplierIndustry.MultiSelect = false;
            this.dgv_SupplierIndustry.Name = "dgv_SupplierIndustry";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierIndustry.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_SupplierIndustry.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierIndustry.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_SupplierIndustry.RowTemplate.Height = 23;
            this.dgv_SupplierIndustry.RowTemplate.ReadOnly = true;
            this.dgv_SupplierIndustry.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierIndustry.Size = new System.Drawing.Size(352, 224);
            this.dgv_SupplierIndustry.TabIndex = 1;
            this.dgv_SupplierIndustry.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierIndustry_CellDoubleClick);
            this.dgv_SupplierIndustry.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SupplierIndustry_RowPostPaint);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(292, 298);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 28);
            this.btn_Close.TabIndex = 13;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(210, 298);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 28);
            this.btn_OK.TabIndex = 12;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // sb_SupplierIndustryId
            // 
            this.sb_SupplierIndustryId.DataPropertyName = "SupplierIndustry_Id";
            this.sb_SupplierIndustryId.Frozen = true;
            this.sb_SupplierIndustryId.HeaderText = "行业编码";
            this.sb_SupplierIndustryId.Name = "sb_SupplierIndustryId";
            this.sb_SupplierIndustryId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sb_SupplierIndustryId.Width = 150;
            // 
            // sb_SupplierIndustryName
            // 
            this.sb_SupplierIndustryName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sb_SupplierIndustryName.DataPropertyName = "SupplierIndustry_Name";
            this.sb_SupplierIndustryName.HeaderText = "行业名称";
            this.sb_SupplierIndustryName.Name = "sb_SupplierIndustryName";
            this.sb_SupplierIndustryName.ReadOnly = true;
            this.sb_SupplierIndustryName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Frm_SelectSupplierIndustry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 334);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pl_Title);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frm_SelectSupplierIndustry";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Frm_SelectSupplierIndustry";
            this.Load += new System.EventHandler(this.Frm_SelectSupplierIndustry_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_SelectSupplierIndustry_Paint);
            this.pl_Title.ResumeLayout(false);
            this.pl_Title.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierIndustry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pl_Title;
        private System.Windows.Forms.PictureBox pb_Close;
        private System.Windows.Forms.Label lb_TitleText;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv_SupplierIndustry;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.DataGridViewTextBoxColumn sb_SupplierIndustryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sb_SupplierIndustryName;
    }
}