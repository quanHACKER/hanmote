﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareBOI : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 公共变量
        /// <summary>
        /// 选择供应商行业信息条件设置
        /// </summary>
        private SelectSupplierConditionSettings selectSupplierConditionSettings = null;

        #endregion

        #region 窗体构造函数

        public Frm_SPCompareBOI()
        {
            InitializeComponent();
        }

        #endregion 

        #region 窗口事件函数
        /// <summary>
        /// 按钮->确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (this.checkInputValidity()) //各项输入检验后才能后续操作
            {
                Frm_SPCompareBOIResult frm_SPCompareBOIResult = new Frm_SPCompareBOIResult(this.saveInterfaceAllValue());
                frm_SPCompareBOIResult.TopLevel = false;
                frm_SPCompareBOIResult.Dock = DockStyle.Fill;
                frm_SPCompareBOIResult.Location = new Point(0, 40);
                frm_SPCompareBOIResult.Show(SPReportGlobalVariable.userUI.dockPnlForm, DockState.Document);
            }
        }

        /// <summary>
        /// 按钮->清除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            SPCompare_CommonMethod.clearGroupBox(this.gb_ConditionSettings);

        }

        /// <summary>
        /// 按钮->关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 当年度选择变换时触发
        /// 选择年度，右边时段跟着变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedYear = this.cbb_Year.SelectedItem.ToString();
            switch (selectedYear)
            {
                case "":
                    this.cbb_TimeInterval.Items.Clear();
                    break;
                default:
                    initTimeInterval(cbb_TimeInterval);
                    break;
            }
        }

        /// <summary>
        /// 当时段选择变换时触发
        /// 选择时段,清空物料组信息信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_TimeInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.clearSupplierIndustryNameAndId();
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索采购组织信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectPurchaseGroup_Click(object sender, EventArgs e)
        {
            //获取控件在屏幕上的绝对位置
            Point btnLocation = this.btn_SelectPurchaseGroup.PointToScreen(new Point(0, 0));
            int btnWidth = this.btn_SelectPurchaseGroup.Width;
            Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

            Frm_SelectBuyerOrg frm_SelectBuyerOrg = new Frm_SelectBuyerOrg(formShowLocation);
            DialogResult result = frm_SelectBuyerOrg.ShowDialog();
            if (result == DialogResult.OK)
            {
                //为采购组织编号赋值
                this.txt_PurchaseGroupId.Text = frm_SelectBuyerOrg.purchaseId;
                //为采购组织名称赋值
                this.txt_PurchaseGroupName.Text = frm_SelectBuyerOrg.purchaseName;
            }
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索供应商所属行业
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectSupplierIndustry_Click(object sender, EventArgs e)
        {
            if (this.checkIndustryInputValidity())  //检查行业信息条件
            {
                //获取控件在屏幕上的绝对位置
                Point btnLocation = this.btn_SelectSupplierIndustry.PointToScreen(new Point(0, 0));
                int btnWidth = this.btn_SelectSupplierIndustry.Width;
                Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

                //条件设置
                if (this.selectSupplierConditionSettings == null)
                {
                    this.selectSupplierConditionSettings = new SelectSupplierConditionSettings();
                }

                this.selectSupplierConditionSettings.PurchaseId = this.txt_PurchaseGroupId.Text.Trim();
                this.selectSupplierConditionSettings.PurchaseName = this.txt_PurchaseGroupName.Text.Trim();
                this.selectSupplierConditionSettings.Year = this.cbb_Year.Text.ToString();
                this.selectSupplierConditionSettings.Month = this.cbb_TimeInterval.Text.ToString();

                Frm_SelectSupplierIndustry frm_SelectSupplierIndustry = new Frm_SelectSupplierIndustry(formShowLocation,this.selectSupplierConditionSettings);
                DialogResult result = frm_SelectSupplierIndustry.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //为供应商行业编号赋值
                    this.txt_SupplierIndustryId.Text = frm_SelectSupplierIndustry.supplierIndustryId;
                    //为供应商行业名称赋值
                    this.txt_SupplierIndustryName.Text = frm_SelectSupplierIndustry.supplierIndustryName;
                }
            }
        }

        #endregion

        #region 自定义公共函数

        /// <summary>
        /// ComboBox
        /// 初始化时段
        /// </summary>
        private void initTimeInterval(ComboBox cbb)
        {
            SPCompare_CommonMethod.initCbbYearData(cbb);
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 物料组的输入条件
        /// </summary>
        /// <returns></returns>
        private bool checkIndustryInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.cbb_Year.Text.Trim().Length <= 0 || this.cbb_TimeInterval.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估周期信息不完整，请核对！",
                                "评估周期信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 行业查询检查
        /// </summary>
        /// <returns></returns>
        private bool checkInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.cbb_Year.Text.Trim().Length <= 0 || this.cbb_TimeInterval.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估周期信息不完整，请核对！",
                                "评估周期信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.txt_SupplierIndustryId.Text.Trim().Length <= 0 || this.txt_SupplierIndustryName.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "行业信息不完整，请核对！",
                                "行业信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 清空行业的名称和编码
        /// </summary>
        private void clearSupplierIndustryNameAndId()
        {
            this.txt_SupplierIndustryId.Text = "";
            this.txt_SupplierIndustryName.Text = "";
        }

        /// <summary>
        /// 保存界面所有值
        /// </summary>
        private SPCompareBaseOnBOIConditionValue saveInterfaceAllValue()
        {

            SPCompareBaseOnBOIConditionValue sPCompareBaseOnBOIConditionValue = new SPCompareBaseOnBOIConditionValue();
            sPCompareBaseOnBOIConditionValue.SelectSupplierConditionSettings = this.selectSupplierConditionSettings;
            sPCompareBaseOnBOIConditionValue.SupplierIndustryId = this.txt_SupplierIndustryId.Text.Trim();
            sPCompareBaseOnBOIConditionValue.SupplierIndustryName = this.txt_SupplierIndustryName.Text.Trim();
            return sPCompareBaseOnBOIConditionValue;
        }

        #endregion

    }
}
