﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareRadarChart : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 公共变量

        /// <summary>
        /// 基于雷大图的供应商比较业务层
        /// </summary>
        private SPR_SPCompareRadarCharBLL sPR_SPCompareRadarCharBLL = null;

        /// <summary>
        /// 存储选择供应商信息变量
        /// </summary>
        private SelectSupplierConditionSettings selectSupplierConditionSettings = null;

        /// <summary>
        /// 雷达图查询条件
        /// </summary>
        private SPCompareRadarChartConditionValue sPCompareRadarChartConditionValue = null;

        /// <summary>
        /// 存放选中的标准,和名称
        /// </summary>
        private Dictionary<string,string> standardNameMap = null;

        /// <summary>
        /// 存放选中的供应商列表
        /// </summary>
        private Dictionary<string,string> supplierMap = null;

        #endregion

        #region 窗体构造函数

        public Frm_SPCompareRadarChart()
        {
            InitializeComponent();

            this.dgv_SupplierList.TopLeftHeaderCell.Value = "序号";
            this.dgv_SupplierList.Columns["sSelection"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        #endregion 

        #region 事件函数

        /// <summary>
        /// 按钮->确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (this.checkInputValidity())   //检查输入的合法性
            {
                Frm_SPCompareRadarChartResult frm_SPCompareRadarChartResult = new Frm_SPCompareRadarChartResult(this.saveInterfaceAllValue());
                frm_SPCompareRadarChartResult.TopLevel = false;
                frm_SPCompareRadarChartResult.Dock = DockStyle.Fill;
                frm_SPCompareRadarChartResult.Location = new Point(0, 40);
                frm_SPCompareRadarChartResult.Show(SPReportGlobalVariable.userUI.dockPnlForm, DockState.Document);
            }
        }

        /// <summary>
        /// 按钮->清除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            SPCompare_CommonMethod.clearGroupBox(this.gb_ConditionSettings);
            //清空供应商列表    清空的时段的同时，时段的选择改变则相应的DGV也被清空，此不用再次写清空DGV的方法
            //this.dgv_SupplierList.Rows.Clear();
        }

        /// <summary>
        /// 按钮->关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 当年度选择变换时触发
        /// 选择年度，右边时段跟着变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedYear = this.cbb_Year.SelectedItem.ToString();
            switch (selectedYear)
            {
                case "":
                    this.cbb_TimeInterval.Items.Clear();
                    break;
                default:
                    this.initTimeInterval(this.cbb_TimeInterval);
                    break;
            }
        }

        /// <summary>
        /// 当时段选择变换时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_TimeInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.checkEvaluationPeriodInputValidity()) //所有输入项满足要求
            {
                //获取当前选择项的值
                string selectedPeriod = this.cbb_TimeInterval.SelectedItem.ToString();
                switch (selectedPeriod)
                {
                    case "":
                        //this.dgv_SupplierList.Rows.Clear();  用Clear()方法会报"不能清除此列表的错误"
                        SPCompare_CommonMethod.clearDataGridViewData(this.dgv_SupplierList);
                        break;
                    default:
                        if (this.selectSupplierConditionSettings == null)
                        {
                            this.selectSupplierConditionSettings = new SelectSupplierConditionSettings();
                        }
                        this.selectSupplierConditionSettings.PurchaseId = this.txt_PurchaseGroupId.Text.Trim();
                        this.selectSupplierConditionSettings.PurchaseName = this.txt_PurchaseGroupName.Text.Trim();
                        this.selectSupplierConditionSettings.Year = this.cbb_Year.Text.Trim();
                        this.selectSupplierConditionSettings.Month = selectedPeriod.Trim();
                        this.initSupplierList(this.selectSupplierConditionSettings);
                        break;
                }
            }
            else
            {
                //输入项不对不让选择
                //this.cbb_TimeInterval.Text = "";
            }
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索采购组织信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectPurchaseGroup_Click(object sender, EventArgs e)
        {
            //获取控件在屏幕上的绝对位置
            Point btnLocation = this.btn_SelectPurchaseGroup.PointToScreen(new Point(0, 0));
            int btnWidth = this.btn_SelectPurchaseGroup.Width;
            Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

            Frm_SelectBuyerOrg frm_SelectBuyerOrg = new Frm_SelectBuyerOrg(formShowLocation);
            DialogResult result = frm_SelectBuyerOrg.ShowDialog();
            if (result == DialogResult.OK)
            {
                //为采购组织编号赋值
                this.txt_PurchaseGroupId.Text = frm_SelectBuyerOrg.purchaseId;
                //为采购组织名称赋值
                this.txt_PurchaseGroupName.Text = frm_SelectBuyerOrg.purchaseName;
            }
        }

        /// <summary>
        /// 定义一个矩形变量
        /// </summary>
        private Rectangle rect;

        /// <summary>
        /// 在行表头出画出序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierList_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                rect = new Rectangle((e.RowBounds.Location.X + dgv.RowHeadersWidth) / 2, e.RowBounds.Location.Y, dgv.RowHeadersWidth / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        #endregion

        #region 自定义公共函数

        /// <summary>
        /// ComboBox
        /// 初始化时段
        /// </summary>
        private void initTimeInterval(ComboBox cbb)
        {
            SPCompare_CommonMethod.initCbbYearData(cbb);
        }

        /// <summary>
        /// DataGridView
        /// SupplierList
        /// 初始化时段
        /// </summary>
        private void initSupplierList(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            if (sPR_SPCompareRadarCharBLL == null)
            {
                sPR_SPCompareRadarCharBLL = new SPR_SPCompareRadarCharBLL();
            }
            //查询数据库，得到供应商信息的DataTable
            DataTable dtSupplierInfo = sPR_SPCompareRadarCharBLL.getAllSupplierInfo(selectSupplierConditionSettings);
            this.dgv_SupplierList.DataSource = dtSupplierInfo;
        }

        /// <summary>
        /// 检查各个输入框输入的合法性
        /// </summary>
        /// <returns></returns>
        private bool checkInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.cbb_Year.Text.Trim().Length <= 0 || this.cbb_TimeInterval.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估周期信息不完整，请核对！",
                                "评估周期信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            //先清除集合
            if (this.standardNameMap != null)
            {
                this.standardNameMap.Clear();
            }
            Dictionary<string, string> tempStandardNameMap = this.saveSelectedStandardName();
            if (tempStandardNameMap == null || tempStandardNameMap.Count <= 0)
            {
                MessageBox.Show(this,
                                "您还没有选择任何评价标准，请核对！",
                                "评估标准信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            //先清空Map信息
            if (this.supplierMap != null)
            {
                this.supplierMap.Clear();
            }
            Dictionary<string, string> tempSelectedSupplierMap = this.saveSelectedSupplierMap();
            if (tempSelectedSupplierMap == null || tempSelectedSupplierMap.Count <= 0)
            {
                MessageBox.Show(this,
                                "您还没有选择任何供应商，请核对！",
                                "供应商信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 当时段选择改变时触发
        /// 带出该时段的所有供应商
        /// </summary>
        /// <returns></returns>
        private bool checkEvaluationPeriodInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.cbb_Year.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估年度信息不完整，请核对！",
                                "评估年度信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 保存界面所有值
        /// </summary>
        private SPCompareRadarChartConditionValue saveInterfaceAllValue()
        {
            if (this.sPCompareRadarChartConditionValue == null)
            {
                this.sPCompareRadarChartConditionValue = new SPCompareRadarChartConditionValue();
            }
            sPCompareRadarChartConditionValue.SelectSupplierConditionSettings = this.selectSupplierConditionSettings;
            //保存选择的标准名称
            sPCompareRadarChartConditionValue.SaveSelectedStandardName = this.standardNameMap;
            //保存选择的供应商列表信息
            sPCompareRadarChartConditionValue.SavaSelectedSupplierMap = this.supplierMap;
            return this.sPCompareRadarChartConditionValue ;
        }

        /// <summary>
        /// 保存选择的标准名称
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> saveSelectedStandardName()
        {
            //存放选中的标准
            if (this.standardNameMap == null)
            {
                this.standardNameMap = new Dictionary<string, string>();
            }
            //遍历标准选择区的所有被选中的标准
            foreach (Control ctr in this.gb_Standard.Controls)
            {
                //只遍历CheckBox
                if (ctr is CheckBox)
                {
                    CheckBox cb = ctr as CheckBox;
                    if (cb.Checked)
                    {
                        this.standardNameMap.Add(cb.Name, cb.Text);
                    }
                }
            }
            return this.standardNameMap;
        }

        /// <summary>
        /// 保存选择的供应商列表信息
        /// </summary>
        /// <returns></returns>
        private Dictionary<string,string> saveSelectedSupplierMap()
        {
            if (this.dgv_SupplierList.Rows.Count > 0)
            {
                //存放选中的供应商列表
                if (this.supplierMap == null)
                {
                    this.supplierMap = new Dictionary<string, string>();
                }
                //遍历供应商列表
                foreach (DataGridViewRow currentRow in this.dgv_SupplierList.Rows)
                {
                    if (Convert.ToBoolean(currentRow.Cells["sSelection"].Value))  //如果被选中则存入Map中
                    {
                        this.supplierMap.Add(currentRow.Cells["sId"].Value.ToString(), currentRow.Cells["sName"].Value.ToString());
                    }
                }
                return this.supplierMap;
            }
            else
            {
                return null;
            }
        }

        #endregion

    }
}
