﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompareBubbleChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_ConditionSettings = new System.Windows.Forms.GroupBox();
            this.lb_seperator = new System.Windows.Forms.Label();
            this.gb_Supplier = new System.Windows.Forms.GroupBox();
            this.dgv_SupplierList = new System.Windows.Forms.DataGridView();
            this.sSelection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sIndustry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_SelectPurchaseGroup = new System.Windows.Forms.Button();
            this.cbb_TimeInterval = new System.Windows.Forms.ComboBox();
            this.cbb_Year = new System.Windows.Forms.ComboBox();
            this.lb_TimeInterval = new System.Windows.Forms.Label();
            this.lb_Year = new System.Windows.Forms.Label();
            this.lb_EvaluationPeriod = new System.Windows.Forms.Label();
            this.txt_PurchaseGroupId = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroupName = new System.Windows.Forms.TextBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_ConditionSettings.SuspendLayout();
            this.gb_Supplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_ConditionSettings
            // 
            this.gb_ConditionSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_ConditionSettings.Controls.Add(this.lb_seperator);
            this.gb_ConditionSettings.Controls.Add(this.gb_Supplier);
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectPurchaseGroup);
            this.gb_ConditionSettings.Controls.Add(this.cbb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.cbb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.lb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_EvaluationPeriod);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupId);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupName);
            this.gb_ConditionSettings.Controls.Add(this.lb_PurchaseGroup);
            this.gb_ConditionSettings.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_ConditionSettings.Location = new System.Drawing.Point(28, 96);
            this.gb_ConditionSettings.Name = "gb_ConditionSettings";
            this.gb_ConditionSettings.Size = new System.Drawing.Size(1069, 619);
            this.gb_ConditionSettings.TabIndex = 39;
            this.gb_ConditionSettings.TabStop = false;
            this.gb_ConditionSettings.Text = "条件设置";
            // 
            // lb_seperator
            // 
            this.lb_seperator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_seperator.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lb_seperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_seperator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_seperator.Location = new System.Drawing.Point(6, 125);
            this.lb_seperator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_seperator.Name = "lb_seperator";
            this.lb_seperator.Size = new System.Drawing.Size(1057, 1);
            this.lb_seperator.TabIndex = 10;
            // 
            // gb_Supplier
            // 
            this.gb_Supplier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Supplier.Controls.Add(this.dgv_SupplierList);
            this.gb_Supplier.Location = new System.Drawing.Point(7, 138);
            this.gb_Supplier.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Name = "gb_Supplier";
            this.gb_Supplier.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Size = new System.Drawing.Size(700, 476);
            this.gb_Supplier.TabIndex = 34;
            this.gb_Supplier.TabStop = false;
            this.gb_Supplier.Text = "选择供应商";
            // 
            // dgv_SupplierList
            // 
            this.dgv_SupplierList.AllowUserToAddRows = false;
            this.dgv_SupplierList.AllowUserToDeleteRows = false;
            this.dgv_SupplierList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_SupplierList.ColumnHeadersHeight = 25;
            this.dgv_SupplierList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sSelection,
            this.sId,
            this.sName,
            this.sIndustry,
            this.sAddress});
            this.dgv_SupplierList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierList.EnableHeadersVisualStyles = false;
            this.dgv_SupplierList.Location = new System.Drawing.Point(2, 18);
            this.dgv_SupplierList.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_SupplierList.MultiSelect = false;
            this.dgv_SupplierList.Name = "dgv_SupplierList";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_SupplierList.RowHeadersWidth = 47;
            this.dgv_SupplierList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.Height = 27;
            this.dgv_SupplierList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierList.Size = new System.Drawing.Size(696, 456);
            this.dgv_SupplierList.TabIndex = 35;
            this.dgv_SupplierList.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SupplierList_RowPostPaint);
            // 
            // sSelection
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.sSelection.DefaultCellStyle = dataGridViewCellStyle2;
            this.sSelection.HeaderText = "OK";
            this.sSelection.Name = "sSelection";
            this.sSelection.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sSelection.Width = 30;
            // 
            // sId
            // 
            this.sId.DataPropertyName = "Supplier_ID";
            this.sId.HeaderText = "供应商编号";
            this.sId.Name = "sId";
            this.sId.ReadOnly = true;
            this.sId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sName
            // 
            this.sName.DataPropertyName = "Supplier_Name";
            this.sName.HeaderText = "供应商名称";
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sName.Width = 250;
            // 
            // sIndustry
            // 
            this.sIndustry.DataPropertyName = "SupplierIndustry_Id";
            this.sIndustry.HeaderText = "行业";
            this.sIndustry.Name = "sIndustry";
            this.sIndustry.ReadOnly = true;
            this.sIndustry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sAddress
            // 
            this.sAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sAddress.DataPropertyName = "Address";
            this.sAddress.HeaderText = "供应商地址";
            this.sAddress.Name = "sAddress";
            this.sAddress.ReadOnly = true;
            this.sAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btn_SelectPurchaseGroup
            // 
            this.btn_SelectPurchaseGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectPurchaseGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectPurchaseGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectPurchaseGroup.Location = new System.Drawing.Point(496, 25);
            this.btn_SelectPurchaseGroup.Name = "btn_SelectPurchaseGroup";
            this.btn_SelectPurchaseGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectPurchaseGroup.TabIndex = 4;
            this.btn_SelectPurchaseGroup.UseVisualStyleBackColor = false;
            this.btn_SelectPurchaseGroup.Click += new System.EventHandler(this.btn_SelectPurchaseGroup_Click);
            // 
            // cbb_TimeInterval
            // 
            this.cbb_TimeInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_TimeInterval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_TimeInterval.FormattingEnabled = true;
            this.cbb_TimeInterval.Location = new System.Drawing.Point(192, 86);
            this.cbb_TimeInterval.Name = "cbb_TimeInterval";
            this.cbb_TimeInterval.Size = new System.Drawing.Size(159, 25);
            this.cbb_TimeInterval.TabIndex = 9;
            this.cbb_TimeInterval.SelectedIndexChanged += new System.EventHandler(this.cbb_TimeInterval_SelectedIndexChanged);
            // 
            // cbb_Year
            // 
            this.cbb_Year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Year.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Year.FormattingEnabled = true;
            this.cbb_Year.Items.AddRange(new object[] {
            "",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024"});
            this.cbb_Year.Location = new System.Drawing.Point(103, 86);
            this.cbb_Year.Name = "cbb_Year";
            this.cbb_Year.Size = new System.Drawing.Size(83, 25);
            this.cbb_Year.TabIndex = 8;
            this.cbb_Year.SelectedIndexChanged += new System.EventHandler(this.cbb_Year_SelectedIndexChanged);
            // 
            // lb_TimeInterval
            // 
            this.lb_TimeInterval.AutoSize = true;
            this.lb_TimeInterval.Location = new System.Drawing.Point(190, 64);
            this.lb_TimeInterval.Name = "lb_TimeInterval";
            this.lb_TimeInterval.Size = new System.Drawing.Size(32, 17);
            this.lb_TimeInterval.TabIndex = 7;
            this.lb_TimeInterval.Text = "时段";
            // 
            // lb_Year
            // 
            this.lb_Year.AutoSize = true;
            this.lb_Year.Location = new System.Drawing.Point(103, 64);
            this.lb_Year.Name = "lb_Year";
            this.lb_Year.Size = new System.Drawing.Size(32, 17);
            this.lb_Year.TabIndex = 6;
            this.lb_Year.Text = "年度";
            // 
            // lb_EvaluationPeriod
            // 
            this.lb_EvaluationPeriod.AutoSize = true;
            this.lb_EvaluationPeriod.Location = new System.Drawing.Point(9, 64);
            this.lb_EvaluationPeriod.Name = "lb_EvaluationPeriod";
            this.lb_EvaluationPeriod.Size = new System.Drawing.Size(56, 17);
            this.lb_EvaluationPeriod.TabIndex = 5;
            this.lb_EvaluationPeriod.Text = "评估周期";
            // 
            // txt_PurchaseGroupId
            // 
            this.txt_PurchaseGroupId.Location = new System.Drawing.Point(353, 26);
            this.txt_PurchaseGroupId.MaxLength = 20;
            this.txt_PurchaseGroupId.Name = "txt_PurchaseGroupId";
            this.txt_PurchaseGroupId.Size = new System.Drawing.Size(144, 23);
            this.txt_PurchaseGroupId.TabIndex = 3;
            // 
            // txt_PurchaseGroupName
            // 
            this.txt_PurchaseGroupName.Location = new System.Drawing.Point(103, 26);
            this.txt_PurchaseGroupName.MaxLength = 20;
            this.txt_PurchaseGroupName.Name = "txt_PurchaseGroupName";
            this.txt_PurchaseGroupName.Size = new System.Drawing.Size(248, 23);
            this.txt_PurchaseGroupName.TabIndex = 2;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(9, 31);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(56, 17);
            this.lb_PurchaseGroup.TabIndex = 1;
            this.lb_PurchaseGroup.Text = "采购组织";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Clear.Location = new System.Drawing.Point(116, 50);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(70, 27);
            this.btn_Clear.TabIndex = 41;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(203, 50);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(70, 27);
            this.btn_Close.TabIndex = 42;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 50);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 40;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // Frm_SPCompareBubbleChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 721);
            this.Controls.Add(this.gb_ConditionSettings);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_SPCompareBubbleChart";
            this.ShowInTaskbar = false;
            this.Text = "供应商分析";
            this.gb_ConditionSettings.ResumeLayout(false);
            this.gb_ConditionSettings.PerformLayout();
            this.gb_Supplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_ConditionSettings;
        private System.Windows.Forms.Label lb_seperator;
        private System.Windows.Forms.GroupBox gb_Supplier;
        private System.Windows.Forms.DataGridView dgv_SupplierList;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sSelection;
        private System.Windows.Forms.DataGridViewTextBoxColumn sId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sIndustry;
        private System.Windows.Forms.DataGridViewTextBoxColumn sAddress;
        private System.Windows.Forms.Button btn_SelectPurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_TimeInterval;
        private System.Windows.Forms.ComboBox cbb_Year;
        private System.Windows.Forms.Label lb_TimeInterval;
        private System.Windows.Forms.Label lb_Year;
        private System.Windows.Forms.Label lb_EvaluationPeriod;
        private System.Windows.Forms.TextBox txt_PurchaseGroupId;
        private System.Windows.Forms.TextBox txt_PurchaseGroupName;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_OK;
    }
}