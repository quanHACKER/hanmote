﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompare : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        #region 公共变量
        /// <summary>
        /// 选择供应商信息条件设置
        /// </summary>
        private SelectSupplierConditionSettings selectSupplierConditionSettings = null;
        /// <summary>
        /// 选择物料信息条件设置
        /// </summary>
        private SelectMaterialConditionSettings selectMaterialConditionSettings = null;

        #endregion

        #region 窗体构造函数

        public Frm_SPCompare()
        {
            InitializeComponent();
        }

        #endregion

        #region 事件函数
        /// <summary>
        /// 按钮->确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (checkInputValidity())    //各项输入检验后才能后续操作
            {
                Frm_SPCompareResult frm_SPCompareResult = new Frm_SPCompareResult(this.saveInterfaceAllValue());
                frm_SPCompareResult.TopLevel = false;
                frm_SPCompareResult.Dock = DockStyle.Fill;
                frm_SPCompareResult.Location = new Point(0, 40);
                frm_SPCompareResult.Show(SPReportGlobalVariable.userUI.dockPnlForm, DockState.Document);
            }
           
        }

        /// <summary>
        /// 按钮->清除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            SPCompare_CommonMethod.clearGroupBox(this.gb_ConditionSettings);
        }

        /// <summary>
        /// 按钮->关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 当年度选择变换时触发
        /// 选择年度，右边时段跟着变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedYear = this.cbb_Year.SelectedItem.ToString();
            switch (selectedYear)
            {
                case "":
                    this.cbb_TimeInterval.Items.Clear();
                    break;
                default:
                    initTimeInterval(cbb_TimeInterval);
                    break;
            }
        }

        /// <summary>
        /// 采购组织编码变化时触发
        /// 内容改变时就情况供应商信息和物料信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PurchaseGroupId_TextChanged(object sender, EventArgs e)
        {
            this.clearSupplierNameAndId();
            this.clearMaterialNameAndId();
        }

        /// <summary>
        /// 当时段选择变换时触发
        /// 选择时段,清空供应商信息和物料信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_TimeInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.clearSupplierNameAndId();
            this.clearMaterialNameAndId();
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索采购组织信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectPurchaseGroup_Click(object sender, EventArgs e)
        {
            //获取控件在屏幕上的绝对位置
            Point btnLocation = this.btn_SelectPurchaseGroup.PointToScreen(new Point(0, 0));
            int btnWidth = this.btn_SelectPurchaseGroup.Width;
            Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

            Frm_SelectBuyerOrg frm_SelectBuyerOrg = new Frm_SelectBuyerOrg(formShowLocation);
            DialogResult result = frm_SelectBuyerOrg.ShowDialog();
            if (result == DialogResult.OK)
            {
                //为采购组织编号赋值
                this.txt_PurchaseGroupId.Text = frm_SelectBuyerOrg.purchaseId;
                //为采购组织名称赋值
                this.txt_PurchaseGroupName.Text = frm_SelectBuyerOrg.purchaseName;
            }
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索供应商信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectSupplier_Click(object sender, EventArgs e)
        {
            if (this.checkEvaluationPeriodInputValidity())
            {
                //获取控件在屏幕上的绝对位置
                Point btnLocation = this.btn_SelectSupplier.PointToScreen(new Point(0, 0));
                int btnWidth = this.btn_SelectSupplier.Width;
                Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

                //条件设置
                if (this.selectSupplierConditionSettings == null)
                {
                    this.selectSupplierConditionSettings = new SelectSupplierConditionSettings();
                }
                
                this.selectSupplierConditionSettings.PurchaseId = this.txt_PurchaseGroupId.Text.Trim() ;
                this.selectSupplierConditionSettings.Year = this.cbb_Year.Text.ToString();
                this.selectSupplierConditionSettings.Month = this.cbb_TimeInterval.Text.ToString();
                //生成选择供应商信息窗口
                Frm_SelectSupplier frm_SelectSupplier = new Frm_SelectSupplier(formShowLocation, selectSupplierConditionSettings);
                DialogResult result = frm_SelectSupplier.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //为供应商编号赋值
                    this.txt_SupplierId.Text = frm_SelectSupplier.supplierId;
                    //为供应商名称赋值
                    this.txt_SupplierName.Text = frm_SelectSupplier.supplierName;
                }
            }  
        }

        /// <summary>
        /// 图标
        /// 查询按钮
        /// 搜索物料信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SelectMaterial_Click(object sender, EventArgs e)
        {
            if (this.checkSupplierInfoInputValidity())
            {
                //获取控件在屏幕上的绝对位置
                Point btnLocation = this.btn_SelectMaterial.PointToScreen(new Point(0, 0));
                int btnWidth = this.btn_SelectMaterial.Width;
                Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

                //条件设置
                if (this.selectMaterialConditionSettings == null)
                {
                    this.selectMaterialConditionSettings = new SelectMaterialConditionSettings();
                }

                this.selectMaterialConditionSettings.SelectSupplierConditionSettings = this.selectSupplierConditionSettings;
                this.selectMaterialConditionSettings.SupplierId = this.txt_SupplierId.Text.Trim();
                Frm_SelectMaterial frm_SelectMaterial = new Frm_SelectMaterial(formShowLocation, this.selectMaterialConditionSettings);
                DialogResult result = frm_SelectMaterial.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //为物料编号赋值
                    this.txt_MaterialId.Text = frm_SelectMaterial.materialId;
                    //为物料名称赋值
                    this.txt_MaterialName.Text = frm_SelectMaterial.materialName;
                }
            }
        }

        #endregion

        #region 自定义公共函数 

        /// <summary>
        /// ComboBox
        /// 初始化时段
        /// </summary>
        private void initTimeInterval(ComboBox cbb)
        {
            SPCompare_CommonMethod.initCbbYearData(cbb);
        }

        /// <summary>
        /// 检查各个输入框输入的合法性
        /// </summary>
        /// <returns></returns>
        private bool checkInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.cbb_Year.Text.Trim().Length <= 0 || this.cbb_TimeInterval.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估周期信息不完整，请核对！",
                                "评估周期信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.txt_SupplierName.Text.Trim().Length <= 0 || this.txt_SupplierId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "供应商信息不完整，请核对！",
                                "供应商信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            if (this.txt_MaterialName.Text.Trim().Length <= 0 || this.txt_MaterialId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "物料信息不完整，请核对！",
                                "物料信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 供应商名称
        /// 供应商编码
        /// </summary>
        /// <returns></returns>
        private bool checkSupplierInfoInputValidity()
        {
            if (this.txt_SupplierName.Text.Trim().Length <= 0 || this.txt_SupplierId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "供应商信息不完整，请核对！",
                                "供应商信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return this.checkEvaluationPeriodInputValidity();
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 评估周期
        /// 年度
        /// 时段
        /// </summary>
        /// <returns></returns>
        private bool checkEvaluationPeriodInputValidity()
        {

            if (this.cbb_Year.Text.Trim().Length <= 0 || this.cbb_TimeInterval.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "评估周期信息不完整，请核对！",
                                "评估周期信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }

            return this.checkPurchaseInfoInputValidity();
        }

        /// <summary>
        /// 检查输入框输入的合法性
        /// 采购组织名称
        /// 采购组织编号
        /// </summary>
        /// <returns></returns>
        private bool checkPurchaseInfoInputValidity()
        {
            if (this.txt_PurchaseGroupName.Text.Trim().Length <= 0 || this.txt_PurchaseGroupId.Text.Trim().Length <= 0)
            {
                MessageBox.Show(this,
                                "采购组织信息不完整，请核对！",
                                "采购组织信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 清空供应商的名称和编码
        /// </summary>
        private void clearSupplierNameAndId()
        {
            this.txt_SupplierId.Text = "";
            this.txt_SupplierName.Text = "";
        }

        /// <summary>
        /// 清空物料的名称和编码
        /// </summary>
        private void clearMaterialNameAndId()
        {
            this.txt_MaterialId.Text = "";
            this.txt_MaterialName.Text = "";
        }

        /// <summary>
        /// 保存界面所有值
        /// </summary>
        private SPCompareConditionValue saveInterfaceAllValue()
        {

            SPCompareConditionValue sPCompareValue = new SPCompareConditionValue();
            sPCompareValue.PurchaseId = this.txt_PurchaseGroupId.Text.Trim();
            sPCompareValue.PurchaseName = this.txt_PurchaseGroupName.Text.Trim();
            sPCompareValue.Year = this.cbb_Year.Text.Trim();
            sPCompareValue.Month = this.cbb_TimeInterval.Text.Trim();
            sPCompareValue.SupplierId = this.txt_SupplierId.Text.Trim();
            sPCompareValue.SupplierName = this.txt_SupplierName.Text.Trim();
            sPCompareValue.MaterialId = this.txt_MaterialId.Text.Trim();
            sPCompareValue.MaterialName = this.txt_MaterialName.Text.Trim();
            return sPCompareValue;
        }

        #endregion

        


    }
}
