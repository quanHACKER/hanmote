﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SelectBuyerOrg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_BuyerOrg = new System.Windows.Forms.DataGridView();
            this.sb_BuyerOrg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sb_BuyerOrgName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.pl_Title = new System.Windows.Forms.Panel();
            this.pb_Close = new System.Windows.Forms.PictureBox();
            this.lb_TitleText = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_BuyerOrg)).BeginInit();
            this.panel2.SuspendLayout();
            this.pl_Title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_BuyerOrg
            // 
            this.dgv_BuyerOrg.AllowUserToAddRows = false;
            this.dgv_BuyerOrg.AllowUserToResizeColumns = false;
            this.dgv_BuyerOrg.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_BuyerOrg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_BuyerOrg.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_BuyerOrg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_BuyerOrg.ColumnHeadersHeight = 21;
            this.dgv_BuyerOrg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_BuyerOrg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sb_BuyerOrg,
            this.sb_BuyerOrgName});
            this.dgv_BuyerOrg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_BuyerOrg.EnableHeadersVisualStyles = false;
            this.dgv_BuyerOrg.Location = new System.Drawing.Point(0, 0);
            this.dgv_BuyerOrg.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_BuyerOrg.MultiSelect = false;
            this.dgv_BuyerOrg.Name = "dgv_BuyerOrg";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_BuyerOrg.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_BuyerOrg.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_BuyerOrg.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_BuyerOrg.RowTemplate.Height = 23;
            this.dgv_BuyerOrg.RowTemplate.ReadOnly = true;
            this.dgv_BuyerOrg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_BuyerOrg.Size = new System.Drawing.Size(352, 224);
            this.dgv_BuyerOrg.TabIndex = 1;
            this.dgv_BuyerOrg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_BuyerOrg_CellDoubleClick);
            this.dgv_BuyerOrg.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_BuyerOrg_RowPostPaint);
            this.dgv_BuyerOrg.Paint += new System.Windows.Forms.PaintEventHandler(this.dgv_BuyerOrg_Paint);
            // 
            // sb_BuyerOrg
            // 
            this.sb_BuyerOrg.DataPropertyName = "Buyer_Org";
            this.sb_BuyerOrg.Frozen = true;
            this.sb_BuyerOrg.HeaderText = "采购组织编码";
            this.sb_BuyerOrg.Name = "sb_BuyerOrg";
            this.sb_BuyerOrg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sb_BuyerOrg.Width = 120;
            // 
            // sb_BuyerOrgName
            // 
            this.sb_BuyerOrgName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sb_BuyerOrgName.DataPropertyName = "Buyer_Org_Name";
            this.sb_BuyerOrgName.HeaderText = "采购组织名称";
            this.sb_BuyerOrgName.Name = "sb_BuyerOrgName";
            this.sb_BuyerOrgName.ReadOnly = true;
            this.sb_BuyerOrgName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dgv_BuyerOrg);
            this.panel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel2.Location = new System.Drawing.Point(12, 48);
            this.panel2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(352, 224);
            this.panel2.TabIndex = 2;
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(210, 298);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 28);
            this.btn_OK.TabIndex = 3;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(292, 298);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 28);
            this.btn_Close.TabIndex = 4;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // pl_Title
            // 
            this.pl_Title.Controls.Add(this.pb_Close);
            this.pl_Title.Controls.Add(this.lb_TitleText);
            this.pl_Title.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.pl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pl_Title.Location = new System.Drawing.Point(1, 1);
            this.pl_Title.Margin = new System.Windows.Forms.Padding(0);
            this.pl_Title.Name = "pl_Title";
            this.pl_Title.Size = new System.Drawing.Size(374, 34);
            this.pl_Title.TabIndex = 9;
            this.pl_Title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseDown);
            this.pl_Title.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseMove);
            this.pl_Title.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pl_Title_MouseUp);
            // 
            // pb_Close
            // 
            this.pb_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Close.Image = global::MMClient.Properties.Resources.close;
            this.pb_Close.Location = new System.Drawing.Point(354, 10);
            this.pb_Close.Margin = new System.Windows.Forms.Padding(2);
            this.pb_Close.Name = "pb_Close";
            this.pb_Close.Size = new System.Drawing.Size(14, 13);
            this.pb_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Close.TabIndex = 1;
            this.pb_Close.TabStop = false;
            this.pb_Close.Click += new System.EventHandler(this.pb_Close_Click);
            // 
            // lb_TitleText
            // 
            this.lb_TitleText.AutoSize = true;
            this.lb_TitleText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_TitleText.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_TitleText.Location = new System.Drawing.Point(8, 10);
            this.lb_TitleText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_TitleText.Name = "lb_TitleText";
            this.lb_TitleText.Size = new System.Drawing.Size(93, 19);
            this.lb_TitleText.TabIndex = 0;
            this.lb_TitleText.Text = "选择采购组织";
            this.lb_TitleText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseDown);
            this.lb_TitleText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseMove);
            this.lb_TitleText.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lb_TitleText_MouseUp);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Buyer_Org";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "采购组织编码";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Buyer_Org_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "采购组织名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Frm_SelectBuyerOrg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(376, 334);
            this.Controls.Add(this.pl_Title);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_SelectBuyerOrg";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "选择采购组织";
            this.Load += new System.EventHandler(this.Frm_SelectBuyerOrg_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_SelectBuyerOrg_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_BuyerOrg)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pl_Title.ResumeLayout(false);
            this.pl_Title.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_BuyerOrg;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Panel pl_Title;
        private System.Windows.Forms.PictureBox pb_Close;
        private System.Windows.Forms.Label lb_TitleText;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn sb_BuyerOrg;
        private System.Windows.Forms.DataGridViewTextBoxColumn sb_BuyerOrgName;
    }
}