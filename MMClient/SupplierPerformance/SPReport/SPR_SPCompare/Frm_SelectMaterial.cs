﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.SupplierPerformaceBLL;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SelectMaterial : Form
    {

        #region 公共变量

        /// <summary>
        /// 供应商比较业务层
        /// </summary>
        private SPR_SPCompareBLL sPR_SPCompareBLL = null;

        /// <summary>
        /// 查询物料信息条件设置
        /// </summary>
        private SelectMaterialConditionSettings selectMaterialConditionSettings = null;

        /// <summary>
        /// 物料名称
        /// </summary>
        public string materialName;

        /// <summary>
        /// 物料编号
        /// </summary>
        public string materialId;

        #endregion

        #region 窗体构造函数
        public Frm_SelectMaterial()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 取得产生该窗体的初始显示位置
        /// </summary>
        /// <param name="formShowPosition">初始时窗体显示位置</param>
        public Frm_SelectMaterial(Point formShowPosition, SelectMaterialConditionSettings selectMaterialConditionSettings)
            : this()
        {
            this.dgv_Material.TopLeftHeaderCell.Value = "序号";
            //自定义窗体显示的初始位置
            this.Location = formShowPosition;

            //查询物料条件设置
            this.selectMaterialConditionSettings = selectMaterialConditionSettings;
        }

        #endregion

        #region 窗体事件函数
        /// <summary>
        /// 按钮->确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (this.dgv_Material.CurrentRow != null)
            {
                this.saveMaterialNameId(this.dgv_Material.CurrentRow);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(this,
                               "亲，没有可选择的物料哟！",
                               "物料信息警告",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 按钮->关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.pb_Close_Click(sender, e);
        }

        /// <summary>
        /// 点击关闭按钮时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pb_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 重新绘制窗体边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SelectMaterial_Paint(object sender, PaintEventArgs e)
        {
            //画一条分割线，Top
            Pen penTopSeperator = new Pen(Color.Gainsboro, 1);
            Point xTopSeperator = new Point(1, this.pl_Title.Height + 4);
            Point yTopSeperator = new Point(this.pl_Title.Width, this.pl_Title.Height + 3);
            e.Graphics.DrawLine(penTopSeperator, xTopSeperator, yTopSeperator);

            //画一条分割线，Bottom
            Pen penBottomSeperator = new Pen(Color.Gainsboro, 1);
            //int btnY = this.btn_OK.Location.Y;
            Point xBottomSeperator = new Point(1, this.btn_OK.Location.Y - 10); //x坐标
            Point yBottomSeperator = new Point(this.pl_Title.Width, this.btn_OK.Location.Y - 10);  //y坐标
            e.Graphics.DrawLine(penBottomSeperator, xBottomSeperator, yBottomSeperator);

            //初始化画笔，颜色：Slver，宽度：1px；
            Pen pen = new Pen(Color.Silver, 1);
            e.Graphics.DrawRectangle(pen, 0, 0, this.Width - 1, this.Height - 1);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Material_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 窗体加载时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SelectMaterial_Load(object sender, EventArgs e)
        {
            //加载物料信息
            this.bindMaterialInfo(this.selectMaterialConditionSettings);
        }

        /// <summary>
        /// DataGridView
        /// 双击单元格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Material_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (this.dgv_Material.CurrentRow != null)
                {
                    this.saveMaterialNameId(this.dgv_Material.CurrentRow);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        #endregion

        #region 窗体移动事件

        /// <summary>
        /// 鼠标按下时更改变量isMouseDown标记窗体可以随鼠标的移动而移动;
        /// 值为true表示可以移动;
        /// 值为false表示不可以移动
        /// </summary>
        private bool isMouseDown = false;
        /// <summary>
        /// 记录Form的location
        /// </summary>
        private Point formLocation;
        /// <summary>
        /// 鼠标按下的位置
        /// </summary>
        private Point mouseOffset;

        /// <summary>
        /// 临时变量
        /// 存放x坐标
        /// </summary>
        private int _x = 0;

        /// <summary>
        /// 临时变量
        /// 存放x坐标
        /// </summary>
        private int _y = 0;

        /// <summary>
        /// 临时变量
        /// 存放坐标点（x,y)
        /// </summary>
        private Point pt;

        /// <summary>
        /// 标题栏panel;
        /// 鼠标左键按下时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                formLocation = this.Location;
                mouseOffset = Control.MousePosition;
            }
        }

        /// <summary>
        /// 标题栏panel
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                pt = Control.MousePosition;
                _x = mouseOffset.X - pt.X;
                _y = mouseOffset.Y - pt.Y;
                this.Location = new Point(formLocation.X - _x, formLocation.Y - _y);
            }
        }

        /// <summary>
        /// 标题栏panel
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键按下触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            formLocation = this.Location;
            mouseOffset = Control.MousePosition;
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                pt = Control.MousePosition;
                _x = mouseOffset.X - pt.X;
                _y = mouseOffset.Y - pt.Y;
                this.Location = new Point(formLocation.X - _x, formLocation.Y - _y);
            }
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键抬起时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        #endregion

        #region 公共操作函数

        /// <summary>
        /// 绑定物料信息
        /// </summary>
        private void bindMaterialInfo(SelectMaterialConditionSettings selectCondition)
        {
            if (sPR_SPCompareBLL == null)
            {
                sPR_SPCompareBLL = new SPR_SPCompareBLL();
            }
            //查询数据库，得到物料信息的DataTable
            DataTable dtMaterialInfo = sPR_SPCompareBLL.getAllMaterialInfoBySelectMaterialConditon(selectCondition);
            this.dgv_Material.DataSource = dtMaterialInfo;
        }

        /// <summary>
        /// 保存物料名称和Id到变量中
        /// </summary>
        private void saveMaterialNameId(DataGridViewRow dgvRow)
        {
            this.materialName = dgvRow.Cells["sb_MaterialName"].Value.ToString();
            this.materialId = dgvRow.Cells["sb_MaterialId"].Value.ToString();
        }

        #endregion

        
    }
}
