﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareBOMGResult : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 公共变量
        /// <summary>
        /// 基于物料组的供应商比较业务层
        /// </summary>
        private SPR_SPCompareBOMGBLL sPR_SPCompareBOMGBLL = null;

        /// <summary>
        /// 查询评分所用条件
        /// </summary>
        private SPCompareBaseOnMGConditionValue sPCompareBaseOnMGConditionValue = null;

        #endregion

        #region 窗体构造函数
        public Frm_SPCompareBOMGResult()
        {
            InitializeComponent();
        }

        public Frm_SPCompareBOMGResult(SPCompareBaseOnMGConditionValue sPCompareBaseOnMGConditionValue)
            :this()
        {
            this.dgv_Result.TopLeftHeaderCell.Value = "序号";
            this.sPCompareBaseOnMGConditionValue = sPCompareBaseOnMGConditionValue;
        }

        #endregion

        #region 窗体事件函数
        /// <summary>
        /// 按钮->确定
        /// 此处是关闭窗口作用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 定义一个矩形变量
        /// </summary>
        private Rectangle rect;

        /// <summary>
        /// 在行表头出画出序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Result_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                rect = new Rectangle((e.RowBounds.Location.X + dgv.RowHeadersWidth) / 2, e.RowBounds.Location.Y, dgv.RowHeadersWidth / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 窗体载入实触发
        /// 加载评分结果信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SPCompareBOMGResult_Load(object sender, EventArgs e)
        {
            this.initFormInfo(this.sPCompareBaseOnMGConditionValue);
        }

        #endregion

        #region 公共操作函数

        /// <summary>
        /// 初始化窗体信息
        /// </summary>
        /// <param name="sPCompareValue">上一个界面的条件值</param>
        private void initFormInfo(SPCompareBaseOnMGConditionValue spcCondition)
        {
            this.txt_POId.Text = spcCondition.SelectSupplierConditionSettings.PurchaseId;
            this.txt_POName.Text = spcCondition.SelectSupplierConditionSettings.PurchaseName;

            //加载评分数据
            this.bindCompareResult(spcCondition);
        }

        /// <summary>
        /// 绑定采购组织信息
        /// </summary>
        private void bindCompareResult(SPCompareBaseOnMGConditionValue spcCondition)
        {
            if (sPR_SPCompareBOMGBLL == null)
            {
                sPR_SPCompareBOMGBLL = new SPR_SPCompareBOMGBLL();
            }

            //查询数据库，得到供应商信息的DataTable
            DataTable dtResult = sPR_SPCompareBOMGBLL.getEvaluationSupplierScore(spcCondition);
            this.dgv_Result.DataSource = dtResult;
        }

        #endregion 

        


    }
}
