﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SelectMaterialGroup : Form
    {
        #region 公共变量

        /// <summary>
        /// 基于物料组的供应商评估比较业务层
        /// </summary>
        private SPR_SPCompareBOMGBLL sPR_SPCompareBOMGBLL = null;

        /// <summary>
        /// 查询物料组信息条件设置
        /// </summary>
        private SelectSupplierConditionSettings selectSupplierConditionSettings = null;

        /// <summary>
        /// 物料组描述文本名称
        /// </summary>
        public string materialGroupName;

        /// <summary>
        /// 物料组编号
        /// </summary>
        public string materialGroupId;

        #endregion

        public Frm_SelectMaterialGroup()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 取得产生该窗体的初始显示位置
        /// </summary>
        /// <param name="formShowPosition">初始时窗体显示位置</param>
        public Frm_SelectMaterialGroup(Point formShowPosition, SelectSupplierConditionSettings selectSupplierConditionSettings)
            : this()
        {
            this.selectSupplierConditionSettings = selectSupplierConditionSettings;
            this.dgv_MaterialGroup.TopLeftHeaderCell.Value = "序号";
            //自定义窗体显示的初始位置
            this.Location = formShowPosition;
        }

        /// <summary>
        /// 按钮->确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (this.dgv_MaterialGroup.CurrentRow != null)
            {
                this.saveMateriaGroupNameId(this.dgv_MaterialGroup.CurrentRow);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(this,
                               "亲，没有可选择的物料组哟！",
                               "物料组信息警告",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 按钮->关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.pb_Close_Click(sender, e);
        }

        /// <summary>
        /// 点击关闭按钮时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pb_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 重新绘制窗体边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SelectMaterialGroup_Paint(object sender, PaintEventArgs e)
        {
            //画一条分割线，Top
            Pen penTopSeperator = new Pen(Color.Gainsboro, 1);
            Point xTopSeperator = new Point(1, this.pl_Title.Height + 4);
            Point yTopSeperator = new Point(this.pl_Title.Width, this.pl_Title.Height + 3);
            e.Graphics.DrawLine(penTopSeperator, xTopSeperator, yTopSeperator);

            //画一条分割线，Bottom
            Pen penBottomSeperator = new Pen(Color.Gainsboro, 1);
            //int btnY = this.btn_OK.Location.Y;
            Point xBottomSeperator = new Point(1, this.btn_OK.Location.Y - 10); //x坐标
            Point yBottomSeperator = new Point(this.pl_Title.Width, this.btn_OK.Location.Y - 10);  //y坐标
            e.Graphics.DrawLine(penBottomSeperator, xBottomSeperator, yBottomSeperator);

            //初始化画笔，颜色：Slver，宽度：1px；
            Pen pen = new Pen(Color.Silver, 1);
            e.Graphics.DrawRectangle(pen, 0, 0, this.Width - 1, this.Height - 1);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialGroup_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// DataGridView
        /// 双击单元格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialGroup_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (this.dgv_MaterialGroup.CurrentRow != null)
                {
                    this.saveMateriaGroupNameId(this.dgv_MaterialGroup.CurrentRow);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        /// <summary>
        /// 窗体加载时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SelectMaterialGroup_Load(object sender, EventArgs e)
        {
            //加载供应商信息
            this.bindMaterialGroupInfo(this.selectSupplierConditionSettings);
        }

        #region 窗体移动事件

        /// <summary>
        /// 鼠标按下时更改变量isMouseDown标记窗体可以随鼠标的移动而移动;
        /// 值为true表示可以移动;
        /// 值为false表示不可以移动
        /// </summary>
        private bool isMouseDown = false;
        /// <summary>
        /// 记录Form的location
        /// </summary>
        private Point formLocation;
        /// <summary>
        /// 鼠标按下的位置
        /// </summary>
        private Point mouseOffset;

        /// <summary>
        /// 临时变量
        /// 存放x坐标
        /// </summary>
        private int _x = 0;

        /// <summary>
        /// 临时变量
        /// 存放x坐标
        /// </summary>
        private int _y = 0;

        /// <summary>
        /// 临时变量
        /// 存放坐标点（x,y)
        /// </summary>
        private Point pt;

        /// <summary>
        /// 标题栏panel;
        /// 鼠标左键按下时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                formLocation = this.Location;
                mouseOffset = Control.MousePosition;
            }
        }

        /// <summary>
        /// 标题栏panel
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                pt = Control.MousePosition;
                _x = mouseOffset.X - pt.X;
                _y = mouseOffset.Y - pt.Y;
                this.Location = new Point(formLocation.X - _x, formLocation.Y - _y);
            }
        }

        /// <summary>
        /// 标题栏panel
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Title_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键按下触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            formLocation = this.Location;
            mouseOffset = Control.MousePosition;
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键按下并移动时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                pt = Control.MousePosition;
                _x = mouseOffset.X - pt.X;
                _y = mouseOffset.Y - pt.Y;
                this.Location = new Point(formLocation.X - _x, formLocation.Y - _y);
            }
        }

        /// <summary>
        /// 标题文本lable
        /// 鼠标左键抬起时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_TitleText_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }
        #endregion

        #region 公共操作函数

        /// <summary>
        /// 绑定供应商信息
        /// </summary>
        private void bindMaterialGroupInfo(SelectSupplierConditionSettings selectCondition)
        {
            if (sPR_SPCompareBOMGBLL == null)
            {
                sPR_SPCompareBOMGBLL = new SPR_SPCompareBOMGBLL();
            }
            //查询数据库，得到物料组信息的DataTable
            DataTable dtMaterialInfo = sPR_SPCompareBOMGBLL.getMaterialGroupInfo(selectCondition);
            this.dgv_MaterialGroup.DataSource = dtMaterialInfo;
        }

        /// <summary>
        /// 保存物料组描述文本和Id到变量中
        /// </summary>
        private void saveMateriaGroupNameId(DataGridViewRow dgvRow)
        {
            this.materialGroupName = dgvRow.Cells["sb_MaterialGroupName"].Value.ToString();
            this.materialGroupId = dgvRow.Cells["sb_MaterialGroupId"].Value.ToString();
        }

        #endregion

        

    }
}
