﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompareBOMG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_ConditionSettings = new System.Windows.Forms.GroupBox();
            this.btn_SelectMaterialGroup = new System.Windows.Forms.Button();
            this.btn_SelectPurchaseGroup = new System.Windows.Forms.Button();
            this.txt_MaterialGroupId = new System.Windows.Forms.TextBox();
            this.txt_MaterialGroupName = new System.Windows.Forms.TextBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.cbb_TimeInterval = new System.Windows.Forms.ComboBox();
            this.cbb_Year = new System.Windows.Forms.ComboBox();
            this.lb_TimeInterval = new System.Windows.Forms.Label();
            this.lb_Year = new System.Windows.Forms.Label();
            this.lb_EvaluationPeriod = new System.Windows.Forms.Label();
            this.txt_PurchaseGroupId = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroupName = new System.Windows.Forms.TextBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_ConditionSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_ConditionSettings
            // 
            this.gb_ConditionSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectMaterialGroup);
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectPurchaseGroup);
            this.gb_ConditionSettings.Controls.Add(this.txt_MaterialGroupId);
            this.gb_ConditionSettings.Controls.Add(this.txt_MaterialGroupName);
            this.gb_ConditionSettings.Controls.Add(this.lb_MaterialGroup);
            this.gb_ConditionSettings.Controls.Add(this.cbb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.cbb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.lb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_EvaluationPeriod);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupId);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupName);
            this.gb_ConditionSettings.Controls.Add(this.lb_PurchaseGroup);
            this.gb_ConditionSettings.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_ConditionSettings.Location = new System.Drawing.Point(28, 105);
            this.gb_ConditionSettings.Name = "gb_ConditionSettings";
            this.gb_ConditionSettings.Size = new System.Drawing.Size(597, 244);
            this.gb_ConditionSettings.TabIndex = 35;
            this.gb_ConditionSettings.TabStop = false;
            this.gb_ConditionSettings.Text = "条件设置";
            // 
            // btn_SelectMaterialGroup
            // 
            this.btn_SelectMaterialGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectMaterialGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectMaterialGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectMaterialGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectMaterialGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectMaterialGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectMaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectMaterialGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectMaterialGroup.Location = new System.Drawing.Point(501, 176);
            this.btn_SelectMaterialGroup.Name = "btn_SelectMaterialGroup";
            this.btn_SelectMaterialGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectMaterialGroup.TabIndex = 16;
            this.btn_SelectMaterialGroup.UseVisualStyleBackColor = false;
            this.btn_SelectMaterialGroup.Click += new System.EventHandler(this.btn_SelectMaterialGroup_Click);
            // 
            // btn_SelectPurchaseGroup
            // 
            this.btn_SelectPurchaseGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectPurchaseGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectPurchaseGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectPurchaseGroup.Location = new System.Drawing.Point(501, 35);
            this.btn_SelectPurchaseGroup.Name = "btn_SelectPurchaseGroup";
            this.btn_SelectPurchaseGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectPurchaseGroup.TabIndex = 14;
            this.btn_SelectPurchaseGroup.UseVisualStyleBackColor = false;
            this.btn_SelectPurchaseGroup.Click += new System.EventHandler(this.btn_SelectPurchaseGroup_Click);
            // 
            // txt_MaterialGroupId
            // 
            this.txt_MaterialGroupId.Location = new System.Drawing.Point(357, 178);
            this.txt_MaterialGroupId.MaxLength = 20;
            this.txt_MaterialGroupId.Name = "txt_MaterialGroupId";
            this.txt_MaterialGroupId.ReadOnly = true;
            this.txt_MaterialGroupId.Size = new System.Drawing.Size(144, 23);
            this.txt_MaterialGroupId.TabIndex = 13;
            // 
            // txt_MaterialGroupName
            // 
            this.txt_MaterialGroupName.Location = new System.Drawing.Point(103, 178);
            this.txt_MaterialGroupName.MaxLength = 20;
            this.txt_MaterialGroupName.Name = "txt_MaterialGroupName";
            this.txt_MaterialGroupName.ReadOnly = true;
            this.txt_MaterialGroupName.Size = new System.Drawing.Size(248, 23);
            this.txt_MaterialGroupName.TabIndex = 12;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(9, 183);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(44, 17);
            this.lb_MaterialGroup.TabIndex = 11;
            this.lb_MaterialGroup.Text = "物料组";
            // 
            // cbb_TimeInterval
            // 
            this.cbb_TimeInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_TimeInterval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_TimeInterval.FormattingEnabled = true;
            this.cbb_TimeInterval.Location = new System.Drawing.Point(192, 117);
            this.cbb_TimeInterval.Name = "cbb_TimeInterval";
            this.cbb_TimeInterval.Size = new System.Drawing.Size(159, 25);
            this.cbb_TimeInterval.TabIndex = 10;
            this.cbb_TimeInterval.SelectedIndexChanged += new System.EventHandler(this.cbb_TimeInterval_SelectedIndexChanged);
            // 
            // cbb_Year
            // 
            this.cbb_Year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Year.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Year.FormattingEnabled = true;
            this.cbb_Year.Items.AddRange(new object[] {
            "",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024"});
            this.cbb_Year.Location = new System.Drawing.Point(103, 117);
            this.cbb_Year.Name = "cbb_Year";
            this.cbb_Year.Size = new System.Drawing.Size(83, 25);
            this.cbb_Year.TabIndex = 9;
            this.cbb_Year.SelectedIndexChanged += new System.EventHandler(this.cbb_Year_SelectedIndexChanged);
            // 
            // lb_TimeInterval
            // 
            this.lb_TimeInterval.AutoSize = true;
            this.lb_TimeInterval.Location = new System.Drawing.Point(190, 95);
            this.lb_TimeInterval.Name = "lb_TimeInterval";
            this.lb_TimeInterval.Size = new System.Drawing.Size(32, 17);
            this.lb_TimeInterval.TabIndex = 8;
            this.lb_TimeInterval.Text = "时段";
            // 
            // lb_Year
            // 
            this.lb_Year.AutoSize = true;
            this.lb_Year.Location = new System.Drawing.Point(103, 95);
            this.lb_Year.Name = "lb_Year";
            this.lb_Year.Size = new System.Drawing.Size(32, 17);
            this.lb_Year.TabIndex = 7;
            this.lb_Year.Text = "年度";
            // 
            // lb_EvaluationPeriod
            // 
            this.lb_EvaluationPeriod.AutoSize = true;
            this.lb_EvaluationPeriod.Location = new System.Drawing.Point(9, 95);
            this.lb_EvaluationPeriod.Name = "lb_EvaluationPeriod";
            this.lb_EvaluationPeriod.Size = new System.Drawing.Size(56, 17);
            this.lb_EvaluationPeriod.TabIndex = 6;
            this.lb_EvaluationPeriod.Text = "评估周期";
            // 
            // txt_PurchaseGroupId
            // 
            this.txt_PurchaseGroupId.Location = new System.Drawing.Point(357, 37);
            this.txt_PurchaseGroupId.MaxLength = 20;
            this.txt_PurchaseGroupId.Name = "txt_PurchaseGroupId";
            this.txt_PurchaseGroupId.Size = new System.Drawing.Size(144, 23);
            this.txt_PurchaseGroupId.TabIndex = 3;
            // 
            // txt_PurchaseGroupName
            // 
            this.txt_PurchaseGroupName.Location = new System.Drawing.Point(103, 37);
            this.txt_PurchaseGroupName.MaxLength = 20;
            this.txt_PurchaseGroupName.Name = "txt_PurchaseGroupName";
            this.txt_PurchaseGroupName.Size = new System.Drawing.Size(248, 23);
            this.txt_PurchaseGroupName.TabIndex = 2;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(9, 42);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(56, 17);
            this.lb_PurchaseGroup.TabIndex = 1;
            this.lb_PurchaseGroup.Text = "采购组织";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Clear.Location = new System.Drawing.Point(116, 45);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(70, 27);
            this.btn_Clear.TabIndex = 34;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(203, 45);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(70, 27);
            this.btn_Close.TabIndex = 33;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 45);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 32;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // Frm_SPCompareBOMG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 583);
            this.Controls.Add(this.gb_ConditionSettings);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Frm_SPCompareBOMG";
            this.Text = "基于物料组的供应商评估";
            this.gb_ConditionSettings.ResumeLayout(false);
            this.gb_ConditionSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_ConditionSettings;
        private System.Windows.Forms.Button btn_SelectMaterialGroup;
        private System.Windows.Forms.Button btn_SelectPurchaseGroup;
        private System.Windows.Forms.TextBox txt_MaterialGroupId;
        private System.Windows.Forms.TextBox txt_MaterialGroupName;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.ComboBox cbb_TimeInterval;
        private System.Windows.Forms.ComboBox cbb_Year;
        private System.Windows.Forms.Label lb_TimeInterval;
        private System.Windows.Forms.Label lb_Year;
        private System.Windows.Forms.Label lb_EvaluationPeriod;
        private System.Windows.Forms.TextBox txt_PurchaseGroupId;
        private System.Windows.Forms.TextBox txt_PurchaseGroupName;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_OK;
    }
}