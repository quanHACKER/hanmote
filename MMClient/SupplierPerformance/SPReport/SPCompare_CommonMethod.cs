﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Data;

namespace MMClient.SupplierPerformance
{
    public class SPCompare_CommonMethod
    {
        /// <summary>
        /// 初始化ComboBox数据
        /// </summary>
        /// <param name="cbb">指定comboBox</param>
        public static void initCbbYearData(ComboBox cbb)
        {
            //先清空数据
            clearCbbData(cbb);
            cbb.Items.Add("");
            cbb.Items.Add("1月");
            cbb.Items.Add("2月");
            cbb.Items.Add("3月");
            cbb.Items.Add("4月");
            cbb.Items.Add("5月");
            cbb.Items.Add("6月");
            cbb.Items.Add("7月");
            cbb.Items.Add("8月");
            cbb.Items.Add("9月");
            cbb.Items.Add("10月");
            cbb.Items.Add("11月");
            cbb.Items.Add("12月");
            cbb.Items.Add("1月-2月");
            cbb.Items.Add("1月-3月");
            cbb.Items.Add("1月-4月");
            cbb.Items.Add("1月-5月");
            cbb.Items.Add("1月-6月");
            cbb.Items.Add("1月-7月");
            cbb.Items.Add("1月-8月");
            cbb.Items.Add("1月-9月");
            cbb.Items.Add("1月-10月");
            cbb.Items.Add("1月-11月");
            cbb.Items.Add("1月-12月");
        }

        /// <summary>
        /// 清空ComboBox数据
        /// </summary>
        /// <param name="cbb">指定comboBox</param>
        public static void clearCbbData(ComboBox cbb)
        {
            cbb.Items.Clear();
        }

        /// <summary>
        /// 清空groupbox信息
        /// </summary>
        public static void clearGroupBox(Control controls)
        {
            if (controls is GroupBox)
            {
                GroupBox gb = controls as GroupBox;
                foreach (Control ctr in gb.Controls)
                {
                    if ((ctr as TextBox) != null)
                    {
                        (ctr as TextBox).Text = "";
                    }
                    if ((ctr as ComboBox) != null)
                    {
                        (ctr as ComboBox).Text = "";
                    }
                    if ((ctr as RadioButton) != null)
                    {
                        (ctr as RadioButton).Checked = false;
                    }
                }
            }
        }

        /// <summary>
        /// 清空DataGridView数据源
        /// 此方式清空DGV数据不会报错
        /// 直接用this.dgv.Rows.Clear()方式，汇报"不能清除此列表的错误"
        /// </summary>
        /// <param name="dgv"></param>
        public static void clearDataGridViewData(DataGridView dgv)
        {
            if (dgv != null || dgv.Rows.Count > 0)
            {
                DataTable dtTemp = (DataTable)dgv.DataSource;
                //先清空提取出来的DataTable
                dtTemp.Rows.Clear();
                //将没有数据的DataTable再次绑定
                dgv.DataSource = dtTemp;
            }
        }

    }
}
