﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.CostReduction
{
    public partial class CostReductionResultForm : DockContent
    {
        CostReductionBLL costReductionBLL = new CostReductionBLL();
        public CostReductionResultForm()
        {
            InitializeComponent();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            //添加月份
            comboBox_month.Items.Add("1");
            comboBox_month.Items.Add("2");
            comboBox_month.Items.Add("3");
            comboBox_month.Items.Add("4");
            comboBox_month.Items.Add("5");
            comboBox_month.Items.Add("6");
            comboBox_month.Items.Add("7");
            comboBox_month.Items.Add("8");
            comboBox_month.Items.Add("9");
            comboBox_month.Items.Add("10");
            comboBox_month.Items.Add("11");
            comboBox_month.Items.Add("12");
        }

        /// <summary>
        /// button 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_check_Click(object sender, EventArgs e)
        {
            //查看是否选择年份和月份
            if (String.IsNullOrEmpty(comboBox_year.Text.ToString()) || String.IsNullOrEmpty(comboBox_month.Text.ToString()))
            {
                MessageBox.Show("请选择年份及月份！");
                return;
            }

            String year, month;
            year = comboBox_year.Text.ToString();
            month = comboBox_month.Text.ToString();

            DataTable costReductionRst = costReductionBLL.queryCostReductionResult(year,month);

            //如果返回datatable无值
            if (costReductionRst.Rows.Count == 0)
            {
                MessageBox.Show("无符合条件的记录！");
                return;
            }
            else
            {
                //添加之前先清空DataGridView
                dataGridView.Rows.Clear();
            }

            //将从数据表Cost_Reduction中取出的记录，依次显示在DataGridView中
            for (int i = 0; i < costReductionRst.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];

                row.Cells[0].Value = costReductionRst.Rows[i][0];
                row.Cells[1].Value = costReductionRst.Rows[i][1];
                row.Cells[2].Value = costReductionRst.Rows[i][2];
                row.Cells[3].Value = costReductionRst.Rows[i][3];
                row.Cells[4].Value = costReductionRst.Rows[i][4];
                row.Cells[5].Value = costReductionRst.Rows[i][5];
                row.Cells[6].Value = costReductionRst.Rows[i][6];
            }
        }
    }
}
