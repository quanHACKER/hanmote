﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SupplierClassify
{
    public partial class SupplierClassificationForm : DockContent
    {
        String supplierID;
        DataTable supplierInfoTable;
        String year;
        DateTime startTime = new DateTime();
        DateTime endTime = new DateTime();
        String classifyRst;
        List<Object> saveRstPara = new List<object>();//将数据表中的字段依次添加，用于保存结果
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        
        public SupplierClassificationForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            comboBox_timePeriod.Enabled = true;
            comboBox_timePeriod.Items.Add("1月");
            comboBox_timePeriod.Items.Add("2月");
            comboBox_timePeriod.Items.Add("3月");
            comboBox_timePeriod.Items.Add("4月");
            comboBox_timePeriod.Items.Add("5月");
            comboBox_timePeriod.Items.Add("6月");
            comboBox_timePeriod.Items.Add("7月");
            comboBox_timePeriod.Items.Add("8月");
            comboBox_timePeriod.Items.Add("9月");
            comboBox_timePeriod.Items.Add("10月");
            comboBox_timePeriod.Items.Add("11月");
            comboBox_timePeriod.Items.Add("12月");
            comboBox_timePeriod.Items.Add("1月-2月");
            comboBox_timePeriod.Items.Add("1月-3月");
            comboBox_timePeriod.Items.Add("1月-4月");
            comboBox_timePeriod.Items.Add("1月-5月");
            comboBox_timePeriod.Items.Add("1月-6月");
            comboBox_timePeriod.Items.Add("1月-7月");
            comboBox_timePeriod.Items.Add("1月-8月");
            comboBox_timePeriod.Items.Add("1月-9月");
            comboBox_timePeriod.Items.Add("1月-10月");
            comboBox_timePeriod.Items.Add("1月-11月");
            comboBox_timePeriod.Items.Add("1月-12月");
        }

        //comboBox 选择供应商ID
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            if (supplierInfoTable.Rows.Count <= 0)
            {
                lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }      
        }

        /// <summary>
        /// comboBox 选择年份
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            year = comboBox_year.Text.ToString();
        }

        /// <summary>
        /// button 执行分级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_classify_Click(object sender, EventArgs e)
        {
            if ((String.IsNullOrEmpty(supplierID)) || (String.IsNullOrEmpty(year)) || (String.IsNullOrEmpty(comboBox_timePeriod.Text.ToString())))
            {
                MessageBox.Show("请选择供应商代码、分级年份和分级时间范围！");
                return;
            }

            StringBuilder timePeriod = new StringBuilder();
            timePeriod.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);
            classifyRst = supplierPerformanceBLL.executeSupplierClassify(supplierID, timePeriod.ToString());
            
            //无分级结果
            if (classifyRst == "N")
            {
                MessageBox.Show("当前供应商无有效分级结果！");
                textBox_result.Clear();
                return;
            }
            else
            {
                textBox_result.Text = classifyRst;
            }
        }

        /// <summary>
        /// button 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(supplierID) || String.IsNullOrEmpty(year))
            {
                MessageBox.Show("请选择供应商ID及有效年份！");
                return;
            }
            else if (String.IsNullOrEmpty(textBox_result.Text)||String.IsNullOrEmpty(classifyRst)||classifyRst == "N")
            {
                MessageBox.Show("保存结果前请执行分级操作！");
                return;
            }
            else if (String.IsNullOrEmpty(textBox_name.Text))
            {
                MessageBox.Show("请输入分级维护人信息！");
                return;
            }

            StringBuilder timePeriod = new StringBuilder();
            timePeriod.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);

            saveRstPara.Clear();
            saveRstPara.Add(supplierID);
            saveRstPara.Add(classifyRst);
            saveRstPara.Add(year);
            saveRstPara.Add(Convert.ToDateTime(dateTimePicker.Text));
            saveRstPara.Add(textBox_name.Text.ToString());
            saveRstPara.Add(timePeriod.ToString());

            int lines = supplierPerformanceBLL.saveClassificationResult(saveRstPara);

            if (lines > 0)
            {
                MessageBox.Show("分级结果保存成功！");
                return;
            }
            else
            {
                MessageBox.Show("分级结果保存失败！");
                return;
            }

            //执行完保存操作之后给字符串赋空值
            classifyRst = "";
        }

        /// <summary>
        /// 给分级时间范围，startTime和endTime赋值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_timePeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            String year;
            //判断用户是否输入了年份
            if (comboBox_year.Text == null)
            {
                year = DateTime.Now.Year.ToString();
            }
            else
            {
                year = comboBox_year.Text;
            }


            switch (comboBox_timePeriod.SelectedItem.ToString())
            {
                case "1月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "2-1");
                    break;
                case "2月":
                    startTime = Convert.ToDateTime(year + "-" + "2-1");
                    endTime = Convert.ToDateTime(year + "-" + "3-1");
                    break;
                case "3月":
                    startTime = Convert.ToDateTime(year + "-" + "3-1");
                    endTime = Convert.ToDateTime(year + "-" + "4-1");
                    break;
                case "4月":
                    startTime = Convert.ToDateTime(year + "-" + "4-1");
                    endTime = Convert.ToDateTime(year + "-" + "5-1");
                    break;
                case "5月":
                    startTime = Convert.ToDateTime(year + "-" + "5-1");
                    endTime = Convert.ToDateTime(year + "-" + "6-1");
                    break;
                case "6月":
                    startTime = Convert.ToDateTime(year + "-" + "6-1");
                    endTime = Convert.ToDateTime(year + "-" + "7-1");
                    break;
                case "7月":
                    startTime = Convert.ToDateTime(year + "-" + "7-1");
                    endTime = Convert.ToDateTime(year + "-" + "8-1");
                    break;
                case "8月":
                    startTime = Convert.ToDateTime(year + "-" + "8-1");
                    endTime = Convert.ToDateTime(year + "-" + "9-1");
                    break;
                case "9月":
                    startTime = Convert.ToDateTime(year + "-" + "9-1");
                    endTime = Convert.ToDateTime(year + "-" + "10-1");
                    break;
                case "10月":
                    startTime = Convert.ToDateTime(year + "-" + "10-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "11月":
                    startTime = Convert.ToDateTime(year + "-" + "11-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-1");
                    break;
                case "12月":
                    startTime = Convert.ToDateTime(year + "-" + "12-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");
                    break;
                case "1月-2月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "3-1");
                    break;
                case "1月-3月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "4-1");
                    break;
                case "1月-4月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "5-1");
                    break;
                case "1月-5月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "6-1");
                    break;
                case "1月-6月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "7-1");
                    break;
                case "1月-7月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "8-1");
                    break;
                case "1月-8月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "9-1");
                    break;
                case "1月-9月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "100-1");
                    break;
                case "1月-10月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "1月-11月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "1月-12月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");
                    break;
                default:
                    MessageBox.Show("请选择正确的分级时间范围！");
                    break;
            }
        }
    }
}
