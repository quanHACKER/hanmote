﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance
{
    public partial class NoSupplierPerformanceList : DockContent
    {
        DateTime startTime, endTime;
        String startTimeString, endTimeString;
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        DataTable noSPList;
        public NoSupplierPerformanceList()
        {
            InitializeComponent();

            comboBox5.Items.Add("定期评估");
            comboBox5.Items.Add("累计评估");

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;
        }

        //查询按钮
        private void button1_Click(object sender, EventArgs e)
        {
            //startTime = Convert.ToDateTime(dateTimePicker_startTime.Text);
           // endTime = Convert.ToDateTime(dateTimePicker_endTime.Text);
            StringBuilder timePeriod = new StringBuilder();
            timePeriod.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);

            noSPList = supplierPerformanceBLL.queryNoSupplierPerformanceList(timePeriod.ToString());
             //添加之前先清空DataGridView
            dataGridView1.Rows.Clear();

            for (int i = 0; i < noSPList.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView1.Rows.Add();
                DataGridViewRow row = this.dataGridView1.Rows[i];

                //供应商ID 
                row.Cells[0].Value = noSPList.Rows[i][0];
                //供应商名称
                row.Cells[1].Value = noSPList.Rows[i][1];
            }
        }

        //评估类型
        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox_timePeriod.Items.Clear();
            //根据选定的评估类型，更改时间范围下拉框的选项
            if (comboBox5.Text.ToString() == "定期评估")
            {
                comboBox_timePeriod.Enabled = true;
                comboBox_timePeriod.Items.Add("1月");
                comboBox_timePeriod.Items.Add("2月");
                comboBox_timePeriod.Items.Add("3月");
                comboBox_timePeriod.Items.Add("4月");
                comboBox_timePeriod.Items.Add("5月");
                comboBox_timePeriod.Items.Add("6月");
                comboBox_timePeriod.Items.Add("7月");
                comboBox_timePeriod.Items.Add("8月");
                comboBox_timePeriod.Items.Add("9月");
                comboBox_timePeriod.Items.Add("10月");
                comboBox_timePeriod.Items.Add("11月");
                comboBox_timePeriod.Items.Add("12月");
            }
            else if (comboBox5.Text.ToString() == "累计评估")
            {
                comboBox_timePeriod.Enabled = true;
                comboBox_timePeriod.Items.Add("1月-2月");
                comboBox_timePeriod.Items.Add("1月-3月");
                comboBox_timePeriod.Items.Add("1月-4月");
                comboBox_timePeriod.Items.Add("1月-5月");
                comboBox_timePeriod.Items.Add("1月-6月");
                comboBox_timePeriod.Items.Add("1月-7月");
                comboBox_timePeriod.Items.Add("1月-8月");
                comboBox_timePeriod.Items.Add("1月-9月");
                comboBox_timePeriod.Items.Add("1月-10月");
                comboBox_timePeriod.Items.Add("1月-11月");
                comboBox_timePeriod.Items.Add("1月-12月");
            }
        }
    }
}
