﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.LPS
{
    public partial class LPSProcessForm : DockContent
    {
        DataTable supplierInfoTable;
        LPSBLL lpsBLL = new LPSBLL();
        String supplierID, supplierName;
        String lpsDegree;

        Decimal priceScore, qualityScore, deliveryScore, serviceSupportScore, externalServiceScore;
        Decimal totalScore;
        List<Decimal> scoreThreshodList = new List<Decimal>();//依此存放5个主标准的分数阈值

        public LPSProcessForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = lpsBLL.querySupplier();            
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            //lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            
            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            //给时间范围comboBox添加选项
            comboBox_timePeriod.Enabled = true;
            comboBox_timePeriod.Items.Add("1月");
            comboBox_timePeriod.Items.Add("2月");
            comboBox_timePeriod.Items.Add("3月");
            comboBox_timePeriod.Items.Add("4月");
            comboBox_timePeriod.Items.Add("5月");
            comboBox_timePeriod.Items.Add("6月");
            comboBox_timePeriod.Items.Add("7月");
            comboBox_timePeriod.Items.Add("8月");
            comboBox_timePeriod.Items.Add("9月");
            comboBox_timePeriod.Items.Add("10月");
            comboBox_timePeriod.Items.Add("11月");
            comboBox_timePeriod.Items.Add("12月");
            comboBox_timePeriod.Items.Add("1月-2月");
            comboBox_timePeriod.Items.Add("1月-3月");
            comboBox_timePeriod.Items.Add("1月-4月");
            comboBox_timePeriod.Items.Add("1月-5月");
            comboBox_timePeriod.Items.Add("1月-6月");
            comboBox_timePeriod.Items.Add("1月-7月");
            comboBox_timePeriod.Items.Add("1月-8月");
            comboBox_timePeriod.Items.Add("1月-9月");
            comboBox_timePeriod.Items.Add("1月-10月");
            comboBox_timePeriod.Items.Add("1月-11月");
            comboBox_timePeriod.Items.Add("1月-12月");

            //LPS流程决策
            comboBox_stateChange.Items.Add("退出低效能供应商管理流程");
            comboBox_stateChange.Items.Add("进入低效能供应商管理三级流程的下一阶段");
            comboBox_stateChange.Items.Add("不改变当前状态！");
        }

        //comboBox选择供应商
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //给变量supplierID和supplierName赋值，并在lable上显示供应商名称
            supplierID = comboBox_supplierID.SelectedValue.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            supplierName = lable_supplierName.Text.ToString();
        }

        /// <summary>
        /// 显示当前供应商的LPS状态
        /// </summary>
        /// <param name="supplierID"></param>
        private void showLPSDegree(String degreeString)
        {
            //显示lpsDegree
            if (degreeString == "0")
            {
                textBox_degree.Text = "0-初次警告阶段";
            }
            else if (degreeString == "1")
            {
                textBox_degree.Text = "1-警惕阶段";
            }
            else if (degreeString == "2")
            {
                textBox_degree.Text = "2-第二级警告阶段";
            }
            else if (degreeString == "3")
            {
                textBox_degree.Text = "3-第三级警告阶段";
            }
        }

        //button 查看供应商当前状态
        private void button_check_Click(object sender, EventArgs e)
        {
            //检查是否选择了供应商
            if (supplierID == null)
            {
                MessageBox.Show("请选择供应商！");
                return;
            }

            //查看该供应商是否在LPS流程中有记录
            bool recordIdentity = lpsBLL.queryRecordOrNot(supplierID);
            if (recordIdentity == false)
            {
                MessageBox.Show("该供应商不在低效能供应商流程中，或已退出低效能供应商！");
                return;
            }

            //查询当前选择的供应商在LPS流程中的状态
            lpsDegree = lpsBLL.queryLPSDegree(supplierID);
            //显示LPS状态
            this.showLPSDegree(lpsDegree);
        }

        //button 查看供应商是否达标
        private void button_checkState_Click(object sender, EventArgs e)
        {
            //检查是否输入分数阈值
            if (String.IsNullOrEmpty(textBox_price.Text) || String.IsNullOrEmpty(textBox_quality.Text) || String.IsNullOrEmpty(textBox_delivery.Text) || String.IsNullOrEmpty(textBox_serviceSupport.Text) || String.IsNullOrEmpty(textBox_externalService.Text) || String.IsNullOrEmpty(textBox_totalScore.Text))
            {
                MessageBox.Show("请输入绩效评估分数阈值，用于判断供应商绩效是否达标！");
                return;
            }

            try
            {
                priceScore = Convert.ToDecimal(textBox_price.Text);
                qualityScore = Convert.ToDecimal(textBox_quality.Text);
                deliveryScore = Convert.ToDecimal(textBox_delivery.Text);
                serviceSupportScore = Convert.ToDecimal(textBox_serviceSupport.Text);
                externalServiceScore = Convert.ToDecimal(textBox_externalService.Text);
                totalScore = Convert.ToDecimal(textBox_totalScore.Text);

                //检查输入的分数是否在0-100之间
                if (priceScore < 0 || priceScore > 100 || qualityScore < 0 || qualityScore > 100 || deliveryScore < 0 || deliveryScore > 100 || serviceSupportScore < 0 || serviceSupportScore > 100 || externalServiceScore < 0 || externalServiceScore > 100)
                {
                    MessageBox.Show("请检查输入的分数阈值是否在0-100区间内！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请检查输入的分数阈值是否为数字！");
                return;
            }

            //检查是否选择年份和时间范围
            if (String.IsNullOrEmpty(comboBox_year.Text) || String.IsNullOrEmpty(comboBox_timePeriod.Text))
            {
                MessageBox.Show("请选择年份及时间范围！");
                return;
            }

            //将分数阈值依次添加到scoreThresholdList中
            scoreThreshodList.Clear();
            scoreThreshodList.Add(priceScore);
            scoreThreshodList.Add(qualityScore);
            scoreThreshodList.Add(deliveryScore);
            scoreThreshodList.Add(serviceSupportScore);
            scoreThreshodList.Add(externalServiceScore);
            scoreThreshodList.Add(totalScore);

            //时间范围
            String timePeriod;
            StringBuilder timePeriodString = new StringBuilder();
            timePeriodString.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);
            timePeriod = timePeriodString.ToString();

            //记录查询的起止时间
            DateTime startTime, endTime;
            endTime = DateTime.Now;
            startTime = Convert.ToDateTime(dateTimePicker1.Text);
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");

            int qualified = lpsBLL.judgeQualifiedOrNot(supplierID, timePeriod, startTime, endTime, scoreThreshodList);
            if (qualified == 0)
            {
                textBox_state.Text = "否";
            }
            if (qualified == 1)
            {
                textBox_state.Text = "是";
            }
            if (qualified == 2)
            {
                textBox_state.Text = "该时间阈值内无评估记录";
            }
        }

        /// <summary>
        /// button 状态保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            //quit和next分别保存是否退出LPS和是否进入下一阶段
            int quit = 0;
            int next = 0;
            if (comboBox_stateChange.Text.ToString() == "退出低效能供应商管理流程")
            {
                quit = 1;
            }
            if (comboBox_stateChange.Text.ToString() == "进入低效能供应商管理三级流程的下一阶段")
            {
                next = 1;
            }

            //退出低效能供应商管理流程
            if (quit == 1)
            {
                int lines = lpsBLL.deleteLPSRecord(supplierID);
                if (lines > 0)
                {
                    MessageBox.Show("供应商" + supplierID + "已退出低效能供应商管理流程！");
                }
                else
                {
                    MessageBox.Show("低效能供应商管理流程退出失败！");
                    return;
                }
            }

            //更新低效能供应商管理流程，进入下一阶段
            if (next == 1)
            {
                String state = "";
                lpsDegree = lpsBLL.queryLPSDegree(supplierID);
                switch (lpsDegree)
                {
                    case "0": state = "1"; break;
                    case "1": state = "2"; break;
                    case "2": state = "3"; break;
                    case "3":
                        {
                            if (String.IsNullOrEmpty(dateTimePicker2.Text.ToString()) || String.IsNullOrEmpty(textBox_name.Text.ToString()))
                            {
                                MessageBox.Show("请输入记录时间和记录人信息！");
                                return;
                            }
                            DateTime time = Convert.ToDateTime(dateTimePicker2.Text);
                            String name = textBox_name.Text.ToString();
                            
                            //检查是否已经存在于淘汰列表
                            int count = lpsBLL.checkLPSOutList(supplierID);
                            if (count <= 0)
                            {
                                int j = lpsBLL.saveLPSOutList(supplierID, "0", time, name);
                                if (j <= 0)
                                {
                                    MessageBox.Show("进入淘汰列表失败，请重试！");
                                    return;
                                }
                            }

                            //先进入淘汰列表，再从LPS记录表中删除记录
                            int i = lpsBLL.deleteLPSRecord(supplierID);
                            
                            if (i > 0 )
                            {
                                MessageBox.Show("供应商已在低效能供应商管理记录中删除，进入待淘汰供应商列表，请对该供应商进行淘汰审批操作！");
                            }
                          //  comboBox_supplierID.Items.Remove(comboBox_supplierID.SelectedItem);
                        }
                        break;
                }

                //更新供应商在LPS流程中的状态
                int lines = lpsBLL.updateLPSState(supplierID, state);
                if (lines > 0)
                {
                    MessageBox.Show("供应商"+supplierID+"已进入低效能供应商管理流程下一阶段！");
                    return;
                }

            }

            //当前供应商状态无改变
            if (quit == 0 && next == 0)
            {
                MessageBox.Show("当前供应商状态不变！");
            }

            ////更新选择供应商的comboBox
            //supplierInfoTable = lpsBLL.querySupplier();
            //comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            //comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            //comboBox_supplierID.DataSource = supplierInfoTable;
            //lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            
        }

        private void LPSProcessForm_Activated_1(object sender, EventArgs e)
        {
            //更新选择供应商的comboBox
            supplierInfoTable = lpsBLL.querySupplier();
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        //刷新comboBox中的供应商选项
        private void button_refresh_Click(object sender, EventArgs e)
        {
            //更新选择供应商的comboBox
            supplierInfoTable = lpsBLL.querySupplier();
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            if (supplierInfoTable.Rows.Count > 0)
            {
                lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            }   
        }

    }
}


