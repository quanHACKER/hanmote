﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using Lib.Common.CommonUtils;

namespace MMClient.SupplierPerformance.LPS
{
    public partial class WarningSendForm : Form
    {
        String supplierID, supplierName;
        public WarningSendForm(String supplierID, String supplierName)
        {
            InitializeComponent();
            this.supplierID = supplierID;
            this.supplierName = supplierName;
        }

        /// <summary>
        /// “确认进入低效能供应商管理流程”Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //获取记录时间和记录人信息
            DateTime time;
            String name;
            time = Convert.ToDateTime(dateTimePicker1.Text);
            //检查是否输入警告发送人信息
            if (String.IsNullOrEmpty(textBox_name.Text))
            {
                MessageBox.Show("请输入警告发送人姓名！");
                return;
            }
            name = textBox_name.Text;

            //保存LPS记录
            LPSBLL lpsBLL = new LPSBLL();
            //保存LPS记录之前先检查是否已经存在记录
            bool rst = lpsBLL.queryRecordOrNot(supplierID);
            if (rst == true)
            {
                MessageBox.Show("该供应商已进入低效能供应商管理流程，请在低效能供应商管理三级流程中查看或修改状态！");
                return;
            }
            //因为发送警告阶段在LPS流程中的LPS_Degree记录为0，因此直接将'0'作为参数传入
            int lines = lpsBLL.saveLPSState(supplierID, "0", time, name);

            //判断是否保存成功
            if (lines == 1)
            {
                DialogResult dr = MessageBox.Show("ID为" + supplierID + "的供应商" + supplierName + "已进入低效能供应商管理流程！当前状态为0，即警告状态!请于文档管理部份下载模板填写警告并上传！");
                if (dr == DialogResult.OK)
                {
                    this.Close();
                }
                return;
            }
            else
            {
                MessageBox.Show("保存出错，请重试！");
            }
        }

        /// <summary>
        /// 下载 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownload_Click(object sender, EventArgs e)
        {
            //选择保存的文件夹
            FolderBrowserDialog downloadFileFolder = new FolderBrowserDialog();
            downloadFileFolder.Description = "请选择文件夹";
            //允许用户在浏览的时候create new folder
            downloadFileFolder.ShowNewFolderButton = false;

            DialogResult result = downloadFileFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                //格式: E://oil
                string selectedPath = downloadFileFolder.SelectedPath;
                selectedPath = selectedPath.Replace("\\", "/") + "/";

                List<string> remoteLocationList = new List<string>();
                remoteLocationList.Add("供应商绩效评估/模版/低效能供应商警告信模版.xls");
                //remoteLocationList.Add("FX/绩效评估模版/低效能供应商警告信模版.xls");
                //remoteLocationList.Add("寻源文档/201604221650032016001/M20160422155901762.xls");
                //remoteLocationList.Add("文本文档/201601212057302016001/lqc_script.sql");
                Dictionary<string, string> downloadRLDic = new Dictionary<string, string>();
                foreach (string remoteLocation in remoteLocationList)
                {
                    string[] arr = remoteLocation.Split('/');
                    string fileName = arr[arr.Length - 1];
                    string localLocation = selectedPath + fileName;
                    downloadRLDic.Add(remoteLocation, localLocation);
                }

                FTPTool ftp = FTPTool.getInstance();
                LinkedList<string> failList = ftp.download(downloadRLDic);
                if (failList.Count == 0)
                {
                    MessageBox.Show("下载成功！");
                }
            }
        }

        /// <summary>
        /// 上传警告信
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, EventArgs e)
        {

        }
    }
}
