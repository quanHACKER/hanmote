﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.LPS
{
    public partial class LPSOutCheckForm : DockContent
    {
        DataTable supplierInfoTable;
        LPSBLL lpsBLL = new LPSBLL();
        String supplierID, supplierName;


        public LPSOutCheckForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = lpsBLL.queryOutSupplier();
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            try
            {
                label_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            }
            catch
            {
                return;
            }
            
        }

        /// <summary>
        /// 选择供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //给变量supplierID和supplierName赋值，并在label上显示供应商名称m
            supplierID = comboBox_supplierID.Text.ToString();
            if (supplierInfoTable.Rows.Count > 0)
            {
                label_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }
            supplierName = label_supplierName.Text.ToString();
        }

        /// <summary>
        /// 刷新供应商button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_refresh_Click(object sender, EventArgs e)
        {
            //更新选择供应商的comboBox
            supplierInfoTable.Clear();
            supplierInfoTable = lpsBLL.queryOutSupplier();
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            if (supplierInfoTable.Rows.Count > 0)
            {
                label_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            }   
        }

        /// <summary>
        /// 淘汰Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            String time = DateTime.Now.ToString();
            String name = textBox_name.Text.ToString();

            //检查是否输入记录时间和记录人
            if (String.IsNullOrEmpty(time) || String.IsNullOrEmpty(name))
            {
                MessageBox.Show("请输入记录人及记录时间！");
                return;
            }

            //删除选中的供应商记录
            int lines1 = lpsBLL.deleteLPSupplier(supplierID);
            
            //在采购部分删除
            int lines2 = lpsBLL.updateSupplierOutInSourceList(supplierID);
            
            //在准入部分删除
            int lines3 = lpsBLL.updateSupplierOutInSupplierBase(supplierID);

            //添加淘汰记录
            //int lines4 = lpsBLL.insertEliminateSupplierRecord(supplierID,time,name);

            if (lines1 > 0 && lines2 > 0 && lines3 > 0)
            {
                MessageBox.Show("供应商已淘汰！");
            }
            else
            {
                MessageBox.Show("淘汰失败！");
                return;
            }
        }
    }
}
