﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.SupplierPerformance;
using MMClient.SupplierPerformance.PerformanceAssess;

namespace MMClient.SupplierPerformance
{
    public partial class SupplierPerformanceAddForm : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        Decimal totalScore;
        Decimal priceScore, priceLevelScore, priceHistoryScore;
        Decimal qualityScore, receiveGoodsScore, qualityAuditScore, complaintRejectScore;
        Decimal deliverScore, deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, shipmentScore;
        Decimal serviceSupportScore;
        Decimal externalServiceScore;

        List<double> mainStandardWeight = new List<double>();//存放主标准权重

        List<double> priceSSWeight = new List<double>();//存放价格次标准权重
        List<double> qualitySSWeight = new List<double>();//存放质量次标准权重
        List<double> deliverySSWeight = new List<double>();//存放收获次标准权重

        String materialID, supplierID, purchasingORGID;
        DataTable supplierInfoTable,materialInfoTable,purchasingORGInfoTable;
        String assessName;
        String assessDate = DateTime.Now.ToString();

        List<Object> saveRstPara = new List<object>();//将保存结果时各字段的值作为参数传入

        public SupplierPerformanceAddForm()
        {
            InitializeComponent();

            saveRstPara.Clear();//清空结果集保存的List

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();

            comboBox5.Items.Add("定期评估");
            comboBox5.Items.Add("累计评估");

            comboBox_weightMS.Items.Add("01");
            comboBox_weightMS.Items.Add("02");
            comboBox_weightMS.Items.Add("03");

            comboBox_weightSS.Items.Add("01");
            comboBox_weightSS.Items.Add("02");

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;
        }

        //“计算主标准及总分分数” button
        private void button1_Click(object sender, EventArgs e)
        {
            //判断用户输入的评估信息是否完整
            if (supplierID == null || materialID == null || purchasingORGID == null || mainStandardWeight == null || priceSSWeight == null || qualitySSWeight == null || deliverySSWeight == null || assessName == null || assessDate == null||comboBox_timePeriod.Text == null)
            {
                MessageBox.Show("请输入完整的评估信息！");
                return;
            }
            else
            {
                try
                {
                    priceLevelScore = Convert.ToDecimal(TextboxPriceLevel.Text);
                    priceHistoryScore = Convert.ToDecimal(TextboxPriceHistory.Text);

                    receiveGoodsScore = Convert.ToDecimal(TextboxReceiveGoods.Text);
                    qualityAuditScore = Convert.ToDecimal(TextboxQualityAudit.Text);
                    complaintRejectScore = Convert.ToDecimal(TextboxComplaintReject.Text);

                    deliverOnTimeScore = Convert.ToDecimal(TextboxDeliverOnTime.Text);
                    confirmDateScore = Convert.ToDecimal(TextboxConfirmDate.Text);
                    quantityReliabilityScore = Convert.ToDecimal(TextboxQuantityReliability.Text);
                    shipmentScore = Convert.ToDecimal(TextboxGoodsShipment.Text);

                    serviceSupportScore = Convert.ToDecimal(TextboxServiceSupport.Text);
                    externalServiceScore = Convert.ToDecimal(TextboxExternalService.Text);

                    //判断分数是否在0-100之间
                    if (priceLevelScore < 0 || priceLevelScore > 100 || priceHistoryScore < 0 || priceHistoryScore > 100)
                    {
                        MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                        return;
                    }
                    if (qualityAuditScore < 0 || qualityAuditScore > 100 || receiveGoodsScore < 0 || receiveGoodsScore > 100 || complaintRejectScore < 0 || complaintRejectScore > 100)
                    {
                        MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                        return;
                    }
                    if (deliverOnTimeScore < 0 || deliverOnTimeScore > 100 || confirmDateScore < 0 || confirmDateScore > 100 || quantityReliabilityScore < 0 || quantityReliabilityScore > 100 || shipmentScore < 0 || shipmentScore > 100)
                    {
                        MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                        return;
                    }
                    if (serviceSupportScore < 0 || serviceSupportScore > 100 || externalServiceScore < 0 || externalServiceScore > 100)
                    {
                        MessageBox.Show("请检查输入的分数是否在0-100区间内！");
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("请输入次标准的分数并检查已输入的分数是否为数字!");
                    return;
                }

                //调用计算 价格 分数的方法，并显示在界面上
                priceScore = supplierPerformanceBLL.calculatePriceScore(priceLevelScore, priceHistoryScore, priceSSWeight);
                TextboxPrice.Text = Math.Round(priceScore, 1).ToString();

                //调用计算 质量 分数的方法，并显示在界面上
                qualityScore = supplierPerformanceBLL.calculateQualityScore(receiveGoodsScore, qualityAuditScore, complaintRejectScore, qualitySSWeight);
                TextboxQuality.Text = Math.Round(qualityScore, 1).ToString();

                //调用计算 交货 分数的方法，并显示在界面上
                deliverScore = supplierPerformanceBLL.calculateDeliverScore(deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, shipmentScore, deliverySSWeight);
                TextboxDeliver.Text = Math.Round(deliverScore, 1).ToString();

                //调用计算 总分 分数的方法，并显示在界面上
                totalScore = supplierPerformanceBLL.calculateTotalScore(priceScore, qualityScore, deliverScore, serviceSupportScore, externalServiceScore, mainStandardWeight);
                TextBoxTotalScore.Text = Math.Round(totalScore, 1).ToString();
            }


        }

        //“保存” button
        private void button2_Click(object sender, EventArgs e)
        {
            //判断用户输入的评估信息是否完整
            //待修改
            if (supplierID == null || materialID == null)
            {
                MessageBox.Show("请选择供应商和物料ID！");
                return;
            }
            else if (purchasingORGID == null)
            {
                MessageBox.Show("请选择采购组织信息，若无采购组织信息请先于物料主数据中维护！");
                return;
            }
            else if (comboBox_timePeriod.Text == null || mainStandardWeight == null || priceSSWeight == null || qualitySSWeight == null || deliverySSWeight == null || assessName == null || assessDate == null)
            {
                MessageBox.Show("请输入完整的评估信息！");
                return;
            }
            //判断是否输入次标准的分数
            if (String.IsNullOrEmpty(TextboxPriceLevel.Text) || String.IsNullOrEmpty(TextboxPriceHistory.Text) || String.IsNullOrEmpty(TextboxReceiveGoods.Text) || String.IsNullOrEmpty(TextboxQualityAudit.Text) || String.IsNullOrEmpty(TextboxComplaintReject.Text)||String.IsNullOrEmpty(TextboxDeliverOnTime.Text) || String.IsNullOrEmpty(TextboxConfirmDate.Text) || String.IsNullOrEmpty(TextboxQuantityReliability.Text) || String.IsNullOrEmpty(TextboxGoodsShipment.Text) || String.IsNullOrEmpty(TextboxServiceSupport.Text) || String.IsNullOrEmpty(TextboxExternalService.Text))
            {
                MessageBox.Show("请检查是否有未评分的次标准！");
                return;
            }

            //判断是否计算主标准及总分的分数
            if (String.IsNullOrEmpty(TextboxPrice.Text) || String.IsNullOrEmpty(TextboxQuality.Text)|| String.IsNullOrEmpty(TextboxDeliver.Text))
            {
                MessageBox.Show("保存结果之前请先计算主标准及总分分数！");
                return;
            }
            
            saveRstPara.Clear();
            saveRstPara.Add(supplierID);
            saveRstPara.Add(purchasingORGID);
            saveRstPara.Add(materialID);
            saveRstPara.Add(totalScore);
            saveRstPara.Add(priceScore);
            saveRstPara.Add(priceLevelScore);
            saveRstPara.Add(priceHistoryScore);
            saveRstPara.Add(qualityScore);
            saveRstPara.Add(receiveGoodsScore);
            saveRstPara.Add(qualityAuditScore);
            saveRstPara.Add(complaintRejectScore);
            saveRstPara.Add(deliverScore);
            saveRstPara.Add(deliverOnTimeScore);
            saveRstPara.Add(confirmDateScore);
            saveRstPara.Add(quantityReliabilityScore);
            saveRstPara.Add(shipmentScore);
            saveRstPara.Add(serviceSupportScore);
            saveRstPara.Add(externalServiceScore);
           
            //评估类型
            if (comboBox5.Text == "累计评估")
            {
                saveRstPara.Add(1);
            }
            else
            {
                saveRstPara.Add(0);
            }

            
            StringBuilder timePeriod = new StringBuilder();
            timePeriod.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);
            saveRstPara.Add(timePeriod.ToString());//时间范围

            saveRstPara.Add(assessName);
            saveRstPara.Add(assessDate);

            int lines;
            lines = supplierPerformanceBLL.saveResult(saveRstPara);
            try
            {
                MessageBox.Show("评估结果保存成功！");
                
            }
            catch
            {
                MessageBox.Show("保存失败请重试！");
            }

        }

        //时间范围，评估时间
        private void comboBox5_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            comboBox_timePeriod.Items.Clear();
            //根据选定的评估类型，更改时间范围下拉框的选项
            if (comboBox5.SelectedItem.ToString() == "定期评估")
            {
                comboBox_timePeriod.Enabled = true;
                comboBox_timePeriod.Items.Add("1月");
                comboBox_timePeriod.Items.Add("2月");
                comboBox_timePeriod.Items.Add("3月");
                comboBox_timePeriod.Items.Add("4月");
                comboBox_timePeriod.Items.Add("5月");
                comboBox_timePeriod.Items.Add("6月");
                comboBox_timePeriod.Items.Add("7月");
                comboBox_timePeriod.Items.Add("8月");
                comboBox_timePeriod.Items.Add("9月");
                comboBox_timePeriod.Items.Add("10月");
                comboBox_timePeriod.Items.Add("11月");
                comboBox_timePeriod.Items.Add("12月");
            }
            else if (comboBox5.SelectedItem.ToString() == "累计评估")
            {
                comboBox_timePeriod.Enabled = true;
                comboBox_timePeriod.Items.Add("1月-2月");
                comboBox_timePeriod.Items.Add("1月-3月");
                comboBox_timePeriod.Items.Add("1月-4月");
                comboBox_timePeriod.Items.Add("1月-5月");
                comboBox_timePeriod.Items.Add("1月-6月");
                comboBox_timePeriod.Items.Add("1月-7月");
                comboBox_timePeriod.Items.Add("1月-8月");
                comboBox_timePeriod.Items.Add("1月-9月");
                comboBox_timePeriod.Items.Add("1月-10月");
                comboBox_timePeriod.Items.Add("1月-11月");
                comboBox_timePeriod.Items.Add("1月-12月");
            }
        }

        //主标准权重输入
        private void comboBox_weightMS_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (comboBox_weightMS.SelectedItem.ToString() == "03")
            {
                MainStandardWeightForm msWeightForm = new MainStandardWeightForm(mainStandardWeight);
                msWeightForm.Owner = this;
                msWeightForm.ShowDialog();
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "01")
            {
                int i = 0;
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                //将每个主标准权重设置为1
                while (i < 5)
                {
                    mainStandardWeight.Add(1);
                    i++;
                }
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "02")
            {
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                mainStandardWeight.Add(5);
                mainStandardWeight.Add(3);
                mainStandardWeight.Add(2);
                mainStandardWeight.Add(1);
                mainStandardWeight.Add(2);
            }
            else
            {
                MessageBox.Show("请选择主标准权重代码！");
            }
        }

        //主标准权重码说明 button
        private void button3_Click(object sender, EventArgs e)
        {
            MSWeightExplainForm msWeightExplainForm = new MSWeightExplainForm();
            msWeightExplainForm.Owner = this;
            msWeightExplainForm.ShowDialog();
        }

        //次标准权重输入
        private void comboBox_weightSS_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (comboBox_weightSS.SelectedItem.ToString() == "01")
            {
                //如果已经有值，清空
                if (priceSSWeight.Count != 0)
                {
                    priceSSWeight.Clear();
                }
                if (qualitySSWeight.Count != 0)
                {
                    qualitySSWeight.Clear();
                }
                if (deliverySSWeight.Count != 0)
                {
                    deliverySSWeight.Clear();
                }

                int i = 0;
                //将每个价格次标准权重设置为1，共2个
                while (i < 2)
                {
                    priceSSWeight.Add(1);
                    i++;
                }

                //将每个质量次标准权重设置为1，共3个
                int j = 0;
                while (j < 3)
                {
                    qualitySSWeight.Add(1);
                    j++;
                }

                //将每个交货次标准权重设置为1，共4个
                int k = 0;
                while (k < 4)
                {
                    deliverySSWeight.Add(1);
                    k++;
                }

            }
            else if (comboBox_weightSS.SelectedItem.ToString() == "02")
            {
                SecondaryStandardWeightForm ssWeightForm = new SecondaryStandardWeightForm(priceSSWeight, qualitySSWeight, deliverySSWeight);
                ssWeightForm.Owner = this;
                ssWeightForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("请选择次标准权重代码！");
            }
        }

        //次标准权重码说明 button
        private void button4_Click(object sender, EventArgs e)
        {
            SSWeightExplainForm ssWeightExplainForm = new SSWeightExplainForm();
            ssWeightExplainForm.Owner = this;
            ssWeightExplainForm.ShowDialog();
        }

        //评估人
        private void textBox15_TextChanged_1(object sender, EventArgs e)
        {
            assessName = textBox15.Text.ToString();
        }

        //评估日期
        private void dateTimePicker1_ValueChanged_1(object sender, EventArgs e)
        {
            assessDate = dateTimePicker1.Value.ToString();
        }
        
        //供应商id
        private void comboBox_supplierID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的采购组织信息
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG(supplierID);
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //物料id
        private void comboBox_materialID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //采购组织id
        private void comboBox_purchasingORGID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }
        }

        /// <summary>
        /// 判断是否为低效能供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click_1(object sender, EventArgs e)
        {
            List<Decimal> score = new List<Decimal>();
            //score1到score6分别为：价格分数、质量分数、交货分数、一般服务支持分数、外部服务分数、总分
            Decimal score1,score2,score3,score4,score5,score6;
            score.Clear();
            try
            {
                //依次将主标准分数加入List中
                 score1 = Convert.ToDecimal(TextboxPrice.Text.ToString());
                 score2 = Convert.ToDecimal(TextboxQuality.Text.ToString());
                 score3 = Convert.ToDecimal(TextboxDeliver.Text.ToString());
                 score4 = Convert.ToDecimal(TextboxServiceSupport.Text.ToString());
                 score5 = Convert.ToDecimal(TextboxExternalService.Text.ToString());
                 score6 = Convert.ToDecimal(TextBoxTotalScore.Text.ToString());
            }
            catch
            {
                MessageBox.Show("主标准无有效分数！");
                return;
            }
            score.Add(score1);
            score.Add(score2);
            score.Add(score3);
            score.Add(score4);
            score.Add(score5);
            score.Add(score6);
            JudgeLowPerformanceForm judgeLowPerformanceForm = new JudgeLowPerformanceForm(supplierID,score);
            judgeLowPerformanceForm.Owner = this;
            judgeLowPerformanceForm.ShowDialog();
        }
    }
}