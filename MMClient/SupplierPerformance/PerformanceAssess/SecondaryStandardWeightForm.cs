﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance
{
    public partial class SecondaryStandardWeightForm : Form
    {
        List<double> priceSSWeight = new List<double>();
        List<double> qualitySSWeight = new List<double>();
        List<double> deliverySSWeight = new List<double>();

        public SecondaryStandardWeightForm(List<double> priceSSWeight,List<double> qualitySSWeight,List<double> deliverySSWeight)
        {
            InitializeComponent();
            this.priceSSWeight = priceSSWeight;
            this.qualitySSWeight = qualitySSWeight;
            this.deliverySSWeight = deliverySSWeight;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //在add之前判断list中是否有元素，如果有元素，则全部移除之后再加入新元素（保证LIST中始终只有5个权重值）
                if (priceSSWeight.Count != 0)
                {
                    priceSSWeight.Clear();
                }
                if (qualitySSWeight.Count != 0)
                {
                    qualitySSWeight.Clear();
                }
                if (deliverySSWeight.Count != 0)
                {
                    deliverySSWeight.Clear();
                }

                priceSSWeight.Add(Convert.ToDouble(textBox1.Text));
                priceSSWeight.Add(Convert.ToDouble(textBox2.Text));

                qualitySSWeight.Add(Convert.ToDouble(textBox3.Text));
                qualitySSWeight.Add(Convert.ToDouble(textBox4.Text));
                qualitySSWeight.Add(Convert.ToDouble(textBox5.Text));

                deliverySSWeight.Add(Convert.ToDouble(textBox6.Text));
                deliverySSWeight.Add(Convert.ToDouble(textBox7.Text));
                deliverySSWeight.Add(Convert.ToDouble(textBox8.Text));
                deliverySSWeight.Add(Convert.ToDouble(textBox9.Text));
            
            }
            catch
            {
                MessageBox.Show("请输入正确的权重值！");
                return;
            }

            this.Close();
        }

        private void SecondaryStandardWeightForm_Load(object sender, EventArgs e)
        {

        }
    }
}
