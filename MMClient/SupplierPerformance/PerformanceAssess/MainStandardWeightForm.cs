﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance
{
    public partial class MainStandardWeightForm : Form
    {
        List<double> mainStandardWeight;

        public MainStandardWeightForm(List<double> mainStandardWeight)
        {
            InitializeComponent();
            this.mainStandardWeight = mainStandardWeight;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //在add之前判断list中是否有元素，如果有元素，则全部移除之后再加入新元素（保证LIST中始终只有5个权重值）
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                mainStandardWeight.Add(Convert.ToDouble(textBox1.Text));
                mainStandardWeight.Add(Convert.ToDouble(textBox2.Text));
                mainStandardWeight.Add(Convert.ToDouble(textBox3.Text));
                mainStandardWeight.Add(Convert.ToDouble(textBox4.Text));
                mainStandardWeight.Add(Convert.ToDouble(textBox5.Text));
            }
            catch
            {
                MessageBox.Show("请输入正确的权重值！");
                return;
            }

            this.Close();
        }

    }
}
