﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.PerformanceAssess
{
    public partial class JudgeLowPerformanceForm : Form
    {
        List<Decimal> actualScore = new List<Decimal>();
        LPSBLL lpsBLL = new LPSBLL();
        String supplierID;

        public JudgeLowPerformanceForm(String supplierID,List<Decimal> score)
        {
            InitializeComponent();
            this.supplierID = supplierID;
            textBox_supplierID.Text = supplierID;//绩效评估分数指标中显示供应商ID
            textBox_supplierID2.Text = supplierID;//关键指数表现选项卡中显示供应商ID
            this.actualScore = score;
        }

        #region 绩效评估分数指标不达标
        /// <summary>
        /// button 查看是否达标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_check_Click(object sender, EventArgs e)
        {
            Decimal priceScore;
            Decimal qualityScore;
            Decimal deliveryScore ;
            Decimal serviceSupportScore;
            Decimal externalServiceScore;
            try
            {
                 priceScore = Convert.ToDecimal(textBox_price.Text.ToString());
                 qualityScore = Convert.ToDecimal(textBox_quality.Text.ToString());
                 deliveryScore = Convert.ToDecimal(textBox_delivery.Text.ToString());
                 serviceSupportScore = Convert.ToDecimal(textBox_serviceSupport.Text.ToString());
                 externalServiceScore = Convert.ToDecimal(textBox_externalService.Text.ToString());
            }
            catch
            {
                MessageBox.Show("请输入主标准分数阈值！");
                return;
            }

            //判断输入的主标准分数阈值是否在0-100区间内
            if (priceScore < 0 || priceScore > 100)
            {
                MessageBox.Show("价格标准分数阈值需在0-100分区间内！");
                return;
            }
            else if(qualityScore < 0 || qualityScore > 100)
            {
                MessageBox.Show("质量标准分数阈值需在0-100分区间内！");
                return;
            }
            else if (deliveryScore < 0 || deliveryScore > 100)
            {
                MessageBox.Show("交货标准分数阈值需在0-100分区间内！");
                return; 
            }
            else if (serviceSupportScore < 0 || serviceSupportScore > 100)
            {
                MessageBox.Show("一般服务/支持标准分数阈值需在0-100分区间内！");
                return;
            }
            else if (externalServiceScore < 0 || externalServiceScore > 100)
            {
                MessageBox.Show("外部服务标准分数阈值需在0-100分区间内！");
                return;
            }

            //判断供应商是否达标
            if (actualScore[0] < priceScore || actualScore[1] < qualityScore || actualScore[2] < deliveryScore || actualScore[3] < serviceSupportScore || actualScore[4] < externalServiceScore)
            {
                textBox_qualifiedOrNot.Text = "否";
            }
            else if (actualScore[5] < 60)//总分小于60分，即供应商评级在C和D的供应商
            {
                textBox_qualifiedOrNot.Text = "否";
            }
            else
            {
                textBox_qualifiedOrNot.Text = "是";
            }
            
        }

        /// <summary>
        /// button 发送警告
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_send_Click(object sender, EventArgs e)
        {
            //保存LPS记录之前先检查是否已经存在记录
            bool rst = lpsBLL.queryRecordOrNot(supplierID);
            if (rst == true)
            {
                DialogResult dr = MessageBox.Show("该供应商已进入低效能供应商管理流程，请在低效能供应商管理三级流程中查看或修改状态！");
                if (dr == DialogResult.OK)
                {
                    this.Close();
                }
                return;
            }

            if ( String.IsNullOrEmpty(textBox_name1.Text.ToString()) || String.IsNullOrEmpty(dateTimePicker1.Value.ToString()))
            {
                MessageBox.Show("请输入发送警告时间及警告人！");
                return;
            }
            

            DateTime date = Convert.ToDateTime(dateTimePicker1.Value);
            int lines = lpsBLL.saveLPSState(supplierID, "0", date, textBox_name1.ToString());
            if (lines > 0)
            {
                DialogResult dr = MessageBox.Show("供应商"+supplierID+"已进入LPS三级流程，请于文档管理部份下载模板填写警告并上传！");
                if (dr == DialogResult.OK )
                {
                    this.Close();
                }
                return;
            }
        }
        #endregion

       
        #region 关键指数表现不达标
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    //保存LPS记录之前先检查是否已经存在记录
        //    bool rst = lpsBLL.queryRecordOrNot(supplierID);
        //    if (rst == true)
        //    {
        //        DialogResult dr = MessageBox.Show("该供应商已进入低效能供应商管理流程，请在低效能供应商管理三级流程中查看或修改状态！");
        //        if (dr == DialogResult.OK)
        //        {
        //            this.Close();
        //        }
        //        return;
        //    }

        //    if (String.IsNullOrEmpty(textBox_name2.Text.ToString()) || String.IsNullOrEmpty(dateTimePicker2.Value.ToString()))
        //    {
        //        MessageBox.Show("请输入发送警告时间及警告人！");
        //        return;
        //    }

        /// <summary>
        /// button 确定进入LPS流程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //获取记录时间和记录人信息
            DateTime time;
            String name;
            time = Convert.ToDateTime(dateTimePicker1.Text);


            //检查是否输入警告发送人信息
            if (String.IsNullOrEmpty(textBox_name2.Text))
            {
                MessageBox.Show("请输入警告发送人姓名！");
                return;
            }
            name = textBox_name2.Text;

            //保存LPS记录和关键指标陈述记录
            LPSBLL lpsBLL = new LPSBLL();
            //保存LPS记录之前先检查是否已经存在记录
            bool rst = lpsBLL.queryRecordOrNot(supplierID);
            if (rst == true)
            {
                MessageBox.Show("该供应商已进入低效能供应商管理流程，请在低效能供应商管理三级流程中查看或修改状态！");
                return;
            }
            //检查是否选择了一个或多个关键指标
            if (checkBox1.Checked == false && checkBox2.Checked == false && checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false && checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == false && checkBox12.Checked == false && checkBox13.Checked == false && checkBox14.Checked == false)
            {
                MessageBox.Show("请选择未达标的关键指标！");
                return;
            }

            ////如果选中了任何一个关键指标，则将陈述框设置为用户可输入
            //foreach (Control c in groupBox_kpi.Controls)
            //{
            //    if (c is CheckBox && ((CheckBox)c).Checked == true)
            //    {
            //        textBox_statement.ReadOnly = false;
            //    }
            //}

            //将用户选择的checkbox的选项添加到一个List中，作为保存到数据库时的参数
            List<String> kpiList = new List<string>();
            foreach (Control c in groupBox_kpi.Controls)
            {
                if (c is CheckBox && ((CheckBox)c).Checked == true)
                {
                    kpiList.Add(((CheckBox)c).Text.ToString());
                }
            }

            //保存陈述内容（先保存陈述内容，再更新低效能供应商数据表，以防陈述内容保存失败，但是却更新了低效能供应商数据表的情况）
            String statement = textBox_statement.Text.ToString();
            int linesStatement = lpsBLL.saveKPIStatement(supplierID, kpiList, statement, time, name);
            if (linesStatement != 1)
            {
                MessageBox.Show("关键指标及陈述保存失败！");
                return;
            }

            //因为发送警告阶段在LPS流程中的LPS_Degree记录为0，因此直接将'0'作为参数传入
            int lines = lpsBLL.saveLPSState(supplierID, "0", time, name);

            //判断是否保存成功
            if (lines == 1)
            {
                MessageBox.Show("ID为" + supplierID + "的供应商" + name + "已进入低效能供应商管理流程！当前状态为0，即警告状态!");
                return;
            }
            else
            {
                MessageBox.Show("保存出错，请重试！");
            }
        }


        //    DateTime date = Convert.ToDateTime(dateTimePicker2.Value);
        //    int lines = lpsBLL.saveLPSState(supplierID, "0", date, textBox_name1.ToString());
        //    if (lines > 0)
        //    {
        //        DialogResult dr = MessageBox.Show("供应商" + supplierID + "已进入LPS三级流程，请于文档管理部份下载模板填写警告并上传！");
        //        if (dr == DialogResult.OK)
        //        {
        //            this.Close();
        //        }
        //        return;
        //    }
        //}

        # endregion
    }
}
