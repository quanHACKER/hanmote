﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance
{
    public partial class UserDefineForm : Form
    {
        //还要输入一个时间偏差标准值

        //从与用户交互的界面获得这些值，传递给评估界面，从而供调用计算函数使用
        List<double> priceLevelPara = new List<double>();
        List<double> priceHistoryPara = new List<double>();
        List<double> PPMPara = new List<double>();
        List<double> confirmDatePara;

        public UserDefineForm(List<double> priceLevelPara, List<double> priceHistoryPara, List<double> PPMPara, List<double> confirmDatePara)
        {
            InitializeComponent();
            this.priceLevelPara = priceLevelPara;
            this.priceHistoryPara = priceHistoryPara;
            this.PPMPara = PPMPara;
            this.confirmDatePara = confirmDatePara;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double priceLevelLow, priceLevelHigh, priceHistoryLow, priceHistoryHigh, PPMLow, PPMHigh,confirmDateParatmp;

            //如果用户输入内容不是数字，则抛出异常
            try
            {
                priceLevelLow = Convert.ToDouble(textBox1.Text) / 100; //100分下价格水平差额百分比
                priceLevelHigh = Convert.ToDouble(textBox2.Text) / 100; //0分下价格水平差额百分比
                priceHistoryLow = Convert.ToDouble(textBox3.Text) / 100; //100分下价格历史差额百分比
                priceHistoryHigh = Convert.ToDouble(textBox4.Text) / 100; //0分下价格历史差额百分比
                PPMLow = Convert.ToDouble(textBox5.Text); //100分下PPM值
                PPMHigh = Convert.ToDouble(textBox6.Text); //0分下PPM值

                confirmDateParatmp = Convert.ToDouble(textBox7.Text);

            }
            catch
            {
                MessageBox.Show("请输入符合要求的差额百分比数值、PPM值或确认日期时间偏差标准值！");
                return;
            }

            //判断：100分对应的价格水平的差额百分比值（P LOW），应小于，0分对应的价格水平的差额百分比值（P HIGH）
            if (priceLevelLow >= priceLevelHigh)
            {
                MessageBox.Show("100分对应的价格水平的差额百分比值应小于0分对应的价格水平的差额百分比值!");
                return;
            }
          
            //判断：100分对应的价格历史的差额百分比值（P LOW），应小于，0分对应的价格历史的差额百分比值（P HIGH）
            if (priceHistoryLow >= priceHistoryHigh)
            {
                MessageBox.Show("100分对应的价格历史的差额百分比值应小于0分对应的价格历史的差额百分比值!");
                return;
            }

            //判断：100分对应的PPM，值（P LOW），应小于，0分对应的PPM值（P HIGH），且PPM值要大于0
            if (PPMLow >= PPMHigh || PPMLow < 0 || PPMHigh <= 0)
            {
                MessageBox.Show("100分对应的PPM值应大于0，0分对应的PPM值应大于等于0，且100分对应的PPM值应小于0分对应的PPM值!");
                return;
            }

            //list赋值前判断是否已经有值
            if (priceLevelPara.Count != 0)
            {
                priceLevelPara.Clear();
            }
            if (priceHistoryPara.Count != 0)
            {
                priceHistoryPara.Clear();
            }
            if (PPMPara.Count != 0)
            {
                PPMPara.Clear();
            }
            if (confirmDatePara.Count != 0)
            {
                confirmDatePara.Clear();
            }
            priceLevelPara.Add(priceLevelLow);
            priceLevelPara.Add(priceLevelHigh);
            priceHistoryPara.Add(priceHistoryLow);
            priceHistoryPara.Add(priceHistoryHigh);
            PPMPara.Add(PPMLow);
            PPMPara.Add(PPMHigh);
            confirmDatePara.Add(confirmDateParatmp);

            this.Close();
        }
    }
}

