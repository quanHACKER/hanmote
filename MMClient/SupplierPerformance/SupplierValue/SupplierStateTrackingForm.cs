﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class SupplierStateTrackingForm : DockContent
    {
        String year;
        SupplierValueBLL supplierValueBLL = new SupplierValueBLL();
        DataTable industryTable;
        String industry;

        public SupplierStateTrackingForm()
        {
            InitializeComponent();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            ////选择行业类别
            //industryTable = supplierValueBLL.queryIndustry();
            //comboBox_industry.DataSource = industryTable;
            //comboBox_industry.DisplayMember = industryTable.Columns["Industry_Category"].ToString();
            //comboBox_industry.ValueMember = industryTable.Columns["Industry_Category"].ToString();
            //comboBox_industry.SelectedIndex = 1;
            //industry = comboBox_industry.Text.ToString();

            //选择行业类别
            industryTable = supplierValueBLL.queryIndustry();
            comboBox_industry.DataSource = industryTable;
            comboBox_industry.DisplayMember = industryTable.Columns["Industry_Category"].ToString();
        }

        /// <summary>
        /// 年份
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            year = comboBox_year.Text.ToString();
        }

        /// <summary>
        /// 查看Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            DataTable supplierStateTable = supplierValueBLL.querySupplierState(year,industry);
            dataGridView1.Rows.Clear();

            for (int i = 0; i < supplierStateTable.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView1.Rows.Add();
                DataGridViewRow row = this.dataGridView1.Rows[i];
                //供应商id
                row.Cells[0].Value = supplierStateTable.Rows[i][0].ToString();
                //供应商名
                row.Cells[1].Value = supplierStateTable.Rows[i][1].ToString();
                //供应商定位
                row.Cells[2].Value = supplierStateTable.Rows[i][2].ToString();
                //添加Button
                row.Cells[4].Value = "确定";
            }
        }

        //判断是否点击“确认”按钮
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //如果是“确认”button
            try
            {
                if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
                {
                   int rowNum = dataGridView1.CurrentRow.Index;
                   String state = dataGridView1.Rows[rowNum].Cells[3].Value.ToString();
                   String supplierID = dataGridView1.Rows[rowNum].Cells[0].Value.ToString();

                    //修改供应商状态，在Supplier_Base中修改
                   int lines = supplierValueBLL.changeSupplierState(supplierID,state);

                   if (lines > 0)
                   {
                       MessageBox.Show("保存成功！");
                       return;
                   }
                   else
                   {
                       MessageBox.Show("保存失败！");
                       return;
                   }
                }
            }
            catch
            {
                MessageBox.Show("状态无变动！");
                return;
            }
        }

        /// <summary>
        /// 行业类别 ComboBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_industry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //industry = comboBox_industry.SelectedValue.ToString();

            industry = comboBox_industry.Text.ToString();
        }
    }
}
