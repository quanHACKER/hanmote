﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL.CertificationProcess;

namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class HandelStrategy : Form
    {
        private string od;
        private string oe;
        private string sd;
        private string se;
        FTPHelper fTPHelper = new FTPHelper("");
        public HandelStrategy(string orgid,string orgname,string supplierid,string suppliername)
        {

            this.od = orgid;
            this.oe = orgname;
            this.sd = supplierid;
            this.se = suppliername;
            
            InitializeComponent();

           

        }

        private void HandelStrategy_Load(object sender, EventArgs e)
        {
            textBox2.Text = this.od;
            textBox1.Text = this.oe;
            textBox3.Text = this.sd;
            textBox4.Text = this.se;
            List<String> MtGroup_ID = new List<string>();

            String sql1 = " select  distinct Year  from  Supplier_Position where Supplier_ID='" + textBox3.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt1.Rows[i][0].ToString());
                }
            }
            comboBox2.DataSource = MtGroup_ID;



            String sql2 = "select Supplier_Position  from  Supplier_Position where Supplier_ID='" + textBox3.Text + "' and Year='"+ comboBox2 .Text+ "';";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            if (dt2.Rows.Count != 0)
            {
                textBox5.Text = dt2.Rows[0][0].ToString();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)//使区分结果与年度联动
        {
            List<String> MtGroup_ID = new List<string>();
            
            String sql1 = "select Supplier_Position  from  Supplier_Position where Supplier_ID='" + textBox3.Text + "' and Year='" + comboBox2.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                textBox5.Text = dt1.Rows[0][0].ToString();
            }

            if(dt1.Rows.Count == 0)
            {
                comboBox1.DataSource = null;
            }
           /* else if(dt1.Rows.Count > 1)
            {
                MessageUtil.ShowWarning("这一年保存的有重复的数据！");
            }*/
            else if(textBox5.Text.Equals("A"))
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("建立长期战略合作");
                richTextBox1.Text = "建立长期战略合作";

            }
            else if (textBox5.Text.Equals("B"))
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("支持改善活动");
                richTextBox1.Text = "支持改善活动";

            }
            else if (textBox5.Text.Equals("C"))
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("鼓励不断竞争，不断寻源");
                richTextBox1.Text = "鼓励不断竞争，不断寻源";

            }
            else if (textBox5.Text.Equals("D"))
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("警告,触发LPS流程");
                comboBox1.Items.Add("冻结，触发源清单流程");
                comboBox1.Items.Add("进入黑名单，启动供应商删除流程");
                



            }


        }

        private void button1_Click(object sender, EventArgs e)//发送
        {
            MessageUtil.ShowWarning("目前发送需求尚不明确，只完成了保存功能！");
        }

        private void button2_Click(object sender, EventArgs e)//提交
        {
            MessageUtil.ShowWarning("目前决策流程尚不明确，只完成了保存功能！");
        }

        private void button3_Click(object sender, EventArgs e)//保存
        {
            string sql = " update Supplier_Base set Strategy='"+ richTextBox1.Text + "', StrategyFounder='"+ textBox6.Text + "', StrategyDate='"+ dateTimePicker1.Text + "', StrategyFileName='"+ textBox8.Text + "' where Supplier_ID='" + textBox3.Text + "' ";
            DBHelper.ExecuteQueryDT(sql);

            string sql1 = " update Supplier_Position set Type='1' where Supplier_ID='" + textBox3.Text + "' ";
            DBHelper.ExecuteQueryDT(sql1);

            MessageUtil.ShowWarning("保存成功");

        }

        private void button4_Click(object sender, EventArgs e)//上传文档
        {
            this.fTPHelper.MakeDir(textBox3.Text);
            string[] resultFile = null;
            FTPHelper fTPHelper1 = new FTPHelper(textBox3.Text);
            fTPHelper1.MakeDir("Strategy");
            FTPHelper fTPHelper3 = new FTPHelper(textBox3.Text + "/Strategy" );
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            string filePath = "/" + textBox3.Text + "/Strategy";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;

            }
            if (resultFile == null)
                MessageBox.Show("未选择文件");
            else
            {
                for (int i = 0; i < resultFile.Length; i++)
                    fTPHelper3.Upload(resultFile[i]);
                MessageUtil.ShowWarning("上传成功！");
                string file = "";

                if (resultFile != null)
                {
                    for (int i = 0; i < resultFile.Length; i++)
                    {
                        string[] sArray5 = Regex.Split(resultFile[i], @"\\", RegexOptions.IgnoreCase);


                       // FileUploadIDAL.InsertFileInfo(this.id, sArray5[sArray5.Length - 1], "Evaluation", SingleUserInstance.getCurrentUserInstance().Username, "评审员", filePath + "/");
                        file = file + ";" + sArray5[sArray5.Length - 1];

                    }
                }
                textBox8.Text = file;
                return;
            }
        }
    }
}
