﻿namespace MMClient.SupplierPerformance.SupplierValue
{
    partial class SupplierStateTrackingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.comboBox_year = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column_SupplierID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_SupplierPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Operate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column_Confirm = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_industry = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox_year
            // 
            this.comboBox_year.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_year.FormattingEnabled = true;
            this.comboBox_year.Location = new System.Drawing.Point(178, 45);
            this.comboBox_year.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_year.Name = "comboBox_year";
            this.comboBox_year.Size = new System.Drawing.Size(148, 25);
            this.comboBox_year.TabIndex = 61;
            this.comboBox_year.SelectedIndexChanged += new System.EventHandler(this.comboBox_year_SelectedIndexChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label32.Location = new System.Drawing.Point(97, 48);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 17);
            this.label32.TabIndex = 60;
            this.label32.Text = "年份：";
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierID,
            this.Column_SupplierName,
            this.Column_SupplierPosition,
            this.Column_Operate,
            this.Column_Confirm});
            this.dataGridView1.Location = new System.Drawing.Point(79, 229);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(616, 245);
            this.dataGridView1.TabIndex = 62;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column_SupplierID
            // 
            this.Column_SupplierID.HeaderText = "供应商ID";
            this.Column_SupplierID.Name = "Column_SupplierID";
            this.Column_SupplierID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.Name = "Column_SupplierName";
            // 
            // Column_SupplierPosition
            // 
            this.Column_SupplierPosition.HeaderText = "供应商定位状态";
            this.Column_SupplierPosition.Name = "Column_SupplierPosition";
            // 
            // Column_Operate
            // 
            this.Column_Operate.HeaderText = "状态修改";
            this.Column_Operate.Items.AddRange(new object[] {
            "集中锁定",
            "归档状态",
            "删除供应商"});
            this.Column_Operate.Name = "Column_Operate";
            this.Column_Operate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_Operate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column_Confirm
            // 
            this.Column_Confirm.HeaderText = "确认";
            this.Column_Confirm.Name = "Column_Confirm";
            this.Column_Confirm.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_Confirm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.comboBox_industry);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.comboBox_year);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(79, 49);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(616, 143);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "供应商清单查询";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(395, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 26);
            this.button1.TabIndex = 62;
            this.button1.Text = "查看\r\n";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "状态修改";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "集中锁定",
            "归档状态",
            "删除供应商"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 101;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(76, 495);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(497, 17);
            this.label1.TabIndex = 64;
            this.label1.Text = "供应商定位状态说明：A.优选供应商，B.有价值供应商，C.需改善供应商，D.可剔除供应商。";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 19);
            this.label2.TabIndex = 63;
            this.label2.Text = "行业类别：";
            // 
            // comboBox_industry
            // 
            this.comboBox_industry.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_industry.FormattingEnabled = true;
            this.comboBox_industry.Location = new System.Drawing.Point(178, 91);
            this.comboBox_industry.Name = "comboBox_industry";
            this.comboBox_industry.Size = new System.Drawing.Size(148, 25);
            this.comboBox_industry.TabIndex = 64;
            this.comboBox_industry.SelectedIndexChanged += new System.EventHandler(this.comboBox_industry_SelectedIndexChanged);
            // 
            // SupplierStateTrackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 566);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierStateTrackingForm";
            this.Text = "供应商清单";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_year;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierPosition;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column_Operate;
        private System.Windows.Forms.DataGridViewButtonColumn Column_Confirm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_industry;
        private System.Windows.Forms.Label label2;
    }
}