﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using WeifenLuo.WinFormsUI.Docking;
namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class SupplierManageStrategy : DockContent
    {
        public SupplierManageStrategy()
        {
            InitializeComponent();
        }

        private void SupplierManageStrategy_Load(object sender, EventArgs e)
        {
            //绑定采购组织下拉框
            List<String> MtGroup_ID = new List<string>();

            String sql1 = " select Buyer_Org  from  Buyer_Org ";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt1.Rows[i][0].ToString());
                }
            }
            comboBox1.DataSource = MtGroup_ID;

            String sql2 = "select Buyer_Org_Name  from  Buyer_Org where Buyer_Org='" + comboBox1.Text + "';";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            if (dt2.Rows.Count != 0)
            {
                textBox2.Text = dt2.Rows[0][0].ToString();
            }


            //刷列表数据
            string sql = String.Format(@" SELECT
												u.Year as '年度',
	                                            sr.SupplierId AS '供应商编号',
	                                            sr.CompanyName AS '供应商名称',
                                                 
	                                           
                                                CASE
                                                WHEN  u.Type = '0' THEN
	                                                '未处理'
                                                WHEN u.Type = '1' THEN
	                                                '已处理'
                                                END AS '处置'
                                                        FROM
	                                            

	                                            Supplier_Position u,
	                                            SR_Info sr
                                           
                                            WHERE
	                                             sr.SupplierId =u.Supplier_ID
                                            and sr.SelectedDep='" + textBox2.Text+"';");
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dataGridView1.DataSource = dt;

            

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)//使采购组织名称与采购组织ID联动
        {
            List<String> MtGroup_ID = new List<string>();





            //String sql1 = "SELECT distinct PurOrgName FROM [MTGAttributeDefinition] WHERE Category='生产性物料' and   PurOrgID='" + comboBox1.Text + "'";
            String sql1 = "select Buyer_Org_Name  from  Buyer_Org where Buyer_Org='" + comboBox1.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                textBox2.Text = dt1.Rows[0][0].ToString();
            }


            //String sql = " select Buyer_Org_Name  from  Buyer_Org ";
            /* String sql = "SELECT distinct id FROM [MTGAttributeDefinition] WHERE Category='生产性物料' and  PurOrgID='" + comboBox1.Text + "'";*/
            string sql = String.Format(@" SELECT
												u.Year as '年度',
	                                            sr.SupplierId AS '供应商编号',
	                                            sr.CompanyName AS '供应商名称',
                                                 
	                                           
                                                CASE
                                                WHEN  u.Type = '0' THEN
	                                                '未处理'
                                                WHEN u.Type = '1' THEN
	                                                '已处理'
                                                END AS '处置'
                                            FROM
	                                            

	                                            Supplier_Position u,
	                                            SR_Info sr
                                           
                                            WHERE
	                                             sr.SupplierId =u.Supplier_ID
                                            and sr.SelectedDep='" + textBox2.Text + "';");
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            /*if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }*/
            dataGridView1.DataSource = dt;

            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "处理";
            //this.dataGridView1.Columns.AddRange(button1);
            if (dataGridView1.Columns.Count >= 5)
            {
                dataGridView1.Columns.Remove("button1");
            }
            dataGridView1.Columns.Add(button1);
           // this.dataGridView1.Columns["物料组名称"].FillWeight = 100;//设置某一列的宽度，数值为比例值


           
        }

        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {


                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//单击处理对应事件
                {
                    HandelStrategy a = new HandelStrategy(comboBox1.Text, textBox2.Text, this.dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString(), this.dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString());
                    a.Show();
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)//
        {

        }
    }
}
