﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class NewGenServiceModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceModelName = new System.Windows.Forms.ComboBox();
            this.Re5 = new System.Windows.Forms.TextBox();
            this.Inno5 = new System.Windows.Forms.TextBox();
            this.Re4 = new System.Windows.Forms.TextBox();
            this.Inno4 = new System.Windows.Forms.TextBox();
            this.Re3 = new System.Windows.Forms.TextBox();
            this.Inno3 = new System.Windows.Forms.TextBox();
            this.Re2 = new System.Windows.Forms.TextBox();
            this.Inno2 = new System.Windows.Forms.TextBox();
            this.Re1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Inno1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.US1 = new System.Windows.Forms.TextBox();
            this.US2 = new System.Windows.Forms.TextBox();
            this.US3 = new System.Windows.Forms.TextBox();
            this.US4 = new System.Windows.Forms.TextBox();
            this.US5 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serviceModelName
            // 
            this.serviceModelName.FormattingEnabled = true;
            this.serviceModelName.Location = new System.Drawing.Point(71, 10);
            this.serviceModelName.Name = "serviceModelName";
            this.serviceModelName.Size = new System.Drawing.Size(188, 20);
            this.serviceModelName.TabIndex = 13;
            this.serviceModelName.TextChanged += new System.EventHandler(this.serviceModelName_TextChanged);
            // 
            // Re5
            // 
            this.Re5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Re5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Re5.Location = new System.Drawing.Point(375, 260);
            this.Re5.Margin = new System.Windows.Forms.Padding(0);
            this.Re5.Multiline = true;
            this.Re5.Name = "Re5";
            this.Re5.Size = new System.Drawing.Size(312, 57);
            this.Re5.TabIndex = 17;
            // 
            // Inno5
            // 
            this.Inno5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inno5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inno5.Location = new System.Drawing.Point(62, 260);
            this.Inno5.Margin = new System.Windows.Forms.Padding(0);
            this.Inno5.Multiline = true;
            this.Inno5.Name = "Inno5";
            this.Inno5.Size = new System.Drawing.Size(312, 57);
            this.Inno5.TabIndex = 16;
            // 
            // Re4
            // 
            this.Re4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Re4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Re4.Location = new System.Drawing.Point(375, 203);
            this.Re4.Margin = new System.Windows.Forms.Padding(0);
            this.Re4.Multiline = true;
            this.Re4.Name = "Re4";
            this.Re4.Size = new System.Drawing.Size(312, 56);
            this.Re4.TabIndex = 15;
            // 
            // Inno4
            // 
            this.Inno4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inno4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inno4.Location = new System.Drawing.Point(62, 203);
            this.Inno4.Margin = new System.Windows.Forms.Padding(0);
            this.Inno4.Multiline = true;
            this.Inno4.Name = "Inno4";
            this.Inno4.Size = new System.Drawing.Size(312, 56);
            this.Inno4.TabIndex = 14;
            // 
            // Re3
            // 
            this.Re3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Re3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Re3.Location = new System.Drawing.Point(375, 146);
            this.Re3.Margin = new System.Windows.Forms.Padding(0);
            this.Re3.Multiline = true;
            this.Re3.Name = "Re3";
            this.Re3.Size = new System.Drawing.Size(312, 56);
            this.Re3.TabIndex = 13;
            // 
            // Inno3
            // 
            this.Inno3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inno3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inno3.Location = new System.Drawing.Point(62, 146);
            this.Inno3.Margin = new System.Windows.Forms.Padding(0);
            this.Inno3.Multiline = true;
            this.Inno3.Name = "Inno3";
            this.Inno3.Size = new System.Drawing.Size(312, 56);
            this.Inno3.TabIndex = 12;
            // 
            // Re2
            // 
            this.Re2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Re2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Re2.Location = new System.Drawing.Point(375, 89);
            this.Re2.Margin = new System.Windows.Forms.Padding(0);
            this.Re2.Multiline = true;
            this.Re2.Name = "Re2";
            this.Re2.Size = new System.Drawing.Size(312, 56);
            this.Re2.TabIndex = 11;
            // 
            // Inno2
            // 
            this.Inno2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inno2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inno2.Location = new System.Drawing.Point(62, 89);
            this.Inno2.Margin = new System.Windows.Forms.Padding(0);
            this.Inno2.Multiline = true;
            this.Inno2.Name = "Inno2";
            this.Inno2.Size = new System.Drawing.Size(312, 56);
            this.Inno2.TabIndex = 10;
            // 
            // Re1
            // 
            this.Re1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Re1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Re1.Location = new System.Drawing.Point(375, 32);
            this.Re1.Margin = new System.Windows.Forms.Padding(0);
            this.Re1.Multiline = true;
            this.Re1.Name = "Re1";
            this.Re1.Size = new System.Drawing.Size(312, 56);
            this.Re1.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(378, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(306, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "可靠性";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 11;
            this.label9.Text = "模板名称:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(833, 389);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(728, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // supplierLabel
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 10);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(54, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "分数";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(306, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "创新性";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "1";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "3";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "4";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 282);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "5";
            // 
            // Inno1
            // 
            this.Inno1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inno1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inno1.Location = new System.Drawing.Point(62, 32);
            this.Inno1.Margin = new System.Windows.Forms.Padding(0);
            this.Inno1.Multiline = true;
            this.Inno1.Name = "Inno1";
            this.Inno1.Size = new System.Drawing.Size(312, 56);
            this.Inno1.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.US5, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.US4, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.US3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.US2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label10, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.Re5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.Inno5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.Re4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.Inno4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.Re3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.Inno3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.Re2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.Inno2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Re1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Inno1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.US1, 3, 1);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 43);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1004, 318);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Location = new System.Drawing.Point(691, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(309, 12);
            this.label10.TabIndex = 18;
            this.label10.Text = "用户服务";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // US1
            // 
            this.US1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.US1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.US1.Location = new System.Drawing.Point(688, 32);
            this.US1.Margin = new System.Windows.Forms.Padding(0);
            this.US1.Multiline = true;
            this.US1.Name = "US1";
            this.US1.Size = new System.Drawing.Size(315, 56);
            this.US1.TabIndex = 19;
            // 
            // US2
            // 
            this.US2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.US2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.US2.Location = new System.Drawing.Point(688, 89);
            this.US2.Margin = new System.Windows.Forms.Padding(0);
            this.US2.Multiline = true;
            this.US2.Name = "US2";
            this.US2.Size = new System.Drawing.Size(315, 56);
            this.US2.TabIndex = 20;
            // 
            // US3
            // 
            this.US3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.US3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.US3.Location = new System.Drawing.Point(688, 146);
            this.US3.Margin = new System.Windows.Forms.Padding(0);
            this.US3.Multiline = true;
            this.US3.Name = "US3";
            this.US3.Size = new System.Drawing.Size(315, 56);
            this.US3.TabIndex = 21;
            // 
            // US4
            // 
            this.US4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.US4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.US4.Location = new System.Drawing.Point(688, 203);
            this.US4.Margin = new System.Windows.Forms.Padding(0);
            this.US4.Multiline = true;
            this.US4.Name = "US4";
            this.US4.Size = new System.Drawing.Size(315, 56);
            this.US4.TabIndex = 22;
            // 
            // US5
            // 
            this.US5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.US5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.US5.Location = new System.Drawing.Point(688, 260);
            this.US5.Margin = new System.Windows.Forms.Padding(0);
            this.US5.Multiline = true;
            this.US5.Name = "US5";
            this.US5.Size = new System.Drawing.Size(315, 57);
            this.US5.TabIndex = 23;
            // 
            // NewGenServiceModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 449);
            this.Controls.Add(this.serviceModelName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "NewGenServiceModel";
            this.Text = "新建模板";
            this.Load += new System.EventHandler(this.NewGenServiceModel_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox serviceModelName;
        private System.Windows.Forms.TextBox Re5;
        private System.Windows.Forms.TextBox Inno5;
        private System.Windows.Forms.TextBox Re4;
        private System.Windows.Forms.TextBox Inno4;
        private System.Windows.Forms.TextBox Re3;
        private System.Windows.Forms.TextBox Inno3;
        private System.Windows.Forms.TextBox Re2;
        private System.Windows.Forms.TextBox Inno2;
        private System.Windows.Forms.TextBox Re1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Inno1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox US5;
        private System.Windows.Forms.TextBox US4;
        private System.Windows.Forms.TextBox US3;
        private System.Windows.Forms.TextBox US2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox US1;
    }
}