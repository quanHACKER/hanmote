﻿using Lib.Common.CommonUtils;
using Lib.Model.ServiceEvaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Bll.ServiceBll;
using WeifenLuo.WinFormsUI.Docking;
namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class ExServiceForm : DockContent
    {
        Dictionary<string, string> supplierMap = null;
        Dictionary<string, string> mtGroupMap = null;
        Dictionary<string, string> mtIdMap = null;
        ServiceBill serviceBill = new ServiceBill();
        public ExServiceForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NewServiceModel newServiceModel = new NewServiceModel();
            newServiceModel.Show();
        }

        /// <summary>
        /// 保存外部服务数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            DataTable dt = serviceBill.getExServiceRate(this.MtGroupId.Text);

            ServiceModel serviceModel = new ServiceModel();
            serviceModel.SupplierId = this.supplierId.Text;
            serviceModel.MtGroupName = this.MTGroupName.Text;
            serviceModel.MtName = this.mtName.Text;
            serviceModel.MtID = this.mtId.Text;

            serviceModel.ExtServiceId = this.ExtServiceId.Text;
            serviceModel.ServicePlace = this.servicePlace.Text;
            serviceModel.Creator = this.creator.Text;
            serviceModel.ServiceTime = DateTime.Now;
          
            serviceModel.ServiceTimeScore =float.Parse(showmm(this.panel3))*20;
            
            serviceModel.ServiceTimeReason = this.STReason.Text.ToString();
          
            serviceModel.ServiceQualityScroce = float.Parse(showmm(this.panel4))*20;
            serviceModel.ServiceQualityReason = this.SQReason.Text.ToString();
            serviceModel.SupplierName = this.supplierName.Text;
           
            if (serviceModel.ServiceTimeScore==0 || serviceModel.ServiceQualityScroce==0) {
                MessageUtil.ShowError("请填写分数!");
                return;
            }
            serviceModel.ServiceTotalScore = (serviceModel.ServiceTimeScore + serviceModel.ServiceQualityScroce) / 2;
            if (dt.Rows.Count>0) {
                serviceModel.STRate1 = dt.Rows[0]["服务及时性"].ToString();
                serviceModel.SQRate1 = dt.Rows[0]["服务质量"].ToString();
                serviceModel.ServiceTotalScore = (serviceModel.ServiceTimeScore * float.Parse(serviceModel.STRate1.TrimEnd('%')) + serviceModel.ServiceQualityScroce * float.Parse(serviceModel.SQRate1.TrimEnd('%'))) / 100;

            }

            if (serviceBill.insertServiceData(serviceModel)) {
                MessageUtil.ShowTips("保存成功！");

            }

            
        }

        private string showmm(Panel panel)
        {
        
            foreach (RadioButton rad in panel.Controls)
            {
              
                    if (rad.Checked)
                    {
                    return rad.Tag.ToString();
                    }
                
            }
            MessageUtil.ShowError("未选择分数！");
            return "0";
        }

        private void clear(Panel panel)
        {

            foreach (RadioButton rad in panel.Controls)
            {

                if (rad.Checked)
                {
                    rad.Checked=false;
                }

            }
        
        }


        //初始化数据
        private void ExServiceForm_Load(object sender, EventArgs e)
        {


            //供应商Id
            // supplierMap = serviceBill.getSupplierId(SingleUserInstance.getCurrentUserInstance().User_ID);
            supplierMap = serviceBill.getSupplierId("20180416181829");
            if (supplierMap.Count == 0)
            {
                MessageUtil.ShowTips("没有需要评估的供应商！");
            }
            List<String> supplierId = new List<string>();

            foreach (String key in supplierMap.Keys)
            {
                supplierId.Add(key);
            }
            this.supplierId.DataSource = supplierId;

            //物料组Id
            mtGroupMap = serviceBill.getMtGroupidAndName(this.supplierId.Text.ToString());

            if (mtGroupMap.Count == 0)
            {
                MessageUtil.ShowTips("没有物料组！");
            }
            List<String> mtGroupId = new List<string>();

            foreach (String key in mtGroupMap.Keys)
            {
                mtGroupId.Add(key);
            }
            this.MtGroupId.DataSource = mtGroupId;
            mtGroupMap.TryGetValue(this.MtGroupId.Text, out string mtGName);
            this.MTGroupName.Text = mtGName;

            //物料Id
            mtIdMap = serviceBill.getMtidAndName(this.MtGroupId.Text.ToString());

            if (mtIdMap.Count == 0)
            {
                MessageUtil.ShowTips("没有物料！");
            }
            List<String> mtIdList = new List<string>();

            foreach (String key in mtIdMap.Keys)
            {
                mtIdList.Add(key);
            }
            this.mtId.DataSource = mtIdList;
            mtIdMap.TryGetValue (this.mtId.Text, out string mtName);
            this.mtName.Text = mtName;
          
            supplierMap.TryGetValue (this.supplierId.Text, out string value);
            this.supplierName.Text = value;
            this.creator.Text = SingleUserInstance.getCurrentUserInstance().Username;
            this.createTime.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.ExtServiceId.Text = DateTime.Now.ToString("yyyymmddhhss");
            //初始化模板
            this.serviceModelName.DataSource = serviceBill.getServiceModelName();
            if (this.serviceModelName.Items.Count == 0) {
                MessageUtil.ShowTips("请初始化模板");
            }


        }

        private void supplierId_TextChanged(object sender, EventArgs e)
        {
            supplierMap.TryGetValue(this.supplierId.Text, out string value);

            this.supplierName.Text = value;

            //物料组
            mtGroupMap = serviceBill.getMtGroupidAndName(this.supplierId.Text.ToString());

            if (mtGroupMap.Count == 0)
            {
                MessageUtil.ShowTips("没有物料组！");
            }
            List<String> mtGroupId = new List<string>();

            foreach (String key in mtGroupMap.Keys)
            {
                mtGroupId.Add(key);
            }
            this.MtGroupId.DataSource = mtGroupId;
            mtGroupMap.TryGetValue(this.MtGroupId.Text, out string mtGName);
            this.MTGroupName.Text = mtGName;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void supplierId_Validating(object sender, CancelEventArgs e)
        {
            if (supplierId.Text.Equals(""))
            {
                errorUser.SetError(supplierId, "不能为空！");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(supplierId, "");
            }
        }

        private void servicePlace_Validating(object sender, CancelEventArgs e)
        {
            if (servicePlace.Text.Equals(""))
            {
                errorUser.SetError(servicePlace, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(servicePlace, "");
            }
        }

        private void mtId_Validating(object sender, CancelEventArgs e)
        {
            if (mtId.Text.Equals(""))
            {
                errorUser.SetError(mtId, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(mtId, "");
            }
        }

        private void STReason_Validating(object sender, CancelEventArgs e)
        {
            if (STReason.Text.Equals(""))
            {
                errorUser.SetError(STReason, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(STReason, "");
            }
        }

        private void SQReason_Validating(object sender, CancelEventArgs e)
        {
            if (SQReason.Text.Equals(""))
            {
                errorUser.SetError(SQReason, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(SQReason, "");
            }
        }

        private void reset_Click(object sender, EventArgs e)
        {
            this.servicePlace.Text = "";
            this.SQReason.Text = "";
            this.STReason.Text = "";
            clear(this.panel3);
            clear(this.panel4);
            ExServiceForm_Load(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ServiceSummry serviceSummry = new ServiceSummry();
            serviceSummry.Show();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            button3_Click( sender,  e);
        }

        private void label32_Click(object sender, EventArgs e)
        {

        }

        private void serviceModel_TextChanged(object sender, EventArgs e)
        {
            DataTable dt= serviceBill.getServiceModel(this.serviceModelName.Text);
            //质量
            string [] sQs =dt.Rows[0]["sQs"].ToString().Split(',');
            //及时性
            string[] sTs = dt.Rows[0]["sTs"].ToString().Split(',');
            if (sQs.Length == 0) {

                return;
            }
            this.SQ1.Text = sQs[0];
            this.SQ2.Text = sQs[1];
            this.SQ3.Text = sQs[2];
            this.SQ4.Text = sQs[3];
            this.SQ5.Text = sQs[4];

            this.ST1.Text = sTs[0];
            this.ST2.Text = sTs[1];
            this.ST3.Text = sTs[2];
            this.ST4.Text = sTs[3];
            this.ST5.Text = sTs[4];


        }

        private void mtId_TextChanged(object sender, EventArgs e)
        {
          
            mtIdMap.TryGetValue(this.mtId.Text, out string mtName);
            this.mtName.Text = mtName;


        }

       
        private void MtGroupId_TextChanged(object sender, EventArgs e)
        {
            //物料Id
            mtIdMap = serviceBill.getMtidAndName(this.MtGroupId.Text.ToString());

            if (mtIdMap.Count == 0)
            {
                MessageUtil.ShowTips("没有物料！");
            }
            List<String> mtIdList = new List<string>();

            foreach (String key in mtIdMap.Keys)
            {
                mtIdList.Add(key);
            }
            this.mtId.DataSource = mtIdList;
            mtIdMap.TryGetValue(this.mtId.Text, out string mtName);
            this.mtName.Text = mtName;
        }
    }
}
