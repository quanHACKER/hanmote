﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class ServiceSummry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.MaterialName = new System.Windows.Forms.ComboBox();
            this.MateGroupName = new System.Windows.Forms.ComboBox();
            this.supplierName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mtGroupName = new System.Windows.Forms.Label();
            this.供应商 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.服务工单 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.综合 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.质量评分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.质量权重 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.及时性评分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.及时性权重 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.endTime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.startTime);
            this.panel1.Controls.Add(this.MaterialName);
            this.panel1.Controls.Add(this.MateGroupName);
            this.panel1.Controls.Add(this.supplierName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.mtGroupName);
            this.panel1.Controls.Add(this.供应商);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1058, 561);
            this.panel1.TabIndex = 0;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(508, 36);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 15;
            this.searchBtn.Text = "查询";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(447, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 14;
            this.label2.Text = "--";
            // 
            // endTime
            // 
            this.endTime.Location = new System.Drawing.Point(475, 6);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(126, 21);
            this.endTime.TabIndex = 12;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(254, 12);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "日期选择";
            // 
            // startTime
            // 
            this.startTime.Location = new System.Drawing.Point(315, 6);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(126, 21);
            this.startTime.TabIndex = 10;
            // 
            // MaterialName
            // 
            this.MaterialName.FormattingEnabled = true;
            this.MaterialName.Location = new System.Drawing.Point(315, 38);
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.Size = new System.Drawing.Size(126, 20);
            this.MaterialName.TabIndex = 9;
            // 
            // MateGroupName
            // 
            this.MateGroupName.FormattingEnabled = true;
            this.MateGroupName.Location = new System.Drawing.Point(83, 38);
            this.MateGroupName.Name = "MateGroupName";
            this.MateGroupName.Size = new System.Drawing.Size(127, 20);
            this.MateGroupName.TabIndex = 8;
            this.MateGroupName.TextChanged += new System.EventHandler(this.MateGroupName_TextChanged);
            // 
            // supplierName
            // 
            this.supplierName.FormattingEnabled = true;
            this.supplierName.Location = new System.Drawing.Point(83, 9);
            this.supplierName.Name = "supplierName";
            this.supplierName.Size = new System.Drawing.Size(127, 20);
            this.supplierName.TabIndex = 7;
            this.supplierName.TextChanged += new System.EventHandler(this.supplierName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(254, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "物料";
            // 
            // mtGroupName
            // 
            this.mtGroupName.AutoSize = true;
            this.mtGroupName.Location = new System.Drawing.Point(12, 41);
            this.mtGroupName.Name = "mtGroupName";
            this.mtGroupName.Size = new System.Drawing.Size(41, 12);
            this.mtGroupName.TabIndex = 5;
            this.mtGroupName.Text = "物料组";
            // 
            // 供应商
            // 
            this.供应商.AutoSize = true;
            this.供应商.Location = new System.Drawing.Point(12, 13);
            this.供应商.Name = "供应商";
            this.供应商.Size = new System.Drawing.Size(65, 12);
            this.供应商.TabIndex = 4;
            this.供应商.Text = "供应商名称";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.服务工单,
            this.供应商代码,
            this.供应商名称,
            this.物料组名称,
            this.物料名称,
            this.单位,
            this.时间,
            this.综合,
            this.质量评分,
            this.质量权重,
            this.及时性评分,
            this.及时性权重});
            this.dataGridView1.Location = new System.Drawing.Point(14, 71);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1044, 435);
            this.dataGridView1.TabIndex = 0;
            // 
            // 服务工单
            // 
            this.服务工单.DataPropertyName = "服务工单";
            this.服务工单.HeaderText = "服务工单";
            this.服务工单.Name = "服务工单";
            // 
            // 供应商代码
            // 
            this.供应商代码.DataPropertyName = "供应商代码";
            this.供应商代码.HeaderText = "供应商代码";
            this.供应商代码.Name = "供应商代码";
            // 
            // 供应商名称
            // 
            this.供应商名称.DataPropertyName = "供应商名称";
            this.供应商名称.HeaderText = "供应商名称";
            this.供应商名称.Name = "供应商名称";
            // 
            // 物料组名称
            // 
            this.物料组名称.DataPropertyName = "物料组名称";
            this.物料组名称.HeaderText = "物料组名称";
            this.物料组名称.Name = "物料组名称";
            // 
            // 物料名称
            // 
            this.物料名称.DataPropertyName = "物料名称";
            this.物料名称.HeaderText = "物料名称";
            this.物料名称.Name = "物料名称";
            // 
            // 单位
            // 
            this.单位.DataPropertyName = "单位";
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            // 
            // 时间
            // 
            this.时间.DataPropertyName = "时间";
            this.时间.HeaderText = "时间";
            this.时间.Name = "时间";
            // 
            // 综合
            // 
            this.综合.DataPropertyName = "综合";
            this.综合.HeaderText = "综合";
            this.综合.Name = "综合";
            // 
            // 质量评分
            // 
            this.质量评分.DataPropertyName = "质量评分";
            this.质量评分.HeaderText = "质量评分";
            this.质量评分.Name = "质量评分";
            // 
            // 质量权重
            // 
            this.质量权重.DataPropertyName = "质量权重";
            this.质量权重.HeaderText = "质量权重";
            this.质量权重.Name = "质量权重";
            // 
            // 及时性评分
            // 
            this.及时性评分.DataPropertyName = "及时性评分";
            this.及时性评分.HeaderText = "及时性评分";
            this.及时性评分.Name = "及时性评分";
            // 
            // 及时性权重
            // 
            this.及时性权重.DataPropertyName = "及时性权重";
            this.及时性权重.HeaderText = "及时性权重";
            this.及时性权重.Name = "及时性权重";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(631, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "返回";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ServiceSummry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 568);
            this.Controls.Add(this.panel1);
            this.Name = "ServiceSummry";
            this.Text = "外部服务汇总";
            this.Load += new System.EventHandler(this.ServiceSummry_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.ComboBox MaterialName;
        private System.Windows.Forms.ComboBox MateGroupName;
        private System.Windows.Forms.ComboBox supplierName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label mtGroupName;
        private System.Windows.Forms.Label 供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn 服务工单;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 综合;
        private System.Windows.Forms.DataGridViewTextBoxColumn 质量评分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 质量权重;
        private System.Windows.Forms.DataGridViewTextBoxColumn 及时性评分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 及时性权重;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}