﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class IntialServiceRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mtGroupRate = new System.Windows.Forms.DataGridView();
            this.mtGroupId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceTimeRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceInnovate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceReliable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userService = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.MTGName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtGroupRate)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.MTGName);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.mtGroupRate);
            this.panel1.Controls.Add(this.saveBtn);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(760, 555);
            this.panel1.TabIndex = 0;
            // 
            // mtGroupRate
            // 
            this.mtGroupRate.AllowUserToAddRows = false;
            this.mtGroupRate.BackgroundColor = System.Drawing.Color.White;
            this.mtGroupRate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mtGroupRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtGroupRate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mtGroupId,
            this.mtGroupName,
            this.serviceQuality,
            this.serviceTimeRate,
            this.serviceInnovate,
            this.serviceReliable,
            this.userService});
            this.mtGroupRate.Location = new System.Drawing.Point(6, 55);
            this.mtGroupRate.Name = "mtGroupRate";
            this.mtGroupRate.RowTemplate.Height = 23;
            this.mtGroupRate.Size = new System.Drawing.Size(741, 299);
            this.mtGroupRate.TabIndex = 0;
            // 
            // mtGroupId
            // 
            this.mtGroupId.DataPropertyName = "mtGroupId";
            this.mtGroupId.HeaderText = "物料组编号";
            this.mtGroupId.Name = "mtGroupId";
            this.mtGroupId.ReadOnly = true;
            // 
            // mtGroupName
            // 
            this.mtGroupName.DataPropertyName = "mtGroupName";
            this.mtGroupName.HeaderText = "物料组名称";
            this.mtGroupName.Name = "mtGroupName";
            this.mtGroupName.ReadOnly = true;
            // 
            // serviceQuality
            // 
            this.serviceQuality.DataPropertyName = "serviceQuality";
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.serviceQuality.DefaultCellStyle = dataGridViewCellStyle11;
            this.serviceQuality.HeaderText = "质量";
            this.serviceQuality.Name = "serviceQuality";
            this.serviceQuality.ToolTipText = "输入质量占比";
            // 
            // serviceTimeRate
            // 
            this.serviceTimeRate.DataPropertyName = "serviceTimeRate";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.serviceTimeRate.DefaultCellStyle = dataGridViewCellStyle12;
            this.serviceTimeRate.HeaderText = "及时性";
            this.serviceTimeRate.Name = "serviceTimeRate";
            this.serviceTimeRate.ToolTipText = "输入及时性占比";
            // 
            // serviceInnovate
            // 
            this.serviceInnovate.DataPropertyName = "serviceInnovate";
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.serviceInnovate.DefaultCellStyle = dataGridViewCellStyle13;
            this.serviceInnovate.HeaderText = "创新性";
            this.serviceInnovate.Name = "serviceInnovate";
            this.serviceInnovate.ToolTipText = "输入创新性占比";
            // 
            // serviceReliable
            // 
            this.serviceReliable.DataPropertyName = "serviceReliable";
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.serviceReliable.DefaultCellStyle = dataGridViewCellStyle14;
            this.serviceReliable.HeaderText = "可靠性";
            this.serviceReliable.Name = "serviceReliable";
            this.serviceReliable.ToolTipText = "输入可靠性占比";
            // 
            // userService
            // 
            this.userService.DataPropertyName = "userService";
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.userService.DefaultCellStyle = dataGridViewCellStyle15;
            this.userService.HeaderText = "用户服务";
            this.userService.Name = "userService";
            this.userService.ToolTipText = "输入用户服务占比";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(563, 16);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 1;
            this.saveBtn.Text = "批量保存";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(672, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "返回";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(199, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "查询";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MTGName
            // 
            this.MTGName.Location = new System.Drawing.Point(76, 16);
            this.MTGName.Name = "MTGName";
            this.MTGName.Size = new System.Drawing.Size(117, 21);
            this.MTGName.TabIndex = 4;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 21);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "物料组";
            // 
            // IntialServiceRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(784, 630);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "IntialServiceRate";
            this.Text = "自定义服务比例";
            this.Load += new System.EventHandler(this.IntialServiceRate_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtGroupRate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView mtGroupRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGroupId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceTimeRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceInnovate;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceReliable;
        private System.Windows.Forms.DataGridViewTextBoxColumn userService;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MTGName;
        private System.Windows.Forms.Button button2;
    }
}