﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class NewServiceModel : Form
    {

        ServiceBill serviceBill = new ServiceBill();
        public NewServiceModel()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
            string SQs = SQ1.Text +","+ SQ2.Text + "," + SQ3.Text + "," + SQ4.Text + "," + SQ5.Text;
            string STs = ST1.Text + "," + ST2.Text + "," + ST3.Text + "," + ST4.Text + "," + ST5.Text;
            if (SQs.Split(',').Length < 5 || SQs.Split(',').Length < 5 || this.serviceModelName.Text.Equals("")) {

                MessageUtil.ShowTips("信息不完整！请完善信息！");
                return;

            }

            if (serviceBill.insertServiceModel(SQs, STs, this.serviceModelName.Text)) {

                MessageUtil.ShowTips("保存成功！");

            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageUtil.ShowTips("待完善");
        }

        private void NewServiceModel_Load(object sender, EventArgs e)
        {
            this.serviceModelName.DataSource = serviceBill.getServiceModelName();
            DataTable dt = serviceBill.getServiceModel(this.serviceModelName.Text);
            //质量
            string[] sQs = dt.Rows[0]["sQs"].ToString().Split(',');
            //及时性
            string[] sTs = dt.Rows[0]["sTs"].ToString().Split(',');
            if (sQs.Length == 0)
            {

                return;
            }
            this.SQ1.Text = sQs[0];
            this.SQ2.Text = sQs[1];
            this.SQ3.Text = sQs[2];
            this.SQ4.Text = sQs[3];
            this.SQ5.Text = sQs[4];

            this.ST1.Text = sTs[0];
            this.ST2.Text = sTs[1];
            this.ST3.Text = sTs[2];
            this.ST4.Text = sTs[3];
            this.ST5.Text = sTs[4];

        }
    }
}
