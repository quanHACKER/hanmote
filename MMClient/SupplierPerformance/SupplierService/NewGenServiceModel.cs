﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class NewGenServiceModel : Form
    {

        ServiceBill serviceBill = new ServiceBill();
        public NewGenServiceModel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string Innos = Inno1.Text + "," + Inno2.Text + "," + Inno3.Text + "," + Inno4.Text + "," + Inno5.Text;
            string Res = Re1.Text + "," + Re2.Text + "," + Re3.Text + "," + Re4.Text + "," + Re5.Text;
            string USs = US1.Text + "," + US2.Text + "," + US3.Text + "," + US4.Text + "," + US5.Text;
            if (Innos.Split(',').Length < 5 || Innos.Split(',').Length < 5 || this.serviceModelName.Text.Equals(""))
            {

                MessageUtil.ShowTips("信息不完整！请完善信息！");
                return;

            }

            if (serviceBill.insertGenServiceModel(Innos, Res, USs, this.serviceModelName.Text))
            {

                MessageUtil.ShowTips("保存成功！");

            }
        }

        private void serviceModelName_TextChanged(object sender, EventArgs e)
        {
            
            DataTable dt = serviceBill.getGenServiceModel(this.serviceModelName.Text);
            //质量
            string[] Innos = dt.Rows[0]["Innos"].ToString().Split(',');
            //及时性
            string[] Res = dt.Rows[0]["Res"].ToString().Split(',');

            string[] USs = dt.Rows[0]["USs"].ToString().Split(',');
            if (Innos.Length <5)
            {       
                return;
            }
            this.Re1.Text = Res[0];
            this.Re2.Text = Res[1];
            this.Re3.Text = Res[2];
            this.Re4.Text = Res[3];
            this.Re5.Text = Res[4];

            this.Inno1.Text = Innos[0];
            this.Inno2.Text = Innos[1];
            this.Inno3.Text = Innos[2];
            this.Inno4.Text = Innos[3];
            this.Inno5.Text = Innos[4];

            this.US1.Text = USs[0];
            this.US2.Text = USs[1];
            this.US3.Text = USs[2];    
            this.US4.Text = USs[3];
            this.US5.Text = USs[4];


        }

        private void NewGenServiceModel_Load(object sender, EventArgs e)
        {
            this.serviceModelName.DataSource = serviceBill.getGenServiceModelName();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
