﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Model.ServiceEvaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class IntialServiceRate : DockContent
    {
        ServiceBill serviceBill = new ServiceBill();
        mtGroupServiceModel mtGroupServiceModel = new mtGroupServiceModel();
        public IntialServiceRate()
        {
            InitializeComponent();
        }

        private void IntialServiceRate_Load(object sender, EventArgs e)
        {
            //获取所有的物料组

            mtGroupRate.DataSource = serviceBill.getServiceRate();
          


        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            try {
                bool flag = serviceBill.insertServiceRate(mtGroupRate);
                if (flag)
                {

                    MessageUtil.ShowTips("保存成功！");

                    mtGroupRate.Refresh();
                }
                else
                {
                    MessageUtil.ShowError("保存失败！");

                }

            } catch(Exception) {

                MessageUtil.ShowError("数据重复！请检查数据");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mtGroupRate.DataSource = serviceBill.getServiceRate(this.MTGName.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
