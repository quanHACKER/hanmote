﻿using Lib.Bll.ServiceBll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class ServiceSummry : Form
    {
        ServiceBill serviceBll = new ServiceBill();
        public ServiceSummry()
        {
            InitializeComponent();
        }

        private void ServiceSummry_Load(object sender, EventArgs e)
        {

          
            dataGridView1.AutoGenerateColumns = false;
            DataTable dt = serviceBll.getExServiceData();
            this.endTime.Value =this.startTime.Value.AddDays(1);
            this.supplierName.DataSource = serviceBll.getSupplierName();
            this.MateGroupName.DataSource = serviceBll.getMateGroupName(this.supplierName.Text);
            this.MaterialName.DataSource =  serviceBll.getMateName(this.supplierName.Text,this.MateGroupName.Text);

            dataGridView1.DataSource = serviceBll.getExServiceData();

        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            double sum = 0;
            DataTable dt = serviceBll.getExServiceData(this.supplierName.Text, this.MateGroupName.Text, this.MaterialName.Text,this.startTime.Value.ToString("yyyy-MM-dd"),this.endTime.Value.ToString("yyyy-MM-dd"));
            foreach (DataRow dataRow in dt.Rows) {
                sum += float.Parse(dataRow["综合"].ToString());

            }
            DataRow row = dt.NewRow();
            row["综合"] = Math.Round(sum / dt.Rows.Count,2);
            row["服务工单"] = "汇总结果";
            row["供应商名称"] = this.supplierName.Text;
            row["物料组名称"] = this.MateGroupName.Text;
            row["单位"] = "记录条数：" + dt.Rows.Count;
            row["物料名称"] = this.MaterialName.Text;
            row["供应商代码"] = this.startTime.Text + "--" + this.endTime.Text;
            dt.Rows.Add(row.ItemArray);

            dataGridView1.DataSource = dt;
            
        }

        private void supplierName_TextChanged(object sender, EventArgs e)
        {
            this.MateGroupName.DataSource = serviceBll.getMateGroupName(this.supplierName.Text);
        }

        private void MateGroupName_TextChanged(object sender, EventArgs e)
        {
            this.MaterialName.DataSource = serviceBll.getMateName(this.supplierName.Text, this.MateGroupName.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
