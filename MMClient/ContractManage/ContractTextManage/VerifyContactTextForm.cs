﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTextManage
{
    public partial class VerifyContactTextForm : DockContent
    {
        //ContractTextDAL实例
        ContractTextDAL contractTextDAL = new ContractTextDAL();
        //关于合同文本的datatable
        DataTable dtContractText = new DataTable();
        //用于缓存的DataTable(暂时未使用)
        DataTable dtBuffer = new DataTable();

        public VerifyContactTextForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 点击查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //确定选择的采购员
            string buyerID = this.cbxBuyer.SelectedItem.ToString();
            LinkedList<ContractText> contractList = new LinkedList<ContractText>();
            if (buyerID.Equals("全部人员"))
            {
                //查询全部人员
                contractList = contractTextDAL.GetContractTextByBuyerID("all", "未审核");
            }
            else { 
                //查询指定人员
                contractList = contractTextDAL.GetContractTextByBuyerID(buyerID, "未审核");
            }

            //改变DataGridview的数据显示
            dgvVerifyContractTextDisplay(contractList);
        }

        private void dgvVerifyContractTextDisplay(LinkedList<ContractText> list) {
            //先清空原来的数据显示
            dtContractText.Rows.Clear();

            if (list == null || list.Count == 0)
                return;

            //将数据库中读取的数据添加到contractTemplateDataTable中
            foreach (ContractText text in list)
            {
                DataRow dr = dtContractText.NewRow();
                
                dr["合同编号"] = text.ContractID;
                dr["合同名称"] = text.ContractName;
                dr["供应商编号"] = text.SupplierID;
                dr["采购员ID"] = text.CreaterID;
                dr["创建时间"] = text.BeginTime;
                
                dtContractText.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerifyContactTextForm_Load(object sender, EventArgs e)
        {
            //初始化 合同显示框
            InitialContractTextDataTable();
        }

        /// <summary>
        /// 初始化合同信息的DataTable
        /// </summary>
        private void InitialContractTextDataTable() {
            dtContractText.Columns.Add("合同编号", typeof(string));
            dtContractText.Columns.Add("合同名称", typeof(string));
            dtContractText.Columns.Add("供应商编号", typeof(string));
            dtContractText.Columns.Add("采购员ID", typeof(string));
            dtContractText.Columns.Add("创建时间", typeof(DateTime));

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            {
                checkBoxColumn.HeaderText = "是否选中";
                checkBoxColumn.Name = "是否选中";
                checkBoxColumn.CellTemplate = new DataGridViewCheckBoxCell();
            }
            this.dgvVerifyContractText.Columns.Insert(0, checkBoxColumn);
            DataGridViewComboBoxColumn comboBoxColumn = new DataGridViewComboBoxColumn();
            {
                comboBoxColumn.HeaderText = "审核结果";
                comboBoxColumn.Name = "审核结果";
                comboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
                comboBoxColumn.CellTemplate = new DataGridViewComboBoxCell();
                comboBoxColumn.Items.Add("不通过");
                comboBoxColumn.Items.Add("通过");
                comboBoxColumn.DefaultCellStyle.NullValue = "不通过";
            }
            this.dgvVerifyContractText.Columns.Insert(1, comboBoxColumn);

            //其余的数据绑定到DataTable上
            this.dgvVerifyContractText.DataSource = dtContractText;

            //默认执行一次查询
            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 提交审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmitVerify_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("选中合同文本确定通过审核？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            //对DataGridview中的数据进行编辑
            //这里只显示未通过审核的合同模板
            //查看选中的合同模板
            LinkedList<string> selected = new LinkedList<string>();
            for (int i = 0; i < this.dgvVerifyContractText.Rows.Count; i++)
            {
                if (this.dgvVerifyContractText.Rows[i].Cells["是否选中"].EditedFormattedValue.ToString().Equals("True"))
                {
                    string id = this.dgvVerifyContractText.Rows[i].Cells["合同编号"].Value.ToString();
                    selected.AddLast(id);
                }
            }

            if (selected.Count == 0)
            {
                MessageBox.Show("请至少选择一个合同文本", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //update选中的合同模板的状态
                int execResult = contractTextDAL.UpdateContractTextState(selected, true);
                if (execResult == 0 || execResult != selected.Count)
                {
                    MessageBox.Show("操作失败！");
                }
                else
                {
                    MessageBox.Show("操作成功！");
                }

                //执行一次查询操作，刷新当前显示
                this.btnSearch.PerformClick();
            }
        }
    }
}
