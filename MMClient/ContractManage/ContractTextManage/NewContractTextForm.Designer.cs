﻿namespace MMClient.ContractManage.ContractTextManage
{
    partial class NewContractTextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbSupplierID = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbContractTextName = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbContractTextID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSaveUpload = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUploadContractText = new System.Windows.Forms.Button();
            this.tcGenerateType = new System.Windows.Forms.TabControl();
            this.tpUploadContractText = new System.Windows.Forms.TabPage();
            this.btnUploadReset = new System.Windows.Forms.Button();
            this.tbFileAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.tpManualInput = new System.Windows.Forms.TabPage();
            this.panelInputArea = new System.Windows.Forms.Panel();
            this.panelManualHead = new System.Windows.Forms.Panel();
            this.btnClearInput = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnManualReset = new System.Windows.Forms.Button();
            this.btnSelectTemplate = new System.Windows.Forms.Button();
            this.cbxContractTemplateID = new System.Windows.Forms.ComboBox();
            this.cbxContractTemplateName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSaveContractText = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tcGenerateType.SuspendLayout();
            this.tpUploadContractText.SuspendLayout();
            this.tpManualInput.SuspendLayout();
            this.panelManualHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(962, 85);
            this.panel1.TabIndex = 6;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tbSupplierID);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.tbContractTextName);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.tbContractTextID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(962, 85);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "合同基本信息";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // tbSupplierID
            // 
            this.tbSupplierID.Location = new System.Drawing.Point(679, 34);
            this.tbSupplierID.Name = "tbSupplierID";
            this.tbSupplierID.Size = new System.Drawing.Size(171, 21);
            this.tbSupplierID.TabIndex = 11;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(599, 37);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 12);
            this.label25.TabIndex = 10;
            this.label25.Text = "供应商编号：";
            // 
            // tbContractTextName
            // 
            this.tbContractTextName.Location = new System.Drawing.Point(393, 34);
            this.tbContractTextName.Name = "tbContractTextName";
            this.tbContractTextName.Size = new System.Drawing.Size(165, 21);
            this.tbContractTextName.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(311, 37);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 12);
            this.label24.TabIndex = 8;
            this.label24.Text = "合同名称：";
            // 
            // tbContractTextID
            // 
            this.tbContractTextID.Location = new System.Drawing.Point(104, 34);
            this.tbContractTextID.Name = "tbContractTextID";
            this.tbContractTextID.Size = new System.Drawing.Size(167, 21);
            this.tbContractTextID.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "合同编号：";
            // 
            // btnSaveUpload
            // 
            this.btnSaveUpload.Location = new System.Drawing.Point(514, 54);
            this.btnSaveUpload.Name = "btnSaveUpload";
            this.btnSaveUpload.Size = new System.Drawing.Size(75, 23);
            this.btnSaveUpload.TabIndex = 5;
            this.btnSaveUpload.Text = "保存";
            this.btnSaveUpload.UseVisualStyleBackColor = true;
            this.btnSaveUpload.Click += new System.EventHandler(this.btnSaveUpload_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 59);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(173, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "(支持上传doc、docx、pdf文件)";
            // 
            // btnUploadContractText
            // 
            this.btnUploadContractText.Location = new System.Drawing.Point(364, 54);
            this.btnUploadContractText.Name = "btnUploadContractText";
            this.btnUploadContractText.Size = new System.Drawing.Size(75, 23);
            this.btnUploadContractText.TabIndex = 2;
            this.btnUploadContractText.Text = "上传";
            this.btnUploadContractText.UseVisualStyleBackColor = true;
            this.btnUploadContractText.Click += new System.EventHandler(this.btnUploadContractText_Click);
            // 
            // tcGenerateType
            // 
            this.tcGenerateType.Controls.Add(this.tpUploadContractText);
            this.tcGenerateType.Controls.Add(this.tpManualInput);
            this.tcGenerateType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGenerateType.Location = new System.Drawing.Point(0, 85);
            this.tcGenerateType.Name = "tcGenerateType";
            this.tcGenerateType.SelectedIndex = 0;
            this.tcGenerateType.Size = new System.Drawing.Size(962, 656);
            this.tcGenerateType.TabIndex = 9;
            // 
            // tpUploadContractText
            // 
            this.tpUploadContractText.Controls.Add(this.btnUploadReset);
            this.tpUploadContractText.Controls.Add(this.tbFileAddress);
            this.tpUploadContractText.Controls.Add(this.label5);
            this.tpUploadContractText.Controls.Add(this.tbFileName);
            this.tpUploadContractText.Controls.Add(this.label3);
            this.tpUploadContractText.Controls.Add(this.btnSelectFile);
            this.tpUploadContractText.Controls.Add(this.btnUploadContractText);
            this.tpUploadContractText.Controls.Add(this.label1);
            this.tpUploadContractText.Controls.Add(this.btnSaveUpload);
            this.tpUploadContractText.Location = new System.Drawing.Point(4, 22);
            this.tpUploadContractText.Name = "tpUploadContractText";
            this.tpUploadContractText.Padding = new System.Windows.Forms.Padding(3);
            this.tpUploadContractText.Size = new System.Drawing.Size(954, 630);
            this.tpUploadContractText.TabIndex = 0;
            this.tpUploadContractText.Text = "上传文本";
            this.tpUploadContractText.UseVisualStyleBackColor = true;
            // 
            // btnUploadReset
            // 
            this.btnUploadReset.Location = new System.Drawing.Point(651, 54);
            this.btnUploadReset.Name = "btnUploadReset";
            this.btnUploadReset.Size = new System.Drawing.Size(75, 23);
            this.btnUploadReset.TabIndex = 11;
            this.btnUploadReset.Text = "重置";
            this.btnUploadReset.UseVisualStyleBackColor = true;
            this.btnUploadReset.Click += new System.EventHandler(this.btnUploadReset_Click);
            // 
            // tbFileAddress
            // 
            this.tbFileAddress.Location = new System.Drawing.Point(152, 158);
            this.tbFileAddress.Name = "tbFileAddress";
            this.tbFileAddress.Size = new System.Drawing.Size(574, 21);
            this.tbFileAddress.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "文件路径:";
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(152, 109);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(574, 21);
            this.tbFileName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "文件名称:";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Location = new System.Drawing.Point(79, 54);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFile.TabIndex = 6;
            this.btnSelectFile.Text = "选择文件";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // tpManualInput
            // 
            this.tpManualInput.AutoScroll = true;
            this.tpManualInput.Controls.Add(this.panelInputArea);
            this.tpManualInput.Controls.Add(this.panelManualHead);
            this.tpManualInput.Location = new System.Drawing.Point(4, 22);
            this.tpManualInput.Name = "tpManualInput";
            this.tpManualInput.Padding = new System.Windows.Forms.Padding(3);
            this.tpManualInput.Size = new System.Drawing.Size(954, 639);
            this.tpManualInput.TabIndex = 1;
            this.tpManualInput.Text = "手动输入";
            this.tpManualInput.UseVisualStyleBackColor = true;
            // 
            // panelInputArea
            // 
            this.panelInputArea.AutoScroll = true;
            this.panelInputArea.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelInputArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInputArea.Location = new System.Drawing.Point(3, 128);
            this.panelInputArea.Name = "panelInputArea";
            this.panelInputArea.Size = new System.Drawing.Size(948, 508);
            this.panelInputArea.TabIndex = 87;
            // 
            // panelManualHead
            // 
            this.panelManualHead.Controls.Add(this.btnClearInput);
            this.panelManualHead.Controls.Add(this.label2);
            this.panelManualHead.Controls.Add(this.btnManualReset);
            this.panelManualHead.Controls.Add(this.btnSelectTemplate);
            this.panelManualHead.Controls.Add(this.cbxContractTemplateID);
            this.panelManualHead.Controls.Add(this.cbxContractTemplateName);
            this.panelManualHead.Controls.Add(this.label6);
            this.panelManualHead.Controls.Add(this.btnSaveContractText);
            this.panelManualHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelManualHead.Location = new System.Drawing.Point(3, 3);
            this.panelManualHead.Name = "panelManualHead";
            this.panelManualHead.Size = new System.Drawing.Size(948, 125);
            this.panelManualHead.TabIndex = 86;
            // 
            // btnClearInput
            // 
            this.btnClearInput.Location = new System.Drawing.Point(475, 76);
            this.btnClearInput.Name = "btnClearInput";
            this.btnClearInput.Size = new System.Drawing.Size(75, 23);
            this.btnClearInput.TabIndex = 85;
            this.btnClearInput.Text = "清空";
            this.btnClearInput.UseVisualStyleBackColor = true;
            this.btnClearInput.Click += new System.EventHandler(this.btnClearInput_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 77;
            this.label2.Text = "合同模板编号：";
            // 
            // btnManualReset
            // 
            this.btnManualReset.Location = new System.Drawing.Point(725, 76);
            this.btnManualReset.Name = "btnManualReset";
            this.btnManualReset.Size = new System.Drawing.Size(75, 23);
            this.btnManualReset.TabIndex = 84;
            this.btnManualReset.Text = "重置";
            this.btnManualReset.UseVisualStyleBackColor = true;
            this.btnManualReset.Click += new System.EventHandler(this.btnManualReset_Click);
            // 
            // btnSelectTemplate
            // 
            this.btnSelectTemplate.Location = new System.Drawing.Point(65, 76);
            this.btnSelectTemplate.Name = "btnSelectTemplate";
            this.btnSelectTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnSelectTemplate.TabIndex = 79;
            this.btnSelectTemplate.Text = "使用";
            this.btnSelectTemplate.UseVisualStyleBackColor = true;
            this.btnSelectTemplate.Click += new System.EventHandler(this.btnSelectTemplate_Click);
            // 
            // cbxContractTemplateID
            // 
            this.cbxContractTemplateID.FormattingEnabled = true;
            this.cbxContractTemplateID.Location = new System.Drawing.Point(132, 34);
            this.cbxContractTemplateID.Name = "cbxContractTemplateID";
            this.cbxContractTemplateID.Size = new System.Drawing.Size(255, 20);
            this.cbxContractTemplateID.TabIndex = 78;
            // 
            // cbxContractTemplateName
            // 
            this.cbxContractTemplateName.FormattingEnabled = true;
            this.cbxContractTemplateName.Location = new System.Drawing.Point(555, 34);
            this.cbxContractTemplateName.Name = "cbxContractTemplateName";
            this.cbxContractTemplateName.Size = new System.Drawing.Size(333, 20);
            this.cbxContractTemplateName.TabIndex = 82;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(440, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 81;
            this.label6.Text = "合同模板名称：";
            // 
            // btnSaveContractText
            // 
            this.btnSaveContractText.Location = new System.Drawing.Point(273, 76);
            this.btnSaveContractText.Name = "btnSaveContractText";
            this.btnSaveContractText.Size = new System.Drawing.Size(75, 23);
            this.btnSaveContractText.TabIndex = 80;
            this.btnSaveContractText.Text = "保存";
            this.btnSaveContractText.UseVisualStyleBackColor = true;
            this.btnSaveContractText.Click += new System.EventHandler(this.btnSaveContractText_Click);
            // 
            // NewContractTextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(962, 741);
            this.Controls.Add(this.tcGenerateType);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "NewContractTextForm";
            this.Text = "新建合同";
            this.Load += new System.EventHandler(this.NewContractTextForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcGenerateType.ResumeLayout(false);
            this.tpUploadContractText.ResumeLayout(false);
            this.tpUploadContractText.PerformLayout();
            this.tpManualInput.ResumeLayout(false);
            this.panelManualHead.ResumeLayout(false);
            this.panelManualHead.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUploadContractText;
        private System.Windows.Forms.Button btnSaveUpload;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbContractTextID;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbContractTextName;
        private System.Windows.Forms.TextBox tbSupplierID;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabControl tcGenerateType;
        private System.Windows.Forms.TabPage tpUploadContractText;
        private System.Windows.Forms.TabPage tpManualInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectTemplate;
        private System.Windows.Forms.Button btnSaveContractText;
        private System.Windows.Forms.ComboBox cbxContractTemplateID;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFileAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxContractTemplateName;
        private System.Windows.Forms.Button btnManualReset;
        private System.Windows.Forms.Panel panelInputArea;
        private System.Windows.Forms.Panel panelManualHead;
        private System.Windows.Forms.Button btnClearInput;
        private System.Windows.Forms.Button btnUploadReset;
    }
}