﻿namespace MMClient.ContractManage.ContractTextManage
{
    partial class ContractTextManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractTextManageForm));
            this.dgvDisplayContractText = new System.Windows.Forms.DataGridView();
            this.groupBoxContactText = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxContractTextType = new System.Windows.Forms.ComboBox();
            this.tbSupplierID = new System.Windows.Forms.TextBox();
            this.tbContractTextName = new System.Windows.Forms.TextBox();
            this.tbContractTextID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripContactTextManege = new System.Windows.Forms.ToolStrip();
            this.TsbNewContract = new System.Windows.Forms.ToolStripButton();
            this.tsbPreview = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.TsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayContractText)).BeginInit();
            this.groupBoxContactText.SuspendLayout();
            this.toolStripContactTextManege.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDisplayContractText
            // 
            this.dgvDisplayContractText.AllowUserToAddRows = false;
            this.dgvDisplayContractText.AllowUserToDeleteRows = false;
            this.dgvDisplayContractText.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.dgvDisplayContractText.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDisplayContractText.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDisplayContractText.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDisplayContractText.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDisplayContractText.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDisplayContractText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDisplayContractText.Location = new System.Drawing.Point(0, 179);
            this.dgvDisplayContractText.MultiSelect = false;
            this.dgvDisplayContractText.Name = "dgvDisplayContractText";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDisplayContractText.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDisplayContractText.RowHeadersVisible = false;
            this.dgvDisplayContractText.RowTemplate.Height = 23;
            this.dgvDisplayContractText.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDisplayContractText.Size = new System.Drawing.Size(813, 475);
            this.dgvDisplayContractText.TabIndex = 2;
            // 
            // groupBoxContactText
            // 
            this.groupBoxContactText.Controls.Add(this.btnSearch);
            this.groupBoxContactText.Controls.Add(this.dtpEndTime);
            this.groupBoxContactText.Controls.Add(this.dtpBeginTime);
            this.groupBoxContactText.Controls.Add(this.label6);
            this.groupBoxContactText.Controls.Add(this.cbxContractTextType);
            this.groupBoxContactText.Controls.Add(this.tbSupplierID);
            this.groupBoxContactText.Controls.Add(this.tbContractTextName);
            this.groupBoxContactText.Controls.Add(this.tbContractTextID);
            this.groupBoxContactText.Controls.Add(this.label5);
            this.groupBoxContactText.Controls.Add(this.label4);
            this.groupBoxContactText.Controls.Add(this.label3);
            this.groupBoxContactText.Controls.Add(this.label2);
            this.groupBoxContactText.Controls.Add(this.label1);
            this.groupBoxContactText.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxContactText.Location = new System.Drawing.Point(0, 44);
            this.groupBoxContactText.Name = "groupBoxContactText";
            this.groupBoxContactText.Size = new System.Drawing.Size(813, 135);
            this.groupBoxContactText.TabIndex = 1;
            this.groupBoxContactText.TabStop = false;
            this.groupBoxContactText.Text = "合同信息";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(693, 83);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 13;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.CustomFormat = "yyyy-MM-dd hh:MM:ss";
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new System.Drawing.Point(485, 82);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(156, 21);
            this.dtpEndTime.TabIndex = 12;
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.CustomFormat = "yyyy-MM-dd hh:MM:ss";
            this.dtpBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginTime.Location = new System.Drawing.Point(121, 82);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(156, 21);
            this.dtpBeginTime.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(375, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "创建结束时间：";
            // 
            // cbxContractTextType
            // 
            this.cbxContractTextType.FormattingEnabled = true;
            this.cbxContractTextType.Items.AddRange(new object[] {
            "全部类型",
            "现货合同",
            "定期合同",
            "定额合同",
            "无定额合同"});
            this.cbxContractTextType.Location = new System.Drawing.Point(668, 30);
            this.cbxContractTextType.Name = "cbxContractTextType";
            this.cbxContractTextType.Size = new System.Drawing.Size(100, 20);
            this.cbxContractTextType.TabIndex = 9;
            // 
            // tbSupplierID
            // 
            this.tbSupplierID.Location = new System.Drawing.Point(463, 30);
            this.tbSupplierID.Name = "tbSupplierID";
            this.tbSupplierID.Size = new System.Drawing.Size(100, 21);
            this.tbSupplierID.TabIndex = 8;
            // 
            // tbContractTextName
            // 
            this.tbContractTextName.Location = new System.Drawing.Point(274, 33);
            this.tbContractTextName.Name = "tbContractTextName";
            this.tbContractTextName.Size = new System.Drawing.Size(100, 21);
            this.tbContractTextName.TabIndex = 7;
            // 
            // tbContractTextID
            // 
            this.tbContractTextID.Location = new System.Drawing.Point(84, 33);
            this.tbContractTextID.Name = "tbContractTextID";
            this.tbContractTextID.Size = new System.Drawing.Size(100, 21);
            this.tbContractTextID.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(380, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "供应商编号：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(585, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "合同类型：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "合同名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "创建起止时间：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 33);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "合同编号：";
            // 
            // toolStripContactTextManege
            // 
            this.toolStripContactTextManege.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripContactTextManege.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TsbNewContract,
            this.tsbPreview,
            this.tsbDelete,
            this.TsbRefresh,
            this.toolStripSeparator1,
            this.toolStripButton8});
            this.toolStripContactTextManege.Location = new System.Drawing.Point(0, 0);
            this.toolStripContactTextManege.Name = "toolStripContactTextManege";
            this.toolStripContactTextManege.Size = new System.Drawing.Size(813, 44);
            this.toolStripContactTextManege.TabIndex = 0;
            this.toolStripContactTextManege.Text = "toolStrip1";
            // 
            // TsbNewContract
            // 
            this.TsbNewContract.Image = ((System.Drawing.Image)(resources.GetObject("TsbNewContract.Image")));
            this.TsbNewContract.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TsbNewContract.Name = "TsbNewContract";
            this.TsbNewContract.Size = new System.Drawing.Size(36, 41);
            this.TsbNewContract.Text = "新建";
            this.TsbNewContract.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TsbNewContract.Click += new System.EventHandler(this.TsbNewContract_Click);
            // 
            // tsbPreview
            // 
            this.tsbPreview.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreview.Image")));
            this.tsbPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreview.Name = "tsbPreview";
            this.tsbPreview.Size = new System.Drawing.Size(36, 41);
            this.tsbPreview.Text = "预览";
            this.tsbPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPreview.Click += new System.EventHandler(this.tsbPreview_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsbDelete.Image")));
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(36, 41);
            this.tsbDelete.Text = "删除";
            this.tsbDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // TsbRefresh
            // 
            this.TsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("TsbRefresh.Image")));
            this.TsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TsbRefresh.Name = "TsbRefresh";
            this.TsbRefresh.Size = new System.Drawing.Size(36, 41);
            this.TsbRefresh.Text = "刷新";
            this.TsbRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TsbRefresh.Click += new System.EventHandler(this.TsbRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 44);
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(60, 41);
            this.toolStripButton8.Text = "提交审核";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // ContractTextManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 654);
            this.Controls.Add(this.dgvDisplayContractText);
            this.Controls.Add(this.groupBoxContactText);
            this.Controls.Add(this.toolStripContactTextManege);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ContractTextManageForm";
            this.Text = "合同文本管理";
            this.Load += new System.EventHandler(this.ContactTextManageForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayContractText)).EndInit();
            this.groupBoxContactText.ResumeLayout(false);
            this.groupBoxContactText.PerformLayout();
            this.toolStripContactTextManege.ResumeLayout(false);
            this.toolStripContactTextManege.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripContactTextManege;
        private System.Windows.Forms.ToolStripButton TsbNewContract;
        private System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripButton TsbRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBoxContactText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxContractTextType;
        private System.Windows.Forms.TextBox tbSupplierID;
        private System.Windows.Forms.TextBox tbContractTextName;
        private System.Windows.Forms.TextBox tbContractTextID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.DataGridView dgvDisplayContractText;
        private System.Windows.Forms.ToolStripButton tsbPreview;
        private System.Windows.Forms.ToolStripButton toolStripButton8;

    }
}