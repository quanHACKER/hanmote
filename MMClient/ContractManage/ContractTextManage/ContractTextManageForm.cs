﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTextManage
{
    public partial class ContractTextManageForm : DockContent
    {
        //保存所有的合同信息
        DataTable dtAllContractTextInfo = new DataTable();
        //保存用于显示的合同的信息
        DataTable dtDisplayContractTextInfo = new DataTable();

        DataTable dt = new DataTable();

        //数据库查询
        ContractTextDAL contractTextDAL = new ContractTextDAL();

        public ContractTextManageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 点击 搜索 按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //从所有的合同信息中搜索满足当前条件的合同信息
            //先看开始时间和截止时间
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;

            if (beginTime > endTime)
            {
                MessageBox.Show("请正确选择起止日期", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string contractTextID = this.tbContractTextID.Text.Trim();
            string contractTextName = this.tbContractTextName.Text.Trim();
            string supplierID = this.tbSupplierID.Text.Trim();
            string contractTextType = this.cbxContractTextType.SelectedItem.ToString();

            //写SQL语句
            StringBuilder strBui = new StringBuilder("select * from dbo.Contract where Create_Time >= '")
                .Append(beginTime.ToString("yyyy-MM-dd hh:mm:ss.fff"))
                .Append("' and Create_Time <= '")
                .Append(endTime.ToString("yyyy-MM-dd hh:mm:ss.fff"))
                .Append("' ");

            //合同模板ID不为空
            if (!contractTextID.Equals(""))
            {
                strBui.Append(" and Contract_ID = '")
                    .Append(contractTextID)
                    .Append("' ");
            }
            //合同Name不为空
            if (!contractTextName.Equals(""))
            {
                strBui.Append(" and Contract_Name = '")
                    .Append(contractTextName)
                    .Append("'");
            }
            //供应商ID不为空
            if (!supplierID.Equals(""))
            {
                strBui.Append(" and Supplier_ID = '")
                    .Append(supplierID)
                    .Append("'");
            }
            //合同类型不为 "全部类型"
            if (!contractTextType.Equals("全部类型"))
            {
                strBui.Append(" and Contract_Type = '")
                    .Append(contractTextType)
                    .Append("'");
            }

            //对结果排序
            strBui.Append(" order by Contract_ID asc");

            //查询
            LinkedList<ContractText> resultList = new LinkedList<ContractText>();
            resultList = contractTextDAL.getContractTextBySQL(strBui.ToString());

            //显示搜索结果
            displaySearchResultOnDgv(resultList);
        }

        private void displaySearchResultOnDgv(LinkedList<ContractText> resultList) {
            //清空之前的数据
            dtDisplayContractTextInfo.Rows.Clear();
            if (resultList == null || resultList.Count == 0)
            {
                return;
            }

            //将数据库中读取的数据添加到dtDisplayContractTextInfo中 
            foreach (ContractText text in resultList)
            {
                DataRow dr = dtDisplayContractTextInfo.NewRow();

                dr["合同编号"] = text.ContractID;
                dr["合同名称"] = text.ContractName;
                dr["供应商编号"] = text.SupplierID;
                dr["文本状态"] = text.Status;
                dr["创建时间"] = text.CreateTime;
                dr["合同生成方式"] = text.CreateMode;
                dr["合同类型"] = text.ContractType;
                dr["创建人"] = text.CreaterID;
                dr["开始时间"] = text.BeginTime;
                dr["截止时间"] = text.EndTime;

                dtDisplayContractTextInfo.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 点击新建合同文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsbNewContract_Click(object sender, EventArgs e)
        {
            NewContractTextForm newContractText = new NewContractTextForm();
            newContractText.ShowDialog();
        }

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContactTextManageForm_Load(object sender, EventArgs e)
        {
            //初始化DataTable的结构
            //initialDataTableStructure(dtAllContractTextInfo);
            initialDataTableStructure(dtDisplayContractTextInfo);

            //合同类型的ComboBox默认selectedItem为0
            this.cbxContractTextType.SelectedIndex = 0;

            //从服务器读取所有的合同文本信息到dtAllContractTextInfo
            //getAllContractTextInfo(dtAllContractTextInfo);

            //绑定 dtDisplayContractTextInfo 的数据源
            this.dgvDisplayContractText.DataSource = dtDisplayContractTextInfo;
        }

        /// <summary>
        /// 点击刷新按钮，从服务器读取新的数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsbRefresh_Click(object sender, EventArgs e)
        {
            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 初始化DataTable的结构
        /// </summary>
        /// <param name="dtContractText"></param>
        private void initialDataTableStructure(DataTable dtContractText) {
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            {
                checkBoxColumn.HeaderText = "是否选中";
                checkBoxColumn.Name = "是否选中";
                checkBoxColumn.CellTemplate = new DataGridViewCheckBoxCell();

            }

            this.dgvDisplayContractText.Columns.Insert(0, checkBoxColumn);
            //这里的结构包含了合同文本的所有信息
            DataColumn dc;
            dc = new DataColumn("合同编号");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("合同编号", typeof(string));
            dc = new DataColumn("合同名称");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("合同名称", typeof(string));
            dc = new DataColumn("供应商编号");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("供应商编号", typeof(string));
            dc = new DataColumn("合同类型");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("合同类型", typeof(string));
            dc = new DataColumn("合同生成方式");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("合同生成方式", typeof(string));
            dc = new DataColumn("文本状态");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("文本状态", typeof(string));
            dc = new DataColumn("创建时间");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("创建时间", typeof(string));
            dc = new DataColumn("创建人");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("采购员ID", typeof(string));
            dc = new DataColumn("开始时间");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("开始时间", typeof(string));
            dc = new DataColumn("截止时间");
            dc.ReadOnly = true;
            dtContractText.Columns.Add(dc);
            //dtContractText.Columns.Add("结束时间", typeof(string));
        }

        /// <summary>
        /// 获取所有的合同信息
        /// </summary>
        /// <param name="datetable"></param>
        private void getAllContractTextInfo(DataTable datetable) { 
            
        }

        /// <summary>
        /// 删除选定的合同文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            //检测是否有合同模板被选中
            var result = MessageBox.Show("确定删除选中的合同文本？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            //对DataGridview中的数据进行编辑
            //这里只显示未通过审核的合同模板
            //查看选中的合同模板
            LinkedList<string> selected = new LinkedList<string>();
            for (int i = 0; i < this.dgvDisplayContractText.Rows.Count; i++)
            {
                if (this.dgvDisplayContractText.Rows[i].Cells["是否选中"].EditedFormattedValue.ToString().Equals("True"))
                {
                    string id = this.dgvDisplayContractText.Rows[i].Cells["合同编号"].Value.ToString();
                    selected.AddLast(id);
                }
            }

            if (selected.Count == 0)
            {
                MessageBox.Show("请至少选择一个合同文本", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //删除中的合同模板
                int execResult = contractTextDAL.DeleteContractTemplate(selected);
                if (execResult == 0 || execResult != selected.Count)
                {
                    MessageBox.Show("操作失败！");
                }
                else
                {
                    MessageBox.Show("操作成功！");
                }

                //执行一次查询操作，刷新当前显示
                this.btnSearch.PerformClick();
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("请对文本进行人工审核！");
        }

        private void tsbPreview_Click(object sender, EventArgs e)
        {
            MessageBox.Show("当前版本不支持在线预览！");
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            
        }
    }
}
