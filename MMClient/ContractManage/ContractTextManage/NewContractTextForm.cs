﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;
using System.IO;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;
using Microsoft.Office.Interop.Word;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.ContractTextManage
{
    public partial class NewContractTextForm : DockContent
    {
        //用来存储合同信息
        private class SectionEntry {
            public Contract_Template_Section section = new Contract_Template_Section();
            public LinkedList<TermEntry> termList = new LinkedList<TermEntry>();
        }
        private class TermEntry {
            public Contract_Template_Term term = new Contract_Template_Term();
            public Contract_Template_Term_Sample TermSample = new Contract_Template_Term_Sample();
            public LinkedList<VariableEntry> variableList = new LinkedList<VariableEntry>();
        }
        private class VariableEntry{
            public Contract_Template_Variable variable = new Contract_Template_Variable();
            public Control control;
        }
        //用来记录合同条款的信息
        private class TermInfo {
            public string TermFileName;
            //默认为0
            public int order = 0;
            public string title;
        }

        //记录所有的Label控件
        private LinkedList<Label> labelControlsList = new LinkedList<Label>();
        //记录所有的TextBox控件
        private LinkedList<TextBox> textBoxControlsList = new LinkedList<TextBox>();
        //记录所有的DateTimePicker控件
        private LinkedList<DateTimePicker> dateTimePickerList = new LinkedList<DateTimePicker>();
        
        //是否已经生成控件
        private bool alreadyGenerate = false;
        //手动上传的文件是否已经上传成功
        private bool uploadSuccess = false;

        //合同模板数据库工具
        ContractTemplateDAL contractTemplateDALTool = new ContractTemplateDAL();
        ContractTemplateSectionDAL contractTemplateSectionDALTool = new ContractTemplateSectionDAL();
        ContractTemplateTermDAL contractTemplateTermDALTool = new ContractTemplateTermDAL();
        ContractTemplateVariableDAL contractTemplateVariableDALTool = new ContractTemplateVariableDAL();
        ContractTemplateTermSampleDAL contractTemplateTermSampleDALTool = new ContractTemplateTermSampleDAL();
        ContractTextDAL contractTextDALTool = new ContractTextDAL();

        //ftp工具
        FTPTool ftp = FTPTool.getInstance();
        //默认的本地存放位置
        string defaultDic = @"D:\ContractFiles\";
        //pp存放term_sample在服务器的位置和本地存放的位置,termFileList存放word在本地的位置,termInfoList存放条款信息
        LinkedList<string> termFileList = new LinkedList<string>();
        LinkedList<TermInfo> termInfoList = new LinkedList<TermInfo>();
        Dictionary<string, string> pp = new Dictionary<string, string>();

        //保存所有的章节、条款
        Dictionary<Contract_Template_Section, LinkedList<Contract_Template_Term>> contentDic = new Dictionary<Contract_Template_Section, LinkedList<Contract_Template_Term>>();
        //选定的合同模板的ID
        string currentContractTemplateID = "";
        //记录所有的控件
        LinkedList<SectionEntry> record = new LinkedList<SectionEntry>();

        //记录添加控件的起始位置
        private System.Drawing.Point startPosition;
        //控件的参数
        private System.Drawing.Point TEXTBOX_SIZE = new System.Drawing.Point(167, 21);
        private const int SPACE_SIZE = 30;
        private const int CONTROL_NUM_ONELINE = 2;

        public NewContractTextForm()
        {
            InitializeComponent();
            //默认合同文本ID
            this.tbContractTextID.Text = System.DateTime.Now.ToString("yyyyMMddHHmmss")
                + "2016001";
        }

        /// <summary>
        /// 窗体加载时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewContractTextForm_Load(object sender, EventArgs e)
        {
            //绑定 手动输入tabpage的 合同模板编号cbx和合同模板名称cbx ，实现级联
            initialTabpageManualInputComboBox();
            //确定动态生成控件的起始位置
            //startPosition.X = this.panelInputArea.Location.X + 50;
            //startPosition.Y = this.panelInputArea.Location.Y;
            //关于panel的startPosition有问题
            startPosition.X = 0;
            startPosition.Y = 30;
        }

        /// <summary>
        /// 绑定 手动输入tabpage的 合同模板编号cbx和合同模板名称cbx 的数据源，实现级联
        /// </summary>
        private void initialTabpageManualInputComboBox() {
            System.Data.DataTable dataSource = contractTemplateDALTool.getContractTemplateDataTableBySQL("select * from Contract_Template");
            this.cbxContractTemplateID.DataSource = dataSource;
            this.cbxContractTemplateID.DisplayMember = "ID";
            this.cbxContractTemplateID.ValueMember = "ID";

            this.cbxContractTemplateName.DataSource = dataSource;
            this.cbxContractTemplateName.DisplayMember = "Name";
            this.cbxContractTemplateName.ValueMember = "Name";
        }

        /// <summary>
        /// 上传文本-点击上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUploadContractText_Click(object sender, EventArgs e)
        {
            //上传至服务器
            //如果上传成功，uploadSuccess标记为true
            uploadSuccess = true;
        }

        /// <summary>
        /// 选择上传文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "contract files(*.doc;*.docx;*.pdf)|*.doc;*.docx;*.pdf";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = openFileDialog.SafeFileName;
                this.tbFileAddress.Text = openFileDialog.FileName;
            }
        }

        /// <summary>
        /// 点击上传文本的重置按钮，清空当前所有信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUploadReset_Click(object sender, EventArgs e)
        {
            this.uploadSuccess = false;
            this.tbFileAddress.Text = "";
            this.tbFileName.Text = "";
        }

        /// <summary>
        /// 上传文本-点击保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveUpload_Click(object sender, EventArgs e)
        {
            if (!this.uploadSuccess) {
                MessageBox.Show("请先上传文件");
                return;
            }
            if (this.tbContractTextID.Text.Trim().Equals("") ||
               this.tbContractTextName.Text.Trim().Equals("") ||
               this.tbSupplierID.Text.Trim().Equals("")) {
                   MessageBox.Show("信息填写不完整");
                   return;
            }

            ContractText uploadContractText = new ContractText();
            uploadContractText.ContractID = this.tbContractTextID.Text;
            uploadContractText.ContractName = this.tbContractTextName.Text;
            uploadContractText.SupplierID = this.tbSupplierID.Text;
            uploadContractText.CreateMode = "直接上传";
            uploadContractText.CreateTime = System.DateTime.Now;
            uploadContractText.BeginTime = System.DateTime.Now;
            uploadContractText.EndTime = System.DateTime.Now;
            uploadContractText.FileLocation = this.tbFileAddress.Text;
            uploadContractText.Status = "未审核";
            uploadContractText.CreaterID = "2015001";
            uploadContractText.ContractType = "定额合同";

            if (contractTextDALTool.addContractText(uploadContractText) > 0)
            {
                MessageBox.Show("保存成功!");
            }
            else
            {
                MessageBox.Show("保存失败!");
            }
        }

        /// <summary>
        /// 手动输入-使用对应的合同模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            //查看是否已经生成过
            if (alreadyGenerate) {
                MessageBox.Show("已经使用");
                return;
            }

            //分析选择的合同模板
            //这里有疑问，下面这句为什么不能执行
            //currentContractTemplateID = this.cbxContractTemplateID.SelectedItem.ToString();
            currentContractTemplateID = this.cbxContractTemplateID.Text;
            //确定该模板所有的章节
            LinkedList<Contract_Template_Section> allSection = contractTemplateSectionDALTool.getContractTemplateSectionByContractTemplateID(currentContractTemplateID);

            //起点位置
            System.Drawing.Point currentPosition = startPosition;
            foreach (Contract_Template_Section section in allSection) {
                //针对每一个Section，选出其下的Term
                SectionEntry sectionEntry = new SectionEntry();
                sectionEntry.section = section;

                string sectionID = section.ID;
                LinkedList<Contract_Template_Term> termList = contractTemplateTermDALTool.getContractTemplateTermByContractTemplateID(sectionID);

                # region 添加章节控件

                currentPosition.X = startPosition.X + 400;
                addSectionOnPanel(this.panelInputArea, currentPosition, section.Name);
                currentPosition.Y += 50;
                
                # endregion

                foreach (Contract_Template_Term term in termList) { 
                    //每一个term
                    TermEntry termEntry = new TermEntry();
                    termEntry.term = term;
                    //有问题
                    termEntry.TermSample = contractTemplateTermSampleDALTool.getContractTemplateTermSampleByTermSampleName(term.Term_Sample_ID);
                    LinkedList<Contract_Template_Variable> allVariableList =
                        contractTemplateVariableDALTool.getContractTemplateVariableByContractTemplateTermSampleID
                        (term.Term_Sample_ID);

                    # region 添加条款控件
                    currentPosition.X = startPosition.X + 400;
                    //在输入页面显示条款说明
                    addTermOnPanel(this.panelInputArea, currentPosition, term.Term_Sample_ID);
                    currentPosition.X = startPosition.X + 50;
                    currentPosition.Y += 40;
                    # endregion

                    int numOfOneLine = 0;
                    foreach (Contract_Template_Variable variable in allVariableList) { 
                        //每一个变量
                        VariableEntry variableEntry = new VariableEntry();
                        variableEntry.variable = variable;
                        
                        # region 添加变量控件
                        //在输入页面显示所有变量生成的控件
                        //Label
                        addLabelPanel(this.panelInputArea, currentPosition, variable.Name);
                        currentPosition.X += (100 + 20);

                        //TextBox or DateTimePicker
                        Control control = addTextBoxOrDateTimePickerControl(this.panelInputArea, currentPosition, variable.Name, variable.Type);

                        if (numOfOneLine >= CONTROL_NUM_ONELINE)
                        {
                            currentPosition.X = startPosition.X + 50;
                            currentPosition.Y += 30;
                            numOfOneLine = 0;
                        }
                        else
                        {
                            currentPosition.X += (TEXTBOX_SIZE.X + SPACE_SIZE);
                            numOfOneLine++;
                        }
                        # endregion

                        variableEntry.control = control;
                        termEntry.variableList.AddLast(variableEntry);
                    }

                    currentPosition.Y += 40;

                    sectionEntry.termList.AddLast(termEntry);
                }
                record.AddLast(sectionEntry);
            }

            //设置已经生成
            alreadyGenerate = true;
        }

        /// <summary>
        /// 向GroupBox中添加章节标签
        /// </summary>
        /// <param name="groupBox"></param>
        /// <param name="position"></param>
        /// <param name="key"></param>
        private void addSectionOnPanel(Panel panel, System.Drawing.Point position, string key)
        {
            Label label = new Label();
            label.Text = key;
            position.Y += 1;
            label.Location = position;
            label.Name = key + "_Section";
            label.AutoSize = true;
            label.TextAlign = ContentAlignment.MiddleRight;
            //设置章节的字体, 大小20， 加粗
            label.Font = new System.Drawing.Font(this.Font.FontFamily, 20, FontStyle.Bold);

            labelControlsList.AddLast(label);
            panel.Controls.Add(label);
        }

        /// <summary>
        /// 向GroupBox中添加条款标签
        /// </summary>
        /// <param name="groupBox"></param>
        /// <param name="position"></param>
        /// <param name="key"></param>
        private void addTermOnPanel(Panel panel, System.Drawing.Point position, string key)
        {
            Label label = new Label();
            label.Text = key;
            position.Y += 1;
            label.Location = position;
            label.Name = key + "_Term";
            label.AutoSize = true;
            label.TextAlign = ContentAlignment.MiddleRight;
            //设置条款的字体, 大小15， 加粗
            label.Font = new System.Drawing.Font(this.Font.FontFamily, 15, FontStyle.Bold);

            labelControlsList.AddLast(label);
            panel.Controls.Add(label);
        }

        /// <summary>
        /// 向GroupBox中动态添加Label
        /// </summary>
        /// <param name="key"></param>
        /// <param name="groupBox"></param>
        /// <param name="position"></param>
        private void addLabelPanel(Panel panel, System.Drawing.Point position, string key)
        {
            Label label = new Label();
            label.Text = key + ":";
            position.Y += 1;
            label.Location = position;
            label.Name = key + "_lbl";
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleRight;

            labelControlsList.AddLast(label);
            panel.Controls.Add(label);
        }

        /// <summary>
        /// 向GroupBox中动态添加TextBox或者DateTimePicker
        /// </summary>
        /// <param name="key"></param>
        /// <param name="groupBox"></param>
        /// <param name="position"></param>
        private Control addTextBoxOrDateTimePickerControl(Panel panel, System.Drawing.Point position, string key, string type)
        {
            if (type.Equals("时间"))
            {
                DateTimePicker datetimePicker = new DateTimePicker();
                datetimePicker.Location = position;
                datetimePicker.Size = new System.Drawing.Size(TEXTBOX_SIZE.X, TEXTBOX_SIZE.Y);
                datetimePicker.Name = key + "_dpk";

                dateTimePickerList.AddLast(datetimePicker);
                panel.Controls.Add(datetimePicker);

                return datetimePicker;
            }
            else {
                TextBox textBox = new TextBox();
                textBox.Location = position;
                textBox.Size = new System.Drawing.Size(TEXTBOX_SIZE.X, TEXTBOX_SIZE.Y);
                textBox.Name = key + "_tb";

                textBoxControlsList.AddLast(textBox);
                panel.Controls.Add(textBox);

                return textBox;
            }
        }

        /// <summary>
        /// 清空所有输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnManualReset_Click(object sender, EventArgs e)
        {
            //清空panel上面的所有控件，以及当前读取到的所有信息
            //清空标识
            alreadyGenerate = false;
            foreach (Label label in labelControlsList) {
                if (this.panelInputArea.Contains(label)) {
                    this.panelInputArea.Controls.Remove(label);
                }
            }
            foreach (TextBox textBox in textBoxControlsList) {
                if (this.panelInputArea.Contains(textBox))
                {
                    this.panelInputArea.Controls.Remove(textBox);
                }
            }
            foreach (DateTimePicker dateTimePicker in dateTimePickerList) {
                if (this.panelInputArea.Controls.Contains(dateTimePicker)) {
                    this.panelInputArea.Controls.Remove(dateTimePicker);
                }
            }

            labelControlsList.Clear();
            textBoxControlsList.Clear();
            dateTimePickerList.Clear();
        }

        /// <summary>
        /// 清空输入的数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearInput_Click(object sender, EventArgs e)
        {
            foreach (TextBox textBox in textBoxControlsList) {
                textBox.Text = "";
            }
        }

        /// <summary>
        /// 保存至新的合同文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveContractText_Click(object sender, EventArgs e)
        {
            string contractID = this.tbContractTextID.Text.Trim();
            string contractName = this.tbContractTextName.Text.Trim();
            string supplierID = this.tbSupplierID.Text.Trim();

            //合同基本信息填写完整
            if (contractID.Equals("") ||
               contractName.Equals("") ||
               supplierID.Equals(""))
            {
                   MessageBox.Show("信息填写不完整");
                   return;
            }

            //检查输入的合法性
            bool isLegal = true;
            foreach (TextBox textBox in textBoxControlsList) {
                if (textBox.Text.Trim().Equals("")) {
                    isLegal = false;
                    break;
                }
            }
            if (!isLegal) {
                var result = MessageBox.Show("存在未输入数据,是否继续！", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                    return;
            }

            //SaveFileDialog saveFile = new SaveFileDialog();
            //saveFile.Title = "保存合同文本";
            //saveFile.Filter = "Doc files|.*doc";
            //saveFile.DefaultExt = "doc";
            //if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.Cancel
            //    || saveFile.FileName.Equals("")) {
            //    return;
            //}

            //string fullFileName = saveFile.FileName;
            //string fileName = Path.GetFileNameWithoutExtension(fullFileName);
            //string fileExt = Path.GetExtension(fullFileName);
            //if (!fileExt.Equals(".doc")) {
            //    MessageBox.Show("文件类型不对！");
            //    return;
            //}

            //默认保存路径 D:\\Contract\\
            string path = defaultDic;
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
            }

            //先把每一份的word下载下来，然后替换
            //名称序号
            int orderOfTerm = 0;
            //记录条款的序号
            bool isMainText = false;
            int termOrder = 0;


            //最后生成的文本
            string contractFile = defaultDic + contractID + ".docx";
            #region 下载模板文件
            //章节
            foreach (SectionEntry sectionEntry in record) {
                if (!isMainText)
                {
                    isMainText = true;
                }
                else {
                    termOrder++;
                }

                //条款
                foreach (TermEntry termEntry in sectionEntry.termList) {
                    string srcFileName = termEntry.TermSample.FileName;
                    int index = srcFileName.IndexOf(@"/");
                    string remoteFile = srcFileName.Substring(index + 1);
                    int lastIndex = remoteFile.LastIndexOf(@"/");
                    string remoteFileName = remoteFile.Substring(lastIndex + 1);
                    orderOfTerm++;
                    Object localFileName = defaultDic + remoteFileName;

                    if (ftp.checkFileExist(remoteFile))
                    {
                        //看要存储的文件是否已存在
                        if (File.Exists(localFileName.ToString())) {
                            File.Delete(localFileName.ToString());
                        }
                    
                        pp.Add(remoteFile,localFileName.ToString());
                        //记录条款信息
                        TermInfo termInfo = new TermInfo();
                        termInfo.TermFileName = localFileName.ToString();
                        termInfo.title = termEntry.term.Detail;
                        termInfo.order = termOrder == 0 ? 0 : termOrder++;
                        termInfoList.AddLast(termInfo);

                        termFileList.AddLast(localFileName.ToString());
                    }
                     
                    else
                    {
                        MessageBox.Show("模板文件不存在！");
                        return;
                    }
                    #region tt
                    /* 
                    # region 替换
                    Word.Application wordApp = new Word.Application();
                    Word.Document wordDocument = new Word.Document();
                    object Obj_FileName = localFileName;
                    object Visible = false;
                    object ReadOnly = false;
                    object missing = System.Reflection.Missing.Value;

                    bool replaceSuccess = true;
                    try
                    {
                        //打开文件
                        wordDocument = wordApp.Documents.Open(ref Obj_FileName, ref missing, ref ReadOnly, ref missing,
                                        ref missing, ref missing, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref Visible,
                                        ref missing, ref missing, ref missing,
                                        ref missing);
                        //读取word内容
                        string wordText = wordDocument.Content.Text.Trim();
                        Regex regex;
                        string pattern;
                        MatchCollection match;
                        //替换这个Term的所有的变量
                        foreach (VariableEntry variableEntry in termEntry.variableList)
                        {
                            Contract_Template_Variable variable = variableEntry.variable;
                            string srcStr = variableEntry.variable.Token_Text;
                            string desStr;
                            if (variable.Type.Equals("文本"))
                            {
                                TextBox control = variableEntry.control as TextBox;
                                desStr = control.Text.Trim();
                                //如果没输入数据，就取消替换
                                //if (desStr.Equals(""))
                                //{
                                //    continue;
                                //}
                            }
                            else {
                                DateTimePicker control = variableEntry.control as DateTimePicker;
                                desStr = control.Text;
                            }

                            pattern = srcStr;
                            regex = new Regex(pattern, RegexOptions.IgnoreCase);
                            match = regex.Matches(wordText);

                            if (match.Count > 0)
                            {
                                wordDocument.Content.Find.Text = srcStr;
                                object findText = srcStr;
                                object replaceWith = desStr;
                                object replace = Word.WdReplace.wdReplaceAll;
                                wordDocument.Content.Find.ClearFormatting();
                                //替换失败
                                if (!wordDocument.Content.Find.Execute(
                                    ref findText, ref missing,
                                    ref missing, ref missing,
                                    ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref replaceWith, ref replace,
                                    ref missing, ref missing,
                                    ref missing, ref missing))
                                {
                                    replaceSuccess = false;
                                    break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        throw ex;
                    }
                    finally
                    {
                        wordDocument.SaveAs(ref Obj_FileName, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing);
                        wordDocument.Close(ref missing, ref missing, ref missing);

                        wordApp.Quit(ref missing, ref missing, ref missing);
                        //结束WINWORD.exe程序
                        wordApp = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        //替换失败的话，删除失败的文件
                        if (!replaceSuccess)
                        {
                            MessageBox.Show("生成失败");
                            if (Directory.Exists(localFileName.ToString()))
                            {
                                Directory.Delete(localFileName.ToString());
                            }
                        }
                    }
                    #endregion
                    * */
                    #endregion
                }
            }
            LinkedList<string> failedFileList = ftp.download(pp);
            if (failedFileList.Count > 0)
            {
                MessageBox.Show("模板文件损坏！");
                return;
            }
            pp.Clear();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            #endregion

            #region 替换模板文件中的令牌文本
            foreach (SectionEntry secionEntry in record)
            {
                foreach (TermEntry termEntry in secionEntry.termList)
                {
                    string srcFileName = termEntry.TermSample.FileName;
                    int index = srcFileName.IndexOf(@"/");
                    string remoteFile = srcFileName.Substring(index + 1);
                    int lastIndex = remoteFile.LastIndexOf(@"/");
                    string remoteFileName = remoteFile.Substring(lastIndex + 1);
                    orderOfTerm++;
                    Object localFileName = defaultDic + remoteFileName;

                    # region 替换
                    Word.Application wordApp = new Word.Application();
                    Word.Document wordDocument = new Word.Document();
                    object Obj_FileName = localFileName;
                    object Visible = false;
                    object ReadOnly = false;
                    object missing = System.Reflection.Missing.Value;

                    bool replaceSuccess = true;
                    try
                    {
                        //打开文件
                        wordDocument = wordApp.Documents.Open(ref Obj_FileName, ref missing, ref ReadOnly, ref missing,
                                        ref missing, ref missing, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref Visible,
                                        ref missing, ref missing, ref missing,
                                        ref missing);
                        //读取word内容
                        string wordText = wordDocument.Content.Text.Trim();
                        Regex regex;
                        string pattern;
                        MatchCollection match;
                        //替换这个Term的所有的变量
                        foreach (VariableEntry variableEntry in termEntry.variableList)
                        {
                            Contract_Template_Variable variable = variableEntry.variable;
                            string srcStr = variableEntry.variable.Token_Text;
                            string desStr;
                            if (variable.Type.Equals("文本"))
                            {
                                TextBox control = variableEntry.control as TextBox;
                                desStr = control.Text.Trim();
                                //如果没输入数据，就取消替换
                                //if (desStr.Equals(""))
                                //{
                                //    continue;
                                //}
                            }
                            else
                            {
                                DateTimePicker control = variableEntry.control as DateTimePicker;
                                desStr = control.Text;
                            }

                            pattern = srcStr;
                            regex = new Regex(pattern, RegexOptions.IgnoreCase);
                            match = regex.Matches(wordText);

                            if (match.Count > 0)
                            {
                                wordDocument.Content.Find.Text = srcStr;
                                object findText = srcStr;
                                object replaceWith = desStr;
                                object replace = Word.WdReplace.wdReplaceAll;
                                wordDocument.Content.Find.ClearFormatting();
                                //替换失败
                                if (!wordDocument.Content.Find.Execute(
                                    ref findText, ref missing,
                                    ref missing, ref missing,
                                    ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref replaceWith, ref replace,
                                    ref missing, ref missing,
                                    ref missing, ref missing))
                                {
                                    replaceSuccess = false;
                                    break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        throw ex;
                    }
                    finally
                    {
                        wordDocument.SaveAs(ref Obj_FileName, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing);
                        wordDocument.Close(ref missing, ref missing, ref missing);

                        wordApp.Quit(ref missing, ref missing, ref missing);
                        //结束WINWORD.exe程序
                        wordApp = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        //替换失败的话，删除失败的文件
                        if (!replaceSuccess)
                        {
                            MessageBox.Show("生成失败");
                            if (Directory.Exists(localFileName.ToString()))
                            {
                                Directory.Delete(localFileName.ToString());
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion

            //最后一个term应该是签署页，它的order设为 -1，表示需要加 换页符
            LinkedListNode<TermInfo> lastNode = termInfoList.Last;
            if (lastNode != null)
            {
                lastNode.Value.order = -1;
            }

            //合并到一起
            MergeFilesByInsert(contractFile, termInfoList);

            #region 上传
            //文件上传到服务器，数据写入数据库
            //文件上传
            string prefix = "ftpserver:root_directory/";
            string remoteContractFile ="合同文本/" + supplierID + "/" + contractID + ".docx";
            bool isUploaded = false;
            while (!isUploaded)
            {
                isUploaded = ftp.upload(contractFile, remoteContractFile);
            }
            //删除本地遗留文件
            if (File.Exists(contractFile))
            {
                File.Delete(contractFile);
            }
            foreach (string termFile in termFileList)
            {
                if (File.Exists(termFile))
                {
                    File.Delete(termFile);
                }
            }
            string remoteContractLocation = prefix + remoteContractFile;

            //创建uploadContractText对象，用于存储
            ContractText uploadContractText = new ContractText();
            uploadContractText.ContractID = this.tbContractTextID.Text;
            uploadContractText.ContractName = this.tbContractTextName.Text;
            uploadContractText.SupplierID = this.tbSupplierID.Text;
            uploadContractText.CreateMode = "手工创建";
            uploadContractText.CreateTime = System.DateTime.Now;
            uploadContractText.BeginTime = System.DateTime.Now;
            uploadContractText.EndTime = System.DateTime.Now;
            uploadContractText.FileLocation = remoteContractLocation;
            uploadContractText.Status = "未审核";
            uploadContractText.CreaterID = "2015001";
            uploadContractText.ContractType = "定额合同";

            if (contractTextDALTool.addContractText(uploadContractText) > 0)
            {
                MessageBox.Show("保存成功!");
            }
            else {
                MessageBox.Show("保存失败!");
            }

            #endregion
        }

        /// <summary>
        /// 采取插入合并两个word
        /// </summary>
        /// <param name="desFileName">目标word文件</param>
        /// <param name="srcFileNameList">需要插入目标word的src文件</param>
        private void MergeFilesByInsert(string desFileName, LinkedList<TermInfo> termInfoList)
        {
            Word.Application wordApp = new Word.Application();
            Word.Document objDocLast = null;
            
            object objMissing = System.Reflection.Missing.Value;
            object objFalse = false;
            object confirmConversion = false;
            object link = false;
            object attachment = false;
            object desObject = desFileName;

            //创建file
            createWordFile(desFileName);

            try
            {
                //打开模板文件
                objDocLast = wordApp.Documents.Open(
                 ref desObject,    //FileName  
                 ref objMissing,   //ConfirmVersions  
                 ref objMissing,   //ReadOnly  
                 ref objMissing,   //AddToRecentFiles  
                 ref objMissing,   //PasswordDocument  
                 ref objMissing,   //PasswordTemplate  
                 ref objMissing,   //Revert  
                 ref objMissing,   //WritePasswordDocument  
                 ref objMissing,   //WritePasswordTemplate  
                 ref objMissing,   //Format  
                 ref objMissing,   //Enconding  
                 ref objMissing,   //Visible  
                 ref objMissing,   //OpenAndRepair  
                 ref objMissing,   //DocumentDirection  
                 ref objMissing,   //NoEncodingDialog  
                 ref objMissing    //XMLTransform  
                 );

                objDocLast.Activate();

                //遍历所有需要插入的word
                foreach (TermInfo termInfo in termInfoList)
                {
                    //判断下是否需要插入标题
                    if (termInfo.order > 0)
                    {
                        //插入标题
                        StringBuilder titleStr = new StringBuilder("第");
                        titleStr.Append(getChineseNumber(termInfo.order))
                            .Append("条  ")
                            .Append(termInfo.title);
                        //字体加粗
                        wordApp.Selection.Font.Bold = 1;
                        //设置段间距
                        wordApp.Selection.ParagraphFormat.LineUnitBefore = 0.5f;
                        wordApp.Selection.ParagraphFormat.LineUnitAfter = 0.5f;
                        //设置首行缩进
                        wordApp.Selection.ParagraphFormat.CharacterUnitFirstLineIndent = 2;
                        //插入文本
                        wordApp.Selection.TypeText(titleStr.ToString());
                        //回车符
                        wordApp.Selection.TypeParagraph();
                    }
                    else if(termInfo.order == -1){ 
                        //插入换页符
                        object nextPageBreak = (int)WdBreakType.wdSectionBreakNextPage;
                        //字体不加粗
                        wordApp.Selection.Font.Bold = 0;
                        //先插入一句 "（以下无正文，为合同签署页）"
                        wordApp.Selection.TypeText("（以下无正文，为合同签署页）");
                        wordApp.Selection.InsertBreak(ref nextPageBreak);
                    }

                    wordApp.Selection.InsertFile(
                        termInfo.TermFileName,
                        ref objMissing,
                        ref confirmConversion,
                        ref link,
                        ref attachment
                        );
                }
                //保存
                objDocLast.SaveAs(
                  ref desObject,      //FileName  
                  ref objMissing,     //FileFormat  
                  ref objMissing,     //LockComments  
                  ref objMissing,     //PassWord       
                  ref objMissing,     //AddToRecentFiles  
                  ref objMissing,     //WritePassword  
                  ref objMissing,     //ReadOnlyRecommended  
                  ref objMissing,     //EmbedTrueTypeFonts  
                  ref objMissing,     //SaveNativePictureFormat  
                  ref objMissing,     //SaveFormsData  
                  ref objMissing,     //SaveAsAOCELetter,  
                  ref objMissing,     //Encoding  
                  ref objMissing,     //InsertLineBreaks  
                  ref objMissing,     //AllowSubstitutions  
                  ref objMissing,     //LineEnding  
                  ref objMissing      //AddBiDiMarks  
                );  
                objDocLast.Close(ref objMissing, ref objMissing, ref objMissing);
            }
            catch (Exception)
            {
                MessageBox.Show("生成失败");
            }
            finally {
                //关闭wordApp
                wordApp.Quit(
                  ref objMissing,     //SaveChanges  
                  ref objMissing,     //OriginalFormat  
                  ref objMissing      //RoutDocument  
                  );
                wordApp = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 创建word文件
        /// </summary>
        /// <param name="fileName">文件名称</param>
        private void createWordFile(string fileName) {
            if (File.Exists(fileName)) {
                MessageBox.Show("文件已存在");
                return;
            }
            object fileObject = fileName;
            Word.Application wordApp = new Word.Application();
            Word.Document wordDocument = new Word.Document();
            object missing = System.Reflection.Missing.Value;
            try
            {
                wordDocument = wordApp.Documents.Add(
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing);
            }
            finally {
                wordDocument.SaveAs(ref fileObject, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing);
                wordDocument.Close(ref missing, ref missing, ref missing);

                wordApp.Quit(ref missing, ref missing, ref missing);
                //结束WINWORD.exe程序
                wordApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 获得数字的中文字符(默认数字不会大于100)
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseNumber(int num) { 
            StringBuilder strBui = new StringBuilder();
            int tens = num / 10;
            int ones = num % 10;

            if (tens == 0)
            {
                strBui.Append(getChineseSingleDigit(ones));
            }
            else {
                if (tens > 1) {
                    strBui.Append(getChineseSingleDigit(tens));
                }
                strBui.Append("十")
                    .Append(getChineseSingleDigit(ones));
            }

            return strBui.ToString();
        }

        /// <summary>
        /// 返回单个字符对应的中文
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseSingleDigit(int num) {
            string result = "";
            switch (num) {
                case 1: result = "一"; break;
                case 2: result = "二"; break;
                case 3: result = "三"; break;
                case 4: result = "四"; break;
                case 5: result = "五"; break;
                case 6: result = "六"; break;
                case 7: result = "七"; break;
                case 8: result = "八"; break;
                case 9: result = "九"; break;

                default: result = ""; break;
            }

            return result;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
