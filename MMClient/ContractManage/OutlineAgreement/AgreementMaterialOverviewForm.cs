﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class AgreementMaterialOverviewForm : DockContent
    {
        #region 字段初始化
        //业务逻辑
        private AgreementContractBLL agreementContractTool = new AgreementContractBLL();
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private MaterialAccountBLL materialAccountTool = new MaterialAccountBLL();
        private SourceItemMaterialBLL sourceItemMaterialTool = new SourceItemMaterialBLL();
        //当前form操作类型(new、edit、change、display)
        private string opType;
        //新的协议信息
        private Agreement_Contract newAgreementContract;
        //保存协议的信息
        private Agreement_Contract agreementContract;
        //parentForm
        private DockContent parentForm;
        // 所有的物料ID
        private List<string> materialIDList = null;
        // 所有的物料组
        private List<string> materialGroupIDList = null;
        #endregion

        /// <summary>
        /// 框架协议项界面初始化
        /// </summary>
        /// <param name="_agreementContract"></param>
        /// <param name="_opType"></param>
        /// <param name="_parentForm"></param>
        public AgreementMaterialOverviewForm(Agreement_Contract _agreementContract, string _opType, DockContent _parentForm) {
            InitializeComponent();

            this.agreementContract = _agreementContract;
            this.opType = _opType;
            this.parentForm = _parentForm;

            initialForm();
        }

        /// <summary>
        /// 框架协议项界面初始化(用于变更)
        /// </summary>
        /// <param name="_agreementContract"></param>
        /// <param name="_newAgreementContract"></param>
        /// <param name="_opType"></param>
        /// <param name="_parentForm"></param>
        public AgreementMaterialOverviewForm(Agreement_Contract _oldAgreementContract, Agreement_Contract _newAgreementContract,
            string _opType, DockContent _parentForm) {
            InitializeComponent();

            this.agreementContract = _oldAgreementContract;
            this.newAgreementContract = _newAgreementContract;
            this.opType = _opType;
            this.parentForm = _parentForm;

            initialForm();
        }

        /// <summary>
        /// 初始化界面
        /// </summary>
        private void initialForm() {
            //获取所有的物料ID
            materialIDList = materialTool.GetAllMaterialID();
            //获取所有的物料组ID
            materialGroupIDList = generalTool.GetAllGroupName();
            
            //显示协议抬头信息
            this.tbAgreementID.Text = agreementContract.Agreement_Contract_ID;
            this.tbAgreementType.Text = agreementContract.Agreement_Contract_Type;
            this.tbSupplierID.Text = agreementContract.Supplier_ID;
            this.tbCurrencyType.Text = agreementContract.Currency_Type;
            this.dtpCreateTime.Value = agreementContract.Create_Time;
            string contractID = agreementContract.Agreement_Contract_ID;

            if (opType.Equals("display")) {
                this.btnAdd.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSave.Enabled = false;
            }
            else if (opType.Equals("change")) {
                this.btnAdd.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            if (!opType.Equals("new"))
            {
                List<Agreement_Contract_Item> itemsList = agreementContractTool.getItemsByAgreementContractID(agreementContract.Agreement_Contract_ID,
                    agreementContract.Agreement_Contract_Version);
                //填写已存在的数据
                fillMaterialItem(itemsList);
            }
            else
            {
                fillMaterialItemForCreat(contractID);
            }
        }

        /// <summary>
        /// 填写已存在的数据
        /// </summary>
        /// <param name="itemList"></param>
        private void fillMaterialItem(List<Agreement_Contract_Item> itemList)
        {
            int rowNum = itemList.Count;
            this.dgvAgreementItems.Rows.Add(rowNum);
            int i = 0;
            foreach (Agreement_Contract_Item item in itemList)
            {
                DataGridViewRow currentRow = this.dgvAgreementItems.Rows[i];
                currentRow.Cells[0].Value = item.Item_Num;
                currentRow.Cells[1].Value = item.I;
                currentRow.Cells[2].Value = item.A;
                //绑定数据源
                DataGridViewComboBoxCell cell =
                    (DataGridViewComboBoxCell)currentRow.Cells[3];
                cell.DataSource = materialIDList;
                cell.Value = item.Material_ID;
                //currentRow.Cells[3].Value = item.Material_ID;
                currentRow.Cells[4].Value = item.Short_Text;
                currentRow.Cells[5].Value = convertDoubleToString(item.Target_Quantity);
                currentRow.Cells[6].Value = item.OUn;
                currentRow.Cells[7].Value = convertDoubleToString(item.Net_Price);
                currentRow.Cells[8].Value = convertDoubleToString(item.Every);
                currentRow.Cells[9].Value = item.OPU;
                currentRow.Cells[10].Value = item.Material_Group;
                currentRow.Cells[11].Value = item.Factory;

                i++;
            }

            if (opType.Equals("display"))
            {
                this.dgvAgreementItems.ReadOnly = true;
            }
            else if (opType.Equals("change")) {
                //只可以修改价格和数量
                this.dgvAgreementItems.Columns[0].ReadOnly = true;
                this.dgvAgreementItems.Columns[1].ReadOnly = true;
                this.dgvAgreementItems.Columns[2].ReadOnly = true;
                this.dgvAgreementItems.Columns[3].ReadOnly = true;
                this.dgvAgreementItems.Columns[4].ReadOnly = true;
                this.dgvAgreementItems.Columns[6].ReadOnly = true;
                this.dgvAgreementItems.Columns[8].ReadOnly = true;
                this.dgvAgreementItems.Columns[9].ReadOnly = true;
                this.dgvAgreementItems.Columns[10].ReadOnly = true;
                this.dgvAgreementItems.Columns[11].ReadOnly = true;
            }
        }

        private void fillMaterialItemForCreat(string contractID)
        {
            List<SourceItemMaterial> materialList = sourceItemMaterialTool.getSourceItemMaterialByFK(contractID);
            if (materialList == null || materialList.Count == 0)
            {
                MessageUtil.ShowError("寻源结果有误，请确认存在对应的寻源结果！");
                return;
            }
            int rowNum = materialList.Count;
            this.dgvAgreementItems.Rows.Add(rowNum);
            int i = 0;
            foreach(SourceItemMaterial material in materialList)
            {
                DataGridViewRow currentRow = this.dgvAgreementItems.Rows[i];
                currentRow.Cells[0].Value = i + 1;
                //绑定数据源
                DataGridViewComboBoxCell cell =
                    (DataGridViewComboBoxCell)currentRow.Cells[3];
                cell.DataSource = materialIDList;
                currentRow.Cells[3].Value = material.MaterialID;
                currentRow.Cells[4].Value = material.MaterialName;
                currentRow.Cells[6].Value = material.Measurement;
                currentRow.Cells[7].Value = material.NetPrice;
                currentRow.Cells[8].Value = material.ProviderNumber;
                currentRow.Cells[9].Value = material.NetPriceUnit;
                currentRow.Cells[10].Value = material.MaterialGroup;
                currentRow.Cells[11].Value = agreementContract.Factory;

                i++;
            }
        }

        /// <summary>
        /// 点击保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            # region 判断输入是否有效
            //先检测是否存在至少一条有效记录
            //最后一个是系统默认条件，不算
            if (this.dgvAgreementItems.Rows.Count <= 0)
            {
                MessageUtil.ShowError("请添加数据!");
                return;
            }
            List<Agreement_Contract_Item> itemList = new List<Agreement_Contract_Item>();
            try
            {
                foreach (DataGridViewRow row in this.dgvAgreementItems.Rows)
                {
                    Agreement_Contract_Item item = new Agreement_Contract_Item();
                    item.Agreement_Contract_ID = agreementContract.Agreement_Contract_ID;
                    item.Agreement_Contract_Version = agreementContract.Agreement_Contract_Version;
                    item.Item_Num = Convert.ToInt32(row.Cells[0].Value.ToString());
                    item.I = getDataGridViewCellValueString(row.Cells[1]);
                    item.A = getDataGridViewCellValueString(row.Cells[2]);
                    item.Material_ID = getDataGridViewCellValueString(row.Cells[3]);
                    item.Short_Text = getDataGridViewCellValueString(row.Cells[4]);
                    item.Target_Quantity = getDataGridViewCellValueDouble(row.Cells[5]);
                    item.OUn = getDataGridViewCellValueString(row.Cells[6]);
                    item.Net_Price = getDataGridViewCellValueDouble(row.Cells[7]);
                    item.Every = getDataGridViewCellValueDouble(row.Cells[8]);
                    item.OPU = getDataGridViewCellValueString(row.Cells[9]);
                    item.Material_Group = getDataGridViewCellValueString(row.Cells[10]);
                    item.Factory = getDataGridViewCellValueString(row.Cells[11]);

                    itemList.Add(item);
                }
            }
            catch (Exception ex) {
                MessageUtil.ShowError("请正确填写!");
                return;
            }

            //如果是数量合同 必须填写目标数量
            if (agreementContract.Agreement_Contract_Type.Equals("数量合同")) { 
                foreach(Agreement_Contract_Item item in itemList){
                    if (item.Target_Quantity == 0.0) {
                        MessageUtil.ShowError("请填写目标数量");
                        return;
                    }
                }
            }

            #endregion

            # region 判断操作类型

            if (opType.Equals("new")) {
                //合同新建
                int result = agreementContractTool.addNewAgreementContract(agreementContract,
                itemList);

                if (result > 0)
                {
                    MessageBox.Show("保存成功");
                    opType = "edit";
                }
                else
                {
                    MessageBox.Show("保存失败");
                }
            }
            else if (opType.Equals("edit")) {
                var result = MessageBox.Show("确认更改合同内容？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                //合同更新
                agreementContract.Status = "待审核";
                int num = agreementContractTool.updateAgreementContract(agreementContract, itemList);
                if (num > 0)
                    MessageBox.Show("更新成功");
                else
                    MessageBox.Show("更新失败");
            }
            else if (opType.Equals("change")) {
                //合同变更
                var result = MessageBox.Show("确认变更合同内容？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }

                agreementContract.Status = "已变更";
                newAgreementContract.Status = "待审核";
                //修改item
                foreach (Agreement_Contract_Item item in itemList) {
                    item.Agreement_Contract_Version = newAgreementContract.Agreement_Contract_Version;
                }

                int num = agreementContractTool.changeAgreementContract(agreementContract, newAgreementContract, itemList);
                if (num > 0)
                {
                    MessageBox.Show("变更成功");
                }
                else {
                    MessageBox.Show("变更失败");
                }
            }

            # endregion
        }

        /// <summary>
        /// 点击添加行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //支持多种添加形式
            this.dgvAgreementItems.Rows.Add();

            //项目号
            int rowCount = this.dgvAgreementItems.Rows.Count;
            if (rowCount == 1)
            {
                this.dgvAgreementItems.Rows[0].Cells[0].Value = 1;
            }
            else {
                this.dgvAgreementItems.Rows[rowCount - 1].Cells[0].Value =
                    Convert.ToInt32(this.dgvAgreementItems.Rows[rowCount - 2].Cells[0].Value) + 1;
            }
            setDataGridViewEditable(this.dgvAgreementItems.Rows[rowCount - 1].Cells[0], false);
            //设置物料ID源
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)this.dgvAgreementItems.Rows[rowCount - 1].Cells[3];
            cell.DataSource = materialIDList;
            //设置工厂
            this.dgvAgreementItems.Rows[rowCount - 1].Cells[this.dgvAgreementItems.Columns.Count - 1].Value = agreementContract.Factory;
            setDataGridViewEditable(this.dgvAgreementItems.Rows[rowCount - 1].Cells[this.dgvAgreementItems.Columns.Count - 1], false);
        }

        /// <summary>
        /// 关闭parentForm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AgreementMaterialOverviewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!(this.parentForm == null || this.parentForm.IsDisposed)) {
                this.parentForm.Close();
            }
        }

        /// <summary>
        /// 删除行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            /*
            if (this.dgvAgreementItems.Rows.Count == 0) {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvAgreementItems.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvAgreementItems.Rows.Count) {
                this.dgvAgreementItems.Rows.RemoveAt(rowIndex);
                updateRowNum();
            }
             * */
            this.parentForm.Show();
        }

        /// <summary>
        /// 刷新行的序号
        /// </summary>
        private void updateRowNum() { 
            int i=1;
            foreach (DataGridViewRow row in this.dgvAgreementItems.Rows) {
                row.Cells[0].Value = i;
                i++;
            }
        }

        /// <summary>
        /// 如果I列内容发生变化，其余列相应变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAgreementItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //初始化的时候也会触发这个时间，此时row为空，这里进行过滤一下
            if (e.RowIndex < 0 || e.RowIndex >= this.dgvAgreementItems.Rows.Count) {
                return;
            }
            if (this.dgvAgreementItems.Columns[e.ColumnIndex].HeaderText.Equals("I")) {
                //datagridview内容为空时，value为null，不是""
                object value = this.dgvAgreementItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string str = value == null ? "" : value.ToString().Trim();
                DataGridViewRow row = this.dgvAgreementItems.Rows[e.RowIndex];
                //为空
                if (str.Equals(""))
                {
                    // 为空表示标准采购
                    setDataGridViewEditable(row.Cells[3], true);           //物料编号需填写
                    // 绑定数据源
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[3];
                    cell.DataSource = materialIDList;
                    setDataGridViewEditable(row.Cells[6], true);            //OUn需要填写
                    setDataGridViewEditable(row.Cells[7], true);            //净价需要填写
                    setDataGridViewEditable(row.Cells[8], true);            //每 需要填写
                    setDataGridViewEditable(row.Cells[9], true);            //OPU需要填写
                    setDataGridViewEditable(row.Cells[10], false);          //物料组不需要填写
                }
                else if(str.Equals("W")) { 
                    // 物料组
                    setDataGridViewEditable(row.Cells[3], false);           //物料编号不需填写
                    // 清空数据源
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[3];
                    cell.Value = "";
                    setDataGridViewEditable(row.Cells[6], false);           //OUn不需要填写
                    setDataGridViewEditable(row.Cells[7], false);           //净价不需要填写
                    setDataGridViewEditable(row.Cells[8], false);           //每 不需要填写
                    setDataGridViewEditable(row.Cells[9], false);           //OPU不需要填写
                    setDataGridViewEditable(row.Cells[10], true);           //物料组需要填写
                }
                else if (str.Equals("M")) {
                    // 表示未知物料
                }
                else if (str.Equals("D")){
                    // 表示服务
                }
                else if (str.Equals("K")){
                    // 表示寄售业务
                }
            }
        }

        /// <summary>
        /// 设置单元格是否可编辑
        /// </summary>
        /// <param name="cell">设置的单元格</param>
        /// <param name="status">是否可编辑</param>
        private void setDataGridViewEditable(DataGridViewCell cell, bool status) {
            if (status)
            {
                cell.ReadOnly = false;
                cell.Style.BackColor = Color.White;
            }
            else {
                cell.ReadOnly = true;
                cell.Style.BackColor = Color.LightSkyBlue;
            }
        }

        /// <summary>
        /// 获得DataGridViewCell的string值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private string getDataGridViewCellValueString(DataGridViewCell cell) {
            if (cell.Value == null)
                return "";
            else
                return cell.Value.ToString();
        }

        /// <summary>
        /// 获得DataGridView的double值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private double getDataGridViewCellValueDouble(DataGridViewCell cell) {
            if (cell.Value == null || cell.Value.ToString().Trim().Equals(""))
                return 0.0;
            else
                return Convert.ToDouble(cell.Value.ToString());
        }

        /// <summary>
        /// 把double转化为string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string convertDoubleToString(double value) {
            if (Math.Abs(value) <= 0.00000001)
                return "";
            else
                return Convert.ToString(value);
        }

        /// <summary>
        /// 编辑单元格控件时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAgreementItems_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = this.dgvAgreementItems.CurrentCell.ColumnIndex;
            if (columnIndex == 3 && e.Control is ComboBox) {
                ComboBox combo = e.Control as ComboBox;
                combo.SelectedIndexChanged += materialID_SelectedIndexChanged;
            }
        }

        private void materialID_SelectedIndexChanged(object sender, EventArgs e) {
            int rowIndex = this.dgvAgreementItems.CurrentCell.RowIndex;
            int columnIndex = this.dgvAgreementItems.CurrentCell.ColumnIndex;
            //可以转换
            if (rowIndex >= 0)
            {
                var sendingCB = sender as DataGridViewComboBoxEditingControl;
                string materialID = sendingCB.EditingControlFormattedValue.ToString();
                if (!materialID.Equals(""))
                {
                    //DataTable dt = generalTool.getMaterialInfo(materialID);
                    MaterialBase materialInfo = materialAccountTool
                        .GetBasicInformation(materialID);

                    //DataRow row = dt.Rows[0];
                    DataGridViewRow dgvRow = this.dgvAgreementItems.Rows[rowIndex];
                    //短文本
                    dgvRow.Cells[4].Value = materialInfo.Material_Name;
                    //单位
                    dgvRow.Cells[6].Value = materialInfo.Measurement;
                    //物料组
                    dgvRow.Cells[10].Value = materialInfo.Material_Group;
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportDataGridViewToWord(this.dgvAgreementItems,new SaveFileDialog());
        }

        /// <summary>
        /// 将dgv中的数据导出到word文档中
        /// </summary>
        /// <param name="srcDgv">要导出的DataGridView</param>
        /// <param name="progreesBar">要显示的进度条</param>
        /// <param name="sfile">保存文件对话框</param>
        private void ExportDataGridViewToWord(DataGridView srcDgv, SaveFileDialog sfile)
        {
            if (srcDgv.Rows.Count == 0)
            {
                MessageBox.Show("没有数据可供导出！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                sfile.AddExtension = true;
                sfile.DefaultExt = ".doc";
                sfile.Filter = "(*.doc)|*.doc";
                if (sfile.ShowDialog() == DialogResult.OK)
                {
                    object path = sfile.FileName;
                    Object none = System.Reflection.Missing.Value;
                    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                    Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Add(ref none, ref none, ref none, ref none);
                    //建立表格
                    Microsoft.Office.Interop.Word.Table table = document.Tables.Add(document.Paragraphs.Last.Range, srcDgv.Rows.Count, srcDgv.Columns.Count, ref none, ref none);
                    try
                    {
                        for (int i = 0; i < srcDgv.Columns.Count; i++)//设置标题
                        {
                            table.Cell(0, i + 1).Range.Text = srcDgv.Columns[i].HeaderText;
                        }
                        MessageBox.Show(srcDgv.RowCount.ToString());
                        int k = 1;
                        for (int i = 0; i < srcDgv.Rows.Count; i++)//填充数据
                        {
                            for (int j = 0; j < srcDgv.Columns.Count; j++)
                            {
                                table.Cell(k + 1, j + 1).Range.Text = srcDgv.Rows[i].Cells[j].Value.ToString();
                            }
                            k++;
                        }
                        document.SaveAs(ref path, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none, ref none);
                        document.Close(ref none, ref none, ref none);

                        MessageBox.Show("数据已经成功导出到：" + sfile.FileName.ToString(), "导出完成", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "友情提示", MessageBoxButtons.OK);
                        MessageBox.Show(e.StackTrace);
                    }
                    finally
                    {
                        wordApp.Quit(ref none, ref none, ref none);
                    }
                }
            }

        } 
    }
}
