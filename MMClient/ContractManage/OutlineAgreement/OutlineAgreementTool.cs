﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.SqlServerDAL;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Model.SourcingManage.SourceingRecord;

namespace MMClient.ContractManage.OutlineAgreement
{
    class OutlineAgreementTool
    {
        //从采购信息记录中获取所有还未签订框架协议的寻源编号
        public List<string> getSourceIdFromRecordInfo()
        {
            List<string> result = new List<string>();
            string sql = "select distinct(FromId) from RecordInfo where FromType = 1 and ContractState = '无效'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据寻源编号读取供应商编号和工厂编号
        public DataTable getBasicInformationBySourceId(string sourceId)
        {
            DataTable dt = null;
            string sql = "select SupplierId,FactoryId from RecordInfo where FromId = '" + sourceId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        //根据寻源编号读取供应商编号
        public List<string> getSupplierBySourceId(string sourceId)
        {
            List<string> result = new List<string>();
            string sql = "select distinct(SupplierId) from RecordInfo where FromId = '" + sourceId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据寻源编号读取工厂编号
        public List<string> getFactoryBySourceId(string sourceId)
        {
            List<string> result = new List<string>();
            string sql = "select distinct(FactoryId) from RecordInfo where FromId = '" + sourceId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据寻源编号，供应商编号，工厂编号读取所有基本数据
        //采购组，采购组织，库存地
        public DataTable getAllBasicInformationFromRecordId(string sourceId, string supplierId, string factoryId)
        {
            DataTable dt = null;
            string sql = "select PurchaseGroup,PurchaseOrg,StockId,MaterialId,MaterialName,DemandCount,NetPrice,PriceDetermineId from RecordInfo where FromId = '" 
                         + sourceId + "' and SupplierId = '" + supplierId + "' and FactoryId = '" + factoryId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public int addRecordInfo(List<RecordInfo> infoList)
        {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();
            foreach (RecordInfo info in infoList)
            {
                StringBuilder sb = new StringBuilder("insert into RecordInfo(")
                    .Append("RecordId,SupplierId,SupplierName,PurchaseOrg,PurchaseGroup,MaterialId,")
                    .Append("MaterialName,FactoryId,StockId,DemandCount,FromType,FromId,PaymentClause,")
                    .Append("ContractState,CreateTime,PriceDetermineId,NetPrice,TradeClause,EndTime,StartTime")
                    .Append(") values(")
                    .Append("@RecordId,@SupplierId,@SupplierName,@PurchaseOrg,@PurchaseGroup,@MaterialId,")
                    .Append("@MaterialName,@FactoryId,@StockId,@DemandCount,@FromType,@FromId,@PaymentClause,")
                    .Append("@ContractState,@CreateTime,@PriceDetermineId,@NetPrice,@TradeClause,@EndTime,@StartTime)");

                SqlParameter[] sqlParas = new SqlParameter[]{
                    new SqlParameter("@RecordId",SqlDbType.VarChar),
                    new SqlParameter("@SupplierId",SqlDbType.VarChar),
                    new SqlParameter("@SupplierName",SqlDbType.VarChar),
                    new SqlParameter("@PurchaseOrg",SqlDbType.VarChar),
                    new SqlParameter("@PurchaseGroup",SqlDbType.VarChar),
                    new SqlParameter("@MaterialId",SqlDbType.VarChar),
                    new SqlParameter("@MaterialName",SqlDbType.VarChar),
                    new SqlParameter("@FactoryId",SqlDbType.VarChar),
                    new SqlParameter("@StockId",SqlDbType.VarChar),
                    new SqlParameter("@DemandCount",SqlDbType.Int),
                    new SqlParameter("@FromType",SqlDbType.Int),
                    new SqlParameter("@FromId",SqlDbType.VarChar),
                    new SqlParameter("@PaymentClause",SqlDbType.VarChar),
                    new SqlParameter("@ContractState",SqlDbType.VarChar),
                    new SqlParameter("@CreateTime",SqlDbType.DateTime),
                    new SqlParameter("@PriceDetermineId",SqlDbType.VarChar),
                    new SqlParameter("@NetPrice",SqlDbType.Decimal),
                    new SqlParameter("@TradeClause",SqlDbType.VarChar),
                    new SqlParameter("@EndTime",SqlDbType.DateTime),
                    new SqlParameter("@StartTime",SqlDbType.DateTime)
                };

                sqlParas[0].Value = info.RecordInfoId;
                sqlParas[1].Value = info.SupplierId;
                sqlParas[2].Value = info.SupplierName;
                sqlParas[3].Value = info.PurchaseOrg;
                sqlParas[4].Value = info.PurchaseGroup;
                sqlParas[5].Value = info.MaterialId;
                sqlParas[6].Value = info.MaterialName;
                sqlParas[7].Value = info.FactoryId;
                sqlParas[8].Value = info.StockId;
                sqlParas[9].Value = info.DemandCount;
                sqlParas[10].Value = info.FromType;
                sqlParas[11].Value = info.FromId;
                sqlParas[12].Value = info.PaymentClause;
                sqlParas[13].Value = info.ContactState;
                sqlParas[14].Value = info.CreatTime;
                sqlParas[15].Value = info.PriceDetermineId;
                sqlParas[16].Value = info.NetPrice;
                sqlParas[17].Value = info.TradeClause;
                sqlParas[18].Value = info.EndTime;
                sqlParas[19].Value = info.StartTime;

                sqlList.Add(sb.ToString());
                sqlParamsList.Add(sqlParas);
            }
            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        public int updateRecordInfo(string sourceId, string supplierId, string factoryId)
        {
            string sql = "update RecordInfo set ContractState = '已签订' where FromId = '" + sourceId
                          + "' and SupplierId = '" + supplierId + "' and FactoryId = '" + factoryId + "'";
            return DBHelper.ExecuteNonQuery(sql);
        }
    }
}
