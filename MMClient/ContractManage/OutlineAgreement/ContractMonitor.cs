﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.SqlServerDAL;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class ContractMonitor : DockContent
    {
        private Agreement_Contract agreementContract;
        private string infoNumber;
        private bool isSet = false;
        public ContractMonitor(Agreement_Contract _agreementContract)
        {
            InitializeComponent();
            this.agreementContract = _agreementContract;
            this.infoNumber = getInfoNumberByContractID(agreementContract.Agreement_Contract_ID);
            initializeForm();
           // forDisplay();
        }

        ///<summary>
        ///初始化屏幕，包括抬头，预警和凭证
        ///</summary>
        private void initializeForm()
        {
            //fill title
            this.lbContractId.Text = agreementContract.Agreement_Contract_ID;
            this.lbContractType.Text = agreementContract.Agreement_Contract_Type;
            this.lbStatus.Text = agreementContract.Status;
            this.lbSupplierName.Text = agreementContract.Supplier_ID;
            this.lbOwner.Text = "0010李静宇";

            if (isSet)
            {
                this.btnSave.Enabled = false;
            }

            fillAlertTab();
            fillOrderTab();
        }

        //填充“预警”界面
        private void fillAlertTab()
        {
            //价值&数量
            if (this.agreementContract.Agreement_Contract_Type.Equals("价值合同"))
            {
                this.tbTargetNumber.ReadOnly = true;
                this.cbxMesurement.Enabled = false;
                this.tbNumberAlert.ReadOnly = true;
                this.tbTargetNumber.Text = "";
                this.tbNumberAlert.Text = "";
                this.tbTargetValue.Text = agreementContract.Target_Value.ToString();
            }
            else if (this.agreementContract.Agreement_Contract_Type.Equals("数量合同"))
            {
                this.tbTargetValue.ReadOnly = true;
                this.cbxCurrencyType.Enabled = false;
                this.tbValueRatioAlert.ReadOnly = true;
                this.tbValueAlert.ReadOnly = true;
                this.tbTargetValue.Text = "";
                this.tbValueRatioAlert.Text = "";
                this.tbValueAlert.Text = "";
                this.tbTargetNumber.Text = agreementContract.Target_Value.ToString();
            }
            //标识
            this.tbContractId.Text = agreementContract.Agreement_Contract_ID;
            this.tbContractType.Text = agreementContract.Agreement_Contract_Type;
            //组织机构
            this.tbPurchaseOrg.Text = agreementContract.Purchase_Organization;
            this.tbPurchaseGroup.Text = agreementContract.Purchase_Group;
            //日期
            this.dtpStartTime.Value = Convert.ToDateTime(agreementContract.Begin_Time);
            this.dtpEndTime.Value = Convert.ToDateTime(agreementContract.End_Time);
        }

        //填充“凭证”界面
        private void fillOrderTab()
        {
            string sql = "select Order_ID,CONVERT(varchar(100),Delivery_Time,1)as Delivery_Time,Number,(Number*net_Price)as Value,Factory_ID from Order_Item where Info_Number = '" + infoNumber + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                int n = dt.Rows.Count;
                int rowCount = this.dgvDetail.Rows.Count;
                for (int i = 1; i <= n; i++)
                {
                    this.dgvDetail.Rows[rowCount - 1].Cells[0].Value = "采购订单";
                    this.dgvDetail.Rows[rowCount - 1].Cells[1].Value = dt.Rows[i - 1][0].ToString();
                    this.dgvDetail.Rows[rowCount - 1].Cells[2].Value = i;
                    this.dgvDetail.Rows[rowCount - 1].Cells[3].Value = dt.Rows[i - 1][1].ToString();
                    this.dgvDetail.Rows[rowCount - 1].Cells[4].Value = dt.Rows[i - 1][2].ToString();
                    this.dgvDetail.Rows[rowCount - 1].Cells[5].Value = "EA";
                    this.dgvDetail.Rows[rowCount - 1].Cells[6].Value = dt.Rows[i - 1][3].ToString();
                    this.dgvDetail.Rows[rowCount - 1].Cells[7].Value = "CNY";
                    this.dgvDetail.Rows[rowCount - 1].Cells[8].Value = dt.Rows[i - 1][4].ToString();
                    this.dgvDetail.Rows[rowCount - 1].Cells[9].Value = "已下单";
                    this.dgvDetail.Rows.Add();
                    rowCount++;
                }
            }
        }

        private string getInfoNumberByContractID(string contractID)
        {
            string infoNumber = "";
            string sql = "select RecordId from RecordInfo where FromType = 2 and FromId = '" + contractID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count != 0)
            {
                infoNumber = dt.Rows[0][0].ToString();
            }
            return infoNumber;
        }

        private void forDisplay()
        {
            //this.dgvDetail.Rows.Add();
            int rowCount = this.dgvDetail.Rows.Count;
            this.dgvDetail.Rows[rowCount - 1].Cells[0].Value = "采购订单";
            this.dgvDetail.Rows[rowCount - 1].Cells[1].Value = "CGDD001";
            this.dgvDetail.Rows[rowCount - 1].Cells[2].Value = "采购订单001";
            this.dgvDetail.Rows[rowCount - 1].Cells[3].Value = "1";
            this.dgvDetail.Rows[rowCount - 1].Cells[4].Value = "04/25/2017";
            this.dgvDetail.Rows[rowCount - 1].Cells[5].Value = "10";
            this.dgvDetail.Rows[rowCount - 1].Cells[6].Value = "EA";
            this.dgvDetail.Rows[rowCount - 1].Cells[7].Value = "100";
            this.dgvDetail.Rows[rowCount - 1].Cells[8].Value = "CNY";
            this.dgvDetail.Rows[rowCount - 1].Cells[9].Value = "s0001";
            this.dgvDetail.Rows[rowCount - 1].Cells[10].Value = "已完成";
        }

        private void lbContractId_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.tbTimeAlert.Text.Equals("") && this.tbValueAlert.Text.Equals("") && this.tbValueRatioAlert.Text.Equals("") && this.tbNumberAlert.Text.Equals(""))
            {
                MessageBox.Show("请至少设置一项预警参数！");
                return;
            }
            MessageBox.Show("预警参数设置成功！");
            isSet = true;
            this.btnSave.Enabled = false;
        }
    }
}
