﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class AgreementContractForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxContractID = new System.Windows.Forms.ComboBox();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbxFactory = new System.Windows.Forms.ComboBox();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxAgreementType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxBuyerOrgnization = new System.Windows.Forms.ComboBox();
            this.cbxBuyerGroup = new System.Windows.Forms.ComboBox();
            this.cbxStorageLocation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tblTermText = new System.Windows.Forms.TextBox();
            this.cbTradeClause = new System.Windows.Forms.ComboBox();
            this.cbPaymentClause = new System.Windows.Forms.ComboBox();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbTargetValue = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbDays3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbDiscountRate2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbDays2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbDiscountRate1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbDays1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxContractID);
            this.groupBox1.Controls.Add(this.cbxSupplierID);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cbxFactory);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.dtpBeginTime);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbxAgreementType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(753, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "抬头数据";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cbxContractID
            // 
            this.cbxContractID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxContractID.FormattingEnabled = true;
            this.cbxContractID.Location = new System.Drawing.Point(121, 37);
            this.cbxContractID.Name = "cbxContractID";
            this.cbxContractID.Size = new System.Drawing.Size(109, 20);
            this.cbxContractID.TabIndex = 19;
            this.cbxContractID.SelectedIndexChanged += new System.EventHandler(this.cbxContractID_SelectedIndexChanged);
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(379, 32);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(112, 20);
            this.cbxSupplierID.TabIndex = 18;
            this.cbxSupplierID.SelectedIndexChanged += new System.EventHandler(this.cbxSupplierID_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "协议编号：";
            // 
            // cbxFactory
            // 
            this.cbxFactory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFactory.FormattingEnabled = true;
            this.cbxFactory.Location = new System.Drawing.Point(608, 32);
            this.cbxFactory.Name = "cbxFactory";
            this.cbxFactory.Size = new System.Drawing.Size(114, 20);
            this.cbxFactory.TabIndex = 16;
            this.cbxFactory.SelectedIndexChanged += new System.EventHandler(this.cbxFactory_SelectedIndexChanged);
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(608, 71);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(110, 21);
            this.dtpEndTime.TabIndex = 13;
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(381, 71);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(110, 21);
            this.dtpBeginTime.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(513, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "协议截止时间：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(539, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 8;
            this.label13.Text = "工厂：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(272, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "协议开始时间：";
            // 
            // cbxAgreementType
            // 
            this.cbxAgreementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAgreementType.FormattingEnabled = true;
            this.cbxAgreementType.Items.AddRange(new object[] {
            "数量合同",
            "价值合同",
            "自助采购"});
            this.cbxAgreementType.Location = new System.Drawing.Point(120, 72);
            this.cbxAgreementType.Name = "cbxAgreementType";
            this.cbxAgreementType.Size = new System.Drawing.Size(110, 20);
            this.cbxAgreementType.TabIndex = 3;
            this.cbxAgreementType.SelectedIndexChanged += new System.EventHandler(this.cbxAgreementType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "协议类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(272, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商编号：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(612, 34);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(110, 21);
            this.dtpCreateTime.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(537, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "协议日期：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxBuyerOrgnization);
            this.groupBox2.Controls.Add(this.cbxBuyerGroup);
            this.groupBox2.Controls.Add(this.cbxStorageLocation);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtpCreateTime);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(4, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(753, 112);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "参考数据";
            // 
            // cbxBuyerOrgnization
            // 
            this.cbxBuyerOrgnization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBuyerOrgnization.FormattingEnabled = true;
            this.cbxBuyerOrgnization.Location = new System.Drawing.Point(384, 31);
            this.cbxBuyerOrgnization.Name = "cbxBuyerOrgnization";
            this.cbxBuyerOrgnization.Size = new System.Drawing.Size(121, 20);
            this.cbxBuyerOrgnization.TabIndex = 19;
            this.cbxBuyerOrgnization.SelectedIndexChanged += new System.EventHandler(this.cbxBuyerOrgnization_SelectedIndexChanged);
            // 
            // cbxBuyerGroup
            // 
            this.cbxBuyerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBuyerGroup.FormattingEnabled = true;
            this.cbxBuyerGroup.Location = new System.Drawing.Point(120, 31);
            this.cbxBuyerGroup.Name = "cbxBuyerGroup";
            this.cbxBuyerGroup.Size = new System.Drawing.Size(112, 20);
            this.cbxBuyerGroup.TabIndex = 18;
            this.cbxBuyerGroup.SelectedIndexChanged += new System.EventHandler(this.cbxBuyerGroup_SelectedIndexChanged);
            // 
            // cbxStorageLocation
            // 
            this.cbxStorageLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStorageLocation.FormattingEnabled = true;
            this.cbxStorageLocation.Location = new System.Drawing.Point(120, 71);
            this.cbxStorageLocation.Name = "cbxStorageLocation";
            this.cbxStorageLocation.Size = new System.Drawing.Size(114, 20);
            this.cbxStorageLocation.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(296, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "采购组织：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "采购组：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(53, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 9;
            this.label14.Text = "库存地：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "付款条件：";
            // 
            // btnNextPage
            // 
            this.btnNextPage.Location = new System.Drawing.Point(682, 478);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(75, 23);
            this.btnNextPage.TabIndex = 2;
            this.btnNextPage.Text = "下一页";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tblTermText);
            this.groupBox3.Controls.Add(this.cbTradeClause);
            this.groupBox3.Controls.Add(this.cbPaymentClause);
            this.groupBox3.Controls.Add(this.cbxCurrencyType);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.tbTargetValue);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.tbDays3);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.tbDiscountRate2);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.tbDays2);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.tbDiscountRate1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.tbDays1);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(4, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(753, 195);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "交货和支付条件";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // tblTermText
            // 
            this.tblTermText.Location = new System.Drawing.Point(541, 72);
            this.tblTermText.Name = "tblTermText";
            this.tblTermText.Size = new System.Drawing.Size(147, 21);
            this.tblTermText.TabIndex = 28;
            // 
            // cbTradeClause
            // 
            this.cbTradeClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTradeClause.FormattingEnabled = true;
            this.cbTradeClause.Location = new System.Drawing.Point(459, 72);
            this.cbTradeClause.Name = "cbTradeClause";
            this.cbTradeClause.Size = new System.Drawing.Size(46, 20);
            this.cbTradeClause.TabIndex = 27;
            this.cbTradeClause.SelectedIndexChanged += new System.EventHandler(this.cbTradeClause_SelectedIndexChanged);
            // 
            // cbPaymentClause
            // 
            this.cbPaymentClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaymentClause.FormattingEnabled = true;
            this.cbPaymentClause.Location = new System.Drawing.Point(120, 36);
            this.cbPaymentClause.Name = "cbPaymentClause";
            this.cbPaymentClause.Size = new System.Drawing.Size(121, 20);
            this.cbPaymentClause.TabIndex = 26;
            this.cbPaymentClause.SelectedIndexChanged += new System.EventHandler(this.cbPaymentClause_SelectedIndexChanged);
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(643, 35);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(45, 20);
            this.cbxCurrencyType.TabIndex = 25;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(602, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 24;
            this.label27.Text = "货币：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(382, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 21;
            this.label26.Text = "交付条件：";
            // 
            // tbTargetValue
            // 
            this.tbTargetValue.Location = new System.Drawing.Point(460, 35);
            this.tbTargetValue.Name = "tbTargetValue";
            this.tbTargetValue.Size = new System.Drawing.Size(120, 21);
            this.tbTargetValue.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(381, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 16;
            this.label24.Text = "目标值：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(224, 152);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 15;
            this.label23.Text = "净额";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(192, 152);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 14;
            this.label21.Text = "天";
            // 
            // tbDays3
            // 
            this.tbDays3.Location = new System.Drawing.Point(121, 149);
            this.tbDays3.Name = "tbDays3";
            this.tbDays3.Size = new System.Drawing.Size(60, 21);
            this.tbDays3.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(41, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 12;
            this.label22.Text = "付款：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(279, 115);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 12);
            this.label18.TabIndex = 11;
            this.label18.Text = "%";
            // 
            // tbDiscountRate2
            // 
            this.tbDiscountRate2.Location = new System.Drawing.Point(226, 112);
            this.tbDiscountRate2.Name = "tbDiscountRate2";
            this.tbDiscountRate2.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate2.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(192, 115);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 9;
            this.label19.Text = "天";
            // 
            // tbDays2
            // 
            this.tbDays2.Location = new System.Drawing.Point(121, 112);
            this.tbDays2.Name = "tbDays2";
            this.tbDays2.Size = new System.Drawing.Size(60, 21);
            this.tbDays2.TabIndex = 8;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(41, 115);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 7;
            this.label20.Text = "付款：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(279, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 12);
            this.label17.TabIndex = 6;
            this.label17.Text = "%";
            // 
            // tbDiscountRate1
            // 
            this.tbDiscountRate1.Location = new System.Drawing.Point(226, 72);
            this.tbDiscountRate1.Name = "tbDiscountRate1";
            this.tbDiscountRate1.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate1.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(192, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 4;
            this.label16.Text = "天";
            // 
            // tbDays1
            // 
            this.tbDays1.Location = new System.Drawing.Point(121, 72);
            this.tbDays1.Name = "tbDays1";
            this.tbDays1.Size = new System.Drawing.Size(60, 21);
            this.tbDays1.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(41, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 2;
            this.label15.Text = "付款：";
            // 
            // AgreementContractForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 516);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnNextPage);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.Name = "AgreementContractForm";
            this.Text = "创建合同 初始数据";
            this.Load += new System.EventHandler(this.AgreementContractForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxAgreementType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbTargetValue;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbDays3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbDiscountRate2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbDays2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbDiscountRate1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbDays1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.ComboBox cbxFactory;
        private System.Windows.Forms.ComboBox cbxStorageLocation;
        private System.Windows.Forms.ComboBox cbPaymentClause;
        private System.Windows.Forms.ComboBox cbxBuyerGroup;
        private System.Windows.Forms.ComboBox cbxBuyerOrgnization;
        private System.Windows.Forms.ComboBox cbTradeClause;
        private System.Windows.Forms.TextBox tblTermText;
        private System.Windows.Forms.ComboBox cbxContractID;

    }
}