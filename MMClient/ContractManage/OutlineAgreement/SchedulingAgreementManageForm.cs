﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class SchedulingAgreementManageForm : DockContent
    {
        //记录查询结果
        //计划协议
        List<Scheduling_Agreement> schedulingAgreementList = new List<Scheduling_Agreement>();
        //查询工具
        SchedulingAgreementBLL schedulingAgreementTool = new SchedulingAgreementBLL();

        public SchedulingAgreementManageForm()
        {
            InitializeComponent();

            initialForm();
        }

        /// <summary>
        /// 初始化form
        /// </summary>
        private void initialForm() { 
            //类型
            this.cbxSchedulingAgreementType.SelectedIndex = 0;
            //状态
            this.cbxStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// 删除计划协议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            List<string> agreementIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvSchedulingAgreement.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    agreementIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (agreementIDList.Count == 0)
            {
                MessageBox.Show("请选择要删除的计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var confirmResult = MessageBox.Show("确定删除选定的计划协议", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Cancel)
            {
                return;
            }

            int deleteResult = schedulingAgreementTool.deleteSchedulingAgreementList(agreementIDList);

            if (deleteResult > 0)
            {
                MessageBox.Show("删除成功");
            }
            else
            {
                MessageBox.Show("删除失败");
            }

            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 显示计划协议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            List<string> agreementIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvSchedulingAgreement.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    agreementIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (agreementIDList.Count == 0)
            {
                MessageBox.Show("请选择要查看的计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (agreementIDList.Count > 1)
            {
                MessageBox.Show("不要同时选择多个计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string agreementID = agreementIDList[0];
            Scheduling_Agreement agreement = null;
            foreach (Scheduling_Agreement con in schedulingAgreementList)
            {
                if (con.Scheduling_Agreement_ID.Equals(agreementID))
                {
                    agreement = con;
                    break;
                }
            }
            if (agreement != null)
            {
                //显示预览的计划协议
                SchedulingAgreementForm displayForm = new SchedulingAgreementForm(agreement, "display");
                UserUI userUI = SingletonUserUI.getUserUI();
                userUI.displayOnDockPanel(displayForm);
            }
        }

        /// <summary>
        /// 编辑计划协议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbEdit_Click(object sender, EventArgs e)
        {
            List<string> agreementIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvSchedulingAgreement.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    agreementIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (agreementIDList.Count == 0)
            {
                MessageBox.Show("请选择要编辑的计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (agreementIDList.Count > 1)
            {
                MessageBox.Show("不要同时选择多个计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string agreementID = agreementIDList[0];
            Scheduling_Agreement agreement = null;
            foreach (Scheduling_Agreement con in schedulingAgreementList)
            {
                if (con.Scheduling_Agreement_ID.Equals(agreementID))
                {
                    agreement = con;
                    break;
                }
            }
            if (agreement != null)
            {
                //显示预览的计划协议
                SchedulingAgreementForm displayForm = new SchedulingAgreementForm(agreement, "edit");
                UserUI userUI = SingletonUserUI.getUserUI();
                userUI.displayOnDockPanel(displayForm);
            }
        }

        /// <summary>
        /// 查询操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //从所有的计划协议信息中搜索满足当前条件的计划协议信息
            //先看开始时间和截止时间
            DateTime _beginTime = this.dtpBeginTime.Value;
            DateTime _endTime = this.dtpEndTime.Value;

            //真正的起始时间设置为当天的0点0分0秒
            //真正的截止时间设置为当天的23点59分59秒
            DateTime beginTime = new DateTime(_beginTime.Year,_beginTime.Month,_beginTime.Day,0,0,0);
            DateTime endTime = new DateTime(_endTime.Year,_endTime.Month,_endTime.Day,23,59,59);

            if (beginTime > endTime)
            {
                MessageBox.Show("请选择正确的起止日期", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string agreementID = this.tbSchedulingAgreementID.Text.Trim();
            string supplierID = this.tbSupplierID.Text.Trim();
            string agreementType = this.cbxSchedulingAgreementType.SelectedItem.ToString();
            if (agreementType.Equals("全部"))
                agreementType = "";
            string agreementStatus = this.cbxStatus.SelectedItem.ToString();
            if (agreementStatus.Equals("全部"))
                agreementStatus = "";

            Dictionary<string, object> conditions = new Dictionary<string, object>();
            conditions.Add("Scheduling_Agreement_ID", agreementID);
            conditions.Add("Supplier_ID", supplierID);
            conditions.Add("Scheduling_Agreement_Type", agreementType);
            conditions.Add("Status", agreementStatus);

            schedulingAgreementList = schedulingAgreementTool.getSchedulingAgreementByCondition(
                conditions, beginTime, endTime);

            displayAgreementList(schedulingAgreementList);
        }

        /// <summary>
        /// 展示查询结果
        /// </summary>
        /// <param name="agreementList"></param>
        private void displayAgreementList(List<Scheduling_Agreement> agreementList)
        {
            //clear
            this.dgvSchedulingAgreement.Rows.Clear();
            int i = 0;
            foreach (Scheduling_Agreement agreement in agreementList)
            {
                this.dgvSchedulingAgreement.Rows.Add();
                DataGridViewRow currentRow = this.dgvSchedulingAgreement.Rows[i];
                currentRow.Cells[1].Value = agreement.Scheduling_Agreement_ID;
                currentRow.Cells[2].Value = agreement.Scheduling_Agreement_Type;
                currentRow.Cells[3].Value = agreement.Create_Time;
                currentRow.Cells[4].Value = agreement.Creater_ID;
                currentRow.Cells[5].Value = agreement.Supplier_ID;
                currentRow.Cells[6].Value = agreement.Status;

                i++;
            }
        }

        /// <summary>
        /// 新建计划协议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbNew_Click(object sender, EventArgs e)
        {
            SchedulingAgreementForm schedulingAgreementForm = new SchedulingAgreementForm();
            UserUI userUI = SingletonUserUI.getUserUI();
            userUI.displayOnDockPanel(schedulingAgreementForm);
        }

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbCheck_Click(object sender, EventArgs e)
        {
            List<Scheduling_Agreement> agreementIDList = new List<Scheduling_Agreement>();

            //schedulingAgreementList中保存的记录顺序和显示的记录顺序应该是一致的
            int i = 0;
            foreach (DataGridViewRow row in this.dgvSchedulingAgreement.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    string status = row.Cells[6].Value.ToString();
                    if (!status.Equals("待审核"))
                    {
                        MessageBox.Show("只有处于待审核的计划协议才能执行审核操作", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    agreementIDList.Add(schedulingAgreementList[i]);
                }

                i++;
            }

            if (agreementIDList.Count == 0)
            {
                MessageBox.Show("请选择要审核的计划协议", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //确认
            var result = MessageBox.Show("确认审核计划协议？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
            {
                return;
            }

            //审核所有的计划协议
            int num = 0;
            foreach (Scheduling_Agreement agreement in agreementIDList)
            {
                agreement.Status = "待供应商确认";
                num += schedulingAgreementTool.updateSchedulingAgreement(agreement.Scheduling_Agreement_ID,
                    agreement);
            }
            if (num > 0)
            {
                MessageBox.Show("审核成功");
            }
            else
            {
                MessageBox.Show("审核失败");
            }

            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 重载方法，防止在子控件获得焦点时父控件的滚动条自动滚动
        /// 本方法不需要手动调用
        /// </summary>
        /// <param name="activeControl">子控件</param>
        /// <returns>位置</returns>
        protected override Point ScrollToControl(Control activeControl)
        {
            return this.AutoScrollPosition;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            this.btnSearch.PerformClick();
        }
    }
}
