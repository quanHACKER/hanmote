﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.ContractManage.OutlineAgreement;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class AgreementContractMonitorSettingForm : DockContent
    {
        private Agreement_Contract agreementContract;           //协议合同对象
        
        public AgreementContractMonitorSettingForm(
            Agreement_Contract _agreementContract)
        {
            InitializeComponent();
            this.agreementContract = _agreementContract;

            initialForm();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initialForm() { 
            //抬头数据
            this.tbContractID.Text = agreementContract.Agreement_Contract_ID;
            this.tbContractType.Text = agreementContract.Agreement_Contract_Type;
            this.tbContractVersion.Text = Convert.ToString(agreementContract.Agreement_Contract_Version);
            this.tbContractValue.Text = Convert.ToString(agreementContract.Target_Value);
            //数量要读取来汇总
            //this.tbContractQuantity.Text
            this.dtpCreateTime.Value = agreementContract.Create_Time;
            this.dtpBeginTime.Value = agreementContract.Begin_Time;
            this.dtpEndTime.Value = agreementContract.End_Time;

            //价值合同不需要设置数量监控
            if (this.agreementContract.Agreement_Contract_Type.Equals("价值合同"))
            {
                this.cbEnableQuantityMonitor.Enabled = false;
                this.tbAlertQuantity.ReadOnly = true;
            }
            else { 
                //数量合同不需要设置价值监控
                this.cbEnableValueMonitor.Enabled = false;
                this.tbAlertValue.ReadOnly = true;
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

        }
    }
}
