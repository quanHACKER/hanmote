﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractExecutionMonitor
{
    public partial class ContactExecutionMonitorForm : DockContent
    {
        public ContactExecutionMonitorForm()
        {
            InitializeComponent();
        }

        private void ContactExecutionMonitorForm_Load(object sender, EventArgs e)
        {
            //下面代码无用，为了写论文截图
            //默认添加些数据
            DataTable dt = new DataTable();
            dt.Columns.Add("序号", typeof(string));
            dt.Columns.Add("合同编号", typeof(string));
            dt.Columns.Add("合同名称", typeof(string));
            dt.Columns.Add("合同类型", typeof(string));
            dt.Columns.Add("供应商编号", typeof(string));
            dt.Columns.Add("合同金额", typeof(string));
            dt.Columns.Add("签约日期", typeof(string));
            dt.Columns.Add("到期日", typeof(string));
            dt.Columns.Add("供应商", typeof(string));
            dt.Columns.Add("供货地点", typeof(string));
            dt.Columns.Add("单价", typeof(string));
            dt.Columns.Add("库存数量", typeof(string));

            dt.Rows.Add(new object[] { "001", "中性笔", "文具", "晨光文具公司", "武汉", "2.0(￥)", "234" });
            dt.Rows.Add(new object[] { "002", "A4书写纸", "文具", "晨光文具公司", "武汉", "1.0(￥)", "500" });
            dt.Rows.Add(new object[] { "003", "橡皮", "文具", "晨光文具公司", "武汉", "1.0(￥)", "100" });

            //this.gridControl1.DataSource = dt;
            //this.gridView1.PopulateColumns();

            this.button1.PerformClick();
        }
    }
}
