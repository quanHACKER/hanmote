﻿namespace MMClient.ContractManage.QuotaArrangement
{
    partial class QuotaArrangementItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFactoryName = new System.Windows.Forms.Label();
            this.lblMaterialName = new System.Windows.Forms.Label();
            this.tbFactoryID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMaterialID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbQuotaArrangementID = new System.Windows.Forms.TextBox();
            this.tbOUn = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbMinQuantityResolve = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.tbCreatorID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.dgvDisplay = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Item_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.S = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PP1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quota_Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quota_Percentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Allocate_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Max_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quota_Base_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Max_Batch_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Min_Batch_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.One_Time = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Max_Delivery_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFactoryName
            // 
            this.lblFactoryName.AutoSize = true;
            this.lblFactoryName.Location = new System.Drawing.Point(391, 62);
            this.lblFactoryName.Name = "lblFactoryName";
            this.lblFactoryName.Size = new System.Drawing.Size(53, 12);
            this.lblFactoryName.TabIndex = 15;
            this.lblFactoryName.Text = "工厂名称";
            // 
            // lblMaterialName
            // 
            this.lblMaterialName.AutoSize = true;
            this.lblMaterialName.Location = new System.Drawing.Point(391, 25);
            this.lblMaterialName.Name = "lblMaterialName";
            this.lblMaterialName.Size = new System.Drawing.Size(53, 12);
            this.lblMaterialName.TabIndex = 14;
            this.lblMaterialName.Text = "物料名称";
            // 
            // tbFactoryID
            // 
            this.tbFactoryID.Location = new System.Drawing.Point(147, 59);
            this.tbFactoryID.Name = "tbFactoryID";
            this.tbFactoryID.ReadOnly = true;
            this.tbFactoryID.Size = new System.Drawing.Size(148, 21);
            this.tbFactoryID.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "工厂：";
            // 
            // tbMaterialID
            // 
            this.tbMaterialID.Location = new System.Drawing.Point(147, 22);
            this.tbMaterialID.Name = "tbMaterialID";
            this.tbMaterialID.ReadOnly = true;
            this.tbMaterialID.Size = new System.Drawing.Size(148, 21);
            this.tbMaterialID.TabIndex = 11;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 25);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "物料：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "配额协议编号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(368, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "基本计量单位：";
            // 
            // tbQuotaArrangementID
            // 
            this.tbQuotaArrangementID.Location = new System.Drawing.Point(147, 97);
            this.tbQuotaArrangementID.Name = "tbQuotaArrangementID";
            this.tbQuotaArrangementID.ReadOnly = true;
            this.tbQuotaArrangementID.Size = new System.Drawing.Size(148, 21);
            this.tbQuotaArrangementID.TabIndex = 18;
            // 
            // tbOUn
            // 
            this.tbOUn.Location = new System.Drawing.Point(463, 97);
            this.tbOUn.Name = "tbOUn";
            this.tbOUn.ReadOnly = true;
            this.tbOUn.Size = new System.Drawing.Size(148, 21);
            this.tbOUn.TabIndex = 19;
            this.tbOUn.Text = "吨";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "有效起止日期：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(404, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "有效至：";
            // 
            // tbMinQuantityResolve
            // 
            this.tbMinQuantityResolve.Location = new System.Drawing.Point(463, 173);
            this.tbMinQuantityResolve.Name = "tbMinQuantityResolve";
            this.tbMinQuantityResolve.ReadOnly = true;
            this.tbMinQuantityResolve.Size = new System.Drawing.Size(148, 21);
            this.tbMinQuantityResolve.TabIndex = 25;
            this.tbMinQuantityResolve.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(370, 176);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "最小数量分解：";
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Enabled = false;
            this.dtpBeginTime.Location = new System.Drawing.Point(147, 131);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(148, 21);
            this.dtpBeginTime.TabIndex = 26;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Enabled = false;
            this.dtpEndTime.Location = new System.Drawing.Point(463, 131);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(148, 21);
            this.dtpEndTime.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(88, 214);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 28;
            this.label10.Text = "创建人：";
            // 
            // tbCreatorID
            // 
            this.tbCreatorID.Location = new System.Drawing.Point(147, 211);
            this.tbCreatorID.Name = "tbCreatorID";
            this.tbCreatorID.ReadOnly = true;
            this.tbCreatorID.Size = new System.Drawing.Size(148, 21);
            this.tbCreatorID.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(391, 214);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 30;
            this.label11.Text = "创建日期：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(463, 208);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(148, 21);
            this.dtpCreateTime.TabIndex = 31;
            // 
            // dgvDisplay
            // 
            this.dgvDisplay.AllowUserToAddRows = false;
            this.dgvDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item_Num,
            this.P,
            this.S,
            this.Supplier_ID,
            this.PP1,
            this.Quota_Value,
            this.Quota_Percentage,
            this.Allocate_Amount,
            this.Max_Amount,
            this.Quota_Base_Amount,
            this.Max_Batch_Size,
            this.Min_Batch_Size,
            this.RPro,
            this.One_Time,
            this.Max_Delivery_Amount,
            this.Period,
            this.Pr});
            this.dgvDisplay.Location = new System.Drawing.Point(12, 277);
            this.dgvDisplay.Name = "dgvDisplay";
            this.dgvDisplay.RowHeadersVisible = false;
            this.dgvDisplay.RowTemplate.Height = 23;
            this.dgvDisplay.Size = new System.Drawing.Size(1037, 304);
            this.dgvDisplay.TabIndex = 32;
            this.dgvDisplay.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDisplay_CellContentClick);
            this.dgvDisplay.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDisplay_CellValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 252);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 33;
            this.label12.Text = "配额分配项目";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(231, 247);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 34;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(350, 247);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(463, 247);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 36;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "项目";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "P";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 40;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "S";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 40;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "供应商";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "PP1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "配额";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "百分比";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "分配的数量";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "最大数量";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "配额基本数量";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "最大批量尺寸";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "最小批量尺寸";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "RPro";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "最大交付数量";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "周期";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Pr";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // Item_Num
            // 
            this.Item_Num.HeaderText = "项目";
            this.Item_Num.Name = "Item_Num";
            this.Item_Num.ReadOnly = true;
            this.Item_Num.Width = 60;
            // 
            // P
            // 
            this.P.HeaderText = "P";
            this.P.Name = "P";
            this.P.Visible = false;
            this.P.Width = 40;
            // 
            // S
            // 
            this.S.HeaderText = "S";
            this.S.Name = "S";
            this.S.Visible = false;
            this.S.Width = 40;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.HeaderText = "供应商";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // PP1
            // 
            this.PP1.HeaderText = "PP1";
            this.PP1.Name = "PP1";
            this.PP1.Visible = false;
            // 
            // Quota_Value
            // 
            this.Quota_Value.HeaderText = "配额";
            this.Quota_Value.Name = "Quota_Value";
            // 
            // Quota_Percentage
            // 
            this.Quota_Percentage.HeaderText = "百分比";
            this.Quota_Percentage.Name = "Quota_Percentage";
            this.Quota_Percentage.ReadOnly = true;
            // 
            // Allocate_Amount
            // 
            this.Allocate_Amount.HeaderText = "分配的数量";
            this.Allocate_Amount.Name = "Allocate_Amount";
            // 
            // Max_Amount
            // 
            this.Max_Amount.HeaderText = "最大数量";
            this.Max_Amount.Name = "Max_Amount";
            // 
            // Quota_Base_Amount
            // 
            this.Quota_Base_Amount.HeaderText = "配额基本数量";
            this.Quota_Base_Amount.Name = "Quota_Base_Amount";
            // 
            // Max_Batch_Size
            // 
            this.Max_Batch_Size.HeaderText = "最大订货批量";
            this.Max_Batch_Size.Name = "Max_Batch_Size";
            // 
            // Min_Batch_Size
            // 
            this.Min_Batch_Size.HeaderText = "最小订货批量";
            this.Min_Batch_Size.Name = "Min_Batch_Size";
            // 
            // RPro
            // 
            this.RPro.HeaderText = "RPro";
            this.RPro.Name = "RPro";
            // 
            // One_Time
            // 
            this.One_Time.HeaderText = "1x";
            this.One_Time.Name = "One_Time";
            this.One_Time.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.One_Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Max_Delivery_Amount
            // 
            this.Max_Delivery_Amount.HeaderText = "最大交付数量";
            this.Max_Delivery_Amount.Name = "Max_Delivery_Amount";
            // 
            // Period
            // 
            this.Period.HeaderText = "周期";
            this.Period.Name = "Period";
            // 
            // Pr
            // 
            this.Pr.HeaderText = "Pr";
            this.Pr.Name = "Pr";
            // 
            // QuotaArrangementItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 585);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dgvDisplay);
            this.Controls.Add(this.dtpCreateTime);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbCreatorID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dtpEndTime);
            this.Controls.Add(this.dtpBeginTime);
            this.Controls.Add(this.tbMinQuantityResolve);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbOUn);
            this.Controls.Add(this.tbQuotaArrangementID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblFactoryName);
            this.Controls.Add(this.lblMaterialName);
            this.Controls.Add(this.tbFactoryID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbMaterialID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "QuotaArrangementItemForm";
            this.Text = "配额安排 主界面";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuotaArrangementItemForm_FormClosed);
            this.Load += new System.EventHandler(this.QuotaArrangementItemForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFactoryName;
        private System.Windows.Forms.Label lblMaterialName;
        private System.Windows.Forms.TextBox tbFactoryID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMaterialID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbQuotaArrangementID;
        private System.Windows.Forms.TextBox tbOUn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbMinQuantityResolve;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbCreatorID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.DataGridView dgvDisplay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn P;
        private System.Windows.Forms.DataGridViewTextBoxColumn S;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quota_Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quota_Percentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Allocate_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quota_Base_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max_Batch_Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn Min_Batch_Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn RPro;
        private System.Windows.Forms.DataGridViewCheckBoxColumn One_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max_Delivery_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Period;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr;
    }
}