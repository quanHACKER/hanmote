﻿namespace MMClient.ContractManage.QuotaArrangement
{
    partial class QuotaArrangementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFactoryID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMaterialID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaterialName = new System.Windows.Forms.Label();
            this.lblFactoryName = new System.Windows.Forms.Label();
            this.dgvQuotaArrangement = new System.Windows.Forms.DataGridView();
            this.Quota_Arrangement_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Min_Quantity_Resolve = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuotaArrangement)).BeginInit();
            this.SuspendLayout();
            // 
            // tbFactoryID
            // 
            this.tbFactoryID.Location = new System.Drawing.Point(119, 66);
            this.tbFactoryID.Name = "tbFactoryID";
            this.tbFactoryID.ReadOnly = true;
            this.tbFactoryID.Size = new System.Drawing.Size(164, 21);
            this.tbFactoryID.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "工厂：";
            // 
            // tbMaterialID
            // 
            this.tbMaterialID.Location = new System.Drawing.Point(119, 23);
            this.tbMaterialID.Name = "tbMaterialID";
            this.tbMaterialID.ReadOnly = true;
            this.tbMaterialID.Size = new System.Drawing.Size(164, 21);
            this.tbMaterialID.TabIndex = 5;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 26);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "物料：";
            // 
            // lblMaterialName
            // 
            this.lblMaterialName.AutoSize = true;
            this.lblMaterialName.Location = new System.Drawing.Point(341, 26);
            this.lblMaterialName.Name = "lblMaterialName";
            this.lblMaterialName.Size = new System.Drawing.Size(53, 12);
            this.lblMaterialName.TabIndex = 8;
            this.lblMaterialName.Text = "物料名称";
            this.lblMaterialName.Visible = false;
            // 
            // lblFactoryName
            // 
            this.lblFactoryName.AutoSize = true;
            this.lblFactoryName.Location = new System.Drawing.Point(341, 69);
            this.lblFactoryName.Name = "lblFactoryName";
            this.lblFactoryName.Size = new System.Drawing.Size(53, 12);
            this.lblFactoryName.TabIndex = 9;
            this.lblFactoryName.Text = "工厂名称";
            this.lblFactoryName.Visible = false;
            // 
            // dgvQuotaArrangement
            // 
            this.dgvQuotaArrangement.AllowUserToAddRows = false;
            this.dgvQuotaArrangement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuotaArrangement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quota_Arrangement_ID,
            this.Min_Quantity_Resolve});
            this.dgvQuotaArrangement.Location = new System.Drawing.Point(12, 155);
            this.dgvQuotaArrangement.Name = "dgvQuotaArrangement";
            this.dgvQuotaArrangement.RowHeadersVisible = false;
            this.dgvQuotaArrangement.RowTemplate.Height = 23;
            this.dgvQuotaArrangement.Size = new System.Drawing.Size(542, 392);
            this.dgvQuotaArrangement.TabIndex = 10;
            // 
            // Quota_Arrangement_ID
            // 
            this.Quota_Arrangement_ID.HeaderText = "配额协议编号";
            this.Quota_Arrangement_ID.Name = "Quota_Arrangement_ID";
            this.Quota_Arrangement_ID.Width = 118;
            // 
            // Min_Quantity_Resolve
            // 
            this.Min_Quantity_Resolve.HeaderText = "最小数量拆分";
            this.Min_Quantity_Resolve.Name = "Min_Quantity_Resolve";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(35, 126);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(219, 126);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 12;
            this.btnPreview.Text = "查看";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(127, 126);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // QuotaArrangementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 559);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvQuotaArrangement);
            this.Controls.Add(this.lblFactoryName);
            this.Controls.Add(this.lblMaterialName);
            this.Controls.Add(this.tbFactoryID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbMaterialID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "QuotaArrangementForm";
            this.Text = "配额安排 总览";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuotaArrangementForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuotaArrangement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFactoryID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMaterialID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMaterialName;
        private System.Windows.Forms.Label lblFactoryName;
        private System.Windows.Forms.DataGridView dgvQuotaArrangement;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quota_Arrangement_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Min_Quantity_Resolve;
    }
}