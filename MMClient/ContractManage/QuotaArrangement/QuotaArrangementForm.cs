﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Common.CommonControls;
using Lib.Model.ContractManage.QuotaArrangement;
using Lib.Bll.ContractManageBLL.QuotaArrangementBLL;

namespace MMClient.ContractManage.QuotaArrangement
{
    public partial class QuotaArrangementForm : DockContent
    {
        private string materialID;                              //物料编号
        private string materianName;                            //物料名称
        private string OUn;                                     //物料计量单位
        private string factoryID;                               //工厂编号
        private string factoryName;                             //工厂名称
        private List<Quota_Arrangement> quotaArrangementList;   //配额协议列表(因为一些信息不需要展示，所有这里的List和DataGridview里面的内容实时一致)
        private QuotaArrangementBLL quotaArrangementTool =
            new QuotaArrangementBLL();                          //配额协议操作工具
        private QuotaArrangementInitialForm parentForm;         //父窗体
        private Quota_Arrangement newQuotaArrangement = null;   //新配额协议
        private QuotaArrangementTool tool = new QuotaArrangementTool();
        List<Quota_Arrangement_Item> itemList = new List<Quota_Arrangement_Item>();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="_materialID"></param>
        /// <param name="_materialName"></param>
        /// <param name="_factoryID"></param>
        /// <param name="_factoryName"></param>
        /// <param name="_quotaArrangementList"></param>
        public QuotaArrangementForm(string _materialID,
            string _materialName,
            string _OUn,
            string _factoryID,
            string _factoryName,
            List<Quota_Arrangement> _quotaArrangementList,
            QuotaArrangementInitialForm _parentForm)
        {
            InitializeComponent();

            this.materialID = _materialID;
            this.materianName = _materialName;
            this.OUn = _OUn;
            this.factoryID = _factoryID;
            this.factoryName = _factoryName;
            this.quotaArrangementList = _quotaArrangementList;
            this.parentForm = _parentForm;

            initialForm();
        }

        /// <summary>
        /// 初始化Form
        /// </summary>
        private void initialForm() {
            //插入"有效开始日"和"有效截止日"两个DateTimePickerColumn
            CalendarColumn calColumn1 = new CalendarColumn();
            calColumn1.Name = "Begin_Time";
            calColumn1.HeaderText = "有效开始日";
            calColumn1.Width = 160;
            this.dgvQuotaArrangement.Columns.Insert(1, calColumn1);
            CalendarColumn calColumn2 = new CalendarColumn();
            calColumn2.Name = "End_Time";
            calColumn2.HeaderText = "有效截止日";
            calColumn2.Width = 160;
            this.dgvQuotaArrangement.Columns.Insert(2, calColumn2);

            //写入抬头数据
            this.tbMaterialID.Text = materialID;
            this.lblMaterialName.Text = materianName;
            this.tbFactoryID.Text = factoryID;
            this.lblFactoryName.Text = factoryName;

            //填充数据
            displayQuotaArrangementList(quotaArrangementList);
        }

        /// <summary>
        /// 显示配额协议List
        /// </summary>
        /// <param name="quotaArrangementList"></param>
        private void displayQuotaArrangementList(List<Quota_Arrangement> quotaArrangementList) {
            //清空原有项
            this.dgvQuotaArrangement.Rows.Clear();
            foreach (Quota_Arrangement quota in quotaArrangementList) {
                this.dgvQuotaArrangement.Rows.Add();
                int newRowIndex = this.dgvQuotaArrangement.Rows.Count - 1;
                DataGridViewRow row = this.dgvQuotaArrangement.Rows[newRowIndex];
                row.Cells[0].Value = quota.Quota_Arrangement_ID;
                row.Cells[1].Value = quota.Begin_Time;
                row.Cells[2].Value = quota.End_Time;
                row.Cells[3].Value = quota.Min_Quantity_Resolve;
            }
        }

        /// <summary>
        /// 增加(只能增加一项)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (newQuotaArrangement != null) {
                MessageUtil.ShowError("新协议已增加");
                return;
            }
            this.dgvQuotaArrangement.Rows.Add();
            int rowIndex = this.dgvQuotaArrangement.Rows.Count - 1;
            DataGridViewRow row = this.dgvQuotaArrangement.Rows[rowIndex];
            //下面的配额协议ID可以添加个自动生成功能 
            //默认有效开始时间和有效截止时间为当前时间
            row.Cells[1].Value = System.DateTime.Now;
            row.Cells[2].Value = System.DateTime.Now;
            //当前操作时间+用户ID
            string dateTme_Now = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            string defaultID = dateTme_Now + "2015001";
            row.Cells[0].Value = defaultID;

            //新建Quota_Arrangement添加到List
            newQuotaArrangement = new Quota_Arrangement();
            newQuotaArrangement.Create_Time = System.DateTime.Now;
            newQuotaArrangement.Creator_ID = "2015001";
            newQuotaArrangement.Factory_ID = this.factoryID;
            newQuotaArrangement.Factory_Name = this.factoryName;
            newQuotaArrangement.Material_ID = this.materialID;
            newQuotaArrangement.Material_Name = this.materianName;
            newQuotaArrangement.OUn = this.OUn;
            quotaArrangementList.Add(newQuotaArrangement);

            #region
            /*****************************************************/
            List<string> suppliers = tool.getSupplierByQuota(this.materialID, this.factoryID);
            if (suppliers != null)
            {
                int count = suppliers.Count;
                for (int i = 0; i < count; i++)
                {
                    Quota_Arrangement_Item item = new Quota_Arrangement_Item();
                    item.Quota_Arrangement_ID = defaultID;
                    item.Item_Num = i + 1;
                    item.Material_ID = this.materialID;
                    item.Factory_ID = this.factoryID;
                    item.Supplier_ID = suppliers[i];
                    DataTable dt = tool.getBatchSizeByID(item.Supplier_ID);
                    if (dt != null)
                    {
                        if (Convert.IsDBNull(dt.Rows[0][0]) || dt.Rows[0][0].ToString().Trim().Equals(""))
                        {
                            item.Max_Batch_Size = 0;
                        }
                        else
                        {
                            item.Max_Batch_Size = Convert.ToDouble(dt.Rows[0][0].ToString().Trim());
                        }
                        if (Convert.IsDBNull(dt.Rows[0][1]) || dt.Rows[0][1].ToString().Trim().Equals(""))
                        {
                            item.Min_Batch_Size = 0;
                        }
                        else
                        {
                            item.Min_Batch_Size = Convert.ToDouble(dt.Rows[0][1].ToString().Trim());
                        }
                    }
                    else
                    {
                        item.Max_Batch_Size = 0;
                        item.Min_Batch_Size = 0;
                    }
                    itemList.Add(item);
                }
            }
            //更新Quota_Arrangement_Item
            /*
            int no = quotaArrangementTool.addQuotaArrangementItemList(
                defaultID,
                itemList);
            if (no > 0)
            {
                MessageUtil.ShowTips("保存成功");
            }
            else
            {
                MessageUtil.ShowTips("保存失败");
            }
             * */
            /*******************************************/
            #endregion
        }

        /// <summary>
        /// 删除一项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvQuotaArrangement.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有可删除内容");
                return;
            }

            int rowIndex = this.dgvQuotaArrangement.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvQuotaArrangement.Rows.Count)
            {
                if (rowIndex == this.dgvQuotaArrangement.Rows.Count - 1
                    && newQuotaArrangement != null) {
                    
                    newQuotaArrangement = null;
                    this.dgvQuotaArrangement.Rows.RemoveAt(rowIndex);
                    this.quotaArrangementList.RemoveAt(rowIndex);
                    MessageUtil.ShowTips("删除成功");

                    return;
                }
                //删除数据库中的项
                Quota_Arrangement quota = quotaArrangementList[rowIndex];
                string quotaArrangementID = quota.Quota_Arrangement_ID;
                int num = quotaArrangementTool.deleteQuotaArrangement(quotaArrangementID);
                if (num > 0)
                {
                    //删除DataGridview中的项
                    this.dgvQuotaArrangement.Rows.RemoveAt(rowIndex);
                    //删除List中的项
                    this.quotaArrangementList.RemoveAt(rowIndex);
                    MessageUtil.ShowTips("删除成功");
                }
                else {
                    MessageUtil.ShowTips("删除失败");
                }
            }
        }

        /// <summary>
        /// 查看指定的配额协议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.dgvQuotaArrangement.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有可查看内容");
                return;
            }

            int rowIndex = this.dgvQuotaArrangement.CurrentCell.RowIndex;
            Quota_Arrangement quota;
            //判断是否是新添加的
            if (newQuotaArrangement != null
                && rowIndex == this.dgvQuotaArrangement.Rows.Count - 1)
            {

                DataGridViewRow row = this.dgvQuotaArrangement.
                    Rows[rowIndex];
                newQuotaArrangement.Begin_Time =
                    Convert.ToDateTime(row.Cells[1].Value);
                newQuotaArrangement.End_Time =
                    Convert.ToDateTime(row.Cells[2].Value);
                newQuotaArrangement.Quota_Arrangement_ID =
                    DataGridViewCellTool.getDataGridViewCellValueString(
                    row.Cells[0]);
                //新建的配额协议的Cal_Type是NotSet
                newQuotaArrangement.Cal_Type = "NotSet";
                //暂定
                newQuotaArrangement.Status = "执行中";

                if (newQuotaArrangement.Begin_Time >=
                    newQuotaArrangement.End_Time)
                {
                    MessageUtil.ShowError("请正确填写时间");
                    return;
                }
                newQuotaArrangement.Min_Quantity_Resolve =
                    DataGridViewCellTool.getDataGridViewCellValueDouble(
                    row.Cells[3]);
                quota = newQuotaArrangement;
                //把新的配额协议填写到数据库
                int num = quotaArrangementTool.addQuotaArrangement(newQuotaArrangement);
                if (num == 0) {
                    MessageUtil.ShowError("新协议保存失败");
                    return;
                }
            }
            else {
                quota = quotaArrangementList[rowIndex];
            }



            QuotaArrangementItemForm form = new QuotaArrangementItemForm(
                quota, this,itemList);
            this.Hide();
            SingletonUserUI.addToUserUI(form);
        }

        /// <summary>
        /// 关闭窗体后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuotaArrangementForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //释放父窗体
            if (this.parentForm != null || !this.parentForm.IsDisposed) {
                this.parentForm.Close();
            }
        }
    }
}
