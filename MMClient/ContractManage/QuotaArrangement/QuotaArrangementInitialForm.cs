﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Model.ContractManage.QuotaArrangement;
using Lib.Bll.ContractManageBLL.QuotaArrangementBLL;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.SqlServerDAL;

namespace MMClient.ContractManage.QuotaArrangement
{
    public partial class QuotaArrangementInitialForm : DockContent
    {
        private string materialID;                              //物料编号
        private string materialName;                            //物料名称
        private string OUn;                                     //物料计量单位
        private string factoryID;                               //工厂编号
        private string factoryName;                             //工厂名称
        private List<Quota_Arrangement> quotaAgreementList;     //配额协议列表
        private QuotaArrangementBLL quotaArrangementTool =
            new QuotaArrangementBLL();                          //配额业务处理工具
        private QuotaArrangementTool tool = new QuotaArrangementTool(); //辅助工具，不规范，应付检查
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private List<string> materialIDList = new List<string>();
        private List<string> factoryList = new List<string>();

        public QuotaArrangementInitialForm()
        {
            InitializeComponent();

            //绑定数据源
            #region   绑定物料数据源
            string sql = "select distinct(materialId) from SourceList";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt == null || dt.Rows.Count == 0)
            {
                MessageBox.Show("警告，请先维护源清单！");
                materialIDList = materialTool.GetAllMaterialID();
            }
            else
            {
                for(int i = 0;i < dt.Rows.Count;i++)
                {
                    materialIDList.Add(dt.Rows[i][0].ToString());
                }
            }
            #endregion
            //materialIDList = materialTool.GetAllMaterialID();
            //materialIDList = tool.getQuotaBasicInformation();
            this.cbxMaterialID.DataSource = materialIDList;
            //factoryList = generalTool.GetAllFactory();
            #region  绑定工厂数据源
            string sql1 = "select distinct(factoryId) from SourceList";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if(dt1 == null || dt1.Rows.Count == 0)
            {
                MessageBox.Show("警告，请先维护源清单！");
                factoryList = generalTool.GetAllFactory();
            }
            else
            {
                for(int i = 0;i < dt1.Rows.Count;i++)
                {
                    factoryList.Add(dt1.Rows[i][0].ToString());
                }
            }
            #endregion
            this.cbxFactoryID.DataSource = factoryList;
           // string materialId = this.cbxMaterialID.Text;
           // this.cbxFactoryID.DataSource = tool.getFactoryIdByMaterial(materialID); 
        }

        /// <summary>
        /// 点击跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.cbxMaterialID.Text.ToString().Trim().Equals("")
                || this.cbxFactoryID.Text.ToString().Trim().Equals(""))
            {
                MessageUtil.ShowError("请填写完整");
                return;
            }
            this.materialID = this.cbxMaterialID.Text.ToString().Trim();
            this.factoryID = this.cbxFactoryID.Text.ToString().Trim();
            //读取物料名称和工厂名称，确保物料ID和工厂ID的正确性
            //先填写默认值
            List<string> names = tool.getFactoryNameByFactoryId(this.cbxFactoryID.Text);
            if (names != null)
            {
                this.factoryName = names[0];
            }
            else
            {
                this.factoryName = "武汉工厂";
            }
            DataTable dt = tool.getMaterailInformationByMaterialId(this.cbxMaterialID.Text);
            if (dt != null)
            {
                this.materialName = dt.Rows[0][0].ToString();
                this.OUn = dt.Rows[0][1].ToString();
            }
            else
            {
                this.materialName = "A型口罩";
                this.OUn = "KG";
            }

            //读取所有的配额协议
            List<Quota_Arrangement> quotaArrangementList = 
                quotaArrangementTool.getQuotaArrangementList(
                this.materialID, this.factoryID);

            //跳转到下一屏幕
            QuotaArrangementForm form = new QuotaArrangementForm(
                this.materialID,
                this.materialName,
                this.OUn,
                this.factoryID,
                this.factoryName,
                quotaArrangementList,
                this);
            this.Hide();
            SingletonUserUI.addToUserUI(form);
        }

        private void cbxMaterialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string materialId = this.cbxMaterialID.Text;
            //this.cbxFactoryID.DataSource = tool.getFactoryIdByMaterial(materialID); 
        }
    }
}
