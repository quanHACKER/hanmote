﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.ContractDocs;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class AddNewContractTemplateSectionForm : DockContent
    {
        public Contract_Template_Item_Section ItemSection;
        public bool isSaved = false;
        public AddNewContractTemplateSectionForm(Contract_Template_Item_Section _ItemSection)
        {
            this.ItemSection = _ItemSection;
            //初始化
            initialForm();

            InitializeComponent();
        }

        /// <summary>
        /// 初始化窗体
        /// </summary>
        private void initialForm() {
            if (ItemSection.name != null) {
                this.tbSectionName.Text = ItemSection.name;
            }
            if (ItemSection.detail != null) {
                this.tbSectionDetail.Text = ItemSection.detail;
            }
            if (ItemSection.annotation != null) {
                this.rtbSectionAnnotation.Text = ItemSection.annotation;
            }
        }

        /// <summary>
        /// 保存后退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //名称是必须填写的
            if (this.tbSectionName.Text.Trim().Equals("")) {
                MessageBox.Show("错误", "章节名称必须填写", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ItemSection.name = this.tbSectionName.Text.Trim();
            ItemSection.detail = this.tbSectionDetail.Text.Trim();
            ItemSection.annotation = this.rtbSectionAnnotation.Text.Trim();
            isSaved = true;
            //关闭窗体
            this.Close();
        }
    }
}
