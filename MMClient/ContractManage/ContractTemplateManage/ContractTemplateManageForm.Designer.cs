﻿namespace MMClient.ContractManage.ContractTemplateManage
{
    partial class ContractTemplateManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractTemplateManageForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvContractTemplate = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxStatus = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbCreaterID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbContractTemplateName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbContractTemplateID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbNewContractTemplate = new System.Windows.Forms.ToolStripButton();
            this.tsbPreview = new System.Windows.Forms.ToolStripButton();
            this.tsbVerifyContractTemplate = new System.Windows.Forms.ToolStripButton();
            this.tsbDeleteContractTemplate = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbVariableMaintain = new System.Windows.Forms.ToolStripButton();
            this.tsbContractTermMaintain = new System.Windows.Forms.ToolStripButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContractTemplate)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvContractTemplate);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(824, 450);
            this.panel1.TabIndex = 1;
            // 
            // dgvContractTemplate
            // 
            this.dgvContractTemplate.AllowUserToAddRows = false;
            this.dgvContractTemplate.AllowUserToResizeRows = false;
            this.dgvContractTemplate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvContractTemplate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvContractTemplate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvContractTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContractTemplate.EnableHeadersVisualStyles = false;
            this.dgvContractTemplate.Location = new System.Drawing.Point(0, 118);
            this.dgvContractTemplate.Name = "dgvContractTemplate";
            this.dgvContractTemplate.RowHeadersVisible = false;
            this.dgvContractTemplate.RowTemplate.Height = 23;
            this.dgvContractTemplate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContractTemplate.Size = new System.Drawing.Size(824, 332);
            this.dgvContractTemplate.TabIndex = 1;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbxStatus);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbCreaterID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.dtpBeginTime);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbContractTemplateName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbContractTemplateID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(824, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "模板信息";
            // 
            // cbxStatus
            // 
            this.cbxStatus.FormattingEnabled = true;
            this.cbxStatus.Items.AddRange(new object[] {
            "全部状态",
            "未审核",
            "已审核"});
            this.cbxStatus.Location = new System.Drawing.Point(696, 28);
            this.cbxStatus.Name = "cbxStatus";
            this.cbxStatus.Size = new System.Drawing.Size(121, 20);
            this.cbxStatus.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(649, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "状态：";
            // 
            // tbCreaterID
            // 
            this.tbCreaterID.Location = new System.Drawing.Point(510, 26);
            this.tbCreaterID.Name = "tbCreaterID";
            this.tbCreaterID.Size = new System.Drawing.Size(118, 21);
            this.tbCreaterID.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(452, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "创建人：";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(742, 70);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(384, 69);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(156, 21);
            this.dtpEndTime.TabIndex = 7;
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(103, 69);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(156, 21);
            this.dtpBeginTime.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "截止日期：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "开始日期：";
            // 
            // tbContractTemplateName
            // 
            this.tbContractTemplateName.Location = new System.Drawing.Point(304, 26);
            this.tbContractTemplateName.Name = "tbContractTemplateName";
            this.tbContractTemplateName.Size = new System.Drawing.Size(118, 21);
            this.tbContractTemplateName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(235, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "模板名称：";
            // 
            // tbContractTemplateID
            // 
            this.tbContractTemplateID.Location = new System.Drawing.Point(90, 26);
            this.tbContractTemplateID.Name = "tbContractTemplateID";
            this.tbContractTemplateID.Size = new System.Drawing.Size(118, 21);
            this.tbContractTemplateID.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 29);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "模板编号：";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNewContractTemplate,
            this.tsbPreview,
            this.tsbVerifyContractTemplate,
            this.tsbDeleteContractTemplate,
            this.tsbRefresh,
            this.tsbVariableMaintain,
            this.tsbContractTermMaintain});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(824, 44);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbNewContractTemplate
            // 
            this.tsbNewContractTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbNewContractTemplate.Image")));
            this.tsbNewContractTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNewContractTemplate.Name = "tsbNewContractTemplate";
            this.tsbNewContractTemplate.Size = new System.Drawing.Size(36, 41);
            this.tsbNewContractTemplate.Text = "新建";
            this.tsbNewContractTemplate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbNewContractTemplate.Click += new System.EventHandler(this.tsbNewContractTemplate_Click);
            // 
            // tsbPreview
            // 
            this.tsbPreview.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreview.Image")));
            this.tsbPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreview.Name = "tsbPreview";
            this.tsbPreview.Size = new System.Drawing.Size(36, 41);
            this.tsbPreview.Text = "预览";
            this.tsbPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPreview.Click += new System.EventHandler(this.tsbPreview_Click);
            // 
            // tsbVerifyContractTemplate
            // 
            this.tsbVerifyContractTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerifyContractTemplate.Image")));
            this.tsbVerifyContractTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerifyContractTemplate.Name = "tsbVerifyContractTemplate";
            this.tsbVerifyContractTemplate.Size = new System.Drawing.Size(36, 41);
            this.tsbVerifyContractTemplate.Text = "审核";
            this.tsbVerifyContractTemplate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbVerifyContractTemplate.Click += new System.EventHandler(this.tsbVerifyContractTemplate_Click);
            // 
            // tsbDeleteContractTemplate
            // 
            this.tsbDeleteContractTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeleteContractTemplate.Image")));
            this.tsbDeleteContractTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeleteContractTemplate.Name = "tsbDeleteContractTemplate";
            this.tsbDeleteContractTemplate.Size = new System.Drawing.Size(36, 41);
            this.tsbDeleteContractTemplate.Text = "删除";
            this.tsbDeleteContractTemplate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDeleteContractTemplate.Click += new System.EventHandler(this.tsbDeleteContractTemplate_Click);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(36, 41);
            this.tsbRefresh.Text = "刷新";
            this.tsbRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // tsbVariableMaintain
            // 
            this.tsbVariableMaintain.Image = ((System.Drawing.Image)(resources.GetObject("tsbVariableMaintain.Image")));
            this.tsbVariableMaintain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVariableMaintain.Name = "tsbVariableMaintain";
            this.tsbVariableMaintain.Size = new System.Drawing.Size(60, 41);
            this.tsbVariableMaintain.Text = "变量维护";
            this.tsbVariableMaintain.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbVariableMaintain.Click += new System.EventHandler(this.tsbVariableMaintain_Click);
            // 
            // tsbContractTermMaintain
            // 
            this.tsbContractTermMaintain.Image = ((System.Drawing.Image)(resources.GetObject("tsbContractTermMaintain.Image")));
            this.tsbContractTermMaintain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbContractTermMaintain.Name = "tsbContractTermMaintain";
            this.tsbContractTermMaintain.Size = new System.Drawing.Size(60, 41);
            this.tsbContractTermMaintain.Text = "条款维护";
            this.tsbContractTermMaintain.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbContractTermMaintain.Click += new System.EventHandler(this.tsbItemMaintain_Click);
            // 
            // ContractTemplateManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 494);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ContractTemplateManageForm";
            this.Text = "合同模板管理";
            this.Load += new System.EventHandler(this.ContractTemplateManageForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvContractTemplate)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbNewContractTemplate;
        private System.Windows.Forms.ToolStripButton tsbVerifyContractTemplate;
        private System.Windows.Forms.ToolStripButton tsbDeleteContractTemplate;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbContractTemplateID;
        private System.Windows.Forms.TextBox tbContractTemplateName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgvContractTemplate;
        private System.Windows.Forms.TextBox tbCreaterID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripButton tsbPreview;
        private System.Windows.Forms.ToolStripButton tsbVariableMaintain;
        private System.Windows.Forms.ToolStripButton tsbContractTermMaintain;
        private System.Windows.Forms.ComboBox cbxStatus;
        private System.Windows.Forms.Label label6;
    }
}