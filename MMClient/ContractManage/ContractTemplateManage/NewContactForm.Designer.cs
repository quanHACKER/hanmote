﻿namespace MMClient.ContractManage.ContractTemplateManage
{
    partial class NewContactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvContractTemplateDisplay = new System.Windows.Forms.DataGridView();
            this.IsSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.editGroupBox = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.tbContractTemplateName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbContractTemplateID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddTerm = new System.Windows.Forms.Button();
            this.btnAddSection = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContractTemplateDisplay)).BeginInit();
            this.editGroupBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvContractTemplateDisplay
            // 
            this.dgvContractTemplateDisplay.AllowUserToAddRows = false;
            this.dgvContractTemplateDisplay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvContractTemplateDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvContractTemplateDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContractTemplateDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsSelected});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvContractTemplateDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvContractTemplateDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContractTemplateDisplay.Location = new System.Drawing.Point(0, 0);
            this.dgvContractTemplateDisplay.Name = "dgvContractTemplateDisplay";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvContractTemplateDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvContractTemplateDisplay.RowHeadersVisible = false;
            this.dgvContractTemplateDisplay.RowTemplate.Height = 23;
            this.dgvContractTemplateDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContractTemplateDisplay.Size = new System.Drawing.Size(872, 557);
            this.dgvContractTemplateDisplay.TabIndex = 0;
            // 
            // IsSelected
            // 
            this.IsSelected.HeaderText = "是否选中";
            this.IsSelected.Name = "IsSelected";
            // 
            // editGroupBox
            // 
            this.editGroupBox.Controls.Add(this.btnDelete);
            this.editGroupBox.Controls.Add(this.btnGenerate);
            this.editGroupBox.Controls.Add(this.tbContractTemplateName);
            this.editGroupBox.Controls.Add(this.label2);
            this.editGroupBox.Controls.Add(this.tbContractTemplateID);
            this.editGroupBox.Controls.Add(this.label1);
            this.editGroupBox.Controls.Add(this.btnAddTerm);
            this.editGroupBox.Controls.Add(this.btnAddSection);
            this.editGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editGroupBox.Location = new System.Drawing.Point(0, 0);
            this.editGroupBox.Name = "editGroupBox";
            this.editGroupBox.Size = new System.Drawing.Size(872, 99);
            this.editGroupBox.TabIndex = 1;
            this.editGroupBox.TabStop = false;
            this.editGroupBox.Text = "编辑";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(277, 58);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(756, 21);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 25);
            this.btnGenerate.TabIndex = 8;
            this.btnGenerate.Text = "生成";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // tbContractTemplateName
            // 
            this.tbContractTemplateName.Location = new System.Drawing.Point(482, 24);
            this.tbContractTemplateName.Name = "tbContractTemplateName";
            this.tbContractTemplateName.Size = new System.Drawing.Size(235, 21);
            this.tbContractTemplateName.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "合同模板名称：";
            // 
            // tbContractTemplateID
            // 
            this.tbContractTemplateID.Location = new System.Drawing.Point(127, 24);
            this.tbContractTemplateID.Name = "tbContractTemplateID";
            this.tbContractTemplateID.Size = new System.Drawing.Size(225, 21);
            this.tbContractTemplateID.TabIndex = 5;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "合同模板编号：";
            // 
            // btnAddTerm
            // 
            this.btnAddTerm.Location = new System.Drawing.Point(152, 58);
            this.btnAddTerm.Name = "btnAddTerm";
            this.btnAddTerm.Size = new System.Drawing.Size(75, 23);
            this.btnAddTerm.TabIndex = 1;
            this.btnAddTerm.Text = "添加条款";
            this.btnAddTerm.UseVisualStyleBackColor = true;
            this.btnAddTerm.Click += new System.EventHandler(this.btnAddTerm_Click);
            // 
            // btnAddSection
            // 
            this.btnAddSection.Location = new System.Drawing.Point(21, 58);
            this.btnAddSection.Name = "btnAddSection";
            this.btnAddSection.Size = new System.Drawing.Size(75, 23);
            this.btnAddSection.TabIndex = 0;
            this.btnAddSection.Text = "添加章节";
            this.btnAddSection.UseVisualStyleBackColor = true;
            this.btnAddSection.Click += new System.EventHandler(this.btnAddSection_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.editGroupBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(872, 99);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvContractTemplateDisplay);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(872, 557);
            this.panel2.TabIndex = 3;
            // 
            // NewContactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 656);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.HideOnClose = true;
            this.MaximizeBox = false;
            this.Name = "NewContactForm";
            this.Text = "合同模板编辑";
            ((System.ComponentModel.ISupportInitialize)(this.dgvContractTemplateDisplay)).EndInit();
            this.editGroupBox.ResumeLayout(false);
            this.editGroupBox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvContractTemplateDisplay;
        private System.Windows.Forms.GroupBox editGroupBox;
        private System.Windows.Forms.Button btnAddTerm;
        private System.Windows.Forms.Button btnAddSection;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbContractTemplateName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbContractTemplateID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSelected;

    }
}