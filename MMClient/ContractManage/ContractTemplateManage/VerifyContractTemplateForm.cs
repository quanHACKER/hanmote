﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class VerifyContractTemplateForm : DockContent
    {
        //数据库查询
        ContractModelDAL contractModelDAL = new ContractModelDAL();
        //合同显示框
        DataTable contractTemplateDataTable = new DataTable();

        //本界面问合同模板审核界面，具有一定权限的人才能查看
        public VerifyContractTemplateForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerifyContractTemplateForm_Load(object sender, EventArgs e)
        {
            //初始化 合同显示框
            InitialContractTemplateDataTable();
        }

        /// <summary>
        /// 初始化显示在dgvContactOfBuyer中的DataTable的结构
        /// </summary>
        private void InitialContractTemplateDataTable()
        {
            //contractTemplateDataTable.Columns.Add("是否选中");
            //contractTemplateDataTable.Columns.Add("审核结果");
            contractTemplateDataTable.Columns.Add("模板编号");
            contractTemplateDataTable.Columns.Add("模板名称");
            contractTemplateDataTable.Columns.Add("模板说明");
            contractTemplateDataTable.Columns.Add("创建时间");
            contractTemplateDataTable.Columns.Add("创建人");

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            {
                checkBoxColumn.HeaderText = "是否选中";
                checkBoxColumn.Name = "是否选中";
                checkBoxColumn.CellTemplate = new DataGridViewCheckBoxCell();
            }
            this.dgvContactOfBuyer.Columns.Insert(0, checkBoxColumn);
            DataGridViewComboBoxColumn comboBoxColumn = new DataGridViewComboBoxColumn(); {
                comboBoxColumn.HeaderText = "审核结果";
                comboBoxColumn.Name = "审核结果";
                comboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
                comboBoxColumn.CellTemplate = new DataGridViewComboBoxCell();
                comboBoxColumn.Items.Add("不通过");
                comboBoxColumn.Items.Add("通过");
                comboBoxColumn.DefaultCellStyle.NullValue = "不通过";
            }
            this.dgvContactOfBuyer.Columns.Insert(1, comboBoxColumn);
            //面板上的DataGridview数据来源为contractTemplateDataTable
            this.dgvContactOfBuyer.DataSource = contractTemplateDataTable;

            //初始化查询
            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 点击查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //根据 采购员ComboBox的index来进行查询
            string cbxBuyerID = this.cbxBuyer.SelectedItem.ToString();
            LinkedList<ContractTemplate> contractTemplateList;
            if (cbxBuyerID.Equals("全部人员"))
            {
                contractTemplateList = contractModelDAL.GetContractTemplateByBuyerID("all", 0);
            }
            else {
                contractTemplateList = contractModelDAL.GetContractTemplateByBuyerID(cbxBuyerID, 0);
            }

            //改变DataGridview的数据显示
            dgvContactOfBuyerDisplay(contractTemplateList);
        }

        //定义一个默认包含两个Items的ComboBox的控件
        //暂时用不到
        //private class DataGridViewComboBoxCellWithItems : DataGridViewComboBoxCell {
        //    public DataGridViewComboBoxCellWithItems() {
        //        this.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
        //        this.Items.Add("通过");
        //        this.Items.Add("不通过");
        //    }
        //}

        /// <summary>
        /// 根据数据显示
        /// </summary>
        /// <param name="contractTemplateList"></param>
        private void dgvContactOfBuyerDisplay(LinkedList<ContractTemplate> contractTemplateList) {
            //先清空原来的数据显示
            contractTemplateDataTable.Rows.Clear();

            if (contractTemplateList == null || contractTemplateList.Count == 0)
                return;

            //将数据库中读取的数据添加到contractTemplateDataTable中
            foreach (ContractTemplate template in contractTemplateList) {
                DataRow dr = contractTemplateDataTable.NewRow();
                //DataGridViewCheckBoxCell checkBoxCell = new DataGridViewCheckBoxCell();
                //dr["是否选中"] = checkBoxCell;
                //DataGridViewComboBoxCellWithItems comobxBoxCell = new DataGridViewComboBoxCellWithItems();
                //dr["审核结果"] = template.ContractTemplateState==true ? comobxBoxCell.DisplayMember = "通过" : "不通过";
                dr["模板编号"] = template.ContractTemplateID;
                dr["模板名称"] = template.ContractTemplateName;
                dr["模板说明"] = template.ContractTemplateContext;
                dr["创建时间"] = template.BeginTime;
                dr["创建人"] = template.CreaterID;

                contractTemplateDataTable.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 点击 提交审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmitVerify_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("选中模板确定通过审核？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            //对DataGridview中的数据进行编辑
            //这里只显示未通过审核的合同模板
            //查看选中的合同模板
            LinkedList<string> selected = new LinkedList<string>();
            for (int i = 0; i < this.dgvContactOfBuyer.Rows.Count; i++) {
                if (this.dgvContactOfBuyer.Rows[i].Cells["是否选中"].EditedFormattedValue.ToString().Equals("True")) {
                    string id = this.dgvContactOfBuyer.Rows[i].Cells["模板编号"].Value.ToString();
                    selected.AddLast(id);
                }
            }

            if (selected.Count == 0)
            {
                MessageBox.Show("请至少选择一个模板", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else { 
                //update选中的合同模板的状态
                int execResult = contractModelDAL.UpdateContractTemplateState(selected, true);
                if (execResult == 0 || execResult != selected.Count)
                {
                    MessageBox.Show("操作失败！");
                }
                else {
                    MessageBox.Show("操作成功！");
                }

                //执行一次查询操作，刷新当前显示
                this.btnSearch.PerformClick();
            }
        }
    }
}
