﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class TestForm : DockContent
    {
        //用来记录合同条款的信息
        private class TermInfo
        {
            public string TermFileName;
            //默认为0
            public int order = 0;
            public string title;
        }

        public TestForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = @"D:\contract\20161221\";
            LinkedList<TermInfo> termInfoList = new LinkedList<TermInfo>();
            
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Object contractFile = @"D:\contract\20161221\测试合同.docx";
            LinkedList<string> termFileList = new LinkedList<string>();
            LinkedList<string> srcD = new LinkedList<string>();
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款1.docx");
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款2.docx");
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款3.docx");
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款4.docx");
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款5.docx");
            srcD.AddLast(@"E:\硕士3班-廖宇\供应商\合同管理测试\条款6.docx");
            int order = 1;
            foreach (string src in srcD)
            {
                Object desFileName = path + @"条款" + order + ".docx";
                order++;
                if (File.Exists(src))
                {
                    //看要存储的文件是否已存在
                    if (File.Exists(desFileName.ToString()))
                    {
                        File.Delete(desFileName.ToString());
                    }
                    File.Copy(src, desFileName.ToString());
                    TermInfo termInfo = new TermInfo();
                    termInfo.TermFileName = desFileName.ToString();
                    termInfo.title = "测试";
                    termInfo.order = order;
                    termInfoList.AddLast(termInfo);
                    termFileList.AddLast(desFileName.ToString());
                }
                else
                {
                    MessageBox.Show("模板文件不存在！");
                    return;
                }
            }
            LinkedListNode<TermInfo> lastNode = termInfoList.Last;
            if (lastNode != null)
            {
                lastNode.Value.order = -1;
            }
            MergeFilesByInsert(contractFile.ToString(), termInfoList);
        }

        /// <summary>
        /// 采取插入合并两个word
        /// </summary>
        /// <param name="desFileName">目标word文件</param>
        /// <param name="srcFileNameList">需要插入目标word的src文件</param>
        private void MergeFilesByInsert(string desFileName, LinkedList<TermInfo> termInfoList)
        {
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document objDocLast = null;

            object objMissing = System.Reflection.Missing.Value;
            object objFalse = false;
            object confirmConversion = false;
            object link = false;
            object attachment = false;
            object desObject = desFileName;

            //创建file
            createWordFile(desFileName);

            try
            {
                //打开模板文件
                objDocLast = wordApp.Documents.Open(
                 ref desObject,    //FileName  
                 ref objMissing,   //ConfirmVersions  
                 ref objMissing,   //ReadOnly  
                 ref objMissing,   //AddToRecentFiles  
                 ref objMissing,   //PasswordDocument  
                 ref objMissing,   //PasswordTemplate  
                 ref objMissing,   //Revert  
                 ref objMissing,   //WritePasswordDocument  
                 ref objMissing,   //WritePasswordTemplate  
                 ref objMissing,   //Format  
                 ref objMissing,   //Enconding  
                 ref objMissing,   //Visible  
                 ref objMissing,   //OpenAndRepair  
                 ref objMissing,   //DocumentDirection  
                 ref objMissing,   //NoEncodingDialog  
                 ref objMissing    //XMLTransform  
                 );

                objDocLast.Activate();

                //遍历所有需要插入的word
                foreach (TermInfo termInfo in termInfoList)
                {
                    //判断下是否需要插入标题
                    if (termInfo.order > 0)
                    {
                        //插入标题
                        StringBuilder titleStr = new StringBuilder("第");
                        titleStr.Append(getChineseNumber(termInfo.order))
                            .Append("条  ")
                            .Append(termInfo.title);
                        //字体加粗
                        wordApp.Selection.Font.Bold = 1;
                        //设置段间距
                        wordApp.Selection.ParagraphFormat.LineUnitBefore = 0.5f;
                        wordApp.Selection.ParagraphFormat.LineUnitAfter = 0.5f;
                        //设置首行缩进
                        wordApp.Selection.ParagraphFormat.CharacterUnitFirstLineIndent = 2;
                        //插入文本
                        wordApp.Selection.TypeText(titleStr.ToString());
                        //回车符
                        wordApp.Selection.TypeParagraph();
                    }
                    else if (termInfo.order == -1)
                    {
                        //插入换页符
                        object nextPageBreak = (int)WdBreakType.wdSectionBreakNextPage;
                        //字体不加粗
                        wordApp.Selection.Font.Bold = 0;
                        //先插入一句 "（以下无正文，为合同签署页）"
                        wordApp.Selection.TypeText("（以下无正文，为合同签署页）");
                        wordApp.Selection.InsertBreak(ref nextPageBreak);
                    }

                    wordApp.Selection.InsertFile(
                        termInfo.TermFileName,
                        ref objMissing,
                        ref confirmConversion,
                        ref link,
                        ref attachment
                        );
                }
                //保存
                objDocLast.SaveAs(
                  ref desObject,      //FileName  
                  ref objMissing,     //FileFormat  
                  ref objMissing,     //LockComments  
                  ref objMissing,     //PassWord       
                  ref objMissing,     //AddToRecentFiles  
                  ref objMissing,     //WritePassword  
                  ref objMissing,     //ReadOnlyRecommended  
                  ref objMissing,     //EmbedTrueTypeFonts  
                  ref objMissing,     //SaveNativePictureFormat  
                  ref objMissing,     //SaveFormsData  
                  ref objMissing,     //SaveAsAOCELetter,  
                  ref objMissing,     //Encoding  
                  ref objMissing,     //InsertLineBreaks  
                  ref objMissing,     //AllowSubstitutions  
                  ref objMissing,     //LineEnding  
                  ref objMissing      //AddBiDiMarks  
                );
                objDocLast.Close(ref objMissing, ref objMissing, ref objMissing);
            }
            catch (Exception)
            {
                MessageBox.Show("生成失败");
            }
            finally
            {
                //关闭wordApp
                wordApp.Quit(
                  ref objMissing,     //SaveChanges  
                  ref objMissing,     //OriginalFormat  
                  ref objMissing      //RoutDocument  
                  );
                wordApp = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 创建word文件
        /// </summary>
        /// <param name="fileName">文件名称</param>
        private void createWordFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                MessageBox.Show("文件已存在");
                return;
            }
            object fileObject = fileName;
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();
            object missing = System.Reflection.Missing.Value;
            try
            {
                wordDocument = wordApp.Documents.Add(
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing);
            }
            finally
            {
                wordDocument.SaveAs(ref fileObject, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing);
                wordDocument.Close(ref missing, ref missing, ref missing);

                wordApp.Quit(ref missing, ref missing, ref missing);
                //结束WINWORD.exe程序
                wordApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 获得数字的中文字符(默认数字不会大于100)
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseNumber(int num)
        {
            StringBuilder strBui = new StringBuilder();
            int tens = num / 10;
            int ones = num % 10;

            if (tens == 0)
            {
                strBui.Append(getChineseSingleDigit(ones));
            }
            else
            {
                if (tens > 1)
                {
                    strBui.Append(getChineseSingleDigit(tens));
                }
                strBui.Append("十")
                    .Append(getChineseSingleDigit(ones));
            }

            return strBui.ToString();
        }

        /// <summary>
        /// 返回单个字符对应的中文
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseSingleDigit(int num)
        {
            string result = "";
            switch (num)
            {
                case 1: result = "一"; break;
                case 2: result = "二"; break;
                case 3: result = "三"; break;
                case 4: result = "四"; break;
                case 5: result = "五"; break;
                case 6: result = "六"; break;
                case 7: result = "七"; break;
                case 8: result = "八"; break;
                case 9: result = "九"; break;

                default: result = ""; break;
            }

            return result;
        }
    }
}
