﻿namespace MMClient.ContractManage.ContractTemplateManage
{
    partial class AddNewContractTemplateVariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbVariableName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbTokenText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxVariableType = new System.Windows.Forms.ComboBox();
            this.tbVariableDetail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbVariableName
            // 
            this.tbVariableName.Location = new System.Drawing.Point(116, 20);
            this.tbVariableName.Name = "tbVariableName";
            this.tbVariableName.Size = new System.Drawing.Size(174, 21);
            this.tbVariableName.TabIndex = 21;
            this.tbVariableName.TextChanged += new System.EventHandler(this.tbVariableName_TextChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 230);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbTokenText
            // 
            this.tbTokenText.Location = new System.Drawing.Point(116, 120);
            this.tbTokenText.Name = "tbTokenText";
            this.tbTokenText.Size = new System.Drawing.Size(174, 21);
            this.tbTokenText.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "令牌文本：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 20);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "变量名称：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "变量类型：";
            // 
            // cbxVariableType
            // 
            this.cbxVariableType.FormattingEnabled = true;
            this.cbxVariableType.Items.AddRange(new object[] {
            "文本",
            "时间"});
            this.cbxVariableType.Location = new System.Drawing.Point(116, 69);
            this.cbxVariableType.Name = "cbxVariableType";
            this.cbxVariableType.Size = new System.Drawing.Size(174, 20);
            this.cbxVariableType.TabIndex = 23;
            // 
            // tbVariableDetail
            // 
            this.tbVariableDetail.Location = new System.Drawing.Point(116, 168);
            this.tbVariableDetail.Name = "tbVariableDetail";
            this.tbVariableDetail.Size = new System.Drawing.Size(174, 21);
            this.tbVariableDetail.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 24;
            this.label4.Text = "变量说明：";
            // 
            // AddNewContractTemplateVariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 294);
            this.Controls.Add(this.tbVariableDetail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbxVariableType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbVariableName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbTokenText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AddNewContractTemplateVariableForm";
            this.Text = "合同模板条款变量";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbVariableName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbTokenText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxVariableType;
        private System.Windows.Forms.TextBox tbVariableDetail;
        private System.Windows.Forms.Label label4;
    }
}