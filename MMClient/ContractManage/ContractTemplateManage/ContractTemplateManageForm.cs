﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class ContractTemplateManageForm : DockContent
    {
        //用于显示的DataTable
        DataTable contractTemplateDt = new DataTable();
        //查询结果集(暂时不用)
        DataTable allContractTemplateDt = new DataTable();
        //查询数据库
        ContractModelDAL contractModelDAL = new ContractModelDAL();
        ContractTemplateDAL contractTemplateDALTool = new ContractTemplateDAL();
        // 新建合同模板
        private NewContractTemplateForm newContractTemplateForm = null;

        public ContractTemplateManageForm()
        {
            InitializeComponent();

            //初始化DataTable结构
            intialDataTable(contractTemplateDt);

            this.cbxStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// 点击 搜索 按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //查看搜索的条件
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;

            if (beginTime > endTime) {
                MessageBox.Show("请正确选择起止日期", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string contractTemplateID = this.tbContractTemplateID.Text.Trim();
            string contractTemplateName = this.tbContractTemplateName.Text.Trim();
            string contractTemplateCreaterID = this.tbCreaterID.Text.Trim();

            //写SQL语句
            StringBuilder strBui = new StringBuilder("select * from Contract_Template where Create_Time >= '")
                .Append(beginTime.ToString("yyyy-MM-dd hh:mm:ss.fff"))
                .Append("' and Create_Time <= '")
                .Append(endTime.ToString("yyyy-MM-dd hh:mm:ss.fff"))
                .Append("' ");
            
            //合同模板ID不为空
            if (!contractTemplateID.Equals("")) {
                strBui.Append(" and ID = '")
                    .Append(contractTemplateID)
                    .Append("' ");
            }
            //合同Name不为空
            if (!contractTemplateName.Equals("")) {
                strBui.Append(" and Name = '")
                    .Append(contractTemplateName)
                    .Append("'");
            }
            //创建者ID不为空
            if (!contractTemplateCreaterID.Equals("")) {
                strBui.Append(" and Creater_ID = '")
                    .Append(contractTemplateCreaterID)
                    .Append("'");
            }
            //合同模板的状态
            string templateStatus = this.cbxStatus.SelectedItem.ToString();
            if(!templateStatus.Equals("全部状态")){
                strBui.Append(" and Status = '")
                    .Append(templateStatus)
                    .Append("'");
            }

            //对结果排序
            strBui.Append(" order by ID asc");

            LinkedList<Contract_Template> resultList = contractTemplateDALTool.getContractTemplateBySQL(strBui.ToString());
            displaySearchResultOnDgv(resultList);
        }

        /// <summary>
        /// 将查询结果显示在DataGridView中
        /// </summary>
        /// <param name="resultList"></param>
        private void displaySearchResultOnDgv(LinkedList<Contract_Template> resultList) {
            //清空之前的数据
            contractTemplateDt.Rows.Clear();
            if (resultList == null || resultList.Count == 0) {
                return;
            }

            //将数据库中读取的数据添加到contractTemplateDataTable中
            foreach (Contract_Template template in resultList)
            {
                DataRow dr = contractTemplateDt.NewRow();
                
                dr["模板编号"] = template.ID;
                dr["模板名称"] = template.Name;
                dr["审核状态"] = template.Status;
                dr["创建时间"] = template.Create_Time;

                contractTemplateDt.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContractTemplateManageForm_Load(object sender, EventArgs e)
        {
            //绑定数据源
            this.dgvContractTemplate.DataSource = contractTemplateDt;
        }

        /// <summary>
        /// 初始化DataTable的结构
        /// </summary>
        /// <param name="dt"></param>
        private void intialDataTable(DataTable dt) {
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            {
                checkBoxColumn.HeaderText = "是否选中";
                checkBoxColumn.Name = "是否选中";
                checkBoxColumn.CellTemplate = new DataGridViewCheckBoxCell();

            }
            checkBoxColumn.FillWeight = 90;
            this.dgvContractTemplate.Columns.Insert(0, checkBoxColumn);
            //this.dgvContractTemplate.Columns[0].FillWeight = 90;

            DataColumn dc;
            dc = new DataColumn("模板编号");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
            dc = new DataColumn("模板名称");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
            //dt.Columns.Add("创建时间");
            dc = new DataColumn("创建时间");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
            //dt.Columns.Add("审核状态");
            dc = new DataColumn("审核状态");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
        }

        /// <summary>
        /// 点击删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDeleteContractTemplate_Click(object sender, EventArgs e)
        {
            //检测是否有合同模板被选中
            var result = MessageBox.Show("选中模板确定删除？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            //对DataGridview中的数据进行编辑
            //查看选中的合同模板
            LinkedList<string> selected = new LinkedList<string>();
            for (int i = 0; i < this.dgvContractTemplate.Rows.Count; i++)
            {
                if (this.dgvContractTemplate.Rows[i].Cells["是否选中"].EditedFormattedValue.ToString().Equals("True"))
                {
                    string id = this.dgvContractTemplate.Rows[i].Cells["模板编号"].Value.ToString();
                    selected.AddLast(id);
                }
            }

            if (selected.Count == 0)
            {
                MessageBox.Show("请至少选择一个模板", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //删除中的合同模板
                int execResult = contractTemplateDALTool.DeleteContractTemplate(selected);
                if (execResult == 0 || execResult != selected.Count)
                {
                    MessageBox.Show("操作失败！");
                }
                else
                {
                    MessageBox.Show("操作成功！");
                }

                //执行一次查询操作，刷新当前显示
                this.btnSearch.PerformClick();
            }
        }

        /// <summary>
        /// 新建合同模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbNewContractTemplate_Click(object sender, EventArgs e)
        {
            //NewContractTemplateForm newContactForm = new NewContractTemplateForm();
            //newContactForm.ShowDialog();
            if (newContractTemplateForm == null || newContractTemplateForm.IsDisposed) {
                newContractTemplateForm = new NewContractTemplateForm();
            }
            SingletonUserUI.addToUserUI(newContractTemplateForm);
        }

        /// <summary>
        /// 预览合同模板
        /// 更改为下载功能，需要修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            /*
            int selectRowsCount = this.dgvContractTemplate.SelectedRows.Count;
            if (selectRowsCount == 0)
            {
                MessageBox.Show("请选择要预览的合同模板", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //预览
            int selectRowIndex = this.dgvContractTemplate.SelectedRows[0].Index;
            //string fileID = 
            */
            MessageBox.Show("当前版本不支持合同模板的在线预览！");
        }

        /// <summary>
        /// 点击进入合同模板条款维护界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbItemMaintain_Click(object sender, EventArgs e)
        {
            ContractTemplateTermMaintainForm termMaintainForm = new ContractTemplateTermMaintainForm();
            termMaintainForm.ShowDialog();
        }

        /// <summary>
        /// 审核合同模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbVerifyContractTemplate_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("选中模板确定通过审核？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            LinkedList<string> templateIDList = new LinkedList<string>();
            //查看选中的模板中是否存在 "已审核" 的
            foreach (DataGridViewRow dgvr in this.dgvContractTemplate.Rows) {
                //这里不确定
                if (dgvr.Cells["是否选中"].EditedFormattedValue.ToString().Equals("True")
                    && dgvr.Cells["审核状态"].Value.ToString().Equals("未审核"))
                {
                    string templateID = dgvr.Cells["模板编号"].Value.ToString();

                    templateIDList.AddLast(templateID);
                }
            }

            if (templateIDList.Count == 0)
            {
                MessageBox.Show("没有需要审核的模板", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else {
                int num = contractTemplateDALTool.UpdateContractTemplateState(templateIDList, "已审核");
                StringBuilder text = new StringBuilder("成功审核");
                text.Append(num)
                    .Append("个模板");
                MessageBox.Show(text.ToString());

                this.btnSearch.PerformClick();
            }
        }

        /// <summary>
        /// 是否需要
        /// 留作扩展
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbVariableMaintain_Click(object sender, EventArgs e)
        {
            MessageBox.Show("不允许单独维护变量，请在条款中进行变量维护！");
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            this.btnSearch.PerformClick();
        }
    }
}
