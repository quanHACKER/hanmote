﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class ContractTemplateTermEditForm : DockContent
    {
        //记录用于显示的变量
        DataTable variableDT = new DataTable();
        //数据库工具
        ContractTemplateVariableDAL contractTemplateVariableDALTool = new ContractTemplateVariableDAL();
        ContractTemplateTermSampleDAL contractTemplateTermSampleDALTool = new ContractTemplateTermSampleDAL();

        public ContractTemplateTermEditForm()
        {
            InitializeComponent();

            //初始化
            initialDataTable(variableDT);
            //设定数据源
            this.dgvVariableDisplay.DataSource = variableDT;
        }

        private void initialDataTable(DataTable dt) {
            DataColumn dc;
            dc = new DataColumn("变量名称");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("变量类型");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("令牌文本");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("说明");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
        }

        /// <summary>
        /// 增加变量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddVariable_Click(object sender, EventArgs e)
        {
            Contract_Template_Variable variable = new Contract_Template_Variable();
            AddNewContractTemplateVariableForm newVariableForm = new AddNewContractTemplateVariableForm(variable);
            newVariableForm.ShowDialog();

            if (newVariableForm.isSaved) { 
                //加入DT
                DataRow dr = variableDT.NewRow();
                dr["变量名称"] = newVariableForm.variable.Name;
                dr["变量类型"] = newVariableForm.variable.Type;
                dr["令牌文本"] = newVariableForm.variable.Token_Text;
                dr["说明"] = newVariableForm.variable.Detail;

                variableDT.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 上传合同条款
        /// 固定服务器存放位置为：ftpserver:root_directory/合同模板/合同条款/%合同条款类别%/%合同条款名称%.docx
        /// %%中的文本由变量替换而来
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //封装的FTP客户端
            FTPTool ftpClient = FTPTool.getInstance();
            //检查条件
            if (this.tbTermSampleName.Text.Trim().Equals("")
                || this.tbTermSampleClass.Text.Trim().Equals("")
                || this.tbFileName.Text.Trim().Equals("")) {
                    MessageBox.Show("请填写完整", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            //存储
            //存储TermSample
            Contract_Template_Term_Sample termSample = new Contract_Template_Term_Sample();
            termSample.Name = this.tbTermSampleName.Text.Trim();
            termSample.Class = this.tbTermSampleClass.Text.Trim();
            string localFileName = this.tbFileName.Text.Trim();
            //termSample的名称保存为文件在服务器上的存储位置
            termSample.FileName = "合同模板" + "/" + "合同条款" + "/" + termSample.Class + "/" + termSample.Name + ".docx";
            //检验是否存在
            if (contractTemplateTermSampleDALTool.getContractTemplateTermSampleByTermSampleName(termSample.Name) != null) {
                    MessageBox.Show("该名称模板条款已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            //写入
            //bool isSuccess = true;
            bool isConnect = ftpClient.serverIsConnect();
            if (!isConnect)
            {
                MessageBox.Show("无法连接服务器！请确认ftp服务器是否开启！");
                return;
            }
            bool uploadSuccess = ftpClient.upload(localFileName,termSample.FileName);
            if (!uploadSuccess)
            {
                MessageBox.Show("合同条款上传失败，请检查网络状况并稍后尝试重新上传！");
                return;
            }
            contractTemplateTermSampleDALTool.addNewContractTemplateTermSample(termSample);
            int num = 0;
            foreach (DataRow dr in variableDT.Rows) {
                Contract_Template_Variable variable = new Contract_Template_Variable();
                variable.Name = dr["变量名称"].ToString();
                variable.Type = dr["变量类型"].ToString();
                variable.Token_Text = dr["令牌文本"].ToString();
                variable.Detail = dr["说明"].ToString();
                variable.Order_Number = num;
                variable.Contract_Template_Term_Sample_ID = termSample.Name;
                contractTemplateVariableDALTool.addNewContractTemplateVariable(variable);

                num++;
            }

            MessageBox.Show("保存成功");
        }

        /// <summary>
        /// 选择文件上传
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog uploadFileDialog = new OpenFileDialog();
            uploadFileDialog.InitialDirectory = "C:\\";
            uploadFileDialog.Filter = "contract files(*.doc;*.docx)|*.doc;*.docx";
            if (uploadFileDialog.ShowDialog() == DialogResult.OK) {
                this.tbFileName.Text = uploadFileDialog.FileName;
            }
        }

        /// <summary>
        /// 删除变量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteVariable_Click(object sender, EventArgs e)
        {

        }

    }
}
