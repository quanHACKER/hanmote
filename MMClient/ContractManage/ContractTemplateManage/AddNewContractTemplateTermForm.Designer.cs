﻿namespace MMClient.ContractManage.ContractTemplateManage
{
    partial class AddNewContractTemplateTermForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbxTermID = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTermDetail = new System.Windows.Forms.TextBox();
            this.rtbAnnotation = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "合同模板条款：";
            // 
            // cbxTermID
            // 
            this.cbxTermID.FormattingEnabled = true;
            this.cbxTermID.Location = new System.Drawing.Point(137, 29);
            this.cbxTermID.Name = "cbxTermID";
            this.cbxTermID.Size = new System.Drawing.Size(174, 20);
            this.cbxTermID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "合同模板条款说明：";
            // 
            // tbTermDetail
            // 
            this.tbTermDetail.Location = new System.Drawing.Point(137, 63);
            this.tbTermDetail.Name = "tbTermDetail";
            this.tbTermDetail.Size = new System.Drawing.Size(174, 21);
            this.tbTermDetail.TabIndex = 3;
            // 
            // rtbAnnotation
            // 
            this.rtbAnnotation.Location = new System.Drawing.Point(137, 116);
            this.rtbAnnotation.Name = "rtbAnnotation";
            this.rtbAnnotation.Size = new System.Drawing.Size(174, 96);
            this.rtbAnnotation.TabIndex = 4;
            this.rtbAnnotation.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "自定义注释：";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(103, 253);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // AddNewContractTemplateTermForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 303);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rtbAnnotation);
            this.Controls.Add(this.tbTermDetail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxTermID);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AddNewContractTemplateTermForm";
            this.Text = "合同条款编辑";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxTermID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTermDetail;
        private System.Windows.Forms.RichTextBox rtbAnnotation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
    }
}