﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.ContractDocs;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class AddNewContractTemplateVariableForm : DockContent
    { 
        public bool isSaved = false;
        public Contract_Template_Variable variable;

        public AddNewContractTemplateVariableForm(Contract_Template_Variable variable)
        {
            InitializeComponent();

            this.variable = variable;
            //初始化
            initialForm();
        }

        /// <summary>
        /// 初始化窗体
        /// </summary>
        private void initialForm()
        {
            if (variable.Name != null)
            {
                this.tbVariableName.Text = variable.Name;
            }
            if (variable.Type != null)
            {
                this.cbxVariableType.SelectedIndex = this.cbxVariableType.Items.IndexOf(variable.Type);
            }
            else {
                this.cbxVariableType.SelectedIndex = 0;
            }
            if (variable.Token_Text != null) {
                this.tbTokenText.Text = variable.Token_Text;
            }
            if (variable.Detail != null) {
                this.tbVariableDetail.Text = variable.Detail;
            }
        }

        /// <summary>
        /// 点击保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            variable.Name = this.tbVariableName.Text.Trim();
            variable.Type = this.cbxVariableType.SelectedItem.ToString();
            variable.Token_Text = this.tbTokenText.Text.Trim();
            variable.Detail = this.tbVariableDetail.Text.Trim();

            if (variable.Name.Equals("") || variable.Token_Text.Equals("")) {
                MessageBox.Show("请填写完整", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            isSaved = true;
            this.Close();
        }

        /// <summary>
        /// 为了输入方便，搞个这个事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbVariableName_TextChanged(object sender, EventArgs e)
        {
            string str = "%" + this.tbVariableName.Text + "%";
            this.tbTokenText.Text = str;
        }
    }
}
