﻿namespace MMClient.CatalogManage
{
    partial class PurchaseBudgetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxBudgetType = new System.Windows.Forms.ComboBox();
            this.cbxPurchaseBudgetID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.tbSumBudget = new System.Windows.Forms.TextBox();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.cbDetail = new System.Windows.Forms.CheckBox();
            this.dgvBudgetInfo = new System.Windows.Forms.DataGridView();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Budget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblBudgetDetail = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cbxCompanyID = new System.Windows.Forms.ComboBox();
            this.cbxFactoryID = new System.Windows.Forms.ComboBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.MaterialID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBudgetInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 31);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "预算编号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "公司编号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "工厂编号：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "预算类型：";
            // 
            // cbxBudgetType
            // 
            this.cbxBudgetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBudgetType.FormattingEnabled = true;
            this.cbxBudgetType.Items.AddRange(new object[] {
            "单次采购",
            "持续采购"});
            this.cbxBudgetType.Location = new System.Drawing.Point(94, 115);
            this.cbxBudgetType.Name = "cbxBudgetType";
            this.cbxBudgetType.Size = new System.Drawing.Size(137, 20);
            this.cbxBudgetType.TabIndex = 7;
            this.cbxBudgetType.SelectedIndexChanged += new System.EventHandler(this.cbxBudgetType_SelectedIndexChanged);
            // 
            // cbxPurchaseBudgetID
            // 
            this.cbxPurchaseBudgetID.FormattingEnabled = true;
            this.cbxPurchaseBudgetID.Location = new System.Drawing.Point(96, 28);
            this.cbxPurchaseBudgetID.Name = "cbxPurchaseBudgetID";
            this.cbxPurchaseBudgetID.Size = new System.Drawing.Size(369, 20);
            this.cbxPurchaseBudgetID.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "开始时间：";
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(96, 156);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(135, 21);
            this.dtpBeginTime.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(256, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "结束时间：";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(330, 156);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(135, 21);
            this.dtpEndTime.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "预算额度：";
            // 
            // tbSumBudget
            // 
            this.tbSumBudget.Location = new System.Drawing.Point(96, 195);
            this.tbSumBudget.Name = "tbSumBudget";
            this.tbSumBudget.Size = new System.Drawing.Size(135, 21);
            this.tbSumBudget.TabIndex = 14;
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(246, 195);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(63, 20);
            this.cbxCurrencyType.TabIndex = 15;
            this.cbxCurrencyType.SelectedIndexChanged += new System.EventHandler(this.cbxCurrencyType_SelectedIndexChanged);
            // 
            // cbDetail
            // 
            this.cbDetail.AutoSize = true;
            this.cbDetail.Location = new System.Drawing.Point(341, 197);
            this.cbDetail.Name = "cbDetail";
            this.cbDetail.Size = new System.Drawing.Size(96, 16);
            this.cbDetail.TabIndex = 16;
            this.cbDetail.Text = "详细制定预算";
            this.cbDetail.UseVisualStyleBackColor = true;
            this.cbDetail.CheckedChanged += new System.EventHandler(this.cbDetail_CheckedChanged);
            // 
            // dgvBudgetInfo
            // 
            this.dgvBudgetInfo.AllowUserToAddRows = false;
            this.dgvBudgetInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBudgetInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaterialID,
            this.MaterialName,
            this.Budget,
            this.CurrencyType});
            this.dgvBudgetInfo.Location = new System.Drawing.Point(12, 268);
            this.dgvBudgetInfo.Name = "dgvBudgetInfo";
            this.dgvBudgetInfo.RowTemplate.Height = 23;
            this.dgvBudgetInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBudgetInfo.Size = new System.Drawing.Size(520, 199);
            this.dgvBudgetInfo.TabIndex = 17;
            this.dgvBudgetInfo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBudgetInfo_EditingControlShowing);
            // 
            // MaterialName
            // 
            this.MaterialName.HeaderText = "物料名称";
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.Width = 122;
            // 
            // Budget
            // 
            this.Budget.HeaderText = "额度";
            this.Budget.Name = "Budget";
            // 
            // CurrencyType
            // 
            this.CurrencyType.HeaderText = "货币类型";
            this.CurrencyType.Name = "CurrencyType";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(457, 192);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(64, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblBudgetDetail
            // 
            this.lblBudgetDetail.AutoSize = true;
            this.lblBudgetDetail.Location = new System.Drawing.Point(23, 242);
            this.lblBudgetDetail.Name = "lblBudgetDetail";
            this.lblBudgetDetail.Size = new System.Drawing.Size(77, 12);
            this.lblBudgetDetail.TabIndex = 19;
            this.lblBudgetDetail.Text = "预算详细信息";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(392, 237);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(45, 23);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "添加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(457, 237);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(45, 23);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cbxCompanyID
            // 
            this.cbxCompanyID.FormattingEnabled = true;
            this.cbxCompanyID.Location = new System.Drawing.Point(96, 72);
            this.cbxCompanyID.Name = "cbxCompanyID";
            this.cbxCompanyID.Size = new System.Drawing.Size(135, 20);
            this.cbxCompanyID.TabIndex = 22;
            this.cbxCompanyID.SelectedIndexChanged += new System.EventHandler(this.cbxCompanyID_SelectedIndexChanged);
            // 
            // cbxFactoryID
            // 
            this.cbxFactoryID.FormattingEnabled = true;
            this.cbxFactoryID.Location = new System.Drawing.Point(328, 72);
            this.cbxFactoryID.Name = "cbxFactoryID";
            this.cbxFactoryID.Size = new System.Drawing.Size(135, 20);
            this.cbxFactoryID.TabIndex = 23;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 150;
            // 
            // MaterialID
            // 
            this.MaterialID.HeaderText = "物料编号";
            this.MaterialID.Name = "MaterialID";
            this.MaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MaterialID.Width = 150;
            // 
            // PurchaseBudgetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 479);
            this.Controls.Add(this.cbxFactoryID);
            this.Controls.Add(this.cbxCompanyID);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblBudgetDetail);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvBudgetInfo);
            this.Controls.Add(this.cbDetail);
            this.Controls.Add(this.cbxCurrencyType);
            this.Controls.Add(this.tbSumBudget);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dtpEndTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtpBeginTime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbxPurchaseBudgetID);
            this.Controls.Add(this.cbxBudgetType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.Name = "PurchaseBudgetForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "采购预算分配";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBudgetInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxBudgetType;
        private System.Windows.Forms.ComboBox cbxPurchaseBudgetID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbSumBudget;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.CheckBox cbDetail;
        private System.Windows.Forms.DataGridView dgvBudgetInfo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblBudgetDetail;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cbxCompanyID;
        private System.Windows.Forms.ComboBox cbxFactoryID;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Budget;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyType;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
    }
}