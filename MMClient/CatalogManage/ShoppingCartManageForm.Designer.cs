﻿namespace MMClient.CatalogManage
{
    partial class ShoppingCartManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShoppingCartManageForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbPreview = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbOrder = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxPurchaseBudgetID = new System.Windows.Forms.ComboBox();
            this.tbCurrency = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.tbBudgetRemain = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbShoppingCartAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSumBudget = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvPurchaseItem = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseItem)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPreview,
            this.tsbDelete,
            this.tsbRefresh,
            this.tsbOrder});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(767, 40);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbPreview
            // 
            this.tsbPreview.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreview.Image")));
            this.tsbPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreview.Name = "tsbPreview";
            this.tsbPreview.Size = new System.Drawing.Size(36, 37);
            this.tsbPreview.Text = "查看";
            this.tsbPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPreview.Click += new System.EventHandler(this.tsbPreview_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsbDelete.Image")));
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(36, 37);
            this.tsbDelete.Text = "删除";
            this.tsbDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(36, 37);
            this.tsbRefresh.Text = "刷新";
            this.tsbRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbOrder
            // 
            this.tsbOrder.Image = ((System.Drawing.Image)(resources.GetObject("tsbOrder.Image")));
            this.tsbOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOrder.Name = "tsbOrder";
            this.tsbOrder.Size = new System.Drawing.Size(60, 37);
            this.tsbOrder.Text = "一键下单";
            this.tsbOrder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbOrder.Click += new System.EventHandler(this.tsbOrder_Click);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbxPurchaseBudgetID);
            this.groupBox1.Controls.Add(this.tbCurrency);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.lblEndTime);
            this.groupBox1.Controls.Add(this.tbBudgetRemain);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbShoppingCartAmount);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbSumBudget);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 43);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(767, 103);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "采购预算";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cbxPurchaseBudgetID
            // 
            this.cbxPurchaseBudgetID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbxPurchaseBudgetID.FormattingEnabled = true;
            this.cbxPurchaseBudgetID.Location = new System.Drawing.Point(95, 24);
            this.cbxPurchaseBudgetID.Name = "cbxPurchaseBudgetID";
            this.cbxPurchaseBudgetID.Size = new System.Drawing.Size(149, 20);
            this.cbxPurchaseBudgetID.TabIndex = 12;
            this.cbxPurchaseBudgetID.SelectedIndexChanged += new System.EventHandler(this.cbxPurchaseBudgetID_SelectedIndexChanged);
            // 
            // tbCurrency
            // 
            this.tbCurrency.Enabled = false;
            this.tbCurrency.Location = new System.Drawing.Point(612, 62);
            this.tbCurrency.Name = "tbCurrency";
            this.tbCurrency.Size = new System.Drawing.Size(114, 21);
            this.tbCurrency.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(541, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "货币类型：";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Enabled = false;
            this.dtpEndTime.Location = new System.Drawing.Point(612, 21);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(114, 21);
            this.dtpEndTime.TabIndex = 9;
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Location = new System.Drawing.Point(529, 27);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(77, 12);
            this.lblEndTime.TabIndex = 8;
            this.lblEndTime.Text = "预算截止期：";
            // 
            // tbBudgetRemain
            // 
            this.tbBudgetRemain.Enabled = false;
            this.tbBudgetRemain.Location = new System.Drawing.Point(359, 62);
            this.tbBudgetRemain.Name = "tbBudgetRemain";
            this.tbBudgetRemain.Size = new System.Drawing.Size(150, 21);
            this.tbBudgetRemain.TabIndex = 7;
            this.tbBudgetRemain.Text = "0.0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(269, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "预算剩余金额：";
            // 
            // tbShoppingCartAmount
            // 
            this.tbShoppingCartAmount.Enabled = false;
            this.tbShoppingCartAmount.Location = new System.Drawing.Point(95, 62);
            this.tbShoppingCartAmount.Name = "tbShoppingCartAmount";
            this.tbShoppingCartAmount.Size = new System.Drawing.Size(149, 21);
            this.tbShoppingCartAmount.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "购物车金额：";
            // 
            // tbSumBudget
            // 
            this.tbSumBudget.Enabled = false;
            this.tbSumBudget.Location = new System.Drawing.Point(359, 24);
            this.tbSumBudget.Name = "tbSumBudget";
            this.tbSumBudget.Size = new System.Drawing.Size(150, 21);
            this.tbSumBudget.TabIndex = 3;
            this.tbSumBudget.Text = "0.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "预算总金额：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "预算编号：";
            // 
            // dgvPurchaseItem
            // 
            this.dgvPurchaseItem.AllowUserToAddRows = false;
            this.dgvPurchaseItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchaseItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.Material_ID,
            this.Supplier_ID,
            this.Material_Name,
            this.Price,
            this.Purchase_Amount,
            this.Sum});
            this.dgvPurchaseItem.Location = new System.Drawing.Point(4, 152);
            this.dgvPurchaseItem.MultiSelect = false;
            this.dgvPurchaseItem.Name = "dgvPurchaseItem";
            this.dgvPurchaseItem.RowTemplate.Height = 23;
            this.dgvPurchaseItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPurchaseItem.Size = new System.Drawing.Size(763, 195);
            this.dgvPurchaseItem.TabIndex = 2;
            this.dgvPurchaseItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseItem_CellContentClick);
            // 
            // Selected
            // 
            this.Selected.HeaderText = "选中";
            this.Selected.Name = "Selected";
            this.Selected.Width = 50;
            // 
            // Material_ID
            // 
            this.Material_ID.HeaderText = "物料编号";
            this.Material_ID.Name = "Material_ID";
            this.Material_ID.Width = 150;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.HeaderText = "供应商编号";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // Material_Name
            // 
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            this.Material_Name.Width = 150;
            // 
            // Price
            // 
            this.Price.HeaderText = "单价";
            this.Price.Name = "Price";
            this.Price.Width = 70;
            // 
            // Purchase_Amount
            // 
            this.Purchase_Amount.HeaderText = "采购数量";
            this.Purchase_Amount.Name = "Purchase_Amount";
            // 
            // Sum
            // 
            this.Sum.HeaderText = "总额";
            this.Sum.Name = "Sum";
            // 
            // ShoppingCartManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 359);
            this.Controls.Add(this.dgvPurchaseItem);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.Name = "ShoppingCartManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "管理购物车";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseItem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.ToolStripButton tsbOrder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbBudgetRemain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbShoppingCartAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSumBudget;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton tsbPreview;
        private System.Windows.Forms.DataGridView dgvPurchaseItem;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label lblEndTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sum;
        private System.Windows.Forms.TextBox tbCurrency;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxPurchaseBudgetID;
    }
}