﻿namespace MMClient.CatalogManage
{
    partial class CatalogManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CatalogManageForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbPreview = new System.Windows.Forms.ToolStripButton();
            this.tspNew = new System.Windows.Forms.ToolStripButton();
            this.tspDelete = new System.Windows.Forms.ToolStripButton();
            this.tspShoppingCart = new System.Windows.Forms.ToolStripButton();
            this.tsbBudget = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxMaterialType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbKeyword = new System.Windows.Forms.TextBox();
            this.dgvMaterialInfo = new System.Windows.Forms.DataGridView();
            this.MaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddToShoppingCart = new System.Windows.Forms.DataGridViewLinkColumn();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPreview,
            this.tspNew,
            this.tspDelete,
            this.tspShoppingCart,
            this.tsbBudget});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(846, 44);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbPreview
            // 
            this.tsbPreview.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreview.Image")));
            this.tsbPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreview.Name = "tsbPreview";
            this.tsbPreview.Size = new System.Drawing.Size(36, 41);
            this.tsbPreview.Text = "查看";
            this.tsbPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPreview.Click += new System.EventHandler(this.tsbPreview_Click);
            // 
            // tspNew
            // 
            this.tspNew.Image = ((System.Drawing.Image)(resources.GetObject("tspNew.Image")));
            this.tspNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspNew.Name = "tspNew";
            this.tspNew.Size = new System.Drawing.Size(36, 41);
            this.tspNew.Text = "新建";
            this.tspNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tspNew.Click += new System.EventHandler(this.tspNew_Click);
            // 
            // tspDelete
            // 
            this.tspDelete.Image = ((System.Drawing.Image)(resources.GetObject("tspDelete.Image")));
            this.tspDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspDelete.Name = "tspDelete";
            this.tspDelete.Size = new System.Drawing.Size(36, 41);
            this.tspDelete.Text = "删除";
            this.tspDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tspDelete.Click += new System.EventHandler(this.tspDelete_Click);
            // 
            // tspShoppingCart
            // 
            this.tspShoppingCart.Image = ((System.Drawing.Image)(resources.GetObject("tspShoppingCart.Image")));
            this.tspShoppingCart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspShoppingCart.Name = "tspShoppingCart";
            this.tspShoppingCart.Size = new System.Drawing.Size(48, 41);
            this.tspShoppingCart.Text = "购物车";
            this.tspShoppingCart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tspShoppingCart.Click += new System.EventHandler(this.tspShoppingCart_Click);
            // 
            // tsbBudget
            // 
            this.tsbBudget.Image = ((System.Drawing.Image)(resources.GetObject("tsbBudget.Image")));
            this.tsbBudget.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBudget.Name = "tsbBudget";
            this.tsbBudget.Size = new System.Drawing.Size(60, 41);
            this.tsbBudget.Text = "采购预算";
            this.tsbBudget.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbBudget.Click += new System.EventHandler(this.tsbBudget_Click);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbxMaterialType);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbxSupplierID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.tbKeyword);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 44);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(846, 125);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "自助采购";
            // 
            // cbxMaterialType
            // 
            this.cbxMaterialType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMaterialType.FormattingEnabled = true;
            this.cbxMaterialType.Location = new System.Drawing.Point(507, 32);
            this.cbxMaterialType.Name = "cbxMaterialType";
            this.cbxMaterialType.Size = new System.Drawing.Size(182, 20);
            this.cbxMaterialType.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(436, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "物料类型：";
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(131, 32);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(168, 20);
            this.cbxSupplierID.TabIndex = 3;
            this.cbxSupplierID.SelectedIndexChanged += new System.EventHandler(this.cbxSupplierID_SelectedIndexChanged);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 35);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "供应商编号：";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(614, 78);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbKeyword
            // 
            this.tbKeyword.Location = new System.Drawing.Point(50, 80);
            this.tbKeyword.Name = "tbKeyword";
            this.tbKeyword.Size = new System.Drawing.Size(533, 21);
            this.tbKeyword.TabIndex = 0;
            // 
            // dgvMaterialInfo
            // 
            this.dgvMaterialInfo.AllowUserToAddRows = false;
            this.dgvMaterialInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaterialID,
            this.MaterialName,
            this.SupplierID,
            this.Price,
            this.Unit,
            this.Detail,
            this.AddToShoppingCart});
            this.dgvMaterialInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialInfo.Location = new System.Drawing.Point(0, 169);
            this.dgvMaterialInfo.MultiSelect = false;
            this.dgvMaterialInfo.Name = "dgvMaterialInfo";
            this.dgvMaterialInfo.RowTemplate.Height = 23;
            this.dgvMaterialInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMaterialInfo.Size = new System.Drawing.Size(846, 364);
            this.dgvMaterialInfo.TabIndex = 2;
            this.dgvMaterialInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialInfo_CellContentClick);
            this.dgvMaterialInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvMaterialInfo_RowPostPaint);
            // 
            // MaterialID
            // 
            this.MaterialID.HeaderText = "物料编号";
            this.MaterialID.Name = "MaterialID";
            this.MaterialID.Width = 150;
            // 
            // MaterialName
            // 
            this.MaterialName.HeaderText = "物料名称";
            this.MaterialName.Name = "MaterialName";
            // 
            // SupplierID
            // 
            this.SupplierID.HeaderText = "供应商编号";
            this.SupplierID.Name = "SupplierID";
            // 
            // Price
            // 
            this.Price.HeaderText = "价格";
            this.Price.Name = "Price";
            this.Price.Width = 75;
            // 
            // Unit
            // 
            this.Unit.HeaderText = "单位";
            this.Unit.Name = "Unit";
            this.Unit.Width = 75;
            // 
            // Detail
            // 
            this.Detail.HeaderText = "简介";
            this.Detail.Name = "Detail";
            this.Detail.Width = 200;
            // 
            // AddToShoppingCart
            // 
            this.AddToShoppingCart.HeaderText = "加入购物车";
            this.AddToShoppingCart.Name = "AddToShoppingCart";
            this.AddToShoppingCart.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AddToShoppingCart.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CatalogManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 533);
            this.Controls.Add(this.dgvMaterialInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CatalogManageForm";
            this.Text = "自助采购主界面";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tspShoppingCart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripButton tspNew;
        private System.Windows.Forms.ToolStripButton tspDelete;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbKeyword;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxMaterialType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvMaterialInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewLinkColumn AddToShoppingCart;
        private System.Windows.Forms.ToolStripButton tsbPreview;
        private System.Windows.Forms.ToolStripButton tsbBudget;
    }
}