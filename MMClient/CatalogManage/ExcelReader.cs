﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Reflection;
using System.Data.OleDb;
using System.Data;

namespace MMClient.CatalogManage
{
    public class ExcelReader
    {
        private string fileName;
        private string sheetName;
        private bool firstRowAsHeader;
        private bool mixedDataType = true;
        private string sheetRange = string.Empty;
        private OleDbConnection excelConnection;
        private OleDbCommand excelCommand;
        public System.Data.DataTable dataTable;
        public ExcelReader(string fileName)
        {
            this.fileName = fileName;
        }
        public ExcelReader(string fileName, string sheetName) : this(fileName, sheetName, false, true) { }
        public ExcelReader(string fileName, string sheetName, bool firstRowAsHeader) : this(fileName, sheetName, firstRowAsHeader, true) { }
        public ExcelReader(string fileName, string sheetName, bool firstRowAsHeader, bool mixedDataType)
        {
            this.fileName = fileName;
            this.sheetName = sheetName;
            this.firstRowAsHeader = firstRowAsHeader;
            this.mixedDataType = mixedDataType;
        }

        /// <summary> 
        /// 要读取的Excel文件名 
        /// </summary> 
        public string FileName
        {
            get { return fileName; }
            set
            {
                if (fileName != value && excelConnection != null)
                {
                    this.Dispose();
                }
                fileName = value;
            }
        }

        private string SheetRange
        {
            get { return sheetRange; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value.IndexOf(':') == -1)
                {
                    throw new Exception("不是有效的Excel Range！");
                }
                sheetRange = value ?? string.Empty;
            }
        }

        /// <summary> 
        /// 是否将第一行作为列名 
        /// </summary> 
        public bool FirstRowAsHeader
        {
            get { return firstRowAsHeader; }
            set
            {
                if (firstRowAsHeader != value && excelConnection != null)
                    this.Dispose();
                firstRowAsHeader = value;
            }
        }

        /// <summary> 
        /// 是否是混合数据类型 True即Imex=1。默认True 
        /// </summary> 
        public bool MixedDataType
        {
            get { return mixedDataType; }
            set
            {
                if (mixedDataType != value && excelConnection != null)
                    this.Dispose();
                mixedDataType = value;
            }
        }

        /// <summary> 
        /// 要读取的工作表名称 
        /// </summary> 
        public string SheetName
        {
            get { return sheetName; }
            set { sheetName = value; }
        }

        private void OpenConnection()
        {
            if (excelConnection == null)
            {
                excelConnection = new OleDbConnection(GetConnectionString());
            }
            if (excelConnection.State != ConnectionState.Open)
            {
                excelConnection.Open();
            }
            else
            {
                this.Dispose();
            }
        }
        private string GetConnectionString()
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new Exception("FileName没有设置！");
            }
            string optionString = null;
            if (firstRowAsHeader)
            {
                optionString = "HDR=Yes;";
            }
            else
            {
                optionString = "HDR=No;";
            }
            if (mixedDataType)
            {
                optionString += "Imex=1;";
            }
            string fileType = System.IO.Path.GetExtension(fileName);
            if (string.IsNullOrEmpty(fileType)) return null;
            if (fileType == ".xls")
            {
                return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;{1}\"", fileName, optionString);
            }
            else
            {
                return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;{1}\"", fileName, optionString);
            }

        }
        private void PrepareCommand()
        {
            if (string.IsNullOrEmpty(sheetName))
            {
                throw new Exception("SheetName没有设置！");
            }
            OpenConnection();
            string cmdText = string.Format("SELECT * FROM [{0}${1}]", sheetName, sheetRange);
            if (excelCommand == null)
            {
                excelCommand = new OleDbCommand();
                excelCommand.Connection = excelConnection;
            }

            excelCommand.CommandText = cmdText;
        }

        /// <summary> 
        /// 获取当前Sheet到DataTable 
        /// </summary> 
        /// <returns>DataTable</returns> 
        public System.Data.DataTable GetDataTable()
        {
            excelConnection = null;
            return GetDataTable(string.Empty);
        }
        /// <summary> 
        /// 获取指定区域到DataTable   (A1:A5)
        /// </summary> 
        /// <param name="sheetRange">指定区域</param> 
        /// <returns>DataTable</returns> 
        public System.Data.DataTable GetDataTable(string sheetRange)
        {
            this.SheetRange = sheetRange;
            PrepareCommand();
            OleDbDataReader dr = excelCommand.ExecuteReader();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Load(dr);
            return dt;
        }
        /// <summary> 
        /// 获取指定单元格的值 
        /// </summary> 
        /// <param name="cellString">指定单元格 ie. "A1"</param> 
        /// <returns>object - 类型由Excel中单元格决定</returns> 
        public object GetCellValue(string cellString)
        {
            CheckCellString(cellString);
            if (firstRowAsHeader)
            {
                throw new Exception("FirstRowAsHeader为True时不能执行此操作！");
            }
            this.SheetRange = cellString + ":" + cellString;
            PrepareCommand();
            return excelCommand.ExecuteScalar();
        }


        /// <summary> 
        /// 获取指定单元格的值 
        /// </summary> 
        /// <param name="cellString">指定单元格ie. "A1"</param> 
        /// <returns>String</returns> 
        public string GetCellString(string cellString)
        {
            return GetCellValue(cellString).ToString();
        }

        /// <summary> 
        /// 设置指定单元格的值 
        /// </summary> 
        /// <param name="cellString">指定单元格ie. "A1"</param> 
        /// <param name="value">待设定的值</param> 
        public void SetCellValue(string cellString, object value)
        {
            CheckCellString(cellString);
            if (firstRowAsHeader)
            {
                throw new Exception("FirstRowAsHeader为True时不能执行此操作！");
            }
            if (mixedDataType)
            {
                throw new Exception("MixedDataType为True时不能执行此操作！");
            }
            this.SheetRange = cellString + ":" + cellString;
            PrepareCommand();
            excelCommand.CommandText = string.Format("UPDATE [{0}${1}] SET F1=?", sheetName, sheetRange);
            excelCommand.Parameters.AddWithValue("?Value", value);
            excelCommand.ExecuteNonQuery();
            excelCommand.Parameters.Clear();
        }

        private void CheckCellString(string cellString)
        {
            if (string.IsNullOrEmpty(cellString))
            {
                throw new ArgumentException("参数非法！", "cellString");
            }
        }
        public OleDbDataReader GetOleDbDataReader(string sheetRange)
        {
            this.SheetRange = sheetRange;
            PrepareCommand();
            return excelCommand.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary> 
        /// 获取或设置指定单元格的值 
        /// </summary> 
        /// <param name="cellString">指定单元格ie. "A1"</param> 
        /// <returns>获取的值，类型由Excel中单元格决定</returns> 
        public object this[string cellString]
        {
            set { this.SetCellValue(cellString, value); }
            get { return this.GetCellValue(cellString); }
        }

        public List<string> GetExcelSheetNames()
        {
            List<string> list = new List<string>();
            OpenConnection();
            System.Data.DataTable dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            foreach (DataRow row in dt.Rows)
            {
                string strSheetTableName = row["TABLE_NAME"].ToString();
                //截取sheetname前后的"'"
                if (strSheetTableName.StartsWith("'"))
                {
                    strSheetTableName = strSheetTableName.Substring(1, strSheetTableName.Length - 1);
                }
                if (strSheetTableName.EndsWith("'"))
                {
                    strSheetTableName = strSheetTableName.Substring(0, strSheetTableName.Length - 1);
                }
                if (strSheetTableName.EndsWith("$"))
                {
                    list.Add(strSheetTableName.Remove(strSheetTableName.Length - 1));
                }
            }
            return list;
        }


        #region IDisposable Members
        public void Dispose()
        {
            if (excelCommand != null)
            {
                excelCommand.Dispose();
                excelCommand = null;
            }
            if (excelConnection != null)
            {
                excelConnection.Dispose();
                excelConnection = null;
            }
        }
        #endregion
    }
}
