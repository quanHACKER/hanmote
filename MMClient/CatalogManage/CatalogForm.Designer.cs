﻿namespace MMClient.CatalogManage
{
    partial class CatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMaterialName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMaterialStandard = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbUnit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxAgreementID = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.rtbDetail = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbxMaterialID = new System.Windows.Forms.ComboBox();
            this.cbxSupplierName = new System.Windows.Forms.ComboBox();
            this.cbxMaterialType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料编号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "供应商编号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(458, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "物料类别：";
            // 
            // lblMaterialName
            // 
            this.lblMaterialName.AutoSize = true;
            this.lblMaterialName.Location = new System.Drawing.Point(335, 27);
            this.lblMaterialName.Name = "lblMaterialName";
            this.lblMaterialName.Size = new System.Drawing.Size(53, 12);
            this.lblMaterialName.TabIndex = 5;
            this.lblMaterialName.Text = "物料名称";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "物料规格：";
            // 
            // tbMaterialStandard
            // 
            this.tbMaterialStandard.Location = new System.Drawing.Point(97, 64);
            this.tbMaterialStandard.Name = "tbMaterialStandard";
            this.tbMaterialStandard.Size = new System.Drawing.Size(232, 21);
            this.tbMaterialStandard.TabIndex = 7;
            this.tbMaterialStandard.TextChanged += new System.EventHandler(this.tbMaterialStandard_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "价格：";
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(97, 105);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(106, 21);
            this.tbPrice.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "每";
            // 
            // tbUnit
            // 
            this.tbUnit.Location = new System.Drawing.Point(254, 105);
            this.tbUnit.Name = "tbUnit";
            this.tbUnit.Size = new System.Drawing.Size(30, 21);
            this.tbUnit.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(482, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "货币：";
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(529, 105);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(170, 20);
            this.cbxCurrencyType.TabIndex = 13;
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(97, 144);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(232, 20);
            this.cbxSupplierID.TabIndex = 14;
            this.cbxSupplierID.SelectedIndexChanged += new System.EventHandler(this.cbxSupplierID_SelectedIndexChanged);
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.AutoSize = true;
            this.lblSupplierName.Location = new System.Drawing.Point(388, 152);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(77, 12);
            this.lblSupplierName.TabIndex = 15;
            this.lblSupplierName.Text = "供应商名称：";
            // 
            // pbImage
            // 
            this.pbImage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pbImage.Location = new System.Drawing.Point(446, 182);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(253, 225);
            this.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImage.TabIndex = 16;
            this.pbImage.TabStop = false;
            this.pbImage.Click += new System.EventHandler(this.pbImage_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 17;
            this.label10.Text = "协议编号：";
            // 
            // cbxAgreementID
            // 
            this.cbxAgreementID.FormattingEnabled = true;
            this.cbxAgreementID.Location = new System.Drawing.Point(97, 182);
            this.cbxAgreementID.Name = "cbxAgreementID";
            this.cbxAgreementID.Size = new System.Drawing.Size(232, 20);
            this.cbxAgreementID.TabIndex = 18;
            this.cbxAgreementID.SelectedIndexChanged += new System.EventHandler(this.cbxAgreementID_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 307);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 19;
            this.label11.Text = "详细描述：";
            // 
            // rtbDetail
            // 
            this.rtbDetail.Location = new System.Drawing.Point(97, 228);
            this.rtbDetail.Name = "rtbDetail";
            this.rtbDetail.Size = new System.Drawing.Size(232, 179);
            this.rtbDetail.TabIndex = 20;
            this.rtbDetail.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(388, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 21;
            this.label12.Text = "图片：";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(624, 438);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 22;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbxMaterialID
            // 
            this.cbxMaterialID.FormattingEnabled = true;
            this.cbxMaterialID.Location = new System.Drawing.Point(97, 24);
            this.cbxMaterialID.Name = "cbxMaterialID";
            this.cbxMaterialID.Size = new System.Drawing.Size(232, 20);
            this.cbxMaterialID.TabIndex = 23;
            this.cbxMaterialID.SelectedIndexChanged += new System.EventHandler(this.cbxMaterialID_SelectedIndexChanged);
            // 
            // cbxSupplierName
            // 
            this.cbxSupplierName.FormattingEnabled = true;
            this.cbxSupplierName.Location = new System.Drawing.Point(471, 149);
            this.cbxSupplierName.Name = "cbxSupplierName";
            this.cbxSupplierName.Size = new System.Drawing.Size(228, 20);
            this.cbxSupplierName.TabIndex = 24;
            // 
            // cbxMaterialType
            // 
            this.cbxMaterialType.FormattingEnabled = true;
            this.cbxMaterialType.Location = new System.Drawing.Point(529, 24);
            this.cbxMaterialType.Name = "cbxMaterialType";
            this.cbxMaterialType.Size = new System.Drawing.Size(170, 20);
            this.cbxMaterialType.TabIndex = 25;
            // 
            // CatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 483);
            this.Controls.Add(this.cbxMaterialType);
            this.Controls.Add(this.cbxSupplierName);
            this.Controls.Add(this.cbxMaterialID);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.rtbDetail);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbxAgreementID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pbImage);
            this.Controls.Add(this.lblSupplierName);
            this.Controls.Add(this.cbxSupplierID);
            this.Controls.Add(this.cbxCurrencyType);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbUnit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbPrice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbMaterialStandard);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMaterialName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CatalogForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "创建物料目录";
            this.Load += new System.EventHandler(this.CatalogForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMaterialName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMaterialStandard;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbUnit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.Label lblSupplierName;
        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbxAgreementID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox rtbDetail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbxMaterialID;
        private System.Windows.Forms.ComboBox cbxSupplierName;
        private System.Windows.Forms.ComboBox cbxMaterialType;
    }
}