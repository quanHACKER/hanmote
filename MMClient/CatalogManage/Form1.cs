﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using Lib.SqlServerDAL;
using System.Data.OleDb;
using System.Globalization;

namespace MMClient.CatalogManage
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try{
                OpenFileDialog frm = new OpenFileDialog();
                frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
                string[] unitArr = {"件", "套", "想", "个", "盒", "桶", "筒", "条", "箱", "份"};
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    string excelName = frm.FileName;
                    //ExcelReader excelReader = new ExcelReader(excelName);
                    List<DataTable> dtList = exceldata(excelName);

                    foreach (DataTable dt in dtList) {
                        for (int i = 3; i < dt.Rows.Count; i++)
                        {
                            try
                            {
                                //ID
                                string ID = System.DateTime.Now.ToString("yyyyMMddHHmmss") + i.ToString("0000");
                                DataRow row = dt.Rows[i];
                                Random ran = new Random();

                                int key = ran.Next(1000, 9999);
                                string materialID = "001000100213" + key;
                                //供应商编号
                                key = ran.Next(10, 99);
                                string supplierID = "2016341" + key;
                                //物料名称
                                string materialName = row[1].ToString();
                                //细节
                                string detail = row[2].ToString();
                                //价格
                                double price = Convert.ToDouble(row[9].ToString());
                                //单位
                                key = ran.Next(0, 10);
                                string unit = unitArr[key];

                                StringBuilder strBui = new StringBuilder("insert into Material_Catalog (ID, Material_ID, Supplier_ID, Price, Detail, Material_Unit, Material_Name)")
                                    .Append(" values('").Append(ID).Append("', '").Append(materialID).Append("','")
                                    .Append(supplierID).Append("',").Append(price).Append(", '").Append(detail)
                                    .Append("','").Append(unit).Append("','").Append(materialName).Append("')");

                                int result = DBHelper.ExecuteNonQuery(strBui.ToString());
                            }
                            catch (Exception) { 
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<DataTable> exceldata(string filePath)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping all Sheet of Xl File
            List<DataTable> resultList = new List<DataTable>();
            foreach (DataRow row in schemaTable.Rows) {
                string sheet = row["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    dtexcel = new DataTable();
                    string query = "SELECT  * FROM [" + sheet + "]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    dtexcel.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(dtexcel);
                    resultList.Add(dtexcel);
                }
            }
            DataRow schemaRow = schemaTable.Rows[0];
            

            conn.Close();
            return resultList;
        }
    }
}
