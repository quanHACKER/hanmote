﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using MMClient.CatalogManage.CatalogAgreement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.CatalogPurchase;
using Lib.Bll.CatalogManageBLL;

namespace MMClient.CatalogManage
{
    public partial class CatalogManageForm : DockContent
    {
        //业务逻辑处理工具
        private CatalogMaterialBLL catalogMaterialTool = new CatalogMaterialBLL();

        public CatalogManageForm()
        {
            InitializeComponent();
            //物料类型ComboBox初始化
            List<string> typeList = catalogMaterialTool.getAllMaterialTypes();
            if (typeList != null)
            {
                this.cbxMaterialType.DataSource = typeList;
            }
            //供应商编号ComboBox初始化
            List<string> supplierList = catalogMaterialTool.getAllSuppliers();
            if (supplierList != null)
            {
                this.cbxSupplierID.DataSource = supplierList;
            }
        }

        /// <summary>
        /// 点击新建
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tspNew_Click(object sender, EventArgs e)
        {
            CatalogForm catalogForm = new CatalogForm();
            SingletonUserUI.addToUserUI(catalogForm);
        }

        /// <summary>
        /// 点击搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string supplierID = this.cbxSupplierID.Text;
            string materialType = this.cbxMaterialType.Text;

            string input = this.tbKeyword.Text.Trim();
            string[] arr = input.Split(' ');
            for (int i = 0; i < arr.Length; i++) {
                arr[i] = arr[i].Trim();
            }

            DataTable result = catalogMaterialTool.getCatalogMaterialDataTable(materialType, supplierID, arr);
            //清空
            this.dgvMaterialInfo.Rows.Clear();
            //this.dgvMaterialInfo.TopLeftHeaderCell.Value = 
            for (int i = 0; i < result.Rows.Count; i++) {
                this.dgvMaterialInfo.Rows.Add();
                DataGridViewRow dgvRow = this.dgvMaterialInfo.Rows[i];
                DataRow row = result.Rows[i];
                //物料编号
                dgvRow.Cells[0].Value = row["Material_ID"];
                //物料名称
                dgvRow.Cells[1].Value = row["Material_Name"];
                //供应商编号
                dgvRow.Cells[2].Value = row["Supplier_ID"];
                //价格
                dgvRow.Cells[3].Value = row["Price"];
                //单位
                dgvRow.Cells[4].Value = row["Material_Unit"];
                //简介
                dgvRow.Cells[5].Value = row["Detail"];
                //加入购物车
                dgvRow.Cells[6].Value = "购买";
            }
        }

        private void dgvMaterialInfo_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 点击购物车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tspShoppingCart_Click(object sender, EventArgs e)
        {
            ShoppingCartManageForm shoppingCart = new ShoppingCartManageForm();
            shoppingCart.ShowDialog();
        }

        /// <summary>
        /// 判断是否加入购物车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvMaterialInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //判断选择的行号、列号
            int rowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;
            string columnHeaderText = this.dgvMaterialInfo.Columns[columnIndex].HeaderText;
            if (columnHeaderText.Equals("加入购物车"))
            {
                DataGridViewRow curRow = this.dgvMaterialInfo.Rows[rowIndex];

                string materialID = curRow.Cells[0].Value.ToString();
                string supplierID = curRow.Cells[2].Value.ToString();
                CatalogMaterial catalogMaterialInfo = catalogMaterialTool
                    .getCatalogMaterial(materialID, supplierID);
                if (catalogMaterialInfo != null)
                {
                    AddShoppingCartForm addForm = new AddShoppingCartForm(catalogMaterialInfo);
                    addForm.ShowDialog();
                }
            }
        }

        /// <summary>
        /// 点击查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            if (this.dgvMaterialInfo.Rows.Count <= 0) {
                return;
            }
            DataGridViewRow curRow = this.dgvMaterialInfo.SelectedRows[0];

            string materialID = curRow.Cells[0].Value.ToString();
            string supplierID = curRow.Cells[2].Value.ToString();
            CatalogMaterial catalogMaterialInfo = catalogMaterialTool
                .getCatalogMaterial(materialID, supplierID);
            if (catalogMaterialInfo != null) {
                CatalogForm previewForm = new CatalogForm(catalogMaterialInfo, "view");
                previewForm.ShowDialog();
            }
        }

        /// <summary>
        /// 采购预算
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbBudget_Click(object sender, EventArgs e)
        {
            PurchaseBudgetForm form = new PurchaseBudgetForm();
            //SingletonUserUI.addToUserUI(form);
            form.ShowDialog();
        }

        private void cbxSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tspEdit_Click(object sender, EventArgs e)
        {

        }

        private void tspDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("该功能暂不开放，请联系管理人员手动删除！");
        }
    }
}
