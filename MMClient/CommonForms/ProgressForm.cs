﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;

namespace MMClient.CommonForms
{
    public partial class ProgressForm : Form
    {
        private string tipInfo;
        /// <summary>
        /// 提示信息
        /// </summary>
        public string TipInfo
        {
            get { return tipInfo; }
            set { tipInfo = value; }
        }

        private string opType;
        /// <summary>
        /// 操作类型
        /// </summary>
        public string OpType
        {
            get { return opType; }
            set { opType = value; }
        }

        private Dictionary<string, string> rLFileDic =
            new Dictionary<string, string>();
        /// <summary>
        /// 记录文件信息的词典
        /// </summary>
        public Dictionary<string, string> RLFileDic
        {
            get { return rLFileDic; }
            set { rLFileDic = value; }
        }

        private double allFilesSize;
        /// <summary>
        /// 所有文件大小
        /// </summary>
        public double AllFilesSize
        {
            get { return allFilesSize; }
            set { allFilesSize = value; }
        }

        // 失败结果链表
        private LinkedList<string> failedFileList = null;

        public ProgressForm(LinkedList<string> _list)
        {
            InitializeComponent();
            // 失败链表
            this.failedFileList = _list;
            // 最大值设为100
            this.progressBar1.Maximum = 100;
            this.progressBar1.Step = 1;
        }

        /// <summary>
        /// ShowDialog的同时，开始工作
        /// </summary>
        /// <returns></returns>
        public DialogResult ShowDialog() {
            startWork();

            return base.ShowDialog();
        }

        /// <summary>
        /// 当辅助线程指示某些操作已经进行时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            FTPTool.CurInfo curInfo = (FTPTool.CurInfo)e.UserState;
            int curValue = (int)Math.Floor(curInfo.curFilesSize * 100 / AllFilesSize);

            // 下面的代码是以委托的方式实现多线程，现在使用backgroundworker
            // 所以下面的代码不在使用
            // 显示进度比和进度文字信息
            //this.Invoke(new Action(() =>
            //{
            //    this.progressBar1.Value = curValue;
            //    //加载完成的文字信息
            //    showProgressValueOverProgressBar(curInfo.curFilesSize);
            //    showProgressInfoOnRichTextBox(this.rtbProgressInfo, curInfo.curFileName);
            //}));

            this.progressBar1.Value = curValue;
            //加载完成的文字信息
            showProgressValueOverProgressBar(curInfo.curFilesSize);
            showProgressInfoOnRichTextBox(this.rtbProgressInfo, curInfo.curFileName);
        }

        private int scrollIndex = 1;
        // 记录当前文件
        private string curFileName = ""; 
        /// <summary>
        /// 将进度信息进行显示
        /// </summary>
        /// <param name="_curFileName">文件名称</param>
        protected void showProgressInfoOnRichTextBox(Control ctl, string _curFileName)
        {
            if (string.IsNullOrWhiteSpace(ctl.Text))
                ctl.Text = "开始执行！";
            
            // 操作文件有变动
            if (!curFileName.Equals(_curFileName)) {
                ctl.Text = ctl.Text + "\r\n";
                if (opType.Equals("download"))
                    ctl.Text += "正在下载文件:";
                else if (opType.Equals("upload"))
                    ctl.Text += "正在上传文件:";

                curFileName = _curFileName;
                ctl.Text += curFileName;
                //拖动richtextbox的滚动条
                scrollIndex = scrollIndex + this.rtbProgressInfo.Width;
                this.rtbProgressInfo.SelectionStart = scrollIndex;
                this.rtbProgressInfo.ScrollToCaret();
            }
        }

        /// <summary>
        /// 在进度条上显示进度信息(10%),上面的Label上显示100.00KB/1.00MB
        /// </summary>
        private void showProgressValueOverProgressBar(double _curFileSize)
        {
            string strText = this.progressBar1.Value * 100 / this.progressBar1.Maximum + "%";
            string fileSizeInfo = (StringUtil.convertFileSizeToString(_curFileSize) + "/"
                + StringUtil.convertFileSizeToString(allFilesSize));
            // 显示百分比
            Font font = new Font("微软雅黑", (float)10, FontStyle.Regular);
            float infoLen = strText.Length;
            PointF pointF = new PointF((this.progressBar1.Width - infoLen) / 2, this.progressBar1.Height / 2 - 10);
            if(this.progressBar1 != null || !this.progressBar1.IsDisposed)
                this.progressBar1.CreateGraphics().DrawString(strText, font, Brushes.Black, pointF);
            // 显示 下载/上传 大小
            if(this.lblProgressInfo != null || !this.lblProgressInfo.IsDisposed)
                this.lblProgressInfo.Text = fileSizeInfo;
        }

        /// <summary>
        /// 操作开始时在另一线程运行的事件处理程序
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker object that raised this event.
            System.ComponentModel.BackgroundWorker worker;
            worker = (System.ComponentModel.BackgroundWorker)sender;

            //Get the BackgroundWorker object that raised this event.
            FTPTool ftp = (FTPTool)e.Argument;

            LinkedList<string> rs = null;
            if (OpType.Equals("upload"))
            {
                rs = ftp.upload(RLFileDic, AllFilesSize, worker, e);
            }
            else if(OpType.Equals("download")){
                rs = ftp.download(RLFileDic, AllFilesSize, worker, e);
            }
            foreach (string fileName in rs)
                failedFileList.AddLast(fileName);
        }

        /// <summary>
        /// 当辅助线程完成时(成功、失败、错误)触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // This event handler is called when the background thread finishes.
            // This method runs on the main thread.
            if (e.Error != null)
                MessageBox.Show("Error: " + e.Error.Message);
            else if (e.Cancelled)
                MessageBox.Show("操作取消！");
            else
                MessageBox.Show("操作成功！");

            // Close ProgressForm
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// 取消后台进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("操作未完成，确定取消？", "确认",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Cancel)
                return;

            this.backgroundWorker1.CancelAsync();
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// 开始执行
        /// </summary>
        private void startWork() {
            FTPTool ftp = FTPTool.getInstance();

            // Start the asynchronous operation.
            backgroundWorker1.RunWorkerAsync(ftp);
        }
    }
}