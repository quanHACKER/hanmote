﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Bll;
using Lib.Common.CommonUtils;
using Lib.Common;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient
{
    public partial class SelfAssessmentForm : DockContent
    {
        private UserUI ui = null;
        static SupplierManagementBLL smbll = new SupplierManagementBLL();
        DataTable supplierDataTable = smbll.GetAllSupplier();

        public SelfAssessmentForm()
        {
            InitializeComponent();
        }
        public SelfAssessmentForm(UserUI ui)
        {
            InitializeComponent();

            //初始化供应商编号
            FormHelper.combox_bind(this.gysbh_cmb, supplierDataTable, "Supplier_ID", "Supplier_ID");
            //初始化供应商名称
            FormHelper.combox_bind(this.gysmc_cmb, supplierDataTable, "Supplier_Name", "Supplier_Name");

            this.ui = ui;
            LoadListView();

        }

        /// <summary>  
        /// 初始化上传列表  
        /// </summary>  
        private void LoadListView()
        {
            //listView1.View = View.Details;
            //listView1.CheckBoxes = true;
            //listView1.GridLines = true;
            //listView1.Columns.Add("选择", 50, HorizontalAlignment.Center);
            //listView1.Columns.Add("供应商名称", 150, HorizontalAlignment.Center);
            //listView1.Columns.Add("提供物料类型", 100, HorizontalAlignment.Center);
            //listView1.Columns.Add("注册日期", 150, HorizontalAlignment.Center);
            //listView1.Columns.Add("详情", 100, HorizontalAlignment.Center);
        }

        private void BindDateToListView(DataTable dt)
        {
            ////for (int i = 0; i < dt.Rows.Count; i++)
            ////{
            ////    ListViewItem lvi = new ListViewItem();
            ////    lvi.SubItems.Add(dt.Rows[0].ItemArray[1].ToString());
            ////    lvi.SubItems.Add(dt.Rows[0].ItemArray[2].ToString());
            ////    lvi.SubItems.Add(dt.Rows[0].ItemArray[3].ToString());
            ////    lvi.SubItems.Add(dt.Rows[0].ItemArray[4].ToString());
            ////    listView1.Items.Add(lvi);
            ////}
        }


        /// <summary>
        /// 发送自评通知
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendmessagebtn_Click(object sender, EventArgs e)
        {
            string supplierID = this.gysbh_cmb.SelectedValue.ToString();
            bool flag = smbll.UpdateTempState(supplierID,SupplierCertificationState.AfterSelfassessmentDecision);
            if (flag)
            {
                MessageBox.Show("已发送");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DownloadFileForm df = new DownloadFileForm();
            df.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.comboBox1.SelectedValue.ToString()))
            {
                MessageBox.Show("评审结果不能为空");
                return ;
            }
        }
    }
}
