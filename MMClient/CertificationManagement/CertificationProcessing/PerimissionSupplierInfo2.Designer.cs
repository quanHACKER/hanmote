﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class PerimissionSupplierInfo2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFileInfo = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.File_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creator_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creator_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Create_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.fileViewBtn = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbCreatorName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxFileType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFileID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelCompanyNAme = new System.Windows.Forms.Label();
            this.LabelDBsCode = new System.Windows.Forms.Label();
            this.labelPostCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelUrlAddress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTelPhone = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelMobile = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LabelPosition = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelContactMan = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gb_processingInfo = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.评估阶段 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.评估项 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.评估得分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.评估员 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileInfo)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gb_processingInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "创建时间";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "创建者编号";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "文档类型";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "文档编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 177;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "创建者类型";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dgvFileInfo
            // 
            this.dgvFileInfo.AllowUserToAddRows = false;
            this.dgvFileInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvFileInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFileInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.File_ID,
            this.File_Type,
            this.Creator_ID,
            this.Creator_Type,
            this.Create_Time,
            this.filePath});
            this.dgvFileInfo.Location = new System.Drawing.Point(-3, 326);
            this.dgvFileInfo.Name = "dgvFileInfo";
            this.dgvFileInfo.RowHeadersVisible = false;
            this.dgvFileInfo.RowTemplate.Height = 23;
            this.dgvFileInfo.Size = new System.Drawing.Size(842, 461);
            this.dgvFileInfo.TabIndex = 6;
            // 
            // Selected
            // 
            this.Selected.HeaderText = "选中";
            this.Selected.Name = "Selected";
            this.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Selected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Selected.Width = 60;
            // 
            // File_ID
            // 
            this.File_ID.DataPropertyName = "文档名称";
            this.File_ID.HeaderText = "文档名称";
            this.File_ID.Name = "File_ID";
            this.File_ID.Width = 177;
            // 
            // File_Type
            // 
            this.File_Type.DataPropertyName = "文档类型";
            this.File_Type.HeaderText = "文档类型";
            this.File_Type.Name = "File_Type";
            this.File_Type.Width = 150;
            // 
            // Creator_ID
            // 
            this.Creator_ID.DataPropertyName = "上传者";
            this.Creator_ID.HeaderText = "上传者";
            this.Creator_ID.Name = "Creator_ID";
            this.Creator_ID.Width = 150;
            // 
            // Creator_Type
            // 
            this.Creator_Type.DataPropertyName = "角色";
            this.Creator_Type.HeaderText = "角色";
            this.Creator_Type.Name = "Creator_Type";
            this.Creator_Type.Width = 150;
            // 
            // Create_Time
            // 
            this.Create_Time.DataPropertyName = "创建时间";
            this.Create_Time.HeaderText = "创建时间";
            this.Create_Time.Name = "Create_Time";
            this.Create_Time.Width = 150;
            // 
            // filePath
            // 
            this.filePath.DataPropertyName = "文件路径";
            this.filePath.HeaderText = "文件路径";
            this.filePath.Name = "filePath";
            this.filePath.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(0, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(945, 135);
            this.panel1.TabIndex = 7;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.fileViewBtn);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.tbCreatorName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpBeginTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbxFileType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbFileID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(945, 126);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "文档信息";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(766, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "下载";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // fileViewBtn
            // 
            this.fileViewBtn.Location = new System.Drawing.Point(679, 86);
            this.fileViewBtn.Name = "fileViewBtn";
            this.fileViewBtn.Size = new System.Drawing.Size(75, 23);
            this.fileViewBtn.TabIndex = 11;
            this.fileViewBtn.Text = "查看";
            this.fileViewBtn.UseVisualStyleBackColor = true;
            this.fileViewBtn.Click += new System.EventHandler(this.fileViewBtn_Click_1);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(589, 86);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click_1);
            // 
            // tbCreatorName
            // 
            this.tbCreatorName.Location = new System.Drawing.Point(670, 43);
            this.tbCreatorName.Name = "tbCreatorName";
            this.tbCreatorName.Size = new System.Drawing.Size(137, 21);
            this.tbCreatorName.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(611, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "创建者：";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(375, 85);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(137, 21);
            this.dtpEndTime.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "截止时间：";
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(104, 85);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(137, 21);
            this.dtpBeginTime.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "开始时间：";
            // 
            // cbxFileType
            // 
            this.cbxFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFileType.FormattingEnabled = true;
            this.cbxFileType.Items.AddRange(new object[] {
            "全部文件",
            "供应商文件",
            "预评文件",
            "现场评估文件",
            "决策文件"});
            this.cbxFileType.Location = new System.Drawing.Point(375, 43);
            this.cbxFileType.Name = "cbxFileType";
            this.cbxFileType.Size = new System.Drawing.Size(137, 20);
            this.cbxFileType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "文档类型：";
            // 
            // tbFileID
            // 
            this.tbFileID.Location = new System.Drawing.Point(104, 43);
            this.tbFileID.Name = "tbFileID";
            this.tbFileID.ReadOnly = true;
            this.tbFileID.Size = new System.Drawing.Size(137, 21);
            this.tbFileID.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 46);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "文档编号：";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(848, 169);
            this.panel2.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelCompanyNAme);
            this.groupBox2.Controls.Add(this.LabelDBsCode);
            this.groupBox2.Controls.Add(this.labelPostCode);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.labelAddress);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.labelUrlAddress);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.labelEmail);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.labelTelPhone);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.labelMobile);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.LabelPosition);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.labelContactMan);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(836, 163);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基本信息";
            // 
            // labelCompanyNAme
            // 
            this.labelCompanyNAme.AutoSize = true;
            this.labelCompanyNAme.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelCompanyNAme.Location = new System.Drawing.Point(247, 66);
            this.labelCompanyNAme.Name = "labelCompanyNAme";
            this.labelCompanyNAme.Size = new System.Drawing.Size(47, 12);
            this.labelCompanyNAme.TabIndex = 38;
            this.labelCompanyNAme.Text = "label15";
            this.labelCompanyNAme.Visible = false;
            // 
            // LabelDBsCode
            // 
            this.LabelDBsCode.AutoSize = true;
            this.LabelDBsCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelDBsCode.Location = new System.Drawing.Point(247, 38);
            this.LabelDBsCode.Name = "LabelDBsCode";
            this.LabelDBsCode.Size = new System.Drawing.Size(47, 12);
            this.LabelDBsCode.TabIndex = 37;
            this.LabelDBsCode.Text = "label14";
            this.LabelDBsCode.Visible = false;
            // 
            // labelPostCode
            // 
            this.labelPostCode.AutoSize = true;
            this.labelPostCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelPostCode.Location = new System.Drawing.Point(572, 117);
            this.labelPostCode.Name = "labelPostCode";
            this.labelPostCode.Size = new System.Drawing.Size(41, 12);
            this.labelPostCode.TabIndex = 36;
            this.labelPostCode.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(501, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 35;
            this.label8.Text = "邮编：";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelAddress.Location = new System.Drawing.Point(96, 117);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(77, 12);
            this.labelAddress.TabIndex = 34;
            this.labelAddress.Text = "labelAddress";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(13, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 33;
            this.label12.Text = "地址：";
            // 
            // labelUrlAddress
            // 
            this.labelUrlAddress.AutoSize = true;
            this.labelUrlAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelUrlAddress.Location = new System.Drawing.Point(572, 92);
            this.labelUrlAddress.Name = "labelUrlAddress";
            this.labelUrlAddress.Size = new System.Drawing.Size(41, 12);
            this.labelUrlAddress.TabIndex = 32;
            this.labelUrlAddress.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(501, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "网址：";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelEmail.Location = new System.Drawing.Point(96, 92);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(65, 12);
            this.labelEmail.TabIndex = 30;
            this.labelEmail.Text = "labelEmail";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(13, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 29;
            this.label9.Text = "邮箱：";
            // 
            // labelTelPhone
            // 
            this.labelTelPhone.AutoSize = true;
            this.labelTelPhone.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelTelPhone.Location = new System.Drawing.Point(572, 66);
            this.labelTelPhone.Name = "labelTelPhone";
            this.labelTelPhone.Size = new System.Drawing.Size(41, 12);
            this.labelTelPhone.TabIndex = 28;
            this.labelTelPhone.Text = "label3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(501, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 27;
            this.label11.Text = "传真：";
            // 
            // labelMobile
            // 
            this.labelMobile.AutoSize = true;
            this.labelMobile.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelMobile.Location = new System.Drawing.Point(95, 66);
            this.labelMobile.Name = "labelMobile";
            this.labelMobile.Size = new System.Drawing.Size(41, 12);
            this.labelMobile.TabIndex = 26;
            this.labelMobile.Text = "label3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(13, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 25;
            this.label13.Text = "电话号码：";
            // 
            // LabelPosition
            // 
            this.LabelPosition.AutoSize = true;
            this.LabelPosition.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelPosition.Location = new System.Drawing.Point(573, 38);
            this.LabelPosition.Name = "LabelPosition";
            this.LabelPosition.Size = new System.Drawing.Size(41, 12);
            this.LabelPosition.TabIndex = 24;
            this.LabelPosition.Text = "label3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(502, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 23;
            this.label7.Text = "职位：";
            // 
            // labelContactMan
            // 
            this.labelContactMan.AutoSize = true;
            this.labelContactMan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelContactMan.Location = new System.Drawing.Point(97, 38);
            this.labelContactMan.Name = "labelContactMan";
            this.labelContactMan.Size = new System.Drawing.Size(41, 12);
            this.labelContactMan.TabIndex = 22;
            this.labelContactMan.Text = "label3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(14, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "联系人：";
            // 
            // gb_processingInfo
            // 
            this.gb_processingInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_processingInfo.Controls.Add(this.dataGridView1);
            this.gb_processingInfo.Location = new System.Drawing.Point(848, 6);
            this.gb_processingInfo.Name = "gb_processingInfo";
            this.gb_processingInfo.Size = new System.Drawing.Size(438, 781);
            this.gb_processingInfo.TabIndex = 9;
            this.gb_processingInfo.TabStop = false;
            this.gb_processingInfo.Text = "评估信息";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.评估阶段,
            this.评估项,
            this.评估得分,
            this.评估员});
            this.dataGridView1.Location = new System.Drawing.Point(6, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(426, 755);
            this.dataGridView1.TabIndex = 0;
            // 
            // 评估阶段
            // 
            this.评估阶段.HeaderText = "评估阶段";
            this.评估阶段.Name = "评估阶段";
            // 
            // 评估项
            // 
            this.评估项.HeaderText = "评估项";
            this.评估项.Name = "评估项";
            // 
            // 评估得分
            // 
            this.评估得分.HeaderText = "评估得分";
            this.评估得分.Name = "评估得分";
            // 
            // 评估员
            // 
            this.评估员.HeaderText = "评估员";
            this.评估员.Name = "评估员";
            // 
            // PerimissionSupplierInfo2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1298, 792);
            this.Controls.Add(this.gb_processingInfo);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvFileInfo);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "PerimissionSupplierInfo2";
            this.Text = "详细信息";
            this.Load += new System.EventHandler(this.PerimissionSupplierInfo2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFileInfo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gb_processingInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridView dgvFileInfo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn File_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn File_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creator_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creator_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Create_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn filePath;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button fileViewBtn;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbCreatorName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxFileType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbFileID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelPostCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelUrlAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelTelPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelMobile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LabelPosition;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelContactMan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelCompanyNAme;
        private System.Windows.Forms.Label LabelDBsCode;
        private System.Windows.Forms.GroupBox gb_processingInfo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 评估阶段;
        private System.Windows.Forms.DataGridViewTextBoxColumn 评估项;
        private System.Windows.Forms.DataGridViewTextBoxColumn 评估得分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 评估员;
    }
}