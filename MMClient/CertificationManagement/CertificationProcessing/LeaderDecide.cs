﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.CertificationProcess.LeadDecide;
using Lib.SqlServerDAL.CertificationProcess;
using Lib.Bll.CertificationProcess.General;
using MMClient;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class LeaderDecide : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        LeadDecidebll leadDecidebll = new LeadDecidebll();
        int stepTag = 1, backTag = 1, statusEnd = 1, IsProcess = 0;
        string supplierId = "";
        String User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
  
        public LeaderDecide()
        {
            
            InitializeComponent();
           
        }

        private void supplierInfoTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //处理按钮
                if (supplierInfoTable.Columns[e.ColumnIndex].Name == "处理")
                {
                    supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string status = "未处理";
                    string companyName = supplierInfoTable.CurrentRow.Cells["供应商名称"].Value.ToString();
                    status = supplierInfoTable.CurrentRow.Cells["提交状态"].Value.ToString();
                    DataTable dt = leadDecidebll.SelectAgree(supplierId);


                    LeaderDecide_subPage f = new LeaderDecide_subPage(supplierId, status, companyName);
                   
                    if (status == "待审核" || status == "待终审") {
                      
                            SingletonUserUI.addToUserUI(f);
                      
          
                    }
                    if (status == "已处理")
                    {
                        DialogResult dr = MessageBox.Show("审批意见已提交，点击确定查看", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (dr == DialogResult.OK)
                        {

                                SingletonUserUI.addToUserUI(f);
                            

                        }

                    }

                 

                }
                //转移-----
                if (supplierInfoTable.Columns[e.ColumnIndex].Name == "状态追踪") {
                    supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                    decideStatusShow decideStatusShow = new decideStatusShow(supplierId);

                    decideStatusShow.Show();

                   

                }

                    if (supplierInfoTable.Columns[e.ColumnIndex].Name == "反馈")
                {
                    supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string isAgree = supplierInfoTable.CurrentRow.Cells["IsAgree"].Value.ToString();
                    int flag = 0;
                    //判断是否已处理
                    if (IsProcess == 2)
                    {
                        DialogResult dr = MessageBox.Show("该供应商已处理！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    }
                    else
                    {
                        if (stepTag < statusEnd && backTag == 1)
                        {
                            DialogResult dr = MessageBox.Show("审批意见还没提交上级，点击处理按钮进入审批", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                        }
                        if (stepTag == statusEnd && backTag == 1)
                        {
                            DialogResult dr = MessageBox.Show("请点击处理按钮进行终审", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                        }
                        if (stepTag > 1 && stepTag < statusEnd && backTag < statusEnd && backTag > 1)
                        {
                            //反馈下级
                            flag = leadDecidebll.upDataDecideInfo(User_ID, backTag + 1, isAgree,supplierId);
                            if (flag == 1)
                            {
                                DialogResult dr = MessageBox.Show("批示成功，批示结果为" + isAgree, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                            }


                        }

                        if (stepTag == 1 && backTag == statusEnd)
                        {
                            //批示主审
                            string supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                            //更新SR_Info领导决策
                            flag = leadDecidebll.upDataSupplierTag(supplierId, User_ID, isAgree);
                            if (flag == 1)
                            {

                                DialogResult dr = MessageBox.Show("批示成功，批示结果为" + isAgree, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                            }


                        }


                    }

                }

            }
        }

        private void select_button_Click_1(object sender, EventArgs e)
        {
            select_button_Click(sender, e);
        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LeaderDecide_Load(object sender, EventArgs e)
        {
            loadData();

        }

        /// <summary>
        /// 翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {

        }

        #region 加载数据
        /// <summary>
        /// 加载数据
        /// </summary>
        public void loadData() {

            init();
            DataTable dt = leadDecidebll.getDecideSuppInfo(User_ID);
            pageNext1.DrawControl(dt.Rows.Count);
        }


        #endregion


        /// <summary>
        /// 初始化数据
        /// </summary>
        private void init()
        {

            DataTable dt = leadDecidebll.getDecideSuppInfo(User_ID);
           // dt.Columns.Add("状态", System.Type.GetType("System.String"));
            dt.Columns.Add("IsAgree", System.Type.GetType("System.String"));
            dt.Columns.Add("submit_status", System.Type.GetType("System.String"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //获取审批状态
                DataTable decideInfoDt = leadDecidebll.getDecideInfoBySupplierIdAndUserId(User_ID, dt.Rows[i]["供应商编号"].ToString());
                backTag = (int)decideInfoDt.Rows[0]["backTag"];
                stepTag = (int)decideInfoDt.Rows[0]["stepTag"];
                statusEnd = (int)decideInfoDt.Rows[0]["statusEnd"];
                IsProcess = (int)decideInfoDt.Rows[0]["IsProcess"];
                dt.Rows[i]["IsAgree"] = decideInfoDt.Rows[0]["IsAgree"].ToString();

                if (statusEnd == 1 && IsProcess == 1)
                {
                    dt.Rows[i]["submit_status"] = "已处理";

                }

                if (statusEnd == 1&& IsProcess==0)
                {
                    dt.Rows[i]["submit_status"] = "待终审";

                }
                else {


                    if (IsProcess == 1)
                    {
                        dt.Rows[i]["submit_status"] = "已处理";
                    }
                    else
                    {


                        if (stepTag < statusEnd)
                        {
                            dt.Rows[i]["submit_status"] = "待审核";

                        }
                        if (stepTag == statusEnd)
                        {
                            dt.Rows[i]["submit_status"] = "待终审";
                        }


                    }

                }

                }
            dt.Columns.Remove("Tagfour");
            this.supplierInfoTable.AutoGenerateColumns = false;
            supplierInfoTable.DataSource = null;
            supplierInfoTable.DataSource = dt;
        }


        //查询按钮
        private void select_button_Click(object sender, EventArgs e)
        {
            string status = "";
            status = status_comboBox.Text;
            string country = "";
            country = state_comboBox.Text;
            string name = "";
            string purchase = "";
            purchase = PurchaseOrg_textBox.Text;
            string thingGroup = "";
            thingGroup = thing_textBox.Text;
            name = textBox1.Text;
            DataTable dt = leadDecidebll.QueryConditionbll(purchase, thingGroup, status, country, name,1, SingleUserInstance.getCurrentUserInstance().User_ID);

           // dt.Columns.Add("状态", System.Type.GetType("System.String"));
            dt.Columns.Add("IsAgree", System.Type.GetType("System.String"));
            dt.Columns.Add("submit_status", System.Type.GetType("System.String"));


            for (int i = 0; i < dt.Rows.Count; i++)

            {
                DataTable decideInfoDt = leadDecidebll.getDecideInfoBySupplierIdAndUserId(User_ID, dt.Rows[i]["供应商编号"].ToString());
                backTag = (int)decideInfoDt.Rows[0]["backTag"];
                stepTag = (int)decideInfoDt.Rows[0]["stepTag"];
                statusEnd = (int)decideInfoDt.Rows[0]["statusEnd"];
                IsProcess = (int)decideInfoDt.Rows[0]["IsProcess"];
                dt.Rows[i]["IsAgree"] = decideInfoDt.Rows[0]["IsAgree"].ToString();
                if (statusEnd == 1 && IsProcess == 1)
                {
                    dt.Rows[i]["submit_status"] = "已处理";

                }

                if (statusEnd == 1 && IsProcess == 0)
                {
                    dt.Rows[i]["submit_status"] = "待终审";

                }
                else
                {


                    if (IsProcess == 1)
                    {
                        dt.Rows[i]["submit_status"] = "已处理";
                    }
                    else
                    {


                        if (stepTag < statusEnd)
                        {
                            dt.Rows[i]["submit_status"] = "待审核";

                        }
                        if (stepTag == statusEnd)
                        {
                            dt.Rows[i]["submit_status"] = "待终审";
                        }


                    }

                }

            }
            dt.Columns.Remove("Tagfour");
            this.supplierInfoTable.AutoGenerateColumns = false;
            supplierInfoTable.DataSource = null;
            supplierInfoTable.DataSource = dt;


        }
        /// <summary>
        /// 点击按钮监测
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) {
                //处理按钮
                if (supplierInfoTable.Columns[e.ColumnIndex].Name == "处理") {
                    string supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string companyName = supplierInfoTable.CurrentRow.Cells["供应商名称"].Value.ToString();
                    string status = "未处理";

                    status = supplierInfoTable.CurrentRow.Cells["提交状态"].Value.ToString();
                    DataTable dt = leadDecidebll.SelectAgree(supplierId);
 
                  
                    LeaderDecide_subPage f = new LeaderDecide_subPage(supplierId, status, companyName);

                    if (status == "提交上级"|| status == "待终审") { f.Show(); }
                    if (status == "已提交"){
                            DialogResult dr = MessageBox.Show("审批意见已提交，点击确定查看", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (dr == DialogResult.OK)
                            {
                                f.Show();
                            }
                       
                     }

                    if (status == "反馈下级"|| status == "批示主审") {
                        DialogResult dr = MessageBox.Show("审批意见已提交,点击反馈按钮，批示下级", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (dr == DialogResult.OK)
                        {
                            f.Show();
                        }
                    }

                    if (status == "已反馈")
                    {
                        DialogResult dr = MessageBox.Show("审批意见已反馈", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (dr == DialogResult.OK)
                        {
                            f.Show();
                        }
                    }




                }
                if (supplierInfoTable.Columns[e.ColumnIndex].Name == "反馈") {                   
                   string isAgree =  supplierInfoTable.CurrentRow.Cells["IsAgree"].Value.ToString();
                    int flag = 0;
                    //判断是否已处理
                    if (isAgree.Equals("待审核"))
                    {

                        DialogResult dr = MessageBox.Show("还未审核，无法反馈！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    }
                    else {
                        if (IsProcess == 2)
                        {
                            DialogResult dr = MessageBox.Show("该供应商已处理！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                        }
                        else
                        {
                            if (stepTag < statusEnd && backTag == 1)
                            {
                                DialogResult dr = MessageBox.Show("审批意见还没提交上级，点击处理按钮进入审批", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                            }
                            if (stepTag == statusEnd && backTag == 1)
                            {
                                DialogResult dr = MessageBox.Show("请点击处理按钮进行终审", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                            }
                            if (stepTag > 1 && stepTag < statusEnd && backTag < statusEnd && backTag > 1)
                            {
                                //反馈下级
                                flag = leadDecidebll.upDataDecideInfo(User_ID, backTag + 1, isAgree, supplierId);
                                if (flag == 1)
                                {
                                    DialogResult dr = MessageBox.Show("批示成功，批示结果为" + isAgree, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                                }


                            }

                            if (stepTag == 1 && backTag == statusEnd)
                            {
                                //批示主审
                                string supplierId = supplierInfoTable.CurrentRow.Cells["供应商编号"].Value.ToString();
                                //更新SR_Info领导决策
                                flag = leadDecidebll.upDataSupplierTag(supplierId, User_ID, isAgree);
                                if (flag == 1)
                                {

                                    DialogResult dr = MessageBox.Show("批示成功，批示结果为" + isAgree, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                                }


                            }


                        }



                    }
                }

            }
        }
    }
}
