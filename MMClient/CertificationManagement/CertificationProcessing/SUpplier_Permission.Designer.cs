﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class Supplier_Permission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.select_button = new System.Windows.Forms.Button();
            this.state_comboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.status_comboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.thing_textBox = new System.Windows.Forms.TextBox();
            this.物料组 = new System.Windows.Forms.Label();
            this.PurchaseOrg_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.supplierInfoTable = new System.Windows.Forms.DataGridView();
            this.供应商编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.区域 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.联系人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.联系方式 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.电子邮件 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.等级 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.查看 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierInfoTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.select_button);
            this.panel3.Controls.Add(this.state_comboBox);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.status_comboBox);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.thing_textBox);
            this.panel3.Controls.Add(this.物料组);
            this.panel3.Controls.Add(this.PurchaseOrg_textBox);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(1, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(911, 56);
            this.panel3.TabIndex = 39;
            // 
            // select_button
            // 
            this.select_button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.select_button.Location = new System.Drawing.Point(794, 25);
            this.select_button.Name = "select_button";
            this.select_button.Size = new System.Drawing.Size(79, 23);
            this.select_button.TabIndex = 47;
            this.select_button.Text = "查询";
            this.select_button.UseVisualStyleBackColor = true;
            this.select_button.Click += new System.EventHandler(this.select_button_Click);
            // 
            // state_comboBox
            // 
            this.state_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.state_comboBox.FormattingEnabled = true;
            this.state_comboBox.Items.AddRange(new object[] {
            "中国",
            "美国",
            "日本",
            "德国"});
            this.state_comboBox.Location = new System.Drawing.Point(483, 27);
            this.state_comboBox.Name = "state_comboBox";
            this.state_comboBox.Size = new System.Drawing.Size(125, 20);
            this.state_comboBox.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(481, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 45;
            this.label4.Text = "国家";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(648, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(104, 21);
            this.textBox1.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(646, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 43;
            this.label3.Text = "名称";
            // 
            // status_comboBox
            // 
            this.status_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.status_comboBox.FormattingEnabled = true;
            this.status_comboBox.Items.AddRange(new object[] {
            "已拒绝",
            "已准入"});
            this.status_comboBox.Location = new System.Drawing.Point(317, 27);
            this.status_comboBox.Name = "status_comboBox";
            this.status_comboBox.Size = new System.Drawing.Size(125, 20);
            this.status_comboBox.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(315, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 41;
            this.label2.Text = "状态";
            // 
            // thing_textBox
            // 
            this.thing_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thing_textBox.Location = new System.Drawing.Point(180, 27);
            this.thing_textBox.Name = "thing_textBox";
            this.thing_textBox.Size = new System.Drawing.Size(104, 21);
            this.thing_textBox.TabIndex = 40;
            // 
            // 物料组
            // 
            this.物料组.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.物料组.AutoSize = true;
            this.物料组.Location = new System.Drawing.Point(178, 10);
            this.物料组.Name = "物料组";
            this.物料组.Size = new System.Drawing.Size(41, 12);
            this.物料组.TabIndex = 39;
            this.物料组.Text = "物料组";
            // 
            // PurchaseOrg_textBox
            // 
            this.PurchaseOrg_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PurchaseOrg_textBox.Location = new System.Drawing.Point(40, 27);
            this.PurchaseOrg_textBox.Name = "PurchaseOrg_textBox";
            this.PurchaseOrg_textBox.Size = new System.Drawing.Size(104, 21);
            this.PurchaseOrg_textBox.TabIndex = 38;
            // 
            // supplierLabel
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 11);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 37;
            this.label1.Text = "采购组织";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.supplierInfoTable);
            this.panel1.Location = new System.Drawing.Point(1, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1086, 475);
            this.panel1.TabIndex = 41;
            // 
            // supplierInfoTable
            // 
            this.supplierInfoTable.AllowUserToAddRows = false;
            this.supplierInfoTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supplierInfoTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.supplierInfoTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.supplierInfoTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.supplierInfoTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.supplierInfoTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.supplierInfoTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.供应商编号,
            this.供应商名称,
            this.区域,
            this.联系人,
            this.联系方式,
            this.电子邮件,
            this.等级,
            this.查看});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.supplierInfoTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.supplierInfoTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.supplierInfoTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.supplierInfoTable.Location = new System.Drawing.Point(3, 3);
            this.supplierInfoTable.Name = "supplierInfoTable";
            this.supplierInfoTable.RowHeadersWidth = 50;
            this.supplierInfoTable.RowTemplate.Height = 23;
            this.supplierInfoTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.supplierInfoTable.Size = new System.Drawing.Size(1080, 469);
            this.supplierInfoTable.TabIndex = 9;
            this.supplierInfoTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.supplierInfoTable_CellContentClick);
            this.supplierInfoTable.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.supplierInfoTable_RowStateChanged);
            // 
            // 供应商编号
            // 
            this.供应商编号.DataPropertyName = "供应商编号";
            this.供应商编号.HeaderText = "供应商编号";
            this.供应商编号.Name = "供应商编号";
            // 
            // 供应商名称
            // 
            this.供应商名称.DataPropertyName = "供应商名称";
            this.供应商名称.HeaderText = "供应商名称";
            this.供应商名称.Name = "供应商名称";
            // 
            // 区域
            // 
            this.区域.DataPropertyName = "区域";
            this.区域.HeaderText = "区域";
            this.区域.Name = "区域";
            // 
            // 联系人
            // 
            this.联系人.DataPropertyName = "联系人";
            this.联系人.HeaderText = "联系人";
            this.联系人.Name = "联系人";
            // 
            // 联系方式
            // 
            this.联系方式.DataPropertyName = "联系方式";
            this.联系方式.HeaderText = "联系方式";
            this.联系方式.Name = "联系方式";
            // 
            // 电子邮件
            // 
            this.电子邮件.DataPropertyName = "电子邮件";
            this.电子邮件.HeaderText = "电子邮件";
            this.电子邮件.Name = "电子邮件";
            // 
            // 等级
            // 
            this.等级.DataPropertyName = "等级";
            this.等级.HeaderText = "等级";
            this.等级.Name = "等级";
            // 
            // 查看
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "查看";
            this.查看.DefaultCellStyle = dataGridViewCellStyle2;
            this.查看.HeaderText = "查看";
            this.查看.Name = "查看";
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.Location = new System.Drawing.Point(4, 544);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(660, 37);
            this.pageNext1.TabIndex = 42;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // Supplier_Permission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 593);
            this.Controls.Add(this.pageNext1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Supplier_Permission";
            this.Text = "准入供应商查看";
            this.Load += new System.EventHandler(this.Supplier_Permission_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.supplierInfoTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button select_button;
        private System.Windows.Forms.ComboBox state_comboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox status_comboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox thing_textBox;
        private System.Windows.Forms.Label 物料组;
        private System.Windows.Forms.TextBox PurchaseOrg_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView supplierInfoTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 区域;
        private System.Windows.Forms.DataGridViewTextBoxColumn 联系人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 联系方式;
        private System.Windows.Forms.DataGridViewTextBoxColumn 电子邮件;
        private System.Windows.Forms.DataGridViewTextBoxColumn 等级;
        private System.Windows.Forms.DataGridViewButtonColumn 查看;
        private pager.pagetool.pageNext pageNext1;
    }
}