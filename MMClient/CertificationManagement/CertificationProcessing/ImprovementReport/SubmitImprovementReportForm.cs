﻿using Lib.Bll.CertificationProcess.General;
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.CertificationManagement.CertificationProcessing.ImprovementReport
{
    public partial class SubmitImprovementReportForm : DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;
        GeneralBLL gn = new GeneralBLL();
        //存储查询结果
        private DataTable userInforTable;
        private LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
        private LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
        //邮件通知窗口对象
        private LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm;
        //查看报告
        private CheckImprovementReportForm checkImprovementReport;
        public SubmitImprovementReportForm()
        {
            InitializeComponent();
            init();
           
        }
        private void init()
        {
            this.pageNext1.PageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }

        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.ImprovementReportTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.ImprovementReportTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.ImprovementReportTable.CurrentRow.Index;
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            string NationalName = this.NationalName.Text.ToString();//公司地域
            string CompanyNam = this.CompanyNameInput.Text.ToString();//公司名称
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findAllImprovementReportSupplierByCondition(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageSize, 0, NationalName, CompanyNam,this.isProvement.Text.ToString().Trim());
            //2018/3/20
            float a = 0;
            for (int i = 0; i < userInforTable.Rows.Count; i++)
            {
                a = float.Parse(userInforTable.Rows[i]["rate"].ToString());
                if (a >= 0.9)
                {
                    userInforTable.Rows[i]["rate"] = a*100+"%(A)";


                }
                else if (a >= 0.8)
                {

                    userInforTable.Rows[i]["rate"]  =a * 100 + "%(B)";

                }
                else
                {

                    userInforTable.Rows[i]["rate"] = a * 100 + "%(C)";

                }
            }
            //end
            this.ImprovementReportTable.AutoGenerateColumns = false;
            this.ImprovementReportTable.DataSource = userInforTable;
            if (userInforTable.Rows.Count == 0)
            {
                pageNext1.PageIndex = 0;

            }
            else {
                pageNext1.PageIndex = 1;

            }
            this.pageNext1.DrawControl(userInforTable.Rows.Count);
        }
        /// <summary>
        /// chonzhi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       
        private void button1_Click_1(object sender, EventArgs e)
        {
            this.NationalName.Text = "";
            this.CompanyNameInput.Text = "";
            LoadData();
        }

        private void ImprovementReportTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //判断是处理还是重置
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.ImprovementReportTable.Rows[currentIndex];
                String companyID = Convert.ToString(row.Cells["SupplierId"].Value);
                String email = Convert.ToString(row.Cells["ContactMail"].Value);
                String companyName = Convert.ToString(row.Cells["companyName"].Value);
                String MtGroupName = Convert.ToString(row.Cells["MtGroupName"].Value);
                string propose = Convert.ToString(row.Cells["操作"].Value);
                string ispropose = Convert.ToString(row.Cells["Agree"].Value);

                if (this.ImprovementReportTable.Columns[e.ColumnIndex].Name == "查看")
                {

                    if (propose.Equals("已提交")|| propose.Equals("已改善"))
                    {
  
                            //查看改善报告
                            if (checkImprovementReport == null || checkImprovementReport.IsDisposed)
                            {
                                checkImprovementReport = new CheckImprovementReportForm(MtGroupName, companyID, email, companyName, propose);
                            }
                            checkImprovementReport.Show();
                        

                    }
                    else 
                    {
                        DialogResult dr = MessageBox.Show("还未上传改善报告", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);



                    }


                }

                //处理
                if (this.ImprovementReportTable.Columns[e.ColumnIndex].Name == "邮件通知")
                    {
                    if (!String.IsNullOrEmpty(ispropose) && ispropose.Equals("需要改善"))
                    {
                        string flag = locationalEvaluationBLL.getisSubmitImprovementReport(companyID);
                        if (flag == "0")
                        {
                            string url = "http://116.196.99.18:8080/Hanmote0106/SupplierManagement/SupplierUploadFile.jsp?supplierId=" + companyID + "&Mt_GroupName="+MtGroupName+"&improve=1";
                            List<MailAddress> list = new List<MailAddress>();
                            MailAddress mailAddress = new MailAddress(email);
                            list.Add(mailAddress);
                            EmailUtil.SendEmail("改善通知", "请上传改善计划到网址：" + url, list);
                            MessageBox.Show("通知成功");
                        }
                        else if (flag == "1")
                        {
                            //已通知，供应商是否
                            MessageBox.Show("已通知");
                        }
                        else if (flag == "2")
                        {
                            //已通知，供应商是否
                            MessageBox.Show("已同意改善");
                        }
                    }
                    else {
                        MessageBox.Show("此供应商不需要改善");
                        }
                    }
                //操作
                if (this.ImprovementReportTable.Columns[e.ColumnIndex].Name == "邮件通知")
                {
                    if (propose.Equals("已提交"))
                    {
                        DialogResult dr = MessageBox.Show("供应商已提交改善报告，是否查看？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (DialogResult.OK == dr)
                        {

                            //查看改善报告
                            if (checkImprovementReport == null || checkImprovementReport.IsDisposed)
                            {
                                checkImprovementReport = new CheckImprovementReportForm(MtGroupName,companyID, email, companyName, propose);
                            }
                            checkImprovementReport.Show();
                        }
                        
                    }
                    else if(propose.Equals("已改善"))
                    {

                        MessageBox.Show("通知失效，因为此供应商改善报告已认可");

                    }
                }
            }
            else
            {
                //....
            }
        }
        /// <summary>
        /// 载入数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitImprovementReportForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }


        private void LoadData() {
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findAllImprovementReportSupplier(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, this.pageNext1.PageIndex, out pageCount);
            //2018/3/20

            float a = 0;
            for (int i = 0; i < userInforTable.Rows.Count; i++)
            {
                a = float.Parse(userInforTable.Rows[i]["rate"].ToString());
                if (a >= 0.9)
                {
                    userInforTable.Rows[i]["rate"] = a * 100 + "%(A)";
                }
                else if (a >= 0.8)
                {

                    userInforTable.Rows[i]["rate"] = a * 100 + "%(B)";
                }
                else
                {
                    userInforTable.Rows[i]["rate"] = a * 100 + "%(C)";
                }
            }
            //end

            DataTable dtName = gn.getEClassMtName();
            if (dtName.Rows.Count > 0)
            {
                for (int i = 0; i < dtName.Rows.Count; i++)
                {
                    for (int j = 0; j < userInforTable.Rows.Count; j++)
                    {
                        if (userInforTable.Rows[j][9].ToString().Trim().Equals(dtName.Rows[i][0].ToString().Trim()))
                        {
                            locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, companyName + "待改善");
                        }
                    }
                }
            }
            this.ImprovementReportTable.AutoGenerateColumns = false;
            this.ImprovementReportTable.DataSource = userInforTable;
            //记录总数
            pageNext1.DrawControl(pageCount);

        }
    }
}
