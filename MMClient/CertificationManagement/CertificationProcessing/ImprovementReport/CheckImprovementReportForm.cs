﻿using Lib.Bll.CertificationProcess.General;
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//2018/3/19
using MMClient.progress;
using Lib.Common.CommonUtils;
using MMClient.CommFileShow;
using System.IO;
using Lib.SqlServerDAL.CertificationProcess;
//2018/3/19
namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class CheckImprovementReportForm : Form
    {
        FileShowForm fileShowForm = null;
        private LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm;
        private LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
        private LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
        string companyID;
        string email;
        
        FTPHelper fTPHelper = new FTPHelper("");
        string companyName;
        public CheckImprovementReportForm(string MtGroupName , string companyID,string email,string companyName,string propose)
        {
           
            InitializeComponent();
            //2018/3/19
            if (propose.Equals("已改善"))
            {
                button1.Visible = false;
                button2.Visible = false;
                button4.Visible = false;
                textBox1.Visible = false;
                label2.Visible = false;

            }
            //end  //2018/3/19
            this.companyID = companyID;
            this.email = email;
            this.companyName = companyName;

            getFile(companyID+"//improve", this.checkedListBoxImprovement);


           
        }

        /// <summary>
        /// 获得文件
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="checkedListBox"></param>
        private void getFile(string filepath, CheckedListBox checkedListBox)
        {

            string[] filenames = fTPHelper.GetFileList(filepath);
            if (filenames == null) return; 
            for (int i = 0; i < filenames.Length; i++)
                checkedListBox.Items.Add(filenames[i]);

        }
        //同意改善计划
        private void button1_Click(object sender, EventArgs e)
        {
            locationalEvaluationBLL.setSupplierLevelValueABySupplierID(companyID);
            locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID,companyName+"待改善");
            this.Close();
            MessageBox.Show("点击重置以刷新数据");
        }
        //驳回改善计划
        private void button2_Click(object sender, EventArgs e)
        {
            if (localtionEvaluationKeyPointForm == null || localtionEvaluationKeyPointForm.IsDisposed)
            {
                localtionEvaluationKeyPointForm = new LocaltionEvaluationKeyPointForm(companyID,email,3);
            }
            localtionEvaluationKeyPointForm.Show();
        }
        //下载改善报告
        private void button3_Click(object sender, EventArgs e)
        {
            downLoad(this.companyID + "//improve//", this.checkedListBoxImprovement);
        }
        public void downLoad(string filePath, CheckedListBox checkedListBox)
        {
            string mess = "请选择要下载的文件";

            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            BrowDialog.ShowDialog();

            string pathname = BrowDialog.SelectedPath;
            try
            {
                if (pathname != null)
                {
                    for (int i = 0; i < checkedListBox.Items.Count; i++)
                    {


                        if (checkedListBox.GetItemChecked(i))
                        {
                            string filename = checkedListBox.GetItemText(checkedListBox.Items[i]);

                            mess = fTPHelper.Download(pathname, filename, filePath + filename);

                        }

                    }
                    DialogResult dr = MessageBox.Show(mess, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);


                }

            }
            catch (Exception ex)
            {

                DialogResult dr = MessageBox.Show(mess, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            }
        }

        //2018/3/19

        private void textBox1_Click(object sender, EventArgs e)
        {
           
                string[] resultFile = null;
                this.textBox1.Text = "";
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Multiselect = true;
                openFileDialog1.InitialDirectory = "D:\\Patch";
                openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    resultFile = openFileDialog1.FileNames;
                    this.label1.Text = "";
                for (int i = 0; i < resultFile.Length; i++)
                    if (i == resultFile.Length - 1)
                        this.textBox1.Text += resultFile[i];
                    else {
                        this.textBox1.Text += resultFile[i]+";";
                    }
                }


            
        }
    
        private void button4_Click(object sender, EventArgs e)
        {
            
            string[] fileName = this.textBox1.Text.Split(';');
      

                progreBar progreBar = new progreBar(fTPHelper, fileName, "/" + this.companyID + "/improve");
            //插入文件信息
            string filePath = "/" + this.companyID + "/improve";
            for (int i = 0; i < fileName.Length; i++)
            {
                FileInfo fileInf = new FileInfo(fileName[i]);
                FileUploadIDAL.InsertFileInfo(this.companyID, fileInf.Name, "improve", SingleUserInstance.getCurrentUserInstance().Username, "主审", filePath + "/");

            }
            progreBar.Show();
        }
        //end2018/3/19
        private void button5_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            string filePath = this.companyID + "//improve//";

            for (int i = 0; i < checkedListBoxImprovement.Items.Count; i++)
            {
                if (checkedListBoxImprovement.GetItemChecked(i))
                {
                    string filename = checkedListBoxImprovement.GetItemText(checkedListBoxImprovement.Items[i]);
                    fileIDList.Add(filename);

                }

            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];

            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try
            {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }



            }
            catch (Exception)
            {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
