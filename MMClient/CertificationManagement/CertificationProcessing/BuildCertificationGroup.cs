﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class BuildCertificationGroup : Form
    {
        private string id;
        private string sname;
        DataGridViewCheckBoxColumn ChCol = new DataGridViewCheckBoxColumn();
      
        public BuildCertificationGroup(string id,string sname)
        {
            this.id = id;
            this.sname = sname;
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }


        
        private void BuildCertificationGroup_Load(object sender, EventArgs e)
        {
            //textBox1.Text = this.id;
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfo2(this.id);
            dataGridView1.DataSource = dt;
           
            ChCol.Name = "CheckBoxRow";
            ChCol.HeaderText = "操作";
            ChCol.Width = 50;
            ChCol.TrueValue = "1";
            ChCol.FalseValue = "0";
           
            dataGridView1.Columns.Add(ChCol);
          
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        private void dataGridView2_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {
                

                if (dataGridView2.Columns[e.ColumnIndex].Name == "delete")//单击删除对应事件
                {
                    dataGridView2.Rows.Remove(dataGridView2.CurrentRow);

                }

            }

        }
        private void button1_Click(object sender, EventArgs e)//查询
        {
           
            string name = "";
            string purchase = "";

            purchase = comboBox1.Text;
            name = textBox1.Text;
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetInfo2(purchase, name,this.id);
            dataGridView1.DataSource = dt;
            ChCol.Name = "CheckBoxRow";
            ChCol.HeaderText = "操作";
            ChCol.Width = 50;
            ChCol.TrueValue = "1";
            ChCol.FalseValue = "0";
            dataGridView1.Columns.Remove("CheckBoxRow");
            dataGridView1.Columns.Add(ChCol);
        }

        private void button3_Click(object sender, EventArgs e)//建立评估小组
        {
            dataGridView2.Rows.Clear();
          
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {//this.userTableEvaluators.Rows[e.RowIndex].Cells[0].Value = true
               
                Object o = dataGridView1.Rows[i].Cells["CheckBoxRow"].EditedFormattedValue;
                if ((bool)dataGridView1.Rows[i].Cells["CheckBoxRow"].EditedFormattedValue == true)
                {
                    object[] value = new object[dataGridView1.Columns.Count-1];
                    for (int j = 0; j < dataGridView1.Columns.Count-1; j++)
                    {
                       value[j] = dataGridView1.Rows[i].Cells[j].Value;
                    }
                    //dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                    dataGridView2.Rows.Add(value);
                   // m++;
                }
            }
           
        }

        private void button4_Click(object sender, EventArgs e)//通知评估小组
        {
            GeneralBLL gn = new GeneralBLL();
            if (gn.GetStatus1(id))
            {
                List<string> list = new List<string>();
                string obj, obj1, obj2, obj3, obj4, obj5;
                string zzid = this.sname + "待预评审";
                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    obj = dataGridView2.Rows[i].Cells[6].Value.ToString();
                    list.Add(obj);
                    obj = null;

                }

                DataTable lst = gn.GetAll(id);
                string thing = lst.Rows[0][16].ToString();
                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    obj1 = this.id;//供应商编号
                    obj2 = thing;//物料组
                    obj3 = dataGridView2.Rows[i].Cells[0].Value.ToString();//评审员编号
                    obj4 = "0";//评审员状态
                    obj5 = dataGridView2.Rows[i].Cells[3].Value.ToString();//评估方面
                                                                           //list.Add(obj);
                    gn.updateEvaluate(obj1, obj2, obj3, obj4, obj5);
                 
                   
                   
                    //gn.adeletetask(zzid);
                    gn.inserttask(dataGridView2.Rows[i].Cells["评审员编号"].Value.ToString(), zzid);//插入待办事项
                    obj1 = null;
                    obj2 = null;
                    obj3 = null;
                    obj4 = null;
                    obj5 = null;
                }
                for (int i = 0; i < list.Count; i++)
                {
                    string body = "请尽快评估";
                    List<MailAddress> addresses = new List<MailAddress>();
                    string subject = "评审通知邮件";
                    string toAddresses = list[i];
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                                    /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                        for (int i = 0; i < toAddresses.Count; i++)*/
                    message.To.Add(toAddresses);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.Body = body;
                    //Attachment 附件
                    //Attachment att = new Attachment(@"E:/迅雷下载/20151229214321.xls");
                    //message.Attachments.Add(att);//添加附件
                    message.Priority = System.Net.Mail.MailPriority.High;
                    SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
                    client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
                    client.EnableSsl = true; //必须经过ssl加密 
                    try
                    {
                        client.Send(message);

                        //MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

                    }
                    catch (Exception ee)
                    {
                        MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

                    }
                }
                string sname1 = sname + "待主审预评";
                //gn.Updatetask1(SingleUserInstance.getCurrentUserInstance().User_ID, sname1);
                gn.Updatetask(SingleUserInstance.getCurrentUserInstance().User_ID, sname1);
                MessageUtil.ShowTips("各评审员已通知");
            }
            else
            {
                string zzid = this.sname + "待预评审";
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                DialogResult dr = MessageBox.Show("确定要重新通知吗?", "重新通知", messButton);
                if (dr == DialogResult.OK)//如果点击“确定”按钮
                {
                    
                    gn.adeletetask1(zzid);
                    gn.deleteInfo(id);
                    List<string> list = new List<string>();
                    string obj, obj1, obj2, obj3, obj4, obj5;
                    for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
                    {
                        obj = dataGridView2.Rows[i].Cells[6].Value.ToString();
                        list.Add(obj);
                        obj = null;

                    }


                    DataTable lst = gn.GetAll(id);
                    string thing = lst.Rows[0][16].ToString();
                    for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
                    {
                        obj1 = this.id;//供应商编号
                        obj2 = thing;//物料组
                        obj3 = dataGridView2.Rows[i].Cells[0].Value.ToString();//评审员编号
                        obj4 = "0";//评审员状态
                        obj5 = dataGridView2.Rows[i].Cells[3].Value.ToString();//评估方面
                                                                               //list.Add(obj);
                        gn.updateEvaluate(obj1, obj2, obj3, obj4, obj5);
                        gn.inserttask(dataGridView2.Rows[i].Cells["评审员编号"].Value.ToString(), zzid);//插入待办事项
                        obj1 = null;
                        obj2 = null;
                        obj3 = null;
                        obj4 = null;
                        obj5 = null;
                    }
                    for (int i = 0; i < list.Count; i++)
                    {
                        string body = "请尽快评估";
                        List<MailAddress> addresses = new List<MailAddress>();
                        string subject = "评审通知邮件";
                        string toAddresses = list[i];
                        MailMessage message = new MailMessage();
                        message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                                        /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                            for (int i = 0; i < toAddresses.Count; i++)*/
                        message.To.Add(toAddresses);
                        message.Subject = subject;
                        message.IsBodyHtml = true;
                        message.BodyEncoding = System.Text.Encoding.UTF8;
                        message.Body = body;
                        //Attachment 附件
                        //Attachment att = new Attachment(@"E:/迅雷下载/20151229214321.xls");
                        //message.Attachments.Add(att);//添加附件
                        message.Priority = System.Net.Mail.MailPriority.High;
                        SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
                        client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
                        client.EnableSsl = true; //必须经过ssl加密 
                        try
                        {
                            client.Send(message);

                            //MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

                        }
                        catch (Exception ee)
                        {
                            MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

                        }
                    }
                    string sname1 = sname + "待主审预评";
                    gn.Updatetask(SingleUserInstance.getCurrentUserInstance().User_ID, sname1);
                    MessageUtil.ShowTips("各评审员已通知");
                }
                else//如果点击“取消”按钮
                {

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)//刷新
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfo2(this.id);
            dataGridView1.DataSource = dt;
            ChCol.Name = "CheckBoxRow";
            ChCol.HeaderText = "操作";
            ChCol.Width = 50;
            ChCol.TrueValue = "1";
            ChCol.FalseValue = "0";
            dataGridView1.Columns.Remove("CheckBoxRow");
            dataGridView1.Columns.Add(ChCol);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
