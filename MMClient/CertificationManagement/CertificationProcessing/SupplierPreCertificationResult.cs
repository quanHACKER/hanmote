﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;
//using His.WebService.Utility;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL.CertificationProcess;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class SupplierPreCertificationResult : Form
    {
        private string id;
        private string thing;
        string smtName;
        FTPHelper fTPHelper = new FTPHelper("");
        public SupplierPreCertificationResult(string id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)//提交到现场评估
        {
            string reason = richTextBox1.Text;

            GeneralBLL gn = new GeneralBLL();
            if (gn.Get1(this.id))
            {
                //gn.RejectInf(this.id0);//处理状态置1和旧的
               // DataTable dt = gn.GetAllInfo();

                nextCertification1 nm = new nextCertification1(this.email.Text, this.id,reason,this.thing, companyname.Text);
                nm.Show();

            }
            else
            {
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                DialogResult dr = MessageBox.Show("您已经执行过此操作要重新执行吗?", "重新执行", messButton);
                if (dr == DialogResult.OK)//如果点击“确定”按钮
                {
                    gn.restart(this.id);
                    nextCertification1 nm = new nextCertification1(this.email.Text, this.id, reason,this.thing, companyname.Text);
                    nm.Show();
                }
                else//如果点击“取消”按钮
                {

                }
            }
            /*MessageUtil.ShowWarning("处理完成！");
            return;*/
        }

        private void SupplierPreCertificationResult_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable lst = gn.GetAll(id);
            string temp = null;
            dbs.Text= lst.Rows[0][2].ToString();
            companyname.Text= lst.Rows[0][1].ToString();
            contactname.Text= lst.Rows[0][8].ToString();
            position.Text= lst.Rows[0][9].ToString();
            phone.Text= lst.Rows[0][11].ToString();
            tax.Text= lst.Rows[0][13].ToString();
            email.Text= lst.Rows[0][10].ToString();
            website.Text= lst.Rows[0][12].ToString();
            address.Text= lst.Rows[0][7].ToString();
            postcode.Text= lst.Rows[0][6].ToString();
            smtName = lst.Rows[0][16].ToString();
            this.thing= lst.Rows[0][16].ToString();
            DataTable level = gn.GetThingLevel(this.thing);
            if(level==null)
            {
                MessageBox.Show("未定义"+this.thing+"物料组");
            }
            else
            {
                temp= level.Rows[0][0].ToString();
                //条件不明确
                if(temp.Equals("D"))
                {
                    button3.Visible = false;
                }
                else
                {
                    button6.Visible = false;
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)//上传SSEM文件
        {
            this.fTPHelper.MakeDir(this.id);
            string[] resultFile = null;
            FTPHelper fTPHelper1 = new FTPHelper(this.id);
            fTPHelper1.MakeDir("Leader");
            FTPHelper fTPHelper2 = new FTPHelper(this.id + "/Leader");
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            string filePath = "/" + this.id + "/Leader";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;

            }
            if (resultFile == null)
                MessageBox.Show("未选择文件");
            else
            {
                for (int i = 0; i < resultFile.Length; i++)
                    fTPHelper2.Upload(resultFile[i]);
                MessageUtil.ShowWarning("上传成功！");
                string file = "";
                if (resultFile != null)
                {
                    for (int i = 0; i < resultFile.Length; i++)
                    {
                        string[] sArray5 = Regex.Split(resultFile[i], @"\\", RegexOptions.IgnoreCase);
                        file = file + ";" + sArray5[sArray5.Length - 1];
                        FileUploadIDAL.InsertFileInfo(this.id, sArray5[sArray5.Length - 1], "Leader", SingleUserInstance.getCurrentUserInstance().Username, "评审员", filePath + "/");

                    } 
                }
                filename1.Text = file;
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)//拒绝
        {
            
            GeneralBLL gn = new GeneralBLL();
            denyFormprez nm = new denyFormprez(this.email.Text, this.id);
            nm.Show();

        }

        private void button4_Click(object sender, EventArgs e)//
        {
            checkdetails nm = new checkdetails(this.id, companyname.Text);
            nm.Show();

        }

        private void button6_Click(object sender, EventArgs e)//重置
        {

        }

        private void button1_Click(object sender, EventArgs e)//评估员文件下载
        {
            FTPHelper fTPHelper9 = new FTPHelper(this.id + "/PreEvalFile" + "/Evaluation");
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";

            if (BrowDialog.ShowDialog() == DialogResult.OK) {

                string pathname = BrowDialog.SelectedPath;
                string[] filenames = fTPHelper9.GetFileList();
                string file = "";
                if (filenames != null)
                {
                    for (int i = 0; i < filenames.Length; i++)
                    {
                        file = file + ";" + filenames[i];

                    }
                }
                filename.Text = file;
                if (filenames == null)
                    MessageBox.Show("没有评估文件");
                else
                {
                    try
                    {
                        if (pathname != null)
                        {
                            for (int i = 0; i < filenames.Length; i++)
                            {
                                fTPHelper.Download(pathname, filenames[i]);

                            }
                            MessageBox.Show("下载成功");


                        }

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);

                    }
                }
            }
            
        }

        private void button6_Click_1(object sender, EventArgs e)//提交高层
        {
            string reason = richTextBox1.Text;

            GeneralBLL gn = new GeneralBLL();
            if (gn.Get1(this.id))
            {
                //gn.RejectInf(this.id0);//处理状态置1和旧的
                // DataTable dt = gn.GetAllInfo();

                nextCertification2 nm = new nextCertification2(smtName,this.email.Text, this.id,this.thing, richTextBox1.Text, companyname.Text);
                nm.Show();

            }
            else
            {
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                DialogResult dr = MessageBox.Show("您已经执行过此操作要重新执行吗?", "重新执行", messButton);
                if (dr == DialogResult.OK)//如果点击“确定”按钮
                {
                    gn.restart(this.id);
                    nextCertification2 nm = new nextCertification2("we",this.email.Text, this.id, this.thing,richTextBox1.Text, companyname.Text);
                    nm.Show();
                }
                else//如果点击“取消”按钮
                {

                }
            }
            /*MessageUtil.ShowWarning("处理完成！");
            return;*/
        }

        private void filename_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
