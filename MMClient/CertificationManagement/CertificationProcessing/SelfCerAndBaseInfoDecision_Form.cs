﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.Model;
using Lib.Bll;
using MMClient.CommonForms;

namespace MMClient
{
    public partial class SelfCerAndBaseInfoDecision_Form : Form
    {
        private SupplierManagementBLL sbll = new SupplierManagementBLL();
        private OrganizationInfo organizationInfo;
        private List<OrgContacts> list;
        private string companyName;

        public SelfCerAndBaseInfoDecision_Form()
        {
            InitializeComponent();
        }

        public SelfCerAndBaseInfoDecision_Form(OrganizationInfo oi)
        {
            InitializeComponent();
            this.organizationInfo = oi;
        }
        public SelfCerAndBaseInfoDecision_Form(List<OrgContacts> list,string companyName)
        {
            InitializeComponent();
            this.list = list;
            this.companyName = companyName;
        }
        /// <summary>
        /// 发送评审邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendDecision_btn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.suppliername_txt.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if(string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if(string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            List<MailAddress> addresses = new List<MailAddress>();
            if (this.list != null && this.list.Count > 0)
            {
                for (int i = 0; i < list.Count;i++ )
                    addresses.Add(new MailAddress(this.list[i].Email));
            }
            string body = this.describe_rtb.Text;
            string subject = "自评结果和供应商基本信息的评估决策";
            if (EmailUtil.SendEmail(subject, body, addresses))
            {
                string msg = "";
                for (int i = 0; i < list.Count; i++)
                    msg += this.list[i].Email+",";
                msg = msg.Substring(0,msg.Length-1);
                MessageUtil.ShowTips("评估决策结果邮件已经成功发送到" + msg + "！");
            }
        }

        /// <summary>
        /// 保存评审结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.suppliername_txt.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            DecisionInfo di = new DecisionInfo();
            di.SupplierName = this.suppliername_txt.Text;
            di.Descriptions = this.describe_rtb.Text;
            di.DecisionResult = this.decision_cmb.Text.ToString();
            ///暂时
            di.Status = 1;

            bool flag = sbll.saveSelfCerAndBaseInfoDecision(di);
            if (flag)
            {
                MessageUtil.ShowTips("保存成功！");
            }
            else 
            {
                MessageUtil.ShowTips("保存失败！");
            }
        }

        private void SelfCerAndBaseInfoDecision_Form_Load(object sender, EventArgs e)
        {
            this.suppliername_txt.Text = this.companyName;
            //查看是否有评查结果
            //BLLFactory<SupplierManagementBLL>.Instance.
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.suppliername_txt.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            string supplierName = this.suppliername_txt.Text;
            Dictionary<string, string> downloadRLDic = new Dictionary<string, string>();
            double allFilesSize = 120000;
            string filePathOnServer = BLLFactory<SupplierManagementBLL>.Instance.GetFileOnServerPath("Admission_SelfCertification",supplierName) ;
            if (string.IsNullOrWhiteSpace(filePathOnServer))
            {
                MessageUtil.ShowWarning("供应商自评结果为上传请及时通知供应商！");
                return;
            }
            ////选择保存的文件夹
            //FolderBrowserDialog downloadFileFolder = new FolderBrowserDialog();
            //downloadFileFolder.Description = "请选择文件夹";
            ////允许用户在浏览的时候create new folder
            //downloadFileFolder.ShowNewFolderButton = false;

            //DialogResult result = downloadFileFolder.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    //格式: E://oil
            //    string selectedPath = downloadFileFolder.SelectedPath;
            //    selectedPath = selectedPath.Replace("\\", "/");
            //    downloadRLDic.Add(filePathOnServer, selectedPath);
            //}
            downloadRLDic.Add(filePathOnServer, "G:/downloadhanmote");
            //allFilesSize = 
            LinkedList<string> uploadFailList = new LinkedList<string>();
            ProgressForm progress = new ProgressForm(uploadFailList);
            progress.RLFileDic = downloadRLDic;
            progress.AllFilesSize = allFilesSize;
            progress.OpType = "download";
            progress.ShowDialog();           
        }

        

    }
}
