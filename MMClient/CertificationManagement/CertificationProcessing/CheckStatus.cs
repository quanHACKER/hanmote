﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class CheckStatus : Form
    {
        private string id;
        public CheckStatus(string id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void CheckStatus_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfo3(this.id);
            dataGridView1.DataSource = dt;
            DataGridViewCheckBoxColumn ChCol = new DataGridViewCheckBoxColumn();
            ChCol.Name = "CheckBoxRow";
            ChCol.HeaderText = "操作";
            ChCol.Width = 50;
            ChCol.TrueValue = "1";
            ChCol.FalseValue = "0";
            //dataGridView1.Columns.Insert(0, ChCol);
            dataGridView1.Columns.Add(ChCol);
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        private void button1_Click(object sender, EventArgs e)//给未完成的评估员发邮件催促完成
        {
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if ((bool)dataGridView1.Rows[i].Cells[7].EditedFormattedValue == true)
                {
                    obj = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    list.Add(obj);
                    obj = null;
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                string body = "请在"+ dateTimePicker1.Text+"之前完成评估！";
                List<MailAddress> addresses = new List<MailAddress>();
                string subject = "未完成预评催促邮件";
                string toAddresses = list[i];
                MailMessage message = new MailMessage();
                message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                                /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                    for (int i = 0; i < toAddresses.Count; i++)*/
                message.To.Add(toAddresses);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Body = body;
                //Attachment 附件
                //Attachment att = new Attachment(@"E:/迅雷下载/20151229214321.xls");
                //message.Attachments.Add(att);//添加附件
                message.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
                client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
                client.EnableSsl = true; //必须经过ssl加密 
                try
                {
                    client.Send(message);

                    //MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

                }
                catch (Exception ee)
                {
                    MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

                }
            }
            MessageUtil.ShowTips("各未完成评审员已通知");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
