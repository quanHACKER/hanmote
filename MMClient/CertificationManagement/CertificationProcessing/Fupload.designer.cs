﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class Fupload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.filename = new System.Windows.Forms.TextBox();
            this.filename1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pur_Select = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mt_Select = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "采购策略上传";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // filename
            // 
            this.filename.Location = new System.Drawing.Point(166, 119);
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(291, 21);
            this.filename.TabIndex = 1;
            // 
            // filename1
            // 
            this.filename1.Location = new System.Drawing.Point(166, 163);
            this.filename1.Name = "filename1";
            this.filename1.Size = new System.Drawing.Size(291, 21);
            this.filename1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 162);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "市场分析文件上传";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(234, 331);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "返  回";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 60);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "采购组织";
            // 
            // pur_Select
            // 
            this.pur_Select.Enabled = false;
            this.pur_Select.FormattingEnabled = true;
            this.pur_Select.Location = new System.Drawing.Point(93, 57);
            this.pur_Select.Name = "pur_Select";
            this.pur_Select.Size = new System.Drawing.Size(121, 20);
            this.pur_Select.TabIndex = 10;
            this.pur_Select.SelectedValueChanged += new System.EventHandler(this.pur_Select_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "物料组";
            // 
            // mt_Select
            // 
            this.mt_Select.FormattingEnabled = true;
            this.mt_Select.Location = new System.Drawing.Point(298, 57);
            this.mt_Select.Name = "mt_Select";
            this.mt_Select.Size = new System.Drawing.Size(121, 20);
            this.mt_Select.TabIndex = 12;
            // 
            // Fupload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 366);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mt_Select);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pur_Select);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.filename1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Fupload";
            this.Text = "相关文件上传";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox filename;
        private System.Windows.Forms.TextBox filename1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox pur_Select;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox mt_Select;
    }
}