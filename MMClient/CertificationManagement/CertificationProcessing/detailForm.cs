﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using System.IO;
using System.Text.RegularExpressions;
//using His.WebService.Utility;
//using WindowsFormsApp2;
using Lib.SqlServerDAL;


namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class detailForm : Form
    {
        private string supplierId;
        private string zid;
        private string suppliername;

        
        FTPHelper fTPHelper = new FTPHelper("");
        public detailForm(string supplier_name,string dbs,string country,string province,string city,string address,string postcode,string contactname,string position,string email,string phonenumber,string website,string fox,string rich,string org,string thing,string structure,string style,string volume,string device,string product,string trade,string condition,string certification,string other,string id,string zid, string suppliername, string name, string plan)
        {
            this.supplierId = id;
            this.suppliername = suppliername;
            this.zid = zid;
            InitializeComponent();
            suppliername_textBox.Text = supplier_name;
            dbscode_textBox.Text = dbs;
            country_textBox.Text = country;
            province_textBox.Text = province;
            city_textBox.Text = city;
            address_textBox.Text = address;
            postcode_textBox.Text = postcode;
            contactname_textBox.Text = contactname;
            position_textBox.Text = position;
            email_textBox.Text = email;
            phonenumber_textBox.Text = phonenumber;
            website_textBox.Text = website;
            fax_textBox.Text = fox;
            richTextBox1.Text = rich;
            org_textBox.Text = org;
            thing_textBox.Text = thing;
            struture_textBox.Text = structure;
            style_textBox.Text = style;
            textBo2.Text = name;
            textBo3.Text = plan;

            label2017.Text= (int.Parse(DateTime.Now.Year.ToString()) - 1).ToString();
            label2016.Text= (int.Parse(DateTime.Now.Year.ToString())-2).ToString();
            label2015.Text= (int.Parse(DateTime.Now.Year.ToString())-3).ToString();
            label2014.Text= (int.Parse(DateTime.Now.Year.ToString())-4).ToString();
            label2013.Text= (int.Parse(DateTime.Now.Year.ToString())-5).ToString();
            

            string[] sArray = Regex.Split(volume, "&", RegexOptions.IgnoreCase);//年营业额模块
            t1.Text = sArray[0];
            t2.Text = sArray[1];
            t3.Text = sArray[2]; 
            t4.Text = sArray[3];
            t5.Text = sArray[4];
            t6.Text = sArray[5];
            t7.Text = sArray[6];
            t8.Text = sArray[7]; 
            t9.Text = sArray[8];
            t10.Text = sArray[9];
            t11.Text = sArray[10];
            t12.Text = sArray[11];
            t13.Text = sArray[12];
            t14.Text = sArray[13];
            t15.Text = sArray[14];
            string[] sArray1 = device.Split(new char[2] { '&', '^' });//公司设施模块
            te1.Text = sArray1[0];
            if(sArray1[1].Equals("true"))
                 c2.Checked = true;
            else
                c2.Checked = false;
            if (sArray1[2].Equals("true"))
                c3.Checked = true;
            else
                c3.Checked = false;
            if (sArray1[3].Equals("true"))
                c4.Checked = true;
            else
                c4.Checked = false;
            if (sArray1[4].Equals("true"))
                c5.Checked = true;
            else
                c5.Checked = false;
            /* if (sArray1[5].Equals("true"))
                 c6.Checked = true;
             else
                 c6.Checked = false;*/
            te6.Text = sArray1[5];
            te7.Text = sArray1[6];
            if (sArray1[7].Equals("true"))
                c8.Checked = true;
            else
                 c8.Checked = false;
            if (sArray1[8].Equals("true"))
                c9.Checked = true;
            else
                 c9.Checked = false;
            if (sArray1[9].Equals("true"))
                c10.Checked = true;
             else
                c10.Checked = false;
            if (sArray1[10].Equals("true"))
                c11.Checked = true;
            else
                c11.Checked = false;
            /*if (sArray1[11].Equals("true"))
                c12.Checked = true;
            else
                c12.Checked = false;*/
            te12.Text = sArray1[11];
            te13.Text = sArray1[12];
            if (sArray1[13].Equals("true"))
                c14.Checked = true;
            else
                c14.Checked = false;
            if (sArray1[14].Equals("true"))
                c15.Checked = true;
            else
                c15.Checked = false;
            if (sArray1[15].Equals("true"))
                c16.Checked = true;
            else
                c16.Checked = false;
            if (sArray1[16].Equals("true"))
                c17.Checked = true;
            else
                c17.Checked = false;
            /*if (sArray1[17].Equals("true"))
                c18.Checked = true;
            else
                c18.Checked = false;*/
            te18.Text = sArray1[17];
            te19.Text = sArray1[18];
            if (sArray1[19].Equals("true"))
                c20.Checked = true;
            else
                c20.Checked = false;
            if (sArray1[20].Equals("true"))
                c21.Checked = true;
            else
                c21.Checked = false;
            if (sArray1[21].Equals("true"))
                c22.Checked = true;
            else
                c22.Checked = false;
            if (sArray1[22].Equals("true"))
                c23.Checked = true;
            else
                c23.Checked = false;
            /*if (sArray1[23].Equals("true"))
                c24.Checked = true;
            else
                c24.Checked = false;*/
            te24.Text = sArray1[23];
            te25.Text = sArray1[24];
            if (sArray1[25].Equals("true"))
                c26.Checked = true;
            else
                c26.Checked = false;
            if (sArray1[26].Equals("true"))
                c27.Checked = true;
            else
                c27.Checked = false;
            if (sArray1[27].Equals("true"))
                c28.Checked = true;
            else
                c28.Checked = false;
            if (sArray1[28].Equals("true"))
                c29.Checked = true;
            else
                c29.Checked = false;
            /*if (sArray1[29].Equals("true"))
                c30.Checked = true;
            else
                c30.Checked = false;*/
            te30.Text = sArray1[29];
            string[] sArray2 = product.Split(new char[2] { '&', '^' });//主要产品模块
            tex1.Text = sArray2[0];
            tex2.Text = sArray2[1];
            tex3.Text = sArray2[2];
            tex4.Text = sArray2[3];
            tex5.Text = sArray2[4];
            tex6.Text = sArray2[5];
            tex7.Text = sArray2[6];
            tex8.Text = sArray2[7];
            tex9.Text = sArray2[8];
            tex10.Text = sArray2[9];
            string[] sArray3 = trade.Split(new char[2] { '&', '^' });//客户行业模块
            textB1.Text = sArray3[0];
            textB2.Text = sArray3[1];
            textB3.Text = sArray3[2];
            textB4.Text = sArray3[3];
            textB5.Text = sArray3[4];
            textB6.Text = sArray3[5];
            textB7.Text = sArray3[6];
            textB8.Text = sArray3[7];
            textB9.Text = sArray3[8];
            textB10.Text = sArray3[9];
            textB11.Text = sArray3[10];
            textB12.Text = sArray3[11];
            textB13.Text = sArray3[12];
            textB14.Text = sArray3[13];
            textB15.Text = sArray3[14];
            textB16.Text = sArray3[15];
            textB17.Text = sArray3[16];
            textB18.Text = sArray3[17];
            textB19.Text = sArray3[18];
            textB20.Text = sArray3[19];
            textB21.Text = sArray3[20];
            textB22.Text = sArray3[21];
            textB23.Text = sArray3[22];
            textB24.Text = sArray3[23];
            textB25.Text = sArray3[24];
            string[] sArray4 = condition.Split(new char[2] { '&', '^' });//公司所具备条件模块
            text1.Text = sArray4[0];
            text2.Text = sArray4[1];
            text3.Text = sArray4[2];
            text4.Text = sArray4[3];
            text5.Text = sArray4[4];
            text6.Text = sArray4[5];
            text7.Text = sArray4[6];
            text8.Text = sArray4[7];
            string[] sArray5 = Regex.Split(certification, "&", RegexOptions.IgnoreCase);//公司已拥有证书模块
            
            if (sArray5[0].Equals("true"))
                ch1.Checked = true;
            else
                ch1.Checked = false;
            if (sArray5[1].Equals("true"))
                ch2.Checked = true;
            else
                ch2.Checked = false;
            if (sArray5[2].Equals("true"))
                ch3.Checked = true;
            else
                ch3.Checked = false;
            if (sArray5[3].Equals("true"))
                ch4.Checked = true;
            else
                ch4.Checked = false;
            if (sArray5[4]!=null)
                textBo1.Text = sArray5[4];

           // button2_Click(id, email);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*string serverPath= "http://116.196.99.18:8080/Hanmote0106";
            string filePath= "C:\\";
            Download(serverPath, filePath);*/
            //System.IO.File.Copy(@"\\116.196.99.18\C\source.docx", @"E:\TEST.docx", true);
            /*Ftp ft = new Ftp();
            ft.Show();*/
        }
        public void UploadFile(string filePath, string serverPath)
        {
            //创建WebClient实例
            WebClient webClient = new WebClient();
            webClient.Credentials = CredentialCache.DefaultCredentials;
            //要上传的文件 
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] postArray = br.ReadBytes((int)fs.Length);
            Stream postStream = webClient.OpenWrite(serverPath, "PUT");
            try
            {
                if (postStream.CanWrite)
                {
                    postStream.Write(postArray, 0, postArray.Length);
                    postStream.Close();
                    fs.Dispose();
                }
                else
                {
                    postStream.Close();
                    fs.Dispose();
                }
            }
            catch (Exception ex)
            {
                postStream.Close();
                fs.Dispose();
                throw ex;
            }
            finally
            {
                postStream.Close();
                fs.Dispose();
            }
        }
        /// <summary>
        /// 下载文件方法
        /// </summary>
        /// <param name="serverPath">被下载的文件地址（服务器地址包括文件）</param>
        /// <param name="filePath">另存放的路径（本地需要存储文件的文件夹地址）</param>
        public void Download(string serverPath, string filePath)
        {
            WebClient client = new WebClient();
            string fileName = serverPath.Substring(serverPath.LastIndexOf("/") + 1); ;//被下载的文件名
            string path = filePath + fileName;//另存为地址
            try
            {
                WebRequest myre = WebRequest.Create(serverPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            try
            {
                client.DownloadFile(serverPath, fileName);
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader r = new BinaryReader(fs);
                byte[] mbyte = r.ReadBytes((int)fs.Length);
                FileStream fstr = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                fstr.Write(mbyte, 0, (int)fs.Length);
                fstr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void detailForm_Load(object sender, EventArgs e)
        {
            string str = org_textBox.Text + "/" + thing_textBox.Text;
            //公司附件显示
            showFiles(this.supplierId + "/RegisterFile/introduce",companyFileLink);
            //公司研发能力附件显示
            showFiles(this.supplierId + "/RegisterFile/condition", lk_label_studyAble);
            //采购策略显示
            showFiles("general/caigou/"+str, Lk_Label_ProcStrategy);
            //市场
            showFiles("general/gong/" + str, Lk_Label_MarketAnalysis);
        }
        /// <summary>
        /// 显示文件信息
        /// </summary>
        /// <param name="fTPHelper"></param>
        /// <param name="linkLabel"></param>
        private void showFiles(string filePath, LinkLabel linkLabel) {
            FTPHelper companyFileDownLoad = new FTPHelper(filePath);
            string[] filenames = companyFileDownLoad.GetFileList();
            string file = "";
            if (filenames != null)
            {
                for (int i = 0; i < filenames.Length; i++)
                {
                    file = filenames[i] + "\n";

                }
            }
            linkLabel.Text = file;


        }



        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label30_Click(object sender, EventArgs e)
        {

        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //detailForm nm = new detailForm(supplier_name, dbs, country, province, city, address, postcode, contactname, position, email, phonenumber, website, fox, rich, org, thing, structure, style, volume, device, product, trade, condition, certification, other, id);
            GeneralBLL gn = new GeneralBLL();
            if (gn.Get(this.supplierId))
            {
                //gn.RejectInf(this.supplierId);//处理状态置1和旧的
                //DataTable dt = gn.GetAllInfo();

                nextCerticata nm = new nextCerticata(this.email_textBox.Text,this.supplierId, this.thing_textBox.Text,this.zid,this.suppliername);
                nm.Show();

            }
            else
            {
                MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                return;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (gn.Get(this.supplierId))
            {
                //gn.RejectInf(this.supplierId);//处理状态置1和旧的
               // DataTable dt = gn.GetAllInfo();

                nextCertificationRestart nm = new nextCertificationRestart(this.email_textBox.Text, this.supplierId);
                nm.Show();

            }
            else
            {
                MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (gn.Get(supplierId))
            {
               // gn.RejectInf(supplierId);//处理状态置1和旧的
                //DataTable dt = gn.GetAllInfo();
                denyForm nm = new denyForm(this.email_textBox.Text, supplierId);
                nm.Show();

            }
            else
            {
                MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                return;
            }
        }

        private void button7_Click(object sender, EventArgs e)//公司具有条件下载
        {
            FTPHelper fTPHelper8 = new FTPHelper(this.supplierId + "/RegisterFile/condition");
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            BrowDialog.ShowDialog();
            string pathname = BrowDialog.SelectedPath;
            string[] filenames = fTPHelper8.GetFileList();
            string file = "";
            if (filenames != null)
            {
                for (int i = 0; i < filenames.Length; i++)
                {
                    file = file + ";" + filenames[i];

                }
            }
            filename4.Text = file;
            if (filenames == null)
                MessageBox.Show("供应商未上传此文件");
            else
            {
                try
                {
                    if (pathname != null)
                    {
                        for (int i = 0; i < filenames.Length; i++)
                        {
                            fTPHelper.Download(pathname, filenames[i]);

                        }
                        MessageBox.Show("下载成功");

                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b1_Click(object sender, EventArgs e)
        {
            //this.tabControl1.SelectedIndex = 1;
            this.tabControl1.SelectedTab = tabPage2;
        }

        private void b2_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage3;
        }

        private void b3_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage4;
        }

        private void b4_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage5;
        }

        private void b5_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage6;
        }

        private void b6_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage7;
        }

        private void b7_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tabPage8;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //detailForm nm = new detailForm(supplier_name, dbs, country, province, city, address, postcode, contactname, position, email, phonenumber, website, fox, rich, org, thing, structure, style, volume, device, product, trade, condition, certification, other, id);
            GeneralBLL gn = new GeneralBLL();
            if (gn.Get(this.supplierId))
            {
                //gn.RejectInf(this.supplierId);//处理状态置1和旧的
                //DataTable dt = gn.GetAllInfo();

                nextCerticata nm = new nextCerticata(this.email_textBox.Text, this.supplierId, this.thing_textBox.Text, this.zid, this.suppliername);
                nm.Show();
              
            }
            else
            {
                MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                return;
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (gn.Get(supplierId))
            {
                // gn.RejectInf(supplierId);//处理状态置1和旧的
                //DataTable dt = gn.GetAllInfo();
                denyForm nm = new denyForm(this.email_textBox.Text, supplierId);
                nm.Show();

            }
            else
            {
                MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                return;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

     

        private void button14_Click(object sender, EventArgs e)
        {
            button3_Click(sender,e);
        }
        /// <summary>
        /// 下载公司附件信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyFileLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DownFile(this.supplierId + "/RegisterFile/introduce");
        }
        /// <summary>
        /// 下载供应商研发附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lk_label_studyAble_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DownFile(this.supplierId + "/RegisterFile/condition");

        }
        /// <summary>
        /// 下载采购策略
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Lk_Label_ProcStrategy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string str = org_textBox.Text + "/" + thing_textBox.Text;
            DownFile("general/caigou/" + str);
        }
        /// <summary>
        /// 下载市场分析文档
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Lk_Label_MarketAnalysis_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string str = org_textBox.Text + "/" + thing_textBox.Text;
            DownFile("general/gong/" + str);
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="filePath">文件所在目录</param>
        private void DownFile(string filePath) {
            FTPHelper companyFileDownLoad = new FTPHelper(filePath);
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            if (BrowDialog.ShowDialog() == DialogResult.OK)
            {
                string pathname = BrowDialog.SelectedPath;
                string[] filenames = companyFileDownLoad.GetFileList();
                string file = "";
                if (filenames != null)
                {
                    for (int i = 0; i < filenames.Length; i++)
                    {
                        file = file + ";" + filenames[i];

                    }
                }
                if (filenames == null)
                    MessageUtil.ShowTips("未找到文件！");
                else
                {
                    try
                    {
                        if (pathname != null)
                        {
                            for (int i = 0; i < filenames.Length; i++)
                            {
                                fTPHelper.Download(pathname, filenames[i]);

                            }
                            MessageUtil.ShowTips("下载成功");

                        }

                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowTips(ex.Message);

                    }
                }
            }



        }

        private void Lk_Label_MarketAnalysis_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}
