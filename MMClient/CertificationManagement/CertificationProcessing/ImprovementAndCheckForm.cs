﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Model;
using Lib.Bll;
using System.Net.Mail;

namespace MMClient
{
    public partial class ImprovementAndCheckForm : DockContent
    {
        public ImprovementAndCheckForm()
        {
            InitializeComponent();
        }

        private void sendDecision_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.companyname_cmb.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            List<MailAddress> addresses = new List<MailAddress>();
            List<OrgContacts> list = BLLFactory<SupplierManagementBLL>.Instance.getOrgContactsByCompanyName(this.companyname_cmb.Text);
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                    addresses.Add(new MailAddress(list[i].Email));
            }
            string body = this.describe_rtb.Text;
            string subject = "改进/验收的评估决策";
            if (EmailUtil.SendEmail(subject, body, addresses))
            {
                string msg = "";
                for (int i = 0; i < list.Count; i++)
                    msg += list[i].Email + ",";
                msg = msg.Substring(0, msg.Length - 1);
                MessageUtil.ShowTips("改进/验收的评估决策结果邮件已经成功发送到" + msg + "！");
            }
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.companyname_cmb.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            DecisionInfo di = new DecisionInfo();
            di.SupplierName = this.companyname_cmb.Text;
            di.Descriptions = this.describe_rtb.Text;
            di.DecisionResult = this.decision_cmb.Text.ToString();
            di.Status = 4;

            bool flag = BLLFactory<SupplierManagementBLL>.Instance.saveSelfCerAndBaseInfoDecision(di);
            if (flag)
            {
                MessageUtil.ShowTips("保存成功！");
            }
            else
            {
                MessageUtil.ShowTips("保存失败！");
            }
        }

        private void ImprovementAndCheckForm_Load(object sender, EventArgs e)
        {
            DataTable companyDT = BLLFactory<SupplierManagementBLL>.Instance.getAllCompany();
            FormHelper.combox_bind(this.companyname_cmb, companyDT, "supplierChineseName", "supplierChineseName");
        }
        /// <summary>
        /// 查看行动计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
