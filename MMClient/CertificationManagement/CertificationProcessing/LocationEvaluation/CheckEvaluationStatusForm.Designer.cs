﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class CheckEvaluationStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.CheckEvaluationStatus = new System.Windows.Forms.DataGridView();
            this.localEvalstat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvaluateTime = new System.Windows.Forms.DateTimePicker();
            this.topPanel = new System.Windows.Forms.Panel();
            this.labelTimeState1 = new System.Windows.Forms.Label();
            this.labelTimeState3 = new System.Windows.Forms.Label();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.labelTimeState2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkResult = new System.Windows.Forms.Button();
            this.InformEvaGroupBtn = new System.Windows.Forms.Button();
            this.getFileName = new System.Windows.Forms.TextBox();
            this.FileUploadBtn = new System.Windows.Forms.Button();
            this.LocalAddressInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEvaluationStatus)).BeginInit();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.CheckEvaluationStatus);
            this.contentPanel.Location = new System.Drawing.Point(0, 45);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(755, 193);
            this.contentPanel.TabIndex = 1;
            // 
            // CheckEvaluationStatus
            // 
            this.CheckEvaluationStatus.AllowUserToAddRows = false;
            this.CheckEvaluationStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckEvaluationStatus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CheckEvaluationStatus.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.CheckEvaluationStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckEvaluationStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.CheckEvaluationStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CheckEvaluationStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.localEvalstat,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CheckEvaluationStatus.DefaultCellStyle = dataGridViewCellStyle2;
            this.CheckEvaluationStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.CheckEvaluationStatus.GridColor = System.Drawing.SystemColors.ControlLight;
            this.CheckEvaluationStatus.Location = new System.Drawing.Point(0, 0);
            this.CheckEvaluationStatus.Name = "CheckEvaluationStatus";
            this.CheckEvaluationStatus.RowTemplate.Height = 23;
            this.CheckEvaluationStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.CheckEvaluationStatus.Size = new System.Drawing.Size(755, 193);
            this.CheckEvaluationStatus.TabIndex = 18;
            // 
            // localEvalstat
            // 
            this.localEvalstat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.localEvalstat.DataPropertyName = "localEvalstat";
            this.localEvalstat.Frozen = true;
            this.localEvalstat.HeaderText = "状  态";
            this.localEvalstat.Name = "localEvalstat";
            this.localEvalstat.ReadOnly = true;
            this.localEvalstat.Width = 79;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn8.FillWeight = 43.61068F;
            this.dataGridViewTextBoxColumn8.Frozen = true;
            this.dataGridViewTextBoxColumn8.HeaderText = "编  号";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 105;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn10.FillWeight = 515.5145F;
            this.dataGridViewTextBoxColumn10.Frozen = true;
            this.dataGridViewTextBoxColumn10.HeaderText = "评审员";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 77;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn14.DataPropertyName = "evalCodeName";
            this.dataGridViewTextBoxColumn14.FillWeight = 12.58458F;
            this.dataGridViewTextBoxColumn14.Frozen = true;
            this.dataGridViewTextBoxColumn14.HeaderText = "评估方面";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "PurGroupName";
            this.dataGridViewTextBoxColumn15.FillWeight = 23.65793F;
            this.dataGridViewTextBoxColumn15.Frozen = true;
            this.dataGridViewTextBoxColumn15.HeaderText = "采购组织";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 102;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "email";
            this.dataGridViewTextBoxColumn11.FillWeight = 33.81242F;
            this.dataGridViewTextBoxColumn11.Frozen = true;
            this.dataGridViewTextBoxColumn11.HeaderText = "邮  箱";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 141;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "mobile";
            this.dataGridViewTextBoxColumn12.FillWeight = 17.20646F;
            this.dataGridViewTextBoxColumn12.Frozen = true;
            this.dataGridViewTextBoxColumn12.HeaderText = "移动电话";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 109;
            // 
            // EvaluateTime
            // 
            this.EvaluateTime.Location = new System.Drawing.Point(84, 490);
            this.EvaluateTime.Name = "EvaluateTime";
            this.EvaluateTime.Size = new System.Drawing.Size(122, 21);
            this.EvaluateTime.TabIndex = 15;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.button1);
            this.topPanel.Controls.Add(this.labelTimeState1);
            this.topPanel.Controls.Add(this.labelTimeState3);
            this.topPanel.Controls.Add(this.endTime);
            this.topPanel.Controls.Add(this.labelTimeState2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.checkResult);
            this.topPanel.Controls.Add(this.InformEvaGroupBtn);
            this.topPanel.Controls.Add(this.getFileName);
            this.topPanel.Controls.Add(this.FileUploadBtn);
            this.topPanel.Controls.Add(this.LocalAddressInput);
            this.topPanel.Controls.Add(this.label3);
            this.topPanel.Controls.Add(this.EvaluateTime);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(755, 380);
            this.topPanel.TabIndex = 3;
            // 
            // labelTimeState1
            // 
            this.labelTimeState1.AutoSize = true;
            this.labelTimeState1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTimeState1.ForeColor = System.Drawing.Color.Tomato;
            this.labelTimeState1.Location = new System.Drawing.Point(12, 241);
            this.labelTimeState1.Name = "labelTimeState1";
            this.labelTimeState1.Size = new System.Drawing.Size(335, 12);
            this.labelTimeState1.TabIndex = 25;
            this.labelTimeState1.Text = "*选择评审工作截止时间，以邮件方式通知评审员尽快完成评审";
            // 
            // labelTimeState3
            // 
            this.labelTimeState3.AutoSize = true;
            this.labelTimeState3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTimeState3.ForeColor = System.Drawing.Color.Tomato;
            this.labelTimeState3.Location = new System.Drawing.Point(237, 286);
            this.labelTimeState3.Name = "labelTimeState3";
            this.labelTimeState3.Size = new System.Drawing.Size(119, 12);
            this.labelTimeState3.TabIndex = 24;
            this.labelTimeState3.Text = "*默认截止时间为当天";
            // 
            // endTime
            // 
            this.endTime.Location = new System.Drawing.Point(109, 280);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(122, 21);
            this.endTime.TabIndex = 23;
            this.endTime.Value = new System.DateTime(2018, 1, 18, 0, 0, 0, 0);
            // 
            // labelTimeState2
            // 
            this.labelTimeState2.AutoSize = true;
            this.labelTimeState2.Location = new System.Drawing.Point(14, 286);
            this.labelTimeState2.Name = "labelTimeState2";
            this.labelTimeState2.Size = new System.Drawing.Size(89, 12);
            this.labelTimeState2.TabIndex = 22;
            this.labelTimeState2.Text = "评审终止时间：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 21;
            this.label1.Text = "状态信息：";
            // 
            // checkResult
            // 
            this.checkResult.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkResult.Location = new System.Drawing.Point(268, 340);
            this.checkResult.Name = "checkResult";
            this.checkResult.Size = new System.Drawing.Size(138, 25);
            this.checkResult.TabIndex = 6;
            this.checkResult.Text = "查看评审结果";
            this.checkResult.UseVisualStyleBackColor = true;
            this.checkResult.Click += new System.EventHandler(this.checkResult_Click);
            // 
            // InformEvaGroupBtn
            // 
            this.InformEvaGroupBtn.Location = new System.Drawing.Point(13, 582);
            this.InformEvaGroupBtn.Name = "InformEvaGroupBtn";
            this.InformEvaGroupBtn.Size = new System.Drawing.Size(107, 23);
            this.InformEvaGroupBtn.TabIndex = 20;
            this.InformEvaGroupBtn.Text = "通知评审小组";
            this.InformEvaGroupBtn.UseVisualStyleBackColor = true;
            // 
            // getFileName
            // 
            this.getFileName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.getFileName.Location = new System.Drawing.Point(138, 527);
            this.getFileName.Name = "getFileName";
            this.getFileName.ReadOnly = true;
            this.getFileName.Size = new System.Drawing.Size(380, 23);
            this.getFileName.TabIndex = 19;
            // 
            // FileUploadBtn
            // 
            this.FileUploadBtn.Location = new System.Drawing.Point(13, 527);
            this.FileUploadBtn.Name = "FileUploadBtn";
            this.FileUploadBtn.Size = new System.Drawing.Size(107, 23);
            this.FileUploadBtn.TabIndex = 18;
            this.FileUploadBtn.Text = "点击上传邀请函";
            this.FileUploadBtn.UseVisualStyleBackColor = true;
            // 
            // LocalAddressInput
            // 
            this.LocalAddressInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.LocalAddressInput.Location = new System.Drawing.Point(346, 488);
            this.LocalAddressInput.Name = "LocalAddressInput";
            this.LocalAddressInput.Size = new System.Drawing.Size(172, 23);
            this.LocalAddressInput.TabIndex = 17;
            this.LocalAddressInput.Tag = "";
            this.LocalAddressInput.Text = "输入评估地点";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(296, 491);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "地点：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(10, 490);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "评估时间：";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(619, 342);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 26);
            this.button1.TabIndex = 26;
            this.button1.Text = "返  回";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CheckEvaluationStatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 380);
            this.Controls.Add(this.topPanel);
            this.Name = "CheckEvaluationStatusForm";
            this.Text = "查看评审状态";
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEvaluationStatus)).EndInit();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.DateTimePicker EvaluateTime;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Button checkResult;
        private System.Windows.Forms.Button InformEvaGroupBtn;
        private System.Windows.Forms.TextBox getFileName;
        private System.Windows.Forms.Button FileUploadBtn;
        private System.Windows.Forms.TextBox LocalAddressInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTimeState2;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Label labelTimeState3;
        private System.Windows.Forms.Label labelTimeState1;
        private System.Windows.Forms.DataGridView CheckEvaluationStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn localEvalstat;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.Button button1;
    }
}