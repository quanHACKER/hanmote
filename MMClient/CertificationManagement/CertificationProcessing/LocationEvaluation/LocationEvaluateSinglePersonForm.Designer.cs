﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class LocationEvaluateSinglePersonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerPanel = new System.Windows.Forms.Panel();
            this.labelPostCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelUrlAddress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTelPhone = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelMobile = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LabelPosition = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelContactMan = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelCompanyNAme = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelDBsCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.ScoreRate = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textDeny = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtScoreSum = new System.Windows.Forms.TextBox();
            this.TxtScore = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.singleFileUpload = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.headerPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerPanel.AutoSize = true;
            this.headerPanel.Controls.Add(this.labelPostCode);
            this.headerPanel.Controls.Add(this.label8);
            this.headerPanel.Controls.Add(this.labelAddress);
            this.headerPanel.Controls.Add(this.label12);
            this.headerPanel.Controls.Add(this.labelUrlAddress);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.labelEmail);
            this.headerPanel.Controls.Add(this.label9);
            this.headerPanel.Controls.Add(this.labelTelPhone);
            this.headerPanel.Controls.Add(this.label11);
            this.headerPanel.Controls.Add(this.labelMobile);
            this.headerPanel.Controls.Add(this.label13);
            this.headerPanel.Controls.Add(this.LabelPosition);
            this.headerPanel.Controls.Add(this.label5);
            this.headerPanel.Controls.Add(this.labelContactMan);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Controls.Add(this.labelCompanyNAme);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.LabelDBsCode);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(877, 179);
            this.headerPanel.TabIndex = 2;
            // 
            // labelPostCode
            // 
            this.labelPostCode.AutoSize = true;
            this.labelPostCode.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelPostCode.Location = new System.Drawing.Point(626, 143);
            this.labelPostCode.Name = "labelPostCode";
            this.labelPostCode.Size = new System.Drawing.Size(49, 13);
            this.labelPostCode.TabIndex = 20;
            this.labelPostCode.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(555, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "邮编：";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelAddress.Location = new System.Drawing.Point(150, 143);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(91, 13);
            this.labelAddress.TabIndex = 18;
            this.labelAddress.Text = "labelAddress";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "地址：";
            // 
            // labelUrlAddress
            // 
            this.labelUrlAddress.AutoSize = true;
            this.labelUrlAddress.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelUrlAddress.Location = new System.Drawing.Point(626, 118);
            this.labelUrlAddress.Name = "labelUrlAddress";
            this.labelUrlAddress.Size = new System.Drawing.Size(49, 13);
            this.labelUrlAddress.TabIndex = 16;
            this.labelUrlAddress.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(555, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "网址：";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelEmail.Location = new System.Drawing.Point(150, 118);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(49, 13);
            this.labelEmail.TabIndex = 14;
            this.labelEmail.Text = "label3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(67, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "邮箱：";
            // 
            // labelTelPhone
            // 
            this.labelTelPhone.AutoSize = true;
            this.labelTelPhone.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelTelPhone.Location = new System.Drawing.Point(626, 92);
            this.labelTelPhone.Name = "labelTelPhone";
            this.labelTelPhone.Size = new System.Drawing.Size(49, 13);
            this.labelTelPhone.TabIndex = 12;
            this.labelTelPhone.Text = "label3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(555, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 11;
            this.label11.Text = "传真：";
            // 
            // labelMobile
            // 
            this.labelMobile.AutoSize = true;
            this.labelMobile.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelMobile.Location = new System.Drawing.Point(150, 92);
            this.labelMobile.Name = "labelMobile";
            this.labelMobile.Size = new System.Drawing.Size(49, 13);
            this.labelMobile.TabIndex = 10;
            this.labelMobile.Text = "label3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(67, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 9;
            this.label13.Text = "电话号码：";
            // 
            // LabelPosition
            // 
            this.LabelPosition.AutoSize = true;
            this.LabelPosition.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelPosition.Location = new System.Drawing.Point(627, 64);
            this.LabelPosition.Name = "LabelPosition";
            this.LabelPosition.Size = new System.Drawing.Size(49, 13);
            this.LabelPosition.TabIndex = 8;
            this.LabelPosition.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(556, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "职位：";
            // 
            // labelContactMan
            // 
            this.labelContactMan.AutoSize = true;
            this.labelContactMan.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelContactMan.Location = new System.Drawing.Point(151, 64);
            this.labelContactMan.Name = "labelContactMan";
            this.labelContactMan.Size = new System.Drawing.Size(49, 13);
            this.labelContactMan.TabIndex = 6;
            this.labelContactMan.Text = "label3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "联系人：";
            // 
            // labelCompanyNAme
            // 
            this.labelCompanyNAme.AutoSize = true;
            this.labelCompanyNAme.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelCompanyNAme.Location = new System.Drawing.Point(627, 34);
            this.labelCompanyNAme.Name = "labelCompanyNAme";
            this.labelCompanyNAme.Size = new System.Drawing.Size(49, 13);
            this.labelCompanyNAme.TabIndex = 4;
            this.labelCompanyNAme.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(556, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "公司名称：";
            // 
            // LabelDBsCode
            // 
            this.LabelDBsCode.AutoSize = true;
            this.LabelDBsCode.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelDBsCode.Location = new System.Drawing.Point(151, 34);
            this.LabelDBsCode.Name = "LabelDBsCode";
            this.LabelDBsCode.Size = new System.Drawing.Size(49, 13);
            this.LabelDBsCode.TabIndex = 2;
            this.LabelDBsCode.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "邓白氏编码：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label1.Location = new System.Drawing.Point(22, 8);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(110, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "基本信息：";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.ScoreRate);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.textDeny);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.txtScoreSum);
            this.panel1.Controls.Add(this.TxtScore);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.fileName);
            this.panel1.Controls.Add(this.singleFileUpload);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Location = new System.Drawing.Point(-2, 180);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(877, 456);
            this.panel1.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(455, 369);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 19);
            this.label22.TabIndex = 23;
            this.label22.Text = "%";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(737, 422);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 31);
            this.button2.TabIndex = 22;
            this.button2.Text = "返  回";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ScoreRate
            // 
            this.ScoreRate.Location = new System.Drawing.Point(390, 367);
            this.ScoreRate.Name = "ScoreRate";
            this.ScoreRate.ReadOnly = true;
            this.ScoreRate.Size = new System.Drawing.Size(59, 21);
            this.ScoreRate.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 10F);
            this.label24.Location = new System.Drawing.Point(387, 332);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 14);
            this.label24.TabIndex = 20;
            this.label24.Text = "得分率：";
            // 
            // textDeny
            // 
            this.textDeny.Location = new System.Drawing.Point(508, 367);
            this.textDeny.Name = "textDeny";
            this.textDeny.Size = new System.Drawing.Size(113, 21);
            this.textDeny.TabIndex = 19;
            this.textDeny.Text = "无";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 10F);
            this.label23.Location = new System.Drawing.Point(505, 332);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 14);
            this.label23.TabIndex = 18;
            this.label23.Text = "否决项：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.DarkOrange;
            this.label21.Location = new System.Drawing.Point(735, 334);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 12);
            this.label21.TabIndex = 17;
            this.label21.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.DarkOrange;
            this.label20.Location = new System.Drawing.Point(307, 335);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 12);
            this.label20.TabIndex = 16;
            this.label20.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.DarkOrange;
            this.label19.Location = new System.Drawing.Point(216, 335);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 12);
            this.label19.TabIndex = 15;
            this.label19.Text = "*";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(114, 422);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 31);
            this.button1.TabIndex = 14;
            this.button1.Text = "提  交";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(673, 368);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(126, 21);
            this.dateTimePicker1.TabIndex = 13;
            // 
            // txtScoreSum
            // 
            this.txtScoreSum.Location = new System.Drawing.Point(244, 368);
            this.txtScoreSum.Name = "txtScoreSum";
            this.txtScoreSum.Size = new System.Drawing.Size(113, 21);
            this.txtScoreSum.TabIndex = 12;
            this.txtScoreSum.TextChanged += new System.EventHandler(this.txtScoreSum_TextChanged);
            // 
            // TxtScore
            // 
            this.TxtScore.Location = new System.Drawing.Point(97, 368);
            this.TxtScore.Name = "TxtScore";
            this.TxtScore.Size = new System.Drawing.Size(113, 21);
            this.TxtScore.TabIndex = 11;
            this.TxtScore.TextChanged += new System.EventHandler(this.TxtScore_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 10F);
            this.label18.Location = new System.Drawing.Point(670, 333);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 14);
            this.label18.TabIndex = 10;
            this.label18.Text = "评估时间：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 10F);
            this.label17.Location = new System.Drawing.Point(241, 333);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 14);
            this.label17.TabIndex = 9;
            this.label17.Text = "该项总分：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 10F);
            this.label16.Location = new System.Drawing.Point(94, 333);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 14);
            this.label16.TabIndex = 8;
            this.label16.Text = "该评估项所的分数：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.DarkOrange;
            this.label15.Location = new System.Drawing.Point(192, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 7;
            this.label15.Text = "*";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(97, 154);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(708, 133);
            this.textBox1.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.DarkOrange;
            this.label14.Location = new System.Drawing.Point(141, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 12);
            this.label14.TabIndex = 5;
            this.label14.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 10F);
            this.label10.Location = new System.Drawing.Point(52, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 14);
            this.label10.TabIndex = 4;
            this.label10.Text = "2、评估说明";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(71, 80);
            this.fileName.Multiline = true;
            this.fileName.Name = "fileName";
            this.fileName.ReadOnly = true;
            this.fileName.Size = new System.Drawing.Size(532, 21);
            this.fileName.TabIndex = 3;
            this.fileName.Text = "           点我上传文件";
            this.fileName.Click += new System.EventHandler(this.fileName_Click);
            // 
            // singleFileUpload
            // 
            this.singleFileUpload.Location = new System.Drawing.Point(648, 78);
            this.singleFileUpload.Name = "singleFileUpload";
            this.singleFileUpload.Size = new System.Drawing.Size(75, 23);
            this.singleFileUpload.TabIndex = 2;
            this.singleFileUpload.Text = "上传文件";
            this.singleFileUpload.UseVisualStyleBackColor = true;
            this.singleFileUpload.Click += new System.EventHandler(this.singleFileUpload_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F);
            this.label3.Location = new System.Drawing.Point(46, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "1、现场评估文件上传";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label32.Location = new System.Drawing.Point(22, 8);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(130, 21);
            this.label32.TabIndex = 0;
            this.label32.Text = "评估信息录入";
            // 
            // LocationEvaluateSinglePersonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 648);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.headerPanel);
            this.Name = "LocationEvaluateSinglePersonForm";
            this.Text = "现场评估";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label labelPostCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelUrlAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelTelPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelMobile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LabelPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelContactMan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCompanyNAme;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelDBsCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtScoreSum;
        private System.Windows.Forms.TextBox TxtScore;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Button singleFileUpload;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textDeny;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox ScoreRate;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label22;
    }
}