﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocationEvaluationPersonForm : Form
    {
        //ID
        private string companyID;

        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;
        //传入的评审员id
        private StringBuilder tmpTextids ;
        //传入物料组
        private String MtGroupName;
        //公司名称
        private String Supplier_Name;
        //评估方面
        private StringBuilder evalCode;
        //得到文件名称
        String resFileName;
        //得到文件路径
        String resFilePath;
        //文件状态标志
        int flagstat = 0;
        private LocationEvalutionPersonBLL locationEvalutionPersonBLL  = new LocationEvalutionPersonBLL();
        //点击事件再点击取消
        int[] flag = new int[10000];

        public LocationEvaluationPersonForm(String Supplier_Id, String Supplier_Name,String MtGroupName)
        {
            this.companyID = Supplier_Id;
            this.Supplier_Name = Supplier_Name;
            this.MtGroupName = MtGroupName;
            InitializeComponent();
            this.Text = "当前处理公司： " + Supplier_Name + "   物料组：" + MtGroupName;
            fillEvaluatorsTable();
             init();
        }
        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }
        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillEvaluatorsTable()
        {
            //查询当前页大小的记录
            userInforTable = locationEvalutionPersonBLL.findAllLocalPersonList(MtGroupName,this.companyID,this.pageSize, 0);
            this.userTableEvaluators.AutoGenerateColumns = false;
            this.userTableEvaluators.DataSource = userInforTable;
            //记录总数
            this.sumSize = locationEvalutionPersonBLL.calculateUserNumber();
        }
        /// <summary>
        /// 选择评审员填充数据
        /// </summary>
        public void fillEvaluatorsedTable()
        {
            //刷之前先删除数据
            this.userTableEvaluatorsed.Rows.Clear();
            //查询当前页大小的记录
            if (tmpTextids != null)
            {
                string[] strArray = tmpTextids.ToString().Split(',');
                int i;
                for (i = 0; i < strArray.Length; i++)
                {
                    if (!String.IsNullOrEmpty(strArray[i]))
                    {
                        DataTable temp = locationEvalutionPersonBLL.getSelectedEvaluationPersonByIds(strArray[i], this.companyID, this.pageSize, 0);
                        this.userTableEvaluatorsed.Rows.Add(new Object[] { temp.Rows[0][0], temp.Rows[0][1], temp.Rows[0][6], temp.Rows[0][5], temp.Rows[0][3], temp.Rows[0][2], temp.Rows[0][4] });
                        temp = null;
                    }
                }
                this.button1.Text = "重新建立小组";
                //记录总数
                this.sumSize = locationEvalutionPersonBLL.calculateUserNumber();
            }
            else
            {
                DataTable userInforTabled = locationEvalutionPersonBLL.getLocalEvalGroupStatByCompanyID(this.companyID);
                this.userTableEvaluatorsed.AutoGenerateColumns = false;
                this.userTableEvaluatorsed.DataSource = userInforTabled;
            }
            
        }

        /// <summary>
        /// 从条件获取数据，填充用户表格
        /// </summary>
        public void fillEvaluatorsTableByNameOrvalCode(String name, String evalcode)
        {
            //查询当前页大小的记录
            userInforTable = locationEvalutionPersonBLL.findAllLocalPersionByCondition(this.companyID,name, evalcode, this.pageSize, 0);

            this.userTableEvaluators.AutoGenerateColumns = false;
            this.userTableEvaluators.DataSource = userInforTable;
            //记录总数
            this.sumSize = locationEvalutionPersonBLL.calculateUserNumber();
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtnEvaluator_Click(object sender, EventArgs e)
        {
            String name = this.nameInput.Text;
            String evalcode = this.evalCodeInput.Text;
            fillEvaluatorsTableByNameOrvalCode(name, evalcode);
        }
        /// <summary>
        /// 重置按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, EventArgs e)
        {
            this.nameInput.Text = "";
            this.evalCodeInput.Text = "";
            fillEvaluatorsTable();
        }
        /// <summary>
        /// 建立评审小组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            tmpTextids = null;
            tmpTextids = new StringBuilder();
            evalCode = null;
            evalCode = new StringBuilder();
            for (int j = 0; j < 10000; j++)//遍历得到标记选择过的评审员
            {
                if(flag[j]!=0)
                {
                    String id = this.userTableEvaluators.Rows[j].Cells["ID"].Value.ToString();
                    String codeEval = this.userTableEvaluators.Rows[j].Cells["evalCodeName"].Value.ToString();
                    tmpTextids.Append(id+",");
                    evalCode.Append(codeEval + ",");
                }
            }
            if (tmpTextids.Length > 1 && evalCode.Length > 1)
            {
                fillEvaluatorsedTable();
            }
            else
            {
                MessageBox.Show("您还没有选择评审员");
            }
        }
        
        private void userTableEvaluators_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex != -1 && !this.userTableEvaluators.Rows[e.RowIndex].IsNewRow)
            {
                if (e.ColumnIndex == 7)
                {
                    if (flag[e.RowIndex] == 0)
                    {
                        this.userTableEvaluators.Rows[e.RowIndex].Cells[7].Value = true;
                        flag[e.RowIndex] = 1;
                    }
                    else
                    {
                        this.userTableEvaluators.Rows[e.RowIndex].Cells[7].Value = false;
                        flag[e.RowIndex] = 0;
                    }
                   
                }
                this.button1.Text = "建立评估小组";
            }
            else
            {
                MessageBox.Show("选择失败");
            }
        }
        /// <summary>
        /// 通知评审小组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InformEvaGroupBtn_Click(object sender, EventArgs e)
        {
            
            if(tmpTextids==null || tmpTextids.Length<1)
            {
                MessageUtil.ShowTips("没有可通知小组");
            }
            else
            {
                //往表LocalEval_Info插入数据
                string[] strArrayEvalIds = tmpTextids.ToString().Split(',');
                string[] strArrayEvalCode = evalCode.ToString().Split(',');
                int i;
                for (i = 0; i < strArrayEvalIds.Length; i++)
                {
                    if (!String.IsNullOrEmpty(strArrayEvalIds[i]))
                    {
                        //得到当前行
                        locationEvalutionPersonBLL.insertLocalEvalInfo(strArrayEvalCode[i], this.companyID, this.MtGroupName, strArrayEvalIds[i]);
                        //添加待办任务
                        locationEvalutionPersonBLL.insertTaskSpecification(strArrayEvalIds[i],Supplier_Name + "待现场评审");
                        //跟新预评审任务
                        locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, Supplier_Name);
                        //根据ID 得到评审员邮箱并发送邮件
                        String email =  locationEvalutionPersonBLL.getEvaluatorMailByEvalID(strArrayEvalIds[i]);
                        List<MailAddress> list = new List<MailAddress>();
                        MailAddress mailAddress = new MailAddress(email);
                        list.Add(mailAddress);
                        EmailUtil.SendEmail("主审通知", "请尽快完成评审工作", list);
                    }
                }
                MessageUtil.ShowTips("邮件通知成功");
                this.Close();
            }

        }
        /// <summary>
        /// 得到当前行数
        /// </summary>
        /// <returns></returns>

        private int getuserTableEvaluatorsedSelectedRowIndex()
        {
            if (this.userTableEvaluatorsed.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.userTableEvaluatorsed.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.userTableEvaluatorsed.CurrentRow.Index;
        }

        private void FileUploadBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "contract files(*.doc;*.docx;*.pdf)|*.doc;*.docx;*.pdf;*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.resFileName = openFileDialog.SafeFileName;
                this.resFilePath = openFileDialog.FileName;
                this.getFileName.Text = resFilePath;
                this.flagstat = 1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.flagstat == 1)//文件上传
            {
               // this.getFileName.Text = resFilePath;
                MessageBox.Show("文件： " + resFileName + " 已上传");
            }
            else
            {
                MessageBox.Show("您还没有选择文件");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
