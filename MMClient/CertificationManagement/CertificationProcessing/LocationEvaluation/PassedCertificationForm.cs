﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using Lib.Model.CertificationProcess.LocationEvaluation;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class PassedCertificationForm : DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        //条件查询窗口对象
        private LocationalEvaluationSupperModel searchCondition;

        // 选评审员窗口
        private LocationEvaluationPersonForm locationEvaluationPersonForm;
        //查看评审状态窗口对象
        private CheckEvaluationStatusForm checkEvaluationStatusForm;
        //邮件通知窗口对象
        private LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm;
        public PassedCertificationForm()
        {
            InitializeComponent();
            init();
            fillCompanyCertificatedTable();
        }

        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }
        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillCompanyCertificatedTable()
        {
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findAllCertificatedLocalCompanyList(SingleUserInstance.getCurrentUserInstance().User_ID,this.pageSize, 0);
            this.LocationalcertificatedTable.AutoGenerateColumns = false;
            this.LocationalcertificatedTable.DataSource = userInforTable;
            //记录总数
            this.sumSize = locationalEvaluationBLL.calculateUserNumber();
        }

        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.LocationalcertificatedTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.LocationalcertificatedTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.LocationalcertificatedTable.CurrentRow.Index;
        }

        private void ReseachByCondition_Click(object sender, EventArgs e)
        {
            if(searchCondition == null)
            {
                searchCondition = new LocationalEvaluationSupperModel();
            }
            searchCondition.companyName = this.CompanyNameInput.Text;
            searchCondition.MtGroupName = this.MtGroupName.Text;
            searchCondition.Country = this.NationalName.Text;
            searchCondition.SelectedDep = this.PurGroupName.Text;
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findCompanyCertificatedListByCondition(SingleUserInstance.getCurrentUserInstance().User_ID,searchCondition, this.pageSize, 0);
            this.LocationalcertificatedTable.AutoGenerateColumns = false;
            this.LocationalcertificatedTable.DataSource = userInforTable;
            //记录总数
            this.sumSize = locationalEvaluationBLL.calculateUserNumber();
        }
        /// <summary>
        /// seset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetInfoList_Click(object sender, EventArgs e)
        {
            this.MtGroupName.Text = ""; 
            this.NationalName.Text = "";
            this.CompanyNameInput.Text = "";
            fillCompanyCertificatedTable();
        }

        private void LocationalcertificatedTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //判断是处理还是重置
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.LocationalcertificatedTable.Rows[currentIndex];
                String companyID = Convert.ToString(row.Cells["SupplierId"].Value);
                String companyName = Convert.ToString(row.Cells["companyName"].Value);
                String email = Convert.ToString(row.Cells["ContactMail"].Value);
                String contactMan = Convert.ToString(row.Cells["ContactName"].Value);
                if (this.LocationalcertificatedTable.Columns[e.ColumnIndex].Name == "准入")
                {
                    SupplierPassCertificationForm supplierPass = new SupplierPassCertificationForm(companyID, companyName, contactMan, email);
                    supplierPass.Show();
                }
                if (this.LocationalcertificatedTable.Columns[e.ColumnIndex].Name == "拒绝")
                {
                    LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm = new LocaltionEvaluationKeyPointForm(companyID, email, 4);
                    localtionEvaluationKeyPointForm.Show();
                }
            }
        }

      
    }
}
