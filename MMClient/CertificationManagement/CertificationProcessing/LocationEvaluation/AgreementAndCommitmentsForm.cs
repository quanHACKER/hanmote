﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.Bll;
using Lib.Model;

namespace MMClient
{
    public partial class AgreementAndCommitmentsForm : DockContent
    {
        private static SupplierManagementBLL sbll = new SupplierManagementBLL();
        private DataTable companyDT = sbll.getAllCompany();

        public AgreementAndCommitmentsForm()
        {
            InitializeComponent();
        }

        private void sendDecision_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.companyname_cmb.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            List<MailAddress> addresses = new List<MailAddress>();
            List<OrgContacts> list = sbll.getOrgContactsByCompanyName(this.companyname_cmb.Text);
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                    addresses.Add(new MailAddress(list[i].Email));
            }
            string body = this.describe_rtb.Text;
            string subject = "协议和承诺的评估决策";
            if (EmailUtil.SendEmail(subject, body, addresses))
            {
                string msg = "";
                for (int i = 0; i < list.Count; i++)
                    msg += list[i].Email + ",";
                msg = msg.Substring(0, msg.Length - 1);
                MessageUtil.ShowTips("协议和承诺的评估决策结果邮件已经成功发送到" + msg + "！");
            }
        }

        private void AgreementAndCommitmentsForm_Load(object sender, EventArgs e)
        {
            FormHelper.combox_bind(this.companyname_cmb, companyDT, "supplierChineseName", "supplierChineseName");
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.companyname_cmb.Text))
            {
                MessageUtil.ShowWarning("供应商名称不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.describe_rtb.Text))
            {
                MessageUtil.ShowWarning("评估描述不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(this.decision_cmb.Text.ToString()))
            {
                MessageUtil.ShowWarning("评估结果不能为空！");
                return;
            }
            DecisionInfo di = new DecisionInfo();
            di.SupplierName = this.companyname_cmb.Text;
            di.Descriptions = this.describe_rtb.Text;
            di.DecisionResult = this.decision_cmb.Text.ToString();
            ///暂时
            di.Status = 2;

            bool flag = sbll.saveSelfCerAndBaseInfoDecision(di);
            if (flag)
            {
                MessageUtil.ShowTips("保存成功！");
            }
            else
            {
                MessageUtil.ShowTips("保存失败！");
            }
        }

        /// <summary>
        /// 查看协议与承诺
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("待完善");
        }
    }
}
