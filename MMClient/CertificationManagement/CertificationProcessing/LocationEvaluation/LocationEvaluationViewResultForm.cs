﻿
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using MMClient.progress;
using MMClient.CommFileShow;
using WeifenLuo.WinFormsUI.Docking;
using Lib.SqlServerDAL.CertificationProcess;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocationEvaluationViewResultForm : DockContent
    {
        FileShowForm fileShowForm = null;
        FTPHelper fTPHelper = new FTPHelper("");
        private LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm;
        private LocationEvaluationSubmitHighLevelForm locationEvaluationSubmitHighLevelForm;
        private LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
        //公司ID
        private string companyID;
        //公司名称
        private string companyNameTex;
        //数据查询
        private LocationEvalutionPersonBLL locationEvalutionPersonBLL;
        //是否已经点击过保存按钮
        private bool isSavebtn = false;
        //datatable
        private DataTable userInfoTable;
        //得到文件名称
        String resFileName;
        //得到文件路径
        String resFilePath;
        //文件状态标志
        int flag = 0;
        private string mtGroupName;
        LocationEvalutionPersonBLL supplier_MainAccountInfo = new LocationEvalutionPersonBLL();
        public LocationEvaluationViewResultForm(string companyID,string mtGroupName)
        {
            this.companyID = companyID;
            InitializeComponent();
            fillBaseInfo();
            getFile();//获得文件，新增
            this.Text = "当前处理公司; " + this.companyNameTex;
            this.mtGroupName = mtGroupName;
        }
        /// <summary>
        /// 提交致高层
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            
        }

        //填充数据
        private void fillBaseInfo()
        {
            locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
            //基本信息显示
            userInfoTable = locationEvalutionPersonBLL.getCompangInfoByCompanyID(this.companyID);
            this.LabelDBsCode.Text = userInfoTable.Rows[0][0].ToString();
            this.labelCompanyNAme.Text = userInfoTable.Rows[0][1].ToString();
            this.companyNameTex = userInfoTable.Rows[0][1].ToString();
            this.labelContactMan.Text = userInfoTable.Rows[0][2].ToString();
            this.LabelPosition.Text = userInfoTable.Rows[0][3].ToString();
            this.labelMobile.Text = userInfoTable.Rows[0][4].ToString();
            this.labelTelPhone.Text = userInfoTable.Rows[0][5].ToString();
            this.labelEmail.Text = userInfoTable.Rows[0][6].ToString();
            this.labelUrlAddress.Text = userInfoTable.Rows[0][7].ToString();
            this.labelAddress.Text = userInfoTable.Rows[0][8].ToString();
            this.labelPostCode.Text = userInfoTable.Rows[0][9].ToString();
            //评分详情
            userInfoTable = locationEvalutionPersonBLL.getDetailEvaluateScoreByCompanyID(SingleUserInstance.getCurrentUserInstance().User_ID,this.companyID);
            //添加总分
            DataTable sumscore = locationEvalutionPersonBLL.getSumEvaluateScoreByCompanyID(SingleUserInstance.getCurrentUserInstance().User_ID, this.companyID);
            Object obRealScore = sumscore.Rows[0][0];
            Object obSumScore = sumscore.Rows[0][1];
            Object obRateTxt = "得分率：";
            Object obRateScore = obRateTxt+(float.Parse(sumscore.Rows[0][0].ToString()) / float.Parse(sumscore.Rows[0][1].ToString())*100+0.0000000001).ToString().Substring(0,4);
            Object o = null;
            Object obSumTxt = "总计";
            userInfoTable.Rows.Add(new Object[]{ obSumTxt,o, obRealScore, obSumScore,o,o, obRateScore+"%"});
            this.LocationalCompanyTable.AutoGenerateColumns = false;
            this.LocationalCompanyTable.DataSource = userInfoTable;
        }
        /// <summary>
        /// 保存上传文件及主审意见
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveBtn_Click(object sender, EventArgs e)
        {

              if (this.getFileName.Text == null || this.getFileName.Text.ToString().Equals(""))
               {
                   MessageBox.Show("请先上传SSEM文件");
                   return;
               }
               else
               {
                //存入文件/主审意见
                locationEvalutionPersonBLL.saveMainAdvicesOrSSEMFile(SingleUserInstance.getCurrentUserInstance().User_ID,this.getFileName.Text,this.textBoxAdvice.Text,this.companyID,this.mtGroupName);
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.LocationalCompanyTable.Rows[currentIndex];
                //现场评估完结,邮件通知搞成并修改标志位......
                locationalEvaluationBLL.updateTagSuperSRInfoByCompanyID(this.companyID);
                locationalEvaluationBLL.insertHanmoteDecisionTable(SingleUserInstance.getCurrentUserInstance().User_ID, this.companyID, "上级领导",this.textBoxAdvice.Text.ToString());
                //插入任务状态
                string pid = locationalEvaluationBLL.getPidByCurUserID(SingleUserInstance.getCurrentUserInstance().User_ID);
                locationEvalutionPersonBLL.insertTaskSpecification(pid,this.companyNameTex+ "现场评审已完结待审批");
                //更新自己的任务状态
                locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID,this.companyNameTex);
                MessageBox.Show("提交成功");
                this.Close();
                
            }
        }

        private void LocationalCompanyTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //提醒评审员重置评审信息
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.LocationalCompanyTable.Rows[currentIndex];
                String companyName = Convert.ToString(row.Cells["companyName"].Value);
                if (this.LocationalCompanyTable.Columns[e.ColumnIndex].Name == "backEval" && !String.IsNullOrEmpty(companyName))
                {
                    MessageBoxButtons mess = MessageBoxButtons.OKCancel;
                    DialogResult result = MessageBox.Show("您确定撤回此条评审信息吗？", "警  告", mess);
                    if (result == DialogResult.OK)
                    {
                        String evaluateID = Convert.ToString(row.Cells["ID"].Value);
                        String name = Convert.ToString(row.Cells["companyName"].Value);
                        //邮件通知评审员尽快处理
                        String email = locationEvalutionPersonBLL.getEvalInfoEmaiml(evaluateID);
                        //同时更新任务状态置为0
                        locationEvalutionPersonBLL.setIsFinishedByUserID(evaluateID, this.companyNameTex);
                        localtionEvaluationKeyPointForm = new LocaltionEvaluationKeyPointForm(name, email, 2);
                        localtionEvaluationKeyPointForm.Show();
                        //刷新评分详情
                        locationEvalutionPersonBLL.backSinglePersonEvalInfo(SingleUserInstance.getCurrentUserInstance().User_ID,evaluateID, this.companyID); 
                        userInfoTable = locationEvalutionPersonBLL.getDetailEvaluateScoreByCompanyID(SingleUserInstance.getCurrentUserInstance().User_ID,this.companyID);
                        this.LocationalCompanyTable.AutoGenerateColumns = false;
                        this.LocationalCompanyTable.DataSource = userInfoTable;
                    }
                }
            }
        }
        /// <summary>
        /// 得到当前行
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.LocationalCompanyTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.LocationalCompanyTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.LocationalCompanyTable.CurrentRow.Index;
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.getFileName.Text = "";
            this.textBoxAdvice.Text = "";
        }
        /// <summary>
        /// 上传文件，新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void upload_Click(object sender, EventArgs e)
        {
            string[] fileName = this.getFileName.Text.Split(';');
            progreBar progreBar = new progreBar(fTPHelper, fileName, "/" + this.companyID + "/Leader");
            //插入文件信息
            string filePath = "/" + this.companyID + "/Leader";
            for (int i = 0; i < fileName.Length - 1; i++)
            {
                FileInfo fileInf = new FileInfo(fileName[i]);
                FileUploadIDAL.InsertFileInfo(this.companyID, fileInf.Name, "Leader", SingleUserInstance.getCurrentUserInstance().Username, "主审", filePath + "/");

            }

            progreBar.Show();
        }
        /// <summary>
        /// 选择文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectdFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "contract files(*.doc;*.docx;*.pdf)|*.doc;*.docx;*.pdf;*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.resFileName = openFileDialog.SafeFileName;
                this.resFilePath = openFileDialog.FileName;
                this.getFileName.Text = resFilePath;
                this.flag = 1;
            }
        }
        //新增
        private void getFileName_Click(object sender, EventArgs e)
        {
            string[] resultFile = null;
            this.getFileName.Text = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;
                this.label1.Text = "";
                for (int i = 0; i < resultFile.Length; i++)

                    if (i == resultFile.Length - 1) {
                        this.getFileName.Text += resultFile[i];
                    } else {
                        this.getFileName.Text += resultFile[i] + ";";

                    }
                    
            }
        }


        /// <summary>
        /// 显示文件,新增
        /// </summary>
        private void getFile()
        {
            string[] filenames = fTPHelper.GetFileList(this.companyID + "//localEvalFile");
            if (filenames == null) return;
            for (int i = 0; i < filenames.Length; i++)
                this.checkedListBox1.Items.Add(filenames[i]);

        }
        /// <summary>
        /// 下载，新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            string mess = "请选择要下载的文件";
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            BrowDialog.ShowDialog();
            string filePath = this.companyID + "//localEvalFile//";
            string pathname = BrowDialog.SelectedPath;
            try
            {
                if (pathname != null)
                {
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                            mess = fTPHelper.Download(pathname, filename, filePath + filename);

                        }

                    }
                    DialogResult dr = MessageBox.Show(mess, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                DialogResult dr = MessageBox.Show(mess, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 查看文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            string filePath = this.companyID + "//localEvalFile//";
            
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    fileIDList.Add(filename);

                }

            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];
           
            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try
            {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }



            }
            catch (Exception)
            {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);
        }
    }
}
