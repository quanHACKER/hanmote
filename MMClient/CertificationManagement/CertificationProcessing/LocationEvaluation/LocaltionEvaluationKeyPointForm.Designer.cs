﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class LocaltionEvaluationKeyPointForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.concelBtn = new System.Windows.Forms.Button();
            this.sendBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(555, 247);
            this.textBox1.TabIndex = 0;
            // 
            // concelBtn
            // 
            this.concelBtn.Location = new System.Drawing.Point(340, 272);
            this.concelBtn.Name = "concelBtn";
            this.concelBtn.Size = new System.Drawing.Size(75, 30);
            this.concelBtn.TabIndex = 1;
            this.concelBtn.Text = "取  消";
            this.concelBtn.UseVisualStyleBackColor = true;
            this.concelBtn.Click += new System.EventHandler(this.concelBtn_Click);
            // 
            // sendBtn
            // 
            this.sendBtn.Location = new System.Drawing.Point(471, 272);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(75, 30);
            this.sendBtn.TabIndex = 2;
            this.sendBtn.Text = "发  送";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // LocaltionEvaluationKeyPointForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 311);
            this.Controls.Add(this.sendBtn);
            this.Controls.Add(this.concelBtn);
            this.Controls.Add(this.textBox1);
            this.Name = "LocaltionEvaluationKeyPointForm";
            this.Text = "填写现场评估要点";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button concelBtn;
        private System.Windows.Forms.Button sendBtn;
    }
}