﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Data;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class LocationEvaluationPersonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.topPanel = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.InformEvaGroupBtn = new System.Windows.Forms.Button();
            this.getFileName = new System.Windows.Forms.TextBox();
            this.FileUploadBtn = new System.Windows.Forms.Button();
            this.LocalAddressInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.EvaluateTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.footerPanel = new System.Windows.Forms.Panel();
            this.userTableEvaluatorsed = new System.Windows.Forms.DataGridView();
            this.IDed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.named = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evalCodeNamed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurGroupNamed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobiled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.userTableEvaluators = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evalCodeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.evalCodeInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.searchBtnEvaluator = new System.Windows.Forms.Button();
            this.topPanel.SuspendLayout();
            this.footerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userTableEvaluatorsed)).BeginInit();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userTableEvaluators)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.button3);
            this.topPanel.Controls.Add(this.label4);
            this.topPanel.Controls.Add(this.button2);
            this.topPanel.Controls.Add(this.button1);
            this.topPanel.Controls.Add(this.InformEvaGroupBtn);
            this.topPanel.Controls.Add(this.getFileName);
            this.topPanel.Controls.Add(this.FileUploadBtn);
            this.topPanel.Controls.Add(this.LocalAddressInput);
            this.topPanel.Controls.Add(this.label3);
            this.topPanel.Controls.Add(this.EvaluateTime);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.footerPanel);
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Controls.Add(this.headerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1088, 617);
            this.topPanel.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(478, 573);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 33);
            this.button3.TabIndex = 23;
            this.button3.Text = "返  回";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(12, 520);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "邀请函上传：";
            this.label4.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(749, 520);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "上  传";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(959, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 25);
            this.button1.TabIndex = 6;
            this.button1.Text = "建立评估小组";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InformEvaGroupBtn
            // 
            this.InformEvaGroupBtn.Location = new System.Drawing.Point(960, 543);
            this.InformEvaGroupBtn.Name = "InformEvaGroupBtn";
            this.InformEvaGroupBtn.Size = new System.Drawing.Size(107, 23);
            this.InformEvaGroupBtn.TabIndex = 20;
            this.InformEvaGroupBtn.Text = "通知评审小组";
            this.InformEvaGroupBtn.UseVisualStyleBackColor = true;
            this.InformEvaGroupBtn.Click += new System.EventHandler(this.InformEvaGroupBtn_Click);
            // 
            // getFileName
            // 
            this.getFileName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.getFileName.Location = new System.Drawing.Point(196, 517);
            this.getFileName.Name = "getFileName";
            this.getFileName.ReadOnly = true;
            this.getFileName.Size = new System.Drawing.Size(509, 23);
            this.getFileName.TabIndex = 19;
            this.getFileName.Visible = false;
            // 
            // FileUploadBtn
            // 
            this.FileUploadBtn.Location = new System.Drawing.Point(98, 514);
            this.FileUploadBtn.Name = "FileUploadBtn";
            this.FileUploadBtn.Size = new System.Drawing.Size(79, 23);
            this.FileUploadBtn.TabIndex = 18;
            this.FileUploadBtn.Text = "选择文件";
            this.FileUploadBtn.UseVisualStyleBackColor = true;
            this.FileUploadBtn.Visible = false;
            this.FileUploadBtn.Click += new System.EventHandler(this.FileUploadBtn_Click);
            // 
            // LocalAddressInput
            // 
            this.LocalAddressInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.LocalAddressInput.Location = new System.Drawing.Point(346, 474);
            this.LocalAddressInput.Name = "LocalAddressInput";
            this.LocalAddressInput.Size = new System.Drawing.Size(172, 23);
            this.LocalAddressInput.TabIndex = 17;
            this.LocalAddressInput.Tag = "";
            this.LocalAddressInput.Text = "输入评估地点";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(296, 477);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "地点：";
            // 
            // EvaluateTime
            // 
            this.EvaluateTime.Location = new System.Drawing.Point(84, 476);
            this.EvaluateTime.Name = "EvaluateTime";
            this.EvaluateTime.Size = new System.Drawing.Size(122, 21);
            this.EvaluateTime.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(10, 476);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "评估时间：";
            // 
            // footerPanel
            // 
            this.footerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.footerPanel.AutoSize = true;
            this.footerPanel.Controls.Add(this.userTableEvaluatorsed);
            this.footerPanel.Location = new System.Drawing.Point(0, 336);
            this.footerPanel.Name = "footerPanel";
            this.footerPanel.Size = new System.Drawing.Size(1088, 127);
            this.footerPanel.TabIndex = 2;
            // 
            // userTableEvaluatorsed
            // 
            this.userTableEvaluatorsed.AllowUserToAddRows = false;
            this.userTableEvaluatorsed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userTableEvaluatorsed.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.userTableEvaluatorsed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userTableEvaluatorsed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.userTableEvaluatorsed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userTableEvaluatorsed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDed,
            this.named,
            this.evalCodeNamed,
            this.PurGroupNamed,
            this.mobiled,
            this.emailed,
            this.addressed});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.userTableEvaluatorsed.DefaultCellStyle = dataGridViewCellStyle2;
            this.userTableEvaluatorsed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userTableEvaluatorsed.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.userTableEvaluatorsed.GridColor = System.Drawing.SystemColors.ControlLight;
            this.userTableEvaluatorsed.Location = new System.Drawing.Point(0, 0);
            this.userTableEvaluatorsed.Name = "userTableEvaluatorsed";
            this.userTableEvaluatorsed.RowTemplate.Height = 23;
            this.userTableEvaluatorsed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userTableEvaluatorsed.Size = new System.Drawing.Size(1088, 127);
            this.userTableEvaluatorsed.TabIndex = 14;
            // 
            // IDed
            // 
            this.IDed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IDed.DataPropertyName = "ID";
            this.IDed.FillWeight = 43.61068F;
            this.IDed.Frozen = true;
            this.IDed.HeaderText = "编  号";
            this.IDed.Name = "IDed";
            this.IDed.Width = 125;
            // 
            // named
            // 
            this.named.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.named.DataPropertyName = "name";
            this.named.FillWeight = 515.5145F;
            this.named.Frozen = true;
            this.named.HeaderText = "评审员";
            this.named.Name = "named";
            this.named.Width = 110;
            // 
            // evalCodeNamed
            // 
            this.evalCodeNamed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.evalCodeNamed.DataPropertyName = "evalCodeName";
            this.evalCodeNamed.FillWeight = 12.58458F;
            this.evalCodeNamed.Frozen = true;
            this.evalCodeNamed.HeaderText = "评估方面";
            this.evalCodeNamed.Name = "evalCodeNamed";
            // 
            // PurGroupNamed
            // 
            this.PurGroupNamed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PurGroupNamed.DataPropertyName = "PurGroupName";
            this.PurGroupNamed.FillWeight = 23.65793F;
            this.PurGroupNamed.Frozen = true;
            this.PurGroupNamed.HeaderText = "采购组织";
            this.PurGroupNamed.Name = "PurGroupNamed";
            this.PurGroupNamed.Width = 112;
            // 
            // mobiled
            // 
            this.mobiled.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.mobiled.DataPropertyName = "mobile";
            this.mobiled.FillWeight = 17.20646F;
            this.mobiled.Frozen = true;
            this.mobiled.HeaderText = "移动电话";
            this.mobiled.Name = "mobiled";
            this.mobiled.Width = 119;
            // 
            // emailed
            // 
            this.emailed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.emailed.DataPropertyName = "email";
            this.emailed.FillWeight = 33.81242F;
            this.emailed.Frozen = true;
            this.emailed.HeaderText = "邮  箱";
            this.emailed.Name = "emailed";
            this.emailed.Width = 200;
            // 
            // addressed
            // 
            this.addressed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.addressed.DataPropertyName = "address";
            this.addressed.FillWeight = 142.8484F;
            this.addressed.Frozen = true;
            this.addressed.HeaderText = "地  址";
            this.addressed.Name = "addressed";
            this.addressed.Width = 290;
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.userTableEvaluators);
            this.contentPanel.Location = new System.Drawing.Point(0, 67);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(1088, 233);
            this.contentPanel.TabIndex = 1;
            // 
            // userTableEvaluators
            // 
            this.userTableEvaluators.AllowUserToAddRows = false;
            this.userTableEvaluators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userTableEvaluators.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userTableEvaluators.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.userTableEvaluators.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userTableEvaluators.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.userTableEvaluators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userTableEvaluators.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.name,
            this.evalCodeName,
            this.PurGroupName,
            this.Mobile,
            this.email,
            this.address,
            this.selected});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.userTableEvaluators.DefaultCellStyle = dataGridViewCellStyle4;
            this.userTableEvaluators.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.userTableEvaluators.GridColor = System.Drawing.SystemColors.ControlLight;
            this.userTableEvaluators.Location = new System.Drawing.Point(0, 0);
            this.userTableEvaluators.Name = "userTableEvaluators";
            this.userTableEvaluators.RowTemplate.Height = 23;
            this.userTableEvaluators.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userTableEvaluators.Size = new System.Drawing.Size(1088, 233);
            this.userTableEvaluators.TabIndex = 8;
            this.userTableEvaluators.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.userTableEvaluators_CellContentClick);
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID.DataPropertyName = "ID";
            this.ID.FillWeight = 0.1436674F;
            this.ID.Frozen = true;
            this.ID.HeaderText = "编  号";
            this.ID.Name = "ID";
            this.ID.Width = 105;
            // 
            // name
            // 
            this.name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.name.DataPropertyName = "name";
            this.name.FillWeight = 0.3673376F;
            this.name.Frozen = true;
            this.name.HeaderText = "评审员";
            this.name.Name = "name";
            // 
            // evalCodeName
            // 
            this.evalCodeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.evalCodeName.DataPropertyName = "evalCodeName";
            this.evalCodeName.FillWeight = 50.04794F;
            this.evalCodeName.Frozen = true;
            this.evalCodeName.HeaderText = "评估方面";
            this.evalCodeName.Name = "evalCodeName";
            // 
            // PurGroupName
            // 
            this.PurGroupName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PurGroupName.DataPropertyName = "PurGroupName";
            this.PurGroupName.FillWeight = 168.7125F;
            this.PurGroupName.Frozen = true;
            this.PurGroupName.HeaderText = "采购组织";
            this.PurGroupName.Name = "PurGroupName";
            this.PurGroupName.Width = 102;
            // 
            // Mobile
            // 
            this.Mobile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Mobile.DataPropertyName = "mobile";
            this.Mobile.FillWeight = 4.41099F;
            this.Mobile.Frozen = true;
            this.Mobile.HeaderText = "移动电话";
            this.Mobile.Name = "Mobile";
            this.Mobile.Width = 119;
            // 
            // email
            // 
            this.email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.email.DataPropertyName = "email";
            this.email.FillWeight = 1.314432F;
            this.email.Frozen = true;
            this.email.HeaderText = "邮  箱";
            this.email.Name = "email";
            this.email.Width = 161;
            // 
            // address
            // 
            this.address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.address.DataPropertyName = "address";
            this.address.FillWeight = 568.7818F;
            this.address.Frozen = true;
            this.address.HeaderText = "地址";
            this.address.Name = "address";
            this.address.Width = 280;
            // 
            // selected
            // 
            this.selected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.selected.FillWeight = 91.37056F;
            this.selected.Frozen = true;
            this.selected.HeaderText = "选  择";
            this.selected.Name = "selected";
            this.selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.selected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.selected.Width = 79;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.evalCodeInput);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Controls.Add(this.nameInput);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.Reset);
            this.headerPanel.Controls.Add(this.searchBtnEvaluator);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1088, 61);
            this.headerPanel.TabIndex = 0;
            // 
            // evalCodeInput
            // 
            this.evalCodeInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.evalCodeInput.Location = new System.Drawing.Point(372, 18);
            this.evalCodeInput.Name = "evalCodeInput";
            this.evalCodeInput.Size = new System.Drawing.Size(123, 23);
            this.evalCodeInput.TabIndex = 16;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(298, 21);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "评估方面：";
            // 
            // nameInput
            // 
            this.nameInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.nameInput.Location = new System.Drawing.Point(139, 18);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(123, 23);
            this.nameInput.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(103, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "姓名：";
            // 
            // Reset
            // 
            this.Reset.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Reset.Location = new System.Drawing.Point(802, 29);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 25);
            this.Reset.TabIndex = 5;
            this.Reset.Text = "重  置";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // searchBtnEvaluator
            // 
            this.searchBtnEvaluator.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.searchBtnEvaluator.Location = new System.Drawing.Point(699, 29);
            this.searchBtnEvaluator.Name = "searchBtnEvaluator";
            this.searchBtnEvaluator.Size = new System.Drawing.Size(75, 25);
            this.searchBtnEvaluator.TabIndex = 0;
            this.searchBtnEvaluator.Text = "查  询";
            this.searchBtnEvaluator.UseVisualStyleBackColor = true;
            this.searchBtnEvaluator.Click += new System.EventHandler(this.searchBtnEvaluator_Click);
            // 
            // LocationEvaluationPersonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 617);
            this.Controls.Add(this.topPanel);
            this.Location = new System.Drawing.Point(325, 38);
            this.Name = "LocationEvaluationPersonForm";
            this.Text = "评审员列表";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.footerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userTableEvaluatorsed)).EndInit();
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userTableEvaluators)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }
        /// <summary>
        /// 设置加载复选框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void getEvalGroupByCheckBox(object sender, EventArgs e)
        {
            //得到当前行
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.userTableEvaluators.Rows[currentIndex];
            String companyID = Convert.ToString(row.Cells["ID"].Value);
            String companyName = Convert.ToString(row.Cells["name"].Value);
            String mtName = Convert.ToString(row.Cells["Mt_GroupName"].Value);
            LocationEvalutionPersonBLL locationalEvaluationBLL = new LocationEvalutionPersonBLL();
            DataTable dt = locationalEvaluationBLL.findAllLocalPersonList(mtName,companyID, 20, 0);
            userTableEvaluators.DataSource = dt;
            DataGridViewCheckBoxColumn dtCheck = new DataGridViewCheckBoxColumn();
            dtCheck.DataPropertyName = "check";
            dtCheck.HeaderText = "";
            userTableEvaluators.Columns.Add(dtCheck);
            userTableEvaluators.Columns[0].Width = 30;

        }
        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.userTableEvaluators.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.userTableEvaluators.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.userTableEvaluators.CurrentRow.Index;
        }

        #endregion
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel footerPanel;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button searchBtnEvaluator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button InformEvaGroupBtn;
        private System.Windows.Forms.TextBox getFileName;
        private System.Windows.Forms.Button FileUploadBtn;
        private System.Windows.Forms.TextBox LocalAddressInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker EvaluateTime;
        private System.Windows.Forms.TextBox evalCodeInput;
        private DataGridView userTableEvaluators;
        private DataGridView userTableEvaluatorsed;
        private Label label4;
        private Button button2;
        private Label label2;
        private DataGridViewTextBoxColumn IDed;
        private DataGridViewTextBoxColumn named;
        private DataGridViewTextBoxColumn evalCodeNamed;
        private DataGridViewTextBoxColumn PurGroupNamed;
        private DataGridViewTextBoxColumn mobiled;
        private DataGridViewTextBoxColumn emailed;
        private DataGridViewTextBoxColumn addressed;
        private Button button3;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn name;
        private DataGridViewTextBoxColumn evalCodeName;
        private DataGridViewTextBoxColumn PurGroupName;
        private DataGridViewTextBoxColumn Mobile;
        private DataGridViewTextBoxColumn email;
        private DataGridViewTextBoxColumn address;
        private DataGridViewCheckBoxColumn selected;
    }
}