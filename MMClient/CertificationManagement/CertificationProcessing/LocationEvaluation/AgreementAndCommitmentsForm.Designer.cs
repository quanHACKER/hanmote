﻿namespace MMClient
{
    partial class AgreementAndCommitmentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.companyname_cmb = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.sendDecision_btn = new System.Windows.Forms.Button();
            this.describe_rtb = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.decision_cmb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // companyname_cmb
            // 
            this.companyname_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyname_cmb.FormattingEnabled = true;
            this.companyname_cmb.Location = new System.Drawing.Point(86, 12);
            this.companyname_cmb.Name = "companyname_cmb";
            this.companyname_cmb.Size = new System.Drawing.Size(213, 25);
            this.companyname_cmb.TabIndex = 48;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(12, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 17);
            this.label17.TabIndex = 47;
            this.label17.Text = "公司名称";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(425, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 23);
            this.button2.TabIndex = 49;
            this.button2.Text = "查看协议与承诺";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // sendDecision_btn
            // 
            this.sendDecision_btn.Location = new System.Drawing.Point(381, 277);
            this.sendDecision_btn.Name = "sendDecision_btn";
            this.sendDecision_btn.Size = new System.Drawing.Size(89, 24);
            this.sendDecision_btn.TabIndex = 177;
            this.sendDecision_btn.Text = "发送决策通知";
            this.sendDecision_btn.UseVisualStyleBackColor = true;
            this.sendDecision_btn.Click += new System.EventHandler(this.sendDecision_btn_Click);
            // 
            // describe_rtb
            // 
            this.describe_rtb.Location = new System.Drawing.Point(86, 63);
            this.describe_rtb.Name = "describe_rtb";
            this.describe_rtb.Size = new System.Drawing.Size(344, 124);
            this.describe_rtb.TabIndex = 176;
            this.describe_rtb.Text = "";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(15, 66);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 175;
            this.label1.Text = "评审描述";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(15, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 174;
            this.label3.Text = "评审结果";
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(501, 277);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(89, 24);
            this.save_btn.TabIndex = 173;
            this.save_btn.Text = "保存";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // decision_cmb
            // 
            this.decision_cmb.FormattingEnabled = true;
            this.decision_cmb.ItemHeight = 12;
            this.decision_cmb.Items.AddRange(new object[] {
            "通过，进入公司评审",
            "不通过"});
            this.decision_cmb.Location = new System.Drawing.Point(86, 227);
            this.decision_cmb.Name = "decision_cmb";
            this.decision_cmb.Size = new System.Drawing.Size(200, 20);
            this.decision_cmb.TabIndex = 172;
            // 
            // AgreementAndCommitmentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(762, 312);
            this.Controls.Add(this.sendDecision_btn);
            this.Controls.Add(this.describe_rtb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.decision_cmb);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.companyname_cmb);
            this.Controls.Add(this.label17);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AgreementAndCommitmentsForm";
            this.Text = "协议与承诺";
            this.Load += new System.EventHandler(this.AgreementAndCommitmentsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox companyname_cmb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button sendDecision_btn;
        private System.Windows.Forms.RichTextBox describe_rtb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.ComboBox decision_cmb;
    }
}