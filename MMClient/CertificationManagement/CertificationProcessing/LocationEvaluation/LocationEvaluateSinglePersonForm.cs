﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using MMClient.progress;
using Lib.SqlServerDAL.CertificationProcess;
using System.IO;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocationEvaluateSinglePersonForm : Form
    {

        FTPHelper fTPHelper = new FTPHelper("");
        //DataTable
        private DataTable userInfoTable;
        private String companyID;
        private String evaluateID;
        private String companyName;
        private int uploadCount=0;
        private String filePath;
        private String state;
        private String score;
        private String scoreSum;
        private string DenyItems;
        private float scoreInt = 0.0000f;
        private float scoreSumInt = 0.0000f;
        private int flag;
        LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
        public LocationEvaluateSinglePersonForm(String companyName,String companyID,int flag ,String evaluateID)
        {
            this.companyID = companyID;
            this.evaluateID = evaluateID;
            this.companyName = companyName;
            this.flag = flag;
            InitializeComponent();
            if (flag == 1)
            {
                fillBaseInfo();//未处理
                this.button1.Text = "保  存";
            }
            if (flag == 2)
            {
                editBaseInfo();//已处理
            }
            this.Text = "当前处理公司 "+ companyName;
        }

        private void editBaseInfo()
        {
            //基本信息显示
            userInfoTable = locationEvalutionPersonBLL.getCompangInfoByCompanyID(this.companyID);
            this.LabelDBsCode.Text = userInfoTable.Rows[0][0].ToString();
            this.labelCompanyNAme.Text = userInfoTable.Rows[0][1].ToString();
            this.labelContactMan.Text = userInfoTable.Rows[0][2].ToString();
            this.LabelPosition.Text = userInfoTable.Rows[0][3].ToString();
            this.labelMobile.Text = userInfoTable.Rows[0][4].ToString();
            this.labelTelPhone.Text = userInfoTable.Rows[0][5].ToString();
            this.labelEmail.Text = userInfoTable.Rows[0][6].ToString();
            this.labelUrlAddress.Text = userInfoTable.Rows[0][7].ToString();
            this.labelAddress.Text = userInfoTable.Rows[0][8].ToString();
            this.labelPostCode.Text = userInfoTable.Rows[0][9].ToString();
            //根据 ID 和 评审员ID查看信息
            DataTable userTable = locationEvalutionPersonBLL.detailLocalInfoByCompanyIDAndEvaluateID(companyID, evaluateID);
            this.fileName.Text = userTable.Rows[0][3].ToString();
            this.TxtScore.Text = userTable.Rows[0][0].ToString();
            this.textBox1.Text = userTable.Rows[0][1].ToString();
            this.txtScoreSum.Text = userTable.Rows[0][2].ToString();
        }

        //填充数据
        private void fillBaseInfo()
        {
            //基本信息显示
            userInfoTable = locationEvalutionPersonBLL.getCompangInfoByCompanyID(this.companyID);
            this.LabelDBsCode.Text = userInfoTable.Rows[0][0].ToString();
            this.labelCompanyNAme.Text = userInfoTable.Rows[0][1].ToString();
            this.labelContactMan.Text = userInfoTable.Rows[0][2].ToString();
            this.LabelPosition.Text = userInfoTable.Rows[0][3].ToString();
            this.labelMobile.Text = userInfoTable.Rows[0][4].ToString();
            this.labelTelPhone.Text = userInfoTable.Rows[0][5].ToString();
            this.labelEmail.Text = userInfoTable.Rows[0][6].ToString();
            this.labelUrlAddress.Text = userInfoTable.Rows[0][7].ToString();
            this.labelAddress.Text = userInfoTable.Rows[0][8].ToString();
            this.labelPostCode.Text = userInfoTable.Rows[0][9].ToString();
        }
        /// <summary>
        /// 提交信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text.ToString().Equals("") || this.TxtScore.Text.ToString().Equals("") || this.txtScoreSum.Text.ToString().Equals("")||this.uploadCount==0)
            {
                MessageBox.Show("信息录入不完整");
                return;
            }
            if (scoreSumInt < scoreInt)
            {
                MessageBox.Show("总分小于得分", "警   告");
                return;
            }
            else
            {
                filePath = this.fileName.Text.ToString();
                state = this.textBox1.Text.ToString();
                DenyItems = this.textDeny.Text.ToString();
                MessageBoxButtons bus = MessageBoxButtons.OKCancel;
                DialogResult dr = MessageBox.Show("确认保存当前信息？", "警  告", bus);
                if (dr == DialogResult.OK) {
                    locationEvalutionPersonBLL.insertLocalEvalInfoBySingelPerson(companyID, evaluateID, filePath, state, score, scoreSum, DenyItems);
                    //更新任务状态通知
                    locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, companyName);
                    //检查该供应商评估是否完成
                    DataTable userInforTable = locationEvalutionPersonBLL.getLocalEvalGroupStatByCompanyID(companyID);
                    bool result = true;
                    for (int k = 0; k < userInforTable.Rows.Count; k++)
                    {
                        if (userInforTable.Rows[k][6].ToString().Equals("未完成"))
                        {
                            result = false;
                        }
                    }
                    if(result)
                    {
                        //根据供应商ID得到物料组
                        string mtGroup = locationEvalutionPersonBLL.getMtGroupNameByCompanyId(this.companyID);
                        //根据供应商ID得到采购组
                        string mtPor = locationEvalutionPersonBLL.getMtPorNameByCompanyId(this.companyID);
                        //根据物料组得到主审ID
                        string MainEvalID = locationEvalutionPersonBLL.getMainEvalIDByMtGroupName(mtGroup, mtPor);
                        //提示主审结束现场评估工作
                        locationEvalutionPersonBLL.insertTaskSpecification(MainEvalID,this.companyName+ "现场评审已完结待提交上级审批");
                    }
                    if (this.flag == 1) {
                        MessageBox.Show("提交成功!, 请点击页面重置按钮已刷新状态信息", "告  知");
                    }
                    this.Close();
                }
                
            }
        }
        /// <summary>
        /// 上传文件
        /// 聂雄超
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void singleFileUpload_Click(object sender, EventArgs e)
        {
            uploadCount++;
            string[] fileName = this.fileName.Text.Split(';');

            string filePath = "/" + this.companyID + "/localEvalFile";
            progreBar progreBar = new progreBar(fTPHelper, fileName,filePath);
            //2018.4.19

            for (int i = 0; i < fileName.Length ; i++) {
                FileInfo fileInf = new FileInfo(fileName[i]);
                FileUploadIDAL.InsertFileInfo(this.companyID, fileInf.Name, "localEvalFile", SingleUserInstance.getCurrentUserInstance().Username, "主审", filePath+"/");

            }
          

            progreBar.Show();
            
        }
        /// <summary>
        /// 文本框点击事件
        /// 聂雄超
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileName_Click(object sender, EventArgs e)
        {
            string[] resultFile = null;
            this.fileName.Text = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;
                this.label1.Text = "";
                for (int i = 0; i < resultFile.Length; i++) {
                    if (i == resultFile.Length - 1) { this.fileName.Text += resultFile[i]; } else { this.fileName.Text += resultFile[i] + ";"; }
                    

                }
                    
            }


        }

        private void txtScoreSum_TextChanged(object sender, EventArgs e)
        {
            if (this.txtScoreSum.Text.ToString() == null || this.txtScoreSum.Text.ToString().Equals(""))
            {
                return;
            }
            try
            {
                scoreSum = this.txtScoreSum.Text.ToString();
                scoreSumInt = float.Parse(scoreSum);
                this.ScoreRate.Text = ((scoreInt/scoreSumInt)*100 + 0.0001).ToString().Substring(0, 4);
            }
            catch (Exception)
            {
                if (!String.IsNullOrEmpty(this.txtScoreSum.Text.ToString())) { MessageBox.Show("只能输入数字", "警   告"); }
                this.txtScoreSum.Text="";
                scoreSum = "";
                return;
            }
        }

        private void TxtScore_TextChanged(object sender, EventArgs e)
        {
            if(this.TxtScore.Text.ToString()==null || this.TxtScore.Text.ToString().Equals(""))
            {
                return;
            }
            try
            {
                score = this.TxtScore.Text.ToString();
                scoreInt = float.Parse(score);
            }
            catch(Exception)
            {
                MessageBox.Show("只能输入数字", "警   告"); 
                this.TxtScore.Text = "";
                score = "";
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
