﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocaltionEvaluationKeyPointForm : Form
    {
        private string email;
        private int  flag;//0 普通通知  1 通知高层   2  驳回通知评审员  3 通知评审员 4 拒绝通知
        private string ID;
        private LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
        private LocationEvalutionPersonBLL locationalEvaluationPersonBLL = new LocationEvalutionPersonBLL();
        public LocaltionEvaluationKeyPointForm(string ID,string email,int flag)
        {
            this.flag = flag;
            this.ID = ID;
            this.email = email;//高层邮箱
            InitializeComponent();
            if (flag == 0)
            {
                this.Text = "请填写要通知的内容";
            }
            if(flag == 1)
            {
                this.Text = "请填写现场评估要点";
            }
            if (flag == 2)
            {
                this.Text = "请填写驳回的理由";
            }
            if(flag==3)
            {
                this.Text = "填写改善意见";
            }
            if(flag==4)
            {
                this.Text = "请填写拒绝的理由";
            }
        }

        private void concelBtn_Click(object sender, EventArgs e)
        {
            if(flag==3)
            {
                MessageBoxButtons mess = MessageBoxButtons.OKCancel;
                DialogResult result = MessageBox.Show("取消后系统不将邮件通知评审员及时处理，您确定要这么做吗？", "警  告", mess);
                if (result == DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    //......
                }
            }
            else
            {
                this.Close();
            }
           
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            
            if(this.textBox1.Text.ToString()==null || this.textBox1.Text.ToString().Equals(""))
            {
                MessageBox.Show("内容不能为空");
            }
            else
            {
                if (1==flag)//提交高层
                {
                    //现场评估完结,邮件通知搞成并修改标志位......
                    locationalEvaluationBLL.updateTagSuperSRInfoByCompanyID(ID);
                    MessageBox.Show("提交成功");
                }
                if (2 == flag)//驳回通知
                {
                    List<MailAddress> list = new List<MailAddress>();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    EmailUtil.SendEmail("主审通知", this.textBox1.Text.ToString(), list);
                    MessageBox.Show("邮件发送成功");
                }
                if (0 == flag)
                {
                    //根据供应商ID得到邮箱并发送
                    //......
                    List<MailAddress> list = new List<MailAddress>();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    EmailUtil.SendEmail("汉默特通知", this.textBox1.Text.ToString(),list);
                    MessageBox.Show("邮件发送成功");
                }
                if(3==flag)
                {
                    List<MailAddress> list = new List<MailAddress>();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    EmailUtil.SendEmail("主审通知", this.textBox1.Text.ToString(), list);
                    MessageBox.Show("邮件发送成功");
                }
                if (4 == flag)
                {
                    List<MailAddress> list = new List<MailAddress>();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    EmailUtil.SendEmail("汉默特拒绝通知", this.textBox1.Text.ToString(), list);
                    locationalEvaluationBLL.updateTagRefuseSRInfoByCompanyID(ID);
                    DataTable dt = locationalEvaluationPersonBLL.getCompangInfoByCompanyID(ID);
                    locationalEvaluationPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID,dt.Rows[0][1].ToString());
                   MessageBox.Show("邮件发送成功");
                }
                this.Close();
            }
        }
    }
}
