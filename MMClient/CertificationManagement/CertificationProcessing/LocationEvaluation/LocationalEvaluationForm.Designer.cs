﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Model.CertificationProcess.LocationEvaluation;
using MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation;
using System;
using System.Data;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class LocationalEvaluationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.LocationalCompanyTable = new System.Windows.Forms.DataGridView();
            this.SupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactMail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mt_GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.but_选择评审员 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.but_查看评审状态 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.miniToolStrip = new System.Windows.Forms.BindingNavigator(this.components);
            this.topPanel = new System.Windows.Forms.Panel();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.ResetInfoList = new System.Windows.Forms.Button();
            this.NationalName = new System.Windows.Forms.TextBox();
            this.CompanyNameInput = new System.Windows.Forms.TextBox();
            this.MtGroupName = new System.Windows.Forms.TextBox();
            this.ReseachByCondition = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PurGroupName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationalCompanyTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miniToolStrip)).BeginInit();
            this.topPanel.SuspendLayout();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.pageNext1);
            this.contentPanel.Controls.Add(this.LocationalCompanyTable);
            this.contentPanel.Location = new System.Drawing.Point(0, 67);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(1036, 461);
            this.contentPanel.TabIndex = 1;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.Location = new System.Drawing.Point(0, 427);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(798, 37);
            this.pageNext1.TabIndex = 1;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // LocationalCompanyTable
            // 
            this.LocationalCompanyTable.AllowUserToAddRows = false;
            this.LocationalCompanyTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LocationalCompanyTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.LocationalCompanyTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.LocationalCompanyTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LocationalCompanyTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.LocationalCompanyTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LocationalCompanyTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SupplierId,
            this.companyName,
            this.Country,
            this.ContactName,
            this.PhoneNumber,
            this.ContactMail,
            this.Mt_GroupName,
            this.but_选择评审员,
            this.but_查看评审状态});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.LocationalCompanyTable.DefaultCellStyle = dataGridViewCellStyle8;
            this.LocationalCompanyTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.LocationalCompanyTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.LocationalCompanyTable.Location = new System.Drawing.Point(3, 0);
            this.LocationalCompanyTable.Name = "LocationalCompanyTable";
            this.LocationalCompanyTable.RowTemplate.Height = 23;
            this.LocationalCompanyTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.LocationalCompanyTable.Size = new System.Drawing.Size(1030, 415);
            this.LocationalCompanyTable.TabIndex = 0;
            this.LocationalCompanyTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Btn_ClikOnOneOf);
            // 
            // SupplierId
            // 
            this.SupplierId.DataPropertyName = "SupplierId";
            this.SupplierId.HeaderText = "供应商编号";
            this.SupplierId.Name = "SupplierId";
            // 
            // companyName
            // 
            this.companyName.DataPropertyName = "companyName";
            this.companyName.HeaderText = "公司名称";
            this.companyName.Name = "companyName";
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "区  域";
            this.Country.Name = "Country";
            // 
            // ContactName
            // 
            this.ContactName.DataPropertyName = "ContactName";
            this.ContactName.HeaderText = "联系人";
            this.ContactName.Name = "ContactName";
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "PhoneNumber";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            // 
            // ContactMail
            // 
            this.ContactMail.DataPropertyName = "ContactMail";
            this.ContactMail.HeaderText = "电子邮箱";
            this.ContactMail.Name = "ContactMail";
            // 
            // Mt_GroupName
            // 
            this.Mt_GroupName.DataPropertyName = "MtGroupName";
            this.Mt_GroupName.HeaderText = "物料组名称";
            this.Mt_GroupName.Name = "Mt_GroupName";
            // 
            // but_选择评审员
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.NullValue = "选择评审员";
            this.but_选择评审员.DefaultCellStyle = dataGridViewCellStyle6;
            this.but_选择评审员.HeaderText = "选择评审员";
            this.but_选择评审员.Name = "but_选择评审员";
            this.but_选择评审员.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.but_选择评审员.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.but_选择评审员.Text = "选择评审员";
            this.but_选择评审员.ToolTipText = "选择评审员";
            // 
            // but_查看评审状态
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.NullValue = "查看评审状态";
            this.but_查看评审状态.DefaultCellStyle = dataGridViewCellStyle7;
            this.but_查看评审状态.HeaderText = "查看评审状态";
            this.but_查看评审状态.Name = "but_查看评审状态";
            this.but_查看评审状态.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.but_查看评审状态.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.but_查看评审状态.Text = "查看评审状态";
            this.but_查看评审状态.ToolTipText = "查看评审状态";
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "新项选择";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.miniToolStrip.AddNewItem = null;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.CountItem = null;
            this.miniToolStrip.DeleteItem = null;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(531, 3);
            this.miniToolStrip.MoveFirstItem = null;
            this.miniToolStrip.MoveLastItem = null;
            this.miniToolStrip.MoveNextItem = null;
            this.miniToolStrip.MovePreviousItem = null;
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.PositionItem = null;
            this.miniToolStrip.Size = new System.Drawing.Size(1060, 25);
            this.miniToolStrip.TabIndex = 2;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Controls.Add(this.headerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1036, 531);
            this.topPanel.TabIndex = 42;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.ResetInfoList);
            this.headerPanel.Controls.Add(this.NationalName);
            this.headerPanel.Controls.Add(this.CompanyNameInput);
            this.headerPanel.Controls.Add(this.MtGroupName);
            this.headerPanel.Controls.Add(this.ReseachByCondition);
            this.headerPanel.Controls.Add(this.label3);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.PurGroupName);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1036, 61);
            this.headerPanel.TabIndex = 0;
            // 
            // ResetInfoList
            // 
            this.ResetInfoList.Location = new System.Drawing.Point(777, 16);
            this.ResetInfoList.Name = "ResetInfoList";
            this.ResetInfoList.Size = new System.Drawing.Size(74, 23);
            this.ResetInfoList.TabIndex = 61;
            this.ResetInfoList.Text = "重  置";
            this.ResetInfoList.UseVisualStyleBackColor = true;
            this.ResetInfoList.Click += new System.EventHandler(this.ResetInfoList_Click);
            // 
            // NationalName
            // 
            this.NationalName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.NationalName.Location = new System.Drawing.Point(266, 15);
            this.NationalName.Name = "NationalName";
            this.NationalName.Size = new System.Drawing.Size(85, 23);
            this.NationalName.TabIndex = 60;
            // 
            // CompanyNameInput
            // 
            this.CompanyNameInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CompanyNameInput.Location = new System.Drawing.Point(508, 16);
            this.CompanyNameInput.Name = "CompanyNameInput";
            this.CompanyNameInput.Size = new System.Drawing.Size(85, 23);
            this.CompanyNameInput.TabIndex = 59;
            // 
            // MtGroupName
            // 
            this.MtGroupName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.MtGroupName.Location = new System.Drawing.Point(72, 17);
            this.MtGroupName.Name = "MtGroupName";
            this.MtGroupName.Size = new System.Drawing.Size(82, 23);
            this.MtGroupName.TabIndex = 58;
            // 
            // ReseachByCondition
            // 
            this.ReseachByCondition.Location = new System.Drawing.Point(686, 16);
            this.ReseachByCondition.Name = "ReseachByCondition";
            this.ReseachByCondition.Size = new System.Drawing.Size(69, 23);
            this.ReseachByCondition.TabIndex = 57;
            this.ReseachByCondition.Text = "查  询";
            this.ReseachByCondition.UseVisualStyleBackColor = true;
            this.ReseachByCondition.Click += new System.EventHandler(this.ReseachByCondition_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(218, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 56;
            this.label3.Text = "国  家：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(443, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "公司名称：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(20, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "物料组：";
            // 
            // PurGroupName
            // 
            this.PurGroupName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PurGroupName.Location = new System.Drawing.Point(72, 17);
            this.PurGroupName.Name = "PurGroupName";
            this.PurGroupName.Size = new System.Drawing.Size(83, 23);
            this.PurGroupName.TabIndex = 53;
            this.PurGroupName.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(5, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "采购组织：";
            this.label7.Visible = false;
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(942, 25);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(83, 23);
            this.Reset.TabIndex = 41;
            this.Reset.Text = "重  置";
            this.Reset.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SupplierId";
            this.dataGridViewTextBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 102;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "companyName";
            this.dataGridViewTextBoxColumn2.HeaderText = "公司名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 102;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Country";
            this.dataGridViewTextBoxColumn3.HeaderText = "区  域";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 102;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ContactName";
            this.dataGridViewTextBoxColumn4.HeaderText = "联系人";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 102;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PhoneNumber";
            this.dataGridViewTextBoxColumn5.HeaderText = "联系方式";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 102;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ContactMail";
            this.dataGridViewTextBoxColumn6.HeaderText = "电子邮箱";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 102;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MtGroupName";
            this.dataGridViewTextBoxColumn7.HeaderText = "物料组名称";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 102;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "操    作";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 103;
            // 
            // LocationalEvaluationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 531);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.Reset);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Location = new System.Drawing.Point(325, 20);
            this.Name = "LocationalEvaluationForm";
            this.Text = "现场评估";
            this.Load += new System.EventHandler(this.OprateButton_Load);
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LocationalCompanyTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miniToolStrip)).EndInit();
            this.topPanel.ResumeLayout(false);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }
        //生成按钮的单击事件
        private void Btn_ClikOnOneOf(object sender, DataGridViewCellEventArgs e)
        {
            LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
            if (e.RowIndex >= 0)
            {
                //得到当前行
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.LocationalCompanyTable.Rows[currentIndex];
                LocationalEvaluationPersonModel.companyID = Convert.ToString(row.Cells["SupplierId"].Value);
                LocationalEvaluationPersonModel.companyName= Convert.ToString(row.Cells["companyName"].Value);
                String companyID = Convert.ToString(row.Cells["SupplierId"].Value);
                String companyName = Convert.ToString(row.Cells["companyName"].Value);
                String MtGroupName = Convert.ToString(row.Cells["Mt_GroupName"].Value);
                String  email = Convert.ToString(row.Cells["ContactMail"].Value);
                //选择评审员
                if (this.LocationalCompanyTable.Columns[e.ColumnIndex].Name == "but_选择评审员")
                {
                    DataTable dataTable = locationEvalutionPersonBLL.findAllLocalEvalInfoByCompanyId(companyID);
                    if(dataTable.Rows.Count <=0 )
                    {
                        locationEvaluationPersonForm = null;
                        if (locationEvaluationPersonForm == null || locationEvaluationPersonForm.IsDisposed)
                        {
                            locationEvaluationPersonForm = new LocationEvaluationPersonForm(companyID, companyName, MtGroupName);
                        }
                        locationEvaluationPersonForm.Show();
                    }
                    else
                    {
                        MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                        DialogResult dr = MessageBox.Show("评审小组已建立，确定要重新建立吗?", "警  告", messButton);
                        if (dr == DialogResult.OK)
                        {
                            //需要删除任务状态说明重新插入
                            LocationEvalutionPersonBLL bll = new LocationEvalutionPersonBLL();
                            bll.deleteTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID,companyName);
                            locationEvaluationPersonForm = null;
                            if (locationEvaluationPersonForm == null || locationEvaluationPersonForm.IsDisposed)
                            {
                                locationEvaluationPersonForm = new LocationEvaluationPersonForm(companyID, companyName, MtGroupName);
                            }
                            //调之前删除历史数据
                            bll.deleteEvalGroupByCompanyID(companyID);
                            locationEvaluationPersonForm.Show();
                        }
                        else
                        {
                            //.....
                        }
                    }

                }
                //查看评审状态
                if (this.LocationalCompanyTable.Columns[e.ColumnIndex].Name == "but_查看评审状态")
                {
                    DataTable dataTable = locationEvalutionPersonBLL.getLocalEvalGroupStatByCompanyID(companyID);
                    if (dataTable.Rows.Count <= 0)
                    {
                        MessageBox.Show("还没有建立评审小组，不能操作");
                    }
                    else
                    {
                        checkEvaluationStatusForm = null; 
                        if (checkEvaluationStatusForm == null || checkEvaluationStatusForm.IsDisposed)
                         {
                              checkEvaluationStatusForm = new CheckEvaluationStatusForm(companyID, companyName, MtGroupName);
                          }
                         checkEvaluationStatusForm.Show();
                    }
                }
                //邮件通知
               
            // if (this.LocationalCompanyTable.Columns[e.ColumnIndex].Name == "but_邮件通知")
               // {
                //    localtionEvaluationKeyPointForm = null;
                //    if (localtionEvaluationKeyPointForm == null || localtionEvaluationKeyPointForm.IsDisposed)
               //     {
                        //根据ID 得到邮箱
               //         localtionEvaluationKeyPointForm = new LocaltionEvaluationKeyPointForm(companyID,email,0);
              //      }
               //     localtionEvaluationKeyPointForm.Show();
             //   }
            }
        }
        //载入按钮
        private void OprateButton_Load(object sender, EventArgs e)
        {
            loadData();

      //      DataGridViewButtonColumn but_邮件通知 = new DataGridViewButtonColumn();
      ///      but_邮件通知.Name = "but_邮件通知";
      ///      but_邮件通知.Text = "邮件通知";//加上这两个就能显示
            //      but_邮件通知.UseColumnTextForButtonValue = true;//
            //       but_邮件通知.Width = 50;
            //       but_邮件通知.HeaderText = "邮件通知";
            //       this.LocationalCompanyTable.Columns.AddRange(but_邮件通知);

        }

        #endregion

        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.BindingNavigator miniToolStrip;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.TextBox NationalName;
        private System.Windows.Forms.TextBox CompanyNameInput;
        private System.Windows.Forms.TextBox MtGroupName;
        private System.Windows.Forms.Button ReseachByCondition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PurGroupName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button ResetInfoList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridView LocationalCompanyTable;
        private pager.pagetool.pageNext pageNext1;
        private DataGridViewTextBoxColumn SupplierId;
        private DataGridViewTextBoxColumn companyName;
        private DataGridViewTextBoxColumn Country;
        private DataGridViewTextBoxColumn ContactName;
        private DataGridViewTextBoxColumn PhoneNumber;
        private DataGridViewTextBoxColumn ContactMail;
        private DataGridViewTextBoxColumn Mt_GroupName;
        private DataGridViewButtonColumn but_选择评审员;
        private DataGridViewButtonColumn but_查看评审状态;
    }
}