﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class LocalEvaluationSingleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.evalID = new System.Windows.Forms.TextBox();
            this.ResetInfoList = new System.Windows.Forms.Button();
            this.NationalNameIN = new System.Windows.Forms.TextBox();
            this.CompanyNameInput = new System.Windows.Forms.TextBox();
            this.MtGroupNameIN = new System.Windows.Forms.TextBox();
            this.ReseachByCondition = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PurGroupNameIN = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.singleListSupplierTable = new System.Windows.Forms.DataGridView();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactMail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mt_GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pur_GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.处理 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.重置 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.evaluateID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.headerPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.singleListSupplierTable)).BeginInit();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Controls.Add(this.evalID);
            this.headerPanel.Controls.Add(this.ResetInfoList);
            this.headerPanel.Controls.Add(this.NationalNameIN);
            this.headerPanel.Controls.Add(this.CompanyNameInput);
            this.headerPanel.Controls.Add(this.MtGroupNameIN);
            this.headerPanel.Controls.Add(this.ReseachByCondition);
            this.headerPanel.Controls.Add(this.label3);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.PurGroupNameIN);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1230, 61);
            this.headerPanel.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(699, 20);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 63;
            this.label1.Text = "模拟：";
            this.label1.Visible = false;
            // 
            // evalID
            // 
            this.evalID.Location = new System.Drawing.Point(749, 17);
            this.evalID.Name = "evalID";
            this.evalID.Size = new System.Drawing.Size(100, 21);
            this.evalID.TabIndex = 62;
            this.evalID.Visible = false;
            // 
            // ResetInfoList
            // 
            this.ResetInfoList.Location = new System.Drawing.Point(982, 35);
            this.ResetInfoList.Name = "ResetInfoList";
            this.ResetInfoList.Size = new System.Drawing.Size(74, 23);
            this.ResetInfoList.TabIndex = 61;
            this.ResetInfoList.Text = "重  置";
            this.ResetInfoList.UseVisualStyleBackColor = true;
            this.ResetInfoList.Click += new System.EventHandler(this.ResetInfoList_Click);
            // 
            // NationalNameIN
            // 
            this.NationalNameIN.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.NationalNameIN.Location = new System.Drawing.Point(381, 16);
            this.NationalNameIN.Name = "NationalNameIN";
            this.NationalNameIN.Size = new System.Drawing.Size(85, 23);
            this.NationalNameIN.TabIndex = 60;
            // 
            // CompanyNameInput
            // 
            this.CompanyNameInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CompanyNameInput.Location = new System.Drawing.Point(555, 16);
            this.CompanyNameInput.Name = "CompanyNameInput";
            this.CompanyNameInput.Size = new System.Drawing.Size(85, 23);
            this.CompanyNameInput.TabIndex = 59;
            // 
            // MtGroupNameIN
            // 
            this.MtGroupNameIN.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.MtGroupNameIN.Location = new System.Drawing.Point(227, 17);
            this.MtGroupNameIN.Name = "MtGroupNameIN";
            this.MtGroupNameIN.Size = new System.Drawing.Size(82, 23);
            this.MtGroupNameIN.TabIndex = 58;
            // 
            // ReseachByCondition
            // 
            this.ReseachByCondition.Location = new System.Drawing.Point(855, 35);
            this.ReseachByCondition.Name = "ReseachByCondition";
            this.ReseachByCondition.Size = new System.Drawing.Size(69, 23);
            this.ReseachByCondition.TabIndex = 57;
            this.ReseachByCondition.Text = "查  询";
            this.ReseachByCondition.UseVisualStyleBackColor = true;
            this.ReseachByCondition.Click += new System.EventHandler(this.ReseachByCondition_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(333, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 56;
            this.label3.Text = "国  家：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(490, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "公司名称：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(175, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "物料组：";
            // 
            // PurGroupNameIN
            // 
            this.PurGroupNameIN.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PurGroupNameIN.Location = new System.Drawing.Point(72, 17);
            this.PurGroupNameIN.Name = "PurGroupNameIN";
            this.PurGroupNameIN.Size = new System.Drawing.Size(83, 23);
            this.PurGroupNameIN.TabIndex = 53;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(5, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "采购组织：";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.singleListSupplierTable);
            this.panel1.Controls.Add(this.pageNext1);
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1230, 462);
            this.panel1.TabIndex = 2;
            // 
            // singleListSupplierTable
            // 
            this.singleListSupplierTable.AllowUserToAddRows = false;
            this.singleListSupplierTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.singleListSupplierTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.singleListSupplierTable.BackgroundColor = System.Drawing.SystemColors.Control;
            this.singleListSupplierTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.singleListSupplierTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.singleListSupplierTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.status,
            this.SupplierId,
            this.companyName,
            this.Country,
            this.ContactName,
            this.PhoneNumber,
            this.ContactMail,
            this.Mt_GroupName,
            this.Pur_GroupName,
            this.处理,
            this.重置,
            this.evaluateID});
            this.singleListSupplierTable.Location = new System.Drawing.Point(8, 3);
            this.singleListSupplierTable.Name = "singleListSupplierTable";
            this.singleListSupplierTable.RowTemplate.Height = 23;
            this.singleListSupplierTable.Size = new System.Drawing.Size(1219, 413);
            this.singleListSupplierTable.TabIndex = 7;
            this.singleListSupplierTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.singleListSupplierTable_CellContentClick);
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "状态";
            this.status.Name = "status";
            this.status.ToolTipText = "状态";
            // 
            // SupplierId
            // 
            this.SupplierId.DataPropertyName = "SupplierId";
            this.SupplierId.HeaderText = "供应商编号";
            this.SupplierId.Name = "SupplierId";
            this.SupplierId.ToolTipText = "供应商编号";
            // 
            // companyName
            // 
            this.companyName.DataPropertyName = "companyName";
            this.companyName.HeaderText = "公司名称 ";
            this.companyName.Name = "companyName";
            this.companyName.ToolTipText = "公司名称 ";
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "区域";
            this.Country.Name = "Country";
            this.Country.ToolTipText = "区域";
            // 
            // ContactName
            // 
            this.ContactName.DataPropertyName = "ContactName";
            this.ContactName.HeaderText = "联系人";
            this.ContactName.Name = "ContactName";
            this.ContactName.ToolTipText = "联系人";
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "PhoneNumber";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            // 
            // ContactMail
            // 
            this.ContactMail.DataPropertyName = "ContactMail";
            this.ContactMail.HeaderText = "电子邮箱";
            this.ContactMail.Name = "ContactMail";
            // 
            // Mt_GroupName
            // 
            this.Mt_GroupName.DataPropertyName = "Mt_GroupName";
            this.Mt_GroupName.HeaderText = "物料组名称";
            this.Mt_GroupName.Name = "Mt_GroupName";
            this.Mt_GroupName.ToolTipText = "物料组名称";
            // 
            // Pur_GroupName
            // 
            this.Pur_GroupName.DataPropertyName = "Pur_GroupName";
            this.Pur_GroupName.HeaderText = "采购组织";
            this.Pur_GroupName.Name = "Pur_GroupName";
            this.Pur_GroupName.ToolTipText = "采购组织";
            // 
            // 处理
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = "处理";
            this.处理.DefaultCellStyle = dataGridViewCellStyle3;
            this.处理.HeaderText = "处理";
            this.处理.Name = "处理";
            this.处理.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.处理.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.处理.Text = "处理";
            this.处理.ToolTipText = "处理";
            // 
            // 重置
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = "重置";
            this.重置.DefaultCellStyle = dataGridViewCellStyle4;
            this.重置.HeaderText = "重置";
            this.重置.Name = "重置";
            this.重置.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.重置.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.重置.Text = "重置";
            this.重置.ToolTipText = "重置";
            // 
            // evaluateID
            // 
            this.evaluateID.DataPropertyName = "evaluateID";
            this.evaluateID.HeaderText = "评估员编号";
            this.evaluateID.Name = "evaluateID";
            this.evaluateID.ToolTipText = "评估员编号";
            this.evaluateID.Visible = false;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.Location = new System.Drawing.Point(8, 422);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(683, 37);
            this.pageNext1.TabIndex = 6;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // LocalEvaluationSingleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1230, 531);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.headerPanel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "LocalEvaluationSingleForm";
            this.Text = "供应商列表";
            this.Load += new System.EventHandler(this.LocalEvaluationSingleForm_Load);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.singleListSupplierTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button ResetInfoList;
        private System.Windows.Forms.TextBox NationalNameIN;
        private System.Windows.Forms.TextBox CompanyNameInput;
        private System.Windows.Forms.TextBox MtGroupNameIN;
        private System.Windows.Forms.Button ReseachByCondition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PurGroupNameIN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox evalID;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DataGridView singleListSupplierTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactMail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mt_GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pur_GroupName;
        private System.Windows.Forms.DataGridViewButtonColumn 处理;
        private System.Windows.Forms.DataGridViewButtonColumn 重置;
        private System.Windows.Forms.DataGridViewTextBoxColumn evaluateID;
    }
}