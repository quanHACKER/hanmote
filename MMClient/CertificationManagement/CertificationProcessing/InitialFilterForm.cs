﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll;
using Lib.Common.CommonUtils;
using Lib.Model;

namespace MMClient
{
    public partial class InitialFilterForm : DockContent
    {
        private static SupplierManagementBLL sbll = new SupplierManagementBLL();
        private DataTable companyDT = sbll.getAllCompany();

        public InitialFilterForm()
        {
            InitializeComponent();
        }

        private void InitialFilterForm_Load(object sender, EventArgs e)
        {
            FormHelper.combox_bind(this.companyname_cmb, companyDT, "supplierChineseName", "supplierChineseName");
            InitialData();
        }

        private void InitialData()
        {
            string cmp = this.companyname_cmb.Text;
            OrganizationInfo obj = sbll.getOrganizationInfoByCompanyName(cmp);
            OrgContacts oc = obj.orgContacts[0];
            if (oc.Email.Contains(","))
            {
                string[] strs4 = oc.Email.Split(',');
                //初始化控件
                //InitialControls(strs4.Length);
            }
            else
            {
                InitialControls(1);
            }

            #region 初始化控件数据
            InitialOrgInfoData(obj);

            #endregion
            
        }

        /// <summary>
        /// 初始化组织信息数据
        /// </summary>
        /// <param name="obj"></param>
        private void InitialOrgInfoData(OrganizationInfo obj)
        {
            this.supplierChineseName_txt.Text = obj.SupplierChineseName;
            this.supplierEnglishName_txt.Text = obj.SupplierEnglishName;
            this.companyProperty_cmb.Text = obj.CompanyProperty;
            this.companyUsedname_txt.Text = obj.CompanyUsedName;
            this.companyRegisterCountry_cmb.Text = obj.CompanyRegisterCountry;
            this.companyWebsite_txt.Text = obj.CompanyWebsite;
            this.companyCode_txt.Text = obj.CompanyCode;
            this.companyRegisterDate_txt.Text = Convert.ToString(obj.CompanyRegisterDate);
            this.isListed_cmb.Text = Convert.ToInt32(obj.IsListed)==0?"否":"是";
            this.companyRepresent_txt.Text = obj.CompanyRepresent;
            this.stockCode_txt.Text = obj.StockCode;
            this.isExport_cmb.Text = Convert.ToInt32(obj.IsExport) == 0 ? "否" : "是";
            this.country_txt.Text = obj.Country;
            this.city_txt.Text = obj.City;
            this.doorNum_txt.Text = obj.DoorNum;
            this.zipCode_txt.Text = obj.ZipCode;

            OrgContacts oc = obj.orgContacts[0];
            if (oc.Email.Contains(","))
            {
                string[] strs1 = oc.ContactMan.Split(',');
                string[] strs2 = oc.Phonenum1.Split(',');
                string[] strs3 = oc.Phonenum2.Split(',');
                string[] strs4 = oc.Email.Split(',');
                InitialControls(strs4.Length);
                if (strs1 != null && strs1.Length > 0)
                {
                    for (int i = 0; i < contract_gb.Controls.Count; i++)
                    {
                        if (contract_gb.Controls[i].GetType() == typeof(TextBox))
                        {
                            TextBox tb = contract_gb.Controls[i] as TextBox;
                            for (int j = 0; j < strs1.Length; j++)
                            {
                                if (tb.Name.Equals("contract" + (j + 1) + "_txt"))
                                {
                                    tb.Text = strs1[j];
                                }
                                if (tb.Name.Equals("phone1" + (j + 1) + "_txt"))
                                {
                                    tb.Text = strs2[j];
                                }
                                if (tb.Name.Equals("phone2" + (j + 1) + "_txt"))
                                {
                                    tb.Text = strs3[j];
                                }
                                if (tb.Name.Equals("email" + (j + 1) + "_txt"))
                                {
                                    tb.Text = strs4[j];
                                }
                            }
                        }
                    }


                }
            }
            else
            {
                for (int i = 0; i < contract_gb.Controls.Count; i++)
                {
                    if (contract_gb.Controls[i].GetType() == typeof(TextBox))
                    {
                        TextBox tb = contract_gb.Controls[i] as TextBox;
                        if (tb.Name.Equals("contract1_txt"))
                        {
                            tb.Text = oc.ContactMan;
                        }
                        if (tb.Name.Equals("phone11_txt"))
                        {
                            tb.Text = oc.Phonenum1;
                        }
                        if (tb.Name.Equals("phone21_txt"))
                        {
                            tb.Text = oc.Phonenum2;
                        }
                        if (tb.Name.Equals("email1_txt"))
                        {
                            tb.Text = oc.Email;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 初始化财务信息的数据
        /// </summary>
        private void InitialFinInfoData()
        { 
            
        }


        /// <summary>
        /// 初始化销售信息的数据
        /// </summary>
        private void InitialSaleInfoData()
        { 
            
        }

        //private void Initial

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitialControls(int num)
        {
            #region 初始化组织信息控件
            
            AddContractMan(num);
            #endregion

            #region 初始化财务控件
            
            #endregion

            #region 初始化销售控件

            #endregion

            #region 初始化技术控件

            #endregion

            #region 初始化质量控件

            #endregion

            #region 初始化产能控件

            #endregion
        }

        /// <summary>
        /// 添加联系人等控件
        /// </summary>
        /// <param name="num"></param>
        private void AddContractMan(int num)
        { 
            if(num < 2)
                return;
            //获取电话1label的高度
            int columns1X = this.phone1_lbl.Location.X;
            int columns1Y = this.phone1_lbl.Location.Y;
            int columns2X = this.phone11_txt.Location.X;
            int columns2Y = this.phone11_txt.Location.Y;
            int columns3X = this.phone2_lbl.Location.X;
            int columns3Y = this.phone2_lbl.Location.Y;
            int columns4X = this.phone21_txt.Location.X;
            int columns4Y = this.phone21_txt.Location.Y;
            int columns5X = this.email_lbl.Location.X;
            int columns5Y = this.email_lbl.Location.Y;
            int columns6X = this.email1_txt.Location.X;
            int columns6Y = this.email1_txt.Location.Y;
            int spaceH = 40;
            int rowH = 40;
            
            for(int i = 2;i <= num;i++)
            {
                //联系人label
                Label lbl = new Label();
                SetLabel("联系人", "contract" + i + "_lbl", columns1X, columns1Y + spaceH + (2*i - 4) * rowH, lbl);
                //联系人textbox
                TextBox contract_txt = new TextBox();
                SetText("contract" + i + "_txt", columns2X, columns2Y + spaceH + (2 * i - 4) * rowH, contract_txt);

                //电话1label
                Label phone1lbl = new Label();
                SetLabel("电话1", "phone1" + i + "_lbl", columns1X, columns1Y + spaceH + (2 * i - 3) * rowH, phone1lbl);
                //电话1textbox
                TextBox phone1_txt = new TextBox();
                SetText("phone1" + i + "_txt", columns2X, columns2Y + spaceH + (2 * i - 3) * rowH, phone1_txt);
                //电话2label
                Label phone2lbl = new Label();
                SetLabel("电话2", "phone2" + i + "_lbl", columns3X, columns3Y + spaceH + (2 * i - 3) * rowH, phone2lbl);
                //电话2textbox
                TextBox phone2_txt = new TextBox();
                SetText("phone2" + i + "_txt", columns4X, columns4Y + spaceH + (2 * i - 3) * rowH, phone2_txt);
                //电话1label
                Label email_lbl = new Label();
                SetLabel("Email", "email" + i + "_lbl", columns5X, columns5Y + spaceH + (2 * i - 3) * rowH, email_lbl);
                //电话1textbox
                TextBox email_txt = new TextBox();
                SetText("email" + i + "_txt", columns6X, columns6Y + spaceH + (2 * i - 3) * rowH, email_txt);

                this.contract_gb.Controls.Add(lbl);
                this.contract_gb.Controls.Add(contract_txt);
                this.contract_gb.Controls.Add(phone1lbl);
                this.contract_gb.Controls.Add(phone1_txt);
                this.contract_gb.Controls.Add(phone2lbl);
                this.contract_gb.Controls.Add(phone2_txt);
                this.contract_gb.Controls.Add(email_lbl);
                this.contract_gb.Controls.Add(email_txt);

            }

            //groupbox增高
            this.contract_gb.Height += spaceH + num * rowH;
            //tabpage增高
            this.tabPage1.Height += spaceH + num * rowH;
            //tabControl增高
            this.tabControl1.Height += spaceH + num * rowH;
            
        }
        /// <summary>
        /// 设置label属性
        /// </summary>
        /// <param name="text"></param>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="lbl"></param>
        private static void SetLabel(string text ,string name,int x, int y, Label lbl)
        {
            lbl.Name = name;
            lbl.AutoSize = false;
            lbl.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            lbl.Text = text;
            lbl.TextAlign = ContentAlignment.MiddleLeft;
            lbl.Size = new System.Drawing.Size(44, 17);
            lbl.Location = new Point(x, y);
        }
        /// <summary>
        /// 设置textbox的属性
        /// </summary>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="txt"></param>
        private static void SetText(string name,int x, int y, TextBox txt)
        {
            txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            txt.Location = new System.Drawing.Point(x, y);
            txt.Name = name;
            txt.Size = new System.Drawing.Size(126, 23);
            
        }

        /// <summary>
        /// 决策按钮单击后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string companyName = this.companyname_cmb.SelectedValue.ToString();
            if (string.IsNullOrWhiteSpace(companyName))
            {
                MessageUtil.ShowWarning("公司名称不能为空！");
                return;
            }
            List<OrgContacts> list = sbll.getOrgContactsByCompanyName(companyName);
            SelfCerAndBaseInfoDecision_Form scFrm = new SelfCerAndBaseInfoDecision_Form(list, companyName);
            scFrm.StartPosition = FormStartPosition.CenterScreen; 
            scFrm.ShowDialog();
            
        }

        private void companyname_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            //初始化控件的数据
            InitialData();
        }

    }
}
