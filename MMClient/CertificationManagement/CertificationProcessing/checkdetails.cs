﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;
//using His.WebService.Utility;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using MMClient.CommFileShow;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class checkdetails : Form
    {

        FileShowForm fileShowForm = null;
        FTPHelper fTPHelper = new FTPHelper("");
        private string id;
        private string sname;
        public checkdetails(string id,string sname)
        {
            this.id = id;
            this.sname = sname;
            InitializeComponent();
        }

        private void checkdetails_Load(object sender, EventArgs e)
        {
            FTPHelper fTPHelper1 = new FTPHelper( this.id + "/PreEvalFile" + "/Evaluation");
            string[] filenames = fTPHelper1.GetFileList();
           
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.checkdetail(this.id);
            dataGridView1.DataSource = dt;
            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "撤回";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 100;
            button1.HeaderText = "撤回";
            this.dataGridView1.Columns.AddRange(button1);

            List<string> list = new List<string>();
            string obj = "";
            float a=0,b=0,c=0;
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                obj = dataGridView1.Rows[i].Cells["评估分数"].Value.ToString();
                //list.Add(obj);
                a = a + float.Parse(obj);
                obj = null;

            }
            textScore.Text = a.ToString("F0");
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                obj = dataGridView1.Rows[i].Cells["总分"].Value.ToString();
                //list.Add(obj);
                b = b + float.Parse(obj);
                obj = null;

            }
            textSum.Text = b.ToString("F0");
            c = (a / b)*100;
            textpercent.Text = c.ToString("F2");
            if (filenames == null) return;
            for (int i = 0; i < filenames.Length; i++)
                this.checkedListBox1.Items.Add(filenames[i]);


        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {


                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//单击撤回对应事件
                {
                    string eid = dataGridView1.CurrentRow.Cells[1].Value.ToString();

                    GeneralBLL gn = new GeneralBLL();
                    gn.back(this.id,eid);
                    gn.Updatetask1(dataGridView1.CurrentRow.Cells[1].Value.ToString(),this.sname+ "待预评审");
                    DataTable dt = gn.checkdetail(this.id);
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)//下载选中的文件
        {
            string mess = "请选择要下载的文件";
            FTPHelper fTPHelper1 = new FTPHelper( this.id + "/PreEvalFile" + "/Evaluation");
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            BrowDialog.ShowDialog();
            string pathname = BrowDialog.SelectedPath;
            try
            {
                if (pathname != null)
                {
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)
                    {


                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                            mess = fTPHelper1.Download(pathname, filename);

                        }

                    }
                    MessageUtil.ShowTips(mess);


                }

            }
            catch (Exception ex)
            {

                MessageUtil.ShowTips(ex.Message);

            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            string filePath = this.id + "//PreEvalFile//Evaluation//";

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    fileIDList.Add(filename);

                }

            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];

            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try
            {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }



            }
            catch (Exception)
            {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);
        }
    }
}
