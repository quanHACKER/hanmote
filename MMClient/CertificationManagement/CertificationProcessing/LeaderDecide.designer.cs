﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class LeaderDecide
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.select_button = new System.Windows.Forms.Button();
            this.state_comboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.status_comboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.thing_textBox = new System.Windows.Forms.TextBox();
            this.物料组 = new System.Windows.Forms.Label();
            this.PurchaseOrg_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.supplierInfoTable = new System.Windows.Forms.DataGridView();
            this.IsAgree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.状态追踪 = new System.Windows.Forms.DataGridViewLinkColumn();
            this.供应商编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.区域 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.联系人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.联系方式 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.电子邮件 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.提交状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.处理 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.反馈 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierInfoTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.select_button);
            this.panel3.Controls.Add(this.state_comboBox);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.status_comboBox);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.thing_textBox);
            this.panel3.Controls.Add(this.物料组);
            this.panel3.Controls.Add(this.PurchaseOrg_textBox);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(-2, -3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(972, 69);
            this.panel3.TabIndex = 26;
            // 
            // select_button
            // 
            this.select_button.Location = new System.Drawing.Point(827, 37);
            this.select_button.Name = "select_button";
            this.select_button.Size = new System.Drawing.Size(75, 23);
            this.select_button.TabIndex = 44;
            this.select_button.Text = "查询";
            this.select_button.UseVisualStyleBackColor = true;
            this.select_button.Click += new System.EventHandler(this.select_button_Click_1);
            // 
            // state_comboBox
            // 
            this.state_comboBox.FormattingEnabled = true;
            this.state_comboBox.Items.AddRange(new object[] {
            "中国",
            "美国",
            "日本",
            "德国"});
            this.state_comboBox.Location = new System.Drawing.Point(516, 39);
            this.state_comboBox.Name = "state_comboBox";
            this.state_comboBox.Size = new System.Drawing.Size(121, 20);
            this.state_comboBox.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(514, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 42;
            this.label4.Text = "国家";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(681, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(679, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 40;
            this.label3.Text = "名称";
            // 
            // status_comboBox
            // 
            this.status_comboBox.Enabled = false;
            this.status_comboBox.FormattingEnabled = true;
            this.status_comboBox.Items.AddRange(new object[] {
            "已处理",
            "未处理"});
            this.status_comboBox.Location = new System.Drawing.Point(350, 39);
            this.status_comboBox.Name = "status_comboBox";
            this.status_comboBox.Size = new System.Drawing.Size(121, 20);
            this.status_comboBox.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(348, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 38;
            this.label2.Text = "状态";
            // 
            // thing_textBox
            // 
            this.thing_textBox.Location = new System.Drawing.Point(213, 39);
            this.thing_textBox.Name = "thing_textBox";
            this.thing_textBox.Size = new System.Drawing.Size(100, 21);
            this.thing_textBox.TabIndex = 37;
            // 
            // 物料组
            // 
            this.物料组.AutoSize = true;
            this.物料组.Location = new System.Drawing.Point(211, 22);
            this.物料组.Name = "物料组";
            this.物料组.Size = new System.Drawing.Size(41, 12);
            this.物料组.TabIndex = 36;
            this.物料组.Text = "物料组";
            // 
            // PurchaseOrg_textBox
            // 
            this.PurchaseOrg_textBox.Location = new System.Drawing.Point(73, 39);
            this.PurchaseOrg_textBox.Name = "PurchaseOrg_textBox";
            this.PurchaseOrg_textBox.Size = new System.Drawing.Size(100, 21);
            this.PurchaseOrg_textBox.TabIndex = 35;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 23);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 34;
            this.label1.Text = "采购组织";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.supplierInfoTable);
            this.panel1.Location = new System.Drawing.Point(-2, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(969, 548);
            this.panel1.TabIndex = 24;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.pageNext1);
            this.panel5.Location = new System.Drawing.Point(7, 468);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(949, 41);
            this.panel5.TabIndex = 28;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.AutoSize = true;
            this.pageNext1.Location = new System.Drawing.Point(152, 3);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(660, 37);
            this.pageNext1.TabIndex = 27;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // supplierInfoTable
            // 
            this.supplierInfoTable.AllowUserToAddRows = false;
            this.supplierInfoTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supplierInfoTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.supplierInfoTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.supplierInfoTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.supplierInfoTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.supplierInfoTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.supplierInfoTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsAgree,
            this.状态追踪,
            this.供应商编号,
            this.供应商名称,
            this.区域,
            this.联系人,
            this.联系方式,
            this.电子邮件,
            this.状态,
            this.提交状态,
            this.处理,
            this.反馈});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.supplierInfoTable.DefaultCellStyle = dataGridViewCellStyle6;
            this.supplierInfoTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.supplierInfoTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.supplierInfoTable.Location = new System.Drawing.Point(0, 6);
            this.supplierInfoTable.Name = "supplierInfoTable";
            this.supplierInfoTable.RowTemplate.Height = 23;
            this.supplierInfoTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.supplierInfoTable.Size = new System.Drawing.Size(956, 453);
            this.supplierInfoTable.TabIndex = 10;
            this.supplierInfoTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.supplierInfoTable_CellContentClick);
            // 
            // IsAgree
            // 
            this.IsAgree.DataPropertyName = "IsAgree";
            dataGridViewCellStyle2.NullValue = "反馈";
            this.IsAgree.DefaultCellStyle = dataGridViewCellStyle2;
            this.IsAgree.HeaderText = "评审状态";
            this.IsAgree.Name = "IsAgree";
            this.IsAgree.Visible = false;
            // 
            // 状态追踪
            // 
            dataGridViewCellStyle3.NullValue = "查看状态";
            this.状态追踪.DefaultCellStyle = dataGridViewCellStyle3;
            this.状态追踪.HeaderText = "状态追踪";
            this.状态追踪.Name = "状态追踪";
            this.状态追踪.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.状态追踪.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // 供应商编号
            // 
            this.供应商编号.DataPropertyName = "供应商编号";
            this.供应商编号.HeaderText = "供应商编号";
            this.供应商编号.Name = "供应商编号";
            // 
            // 供应商名称
            // 
            this.供应商名称.DataPropertyName = "供应商名称";
            this.供应商名称.HeaderText = "供应商名称";
            this.供应商名称.Name = "供应商名称";
            // 
            // 区域
            // 
            this.区域.DataPropertyName = "区域";
            this.区域.HeaderText = "区域";
            this.区域.Name = "区域";
            // 
            // 联系人
            // 
            this.联系人.DataPropertyName = "联系人";
            this.联系人.HeaderText = "联系人";
            this.联系人.Name = "联系人";
            // 
            // 联系方式
            // 
            this.联系方式.DataPropertyName = "联系方式";
            this.联系方式.HeaderText = "联系方式";
            this.联系方式.Name = "联系方式";
            // 
            // 电子邮件
            // 
            this.电子邮件.DataPropertyName = "电子邮件";
            this.电子邮件.HeaderText = "电子邮件";
            this.电子邮件.Name = "电子邮件";
            // 
            // 状态
            // 
            this.状态.DataPropertyName = "状态";
            this.状态.HeaderText = "状态";
            this.状态.Name = "状态";
            this.状态.Visible = false;
            // 
            // 提交状态
            // 
            this.提交状态.DataPropertyName = "submit_status";
            this.提交状态.HeaderText = "提交状态";
            this.提交状态.Name = "提交状态";
            // 
            // 处理
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = "处理";
            this.处理.DefaultCellStyle = dataGridViewCellStyle4;
            this.处理.HeaderText = "处理";
            this.处理.Name = "处理";
            // 
            // 反馈
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = "反馈";
            this.反馈.DefaultCellStyle = dataGridViewCellStyle5;
            this.反馈.HeaderText = "反馈";
            this.反馈.Name = "反馈";
            this.反馈.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 100);
            this.panel4.TabIndex = 0;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.Size = new System.Drawing.Size(100, 25);
            this.bindingNavigator.TabIndex = 0;
            // 
            // LeaderDecide
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 584);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "LeaderDecide";
            this.Text = "领导决策";
            this.Load += new System.EventHandler(this.LeaderDecide_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierInfoTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button select_button;
        private System.Windows.Forms.ComboBox state_comboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox status_comboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox thing_textBox;
        private System.Windows.Forms.Label 物料组;
        private System.Windows.Forms.TextBox PurchaseOrg_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.DataGridView supplierInfoTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsAgree;
        private System.Windows.Forms.DataGridViewLinkColumn 状态追踪;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 区域;
        private System.Windows.Forms.DataGridViewTextBoxColumn 联系人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 联系方式;
        private System.Windows.Forms.DataGridViewTextBoxColumn 电子邮件;
        private System.Windows.Forms.DataGridViewTextBoxColumn 状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 提交状态;
        private System.Windows.Forms.DataGridViewButtonColumn 处理;
        private System.Windows.Forms.DataGridViewButtonColumn 反馈;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.Panel panel5;
    }
}