﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class SupplierAccessFilter : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 10;
        GeneralBLL gn = new GeneralBLL();

      
        public SupplierAccessFilter()
        {
            InitializeComponent();
           
        }
     
        /// <summary>
        /// 筛选查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void select_button_Click(object sender, EventArgs e)
        {
            string status = "";
            //  status = status_comboBox.Text;
            string country = "";
            country = state_comboBox.Text;
            string name = "";
            string purchase = "";
            purchase = PurchaseOrg_textBox.Text;
            string thingGroup = "";
           thingGroup = thing_textBox.Text;
            name = textBox1.Text;
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetInfo(SingleUserInstance.getCurrentUserInstance().User_ID, purchase,thingGroup,status, country, name);
            pageNext1.DrawControl(dt.Rows.Count);
            dataGridView1.DataSource = dt;

           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfo();
            dataGridView1.DataSource = dt;*/


        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        /// <summary>
        /// 筛选预加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplierAccessFilter_Load(object sender, EventArgs e)
        {

            LoadData();
            //筛选页面加载所有信息
           
            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "处理";
            this.dataGridView1.Columns.AddRange(button1);

            DataGridViewButtonColumn button2 = new DataGridViewButtonColumn();
            button2.Name = "button2";
            button2.Text = "拒绝";//加上这两个就能显示
            button2.UseColumnTextForButtonValue = true;//
            button2.Width = 50;
            button2.HeaderText = "拒绝";
            this.dataGridView1.Columns.AddRange(button2);



            DataGridViewButtonColumn button3 = new DataGridViewButtonColumn();
            button3.Name = "button3";
            button3.Text = "重置";//加上这两个就能显示
            button3.UseColumnTextForButtonValue = true;//
            button3.Width = 50;
            button3.HeaderText = "重置";
            this.dataGridView1.Columns.AddRange(button3);


        }
        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {
            
            if (e.RowIndex >= 0)
            {

                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//单击处理对应事件
                {
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    DataTable dt1 = gn.GetAllInfo(SingleUserInstance.getCurrentUserInstance().User_ID);
                   // dataGridView1.DataSource = dt1;
                    if (gn.Get(id))
                    {
                        DataTable lst = gn.GetAll(id);
                        string supplier_name = lst.Rows[0][1].ToString();
                        string dbs = lst.Rows[0][2].ToString();
                        string country = lst.Rows[0][3].ToString();
                        string province = lst.Rows[0][4].ToString();
                        string city = lst.Rows[0][5].ToString();
                        string address = lst.Rows[0][7].ToString();
                        string postcode = lst.Rows[0][6].ToString();
                        string contactname = lst.Rows[0][8].ToString();
                        string position = lst.Rows[0][9].ToString();
                        string email = lst.Rows[0][10].ToString();
                        string phonenumber = lst.Rows[0][11].ToString();
                        string website = lst.Rows[0][12].ToString();
                        string fox = lst.Rows[0][13].ToString();
                        string rich = lst.Rows[0][14].ToString();
                        string org = lst.Rows[0][15].ToString();
                        string thing = lst.Rows[0][16].ToString();
                        string structure = lst.Rows[0][17].ToString();
                        string style = lst.Rows[0][18].ToString();
                        string volume = lst.Rows[0][19].ToString();
                        string device = lst.Rows[0][20].ToString();
                        string product = lst.Rows[0][21].ToString();
                        string trade = lst.Rows[0][22].ToString();
                        string condition = lst.Rows[0][23].ToString();
                        string certification = lst.Rows[0][25].ToString();
                        string other = lst.Rows[0]["PlanToCName"].ToString();
                        string name= lst.Rows[0]["PlanToCName"].ToString();
                        string plan = lst.Rows[0]["PlanToCTime"].ToString();
                        //gn.RestartInf(id);//处理状态置1和旧的
                        DataTable dt = gn.GetAllInfo(SingleUserInstance.getCurrentUserInstance().User_ID);
                       // dataGridView1.DataSource = dt;
                        detailForm nm = new detailForm(supplier_name, dbs, country, province, city, address, postcode, contactname, position, email, phonenumber, website, fox, rich, org, thing, structure, style,volume,device,product,trade,condition,certification,other,id, SingleUserInstance.getCurrentUserInstance().User_ID, supplier_name,name,plan);
                        nm.Show();
                    }
                    else
                    {
                        MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                        
                        return;
                    }


                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button2")//单击拒绝对应事件
                {
                    string email = dataGridView1.CurrentRow.Cells["电子邮件"].Value.ToString();
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    DataTable dt = gn.GetAllInfo(SingleUserInstance.getCurrentUserInstance().User_ID);
                    dataGridView1.DataSource = dt;
                    if (gn.Get(id))
                    {
                        //gn.RejectInf(id);//处理状态置1和旧的
                        DataTable dt1 = gn.GetAllInfo(SingleUserInstance.getCurrentUserInstance().User_ID);
                        dataGridView1.DataSource = dt1;
                        denyForm nm = new denyForm(email,id);
                        nm.Show();
                        
                    }
                    else
                    {
                        MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                        return;
                    }
                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button3")//单击重置对应事件
                {
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    string zzid = dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString() + "待主审预评";
                    string zzid1 = "请对供应商：" + dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString() + "待筛选";
                    gn.RestartInfo(id);
                   // gn.Updatetask1(SingleUserInstance.getCurrentUserInstance().User_ID, zzid1);
                    string supplierName = dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString();
                    gn.updateAndDeleteTask(SingleUserInstance.getCurrentUserInstance().User_ID,supplierName);
                    // gn.adeletetask1(zzid);

                    DataTable dt = gn.GetAllInfo(SingleUserInstance.getCurrentUserInstance().User_ID);
                    dataGridView1.DataSource = dt;
                    
                }

            }
        
    }

    

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string email= dataGridView1.CurrentRow.Cells["电子邮件"].Value.ToString();
           // denyForm nm = new denyForm(email,id0);
          //  nm.Show();
            

    }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {

            LoadData();

        }

        public void LoadData() {
            int totalSize = 0;
            DataTable dt = gn.page(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, this.pageNext1.PageIndex,out totalSize);
            dataGridView1.DataSource = dt;
            pageNext1.DrawControl(totalSize);
        } 


    }
}
