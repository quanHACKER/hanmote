﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class Evaluate : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Evaluate()
        {
            InitializeComponent();
        }

        private void Evaluate_Load(object sender, EventArgs e)
        {//评审员登录时把登录名带过来
            /*BaseUserModel baseUserModel = new BaseUserModel();
            string eid = "";
            eid = baseUserModel.Login_Name;
            PurchaseOrg_textBox.Text = eid;*/
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfoe(SingleUserInstance.getCurrentUserInstance().User_ID);
            dataGridView1.DataSource = dt;
            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "处理";
            this.dataGridView1.Columns.AddRange(button1);

           /* DataGridViewButtonColumn button2 = new DataGridViewButtonColumn();
            button2.Name = "button2";
            button2.Text = "拒绝";//加上这两个就能显示
            button2.UseColumnTextForButtonValue = true;//
            button2.Width = 50;
            button2.HeaderText = "操作2";
            this.dataGridView1.Columns.AddRange(button2);*/



            /*DataGridViewButtonColumn button3 = new DataGridViewButtonColumn();
            button3.Name = "button3";
            button3.Text = "重置";//加上这两个就能显示
            button3.UseColumnTextForButtonValue = true;//
            button3.Width = 50;
            button3.HeaderText = "操作3";
            this.dataGridView1.Columns.AddRange(button3);*/
        }
        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {


                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//单击处理对应事件
                {
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string eid = dataGridView1.CurrentRow.Cells["评估员编号"].Value.ToString();
                    string email = dataGridView1.CurrentRow.Cells["邮箱"].Value.ToString();
                    //PurchaseOrg_textBox.Text = id;
                    //thing_textBox.Text = eid;
                    GeneralBLL gn = new GeneralBLL();
                    // PurchaseOrg_textBox.Text = id;
                    if (gn.fGetStatus(id))
                    {

                        if (gn.GetStatus2(id, eid))
                        {
                            EvaluateCertificate nm = new EvaluateCertificate(id, eid);
                            nm.Show();
                        }
                        else
                        {
                            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                            //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                            //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                            DialogResult dr = MessageBox.Show("确定要重新处理吗吗?", "重新处理", messButton);
                            if (dr == DialogResult.OK)//如果点击“确定”按钮
                            {
                                gn.deleteInfo1(id, eid);
                                EvaluateCertificate nm = new EvaluateCertificate(id, eid);
                                nm.Show();
                            }
                            else//如果点击“取消”按钮
                            {

                            }
                        }
                    }
                    else
                    {
                        MessageUtil.ShowWarning("供应商未上传文件！");
                        return;
                    }

                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button2")//单击拒绝对应事件
                {
                    /*string id = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    string email = dataGridView1.CurrentRow.Cells[9].Value.ToString();
                    string eid = dataGridView1.CurrentRow.Cells[10].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    // PurchaseOrg_textBox.Text = id;
                    if (gn.GetStatus2(id, eid))
                    {
                        denyForm nm = new denyForm(email, id);
                        nm.Show();
                    }
                    else
                    {
                        MessageUtil.ShowWarning("您已经对此供应商进行过相应操作了！");
                        return;
                    }*/
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string eid = dataGridView1.CurrentRow.Cells["评估员编号"].Value.ToString();
                    //PurchaseOrg_textBox.Text = id;
                    //thing_textBox.Text = eid;
                    GeneralBLL gn = new GeneralBLL();
                    // PurchaseOrg_textBox.Text = id;
                    if (gn.GetStatus2(id, eid))
                    {
                        deny1 nm = new deny1(id, eid);
                        nm.Show();
                    }
                    else
                    {
                        MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                        //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                        //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                        DialogResult dr = MessageBox.Show("确定要重新处理吗吗?", "重新处理", messButton);
                        if (dr == DialogResult.OK)//如果点击“确定”按钮
                        {
                            //gn.deleteInfo1(id, eid);
                            deny1 nm = new deny1(id, eid);
                            nm.Show();
                        }
                        else//如果点击“取消”按钮
                        {

                        }
                    }
                    }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button3")//单击重置对应事件
                {
                    /*string id = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    gn.RestartInfo(id);
                    DataTable dt = gn.GetAllInfo();
                    dataGridView1.DataSource = dt;*/

                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    string eid = dataGridView1.CurrentRow.Cells["评估员编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    gn.GetStatus3(id, eid);
                    gn.deleteInfo1(id, eid);
                    DataTable dt = gn.GetAllInfoe(SingleUserInstance.getCurrentUserInstance().User_ID);
                    dataGridView1.DataSource = dt;
                }

            }

        }

        private void select_button_Click(object sender, EventArgs e)//单个评审员查询
        {
            string status = "";
            status = status_comboBox.Text;
            string country = "";
            country = state_comboBox.Text;
            string name = "";
            string purchase = "";
            purchase = PurchaseOrg_textBox.Text;
            string thingGroup = "";
            thingGroup = thing_textBox.Text;
            name = textBox1.Text;
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.eGetInfo(SingleUserInstance.getCurrentUserInstance().User_ID,purchase, thingGroup, status, country, name);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)//刷新
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfoe(SingleUserInstance.getCurrentUserInstance().User_ID);
            dataGridView1.DataSource = dt;
           /* DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "操作1";
            this.dataGridView1.Columns.AddRange(button1);

            DataGridViewButtonColumn button2 = new DataGridViewButtonColumn();
            button2.Name = "button2";
            button2.Text = "拒绝";//加上这两个就能显示
            button2.UseColumnTextForButtonValue = true;//
            button2.Width = 50;
            button2.HeaderText = "操作2";
            this.dataGridView1.Columns.AddRange(button2);



            DataGridViewButtonColumn button3 = new DataGridViewButtonColumn();
            button3.Name = "button3";
            button3.Text = "重置";//加上这两个就能显示
            button3.UseColumnTextForButtonValue = true;//
            button3.Width = 50;
            button3.HeaderText = "操作3";
            this.dataGridView1.Columns.AddRange(button3);*/
        }
    }
}
