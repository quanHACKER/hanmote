﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class decideStatusShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.decideStatusTable = new System.Windows.Forms.DataGridView();
            this.审批层级 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.审批状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.审批人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.审批层数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.decideStatusTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.decideStatusTable);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 234);
            this.panel1.TabIndex = 0;
            // 
            // decideStatusTable
            // 
            this.decideStatusTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.decideStatusTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.decideStatusTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.decideStatusTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.decideStatusTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.审批层级,
            this.审批状态,
            this.审批人,
            this.审批层数});
            this.decideStatusTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.decideStatusTable.Location = new System.Drawing.Point(3, 3);
            this.decideStatusTable.Name = "decideStatusTable";
            this.decideStatusTable.RowTemplate.Height = 23;
            this.decideStatusTable.Size = new System.Drawing.Size(512, 228);
            this.decideStatusTable.TabIndex = 1;
            // 
            // 审批层级
            // 
            this.审批层级.DataPropertyName = "审批层级";
            this.审批层级.HeaderText = "审批层级";
            this.审批层级.Name = "审批层级";
            // 
            // 审批状态
            // 
            this.审批状态.DataPropertyName = "审批状态";
            this.审批状态.HeaderText = "审批状态";
            this.审批状态.Name = "审批状态";
            // 
            // 审批人
            // 
            this.审批人.DataPropertyName = "审批人";
            this.审批人.HeaderText = "审批人";
            this.审批人.Name = "审批人";
            // 
            // 审批层数
            // 
            this.审批层数.DataPropertyName = "审批层数";
            this.审批层数.HeaderText = "审批层数";
            this.审批层数.Name = "审批层数";
            // 
            // decideStatusShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 255);
            this.Controls.Add(this.panel1);
            this.Name = "decideStatusShow";
            this.Text = "评审状态";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.decideStatusTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView decideStatusTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn 审批层级;
        private System.Windows.Forms.DataGridViewTextBoxColumn 审批状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 审批人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 审批层数;
    }
}