﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class detailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.b1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.companyFileLink = new System.Windows.Forms.LinkLabel();
            this.filename4 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.style_textBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.struture_textBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.thing_textBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.org_textBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.fax_textBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.website_textBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.phonenumber_textBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.email_textBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.position_textBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.contactname_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.postcode_textBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.address_textBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.city_textBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.province_textBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.country_textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dbscode_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.suppliername_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.b2 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.t12 = new System.Windows.Forms.TextBox();
            this.t13 = new System.Windows.Forms.TextBox();
            this.t14 = new System.Windows.Forms.TextBox();
            this.t15 = new System.Windows.Forms.TextBox();
            this.t11 = new System.Windows.Forms.TextBox();
            this.t7 = new System.Windows.Forms.TextBox();
            this.t8 = new System.Windows.Forms.TextBox();
            this.t9 = new System.Windows.Forms.TextBox();
            this.t10 = new System.Windows.Forms.TextBox();
            this.t6 = new System.Windows.Forms.TextBox();
            this.t2 = new System.Windows.Forms.TextBox();
            this.t3 = new System.Windows.Forms.TextBox();
            this.t4 = new System.Windows.Forms.TextBox();
            this.t5 = new System.Windows.Forms.TextBox();
            this.t1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label2017 = new System.Windows.Forms.Label();
            this.label2014 = new System.Windows.Forms.Label();
            this.label2015 = new System.Windows.Forms.Label();
            this.label2016 = new System.Windows.Forms.Label();
            this.label2013 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.b3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.te30 = new System.Windows.Forms.TextBox();
            this.te24 = new System.Windows.Forms.TextBox();
            this.te18 = new System.Windows.Forms.TextBox();
            this.te12 = new System.Windows.Forms.TextBox();
            this.te6 = new System.Windows.Forms.TextBox();
            this.c27 = new System.Windows.Forms.CheckBox();
            this.c28 = new System.Windows.Forms.CheckBox();
            this.c29 = new System.Windows.Forms.CheckBox();
            this.c30 = new System.Windows.Forms.CheckBox();
            this.c26 = new System.Windows.Forms.CheckBox();
            this.c21 = new System.Windows.Forms.CheckBox();
            this.c22 = new System.Windows.Forms.CheckBox();
            this.c23 = new System.Windows.Forms.CheckBox();
            this.c24 = new System.Windows.Forms.CheckBox();
            this.c20 = new System.Windows.Forms.CheckBox();
            this.c15 = new System.Windows.Forms.CheckBox();
            this.c16 = new System.Windows.Forms.CheckBox();
            this.c17 = new System.Windows.Forms.CheckBox();
            this.c18 = new System.Windows.Forms.CheckBox();
            this.c14 = new System.Windows.Forms.CheckBox();
            this.c9 = new System.Windows.Forms.CheckBox();
            this.c10 = new System.Windows.Forms.CheckBox();
            this.c11 = new System.Windows.Forms.CheckBox();
            this.c12 = new System.Windows.Forms.CheckBox();
            this.c8 = new System.Windows.Forms.CheckBox();
            this.c3 = new System.Windows.Forms.CheckBox();
            this.c4 = new System.Windows.Forms.CheckBox();
            this.c5 = new System.Windows.Forms.CheckBox();
            this.c6 = new System.Windows.Forms.CheckBox();
            this.c2 = new System.Windows.Forms.CheckBox();
            this.te7 = new System.Windows.Forms.TextBox();
            this.te13 = new System.Windows.Forms.TextBox();
            this.te19 = new System.Windows.Forms.TextBox();
            this.te25 = new System.Windows.Forms.TextBox();
            this.te1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.b4 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tex4 = new System.Windows.Forms.TextBox();
            this.tex6 = new System.Windows.Forms.TextBox();
            this.tex8 = new System.Windows.Forms.TextBox();
            this.tex10 = new System.Windows.Forms.TextBox();
            this.tex2 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tex3 = new System.Windows.Forms.TextBox();
            this.tex5 = new System.Windows.Forms.TextBox();
            this.tex7 = new System.Windows.Forms.TextBox();
            this.tex9 = new System.Windows.Forms.TextBox();
            this.tex1 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.b5 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textB10 = new System.Windows.Forms.TextBox();
            this.textB15 = new System.Windows.Forms.TextBox();
            this.textB20 = new System.Windows.Forms.TextBox();
            this.textB25 = new System.Windows.Forms.TextBox();
            this.textB5 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textB9 = new System.Windows.Forms.TextBox();
            this.textB14 = new System.Windows.Forms.TextBox();
            this.textB19 = new System.Windows.Forms.TextBox();
            this.textB24 = new System.Windows.Forms.TextBox();
            this.textB4 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textB8 = new System.Windows.Forms.TextBox();
            this.textB13 = new System.Windows.Forms.TextBox();
            this.textB18 = new System.Windows.Forms.TextBox();
            this.textB23 = new System.Windows.Forms.TextBox();
            this.textB3 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.textB7 = new System.Windows.Forms.TextBox();
            this.textB12 = new System.Windows.Forms.TextBox();
            this.textB17 = new System.Windows.Forms.TextBox();
            this.textB22 = new System.Windows.Forms.TextBox();
            this.textB2 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textB6 = new System.Windows.Forms.TextBox();
            this.textB11 = new System.Windows.Forms.TextBox();
            this.textB16 = new System.Windows.Forms.TextBox();
            this.textB21 = new System.Windows.Forms.TextBox();
            this.textB1 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.b6 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.研发条件 = new System.Windows.Forms.Label();
            this.lk_label_studyAble = new System.Windows.Forms.LinkLabel();
            this.text8 = new System.Windows.Forms.TextBox();
            this.text7 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.text6 = new System.Windows.Forms.TextBox();
            this.text5 = new System.Windows.Forms.TextBox();
            this.text4 = new System.Windows.Forms.TextBox();
            this.text3 = new System.Windows.Forms.TextBox();
            this.text2 = new System.Windows.Forms.TextBox();
            this.text1 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.b7 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBo3 = new System.Windows.Forms.TextBox();
            this.textBo2 = new System.Windows.Forms.TextBox();
            this.textBo1 = new System.Windows.Forms.TextBox();
            this.ch4 = new System.Windows.Forms.CheckBox();
            this.ch3 = new System.Windows.Forms.CheckBox();
            this.ch2 = new System.Windows.Forms.CheckBox();
            this.ch1 = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Lk_Label_MarketAnalysis = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Lk_Label_ProcStrategy = new System.Windows.Forms.LinkLabel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(9, 11);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(807, 440);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.b1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(799, 414);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "公司概况";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // b1
            // 
            this.b1.Location = new System.Drawing.Point(359, 365);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(75, 23);
            this.b1.TabIndex = 6;
            this.b1.Text = "查看下一项";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 1);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "公司概况";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.companyFileLink);
            this.panel1.Controls.Add(this.filename4);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.style_textBox);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.struture_textBox);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.thing_textBox);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.org_textBox);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.fax_textBox);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.website_textBox);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.phonenumber_textBox);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.email_textBox);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.position_textBox);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.contactname_textBox);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.postcode_textBox);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.address_textBox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.city_textBox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.province_textBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.country_textBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dbscode_textBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.suppliername_textBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(20, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(758, 385);
            this.panel1.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(238, 270);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 12);
            this.label36.TabIndex = 42;
            this.label36.Text = "供应商附件";
            // 
            // companyFileLink
            // 
            this.companyFileLink.AutoSize = true;
            this.companyFileLink.Location = new System.Drawing.Point(328, 270);
            this.companyFileLink.Name = "companyFileLink";
            this.companyFileLink.Size = new System.Drawing.Size(65, 12);
            this.companyFileLink.TabIndex = 41;
            this.companyFileLink.TabStop = true;
            this.companyFileLink.Text = "linkLabel1";
            this.companyFileLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.companyFileLink_LinkClicked);
            // 
            // filename4
            // 
            this.filename4.Location = new System.Drawing.Point(611, 300);
            this.filename4.Name = "filename4";
            this.filename4.ReadOnly = true;
            this.filename4.Size = new System.Drawing.Size(142, 21);
            this.filename4.TabIndex = 40;
            this.filename4.Visible = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(481, 288);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(110, 42);
            this.button7.TabIndex = 39;
            this.button7.Text = "公司具有条件下载";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // style_textBox
            // 
            this.style_textBox.Location = new System.Drawing.Point(330, 309);
            this.style_textBox.Name = "style_textBox";
            this.style_textBox.ReadOnly = true;
            this.style_textBox.Size = new System.Drawing.Size(142, 21);
            this.style_textBox.TabIndex = 37;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(238, 313);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 36;
            this.label19.Text = "所有制形式";
            // 
            // struture_textBox
            // 
            this.struture_textBox.Location = new System.Drawing.Point(75, 309);
            this.struture_textBox.Name = "struture_textBox";
            this.struture_textBox.ReadOnly = true;
            this.struture_textBox.Size = new System.Drawing.Size(100, 21);
            this.struture_textBox.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 313);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "所有制结构";
            // 
            // thing_textBox
            // 
            this.thing_textBox.Location = new System.Drawing.Point(75, 281);
            this.thing_textBox.Name = "thing_textBox";
            this.thing_textBox.ReadOnly = true;
            this.thing_textBox.Size = new System.Drawing.Size(100, 21);
            this.thing_textBox.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 285);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 32;
            this.label16.Text = "已选物料组";
            // 
            // org_textBox
            // 
            this.org_textBox.Location = new System.Drawing.Point(76, 255);
            this.org_textBox.Name = "org_textBox";
            this.org_textBox.ReadOnly = true;
            this.org_textBox.Size = new System.Drawing.Size(100, 21);
            this.org_textBox.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1, 258);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 12);
            this.label17.TabIndex = 30;
            this.label17.Text = "所属业务部门";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(76, 181);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(530, 68);
            this.richTextBox1.TabIndex = 27;
            this.richTextBox1.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 181);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 26;
            this.label15.Text = "公司简介";
            // 
            // fax_textBox
            // 
            this.fax_textBox.Location = new System.Drawing.Point(265, 148);
            this.fax_textBox.Name = "fax_textBox";
            this.fax_textBox.ReadOnly = true;
            this.fax_textBox.Size = new System.Drawing.Size(100, 21);
            this.fax_textBox.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(210, 151);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 24;
            this.label14.Text = "传真";
            // 
            // website_textBox
            // 
            this.website_textBox.Location = new System.Drawing.Point(75, 145);
            this.website_textBox.Name = "website_textBox";
            this.website_textBox.ReadOnly = true;
            this.website_textBox.Size = new System.Drawing.Size(100, 21);
            this.website_textBox.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 22;
            this.label13.Text = "网址";
            // 
            // phonenumber_textBox
            // 
            this.phonenumber_textBox.Location = new System.Drawing.Point(452, 122);
            this.phonenumber_textBox.Name = "phonenumber_textBox";
            this.phonenumber_textBox.ReadOnly = true;
            this.phonenumber_textBox.Size = new System.Drawing.Size(100, 21);
            this.phonenumber_textBox.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(385, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 20;
            this.label12.Text = "电话号码";
            // 
            // email_textBox
            // 
            this.email_textBox.Location = new System.Drawing.Point(452, 89);
            this.email_textBox.Name = "email_textBox";
            this.email_textBox.ReadOnly = true;
            this.email_textBox.Size = new System.Drawing.Size(100, 21);
            this.email_textBox.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(385, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 18;
            this.label11.Text = "邮箱";
            // 
            // position_textBox
            // 
            this.position_textBox.Location = new System.Drawing.Point(265, 119);
            this.position_textBox.Name = "position_textBox";
            this.position_textBox.ReadOnly = true;
            this.position_textBox.Size = new System.Drawing.Size(100, 21);
            this.position_textBox.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(210, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 16;
            this.label10.Text = "职位";
            // 
            // contactname_textBox
            // 
            this.contactname_textBox.Location = new System.Drawing.Point(75, 118);
            this.contactname_textBox.Name = "contactname_textBox";
            this.contactname_textBox.ReadOnly = true;
            this.contactname_textBox.Size = new System.Drawing.Size(100, 21);
            this.contactname_textBox.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "联系人姓名";
            // 
            // postcode_textBox
            // 
            this.postcode_textBox.Location = new System.Drawing.Point(265, 89);
            this.postcode_textBox.Name = "postcode_textBox";
            this.postcode_textBox.ReadOnly = true;
            this.postcode_textBox.Size = new System.Drawing.Size(100, 21);
            this.postcode_textBox.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(210, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "邮编";
            // 
            // address_textBox
            // 
            this.address_textBox.Location = new System.Drawing.Point(75, 80);
            this.address_textBox.Name = "address_textBox";
            this.address_textBox.ReadOnly = true;
            this.address_textBox.Size = new System.Drawing.Size(100, 21);
            this.address_textBox.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "地址";
            // 
            // city_textBox
            // 
            this.city_textBox.Location = new System.Drawing.Point(452, 50);
            this.city_textBox.Name = "city_textBox";
            this.city_textBox.ReadOnly = true;
            this.city_textBox.Size = new System.Drawing.Size(100, 21);
            this.city_textBox.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(384, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "城市";
            // 
            // province_textBox
            // 
            this.province_textBox.Location = new System.Drawing.Point(265, 53);
            this.province_textBox.Name = "province_textBox";
            this.province_textBox.ReadOnly = true;
            this.province_textBox.Size = new System.Drawing.Size(100, 21);
            this.province_textBox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "省份";
            // 
            // country_textBox
            // 
            this.country_textBox.Location = new System.Drawing.Point(75, 53);
            this.country_textBox.Name = "country_textBox";
            this.country_textBox.ReadOnly = true;
            this.country_textBox.Size = new System.Drawing.Size(100, 21);
            this.country_textBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "国家";
            // 
            // dbscode_textBox
            // 
            this.dbscode_textBox.Location = new System.Drawing.Point(452, 14);
            this.dbscode_textBox.Name = "dbscode_textBox";
            this.dbscode_textBox.ReadOnly = true;
            this.dbscode_textBox.Size = new System.Drawing.Size(100, 21);
            this.dbscode_textBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(384, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "邓白氏编码";
            // 
            // suppliername_textBox
            // 
            this.suppliername_textBox.Location = new System.Drawing.Point(75, 14);
            this.suppliername_textBox.Name = "suppliername_textBox";
            this.suppliername_textBox.ReadOnly = true;
            this.suppliername_textBox.Size = new System.Drawing.Size(283, 21);
            this.suppliername_textBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "供应商名称";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.b2);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(799, 414);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "企业规模";
            // 
            // b2
            // 
            this.b2.Location = new System.Drawing.Point(359, 365);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(75, 23);
            this.b2.TabIndex = 7;
            this.b2.Text = "查看下一项";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.t12);
            this.panel3.Controls.Add(this.t13);
            this.panel3.Controls.Add(this.t14);
            this.panel3.Controls.Add(this.t15);
            this.panel3.Controls.Add(this.t11);
            this.panel3.Controls.Add(this.t7);
            this.panel3.Controls.Add(this.t8);
            this.panel3.Controls.Add(this.t9);
            this.panel3.Controls.Add(this.t10);
            this.panel3.Controls.Add(this.t6);
            this.panel3.Controls.Add(this.t2);
            this.panel3.Controls.Add(this.t3);
            this.panel3.Controls.Add(this.t4);
            this.panel3.Controls.Add(this.t5);
            this.panel3.Controls.Add(this.t1);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.label2017);
            this.panel3.Controls.Add(this.label2014);
            this.panel3.Controls.Add(this.label2015);
            this.panel3.Controls.Add(this.label2016);
            this.panel3.Controls.Add(this.label2013);
            this.panel3.Controls.Add(this.label38);
            this.panel3.Location = new System.Drawing.Point(6, 31);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(787, 227);
            this.panel3.TabIndex = 0;
            // 
            // t12
            // 
            this.t12.Location = new System.Drawing.Point(249, 155);
            this.t12.Name = "t12";
            this.t12.ReadOnly = true;
            this.t12.Size = new System.Drawing.Size(58, 21);
            this.t12.TabIndex = 49;
            // 
            // t13
            // 
            this.t13.Location = new System.Drawing.Point(346, 155);
            this.t13.Name = "t13";
            this.t13.ReadOnly = true;
            this.t13.Size = new System.Drawing.Size(58, 21);
            this.t13.TabIndex = 48;
            // 
            // t14
            // 
            this.t14.Location = new System.Drawing.Point(439, 155);
            this.t14.Name = "t14";
            this.t14.ReadOnly = true;
            this.t14.Size = new System.Drawing.Size(58, 21);
            this.t14.TabIndex = 47;
            // 
            // t15
            // 
            this.t15.Location = new System.Drawing.Point(531, 155);
            this.t15.Name = "t15";
            this.t15.ReadOnly = true;
            this.t15.Size = new System.Drawing.Size(58, 21);
            this.t15.TabIndex = 46;
            // 
            // t11
            // 
            this.t11.Location = new System.Drawing.Point(164, 155);
            this.t11.Name = "t11";
            this.t11.ReadOnly = true;
            this.t11.Size = new System.Drawing.Size(58, 21);
            this.t11.TabIndex = 45;
            // 
            // t7
            // 
            this.t7.Location = new System.Drawing.Point(248, 117);
            this.t7.Name = "t7";
            this.t7.ReadOnly = true;
            this.t7.Size = new System.Drawing.Size(58, 21);
            this.t7.TabIndex = 44;
            // 
            // t8
            // 
            this.t8.Location = new System.Drawing.Point(345, 117);
            this.t8.Name = "t8";
            this.t8.ReadOnly = true;
            this.t8.Size = new System.Drawing.Size(58, 21);
            this.t8.TabIndex = 43;
            // 
            // t9
            // 
            this.t9.Location = new System.Drawing.Point(438, 117);
            this.t9.Name = "t9";
            this.t9.ReadOnly = true;
            this.t9.Size = new System.Drawing.Size(58, 21);
            this.t9.TabIndex = 42;
            // 
            // t10
            // 
            this.t10.Location = new System.Drawing.Point(530, 117);
            this.t10.Name = "t10";
            this.t10.ReadOnly = true;
            this.t10.Size = new System.Drawing.Size(58, 21);
            this.t10.TabIndex = 41;
            // 
            // t6
            // 
            this.t6.Location = new System.Drawing.Point(163, 117);
            this.t6.Name = "t6";
            this.t6.ReadOnly = true;
            this.t6.Size = new System.Drawing.Size(58, 21);
            this.t6.TabIndex = 40;
            // 
            // t2
            // 
            this.t2.Location = new System.Drawing.Point(248, 75);
            this.t2.Name = "t2";
            this.t2.ReadOnly = true;
            this.t2.Size = new System.Drawing.Size(58, 21);
            this.t2.TabIndex = 39;
            // 
            // t3
            // 
            this.t3.Location = new System.Drawing.Point(345, 75);
            this.t3.Name = "t3";
            this.t3.ReadOnly = true;
            this.t3.Size = new System.Drawing.Size(58, 21);
            this.t3.TabIndex = 38;
            // 
            // t4
            // 
            this.t4.Location = new System.Drawing.Point(438, 75);
            this.t4.Name = "t4";
            this.t4.ReadOnly = true;
            this.t4.Size = new System.Drawing.Size(58, 21);
            this.t4.TabIndex = 37;
            // 
            // t5
            // 
            this.t5.Location = new System.Drawing.Point(530, 75);
            this.t5.Name = "t5";
            this.t5.ReadOnly = true;
            this.t5.Size = new System.Drawing.Size(58, 21);
            this.t5.TabIndex = 36;
            // 
            // t1
            // 
            this.t1.Location = new System.Drawing.Point(163, 75);
            this.t1.Name = "t1";
            this.t1.ReadOnly = true;
            this.t1.Size = new System.Drawing.Size(58, 21);
            this.t1.TabIndex = 35;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(67, 80);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 34;
            this.label27.Text = "营业额";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(67, 121);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 33;
            this.label28.Text = "员工数量";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(67, 160);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 12);
            this.label32.TabIndex = 32;
            this.label32.Text = "研发投入（%）";
            // 
            // label2017
            // 
            this.label2017.AutoSize = true;
            this.label2017.Location = new System.Drawing.Point(540, 50);
            this.label2017.Name = "label2017";
            this.label2017.Size = new System.Drawing.Size(29, 12);
            this.label2017.TabIndex = 31;
            this.label2017.Text = "2017";
            // 
            // label2014
            // 
            this.label2014.AutoSize = true;
            this.label2014.Location = new System.Drawing.Point(268, 50);
            this.label2014.Name = "label2014";
            this.label2014.Size = new System.Drawing.Size(29, 12);
            this.label2014.TabIndex = 30;
            this.label2014.Text = "2014";
            // 
            // label2015
            // 
            this.label2015.AutoSize = true;
            this.label2015.Location = new System.Drawing.Point(358, 50);
            this.label2015.Name = "label2015";
            this.label2015.Size = new System.Drawing.Size(29, 12);
            this.label2015.TabIndex = 29;
            this.label2015.Text = "2015";
            // 
            // label2016
            // 
            this.label2016.AutoSize = true;
            this.label2016.Location = new System.Drawing.Point(446, 50);
            this.label2016.Name = "label2016";
            this.label2016.Size = new System.Drawing.Size(29, 12);
            this.label2016.TabIndex = 28;
            this.label2016.Text = "2016";
            // 
            // label2013
            // 
            this.label2013.AutoSize = true;
            this.label2013.Location = new System.Drawing.Point(173, 50);
            this.label2013.Name = "label2013";
            this.label2013.Size = new System.Drawing.Size(29, 12);
            this.label2013.TabIndex = 27;
            this.label2013.Text = "2013";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(67, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 12);
            this.label38.TabIndex = 26;
            this.label38.Text = "年度";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.b3);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(799, 414);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "位置及类型";
            // 
            // b3
            // 
            this.b3.Location = new System.Drawing.Point(359, 365);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(75, 23);
            this.b3.TabIndex = 7;
            this.b3.Text = "查看下一项";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.te30);
            this.panel2.Controls.Add(this.te24);
            this.panel2.Controls.Add(this.te18);
            this.panel2.Controls.Add(this.te12);
            this.panel2.Controls.Add(this.te6);
            this.panel2.Controls.Add(this.c27);
            this.panel2.Controls.Add(this.c28);
            this.panel2.Controls.Add(this.c29);
            this.panel2.Controls.Add(this.c30);
            this.panel2.Controls.Add(this.c26);
            this.panel2.Controls.Add(this.c21);
            this.panel2.Controls.Add(this.c22);
            this.panel2.Controls.Add(this.c23);
            this.panel2.Controls.Add(this.c24);
            this.panel2.Controls.Add(this.c20);
            this.panel2.Controls.Add(this.c15);
            this.panel2.Controls.Add(this.c16);
            this.panel2.Controls.Add(this.c17);
            this.panel2.Controls.Add(this.c18);
            this.panel2.Controls.Add(this.c14);
            this.panel2.Controls.Add(this.c9);
            this.panel2.Controls.Add(this.c10);
            this.panel2.Controls.Add(this.c11);
            this.panel2.Controls.Add(this.c12);
            this.panel2.Controls.Add(this.c8);
            this.panel2.Controls.Add(this.c3);
            this.panel2.Controls.Add(this.c4);
            this.panel2.Controls.Add(this.c5);
            this.panel2.Controls.Add(this.c6);
            this.panel2.Controls.Add(this.c2);
            this.panel2.Controls.Add(this.te7);
            this.panel2.Controls.Add(this.te13);
            this.panel2.Controls.Add(this.te19);
            this.panel2.Controls.Add(this.te25);
            this.panel2.Controls.Add(this.te1);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Location = new System.Drawing.Point(18, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(763, 266);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // te30
            // 
            this.te30.Location = new System.Drawing.Point(358, 193);
            this.te30.Name = "te30";
            this.te30.ReadOnly = true;
            this.te30.Size = new System.Drawing.Size(100, 21);
            this.te30.TabIndex = 62;
            // 
            // te24
            // 
            this.te24.Location = new System.Drawing.Point(358, 158);
            this.te24.Name = "te24";
            this.te24.ReadOnly = true;
            this.te24.Size = new System.Drawing.Size(100, 21);
            this.te24.TabIndex = 61;
            // 
            // te18
            // 
            this.te18.Location = new System.Drawing.Point(359, 123);
            this.te18.Name = "te18";
            this.te18.ReadOnly = true;
            this.te18.Size = new System.Drawing.Size(100, 21);
            this.te18.TabIndex = 60;
            // 
            // te12
            // 
            this.te12.Location = new System.Drawing.Point(359, 86);
            this.te12.Name = "te12";
            this.te12.ReadOnly = true;
            this.te12.Size = new System.Drawing.Size(100, 21);
            this.te12.TabIndex = 59;
            // 
            // te6
            // 
            this.te6.Location = new System.Drawing.Point(359, 49);
            this.te6.Name = "te6";
            this.te6.ReadOnly = true;
            this.te6.Size = new System.Drawing.Size(100, 21);
            this.te6.TabIndex = 58;
            // 
            // c27
            // 
            this.c27.AutoSize = true;
            this.c27.Location = new System.Drawing.Point(206, 196);
            this.c27.Name = "c27";
            this.c27.Size = new System.Drawing.Size(15, 14);
            this.c27.TabIndex = 57;
            this.c27.UseVisualStyleBackColor = true;
            // 
            // c28
            // 
            this.c28.AutoSize = true;
            this.c28.Location = new System.Drawing.Point(256, 196);
            this.c28.Name = "c28";
            this.c28.Size = new System.Drawing.Size(15, 14);
            this.c28.TabIndex = 56;
            this.c28.UseVisualStyleBackColor = true;
            // 
            // c29
            // 
            this.c29.AutoSize = true;
            this.c29.Location = new System.Drawing.Point(311, 196);
            this.c29.Name = "c29";
            this.c29.Size = new System.Drawing.Size(15, 14);
            this.c29.TabIndex = 55;
            this.c29.UseVisualStyleBackColor = true;
            // 
            // c30
            // 
            this.c30.AutoSize = true;
            this.c30.Location = new System.Drawing.Point(341, 196);
            this.c30.Name = "c30";
            this.c30.Size = new System.Drawing.Size(15, 14);
            this.c30.TabIndex = 54;
            this.c30.UseVisualStyleBackColor = true;
            this.c30.Visible = false;
            // 
            // c26
            // 
            this.c26.AutoSize = true;
            this.c26.Location = new System.Drawing.Point(149, 196);
            this.c26.Name = "c26";
            this.c26.Size = new System.Drawing.Size(15, 14);
            this.c26.TabIndex = 53;
            this.c26.UseVisualStyleBackColor = true;
            // 
            // c21
            // 
            this.c21.AutoSize = true;
            this.c21.Location = new System.Drawing.Point(206, 160);
            this.c21.Name = "c21";
            this.c21.Size = new System.Drawing.Size(15, 14);
            this.c21.TabIndex = 52;
            this.c21.UseVisualStyleBackColor = true;
            // 
            // c22
            // 
            this.c22.AutoSize = true;
            this.c22.Location = new System.Drawing.Point(256, 160);
            this.c22.Name = "c22";
            this.c22.Size = new System.Drawing.Size(15, 14);
            this.c22.TabIndex = 51;
            this.c22.UseVisualStyleBackColor = true;
            // 
            // c23
            // 
            this.c23.AutoSize = true;
            this.c23.Location = new System.Drawing.Point(311, 160);
            this.c23.Name = "c23";
            this.c23.Size = new System.Drawing.Size(15, 14);
            this.c23.TabIndex = 50;
            this.c23.UseVisualStyleBackColor = true;
            // 
            // c24
            // 
            this.c24.AutoSize = true;
            this.c24.Location = new System.Drawing.Point(341, 160);
            this.c24.Name = "c24";
            this.c24.Size = new System.Drawing.Size(15, 14);
            this.c24.TabIndex = 49;
            this.c24.UseVisualStyleBackColor = true;
            this.c24.Visible = false;
            // 
            // c20
            // 
            this.c20.AutoSize = true;
            this.c20.Location = new System.Drawing.Point(149, 160);
            this.c20.Name = "c20";
            this.c20.Size = new System.Drawing.Size(15, 14);
            this.c20.TabIndex = 48;
            this.c20.UseVisualStyleBackColor = true;
            // 
            // c15
            // 
            this.c15.AutoSize = true;
            this.c15.Location = new System.Drawing.Point(206, 125);
            this.c15.Name = "c15";
            this.c15.Size = new System.Drawing.Size(15, 14);
            this.c15.TabIndex = 47;
            this.c15.UseVisualStyleBackColor = true;
            // 
            // c16
            // 
            this.c16.AutoSize = true;
            this.c16.Location = new System.Drawing.Point(256, 125);
            this.c16.Name = "c16";
            this.c16.Size = new System.Drawing.Size(15, 14);
            this.c16.TabIndex = 46;
            this.c16.UseVisualStyleBackColor = true;
            // 
            // c17
            // 
            this.c17.AutoSize = true;
            this.c17.Location = new System.Drawing.Point(311, 125);
            this.c17.Name = "c17";
            this.c17.Size = new System.Drawing.Size(15, 14);
            this.c17.TabIndex = 45;
            this.c17.UseVisualStyleBackColor = true;
            // 
            // c18
            // 
            this.c18.AutoSize = true;
            this.c18.Location = new System.Drawing.Point(341, 125);
            this.c18.Name = "c18";
            this.c18.Size = new System.Drawing.Size(15, 14);
            this.c18.TabIndex = 44;
            this.c18.UseVisualStyleBackColor = true;
            this.c18.Visible = false;
            // 
            // c14
            // 
            this.c14.AutoSize = true;
            this.c14.Location = new System.Drawing.Point(149, 125);
            this.c14.Name = "c14";
            this.c14.Size = new System.Drawing.Size(15, 14);
            this.c14.TabIndex = 43;
            this.c14.UseVisualStyleBackColor = true;
            // 
            // c9
            // 
            this.c9.AutoSize = true;
            this.c9.Location = new System.Drawing.Point(206, 89);
            this.c9.Name = "c9";
            this.c9.Size = new System.Drawing.Size(15, 14);
            this.c9.TabIndex = 42;
            this.c9.UseVisualStyleBackColor = true;
            // 
            // c10
            // 
            this.c10.AutoSize = true;
            this.c10.Location = new System.Drawing.Point(256, 89);
            this.c10.Name = "c10";
            this.c10.Size = new System.Drawing.Size(15, 14);
            this.c10.TabIndex = 41;
            this.c10.UseVisualStyleBackColor = true;
            // 
            // c11
            // 
            this.c11.AutoSize = true;
            this.c11.Location = new System.Drawing.Point(311, 89);
            this.c11.Name = "c11";
            this.c11.Size = new System.Drawing.Size(15, 14);
            this.c11.TabIndex = 40;
            this.c11.UseVisualStyleBackColor = true;
            // 
            // c12
            // 
            this.c12.AutoSize = true;
            this.c12.Location = new System.Drawing.Point(341, 89);
            this.c12.Name = "c12";
            this.c12.Size = new System.Drawing.Size(15, 14);
            this.c12.TabIndex = 39;
            this.c12.UseVisualStyleBackColor = true;
            this.c12.Visible = false;
            // 
            // c8
            // 
            this.c8.AutoSize = true;
            this.c8.Location = new System.Drawing.Point(149, 89);
            this.c8.Name = "c8";
            this.c8.Size = new System.Drawing.Size(15, 14);
            this.c8.TabIndex = 38;
            this.c8.UseVisualStyleBackColor = true;
            // 
            // c3
            // 
            this.c3.AutoSize = true;
            this.c3.Location = new System.Drawing.Point(205, 53);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(15, 14);
            this.c3.TabIndex = 22;
            this.c3.UseVisualStyleBackColor = true;
            // 
            // c4
            // 
            this.c4.AutoSize = true;
            this.c4.Location = new System.Drawing.Point(255, 53);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(15, 14);
            this.c4.TabIndex = 21;
            this.c4.UseVisualStyleBackColor = true;
            // 
            // c5
            // 
            this.c5.AutoSize = true;
            this.c5.Location = new System.Drawing.Point(310, 53);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(15, 14);
            this.c5.TabIndex = 20;
            this.c5.UseVisualStyleBackColor = true;
            // 
            // c6
            // 
            this.c6.AutoSize = true;
            this.c6.Location = new System.Drawing.Point(340, 53);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(15, 14);
            this.c6.TabIndex = 19;
            this.c6.UseVisualStyleBackColor = true;
            this.c6.Visible = false;
            // 
            // c2
            // 
            this.c2.AutoSize = true;
            this.c2.Location = new System.Drawing.Point(148, 53);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(15, 14);
            this.c2.TabIndex = 18;
            this.c2.UseVisualStyleBackColor = true;
            // 
            // te7
            // 
            this.te7.Location = new System.Drawing.Point(71, 87);
            this.te7.Name = "te7";
            this.te7.ReadOnly = true;
            this.te7.Size = new System.Drawing.Size(56, 21);
            this.te7.TabIndex = 17;
            // 
            // te13
            // 
            this.te13.Location = new System.Drawing.Point(71, 123);
            this.te13.Name = "te13";
            this.te13.ReadOnly = true;
            this.te13.Size = new System.Drawing.Size(56, 21);
            this.te13.TabIndex = 16;
            // 
            // te19
            // 
            this.te19.Location = new System.Drawing.Point(71, 157);
            this.te19.Name = "te19";
            this.te19.ReadOnly = true;
            this.te19.Size = new System.Drawing.Size(56, 21);
            this.te19.TabIndex = 15;
            // 
            // te25
            // 
            this.te25.Location = new System.Drawing.Point(71, 192);
            this.te25.Name = "te25";
            this.te25.ReadOnly = true;
            this.te25.Size = new System.Drawing.Size(56, 21);
            this.te25.TabIndex = 14;
            // 
            // te1
            // 
            this.te1.Location = new System.Drawing.Point(71, 50);
            this.te1.Name = "te1";
            this.te1.ReadOnly = true;
            this.te1.Size = new System.Drawing.Size(56, 21);
            this.te1.TabIndex = 12;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(31, 196);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(11, 12);
            this.label40.TabIndex = 11;
            this.label40.Text = "5";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(83, 17);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(29, 12);
            this.label39.TabIndex = 10;
            this.label39.Text = "城市";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(141, 17);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 12);
            this.label31.TabIndex = 9;
            this.label31.Text = "制造";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(197, 17);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 8;
            this.label30.Text = "装配";
            this.label30.Click += new System.EventHandler(this.label30_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(247, 17);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 7;
            this.label29.Text = "存储";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(302, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 12);
            this.label26.TabIndex = 6;
            this.label26.Text = "设计";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(393, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 12);
            this.label25.TabIndex = 5;
            this.label25.Text = "其他";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(31, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 12);
            this.label24.TabIndex = 4;
            this.label24.Text = "1";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(31, 91);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 12);
            this.label23.TabIndex = 3;
            this.label23.Text = "2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(31, 130);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 12);
            this.label22.TabIndex = 2;
            this.label22.Text = "3";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(31, 162);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 12);
            this.label21.TabIndex = 1;
            this.label21.Text = "4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(31, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "序号";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Transparent;
            this.tabPage4.Controls.Add(this.b4);
            this.tabPage4.Controls.Add(this.panel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(799, 414);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "主要产品";
            // 
            // b4
            // 
            this.b4.Location = new System.Drawing.Point(359, 365);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(75, 23);
            this.b4.TabIndex = 7;
            this.b4.Text = "查看下一项";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.tex4);
            this.panel4.Controls.Add(this.tex6);
            this.panel4.Controls.Add(this.tex8);
            this.panel4.Controls.Add(this.tex10);
            this.panel4.Controls.Add(this.tex2);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.tex3);
            this.panel4.Controls.Add(this.tex5);
            this.panel4.Controls.Add(this.tex7);
            this.panel4.Controls.Add(this.tex9);
            this.panel4.Controls.Add(this.tex1);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.label48);
            this.panel4.Location = new System.Drawing.Point(19, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(760, 254);
            this.panel4.TabIndex = 0;
            // 
            // tex4
            // 
            this.tex4.Location = new System.Drawing.Point(235, 98);
            this.tex4.Name = "tex4";
            this.tex4.ReadOnly = true;
            this.tex4.Size = new System.Drawing.Size(56, 21);
            this.tex4.TabIndex = 72;
            // 
            // tex6
            // 
            this.tex6.Location = new System.Drawing.Point(235, 134);
            this.tex6.Name = "tex6";
            this.tex6.ReadOnly = true;
            this.tex6.Size = new System.Drawing.Size(56, 21);
            this.tex6.TabIndex = 71;
            // 
            // tex8
            // 
            this.tex8.Location = new System.Drawing.Point(235, 168);
            this.tex8.Name = "tex8";
            this.tex8.ReadOnly = true;
            this.tex8.Size = new System.Drawing.Size(56, 21);
            this.tex8.TabIndex = 70;
            // 
            // tex10
            // 
            this.tex10.Location = new System.Drawing.Point(235, 203);
            this.tex10.Name = "tex10";
            this.tex10.ReadOnly = true;
            this.tex10.Size = new System.Drawing.Size(56, 21);
            this.tex10.TabIndex = 69;
            // 
            // tex2
            // 
            this.tex2.Location = new System.Drawing.Point(235, 61);
            this.tex2.Name = "tex2";
            this.tex2.ReadOnly = true;
            this.tex2.Size = new System.Drawing.Size(56, 21);
            this.tex2.TabIndex = 68;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(207, 28);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(119, 12);
            this.label43.TabIndex = 67;
            this.label43.Text = "占总收入的比例（%）";
            // 
            // tex3
            // 
            this.tex3.Location = new System.Drawing.Point(128, 98);
            this.tex3.Name = "tex3";
            this.tex3.ReadOnly = true;
            this.tex3.Size = new System.Drawing.Size(56, 21);
            this.tex3.TabIndex = 66;
            // 
            // tex5
            // 
            this.tex5.Location = new System.Drawing.Point(128, 134);
            this.tex5.Name = "tex5";
            this.tex5.ReadOnly = true;
            this.tex5.Size = new System.Drawing.Size(56, 21);
            this.tex5.TabIndex = 65;
            // 
            // tex7
            // 
            this.tex7.Location = new System.Drawing.Point(128, 168);
            this.tex7.Name = "tex7";
            this.tex7.ReadOnly = true;
            this.tex7.Size = new System.Drawing.Size(56, 21);
            this.tex7.TabIndex = 64;
            // 
            // tex9
            // 
            this.tex9.Location = new System.Drawing.Point(128, 203);
            this.tex9.Name = "tex9";
            this.tex9.ReadOnly = true;
            this.tex9.Size = new System.Drawing.Size(56, 21);
            this.tex9.TabIndex = 63;
            // 
            // tex1
            // 
            this.tex1.Location = new System.Drawing.Point(128, 61);
            this.tex1.Name = "tex1";
            this.tex1.ReadOnly = true;
            this.tex1.Size = new System.Drawing.Size(56, 21);
            this.tex1.TabIndex = 62;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(66, 207);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(11, 12);
            this.label41.TabIndex = 61;
            this.label41.Text = "5";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(140, 28);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 60;
            this.label42.Text = "产品";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(66, 64);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(11, 12);
            this.label44.TabIndex = 58;
            this.label44.Text = "1";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(66, 102);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(11, 12);
            this.label45.TabIndex = 57;
            this.label45.Text = "2";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(66, 141);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(11, 12);
            this.label46.TabIndex = 56;
            this.label46.Text = "3";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(66, 173);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(11, 12);
            this.label47.TabIndex = 55;
            this.label47.Text = "4";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(66, 28);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 54;
            this.label48.Text = "序号";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Transparent;
            this.tabPage5.Controls.Add(this.b5);
            this.tabPage5.Controls.Add(this.panel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(799, 414);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "客户主要数据";
            // 
            // b5
            // 
            this.b5.Location = new System.Drawing.Point(359, 365);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(75, 23);
            this.b5.TabIndex = 7;
            this.b5.Text = "查看下一项";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.textB10);
            this.panel5.Controls.Add(this.textB15);
            this.panel5.Controls.Add(this.textB20);
            this.panel5.Controls.Add(this.textB25);
            this.panel5.Controls.Add(this.textB5);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.textB9);
            this.panel5.Controls.Add(this.textB14);
            this.panel5.Controls.Add(this.textB19);
            this.panel5.Controls.Add(this.textB24);
            this.panel5.Controls.Add(this.textB4);
            this.panel5.Controls.Add(this.label58);
            this.panel5.Controls.Add(this.textB8);
            this.panel5.Controls.Add(this.textB13);
            this.panel5.Controls.Add(this.textB18);
            this.panel5.Controls.Add(this.textB23);
            this.panel5.Controls.Add(this.textB3);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.textB7);
            this.panel5.Controls.Add(this.textB12);
            this.panel5.Controls.Add(this.textB17);
            this.panel5.Controls.Add(this.textB22);
            this.panel5.Controls.Add(this.textB2);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.textB6);
            this.panel5.Controls.Add(this.textB11);
            this.panel5.Controls.Add(this.textB16);
            this.panel5.Controls.Add(this.textB21);
            this.panel5.Controls.Add(this.textB1);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.label52);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.label54);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Controls.Add(this.label56);
            this.panel5.Location = new System.Drawing.Point(20, 17);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 332);
            this.panel5.TabIndex = 0;
            // 
            // textB10
            // 
            this.textB10.Location = new System.Drawing.Point(454, 98);
            this.textB10.Name = "textB10";
            this.textB10.ReadOnly = true;
            this.textB10.Size = new System.Drawing.Size(125, 21);
            this.textB10.TabIndex = 108;
            // 
            // textB15
            // 
            this.textB15.Location = new System.Drawing.Point(454, 134);
            this.textB15.Name = "textB15";
            this.textB15.ReadOnly = true;
            this.textB15.Size = new System.Drawing.Size(125, 21);
            this.textB15.TabIndex = 107;
            // 
            // textB20
            // 
            this.textB20.Location = new System.Drawing.Point(454, 168);
            this.textB20.Name = "textB20";
            this.textB20.ReadOnly = true;
            this.textB20.Size = new System.Drawing.Size(125, 21);
            this.textB20.TabIndex = 106;
            // 
            // textB25
            // 
            this.textB25.Location = new System.Drawing.Point(454, 203);
            this.textB25.Name = "textB25";
            this.textB25.ReadOnly = true;
            this.textB25.Size = new System.Drawing.Size(125, 21);
            this.textB25.TabIndex = 105;
            // 
            // textB5
            // 
            this.textB5.Location = new System.Drawing.Point(454, 61);
            this.textB5.Name = "textB5";
            this.textB5.ReadOnly = true;
            this.textB5.Size = new System.Drawing.Size(125, 21);
            this.textB5.TabIndex = 104;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(452, 30);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(137, 12);
            this.label59.TabIndex = 103;
            this.label59.Text = "市场占有率（客户份额）";
            // 
            // textB9
            // 
            this.textB9.Location = new System.Drawing.Point(366, 99);
            this.textB9.Name = "textB9";
            this.textB9.ReadOnly = true;
            this.textB9.Size = new System.Drawing.Size(56, 21);
            this.textB9.TabIndex = 102;
            // 
            // textB14
            // 
            this.textB14.Location = new System.Drawing.Point(366, 135);
            this.textB14.Name = "textB14";
            this.textB14.ReadOnly = true;
            this.textB14.Size = new System.Drawing.Size(56, 21);
            this.textB14.TabIndex = 101;
            // 
            // textB19
            // 
            this.textB19.Location = new System.Drawing.Point(366, 169);
            this.textB19.Name = "textB19";
            this.textB19.ReadOnly = true;
            this.textB19.Size = new System.Drawing.Size(56, 21);
            this.textB19.TabIndex = 100;
            // 
            // textB24
            // 
            this.textB24.Location = new System.Drawing.Point(366, 204);
            this.textB24.Name = "textB24";
            this.textB24.ReadOnly = true;
            this.textB24.Size = new System.Drawing.Size(56, 21);
            this.textB24.TabIndex = 99;
            // 
            // textB4
            // 
            this.textB4.Location = new System.Drawing.Point(366, 62);
            this.textB4.Name = "textB4";
            this.textB4.ReadOnly = true;
            this.textB4.Size = new System.Drawing.Size(56, 21);
            this.textB4.TabIndex = 98;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(350, 30);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(83, 12);
            this.label58.TabIndex = 97;
            this.label58.Text = "占总收入比例%";
            // 
            // textB8
            // 
            this.textB8.Location = new System.Drawing.Point(276, 98);
            this.textB8.Name = "textB8";
            this.textB8.ReadOnly = true;
            this.textB8.Size = new System.Drawing.Size(56, 21);
            this.textB8.TabIndex = 96;
            // 
            // textB13
            // 
            this.textB13.Location = new System.Drawing.Point(276, 134);
            this.textB13.Name = "textB13";
            this.textB13.ReadOnly = true;
            this.textB13.Size = new System.Drawing.Size(56, 21);
            this.textB13.TabIndex = 95;
            // 
            // textB18
            // 
            this.textB18.Location = new System.Drawing.Point(276, 168);
            this.textB18.Name = "textB18";
            this.textB18.ReadOnly = true;
            this.textB18.Size = new System.Drawing.Size(56, 21);
            this.textB18.TabIndex = 94;
            // 
            // textB23
            // 
            this.textB23.Location = new System.Drawing.Point(276, 203);
            this.textB23.Name = "textB23";
            this.textB23.ReadOnly = true;
            this.textB23.Size = new System.Drawing.Size(56, 21);
            this.textB23.TabIndex = 93;
            // 
            // textB3
            // 
            this.textB3.Location = new System.Drawing.Point(276, 61);
            this.textB3.Name = "textB3";
            this.textB3.ReadOnly = true;
            this.textB3.Size = new System.Drawing.Size(56, 21);
            this.textB3.TabIndex = 92;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(277, 29);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 12);
            this.label57.TabIndex = 91;
            this.label57.Text = "主要产品";
            // 
            // textB7
            // 
            this.textB7.Location = new System.Drawing.Point(189, 98);
            this.textB7.Name = "textB7";
            this.textB7.ReadOnly = true;
            this.textB7.Size = new System.Drawing.Size(56, 21);
            this.textB7.TabIndex = 90;
            // 
            // textB12
            // 
            this.textB12.Location = new System.Drawing.Point(189, 134);
            this.textB12.Name = "textB12";
            this.textB12.ReadOnly = true;
            this.textB12.Size = new System.Drawing.Size(56, 21);
            this.textB12.TabIndex = 89;
            // 
            // textB17
            // 
            this.textB17.Location = new System.Drawing.Point(189, 168);
            this.textB17.Name = "textB17";
            this.textB17.ReadOnly = true;
            this.textB17.Size = new System.Drawing.Size(56, 21);
            this.textB17.TabIndex = 88;
            // 
            // textB22
            // 
            this.textB22.Location = new System.Drawing.Point(189, 203);
            this.textB22.Name = "textB22";
            this.textB22.ReadOnly = true;
            this.textB22.Size = new System.Drawing.Size(56, 21);
            this.textB22.TabIndex = 87;
            // 
            // textB2
            // 
            this.textB2.Location = new System.Drawing.Point(189, 61);
            this.textB2.Name = "textB2";
            this.textB2.ReadOnly = true;
            this.textB2.Size = new System.Drawing.Size(56, 21);
            this.textB2.TabIndex = 86;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(201, 28);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(29, 12);
            this.label49.TabIndex = 85;
            this.label49.Text = "城市";
            // 
            // textB6
            // 
            this.textB6.Location = new System.Drawing.Point(108, 98);
            this.textB6.Name = "textB6";
            this.textB6.ReadOnly = true;
            this.textB6.Size = new System.Drawing.Size(56, 21);
            this.textB6.TabIndex = 84;
            // 
            // textB11
            // 
            this.textB11.Location = new System.Drawing.Point(108, 134);
            this.textB11.Name = "textB11";
            this.textB11.ReadOnly = true;
            this.textB11.Size = new System.Drawing.Size(56, 21);
            this.textB11.TabIndex = 83;
            // 
            // textB16
            // 
            this.textB16.Location = new System.Drawing.Point(108, 168);
            this.textB16.Name = "textB16";
            this.textB16.ReadOnly = true;
            this.textB16.Size = new System.Drawing.Size(56, 21);
            this.textB16.TabIndex = 82;
            // 
            // textB21
            // 
            this.textB21.Location = new System.Drawing.Point(108, 203);
            this.textB21.Name = "textB21";
            this.textB21.ReadOnly = true;
            this.textB21.Size = new System.Drawing.Size(56, 21);
            this.textB21.TabIndex = 81;
            // 
            // textB1
            // 
            this.textB1.Location = new System.Drawing.Point(108, 61);
            this.textB1.Name = "textB1";
            this.textB1.ReadOnly = true;
            this.textB1.Size = new System.Drawing.Size(56, 21);
            this.textB1.TabIndex = 80;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(46, 207);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(11, 12);
            this.label50.TabIndex = 79;
            this.label50.Text = "5";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(112, 28);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(53, 12);
            this.label51.TabIndex = 78;
            this.label51.Text = "客户名称";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(46, 64);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(11, 12);
            this.label52.TabIndex = 77;
            this.label52.Text = "1";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(46, 102);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(11, 12);
            this.label53.TabIndex = 76;
            this.label53.Text = "2";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(46, 141);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(11, 12);
            this.label54.TabIndex = 75;
            this.label54.Text = "3";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(46, 173);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(11, 12);
            this.label55.TabIndex = 74;
            this.label55.Text = "4";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(46, 28);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(29, 12);
            this.label56.TabIndex = 73;
            this.label56.Text = "序号";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Transparent;
            this.tabPage6.Controls.Add(this.b6);
            this.tabPage6.Controls.Add(this.panel6);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(799, 414);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "公司研发能力";
            // 
            // b6
            // 
            this.b6.Location = new System.Drawing.Point(359, 365);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(75, 23);
            this.b6.TabIndex = 7;
            this.b6.Text = "查看下一项";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.研发条件);
            this.panel6.Controls.Add(this.lk_label_studyAble);
            this.panel6.Controls.Add(this.text8);
            this.panel6.Controls.Add(this.text7);
            this.panel6.Controls.Add(this.label65);
            this.panel6.Controls.Add(this.text6);
            this.panel6.Controls.Add(this.text5);
            this.panel6.Controls.Add(this.text4);
            this.panel6.Controls.Add(this.text3);
            this.panel6.Controls.Add(this.text2);
            this.panel6.Controls.Add(this.text1);
            this.panel6.Controls.Add(this.label60);
            this.panel6.Controls.Add(this.label61);
            this.panel6.Controls.Add(this.label62);
            this.panel6.Controls.Add(this.label63);
            this.panel6.Controls.Add(this.label64);
            this.panel6.Location = new System.Drawing.Point(20, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(767, 271);
            this.panel6.TabIndex = 0;
            // 
            // 研发条件
            // 
            this.研发条件.AutoSize = true;
            this.研发条件.Location = new System.Drawing.Point(64, 236);
            this.研发条件.Name = "研发条件";
            this.研发条件.Size = new System.Drawing.Size(53, 12);
            this.研发条件.TabIndex = 68;
            this.研发条件.Text = "研发条件";
            // 
            // lk_label_studyAble
            // 
            this.lk_label_studyAble.AutoSize = true;
            this.lk_label_studyAble.Location = new System.Drawing.Point(153, 236);
            this.lk_label_studyAble.Name = "lk_label_studyAble";
            this.lk_label_studyAble.Size = new System.Drawing.Size(65, 12);
            this.lk_label_studyAble.TabIndex = 67;
            this.lk_label_studyAble.TabStop = true;
            this.lk_label_studyAble.Text = "linkLabel1";
            this.lk_label_studyAble.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lk_label_studyAble_LinkClicked);
            // 
            // text8
            // 
            this.text8.Location = new System.Drawing.Point(240, 192);
            this.text8.Name = "text8";
            this.text8.ReadOnly = true;
            this.text8.Size = new System.Drawing.Size(58, 21);
            this.text8.TabIndex = 64;
            // 
            // text7
            // 
            this.text7.Location = new System.Drawing.Point(155, 192);
            this.text7.Name = "text7";
            this.text7.ReadOnly = true;
            this.text7.Size = new System.Drawing.Size(58, 21);
            this.text7.TabIndex = 63;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(58, 197);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(53, 12);
            this.label65.TabIndex = 62;
            this.label65.Text = "工具车间";
            // 
            // text6
            // 
            this.text6.Location = new System.Drawing.Point(240, 153);
            this.text6.Name = "text6";
            this.text6.ReadOnly = true;
            this.text6.Size = new System.Drawing.Size(58, 21);
            this.text6.TabIndex = 61;
            // 
            // text5
            // 
            this.text5.Location = new System.Drawing.Point(155, 153);
            this.text5.Name = "text5";
            this.text5.ReadOnly = true;
            this.text5.Size = new System.Drawing.Size(58, 21);
            this.text5.TabIndex = 60;
            // 
            // text4
            // 
            this.text4.Location = new System.Drawing.Point(239, 115);
            this.text4.Name = "text4";
            this.text4.ReadOnly = true;
            this.text4.Size = new System.Drawing.Size(58, 21);
            this.text4.TabIndex = 59;
            // 
            // text3
            // 
            this.text3.Location = new System.Drawing.Point(154, 115);
            this.text3.Name = "text3";
            this.text3.ReadOnly = true;
            this.text3.Size = new System.Drawing.Size(58, 21);
            this.text3.TabIndex = 58;
            // 
            // text2
            // 
            this.text2.Location = new System.Drawing.Point(239, 73);
            this.text2.Name = "text2";
            this.text2.ReadOnly = true;
            this.text2.Size = new System.Drawing.Size(58, 21);
            this.text2.TabIndex = 57;
            // 
            // text1
            // 
            this.text1.Location = new System.Drawing.Point(154, 73);
            this.text1.Name = "text1";
            this.text1.ReadOnly = true;
            this.text1.Size = new System.Drawing.Size(58, 21);
            this.text1.TabIndex = 56;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(58, 78);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(77, 12);
            this.label60.TabIndex = 55;
            this.label60.Text = "室内测试设备";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(58, 119);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(65, 12);
            this.label61.TabIndex = 54;
            this.label61.Text = "研发实验室";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(58, 158);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(41, 12);
            this.label62.TabIndex = 53;
            this.label62.Text = "工程部";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(252, 48);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(29, 12);
            this.label63.TabIndex = 52;
            this.label63.Text = "人数";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(162, 48);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(35, 12);
            this.label64.TabIndex = 51;
            this.label64.Text = "是/否";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.Transparent;
            this.tabPage7.Controls.Add(this.b7);
            this.tabPage7.Controls.Add(this.panel7);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(799, 414);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "公司已有证书";
            // 
            // b7
            // 
            this.b7.Location = new System.Drawing.Point(359, 365);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(75, 23);
            this.b7.TabIndex = 7;
            this.b7.Text = "查看下一项";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.label34);
            this.panel7.Controls.Add(this.label33);
            this.panel7.Controls.Add(this.textBo3);
            this.panel7.Controls.Add(this.textBo2);
            this.panel7.Controls.Add(this.textBo1);
            this.panel7.Controls.Add(this.ch4);
            this.panel7.Controls.Add(this.ch3);
            this.panel7.Controls.Add(this.ch2);
            this.panel7.Controls.Add(this.ch1);
            this.panel7.Controls.Add(this.label66);
            this.panel7.Location = new System.Drawing.Point(12, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(765, 356);
            this.panel7.TabIndex = 0;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(76, 220);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(371, 12);
            this.label35.TabIndex = 60;
            this.label35.Text = "如果您计划达到上述证书中的任何一项，请指定证书名称和计划日期:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(74, 293);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 59;
            this.label34.Text = "计划日期";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(76, 251);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 58;
            this.label33.Text = "证书名称";
            // 
            // textBo3
            // 
            this.textBo3.Location = new System.Drawing.Point(139, 290);
            this.textBo3.Name = "textBo3";
            this.textBo3.ReadOnly = true;
            this.textBo3.Size = new System.Drawing.Size(174, 21);
            this.textBo3.TabIndex = 57;
            // 
            // textBo2
            // 
            this.textBo2.Location = new System.Drawing.Point(139, 247);
            this.textBo2.Name = "textBo2";
            this.textBo2.ReadOnly = true;
            this.textBo2.Size = new System.Drawing.Size(174, 21);
            this.textBo2.TabIndex = 56;
            // 
            // textBo1
            // 
            this.textBo1.Location = new System.Drawing.Point(178, 178);
            this.textBo1.Name = "textBo1";
            this.textBo1.ReadOnly = true;
            this.textBo1.Size = new System.Drawing.Size(174, 21);
            this.textBo1.TabIndex = 55;
            // 
            // ch4
            // 
            this.ch4.AutoSize = true;
            this.ch4.Location = new System.Drawing.Point(76, 180);
            this.ch4.Name = "ch4";
            this.ch4.Size = new System.Drawing.Size(102, 16);
            this.ch4.TabIndex = 54;
            this.ch4.Text = "其他 证书名称";
            this.ch4.UseVisualStyleBackColor = true;
            // 
            // ch3
            // 
            this.ch3.AutoSize = true;
            this.ch3.Location = new System.Drawing.Point(76, 145);
            this.ch3.Name = "ch3";
            this.ch3.Size = new System.Drawing.Size(102, 16);
            this.ch3.TabIndex = 53;
            this.ch3.Text = "ISO14001/EMAS";
            this.ch3.UseVisualStyleBackColor = true;
            // 
            // ch2
            // 
            this.ch2.AutoSize = true;
            this.ch2.Location = new System.Drawing.Point(76, 109);
            this.ch2.Name = "ch2";
            this.ch2.Size = new System.Drawing.Size(108, 16);
            this.ch2.TabIndex = 52;
            this.ch2.Text = "ISO 9001：2000";
            this.ch2.UseVisualStyleBackColor = true;
            // 
            // ch1
            // 
            this.ch1.AutoSize = true;
            this.ch1.Location = new System.Drawing.Point(75, 73);
            this.ch1.Name = "ch1";
            this.ch1.Size = new System.Drawing.Size(96, 16);
            this.ch1.TabIndex = 51;
            this.ch1.Text = "ISO/TS 16949";
            this.ch1.UseVisualStyleBackColor = true;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(67, 36);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(113, 12);
            this.label66.TabIndex = 50;
            this.label66.Text = "贵公司有以下证书吗";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox2);
            this.tabPage8.Controls.Add(this.groupBox1);
            this.tabPage8.Controls.Add(this.button12);
            this.tabPage8.Controls.Add(this.button11);
            this.tabPage8.Controls.Add(this.button3);
            this.tabPage8.Controls.Add(this.button9);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(799, 414);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "采购策略及市场分析文件下载";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Lk_Label_MarketAnalysis);
            this.groupBox2.Location = new System.Drawing.Point(89, 213);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(505, 147);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "供应市场分析";
            // 
            // Lk_Label_MarketAnalysis
            // 
            this.Lk_Label_MarketAnalysis.AutoSize = true;
            this.Lk_Label_MarketAnalysis.Location = new System.Drawing.Point(15, 17);
            this.Lk_Label_MarketAnalysis.Name = "Lk_Label_MarketAnalysis";
            this.Lk_Label_MarketAnalysis.Size = new System.Drawing.Size(65, 12);
            this.Lk_Label_MarketAnalysis.TabIndex = 9;
            this.Lk_Label_MarketAnalysis.TabStop = true;
            this.Lk_Label_MarketAnalysis.Text = "linkLabel1";
            this.Lk_Label_MarketAnalysis.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Lk_Label_MarketAnalysis_LinkClicked_1);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.Lk_Label_ProcStrategy);
            this.groupBox1.Location = new System.Drawing.Point(89, 20);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(505, 158);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "采购策略";
            // 
            // Lk_Label_ProcStrategy
            // 
            this.Lk_Label_ProcStrategy.AutoSize = true;
            this.Lk_Label_ProcStrategy.Location = new System.Drawing.Point(15, 17);
            this.Lk_Label_ProcStrategy.Name = "Lk_Label_ProcStrategy";
            this.Lk_Label_ProcStrategy.Size = new System.Drawing.Size(65, 12);
            this.Lk_Label_ProcStrategy.TabIndex = 9;
            this.Lk_Label_ProcStrategy.TabStop = true;
            this.Lk_Label_ProcStrategy.Text = "linkLabel1";
            this.Lk_Label_ProcStrategy.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Lk_Label_ProcStrategy_LinkClicked);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(561, 366);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 8;
            this.button12.Text = "返  回";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(414, 366);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 6;
            this.button11.Text = "拒绝";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(280, 366);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "重置";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(135, 366);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "下一步评估";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // detailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 508);
            this.Controls.Add(this.tabControl1);
            this.Name = "detailForm";
            this.Text = "供应商基本信息";
            this.Load += new System.EventHandler(this.detailForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox style_textBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox struture_textBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox thing_textBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox org_textBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox fax_textBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox website_textBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox phonenumber_textBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox email_textBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox position_textBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox contactname_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox postcode_textBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox address_textBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox city_textBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox province_textBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox country_textBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dbscode_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox suppliername_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox t12;
        private System.Windows.Forms.TextBox t13;
        private System.Windows.Forms.TextBox t14;
        private System.Windows.Forms.TextBox t15;
        private System.Windows.Forms.TextBox t11;
        private System.Windows.Forms.TextBox t7;
        private System.Windows.Forms.TextBox t8;
        private System.Windows.Forms.TextBox t9;
        private System.Windows.Forms.TextBox t10;
        private System.Windows.Forms.TextBox t6;
        private System.Windows.Forms.TextBox t2;
        private System.Windows.Forms.TextBox t3;
        private System.Windows.Forms.TextBox t4;
        private System.Windows.Forms.TextBox t5;
        private System.Windows.Forms.TextBox t1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label2017;
        private System.Windows.Forms.Label label2014;
        private System.Windows.Forms.Label label2015;
        private System.Windows.Forms.Label label2016;
        private System.Windows.Forms.Label label2013;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.CheckBox c3;
        private System.Windows.Forms.CheckBox c4;
        private System.Windows.Forms.CheckBox c5;
        private System.Windows.Forms.CheckBox c6;
        private System.Windows.Forms.CheckBox c2;
        private System.Windows.Forms.TextBox te7;
        private System.Windows.Forms.TextBox te13;
        private System.Windows.Forms.TextBox te19;
        private System.Windows.Forms.TextBox te25;
        private System.Windows.Forms.TextBox te1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox c27;
        private System.Windows.Forms.CheckBox c28;
        private System.Windows.Forms.CheckBox c29;
        private System.Windows.Forms.CheckBox c30;
        private System.Windows.Forms.CheckBox c26;
        private System.Windows.Forms.CheckBox c21;
        private System.Windows.Forms.CheckBox c22;
        private System.Windows.Forms.CheckBox c23;
        private System.Windows.Forms.CheckBox c24;
        private System.Windows.Forms.CheckBox c20;
        private System.Windows.Forms.CheckBox c15;
        private System.Windows.Forms.CheckBox c16;
        private System.Windows.Forms.CheckBox c17;
        private System.Windows.Forms.CheckBox c18;
        private System.Windows.Forms.CheckBox c14;
        private System.Windows.Forms.CheckBox c9;
        private System.Windows.Forms.CheckBox c10;
        private System.Windows.Forms.CheckBox c11;
        private System.Windows.Forms.CheckBox c12;
        private System.Windows.Forms.CheckBox c8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tex4;
        private System.Windows.Forms.TextBox tex6;
        private System.Windows.Forms.TextBox tex8;
        private System.Windows.Forms.TextBox tex10;
        private System.Windows.Forms.TextBox tex2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tex3;
        private System.Windows.Forms.TextBox tex5;
        private System.Windows.Forms.TextBox tex7;
        private System.Windows.Forms.TextBox tex9;
        private System.Windows.Forms.TextBox tex1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textB10;
        private System.Windows.Forms.TextBox textB15;
        private System.Windows.Forms.TextBox textB20;
        private System.Windows.Forms.TextBox textB25;
        private System.Windows.Forms.TextBox textB5;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textB9;
        private System.Windows.Forms.TextBox textB14;
        private System.Windows.Forms.TextBox textB19;
        private System.Windows.Forms.TextBox textB24;
        private System.Windows.Forms.TextBox textB4;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textB8;
        private System.Windows.Forms.TextBox textB13;
        private System.Windows.Forms.TextBox textB18;
        private System.Windows.Forms.TextBox textB23;
        private System.Windows.Forms.TextBox textB3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textB7;
        private System.Windows.Forms.TextBox textB12;
        private System.Windows.Forms.TextBox textB17;
        private System.Windows.Forms.TextBox textB22;
        private System.Windows.Forms.TextBox textB2;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textB6;
        private System.Windows.Forms.TextBox textB11;
        private System.Windows.Forms.TextBox textB16;
        private System.Windows.Forms.TextBox textB21;
        private System.Windows.Forms.TextBox textB1;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox text8;
        private System.Windows.Forms.TextBox text7;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox text6;
        private System.Windows.Forms.TextBox text5;
        private System.Windows.Forms.TextBox text4;
        private System.Windows.Forms.TextBox text3;
        private System.Windows.Forms.TextBox text2;
        private System.Windows.Forms.TextBox text1;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBo1;
        private System.Windows.Forms.CheckBox ch4;
        private System.Windows.Forms.CheckBox ch3;
        private System.Windows.Forms.CheckBox ch2;
        private System.Windows.Forms.CheckBox ch1;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox te30;
        private System.Windows.Forms.TextBox te24;
        private System.Windows.Forms.TextBox te18;
        private System.Windows.Forms.TextBox te12;
        private System.Windows.Forms.TextBox te6;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBo3;
        private System.Windows.Forms.TextBox textBo2;
        private System.Windows.Forms.TextBox filename4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.LinkLabel companyFileLink;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.LinkLabel lk_label_studyAble;
        private System.Windows.Forms.Label 研发条件;
        private System.Windows.Forms.LinkLabel Lk_Label_ProcStrategy;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.LinkLabel Lk_Label_MarketAnalysis;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}