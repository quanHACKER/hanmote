﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class refreshBasicInfo : Form
    {
        private string email;
        public refreshBasicInfo(string email)
        {
            InitializeComponent();
            this.email = email;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            string body = this.richTextBox1.Text;
            List<MailAddress> addresses = new List<MailAddress>();
            string subject = "自评结果和供应商基本信息的评估决策";
            if (string.IsNullOrWhiteSpace(this.richTextBox1.Text))
            {
                MessageUtil.ShowWarning("拒绝理由不能为空！");
                return;
            }

            string toAddresses = this.email;

            MailMessage message = new MailMessage();
            message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                            /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                for (int i = 0; i < toAddresses.Count; i++)*/
            message.To.Add(toAddresses);
            message.Subject = subject;
            //message.CC.Add(new MailAddress("test@126.com"));
            //message.Bcc.Add(new MailAddress("test@126.com"));
            message.IsBodyHtml = true;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = body;
            //Attachment 附件
            //Attachment att = new Attachment(@"E:/迅雷下载/20151229214321.xls");
            //message.Attachments.Add(att);//添加附件

            message.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
            client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
            client.EnableSsl = true; //必须经过ssl加密 
            try
            {
                client.Send(message);

                MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

            }
            catch (Exception ee)
            {
                MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

            }



            /* if (EmailUtil.SendEmail(subject, body, addresses))
             {
                 string msg = "";
                 for (int i = 0; i < list.Count; i++)
                     msg += this.list[i].Email + ",";
                 msg = msg.Substring(0, msg.Length - 1);
                 MessageUtil.ShowTips("拒绝邮件已经成功发送到" + msg + "！");
             }*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
