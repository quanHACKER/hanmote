﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using MMClient.progress;
using System.Text.RegularExpressions;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class Fupload : Form
    {
        FTPHelper fTPHelper = new FTPHelper("");
        public Fupload(string purOrgName,string mtName)
        {
            InitializeComponent();
            string sql = "select distinct(Porg_Name) from Porg_MtGroup_Relationship";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
           List<String> list =new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++) {
                list.Add(dt.Rows[i][0].ToString());
            }
         
            pur_Select.DataSource = list;
            this.pur_Select.Text = purOrgName;
            sql = " select MtGroup_Name from  Porg_MtGroup_Relationship where Porg_Name='"+pur_Select.Text+"'";
            dt = DBHelper.ExecuteQueryDT(sql);
            List<String> list2 = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list2.Add(dt.Rows[i][0].ToString());

            }
            mt_Select.DataSource =list2;
            mt_Select.Text = mtName;


        }

        private void button1_Click(object sender, EventArgs e)//采购策略上传
        {
            string strPath = "/general/caigou/" + pur_Select.Text + "/" + mt_Select.Text;
            FTPHelper fTPHelper1 = new FTPHelper( strPath);
            if (fTPHelper1.GetFileList()!=null)
            {
                string[] filenames = fTPHelper1.GetFileList();
           
            string file1 = "";
            if (filenames != null && filenames.Length>0)
            {
                for (int i = 0; i < filenames.Length; i++)
                {
                    fTPHelper1.Delete(filenames[i]);
                }
            }
            }
            //昨晚改的
            string[] resultFile = null;
            string fileName="";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;

            }

            if (resultFile == null)
                MessageBox.Show("未选择文件");
            else
            {
               
                progreBar progreBar = new progreBar(fTPHelper, resultFile, strPath);
                progreBar.Show();
               
                string file = "";
                if (resultFile != null)
                {
                    for (int i = 0; i < resultFile.Length; i++)
                    {
                        string[] sArray5 = Regex.Split(resultFile[i], @"\\", RegexOptions.IgnoreCase);
                        file = file + ";" + sArray5[sArray5.Length - 1];
                        //file = file + ";" + resultFile[i];

                    }
                }
                filename.Text = file;
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)//市场分析文件上传
        {
            string strPath = "/general/gong/" + pur_Select.Text + "/" + mt_Select.Text;
            FTPHelper fTPHelper1 = new FTPHelper(strPath);
            string[] filenames = fTPHelper1.GetFileList();
            string file1 = "";
            if (filenames != null)
            {
                for (int i = 0; i < filenames.Length; i++)
                {
                    fTPHelper1.Delete(filenames[i]);
                    //file1 = file1 + ";" + filenames[i];

                }
            }
            string[] resultFile = null;
            string fileName = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;

            }
            if (resultFile == null)
                MessageBox.Show("未选择文件");
            else
            {
                
                progreBar progreBar = new progreBar(fTPHelper, resultFile, strPath);
                progreBar.Show();

                string file = "";
                if (resultFile != null)
                {
                    for (int i = 0; i < resultFile.Length; i++)
                    {
                        string[] sArray5 = Regex.Split(resultFile[i], @"\\", RegexOptions.IgnoreCase);
                        file = file + ";" + sArray5[sArray5.Length - 1];

                    }
                }
                filename1.Text = file;
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pur_Select_SelectedValueChanged(object sender, EventArgs e)
        {
            string sql = " select MtGroup_Name from  Porg_MtGroup_Relationship where Porg_Name='" + pur_Select.Text + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<String> list2 = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list2.Add(dt.Rows[i][0].ToString());

            }
            mt_Select.DataSource = list2;
        }
    }
}
