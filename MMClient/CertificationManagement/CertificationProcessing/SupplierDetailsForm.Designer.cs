﻿namespace MMClient
{
    partial class SupplierDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.contract_gb = new System.Windows.Forms.GroupBox();
            this.email1_txt = new System.Windows.Forms.TextBox();
            this.email_lbl = new System.Windows.Forms.Label();
            this.phone11_txt = new System.Windows.Forms.TextBox();
            this.phone1_lbl = new System.Windows.Forms.Label();
            this.phone21_txt = new System.Windows.Forms.TextBox();
            this.phone2_lbl = new System.Windows.Forms.Label();
            this.contract1_txt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.zipCode_txt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.doorNum_txt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.city_txt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.country_txt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.isExport_cmb = new System.Windows.Forms.ComboBox();
            this.isListed_cmb = new System.Windows.Forms.ComboBox();
            this.stockCode_txt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.companyRegisterDate_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.companyRepresent_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.companyCode_txt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.companyRegisterCountry_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.companyWebsite_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.companyUsedname_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.companyProperty_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.supplierEnglishName_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.supplierChineseName_txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgv_MoveTypeDetails = new System.Windows.Forms.DataGridView();
            this.Col_MT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MT_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MT_TransactionDtb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.quickRatio_txt = new System.Windows.Forms.TextBox();
            this.years_txt = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.equity_txt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.turnoverRate_txt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.currentRatio_txt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.assetLiability_txt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.registerDate_txt = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.registerFund_txt = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.dataGridView12 = new System.Windows.Forms.DataGridView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.dataGridView13 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.dataGridView14 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contract_gb.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MoveTypeDetails)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).BeginInit();
            this.groupBox19.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(913, 520);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.contract_gb);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(905, 495);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "组织信息";
            // 
            // contract_gb
            // 
            this.contract_gb.Controls.Add(this.email1_txt);
            this.contract_gb.Controls.Add(this.email_lbl);
            this.contract_gb.Controls.Add(this.phone11_txt);
            this.contract_gb.Controls.Add(this.phone1_lbl);
            this.contract_gb.Controls.Add(this.phone21_txt);
            this.contract_gb.Controls.Add(this.phone2_lbl);
            this.contract_gb.Controls.Add(this.contract1_txt);
            this.contract_gb.Controls.Add(this.label16);
            this.contract_gb.Location = new System.Drawing.Point(27, 339);
            this.contract_gb.Name = "contract_gb";
            this.contract_gb.Size = new System.Drawing.Size(850, 100);
            this.contract_gb.TabIndex = 47;
            this.contract_gb.TabStop = false;
            this.contract_gb.Text = "联系人";
            // 
            // email1_txt
            // 
            this.email1_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.email1_txt.Location = new System.Drawing.Point(667, 65);
            this.email1_txt.Name = "email1_txt";
            this.email1_txt.Size = new System.Drawing.Size(126, 23);
            this.email1_txt.TabIndex = 29;
            // 
            // email_lbl
            // 
            this.email_lbl.AutoSize = true;
            this.email_lbl.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.email_lbl.Location = new System.Drawing.Point(570, 68);
            this.email_lbl.Name = "email_lbl";
            this.email_lbl.Size = new System.Drawing.Size(39, 17);
            this.email_lbl.TabIndex = 28;
            this.email_lbl.Text = "Email";
            // 
            // phone11_txt
            // 
            this.phone11_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.phone11_txt.Location = new System.Drawing.Point(122, 65);
            this.phone11_txt.Name = "phone11_txt";
            this.phone11_txt.Size = new System.Drawing.Size(126, 23);
            this.phone11_txt.TabIndex = 27;
            // 
            // phone1_lbl
            // 
            this.phone1_lbl.AutoSize = true;
            this.phone1_lbl.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.phone1_lbl.Location = new System.Drawing.Point(15, 65);
            this.phone1_lbl.Name = "phone1_lbl";
            this.phone1_lbl.Size = new System.Drawing.Size(39, 17);
            this.phone1_lbl.TabIndex = 26;
            this.phone1_lbl.Text = "电话1";
            // 
            // phone21_txt
            // 
            this.phone21_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.phone21_txt.Location = new System.Drawing.Point(397, 65);
            this.phone21_txt.Name = "phone21_txt";
            this.phone21_txt.Size = new System.Drawing.Size(126, 23);
            this.phone21_txt.TabIndex = 23;
            // 
            // phone2_lbl
            // 
            this.phone2_lbl.AutoSize = true;
            this.phone2_lbl.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.phone2_lbl.Location = new System.Drawing.Point(293, 68);
            this.phone2_lbl.Name = "phone2_lbl";
            this.phone2_lbl.Size = new System.Drawing.Size(39, 17);
            this.phone2_lbl.TabIndex = 22;
            this.phone2_lbl.Text = "电话2";
            // 
            // contract1_txt
            // 
            this.contract1_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.contract1_txt.Location = new System.Drawing.Point(122, 24);
            this.contract1_txt.Name = "contract1_txt";
            this.contract1_txt.Size = new System.Drawing.Size(126, 23);
            this.contract1_txt.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(15, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 17);
            this.label16.TabIndex = 11;
            this.label16.Text = "联系人";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.zipCode_txt);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.doorNum_txt);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.city_txt);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.country_txt);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Location = new System.Drawing.Point(27, 229);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(850, 100);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "公司地址";
            // 
            // zipCode_txt
            // 
            this.zipCode_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zipCode_txt.Location = new System.Drawing.Point(402, 65);
            this.zipCode_txt.Name = "zipCode_txt";
            this.zipCode_txt.Size = new System.Drawing.Size(126, 23);
            this.zipCode_txt.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(293, 68);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 17);
            this.label20.TabIndex = 28;
            this.label20.Text = "邮编";
            // 
            // doorNum_txt
            // 
            this.doorNum_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.doorNum_txt.Location = new System.Drawing.Point(122, 65);
            this.doorNum_txt.Name = "doorNum_txt";
            this.doorNum_txt.Size = new System.Drawing.Size(126, 23);
            this.doorNum_txt.TabIndex = 27;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(15, 65);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 17);
            this.label21.TabIndex = 26;
            this.label21.Text = "街道/门牌号";
            // 
            // city_txt
            // 
            this.city_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.city_txt.Location = new System.Drawing.Point(402, 24);
            this.city_txt.Name = "city_txt";
            this.city_txt.Size = new System.Drawing.Size(126, 23);
            this.city_txt.TabIndex = 23;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(293, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 17);
            this.label23.TabIndex = 22;
            this.label23.Text = "城市";
            // 
            // country_txt
            // 
            this.country_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.country_txt.Location = new System.Drawing.Point(122, 24);
            this.country_txt.Name = "country_txt";
            this.country_txt.Size = new System.Drawing.Size(126, 23);
            this.country_txt.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(15, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 17);
            this.label24.TabIndex = 11;
            this.label24.Text = "国家";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.isExport_cmb);
            this.groupBox1.Controls.Add(this.isListed_cmb);
            this.groupBox1.Controls.Add(this.stockCode_txt);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.companyRegisterDate_txt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.companyRepresent_txt);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.companyCode_txt);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.companyRegisterCountry_cmb);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.companyWebsite_txt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.companyUsedname_txt);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.companyProperty_cmb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.supplierEnglishName_txt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.supplierChineseName_txt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(27, 25);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(850, 192);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "公司概况";
            // 
            // isExport_cmb
            // 
            this.isExport_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.isExport_cmb.FormattingEnabled = true;
            this.isExport_cmb.Location = new System.Drawing.Point(667, 144);
            this.isExport_cmb.Name = "isExport_cmb";
            this.isExport_cmb.Size = new System.Drawing.Size(121, 25);
            this.isExport_cmb.TabIndex = 45;
            // 
            // isListed_cmb
            // 
            this.isListed_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.isListed_cmb.FormattingEnabled = true;
            this.isListed_cmb.Location = new System.Drawing.Point(122, 141);
            this.isListed_cmb.Name = "isListed_cmb";
            this.isListed_cmb.Size = new System.Drawing.Size(121, 25);
            this.isListed_cmb.TabIndex = 44;
            // 
            // stockCode_txt
            // 
            this.stockCode_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.stockCode_txt.Location = new System.Drawing.Point(402, 141);
            this.stockCode_txt.Name = "stockCode_txt";
            this.stockCode_txt.Size = new System.Drawing.Size(126, 23);
            this.stockCode_txt.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(570, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 42;
            this.label10.Text = "是否有出口权";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(293, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 17);
            this.label11.TabIndex = 40;
            this.label11.Text = "股票代码";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(15, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 17);
            this.label12.TabIndex = 38;
            this.label12.Text = "是否为上市公司";
            // 
            // companyRegisterDate_txt
            // 
            this.companyRegisterDate_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyRegisterDate_txt.Location = new System.Drawing.Point(402, 104);
            this.companyRegisterDate_txt.Name = "companyRegisterDate_txt";
            this.companyRegisterDate_txt.Size = new System.Drawing.Size(126, 23);
            this.companyRegisterDate_txt.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(570, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 36;
            this.label7.Text = "法人代表";
            // 
            // companyRepresent_txt
            // 
            this.companyRepresent_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyRepresent_txt.Location = new System.Drawing.Point(667, 108);
            this.companyRepresent_txt.Name = "companyRepresent_txt";
            this.companyRepresent_txt.Size = new System.Drawing.Size(126, 23);
            this.companyRepresent_txt.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(293, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 34;
            this.label8.Text = "注册时间";
            // 
            // companyCode_txt
            // 
            this.companyCode_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyCode_txt.Location = new System.Drawing.Point(122, 105);
            this.companyCode_txt.Name = "companyCode_txt";
            this.companyCode_txt.Size = new System.Drawing.Size(126, 23);
            this.companyCode_txt.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(15, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 32;
            this.label9.Text = "公司代码";
            // 
            // companyRegisterCountry_cmb
            // 
            this.companyRegisterCountry_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyRegisterCountry_cmb.FormattingEnabled = true;
            this.companyRegisterCountry_cmb.Location = new System.Drawing.Point(402, 63);
            this.companyRegisterCountry_cmb.Name = "companyRegisterCountry_cmb";
            this.companyRegisterCountry_cmb.Size = new System.Drawing.Size(121, 25);
            this.companyRegisterCountry_cmb.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(570, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "公司网址";
            // 
            // companyWebsite_txt
            // 
            this.companyWebsite_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyWebsite_txt.Location = new System.Drawing.Point(667, 68);
            this.companyWebsite_txt.Name = "companyWebsite_txt";
            this.companyWebsite_txt.Size = new System.Drawing.Size(126, 23);
            this.companyWebsite_txt.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(293, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "企业注册国家";
            // 
            // companyUsedname_txt
            // 
            this.companyUsedname_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyUsedname_txt.Location = new System.Drawing.Point(122, 65);
            this.companyUsedname_txt.Name = "companyUsedname_txt";
            this.companyUsedname_txt.Size = new System.Drawing.Size(126, 23);
            this.companyUsedname_txt.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(15, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 26;
            this.label5.Text = "公司曾用名";
            // 
            // companyProperty_cmb
            // 
            this.companyProperty_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyProperty_cmb.FormattingEnabled = true;
            this.companyProperty_cmb.Location = new System.Drawing.Point(667, 27);
            this.companyProperty_cmb.Name = "companyProperty_cmb";
            this.companyProperty_cmb.Size = new System.Drawing.Size(121, 25);
            this.companyProperty_cmb.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(570, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "企业性质";
            // 
            // supplierEnglishName_txt
            // 
            this.supplierEnglishName_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.supplierEnglishName_txt.Location = new System.Drawing.Point(402, 24);
            this.supplierEnglishName_txt.Name = "supplierEnglishName_txt";
            this.supplierEnglishName_txt.Size = new System.Drawing.Size(126, 23);
            this.supplierEnglishName_txt.TabIndex = 23;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(293, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "供应商英文名称";
            // 
            // supplierChineseName_txt
            // 
            this.supplierChineseName_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.supplierChineseName_txt.Location = new System.Drawing.Point(122, 24);
            this.supplierChineseName_txt.Name = "supplierChineseName_txt";
            this.supplierChineseName_txt.Size = new System.Drawing.Size(126, 23);
            this.supplierChineseName_txt.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(15, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "供应商中文名称";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(905, 495);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "财务";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView1);
            this.groupBox6.Location = new System.Drawing.Point(27, 518);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(850, 171);
            this.groupBox6.TabIndex = 52;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "近五年财务状况";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle59.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle59.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle59.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridView1.ColumnHeadersHeight = 25;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column1,
            this.Column2});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(814, 125);
            this.dataGridView1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewTextBoxColumn1.HeaderText = "年度";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.ToolTipText = "年度";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle61.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewTextBoxColumn2.HeaderText = "总资产";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.ToolTipText = "总资产";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle62.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewTextBoxColumn3.HeaderText = "销售收入";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "利润率";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "研发经费投入";
            this.Column2.Name = "Column2";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgv_MoveTypeDetails);
            this.groupBox5.Location = new System.Drawing.Point(27, 344);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(850, 163);
            this.groupBox5.TabIndex = 51;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "前五大股东";
            // 
            // dgv_MoveTypeDetails
            // 
            this.dgv_MoveTypeDetails.AllowUserToAddRows = false;
            this.dgv_MoveTypeDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_MoveTypeDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle64.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_MoveTypeDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle64;
            this.dgv_MoveTypeDetails.ColumnHeadersHeight = 25;
            this.dgv_MoveTypeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_MoveTypeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_MT_ID,
            this.Col_MT_Name,
            this.Col_MT_TransactionDtb});
            this.dgv_MoveTypeDetails.EnableHeadersVisualStyles = false;
            this.dgv_MoveTypeDetails.Location = new System.Drawing.Point(6, 17);
            this.dgv_MoveTypeDetails.MultiSelect = false;
            this.dgv_MoveTypeDetails.Name = "dgv_MoveTypeDetails";
            this.dgv_MoveTypeDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_MoveTypeDetails.RowsDefaultCellStyle = dataGridViewCellStyle68;
            this.dgv_MoveTypeDetails.RowTemplate.Height = 23;
            this.dgv_MoveTypeDetails.RowTemplate.ReadOnly = true;
            this.dgv_MoveTypeDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_MoveTypeDetails.Size = new System.Drawing.Size(814, 121);
            this.dgv_MoveTypeDetails.TabIndex = 39;
            // 
            // Col_MT_ID
            // 
            dataGridViewCellStyle65.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Col_MT_ID.DefaultCellStyle = dataGridViewCellStyle65;
            this.Col_MT_ID.HeaderText = "股东名称";
            this.Col_MT_ID.Name = "Col_MT_ID";
            this.Col_MT_ID.ReadOnly = true;
            this.Col_MT_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Col_MT_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MT_ID.ToolTipText = "股东名称";
            // 
            // Col_MT_Name
            // 
            dataGridViewCellStyle66.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Col_MT_Name.DefaultCellStyle = dataGridViewCellStyle66;
            this.Col_MT_Name.HeaderText = "股比";
            this.Col_MT_Name.Name = "Col_MT_Name";
            this.Col_MT_Name.ReadOnly = true;
            this.Col_MT_Name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Col_MT_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MT_Name.ToolTipText = "股比";
            // 
            // Col_MT_TransactionDtb
            // 
            dataGridViewCellStyle67.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Col_MT_TransactionDtb.DefaultCellStyle = dataGridViewCellStyle67;
            this.Col_MT_TransactionDtb.HeaderText = "控股类型";
            this.Col_MT_TransactionDtb.Name = "Col_MT_TransactionDtb";
            this.Col_MT_TransactionDtb.ReadOnly = true;
            this.Col_MT_TransactionDtb.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Col_MT_TransactionDtb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox27);
            this.groupBox4.Controls.Add(this.textBox28);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.textBox34);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.textBox35);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.textBox36);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Location = new System.Drawing.Point(27, 222);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(850, 106);
            this.groupBox4.TabIndex = 50;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "银行细目";
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox27.Location = new System.Drawing.Point(402, 65);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(169, 23);
            this.textBox27.TabIndex = 47;
            // 
            // textBox28
            // 
            this.textBox28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox28.Location = new System.Drawing.Point(667, 24);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(153, 23);
            this.textBox28.TabIndex = 46;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label38.Location = new System.Drawing.Point(293, 68);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(56, 17);
            this.label38.TabIndex = 28;
            this.label38.Text = "银行账号";
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox34.Location = new System.Drawing.Point(122, 65);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(126, 23);
            this.textBox34.TabIndex = 27;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label39.Location = new System.Drawing.Point(15, 65);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 17);
            this.label39.TabIndex = 26;
            this.label39.Text = "银行户主";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.Location = new System.Drawing.Point(570, 27);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 17);
            this.label40.TabIndex = 24;
            this.label40.Text = "银行名称";
            // 
            // textBox35
            // 
            this.textBox35.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox35.Location = new System.Drawing.Point(402, 24);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(126, 23);
            this.textBox35.TabIndex = 23;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label41.Location = new System.Drawing.Point(293, 27);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(56, 17);
            this.label41.TabIndex = 22;
            this.label41.Text = "银行代码";
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox36.Location = new System.Drawing.Point(122, 24);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(126, 23);
            this.textBox36.TabIndex = 21;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label42.Location = new System.Drawing.Point(15, 27);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 17);
            this.label42.TabIndex = 11;
            this.label42.Text = "国家";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox24);
            this.groupBox3.Controls.Add(this.textBox23);
            this.groupBox3.Controls.Add(this.quickRatio_txt);
            this.groupBox3.Controls.Add(this.years_txt);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.equity_txt);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.textBox12);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.turnoverRate_txt);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.currentRatio_txt);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.assetLiability_txt);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.registerDate_txt);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.registerFund_txt);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Location = new System.Drawing.Point(29, 29);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(850, 180);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "财务";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox24.Location = new System.Drawing.Point(667, 141);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(126, 23);
            this.textBox24.TabIndex = 49;
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox23.Location = new System.Drawing.Point(122, 141);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(126, 23);
            this.textBox23.TabIndex = 48;
            // 
            // quickRatio_txt
            // 
            this.quickRatio_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.quickRatio_txt.Location = new System.Drawing.Point(402, 65);
            this.quickRatio_txt.Name = "quickRatio_txt";
            this.quickRatio_txt.Size = new System.Drawing.Size(126, 23);
            this.quickRatio_txt.TabIndex = 47;
            // 
            // years_txt
            // 
            this.years_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.years_txt.Location = new System.Drawing.Point(667, 24);
            this.years_txt.Name = "years_txt";
            this.years_txt.Size = new System.Drawing.Size(126, 23);
            this.years_txt.TabIndex = 46;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox9.Location = new System.Drawing.Point(402, 141);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(126, 23);
            this.textBox9.TabIndex = 43;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(570, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 17);
            this.label13.TabIndex = 42;
            this.label13.Text = "总资产汇报率";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(293, 147);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 17);
            this.label14.TabIndex = 40;
            this.label14.Text = "使用资本回报率";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(15, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 17);
            this.label15.TabIndex = 38;
            this.label15.Text = "净利率";
            // 
            // equity_txt
            // 
            this.equity_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.equity_txt.Location = new System.Drawing.Point(402, 105);
            this.equity_txt.Name = "equity_txt";
            this.equity_txt.Size = new System.Drawing.Size(126, 23);
            this.equity_txt.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(570, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 34);
            this.label18.TabIndex = 36;
            this.label18.Text = "利润总额占\r\n营业额的百分比";
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox12.Location = new System.Drawing.Point(667, 108);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(126, 23);
            this.textBox12.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(293, 108);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 17);
            this.label19.TabIndex = 34;
            this.label19.Text = "股东权益";
            // 
            // turnoverRate_txt
            // 
            this.turnoverRate_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.turnoverRate_txt.Location = new System.Drawing.Point(122, 105);
            this.turnoverRate_txt.Name = "turnoverRate_txt";
            this.turnoverRate_txt.Size = new System.Drawing.Size(126, 23);
            this.turnoverRate_txt.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(15, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 17);
            this.label22.TabIndex = 32;
            this.label22.Text = "账款周转率";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.Location = new System.Drawing.Point(570, 68);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 17);
            this.label25.TabIndex = 30;
            this.label25.Text = "流动比率";
            // 
            // currentRatio_txt
            // 
            this.currentRatio_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.currentRatio_txt.Location = new System.Drawing.Point(667, 68);
            this.currentRatio_txt.Name = "currentRatio_txt";
            this.currentRatio_txt.Size = new System.Drawing.Size(126, 23);
            this.currentRatio_txt.TabIndex = 29;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.Location = new System.Drawing.Point(293, 68);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 17);
            this.label26.TabIndex = 28;
            this.label26.Text = "速动比率";
            // 
            // assetLiability_txt
            // 
            this.assetLiability_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.assetLiability_txt.Location = new System.Drawing.Point(122, 65);
            this.assetLiability_txt.Name = "assetLiability_txt";
            this.assetLiability_txt.Size = new System.Drawing.Size(126, 23);
            this.assetLiability_txt.TabIndex = 27;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(15, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 17);
            this.label27.TabIndex = 26;
            this.label27.Text = "资产负载率";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(570, 27);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(56, 17);
            this.label28.TabIndex = 24;
            this.label28.Text = "从业年数";
            // 
            // registerDate_txt
            // 
            this.registerDate_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.registerDate_txt.Location = new System.Drawing.Point(402, 24);
            this.registerDate_txt.Name = "registerDate_txt";
            this.registerDate_txt.Size = new System.Drawing.Size(126, 23);
            this.registerDate_txt.TabIndex = 23;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.Location = new System.Drawing.Point(293, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(80, 17);
            this.label29.TabIndex = 22;
            this.label29.Text = "公司成立日期";
            // 
            // registerFund_txt
            // 
            this.registerFund_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.registerFund_txt.Location = new System.Drawing.Point(122, 24);
            this.registerFund_txt.Name = "registerFund_txt";
            this.registerFund_txt.Size = new System.Drawing.Size(126, 23);
            this.registerFund_txt.TabIndex = 21;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(15, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 17);
            this.label30.TabIndex = 11;
            this.label30.Text = "注册资金（万元）";
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Location = new System.Drawing.Point(4, 21);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(905, 495);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "销售";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dataGridView4);
            this.groupBox9.Location = new System.Drawing.Point(27, 456);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(850, 179);
            this.groupBox9.TabIndex = 42;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "前五大客户";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle69.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle69.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle69.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle69.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle69.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle69.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridView4.ColumnHeadersHeight = 25;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView4.EnableHeadersVisualStyles = false;
            this.dataGridView4.Location = new System.Drawing.Point(18, 30);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle70.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle70.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView4.RowsDefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridView4.RowTemplate.Height = 23;
            this.dataGridView4.RowTemplate.ReadOnly = true;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(814, 144);
            this.dataGridView4.TabIndex = 40;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView3);
            this.groupBox8.Location = new System.Drawing.Point(27, 237);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(850, 198);
            this.groupBox8.TabIndex = 41;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "前五大竞争对手";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle71.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle71.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle71.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle71.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle71.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridView3.ColumnHeadersHeight = 25;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dataGridView3.EnableHeadersVisualStyles = false;
            this.dataGridView3.Location = new System.Drawing.Point(18, 30);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle74.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle74.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView3.RowsDefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridView3.RowTemplate.Height = 23;
            this.dataGridView3.RowTemplate.ReadOnly = true;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(814, 144);
            this.dataGridView3.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle72.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn9.HeaderText = "竞争对手名称";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.ToolTipText = "竞争对手名称";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle73.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn10.HeaderText = "所属行业";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.ToolTipText = "所属行业";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dataGridView2);
            this.groupBox7.Location = new System.Drawing.Point(27, 24);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(850, 198);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "最近五年销售额";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle75.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle75.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle75.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle75.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridView2.ColumnHeadersHeight = 25;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(18, 30);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle81.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle81.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle81;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.RowTemplate.ReadOnly = true;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(814, 144);
            this.dataGridView2.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle76.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle76;
            this.dataGridViewTextBoxColumn4.HeaderText = "年度";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.ToolTipText = "年度";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle77.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle77;
            this.dataGridViewTextBoxColumn5.HeaderText = "全球总营业额";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.ToolTipText = "全球总营业额";
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle78.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle78;
            this.dataGridViewTextBoxColumn6.HeaderText = "中国地区总营业额";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.ToolTipText = "股东名称";
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle79.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewTextBoxColumn7.HeaderText = "全球销售总利润";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.ToolTipText = "股比";
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle80.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn8.HeaderText = "中国地区总利润";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.groupBox16);
            this.tabPage4.Controls.Add(this.groupBox15);
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Controls.Add(this.groupBox13);
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Location = new System.Drawing.Point(4, 21);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(905, 495);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "技术";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.dataGridView11);
            this.groupBox16.Location = new System.Drawing.Point(27, 1368);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(850, 209);
            this.groupBox16.TabIndex = 51;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "技术能力项目";
            // 
            // dataGridView11
            // 
            this.dataGridView11.AllowUserToAddRows = false;
            this.dataGridView11.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle82.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle82.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle82.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle82.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle82.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle82.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle82;
            this.dataGridView11.ColumnHeadersHeight = 25;
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView11.EnableHeadersVisualStyles = false;
            this.dataGridView11.Location = new System.Drawing.Point(18, 26);
            this.dataGridView11.MultiSelect = false;
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle83.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle83.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView11.RowsDefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridView11.RowTemplate.Height = 23;
            this.dataGridView11.RowTemplate.ReadOnly = true;
            this.dataGridView11.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView11.Size = new System.Drawing.Size(814, 144);
            this.dataGridView11.TabIndex = 40;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.dataGridView10);
            this.groupBox15.Location = new System.Drawing.Point(27, 1155);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(850, 209);
            this.groupBox15.TabIndex = 51;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "专利情况";
            // 
            // dataGridView10
            // 
            this.dataGridView10.AllowUserToAddRows = false;
            this.dataGridView10.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView10.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridView10.ColumnHeadersHeight = 25;
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView10.EnableHeadersVisualStyles = false;
            this.dataGridView10.Location = new System.Drawing.Point(18, 26);
            this.dataGridView10.MultiSelect = false;
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView10.RowsDefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridView10.RowTemplate.Height = 23;
            this.dataGridView10.RowTemplate.ReadOnly = true;
            this.dataGridView10.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView10.Size = new System.Drawing.Size(814, 144);
            this.dataGridView10.TabIndex = 40;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.dataGridView9);
            this.groupBox14.Location = new System.Drawing.Point(27, 941);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(850, 209);
            this.groupBox14.TabIndex = 51;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "实验室情况";
            // 
            // dataGridView9
            // 
            this.dataGridView9.AllowUserToAddRows = false;
            this.dataGridView9.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle86.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle86.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle86.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle86.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle86.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle86.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView9.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridView9.ColumnHeadersHeight = 25;
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView9.EnableHeadersVisualStyles = false;
            this.dataGridView9.Location = new System.Drawing.Point(18, 26);
            this.dataGridView9.MultiSelect = false;
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView9.RowsDefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridView9.RowTemplate.Height = 23;
            this.dataGridView9.RowTemplate.ReadOnly = true;
            this.dataGridView9.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView9.Size = new System.Drawing.Size(814, 144);
            this.dataGridView9.TabIndex = 40;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.dataGridView8);
            this.groupBox13.Location = new System.Drawing.Point(27, 726);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(850, 209);
            this.groupBox13.TabIndex = 50;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "设备情况";
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToAddRows = false;
            this.dataGridView8.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle88.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle88.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle88.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView8.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridView8.ColumnHeadersHeight = 25;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView8.EnableHeadersVisualStyles = false;
            this.dataGridView8.Location = new System.Drawing.Point(18, 26);
            this.dataGridView8.MultiSelect = false;
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView8.RowsDefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridView8.RowTemplate.Height = 23;
            this.dataGridView8.RowTemplate.ReadOnly = true;
            this.dataGridView8.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView8.Size = new System.Drawing.Size(814, 144);
            this.dataGridView8.TabIndex = 40;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.dataGridView7);
            this.groupBox12.Location = new System.Drawing.Point(27, 477);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(850, 234);
            this.groupBox12.TabIndex = 49;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "软件使用情况";
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToAddRows = false;
            this.dataGridView7.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle90.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle90;
            this.dataGridView7.ColumnHeadersHeight = 25;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView7.EnableHeadersVisualStyles = false;
            this.dataGridView7.Location = new System.Drawing.Point(18, 26);
            this.dataGridView7.MultiSelect = false;
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView7.RowsDefaultCellStyle = dataGridViewCellStyle91;
            this.dataGridView7.RowTemplate.Height = 23;
            this.dataGridView7.RowTemplate.ReadOnly = true;
            this.dataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView7.Size = new System.Drawing.Size(814, 144);
            this.dataGridView7.TabIndex = 40;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.comboBox8);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Controls.Add(this.comboBox5);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.dataGridView6);
            this.groupBox11.Location = new System.Drawing.Point(27, 237);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(850, 234);
            this.groupBox11.TabIndex = 41;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "技术情况";
            // 
            // comboBox8
            // 
            this.comboBox8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(447, 33);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(121, 25);
            this.comboBox8.TabIndex = 48;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label32.Location = new System.Drawing.Point(331, 36);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 17);
            this.label32.TabIndex = 47;
            this.label32.Text = "技术来源";
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(136, 33);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 25);
            this.comboBox5.TabIndex = 46;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(20, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(56, 17);
            this.label31.TabIndex = 45;
            this.label31.Text = "技术水平";
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle92.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle92.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle92.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle92.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle92.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle92.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle92;
            this.dataGridView6.ColumnHeadersHeight = 25;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView6.EnableHeadersVisualStyles = false;
            this.dataGridView6.Location = new System.Drawing.Point(18, 76);
            this.dataGridView6.MultiSelect = false;
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle93.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle93.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView6.RowsDefaultCellStyle = dataGridViewCellStyle93;
            this.dataGridView6.RowTemplate.Height = 23;
            this.dataGridView6.RowTemplate.ReadOnly = true;
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.Size = new System.Drawing.Size(814, 144);
            this.dataGridView6.TabIndex = 40;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.dataGridView5);
            this.groupBox10.Location = new System.Drawing.Point(27, 24);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(850, 198);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "合作经历";
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToAddRows = false;
            this.dataGridView5.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle94.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle94.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle94.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle94.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle94.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle94.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle94;
            this.dataGridView5.ColumnHeadersHeight = 25;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19});
            this.dataGridView5.EnableHeadersVisualStyles = false;
            this.dataGridView5.Location = new System.Drawing.Point(18, 30);
            this.dataGridView5.MultiSelect = false;
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView5.RowsDefaultCellStyle = dataGridViewCellStyle101;
            this.dataGridView5.RowTemplate.Height = 23;
            this.dataGridView5.RowTemplate.ReadOnly = true;
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.Size = new System.Drawing.Size(814, 144);
            this.dataGridView5.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle95.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle95;
            this.dataGridViewTextBoxColumn14.HeaderText = "编号";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.ToolTipText = "编号";
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle96.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle96;
            this.dataGridViewTextBoxColumn15.HeaderText = "合作项目名称";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.ToolTipText = "合作项目名称";
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle97.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle97;
            this.dataGridViewTextBoxColumn16.HeaderText = "负责人";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.ToolTipText = "职称";
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle98.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle98;
            this.dataGridViewTextBoxColumn17.HeaderText = "联系电话";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.ToolTipText = "人数";
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle99.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle99;
            this.dataGridViewTextBoxColumn18.HeaderText = "手机";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.ToolTipText = "职称";
            this.dataGridViewTextBoxColumn18.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle100.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle100;
            this.dataGridViewTextBoxColumn19.HeaderText = "项目是否完成";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.ToolTipText = "编号";
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.groupBox17);
            this.tabPage5.Controls.Add(this.groupBox18);
            this.tabPage5.Location = new System.Drawing.Point(4, 21);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(905, 495);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "质量";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.dataGridView12);
            this.groupBox17.Location = new System.Drawing.Point(27, 250);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(850, 209);
            this.groupBox17.TabIndex = 53;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "质量PPM";
            // 
            // dataGridView12
            // 
            this.dataGridView12.AllowUserToAddRows = false;
            this.dataGridView12.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle102.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle102.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle102.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle102.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle102.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle102.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView12.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridView12.ColumnHeadersHeight = 25;
            this.dataGridView12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView12.EnableHeadersVisualStyles = false;
            this.dataGridView12.Location = new System.Drawing.Point(18, 26);
            this.dataGridView12.MultiSelect = false;
            this.dataGridView12.Name = "dataGridView12";
            this.dataGridView12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView12.RowsDefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridView12.RowTemplate.Height = 23;
            this.dataGridView12.RowTemplate.ReadOnly = true;
            this.dataGridView12.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView12.Size = new System.Drawing.Size(814, 144);
            this.dataGridView12.TabIndex = 40;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.dataGridView13);
            this.groupBox18.Location = new System.Drawing.Point(27, 19);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(850, 209);
            this.groupBox18.TabIndex = 52;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "质量/证书";
            // 
            // dataGridView13
            // 
            this.dataGridView13.AllowUserToAddRows = false;
            this.dataGridView13.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle104.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle104.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle104.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle104.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle104.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle104.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView13.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridView13.ColumnHeadersHeight = 25;
            this.dataGridView13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53});
            this.dataGridView13.EnableHeadersVisualStyles = false;
            this.dataGridView13.Location = new System.Drawing.Point(18, 26);
            this.dataGridView13.MultiSelect = false;
            this.dataGridView13.Name = "dataGridView13";
            this.dataGridView13.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle110.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle110.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView13.RowsDefaultCellStyle = dataGridViewCellStyle110;
            this.dataGridView13.RowTemplate.Height = 23;
            this.dataGridView13.RowTemplate.ReadOnly = true;
            this.dataGridView13.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView13.Size = new System.Drawing.Size(814, 144);
            this.dataGridView13.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn49
            // 
            dataGridViewCellStyle105.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridViewTextBoxColumn49.HeaderText = "编号";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn49.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn50
            // 
            dataGridViewCellStyle106.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn50.DefaultCellStyle = dataGridViewCellStyle106;
            this.dataGridViewTextBoxColumn50.HeaderText = "证书编号";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn50.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn51
            // 
            dataGridViewCellStyle107.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn51.DefaultCellStyle = dataGridViewCellStyle107;
            this.dataGridViewTextBoxColumn51.HeaderText = "证书名称";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 150;
            // 
            // dataGridViewTextBoxColumn52
            // 
            dataGridViewCellStyle108.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn52.DefaultCellStyle = dataGridViewCellStyle108;
            this.dataGridViewTextBoxColumn52.HeaderText = "获得年份";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn52.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn53
            // 
            dataGridViewCellStyle109.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn53.DefaultCellStyle = dataGridViewCellStyle109;
            this.dataGridViewTextBoxColumn53.HeaderText = "附件";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn53.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.groupBox20);
            this.tabPage6.Controls.Add(this.groupBox19);
            this.tabPage6.Location = new System.Drawing.Point(4, 21);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(905, 495);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "产能";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.dataGridView14);
            this.groupBox20.Location = new System.Drawing.Point(25, 255);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(850, 229);
            this.groupBox20.TabIndex = 50;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "产品/产能";
            // 
            // dataGridView14
            // 
            this.dataGridView14.AllowUserToAddRows = false;
            this.dataGridView14.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle111.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle111.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle111.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle111.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle111.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle111.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView14.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle111;
            this.dataGridView14.ColumnHeadersHeight = 25;
            this.dataGridView14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView14.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn54});
            this.dataGridView14.EnableHeadersVisualStyles = false;
            this.dataGridView14.Location = new System.Drawing.Point(18, 24);
            this.dataGridView14.MultiSelect = false;
            this.dataGridView14.Name = "dataGridView14";
            this.dataGridView14.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle116.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle116.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView14.RowsDefaultCellStyle = dataGridViewCellStyle116;
            this.dataGridView14.RowTemplate.Height = 23;
            this.dataGridView14.RowTemplate.ReadOnly = true;
            this.dataGridView14.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView14.Size = new System.Drawing.Size(814, 144);
            this.dataGridView14.TabIndex = 41;
            // 
            // dataGridViewTextBoxColumn46
            // 
            dataGridViewCellStyle112.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn46.DefaultCellStyle = dataGridViewCellStyle112;
            this.dataGridViewTextBoxColumn46.HeaderText = "主要产品名称";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn46.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn47
            // 
            dataGridViewCellStyle113.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn47.DefaultCellStyle = dataGridViewCellStyle113;
            this.dataGridViewTextBoxColumn47.HeaderText = "年供应量";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn47.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn48
            // 
            dataGridViewCellStyle114.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn48.DefaultCellStyle = dataGridViewCellStyle114;
            this.dataGridViewTextBoxColumn48.HeaderText = "设备型号";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn48.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn48.Width = 150;
            // 
            // dataGridViewTextBoxColumn54
            // 
            dataGridViewCellStyle115.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dataGridViewTextBoxColumn54.DefaultCellStyle = dataGridViewCellStyle115;
            this.dataGridViewTextBoxColumn54.HeaderText = "所占设备负荷";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn54.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn54.Width = 150;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.comboBox13);
            this.groupBox19.Controls.Add(this.textBox38);
            this.groupBox19.Controls.Add(this.label50);
            this.groupBox19.Controls.Add(this.label51);
            this.groupBox19.Controls.Add(this.comboBox9);
            this.groupBox19.Controls.Add(this.comboBox10);
            this.groupBox19.Controls.Add(this.textBox25);
            this.groupBox19.Controls.Add(this.label33);
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.label35);
            this.groupBox19.Controls.Add(this.textBox26);
            this.groupBox19.Controls.Add(this.label36);
            this.groupBox19.Controls.Add(this.textBox29);
            this.groupBox19.Controls.Add(this.label37);
            this.groupBox19.Controls.Add(this.textBox30);
            this.groupBox19.Controls.Add(this.label43);
            this.groupBox19.Controls.Add(this.comboBox11);
            this.groupBox19.Controls.Add(this.label44);
            this.groupBox19.Controls.Add(this.textBox31);
            this.groupBox19.Controls.Add(this.label45);
            this.groupBox19.Controls.Add(this.textBox32);
            this.groupBox19.Controls.Add(this.label46);
            this.groupBox19.Controls.Add(this.comboBox12);
            this.groupBox19.Controls.Add(this.label47);
            this.groupBox19.Controls.Add(this.textBox33);
            this.groupBox19.Controls.Add(this.label48);
            this.groupBox19.Controls.Add(this.textBox37);
            this.groupBox19.Controls.Add(this.label49);
            this.groupBox19.Location = new System.Drawing.Point(25, 20);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(850, 229);
            this.groupBox19.TabIndex = 2;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "生产基础设施";
            // 
            // comboBox13
            // 
            this.comboBox13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Location = new System.Drawing.Point(122, 179);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(121, 25);
            this.comboBox13.TabIndex = 49;
            // 
            // textBox38
            // 
            this.textBox38.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox38.Location = new System.Drawing.Point(402, 179);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(126, 23);
            this.textBox38.TabIndex = 48;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label50.Location = new System.Drawing.Point(293, 185);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(83, 17);
            this.label50.TabIndex = 47;
            this.label50.Text = "发电能力(kW)";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label51.Location = new System.Drawing.Point(15, 182);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(92, 17);
            this.label51.TabIndex = 46;
            this.label51.Text = "是否租用发电机";
            // 
            // comboBox9
            // 
            this.comboBox9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(667, 144);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(121, 25);
            this.comboBox9.TabIndex = 45;
            // 
            // comboBox10
            // 
            this.comboBox10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(122, 141);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(121, 25);
            this.comboBox10.TabIndex = 44;
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox25.Location = new System.Drawing.Point(402, 141);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(126, 23);
            this.textBox25.TabIndex = 43;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label33.Location = new System.Drawing.Point(570, 147);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 17);
            this.label33.TabIndex = 42;
            this.label33.Text = "用电负荷";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label34.Location = new System.Drawing.Point(293, 147);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 17);
            this.label34.TabIndex = 40;
            this.label34.Text = "天然气用量";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(15, 144);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 17);
            this.label35.TabIndex = 38;
            this.label35.Text = "中转库名称";
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox26.Location = new System.Drawing.Point(402, 99);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(126, 23);
            this.textBox26.TabIndex = 37;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label36.Location = new System.Drawing.Point(570, 108);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(56, 17);
            this.label36.TabIndex = 36;
            this.label36.Text = "运输时间";
            // 
            // textBox29
            // 
            this.textBox29.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox29.Location = new System.Drawing.Point(667, 108);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(126, 23);
            this.textBox29.TabIndex = 35;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(293, 108);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(98, 17);
            this.label37.TabIndex = 34;
            this.label37.Text = "运输距离（km）";
            // 
            // textBox30
            // 
            this.textBox30.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox30.Location = new System.Drawing.Point(122, 105);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(126, 23);
            this.textBox30.TabIndex = 33;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label43.Location = new System.Drawing.Point(15, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 17);
            this.label43.TabIndex = 32;
            this.label43.Text = "运输方式";
            // 
            // comboBox11
            // 
            this.comboBox11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(402, 63);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(121, 25);
            this.comboBox11.TabIndex = 31;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(570, 68);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(80, 17);
            this.label44.TabIndex = 30;
            this.label44.Text = "内部仓库面积";
            // 
            // textBox31
            // 
            this.textBox31.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox31.Location = new System.Drawing.Point(667, 68);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(126, 23);
            this.textBox31.TabIndex = 29;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label45.Location = new System.Drawing.Point(293, 68);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(80, 17);
            this.label45.TabIndex = 28;
            this.label45.Text = "内部仓库情况";
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox32.Location = new System.Drawing.Point(122, 65);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(126, 23);
            this.textBox32.TabIndex = 27;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label46.Location = new System.Drawing.Point(15, 65);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 17);
            this.label46.TabIndex = 26;
            this.label46.Text = "厂房使用率";
            // 
            // comboBox12
            // 
            this.comboBox12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(667, 27);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(121, 25);
            this.comboBox12.TabIndex = 25;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(570, 27);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(56, 17);
            this.label47.TabIndex = 24;
            this.label47.Text = "厂房面积";
            // 
            // textBox33
            // 
            this.textBox33.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox33.Location = new System.Drawing.Point(402, 24);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(126, 23);
            this.textBox33.TabIndex = 23;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label48.Location = new System.Drawing.Point(293, 27);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(56, 17);
            this.label48.TabIndex = 22;
            this.label48.Text = "占地面积";
            // 
            // textBox37
            // 
            this.textBox37.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox37.Location = new System.Drawing.Point(122, 24);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(126, 23);
            this.textBox37.TabIndex = 21;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label49.Location = new System.Drawing.Point(15, 27);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(104, 17);
            this.label49.TabIndex = 11;
            this.label49.Text = "直接生产人员总数";
            // 
            // SupplierDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(926, 537);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "SupplierDetailsForm";
            this.Text = "供应商详细信息";
            this.Load += new System.EventHandler(this.SupplierDetailsForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contract_gb.ResumeLayout(false);
            this.contract_gb.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MoveTypeDetails)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.groupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.groupBox14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).EndInit();
            this.groupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox contract_gb;
        private System.Windows.Forms.TextBox email1_txt;
        private System.Windows.Forms.Label email_lbl;
        private System.Windows.Forms.TextBox phone11_txt;
        private System.Windows.Forms.Label phone1_lbl;
        private System.Windows.Forms.TextBox phone21_txt;
        private System.Windows.Forms.Label phone2_lbl;
        private System.Windows.Forms.TextBox contract1_txt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox zipCode_txt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox doorNum_txt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox city_txt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox country_txt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox isExport_cmb;
        private System.Windows.Forms.ComboBox isListed_cmb;
        private System.Windows.Forms.TextBox stockCode_txt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox companyRegisterDate_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox companyRepresent_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox companyCode_txt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox companyRegisterCountry_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox companyWebsite_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox companyUsedname_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox companyProperty_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox supplierEnglishName_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox supplierChineseName_txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgv_MoveTypeDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_TransactionDtb;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox quickRatio_txt;
        private System.Windows.Forms.TextBox years_txt;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox equity_txt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox turnoverRate_txt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox currentRatio_txt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox assetLiability_txt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox registerDate_txt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox registerFund_txt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.DataGridView dataGridView12;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DataGridView dataGridView13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.DataGridView dataGridView14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label49;
    }
}