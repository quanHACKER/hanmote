﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Bll.CertificationProcess.LeadDecide;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class Supplier_Permission : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        LeadDecidebll leaderDecidebll = new LeadDecidebll();
        PerimissionSupplierInfo2 f = null;
        public Supplier_Permission()
        {
            InitializeComponent();
            LoadData();
        }



        private void supplierInfoTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0)
            {

                if (supplierInfoTable.Columns[e.ColumnIndex].Name == "查看")
                {
                    string supplierId = supplierInfoTable.CurrentRow.Cells[1].Value.ToString();

                    if (this.f == null || this.f.IsDisposed)
                    {
                        f = new PerimissionSupplierInfo2(supplierId);
                       
                    }

                    SingletonUserUI.addToUserUI(f);

                }

            }

        }

        private void select_button_Click(object sender, EventArgs e)
        {

            string status = "";
            status = status_comboBox.Text;
            string country = "";
            country = state_comboBox.Text;
            string name = "";
            string purchase = "";
            purchase = PurchaseOrg_textBox.Text;
            string thingGroup = "";
            thingGroup = thing_textBox.Text;
            name = textBox1.Text;
            DataTable dt = leaderDecidebll.QueryConditionbll(purchase, thingGroup, status, country, name, 2, SingleUserInstance.getCurrentUserInstance().User_ID);
            dt.Columns.Add("等级", System.Type.GetType("System.String"));
            string flag = "";
            string level = "A";
            float SocrePerence = 0;
            float cGreenValue = 0;
            float cLowValue = 0;
            for (int count = 0; count < dt.Rows.Count; count++)
            {
                flag = dt.Rows[count]["TagLocalEval"].ToString();
                //获取绿色目标和最低目标
                cGreenValue = (float)leaderDecidebll.getCGreenValueAndGLowValue().Rows[0]["CGreenValue"];
                cLowValue = (float)leaderDecidebll.getCGreenValueAndGLowValue().Rows[0]["CLowValue"];
                DataTable Socre = leaderDecidebll.getScore(dt.Rows[count][0].ToString(), flag);
                if (Socre.Rows.Count > 0)
                    SocrePerence = float.Parse(Socre.Rows[0][0].ToString()) / float.Parse(Socre.Rows[0][1].ToString());
                SocrePerence=(float) Math.Round(SocrePerence, 3);
                if (!dt.Rows[count]["SupLevel"].ToString().Equals("A")) {
                    if (SocrePerence >= cGreenValue)
                    {
                        level = "A";
                    }
                    else
                    {
                        if (SocrePerence >= cLowValue)
                        {
                            level = "B";
                        }
                        else
                        {
                            level = "C";
                        }

                    }
                    SocrePerence = SocrePerence * 100;
                    dt.Rows[count]["等级"] = level + "( 得分率：" + SocrePerence.ToString() + "%)";

                }
                dt.Rows[count]["等级"] = "A(已改善)";
            }
            dt.Columns.Remove("TagLocalEval");
            dt.Columns.Remove("SupLevel");
            this.supplierInfoTable.DataSource = dt;
            if (dt.Rows.Count == 0)
            {
                pageNext1.PageIndex = 0;
            }
            else {
                pageNext1.PageIndex = 1;
            }
           
            pageNext1.DrawControl(dt.Rows.Count);


        }

        private void supplierInfoTable_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            for (int i = 0; i < this.supplierInfoTable.Rows.Count; i++)
            {
                DataGridViewRow r = this.supplierInfoTable.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.supplierInfoTable.Refresh();
        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }


        public void LoadData()
        {
           
            int totalSize = 0;
            DataTable dt = leaderDecidebll.SelectPermission(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, this.pageNext1.PageIndex, out totalSize);
            dt.Columns.Add("等级", System.Type.GetType("System.String"));
            string flag = "";
            string level = "A";
            float SocrePerence = 0;
            float cGreenValue = 0;
            float cLowValue = 0;
            for (int count = 0; count < dt.Rows.Count; count++)
            {

                cGreenValue = (float)leaderDecidebll.getCGreenValueAndGLowValue().Rows[0]["CGreenValue"];
                cLowValue = (float)leaderDecidebll.getCGreenValueAndGLowValue().Rows[0]["LowValue"];

                flag = dt.Rows[count]["TagLocalEval"].ToString();
                if (!dt.Rows[count]["SupLevel"].ToString().Equals("A"))
                {
                    DataTable Socre = leaderDecidebll.getScore(dt.Rows[count][0].ToString(), flag);
                    if (Socre.Rows.Count > 0)
                        SocrePerence = 0.9f;
                    try
                    {
                        SocrePerence = float.Parse(Socre.Rows[0][0].ToString()) / float.Parse(Socre.Rows[0][1].ToString());
                    }
                    catch
                    {

                    }
                    SocrePerence = (float)Math.Round(SocrePerence, 3);
                    if (SocrePerence >= cGreenValue)
                    {
                        level = "A";
                    }
                    else
                    {
                        if (SocrePerence >= cLowValue)
                        {
                            level = "B";
                        }
                        else
                        {
                            level = "C";
                        }

                    }
                    SocrePerence = SocrePerence * 100;
                    dt.Rows[count]["等级"] = level + "(得分率：" + SocrePerence.ToString() + "%)";
                }
                else
                {
                    dt.Rows[count]["等级"] = "A(已改善)";
                }

            }
            dt.Columns.Remove("TagLocalEval");
            dt.Columns.Remove("SupLevel");
            this.supplierInfoTable.DataSource = dt;
            pageNext1.DrawControl(totalSize);
        }

        private void Supplier_Permission_Load(object sender, EventArgs e)
        {

        }
    }
}
