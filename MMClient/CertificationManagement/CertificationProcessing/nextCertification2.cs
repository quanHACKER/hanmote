﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class nextCertification2 : Form
    {
        string User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;//主审id
        private string email;
        private string id;
        private string thing;
        private string reason;
        private string sname;
        string mtName;
        public nextCertification2(string mtName, string email, string id,string thing, string reason, string sname)
        {
            InitializeComponent();
            this.email = email;
            this.id = id;
            this.thing = thing;
            this.reason = reason;
            this.sname = sname;
            this.mtName = mtName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            string body = this.richTextBox1.Text;
            List<MailAddress> addresses = new List<MailAddress>();
            string subject = "告知用户进入高层评估";
            if (string.IsNullOrWhiteSpace(this.richTextBox1.Text))
            {
                MessageUtil.ShowWarning("通知理由不能为空！");
                return;
            }

            string toAddresses = this.email;

            MailMessage message = new MailMessage();
            message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                            /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                for (int i = 0; i < toAddresses.Count; i++)*/
            message.To.Add(toAddresses);
            message.Subject = subject;

            message.IsBodyHtml = true;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = body;


            message.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
            client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
            client.EnableSsl = true; //必须经过ssl加密 
            try
            {
                client.Send(message);

                MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

            }
            catch (Exception ee)
            {
                MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

            }
            gn.RestartInf1(id);//处理状态置1
            gn.RestartInf2(id);//提交高层状态置1
            string gid = null;
            string level=null;
            DataTable dt= gn.getid(User_ID);
            if (dt.Rows.Count == 0 )
            {
                MessageUtil.ShowWarning("该主审无高层领导！");
            }
            else
                gid = dt.Rows[0][0].ToString();
            DataTable dt1 = gn.getthing(this.thing);
            if (dt1.Rows.Count == 0)
            {
                MessageUtil.ShowWarning("该供应商无物料组！");
            }
            else
                level = dt1.Rows[0][0].ToString();

            string zzid = sname + "预评审已完结待提交上级审批";
            //string sname1 = sname + "待主审预评";
           // string sname2 = sname + "预评审已完结待提交现场评审";
            string zhuid = null;
            gn.adeletetask(zhuid, zzid);
            DataTable dt5 = gn.getid(User_ID);
            zhuid = dt5.Rows[0][0].ToString();
            gn.adeletetask(zhuid, zzid);
            //gn.Updatetask(zhuid, sname);
            //gn.Updatetask(zhuid, sname1);
        //    gn.Updatetask(zhuid, sname2);

            gn.RestartInf1(id);//处理状态置1
            gn.pdelete(id);
            gn.RestartInf3(id, gid,User_ID, level,this.reason);//供应商id
            //插一个主审数据
            gn.prdelete(id, User_ID);
            int score=0;
            int sum=0;
            DataTable dt2 = gn.getscore(this.id);
            for (int i = 0; i < dt2.Rows.Count; i++)
                score = score + int.Parse(dt2.Rows[i][0].ToString());
            DataTable dt3 = gn.getsum(this.id);
            for (int i = 0; i < dt2.Rows.Count; i++)
                sum = sum + int.Parse(dt2.Rows[i][0].ToString());
            
            string ascore;
            string bsum;
            ascore = Convert.ToString(score);
            bsum = Convert.ToString(sum);
            gn.deal1(this.id, this.thing, User_ID, ascore, bsum);
            DataTable dtName = gn.getEClassMtName();
            if (dtName.Rows.Count > 0)
            {
                for (int i = 0; i < dtName.Rows.Count; i++)
                {
                    if (mtName.Trim().Equals(dtName.Rows[i][0].ToString().Trim()))
                    {
                        try {
                           // string pid = gn.getid(zhuid).Rows[0][0].ToString();
                            gn.inserttask(zhuid, zzid);
                            MessageUtil.ShowWarning("处理完成！");
                            return;
                        }
                        catch (Exception) {
                            MessageBox.Show("无上级");
                        }
                    }
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
