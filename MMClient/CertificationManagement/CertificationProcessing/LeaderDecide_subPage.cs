﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Bll.CertificationProcess.LeadDecide;
using System.IO;
using Lib.SqlServerDAL;
using MMClient.progress;
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using MMClient.CommFileShow;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class LeaderDecide_subPage : DockContent
    {
        FileShowForm fileShowForm = null;
        int tagThird, TagLocalEval;
        FTPHelper fTPHelper = new FTPHelper("");
        LeadDecidebll leadDecidebll = new LeadDecidebll();
        string supplierID;
        int backTag, stepTag, statusEnd, IsProcess;
        string IsAgree;
        string user_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
        string companyName;
        LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
        string MtName;
        public LeaderDecide_subPage(string id,string status,string companyName)
        {
            InitializeComponent();
            intial(id,status);
            this.companyName = companyName;
        }
        //领导决策，拒绝按钮
        private void RefuseButton_Click(object sender, EventArgs e)
        {
            
           int flag=  leadDecidebll.refuseSupplier(supplierID,user_ID,suggession.Text);
            if (flag == 1)
            {
                locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, companyName);
                DialogResult dr = MessageBox.Show("拒绝成功！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                this.Close();
            }
            else {

                DialogResult dr = MessageBox.Show("提交失败！请重试", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 查看文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button4_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            string filePath = this.supplierID + "//Leader//";

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    fileIDList.Add(filename);

                }

            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];

            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try
            {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }



            }
            catch (Exception)
            {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="id"></param>
        void intial(string id,string status) {
            
            this.agree_comboBox.Enabled = true;
            supplierID = id;

            DataTable decideInfoDt = leadDecidebll.getDecideInfoBySupplierIdAndUserId(user_ID, supplierID);
             backTag = (int)decideInfoDt.Rows[0]["backTag"];
             stepTag = (int)decideInfoDt.Rows[0]["stepTag"];
             statusEnd = (int)decideInfoDt.Rows[0]["statusEnd"];
             IsProcess = (int)decideInfoDt.Rows[0]["IsProcess"];
             IsAgree = decideInfoDt.Rows[0]["IsAgree"].ToString();
             this.MainAdviceLable.Text = decideInfoDt.Rows[0]["suggestion"].ToString();
            //this.suggession.Text = leadDecidebll.getSuggesstion(user_ID, supplierID).Rows[0][0].ToString();
            if (!decideInfoDt.Rows[0]["super_Suggstion"].ToString().Equals("")) {
                this.suggession.Text = decideInfoDt.Rows[0]["super_Suggstion"].ToString();
                suggession.Enabled = false;

            }
               

            //判断是否处理：0未处理，1已处理
            if (IsProcess == 1) {

                this.suggession.Enabled = false;
                this.agree_comboBox.Enabled = false;
                this.button1.Enabled = false;
                this.agree_comboBox.Text = IsAgree;
                RefuseButton.Enabled = false;

            }

            //获取基本信息
            DataTable dt = leadDecidebll.SelectById(id);
            if (dt != null) {
                this.LabelDBsCode.Text = dt.Rows[0][0].ToString();
                this.labelCompanyNAme.Text = dt.Rows[0][1].ToString();
                this.labelContactMan.Text = dt.Rows[0][2].ToString();
                this.LabelPosition.Text = dt.Rows[0][3].ToString();
                this.labelMobile.Text = dt.Rows[0][4].ToString();
                this.labelTelPhone.Text = dt.Rows[0][5].ToString();
                this.labelEmail.Text = dt.Rows[0][6].ToString();
                this.labelUrlAddress.Text = dt.Rows[0][7].ToString();
                this.labelAddress.Text = dt.Rows[0][8].ToString();
                this.labelPostCode.Text = dt.Rows[0][9].ToString();
                this.MtName = dt.Rows[0][13].ToString();
                //获取预评和现场评估状态，判断有没有经过现场评估
                this.tagThird = (int)dt.Rows[0][10];
                this.TagLocalEval = (int)dt.Rows[0][11];             
                         
            }
            getFile();
        }

        /// <summary>
        /// 显示文件
        /// </summary>
        private void getFile() {
            string[] filenames = fTPHelper.GetFileList(supplierID+"//Leader");
            if (filenames == null) return;
            for (int i = 0; i < filenames.Length; i++)
                this.checkedListBox1.Items.Add(filenames[i]);

        }


        private void button1_Click(object sender, EventArgs e)
        {

            int statu=0;
            //获取审批数据
           
            int flag = 0;
            //向上提交

           
            if (stepTag < statusEnd && backTag == 1)
            {
                //提交上级插入数据
                flag = leadDecidebll.insertDecideInfo(supplierID, suggession.Text, user_ID, stepTag + 1, statusEnd, agree_comboBox.Text.ToString());
            }
            //最高领导层
            if (stepTag == statusEnd && backTag == 1)
            {
                //给出决策意见
                IsAgree = "同意";
                flag = leadDecidebll.insertDecideInfoSubmitEnd(user_ID, backTag + 1, IsAgree, supplierID, suggession.Text);
                //如果是E类物料直接将后面的状态置为1
                //查找E类物料
                GeneralBLL gn = new GeneralBLL();
                DataTable dtName = gn.getEClassMtName();
                if (dtName.Rows.Count > 0)
                {
                    for (int i = 0; i < dtName.Rows.Count; i++)
                    {
                        if (MtName.Trim().Equals(dtName.Rows[i][0].ToString().Trim()))
                        {
                            leadDecidebll.updateConditionToPermission(supplierID);
                            
                        }
                    }
                }
            }
            if (flag > 0)
            {
                DialogResult dr = MessageBox.Show("提交成功！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, companyName);
                this.Close();
            }
            else {
                DialogResult dr = MessageBox.Show("提交失败！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                this.Close();
            }
            
        }

        /// <summary>
        /// 下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click_1(object sender, EventArgs e)
        {
            string mess = "请选择要下载的文件";

            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            BrowDialog.ShowDialog();
            string filePath = supplierID+"//Leader//";
            string pathname = BrowDialog.SelectedPath;
            int flag = 0;
            try
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        flag++;
                    }
                }
                if (flag > 0)
                {              
                    if (pathname != null)
                    {
                        progressDownLoad progressDownLoad = new progressDownLoad(pathname, filePath, checkedListBox1);
                        progressDownLoad.Show();
                        System.Threading.Thread.Sleep(1000);//暂停1秒
                        DialogResult dr = MessageBox.Show("下载成功", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    }
                    else
                    {
                        DialogResult dr = MessageBox.Show("请选择存储目录", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    }
                }
                else {

                    DialogResult dr = MessageBox.Show("请选择文件", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                }
 

            }
            catch (Exception ex)
            {
           
                DialogResult dr = MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            }
        }

       
    }
}
