﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;
//using His.WebService.Utility;
using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using Lib.SqlServerDAL;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL.CertificationProcess;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class EvaluateCertificate : Form
    {
        private string id;
        private string eid;
        string mtName;
        FTPHelper fTPHelper = new FTPHelper( "");
        public EvaluateCertificate(string id,string eid)
        {
            this.id = id;
            this.eid = eid;
            InitializeComponent();
        }

        private void EvaluateCertificate_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable lst = gn.GetAll(id);
            textBox1.Text = lst.Rows[0][2].ToString();
            textBox2.Text = lst.Rows[0][1].ToString();
            textBox3.Text = lst.Rows[0][8].ToString();
            textBox4.Text = lst.Rows[0][9].ToString();
            textBox5.Text = lst.Rows[0][11].ToString();
            textBox8.Text = lst.Rows[0][13].ToString();
            textBox7.Text = lst.Rows[0][10].ToString();
            textBox6.Text = lst.Rows[0][12].ToString();
            textBox10.Text = lst.Rows[0][7].ToString();
            textBox9.Text = lst.Rows[0][6].ToString();
            mtName = lst.Rows[0][16].ToString();
            showFiles(this.id + "/PreEvalFile" + "/supplier", LK_evalFile);


        }
        //显示文件信息
        private void showFiles(string filePath, LinkLabel linkLabel)
        {
            FTPHelper companyFileDownLoad = new FTPHelper(filePath);
            string[] filenames = companyFileDownLoad.GetFileList();
            string file = "";
            if (filenames != null)
            {
                for (int i = 0; i < filenames.Length; i++)
                {
                    file = filenames[i] + "\n";

                }
            }
            linkLabel.Text = file;


        }



        private void button3_Click(object sender, EventArgs e)//提交
        {
            string reason = richTextBox1.Text;
            string score = textScore.Text;
            string sum = textSum.Text;
            float a=0;
            if (int.Parse(score) < 0 || int.Parse(score) > 100 || int.Parse(sum) < 0 || int.Parse(sum) > 100 || int.Parse(score) > int.Parse(sum))
            { MessageUtil.ShowWarning("得分和总分都必须是0—100之间并且得分需要小于等于总分！请重新填写。"); }
            else
            {
                if (score == "" || sum == "")
                { MessageUtil.ShowWarning("请填写得分和总分！"); }
                else
                {
                    string zzid = textBox2.Text + "待预评审";
                    string kid = textBox2.Text + "预评审已完结待提交现场评审";
                    a =( float.Parse(score) / float.Parse(sum))*100;
                    textpercent.Text = a.ToString("F2");
                    string isno = textIsno.Text;
                    GeneralBLL gn = new GeneralBLL();
                    gn.deal(this.id, reason, score, sum, isno, this.eid);
                    gn.updateTask(this.eid,zzid);
                    if(gn.GetTaskStatus(zzid))
                    {
                        string zhuid = null;
                        DataTable dt = gn.Getzhuid(textBox2.Text + "待筛选");
                        zhuid = dt.Rows[0][0].ToString();
                        gn.adeletetask(zhuid, kid);
                        //查找E类物料
                        DataTable dtName  = gn.getEClassMtName();
                        if (dtName.Rows.Count>0)
                        {
                            for(int i = 0;i<dtName.Rows.Count;i++)
                            {
                                if(mtName.Trim().Equals(dtName.Rows[i][0].ToString().Trim()))
                                {
                                    string E= textBox2.Text + "预评审已完结待提交上级审批";
                                    string pid = gn.getid(zhuid).Rows[0][0].ToString();
                                    gn.inserttask(zhuid, E);
                                    MessageUtil.ShowWarning("处理完成！");
                                    return ;
                                }
                            }
                        }
                        gn.inserttask(zhuid, kid);
                    }
                    MessageUtil.ShowWarning("处理完成！");
                    return;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)//上传评估员评估报告
        {
            this.fTPHelper.MakeDir(this.id);
            string[] resultFile = null;
            FTPHelper fTPHelper1 = new FTPHelper(this.id);
            fTPHelper1.MakeDir("PreEvalFile");
            FTPHelper fTPHelper2 = new FTPHelper(this.id+ "/PreEvalFile");
            fTPHelper2.MakeDir("Evaluation");
            FTPHelper fTPHelper3 = new FTPHelper(this.id + "/PreEvalFile"+ "/Evaluation");
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            string filePath = "/"+this.id + "/PreEvalFile" + "/Evaluation";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;
                
            }
            if (resultFile == null)
                MessageBox.Show("未选择文件");
            else
            {
                for (int i = 0; i < resultFile.Length; i++) {

                    fTPHelper3.Upload(resultFile[i]);
                }
  
                MessageUtil.ShowWarning("上传成功！");
                string file = "";
               
                if (resultFile != null)
                {
                    for (int i = 0; i < resultFile.Length; i++)
                    {
                        string[] sArray5 = Regex.Split(resultFile[i], @"\\", RegexOptions.IgnoreCase);
                        FileUploadIDAL.InsertFileInfo(this.id, sArray5[sArray5.Length - 1], "Evaluation", SingleUserInstance.getCurrentUserInstance().Username, "评审员", filePath + "/");
                        file = file + ";" + sArray5[sArray5.Length-1];

                    }
                }
                filename1.Text = file;
            return;
            }
        }

      
        private void button4_Click(object sender, EventArgs e)//取消
        {
            
        }
        private string result(string a, string b)
        {
            double c = 0;

            if (isdouble(a) && isdouble(b))
            {
                if (b == "0")
                {
                    MessageBox.Show("被除数不能为0");
                    return "";
                }
               /* else if(int.Parse(a) < 0 || int.Parse(a) > 100 || int.Parse(b) < 0 || int.Parse(b) > 100 || int.Parse(a) > int.Parse(b))
                {
                    MessageBox.Show("得分和总分都必须是0—100之间并且得分需要小于等于总分！请重新填写。");
                    return "";
                }*/
                else
                {
                    c = (Convert.ToDouble(a) / Convert.ToDouble(b)*100);
                    return c.ToString("F2");
                }
            }
            else
            {
                MessageBox.Show("得分和总分的输入都必须是数字");
                return "";
            }

        }
        private bool isdouble(string a)
        {
            try
            {
                double b = 0;
                b = Convert.ToDouble(a);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void textScore_TextChanged(object sender, EventArgs e)
        {
            //this.textpercent.Text = result(this.textScore.Text, this.textSum.Text);

        }

        private void textSum_TextChanged(object sender, EventArgs e)
        {
            this.textpercent.Text = result(this.textScore.Text, this.textSum.Text);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LK_evalFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DownFile(this.id + "/PreEvalFile" + "/supplier");
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="filePath">文件所在目录</param>
        private void DownFile(string filePath)
        {
            FTPHelper companyFileDownLoad = new FTPHelper(filePath);
            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";
            if (BrowDialog.ShowDialog() == DialogResult.OK)
            {
                string pathname = BrowDialog.SelectedPath;
                string[] filenames = companyFileDownLoad.GetFileList();
                string file = "";
                if (filenames != null)
                {
                    for (int i = 0; i < filenames.Length; i++)
                    {
                        file = file + ";" + filenames[i];

                    }
                }
                if (filenames == null)
                    MessageUtil.ShowTips("未找到文件！");
                else
                {
                    try
                    {
                        if (pathname != null)
                        {
                            for (int i = 0; i < filenames.Length; i++)
                            {
                                fTPHelper.Download(pathname, filenames[i]);

                            }
                            MessageUtil.ShowTips("下载成功");

                        }

                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowTips(ex.Message);

                    }
                }
            }



        }
    }
}
