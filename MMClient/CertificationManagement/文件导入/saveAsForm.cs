﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using Microsoft.Office.Interop;

namespace MMClient.CertificationManagement.文件导入
{
    public partial class saveAsForm : DockContent
    {
        private string filepath;
        DataTable dataTable;
        CompanyUserBLL userListBLL = new CompanyUserBLL();
        public saveAsForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr) //判断是否选择文件  
            {
                this.textBox1.Text = this.openFileDialog1.FileName;
                string sql = "select * from Hanmote_Base_User";
                dataTable = DBHelper.ExecuteQueryDT(sql);
                DataSetToExcel(dataTable,filepath);
            }
           
        }
       
            /// <summary>  
            /// 数据库转为excel表格  
            /// </summary>  
            /// <param name="dataTable">数据库数据</param>  
            /// <param name="SaveFile">导出的excel文件</param>  
            public static void DataSetToExcel(DataTable dataTable, string SaveFile)
            {
                Microsoft.Office.Interop.Excel.Application excel;

                Microsoft.Office.Interop.Excel._Workbook workBook;

                Microsoft.Office.Interop.Excel._Worksheet workSheet;

                object misValue = System.Reflection.Missing.Value;


                excel = new Microsoft.Office.Interop.Excel.Application();
                 
                workBook = excel.Workbooks.Add(misValue);

                workSheet = (Microsoft.Office.Interop.Excel._Worksheet)workBook.ActiveSheet;

                int rowIndex = 1;

                int colIndex = 0;

                //取得标题  
                foreach (DataColumn col in dataTable.Columns)
                {
                    colIndex++;

                    excel.Cells[1, colIndex] = col.ColumnName;
                }

                //取得表格中的数据  
                foreach (DataRow row in dataTable.Rows)
                {
                    rowIndex++;

                    colIndex = 0;

                    foreach (DataColumn col in dataTable.Columns)
                    {
                        colIndex++;

                        excel.Cells[rowIndex, colIndex] =

                             row[col.ColumnName].ToString().Trim();

                        //设置表格内容居中对齐  
                        workSheet.get_Range(excel.Cells[rowIndex, colIndex],

                          excel.Cells[rowIndex, colIndex]).HorizontalAlignment =

                          Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    }
                }

                excel.Visible = false;

                workBook.SaveAs(SaveFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue,

                    misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,

                    misValue, misValue, misValue, misValue, misValue);

                dataTable = null;

                workBook.Close(true, misValue, misValue);

                excel.Quit();

                PublicMethod.Kill(excel);//调用kill当前excel进程  

                releaseObject(workSheet);

                releaseObject(workBook);

                releaseObject(excel);

            }

            private static void releaseObject(object obj)
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                    obj = null;
                }
                catch
                {
                    obj = null;
                }
                finally
                {
                    GC.Collect();
                }
            }
        }

        //关闭进程的类
        public class PublicMethod
        {
            [DllImport("User32.dll", CharSet = CharSet.Auto)]

            public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);

            public static void Kill(Microsoft.Office.Interop.Excel.Application excel)
            {
                try
                {
                    IntPtr t = new IntPtr(excel.Hwnd);

                    int k = 0;

                    GetWindowThreadProcessId(t, out k);

                    System.Diagnostics.Process p = System.Diagnostics.Process.GetProcessById(k);

                    p.Kill();
                }
                catch
                { }
            }
        }
    
}
