﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.CertificationManagement.文件导入
{
    public partial class ImportFileForm :  DockContent
    {
        public  string OledbConnString = "Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = {0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
        public ImportFileForm()
        {
            InitializeComponent();
        }

        private void ImportFileForm_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 选择文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileView_Click(object sender, EventArgs e)
        {
            DialogResult dr = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr) //判断是否选择文件  
            {
                this.fileName.Text = this.openFileDialog1.FileName;
                this.upload.Enabled = true;
            }
        }

        private void upload_Click(object sender, EventArgs e)
        {
            string path = this.fileName.Text.Trim();
            if (string.IsNullOrEmpty(path))
            {
                MessageBox.Show("请选择要导入的EXCEL文件。", "信息");
                return;
            }
            if (!File.Exists(path))  //判断文件是否存在  
            {
                MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                this.FileView.Focus();
                return;
            }
            DataTable excelTbl = this.GetExcelTable(path);  //调用函数获取Excel中的信息  
            if (excelTbl == null)
            {
                return;
            }

            ExcelFileTable.DataSource = excelTbl;


            for (int i = 0; i < ExcelFileTable.Rows.Count-1 ; i++)//保存创建数据
            {

                string sql2 = @"IF NOT EXISTS (SELECT * FROM  RecordInfo WHERE SupplierId='" + ExcelFileTable.Rows[i].Cells[0].Value + "' and CreateTime='" + ExcelFileTable.Rows[i].Cells[2].Value + "' and DemandCount='" + ExcelFileTable.Rows[i].Cells[3].Value + "' and NetPrice='" + ExcelFileTable.Rows[i].Cells[4].Value + "') BEGIN insert into RecordInfo (SupplierId, SupplierName, CreateTime, DemandCount, NetPrice) values('" + ExcelFileTable.Rows[i].Cells[0].Value + "', '" + ExcelFileTable.Rows[i].Cells[1].Value + "', '" + ExcelFileTable.Rows[i].Cells[2].Value + "', '" + ExcelFileTable.Rows[i].Cells[3].Value + "', '" + ExcelFileTable.Rows[i].Cells[4].Value + "'" + ");  END";


                DBHelper.ExecuteQueryDT(sql2);


            }
            MessageUtil.ShowWarning("导入成功！");


        }

        private DataTable GetExcelTable(string path)
        {
            try
            {
                //获取excel数据  
                DataTable dt1 = new DataTable("excelTable");
                if (!(path.ToLower().IndexOf(".xlsx") < 0))
                {
                    OledbConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
                }
                string strConn = string.Format(OledbConnString, path);
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable dt = conn.GetSchema("Tables");
                //判断excel的sheet页数量，查询第1页  
                if (dt.Rows.Count > 0)
                {
                    string selSqlStr = string.Format("select * from [{0}]", dt.Rows[0]["TABLE_NAME"]);
                    OleDbDataAdapter oleDa = new OleDbDataAdapter(selSqlStr, conn);
                    oleDa.Fill(dt1);
                }
                conn.Close();
                return dt1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel转换DataTable出错：" + ex.Message);
                return null;
            }
        }
        
    }
}
