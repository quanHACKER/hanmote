﻿namespace MMClient.CertificationManagement.文件导入
{
    partial class ImportFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExcelFileTable = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.upload = new System.Windows.Forms.Button();
            this.FileView = new System.Windows.Forms.Button();
            this.fileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExcelFileTable)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.ExcelFileTable);
            this.panel1.Location = new System.Drawing.Point(6, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(810, 543);
            this.panel1.TabIndex = 4;
            // 
            // ExcelFileTable
            // 
            this.ExcelFileTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExcelFileTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExcelFileTable.Location = new System.Drawing.Point(6, 15);
            this.ExcelFileTable.Name = "ExcelFileTable";
            this.ExcelFileTable.RowTemplate.Height = 23;
            this.ExcelFileTable.Size = new System.Drawing.Size(804, 500);
            this.ExcelFileTable.TabIndex = 3;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.upload);
            this.panel2.Controls.Add(this.FileView);
            this.panel2.Controls.Add(this.fileName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(6, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(810, 64);
            this.panel2.TabIndex = 6;
            // 
            // upload
            // 
            this.upload.Enabled = false;
            this.upload.Location = new System.Drawing.Point(580, 18);
            this.upload.Name = "upload";
            this.upload.Size = new System.Drawing.Size(108, 23);
            this.upload.TabIndex = 7;
            this.upload.Text = "导入";
            this.upload.UseVisualStyleBackColor = true;
            this.upload.Click += new System.EventHandler(this.upload_Click);
            // 
            // FileView
            // 
            this.FileView.Location = new System.Drawing.Point(430, 18);
            this.FileView.Name = "FileView";
            this.FileView.Size = new System.Drawing.Size(98, 23);
            this.FileView.TabIndex = 6;
            this.FileView.Text = "浏览 . . . .......";
            this.FileView.UseVisualStyleBackColor = true;
            this.FileView.Click += new System.EventHandler(this.FileView_Click);
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(140, 20);
            this.fileName.Name = "fileName";
            this.fileName.ReadOnly = true;
            this.fileName.Size = new System.Drawing.Size(238, 21);
            this.fileName.TabIndex = 5;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 23);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "选择文件";
            // 
            // ImportFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 654);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ImportFileForm";
            this.Text = "导入采购业务额";
            this.Load += new System.EventHandler(this.ImportFileForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExcelFileTable)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView ExcelFileTable;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button upload;
        private System.Windows.Forms.Button FileView;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Label label1;
    }
}