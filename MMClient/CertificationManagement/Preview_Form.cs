﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient
{
    public partial class Preview_Form : Form
    {
        string title;
        List<string> list;
        string[] mainStandard = { "公司概况", "管理1", "环境", "质量", "物流", "售后", "技术能力", "财务", "生产效率", "采购" };

        public Preview_Form()
        {
            InitializeComponent();
        }

        public Preview_Form(string title, List<string> list)
        {
            InitializeComponent();
            this.title = title;
            this.list = list;
           
        }
        private void Preview_Form_Load(object sender, EventArgs e)
        {
            //this.dgvShowTable.ReadOnly = true;  //设置为只读
            this.lblTitle.Text = this.title;
            //this.dgvShowTable.DataSource = ds.Tables[0].b
            this.LoadListView();
            this.BindDataToColumns(list);
        }
        /// <summary>  
        /// 初始化上传列表  
        /// </summary>  
        private void LoadListView()
        {
            listView1.View = View.Details;
            listView1.CheckBoxes = false;
            listView1.GridLines = true;
            listView1.Columns.Add("主标准", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("次标准", 100, HorizontalAlignment.Center);
            listView1.Columns.Add("否决项", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("3分", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("2分", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("1分", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("0分", 50, HorizontalAlignment.Center);
        }

        /// <summary>
        /// 将数据绑定到每一列上
        /// </summary>
        /// <param name="dt"></param>
        private void BindDataToColumns(List<string> list)
        {
            ListViewItem lvi = null;
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < mainStandard.Length; j++)
                {
                    if (list[i].Equals(mainStandard[j]))
                    {
                        lvi = new ListViewItem(list[i]);
                        break;
                    }
                }
            }

        }


    }
}
