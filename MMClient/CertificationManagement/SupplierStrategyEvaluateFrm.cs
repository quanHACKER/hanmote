﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using MMClient.Splash;

using DevComponents.DotNetBar;
namespace MMClient
{
    /// <summary>
    /// 供应商管理策略的评价窗口
    /// </summary>
    public partial class SupplierStrategyEvaluateFrm : Office2007Form,ISplashForm
    {
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column5;

        public SupplierStrategyEvaluateFrm()
        {
            InitializeComponent();
            
        }
        #region ISplashForm

        void ISplashForm.SetStatusInfo(string NewStatusInfo)
        {
            //this.supplierLabel.Text = NewStatusInfo;
        }

        #endregion
        public void InitColumns()
        {
            this.MaterialGroup = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewComboBoxColumn();

           
        }

        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            #region  添加列的别名
            //this.winGridView1.AddColumnAlias("MaterialGroup","物料项目");
            //this.winGridView1.AddColumnAlias("BusinessValue", "供应商营业额");
            //this.winGridView1.AddColumnAlias("Expenture", "采购支出项");
            //this.winGridView1.AddColumnAlias("Dependence", "依赖度");
            //this.winGridView1.AddColumnAlias("PIPLevel", "PIP等级");
            #endregion 

           
            
            //this.winGridView1.dataGridView1.DataSource = 
        }

        private void SupplierStrategyEvaluateFrm_Load(object sender, EventArgs e)
        {
            InitColumns();
            BindData();

            //AnitmateWindowUtil.AnimateOpen(this, this.timer1);
            
        }

        private DataTable GetDataTableStructure(Dictionary<string,Type> dic)
        {
            DataTable table = new DataTable();
            foreach (var each in dic)
            {
                table.Columns.Add(each.Key,each.Value);
            }

            return table;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            AnitmateWindowUtil.ChangeOpacity(this, this.timer1);
        }

        private void SupplierStrategyEvaluateFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AnitmateWindowUtil.AnimateClose(this);
        }
        

    }
}
