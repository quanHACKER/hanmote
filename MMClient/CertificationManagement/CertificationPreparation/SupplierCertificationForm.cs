﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll;
using MMClient.ContractManage;
using DevComponents.DotNetBar;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient
{
    public partial class SupplierCertificationForm : DockContent
    {
        static SupplierManagementBLL smbll = new SupplierManagementBLL();
        UserUI ui = null;

        //物料品项信息在数据库是否存在
        private bool extenseTypeExists = false;
        private string expensetype = null;

        DataTable supplierDataTable = smbll.GetAllSupplier();
        DataTable materialDT = smbll.GetAllMaterialGroup();
        public SupplierCertificationForm()
        {
            InitializeComponent();

        }
        public SupplierCertificationForm(UserUI ui)
        {
            InitializeComponent();
            this.ui = ui;
        }

        public ComboBox getMaterialCmb()
        {
            return this.materialGroup_cmb;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplierCertificationForm_Load(object sender, EventArgs e)
        {
            DataTable table = smbll.getFirstDegree();
            
            FormHelper.combox_bind(this.materialGroup_cmb, table, "Bigclassfy_Name", "Bigclassfy_Name");
            ////FormHelper.combox_bind(this.materialGroup_cmb, materialDT, "Material_Group", "Material_Group");
            string groupid = this.materialGroup_cmb.Text.ToString();
            DataTable materialTable = smbll.getMaterialName(groupid, "Material_Type");
            this.materialName_cmb.DataSource = null;
            FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name", "Material_Name");
            //DataTable dt = smbll.GetItemsInfoByMaterialGroupID(groupid);
            //if (dt == null || dt.Rows.Count == 0)
            //{
            //    //this.ListMaterialGgroup(groupid);
            //}
            //else
            //{
            //    //BindDatagridView(this.dataGridView1, dt);
            //} 
        }


        /// <summary>
        /// 给指定的列绑定数据
        /// </summary>
        public void BindDatagridView(DataGridView dgv,DataTable dt)
        {
            //设置不会自动生成列
            dgv.AutoGenerateColumns = false;
            //为每列指定字段名
            dgv.Columns[0].DataPropertyName = "";

        }


        /// <summary>
        /// 保存物料群组的物料类型A/B
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save_btn_Click(object sender, EventArgs e)
        {
            string groupid = this.materialGroup_cmb.SelectedValue.ToString();
            //获取物料组编号            
            DataTable dt = GetDataStructure();
            DataRow dr = null;
            for (int i = 0; i < this.dataGridView1.Rows.Count - 1; i++)
            {
                dr = dt.NewRow();
                if (this.qualitySpecail_ckb.Checked)
                {
                    dr["Supplier_ID"] = groupid;
                    dr["Expense_Type"] = this.dataGridView1.Rows[i].Cells[0].Value.ToString();
                    dr["PIP_Level"] = this.dataGridView1.Rows[i].Cells[6].Value;
                    dr["Supply_Type"] = this.dataGridView1.Rows[i].Cells[7].Value.ToString();
                    dr["Material_Group"] = groupid;
                }
                else
                {
                    dr["Supplier_ID"] = groupid;
                    dr["Expense_Type"] = this.dataGridView1.Rows[i].Cells[0].Value.ToString();
                    dr["Quailty_Score"] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells[1].Value);
                    dr["Cost_Score"] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells[2].Value.ToString());
                    dr["Deliver_Score"] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells[3].Value.ToString());
                    dr["Service_Score"] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells[4].Value.ToString());
                    dr["Total_Score"] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells[5].Value.ToString());
                    dr["PIP_Level"] = this.dataGridView1.Rows[i].Cells[6].Value;
                    dr["Supply_Type"] = this.dataGridView1.Rows[i].Cells[7].Value.ToString();
                    dr["Material_Group"] = groupid;
                }
                dt.Rows.Add(dr);
            }
            bool success = smbll.SetExpenseTypeByMaterialGroup(dt);
            if (success)
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
            }
        }

        private DataTable GetDataStructure()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Supplier_ID",typeof(string));
            dt.Columns.Add("Expense_Type", typeof(string));
            dt.Columns.Add("Quailty_Score", typeof(int));
            dt.Columns.Add("Cost_Score", typeof(int));
            dt.Columns.Add("Deliver_Score", typeof(int));
            dt.Columns.Add("Service_Score", typeof(int));
            dt.Columns.Add("Total_Score", typeof(int));
            dt.Columns.Add("PIP_Level", typeof(string));
            dt.Columns.Add("Supply_Type", typeof(string));
            dt.Columns.Add("Material_Group", typeof(string));

            return dt;
        }

        private void gysbh_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// 列出所有物料组
        /// </summary>
        /// <param name="supplierID"></param>
        private void ListMaterialGgroup(string groupid)
        {
            //禁止自动生成列
            //this.dataGridView1.AutoGenerateColumns = false;
            //根据供应商编号获取所有供应商的物料组  赋值给第一列    
            //this.dataGridView1.DataSource = smbll.GetAllMaterialGroupBySupplierID(groupid);
            //this.dataGridView1.Columns[0].DataPropertyName = "Material_Group";
        }
        /// <summary>
        /// 查看具体物料品项信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void seeExpenseTypeInfobtn_Click(object sender, EventArgs e)
        {
            //获取选中的物料组编号
            string groupid = this.materialGroup_cmb.SelectedValue.ToString();
            string title = groupid + "品项表";
            DataTable dt = smbll.GetItemsInfoByMaterialGroupID(groupid);
            if (dt == null || dt.Rows.Count == 0)
            {
                MessageBox.Show("数据为空");
                return;
            }
            ListSupplierExpenseTypeInfoForm listFrm = new ListSupplierExpenseTypeInfoForm(title, groupid, dt);
            listFrm.ShowDialog();
        }

        /// <summary>
        /// 显示认证评估模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showFrmBtn_Click(object sender, EventArgs e)
        {
            SetTemplateForm frm = new SetTemplateForm(this,this.expensetype);
            frm.FormBorderStyle = FormBorderStyle.None; // 无边框
            frm.TopLevel = false; // 不是最顶层窗体
            panel2.Controls.Add(frm);  // 添加到 Panel中
            frm.Show();     // 显示
        }
        
        /// <summary>
        /// 实现单击一次显示下拉列表框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn && e.RowIndex != -1)
            {
                //SendKeys.Send("{F4}");  //设置点击一次触发事件
                rowIndex = e.RowIndex;
                colIndex = e.ColumnIndex;
            }
        }

        int rowIndex = -1;
        int colIndex = -1;

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control.GetType() == typeof(DataGridViewComboBoxEditingControl))
            {
                ComboBox cbo = e.Control as ComboBox;
                cbo.SelectedIndexChanged -= new EventHandler(cbo_SelectedIndexChanged);
                cbo.SelectedIndexChanged += new EventHandler(cbo_SelectedIndexChanged);
            }
            if (e.Control.GetType() == typeof(DataGridViewTextBoxEditingControl))
            {
                TextBox tb = e.Control as TextBox;
                tb.TextChanged -= new EventHandler(tb_TextChanged);
                tb.TextChanged += new EventHandler(tb_TextChanged);
            }
            
        }

        private void tb_TextChanged(object sender, EventArgs e)
        {
            //得分改变
            //if (colIndex == 5)
            //{ 
                TextBox tb = sender as TextBox;
                this.dataGridView1.Rows[rowIndex].Cells[6].Value = Query(Convert.ToInt32(tb.Text));
                this.dataGridView1.Rows[rowIndex].Cells[5].Value = "";
            //}
        }
        private string Query(int score)
        {
            if (score >= 7) return "H";
            else if (score == 6) return "M";
            else if (score <= 5) return "L";
            else return "";
        }


        private void SetExpenseType(string text,int colIndex)
        {
            if (colIndex == 6)
            {
                if (this.dataGridView1.Rows[rowIndex].Cells[1].Value != null &&
                     text != null)
                {
                    string xStr = this.dataGridView1.Rows[rowIndex].Cells[0].Value.ToString().Trim();
                    string yStr = text.Trim();
                    if (xStr.Equals("A类"))
                    {
                        if (yStr.Equals("H") || yStr.Equals("M"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "关键";
                        }
                        if (yStr.Equals("L"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "杠杆";
                        }
                    }
                    else if (xStr.Equals("B类"))
                    {
                        if (yStr.Equals("H") || yStr.Equals("M"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "瓶颈";
                        }
                        if (yStr.Equals("L"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "一般";
                        }
                    }
                }
            }
            if (colIndex == 0)
            {
                if (this.dataGridView1.Rows[rowIndex].Cells[6].Value != null &&
                     text != null)
                {
                    string xStr = text.Trim();
                    string yStr = this.dataGridView1.Rows[rowIndex].Cells[6].Value.ToString().Trim();
                    if (xStr.Equals("A类"))
                    {
                        if (yStr.Equals("H") || yStr.Equals("M"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "关键";
                        }
                        if (yStr.Equals("L"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "杠杆";
                        }
                    }
                    else if (xStr.Equals("B类"))
                    {
                        if (yStr.Equals("H") || yStr.Equals("M"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "瓶颈";
                        }
                        if (yStr.Equals("L"))
                        {
                            this.dataGridView1.Rows[rowIndex].Cells[7].Value = "一般";
                        }
                    }
                }
            }
            this.expensetype = this.dataGridView1.Rows[rowIndex].Cells[7].Value == null ? null : this.dataGridView1.Rows[rowIndex].Cells[7].Value.ToString().Trim();
        }

        private void refreshData() 
        { 
            
        }

    
        private void cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combox = sender as ComboBox;
            if(colIndex>=1&&colIndex<=4)
                ComputeTotalScore(combox.Text);
            if (colIndex == 6||colIndex==0)
            {
                SetExpenseType(combox.Text,colIndex);
            }
        }

        private void ComputeTotalScore(string text)
        {
            
            int result = string.IsNullOrWhiteSpace(text) ? 0 : Convert.ToInt32(text);
            for (int i = 1; i < 5; i++)
            {
                if (!isNull(this.dataGridView1.Rows[rowIndex].Cells[i].Value) && i != colIndex)
                {
                    result += Convert.ToInt32(this.dataGridView1.Rows[rowIndex].Cells[i].Value.ToString());
                }
            }
            //设置总得分
            this.dataGridView1.Rows[rowIndex].Cells[5].Value = result.ToString();
            //string pip = this.dataGridView1.Rows[rowIndex].Cells[6].Value.ToString();
            //this.dataGridView1.Rows[rowIndex].Cells[7].Value = Query(Convert.ToInt32(pip));
            //DataGridViewComboBoxCell smb = (DataGridViewComboBoxCell)this.dataGridView1.Rows[rowIndex].Cells[7];
            //smb.DisplayMember = Query(Convert.ToInt32(pip));
        }

        private bool isNull(object obj)
        { 
            bool result = false;
            if (obj == null||string.IsNullOrWhiteSpace(obj.ToString()))
                result = true;
            return result;
        }

        private void materialGroup_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string groupid = this.materialGroup_cmb.SelectedValue.ToString();
            //DataTable dt = smbll.GetItemsInfoByMaterialGroupID(groupid);
            //if(dt != null && dt.Rows.Count > 0)
            //    this.dataGridView1.DataSource = dt;
            if(first_rb.Checked)
            {
                string groupid = this.materialGroup_cmb.Text.ToString();
                DataTable materialTable = smbll.getMaterialName(groupid, "Material_Type");
                this.materialName_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name", "Material_Name");
            }
            if(this.radioButton1.Checked)
            {
                string groupid = this.materialGroup_cmb.Text.ToString();
                DataTable materialTable = smbll.getMaterialName(groupid, "Type_Name");
                this.materialName_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name", "Material_Name");
            }
            if(this.radioButton2.Checked)
            {
                string groupid = this.materialGroup_cmb.Text.ToString();
                DataTable materialTable = smbll.getMaterialName(groupid, "Purpose");
                this.materialName_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name", "Material_Name");
            }
        }
        /// <summary>
        /// 物料组rb改变时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialgroup_rb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                this.unit_lbl.Text = "物料群组";
                this.materialGroup_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialGroup_cmb, materialDT, "Material_Group", "Material_Group");
            }

        }
        /// <summary>
        /// 物料类型rb改变时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialtype_rb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                this.unit_lbl.Text = "物料类型";
                if (this.first_rb.Checked)
                {
                    DataTable dt = smbll.getFirstDegree();
                    this.materialGroup_cmb.DataSource = null;
                    FormHelper.combox_bind(this.materialGroup_cmb, dt, "Bigclassfy_Name", "Bigclassfy_Name");
                }
                if (this.radioButton1.Checked)
                {
                    DataTable dt = smbll.getSecondDegree();
                    this.materialGroup_cmb.DataSource = null;
                    FormHelper.combox_bind(this.materialGroup_cmb, dt, "Littleclassfy_Name", "Littleclassfy_Name");
                }
                if (this.radioButton2.Checked)
                {
                    DataTable dt = smbll.getThirdDegree();
                    this.materialGroup_cmb.DataSource = null;
                    FormHelper.combox_bind(this.materialGroup_cmb, dt, "Type_Name", "Type_Name");
                }
            }
        }

        /// <summary>
        /// 一级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void first_rb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                DataTable dt = smbll.getFirstDegree();
                this.materialGroup_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialGroup_cmb, dt, "Bigclassfy_Name", "Bigclassfy_Name");
                //string groupid = this.materialGroup_cmb.SelectedValue.ToString();
                //DataTable materialTable = smbll.getMaterialName(groupid, "Material_Type");
                //this.materialName_cmb.DataSource = null;
                //FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name ", "Material_Name ");
            }
            
        }
        /// <summary>
        /// 二级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                DataTable dt = smbll.getSecondDegree();
                this.materialGroup_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialGroup_cmb, dt, "Littleclassfy_Name", "Littleclassfy_Name");
                //string groupid = this.materialGroup_cmb.SelectedValue.ToString();
                //DataTable materialTable = smbll.getMaterialName(groupid, "Type_Name");
                //this.materialName_cmb.DataSource = null;
                //FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name ", "Material_Name ");
            }
        }

        /// <summary>
        /// 三级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                DataTable dt = smbll.getThirdDegree();
                this.materialGroup_cmb.DataSource = null;
                FormHelper.combox_bind(this.materialGroup_cmb, dt, "Type_Name", "Type_Name");
                //string groupid = this.materialGroup_cmb.SelectedValue.ToString();
                //DataTable materialTable = smbll.getMaterialName(groupid, "Purpose");
                //this.materialName_cmb.DataSourceo = null;
                //FormHelper.combox_bind(this.materialName_cmb, materialTable, "Material_Name ", "Material_Name ");
            }
        }

        private void qualitySpecail_ckb_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            //如果选中
            if (cb.Checked)
            {
                this.dataGridView1.Rows[0].Cells[6].Value = "H";
                this.dataGridView1.Rows[0].Cells[1].ReadOnly = true;
                this.dataGridView1.Rows[0].Cells[2].ReadOnly = true;
                this.dataGridView1.Rows[0].Cells[3].ReadOnly = true;
                this.dataGridView1.Rows[0].Cells[4].ReadOnly = true;
                this.dataGridView1.Rows[0].Cells[5].ReadOnly = true;
            }
            else {
                this.dataGridView1.Rows[0].Cells[1].ReadOnly = false;
                this.dataGridView1.Rows[0].Cells[2].ReadOnly = false;
                this.dataGridView1.Rows[0].Cells[3].ReadOnly = false;
                this.dataGridView1.Rows[0].Cells[4].ReadOnly = false;
                this.dataGridView1.Rows[0].Cells[5].ReadOnly = false;
            }
        }

    }
}
