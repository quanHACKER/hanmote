﻿namespace MMClient
{
    partial class SupplierCertificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupplierCertificationForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.materialName_cmb = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.first_rb = new System.Windows.Forms.RadioButton();
            this.materialGroup_cmb = new System.Windows.Forms.ComboBox();
            this.unit_lbl = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.seeExpenseTypeInfobtn = new System.Windows.Forms.Button();
            this.save_btn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.c1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.c8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.showFrmBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.qualitySpecail_ckb = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.qualitySpecail_ckb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.materialName_cmb);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.materialGroup_cmb);
            this.panel1.Controls.Add(this.unit_lbl);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(15, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(966, 329);
            this.panel1.TabIndex = 155;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(602, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 165;
            this.label2.Text = "物料名称";
            // 
            // materialName_cmb
            // 
            this.materialName_cmb.FormattingEnabled = true;
            this.materialName_cmb.ItemHeight = 12;
            this.materialName_cmb.Location = new System.Drawing.Point(697, 10);
            this.materialName_cmb.Name = "materialName_cmb";
            this.materialName_cmb.Size = new System.Drawing.Size(200, 20);
            this.materialName_cmb.TabIndex = 164;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.radioButton2);
            this.panel4.Controls.Add(this.radioButton1);
            this.panel4.Controls.Add(this.first_rb);
            this.panel4.Location = new System.Drawing.Point(90, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(242, 32);
            this.panel4.TabIndex = 163;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(159, 10);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 16);
            this.radioButton2.TabIndex = 166;
            this.radioButton2.Text = "三级";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(82, 10);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(47, 16);
            this.radioButton1.TabIndex = 165;
            this.radioButton1.Text = "二级";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // first_rb
            // 
            this.first_rb.AutoSize = true;
            this.first_rb.Checked = true;
            this.first_rb.Location = new System.Drawing.Point(5, 10);
            this.first_rb.Name = "first_rb";
            this.first_rb.Size = new System.Drawing.Size(47, 16);
            this.first_rb.TabIndex = 164;
            this.first_rb.TabStop = true;
            this.first_rb.Text = "一级";
            this.first_rb.UseVisualStyleBackColor = true;
            this.first_rb.CheckedChanged += new System.EventHandler(this.first_rb_CheckedChanged);
            // 
            // materialGroup_cmb
            // 
            this.materialGroup_cmb.FormattingEnabled = true;
            this.materialGroup_cmb.ItemHeight = 12;
            this.materialGroup_cmb.Location = new System.Drawing.Point(358, 9);
            this.materialGroup_cmb.Name = "materialGroup_cmb";
            this.materialGroup_cmb.Size = new System.Drawing.Size(200, 20);
            this.materialGroup_cmb.TabIndex = 160;
            this.materialGroup_cmb.SelectedIndexChanged += new System.EventHandler(this.materialGroup_cmb_SelectedIndexChanged);
            // 
            // unit_lbl
            // 
            this.unit_lbl.AutoSize = true;
            this.unit_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.unit_lbl.Location = new System.Drawing.Point(18, 15);
            this.unit_lbl.Name = "unit_lbl";
            this.unit_lbl.Size = new System.Drawing.Size(53, 12);
            this.unit_lbl.TabIndex = 159;
            this.unit_lbl.Text = "物料类型";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.seeExpenseTypeInfobtn);
            this.panel3.Controls.Add(this.save_btn);
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(6, 94);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(969, 217);
            this.panel3.TabIndex = 158;
            // 
            // seeExpenseTypeInfobtn
            // 
            this.seeExpenseTypeInfobtn.Location = new System.Drawing.Point(881, 180);
            this.seeExpenseTypeInfobtn.Name = "seeExpenseTypeInfobtn";
            this.seeExpenseTypeInfobtn.Size = new System.Drawing.Size(75, 23);
            this.seeExpenseTypeInfobtn.TabIndex = 36;
            this.seeExpenseTypeInfobtn.Text = "查看";
            this.seeExpenseTypeInfobtn.UseVisualStyleBackColor = true;
            this.seeExpenseTypeInfobtn.Click += new System.EventHandler(this.seeExpenseTypeInfobtn_Click);
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(794, 180);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(75, 23);
            this.save_btn.TabIndex = 35;
            this.save_btn.Text = "保存";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.CausesValidation = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.c1,
            this.c2,
            this.c3,
            this.c4,
            this.c5,
            this.c6,
            this.c7,
            this.c8});
            this.dataGridView1.Location = new System.Drawing.Point(12, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(944, 148);
            this.dataGridView1.TabIndex = 34;
            this.dataGridView1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // c1
            // 
            this.c1.HeaderText = "采购支出类别";
            this.c1.Items.AddRange(new object[] {
            "A类",
            "B类"});
            this.c1.Name = "c1";
            // 
            // c2
            // 
            this.c2.HeaderText = "质量";
            this.c2.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.c2.Name = "c2";
            this.c2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // c3
            // 
            this.c3.HeaderText = "成本";
            this.c3.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.c3.Name = "c3";
            this.c3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // c4
            // 
            this.c4.HeaderText = "交付";
            this.c4.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.c4.Name = "c4";
            this.c4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // c5
            // 
            this.c5.HeaderText = "服务";
            this.c5.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.c5.Name = "c5";
            this.c5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // c6
            // 
            this.c6.HeaderText = "得分";
            this.c6.Name = "c6";
            // 
            // c7
            // 
            this.c7.HeaderText = "PIP综合等级";
            this.c7.Items.AddRange(new object[] {
            "H",
            "M",
            "L"});
            this.c7.Name = "c7";
            this.c7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // c8
            // 
            this.c8.HeaderText = "供应定位类型";
            this.c8.Items.AddRange(new object[] {
            "一般",
            "瓶颈",
            "杠杆",
            "关键"});
            this.c8.Name = "c8";
            this.c8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.c8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(179, 12);
            this.label6.TabIndex = 33;
            this.label6.Text = "物料归类->计算IOR值->品项定位";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(97, 14);
            this.label1.TabIndex = 156;
            this.label1.Text = "物料品项定位";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(12, 378);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 14);
            this.label3.TabIndex = 157;
            this.label3.Text = "确定评估模板";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Location = new System.Drawing.Point(12, 405);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(962, 272);
            this.panel2.TabIndex = 158;
            // 
            // showFrmBtn
            // 
            this.showFrmBtn.Location = new System.Drawing.Point(134, 375);
            this.showFrmBtn.Name = "showFrmBtn";
            this.showFrmBtn.Size = new System.Drawing.Size(75, 23);
            this.showFrmBtn.TabIndex = 159;
            this.showFrmBtn.Text = "设置模板";
            this.showFrmBtn.UseVisualStyleBackColor = true;
            this.showFrmBtn.Click += new System.EventHandler(this.showFrmBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(132, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(179, 12);
            this.label7.TabIndex = 38;
            this.label7.Text = "物料归类->计算IOR值->品项定位";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "采购支出类别";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "A类",
            "B类"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.ToolTipText = "采购支出类别";
            this.dataGridViewComboBoxColumn1.Width = 150;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "质量";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.ReadOnly = true;
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.ToolTipText = "质量";
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "成本";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.ReadOnly = true;
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "交付";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "服务";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "得分";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.HeaderText = "PIP综合等级";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "H",
            "M",
            "L"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.HeaderText = "供应定位类型";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "一般",
            "瓶颈",
            "杠杆",
            "关键"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.HeaderText = "采购支出类别";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "A类",
            "B类"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.HeaderText = "质量";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.HeaderText = "成本";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.HeaderText = "交付";
            this.dataGridViewComboBoxColumn11.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.HeaderText = "服务";
            this.dataGridViewComboBoxColumn12.Items.AddRange(new object[] {
            "3",
            "2",
            "1"});
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.HeaderText = "PIP综合等级";
            this.dataGridViewComboBoxColumn13.Items.AddRange(new object[] {
            "H",
            "M",
            "L"});
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            this.dataGridViewComboBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.HeaderText = "供应定位类型";
            this.dataGridViewComboBoxColumn14.Items.AddRange(new object[] {
            "一般",
            "瓶颈",
            "杠杆",
            "关键"});
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            this.dataGridViewComboBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // qualitySpecail_ckb
            // 
            this.qualitySpecail_ckb.AutoSize = true;
            this.qualitySpecail_ckb.Location = new System.Drawing.Point(18, 62);
            this.qualitySpecail_ckb.Name = "qualitySpecail_ckb";
            this.qualitySpecail_ckb.Size = new System.Drawing.Size(120, 16);
            this.qualitySpecail_ckb.TabIndex = 166;
            this.qualitySpecail_ckb.Text = "是否特殊质量特性";
            this.qualitySpecail_ckb.UseVisualStyleBackColor = true;
            this.qualitySpecail_ckb.CheckedChanged += new System.EventHandler(this.qualitySpecail_ckb_CheckedChanged);
            // 
            // SupplierCertificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(985, 736);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.showFrmBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SupplierCertificationForm";
            this.Text = "供应商认证前期准备";
            this.Load += new System.EventHandler(this.SupplierCertificationForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button seeExpenseTypeInfobtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button showFrmBtn;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ComboBox materialGroup_cmb;
        private System.Windows.Forms.Label unit_lbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn c1;
        private System.Windows.Forms.DataGridViewComboBoxColumn c2;
        private System.Windows.Forms.DataGridViewComboBoxColumn c3;
        private System.Windows.Forms.DataGridViewComboBoxColumn c4;
        private System.Windows.Forms.DataGridViewComboBoxColumn c5;
        private System.Windows.Forms.DataGridViewTextBoxColumn c6;
        private System.Windows.Forms.DataGridViewComboBoxColumn c7;
        private System.Windows.Forms.DataGridViewComboBoxColumn c8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton first_rb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox materialName_cmb;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.CheckBox qualitySpecail_ckb;
    }
}