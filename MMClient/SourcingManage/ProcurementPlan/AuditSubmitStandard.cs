﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class AuditSubmitStandard_Form : DockContent
    {
        public UserUI userUI;

        public AuditSubmitStandard_Form(UserUI userUi)
        {
            this.AutoScroll = true;
            InitializeComponent();
            this.userUI = userUi;
          
        }

        public void auditSubmitStandardByDemand_ID(DataTable dt)
        {
            this.name_label.Text = dt.Rows[0][2].ToString() + "采购需求计划审核";
            this.Text = "审核订单" + dt.Rows[0][0].ToString();
            this.xqdh_tb.Text = dt.Rows[0][0].ToString();
            this.wlbh_cmb.Text = dt.Rows[0][1].ToString();
            this.wlmc_cmb.Text = dt.Rows[0][2].ToString();
            this.cglx_cmb.Text = dt.Rows[0][3].ToString();
            this.wlfs_cmb.Text = dt.Rows[0][22].ToString();
            this.ckbh_cmb.Text = dt.Rows[0][5].ToString();
            this.jldw_tb.Text = dt.Rows[0][6].ToString();
            this.mindhpl_tb.Text = dt.Rows[0][7].ToString();
            this.xqsl_tb.Text = dt.Rows[0][8].ToString();
            this.sqbm_cmb.Text = dt.Rows[0][21].ToString();
            this.sqr_cmb.Text = dt.Rows[0][9].ToString();
            this.lxdh_tb.Text = dt.Rows[0][19].ToString();
            this.sqrq_dt.Text = dt.Rows[0][17].ToString();
            this.qtyq_rtb.Text = dt.Rows[0][12].ToString();

        }

        private void submit_bt_Click(object sender, EventArgs e)
        {
            AuditCommand auditCommand = new AuditCommand(this);
            auditCommand.Show();            
        }

        public void judgeAudit()
        {
            Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
            if (!(this.shzt_cmb.SelectedIndex==0 || this.shzt_cmb.SelectedIndex == 1))
            {
                MessageBox.Show("请审核采购计划！","提示");
                return;
            }
            if (this.shzt_cmb.SelectedIndex==0)
            {
                
                
                int done = newStandardDAL.changeState("审核通过",this.xqdh_tb.Text.ToString());
                if (done <= 0)
                {
                    MessageBox.Show("审核出现错误，请重新审核！");
                }
                else
                {
                    MessageBox.Show("审核通过！");
                    CreatePurchasing_Form createPurchasing_Form = new CreatePurchasing_Form(userUI);

                    //传递参数至计划订单模块
                    String item1 = null;
                    String item2 = null;
                    String item3 = null;
                    if (!string.IsNullOrWhiteSpace(this.cglx_cmb.Text))
                    {
                        item1 = " Purchase_Type = '" + this.cglx_cmb.Text + "'";
                    }
                    if (!string.IsNullOrWhiteSpace(this.xqdh_tb.Text))
                    {
                        item2 = " Demand_ID = '" + this.xqdh_tb.Text + "'";
                    }
                    if (!string.IsNullOrWhiteSpace(this.wlbh_cmb.Text))
                    {
                        item3 = " Material_ID = '" + this.wlbh_cmb.Text + "'";
                    }
                    createPurchasing_Form.DynamicDrawTable(item1, item2, item3, true);       
                    SingletonUserUI.addToUserUI(createPurchasing_Form);
                }

            }
            else if (this.shzt_cmb.SelectedIndex == 1)
            {
                int done = newStandardDAL.changeState("审核未通过",this.xqdh_tb.Text.ToString());
                if (done <= 0)
                {
                    MessageBox.Show("审核出现错误，请重新审核！");
                }
                else
                {
                    MessageBox.Show("审核未通过！");
                }
            }
            //审核时间
            newStandardDAL.changeReviewTime(DateTime.Now.ToString(), this.xqdh_tb.Text.ToString());
         }
            

        private void return_bt_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
