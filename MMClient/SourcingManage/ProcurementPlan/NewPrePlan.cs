﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class NewPrePlan_Form : DockContent
    {
        public NewPrePlan_Form()
        {
            InitializeComponent();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void cglx_cmb_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cglx_cmb.SelectedItem.Equals("1")) {
                panel2.Visible = true;
                panel3.Visible = false;
                panel4.Visible = false;
            }
            else if (cglx_cmb.SelectedItem.Equals("2"))
            {
                panel2.Visible = false;
                panel3.Visible = true;
                panel4.Visible = false;
            }
            else if (cglx_cmb.SelectedItem.Equals("3"))
            {
                panel2.Visible = false;
                panel3.Visible = false;
                panel4.Visible = true;
            }
            
        }

    }
}
