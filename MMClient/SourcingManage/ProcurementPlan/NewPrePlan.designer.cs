﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class NewPrePlan_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.lxdh_tb = new System.Windows.Forms.TextBox();
            this.xqsl_tb = new System.Windows.Forms.TextBox();
            this.mindhpl_tb = new System.Windows.Forms.TextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sqbm_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.jldw_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cqbh_cmb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.return_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(30, 24);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(231, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "新建需求计划(需求预测)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(379, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 58;
            this.label16.Text = "通过日期";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(29, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 57;
            this.label17.Text = "审核状态";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(89, 21);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(200, 20);
            this.comboBox3.TabIndex = 61;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(439, 21);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 21);
            this.dateTimePicker1.TabIndex = 148;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 80);
            this.panel1.TabIndex = 149;
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Location = new System.Drawing.Point(440, 51);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlfs_cmb.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(380, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cglx_cmb.Location = new System.Drawing.Point(90, 51);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 20);
            this.cglx_cmb.TabIndex = 30;
            this.cglx_cmb.SelectedValueChanged += new System.EventHandler(this.cglx_cmb_SelectedValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(30, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 11);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlmc_cmb.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(380, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 12;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 11);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlbh_cmb.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(30, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sqrq_dt);
            this.panel2.Controls.Add(this.lxdh_tb);
            this.panel2.Controls.Add(this.xqsl_tb);
            this.panel2.Controls.Add(this.mindhpl_tb);
            this.panel2.Controls.Add(this.qtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.sqr_cmb);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.sqbm_cmb);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.jldw_tb);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cqbh_cmb);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.xqdh_tb);
            this.panel2.Location = new System.Drawing.Point(8, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 280);
            this.panel2.TabIndex = 150;
            // 
            // sqrq_dt
            // 
            this.sqrq_dt.CustomFormat = "yyyy-MM-dd";
            this.sqrq_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sqrq_dt.Location = new System.Drawing.Point(440, 171);
            this.sqrq_dt.Name = "sqrq_dt";
            this.sqrq_dt.Size = new System.Drawing.Size(200, 21);
            this.sqrq_dt.TabIndex = 167;
            // 
            // lxdh_tb
            // 
            this.lxdh_tb.Location = new System.Drawing.Point(90, 171);
            this.lxdh_tb.Name = "lxdh_tb";
            this.lxdh_tb.Size = new System.Drawing.Size(200, 21);
            this.lxdh_tb.TabIndex = 166;
            // 
            // xqsl_tb
            // 
            this.xqsl_tb.Location = new System.Drawing.Point(440, 91);
            this.xqsl_tb.Name = "xqsl_tb";
            this.xqsl_tb.Size = new System.Drawing.Size(200, 21);
            this.xqsl_tb.TabIndex = 165;
            // 
            // mindhpl_tb
            // 
            this.mindhpl_tb.Location = new System.Drawing.Point(90, 91);
            this.mindhpl_tb.Name = "mindhpl_tb";
            this.mindhpl_tb.Size = new System.Drawing.Size(200, 21);
            this.mindhpl_tb.TabIndex = 164;
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Location = new System.Drawing.Point(90, 212);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(550, 60);
            this.qtyq_rtb.TabIndex = 163;
            this.qtyq_rtb.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(30, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(380, 175);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 161;
            this.label11.Text = "申请日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(30, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 160;
            this.label12.Text = "联系电话";
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.Location = new System.Drawing.Point(440, 131);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(200, 20);
            this.sqr_cmb.TabIndex = 159;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(392, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 158;
            this.label13.Text = "申请人";
            // 
            // sqbm_cmb
            // 
            this.sqbm_cmb.FormattingEnabled = true;
            this.sqbm_cmb.Location = new System.Drawing.Point(90, 131);
            this.sqbm_cmb.Name = "sqbm_cmb";
            this.sqbm_cmb.Size = new System.Drawing.Size(200, 20);
            this.sqbm_cmb.TabIndex = 157;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(30, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 156;
            this.label14.Text = "申请部门";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(380, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 155;
            this.label4.Text = "需求数量";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 154;
            this.label5.Text = "最小订货批量";
            // 
            // jldw_tb
            // 
            this.jldw_tb.FormattingEnabled = true;
            this.jldw_tb.Location = new System.Drawing.Point(440, 51);
            this.jldw_tb.Name = "jldw_tb";
            this.jldw_tb.Size = new System.Drawing.Size(200, 20);
            this.jldw_tb.TabIndex = 153;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(380, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 152;
            this.label8.Text = "计量单位";
            // 
            // cqbh_cmb
            // 
            this.cqbh_cmb.FormattingEnabled = true;
            this.cqbh_cmb.Location = new System.Drawing.Point(90, 51);
            this.cqbh_cmb.Name = "cqbh_cmb";
            this.cqbh_cmb.Size = new System.Drawing.Size(200, 20);
            this.cqbh_cmb.TabIndex = 151;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(30, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 150;
            this.label9.Text = "仓库编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(30, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 149;
            this.label10.Text = "需求单号";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Location = new System.Drawing.Point(90, 11);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(550, 21);
            this.xqdh_tb.TabIndex = 148;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dateTimePicker1);
            this.panel3.Controls.Add(this.comboBox3);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(8, 286);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(655, 55);
            this.panel3.TabIndex = 151;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cancel_bt);
            this.panel4.Controls.Add(this.return_bt);
            this.panel4.Controls.Add(this.submit_bt);
            this.panel4.Location = new System.Drawing.Point(8, 340);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(655, 60);
            this.panel4.TabIndex = 152;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            // 
            // return_bt
            // 
            this.return_bt.Location = new System.Drawing.Point(492, 15);
            this.return_bt.Name = "return_bt";
            this.return_bt.Size = new System.Drawing.Size(100, 30);
            this.return_bt.TabIndex = 48;
            this.return_bt.Text = "返回";
            this.return_bt.UseVisualStyleBackColor = true;
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(82, 15);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Location = new System.Drawing.Point(12, 130);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(674, 405);
            this.panel5.TabIndex = 153;
            // 
            // NewPrePlan_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(729, 745);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewPrePlan_Form";
            this.Text = "新建需求计划(需求预测)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox wlfs_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker sqrq_dt;
        private System.Windows.Forms.TextBox lxdh_tb;
        private System.Windows.Forms.TextBox xqsl_tb;
        private System.Windows.Forms.TextBox mindhpl_tb;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox sqbm_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox jldw_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cqbh_cmb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button return_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.Panel panel5;
    }
}