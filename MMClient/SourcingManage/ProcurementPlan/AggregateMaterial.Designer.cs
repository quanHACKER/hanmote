﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class AggregateMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.aggregateGridView = new System.Windows.Forms.DataGridView();
            this.num = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aggregateId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aggregateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_submit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_factoryID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_operator = new System.Windows.Forms.TextBox();
            this.lbl_StartTime = new System.Windows.Forms.Label();
            this.lbl_EndTime = new System.Windows.Forms.Label();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.submit = new System.Windows.Forms.Button();
            this.demandGridView = new System.Windows.Forms.DataGridView();
            this.demandId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proposer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.dmaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ddemandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dmeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aggregateGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.aggregateGridView);
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(993, 97);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "汇总信息";
            // 
            // aggregateGridView
            // 
            this.aggregateGridView.AllowUserToAddRows = false;
            this.aggregateGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.aggregateGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.aggregateGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aggregateGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num,
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.factoryId,
            this.demandCount,
            this.measurement,
            this.aggregateId,
            this.aggregateTime});
            this.aggregateGridView.EnableHeadersVisualStyles = false;
            this.aggregateGridView.Location = new System.Drawing.Point(6, 20);
            this.aggregateGridView.Name = "aggregateGridView";
            this.aggregateGridView.RowTemplate.Height = 23;
            this.aggregateGridView.Size = new System.Drawing.Size(981, 109);
            this.aggregateGridView.TabIndex = 0;
            this.aggregateGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.aggregateGridView_CellMouseEnter);
            this.aggregateGridView.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.aggregateGridView_CellMouseLeave);
            this.aggregateGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.aggregateGridView_RowPostPaint);
            // 
            // num
            // 
            this.num.HeaderText = "";
            this.num.Name = "num";
            this.num.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.num.Width = 40;
            // 
            // materialId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle1;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            this.materialName.Width = 150;
            // 
            // materialGroup
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle3;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // factoryId
            // 
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            this.factoryId.Width = 80;
            // 
            // demandCount
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.demandCount.DefaultCellStyle = dataGridViewCellStyle4;
            this.demandCount.HeaderText = "需求数量";
            this.demandCount.Name = "demandCount";
            this.demandCount.Width = 80;
            // 
            // measurement
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle5;
            this.measurement.HeaderText = "计量单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 80;
            // 
            // aggregateId
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.aggregateId.DefaultCellStyle = dataGridViewCellStyle6;
            this.aggregateId.HeaderText = "汇总单号";
            this.aggregateId.Name = "aggregateId";
            // 
            // aggregateTime
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.aggregateTime.DefaultCellStyle = dataGridViewCellStyle7;
            this.aggregateTime.HeaderText = "汇总时间";
            this.aggregateTime.Name = "aggregateTime";
            this.aggregateTime.Width = 150;
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(949, 193);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(56, 26);
            this.btn_submit.TabIndex = 2;
            this.btn_submit.Text = "寻源";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 23);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "工厂编号";
            // 
            // txt_factoryID
            // 
            this.txt_factoryID.Location = new System.Drawing.Point(77, 20);
            this.txt_factoryID.Name = "txt_factoryID";
            this.txt_factoryID.ReadOnly = true;
            this.txt_factoryID.Size = new System.Drawing.Size(100, 21);
            this.txt_factoryID.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(211, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "操作员";
            // 
            // txt_operator
            // 
            this.txt_operator.Location = new System.Drawing.Point(258, 20);
            this.txt_operator.Name = "txt_operator";
            this.txt_operator.ReadOnly = true;
            this.txt_operator.Size = new System.Drawing.Size(100, 21);
            this.txt_operator.TabIndex = 6;
            // 
            // lbl_StartTime
            // 
            this.lbl_StartTime.AutoSize = true;
            this.lbl_StartTime.Location = new System.Drawing.Point(20, 198);
            this.lbl_StartTime.Name = "lbl_StartTime";
            this.lbl_StartTime.Size = new System.Drawing.Size(53, 12);
            this.lbl_StartTime.TabIndex = 7;
            this.lbl_StartTime.Text = "开始时间";
            // 
            // lbl_EndTime
            // 
            this.lbl_EndTime.AutoSize = true;
            this.lbl_EndTime.Location = new System.Drawing.Point(256, 198);
            this.lbl_EndTime.Name = "lbl_EndTime";
            this.lbl_EndTime.Size = new System.Drawing.Size(53, 12);
            this.lbl_EndTime.TabIndex = 7;
            this.lbl_EndTime.Text = "结束时间";
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.Location = new System.Drawing.Point(79, 192);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.Size = new System.Drawing.Size(133, 21);
            this.dtp_startTime.TabIndex = 8;
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.Location = new System.Drawing.Point(334, 192);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.Size = new System.Drawing.Size(139, 21);
            this.dtp_endTime.TabIndex = 9;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(558, 193);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(64, 23);
            this.submit.TabIndex = 10;
            this.submit.Text = "确定";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // demandGridView
            // 
            this.demandGridView.AllowUserToAddRows = false;
            this.demandGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.demandGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.demandGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.demandGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.demandId,
            this.purchaseType,
            this.proposer,
            this.state,
            this.createTime});
            this.demandGridView.EnableHeadersVisualStyles = false;
            this.demandGridView.Location = new System.Drawing.Point(6, 14);
            this.demandGridView.Name = "demandGridView";
            this.demandGridView.RowTemplate.Height = 23;
            this.demandGridView.Size = new System.Drawing.Size(666, 150);
            this.demandGridView.TabIndex = 11;
            this.demandGridView.SelectionChanged += new System.EventHandler(this.demandGridView_SelectionChanged);
            // 
            // demandId
            // 
            this.demandId.HeaderText = "需求单号";
            this.demandId.Name = "demandId";
            // 
            // purchaseType
            // 
            this.purchaseType.HeaderText = "采购类型";
            this.purchaseType.Name = "purchaseType";
            // 
            // proposer
            // 
            this.proposer.HeaderText = "申请人";
            this.proposer.Name = "proposer";
            // 
            // state
            // 
            this.state.HeaderText = "状态";
            this.state.Name = "state";
            // 
            // createTime
            // 
            this.createTime.HeaderText = "申请日期";
            this.createTime.Name = "createTime";
            this.createTime.Width = 150;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.materialGridView);
            this.groupBox2.Location = new System.Drawing.Point(22, 416);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(770, 155);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dmaterialId,
            this.dMaterialName,
            this.ddemandCount,
            this.dmeasurement});
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(6, 20);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 23;
            this.materialGridView.Size = new System.Drawing.Size(745, 118);
            this.materialGridView.TabIndex = 0;
            // 
            // dmaterialId
            // 
            this.dmaterialId.HeaderText = "物料编号";
            this.dmaterialId.Name = "dmaterialId";
            // 
            // dMaterialName
            // 
            this.dMaterialName.HeaderText = "物料名称";
            this.dMaterialName.Name = "dMaterialName";
            // 
            // ddemandCount
            // 
            this.ddemandCount.HeaderText = "需求数量";
            this.ddemandCount.Name = "ddemandCount";
            // 
            // dmeasurement
            // 
            this.dmeasurement.HeaderText = "计量单位";
            this.dmeasurement.Name = "dmeasurement";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.demandGridView);
            this.groupBox3.Location = new System.Drawing.Point(26, 222);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(766, 170);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "需求信息";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 574);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "合计:";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(268, 574);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(11, 12);
            this.result.TabIndex = 15;
            this.result.Text = "0";
            // 
            // AggregateMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 619);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.dtp_endTime);
            this.Controls.Add(this.dtp_startTime);
            this.Controls.Add(this.lbl_EndTime);
            this.Controls.Add(this.lbl_StartTime);
            this.Controls.Add(this.txt_operator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_factoryID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AggregateMaterial";
            this.Text = "需求计划汇总";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aggregateGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView aggregateGridView;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_factoryID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_operator;
        private System.Windows.Forms.DataGridViewCheckBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn aggregateId;
        private System.Windows.Forms.DataGridViewTextBoxColumn aggregateTime;
        private System.Windows.Forms.Label lbl_StartTime;
        private System.Windows.Forms.Label lbl_EndTime;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.DataGridView demandGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dmaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ddemandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dmeasurement;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandId;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn proposer;
        private System.Windows.Forms.DataGridViewTextBoxColumn state;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label result;
    }
}