﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using MMClient.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class AggregateMaterial : DockContent
    {
        private UserUI userUI;
        private Summary_DemandBLL summaryDemandBll;
        private Demand_MaterialBLL demandMaterialBll;
        private Aggregate_MaterialBLL aggregateMaterialBll;

        //分散采购的工厂编号
        private string factory_Id;
        private Summary_Demand summaryDemand;
        public AggregateMaterial(UserUI userUI)
        {
            InitializeComponent();
            this.userUI = userUI;
            summaryDemandBll = new Summary_DemandBLL();
            demandMaterialBll = new Demand_MaterialBLL();
            aggregateMaterialBll = new Aggregate_MaterialBLL();
            initData();
            showData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            string item = "审核通过";
            //获取审核通过的需求单号
            List<string> demandIDs = summaryDemandBll.findReviewedDemands(item);
            //分散采购的工厂编号
            if (demandIDs != null)
            {
                factory_Id = summaryDemandBll.findSummaryDemandByDemandID(demandIDs.ElementAt(0)).Factory_ID;
            }
            //未汇总的需求-物料
            List<Demand_Material> demandMaterials = demandMaterialBll.findDemandMaterialNonAggregate(demandIDs);

            Dictionary<string, Aggregate_Material> map = new Dictionary<string, Aggregate_Material>();
            for (int i = 0; i < demandMaterials.Count; i++)
            {
                Demand_Material demandMaterial = demandMaterials.ElementAt(i);
                if (map.ContainsKey(demandMaterial.Material_ID))
                {
                    Aggregate_Material aggregateMaterial = map[demandMaterial.Material_ID];
                    aggregateMaterial.Aggregate_Count += demandMaterial.Demand_Count;
                }
                else
                {
                    Aggregate_Material aggregateMaterial = new Aggregate_Material();
                    aggregateMaterial.Aggregate_ID = i + "";
                    aggregateMaterial.Material_ID = demandMaterial.Material_ID;
                    aggregateMaterial.Material_Name = demandMaterial.Material_Name;
                    aggregateMaterial.Material_Group = demandMaterial.Material_Group;
                    aggregateMaterial.Factory_ID = factory_Id;
                    aggregateMaterial.Aggregate_Count = demandMaterial.Demand_Count;
                    aggregateMaterial.Measurement = demandMaterial.Measurement;
                    aggregateMaterial.Create_Time = DateTime.Now;
                    aggregateMaterial.Factory_ID = "";//工厂编号
                    map.Add(demandMaterial.Material_ID, aggregateMaterial);
                }
            }
            //处理map
            foreach (string key in map.Keys)
            {
                //汇总记录
                Aggregate_Material am = map[key];
                //插入汇总记录
                int result = aggregateMaterialBll.addAggregateMaterial(am);
                //根据物料编号更改汇总编号
                int suc = demandMaterialBll.updateDemandMaterialAggregateID(am, demandIDs);
                if (result > 0 && suc > 0)
                {
                    MessageBox.Show("汇总成功");
                }
                else
                {
                    MessageBox.Show("汇总失败");
                }
            }

        }

        /// <summary>
        /// 展示数据
        /// </summary>
        public void showData()
        {
            aggregateGridView.Rows.Clear();
            //获取所有的汇总信息
            List<Aggregate_Material> aggregateMaterials = aggregateMaterialBll.getAllAggregates();
            if (aggregateMaterials.Count > aggregateGridView.Rows.Count)
            {
                aggregateGridView.Rows.Add(aggregateMaterials.Count - aggregateGridView.Rows.Count);
            }
            for (int i = 0; i < aggregateMaterials.Count; i++)
            {
                Aggregate_Material am = aggregateMaterials.ElementAt(i);
                aggregateGridView.Rows[i].Cells["materialId"].Value = am.Material_ID;
                aggregateGridView.Rows[i].Cells["aggregateId"].Value = am.Aggregate_ID;
                aggregateGridView.Rows[i].Cells["materialName"].Value = am.Material_Name;
                aggregateGridView.Rows[i].Cells["materialGroup"].Value = am.Material_Group;
                aggregateGridView.Rows[i].Cells["factoryId"].Value = factory_Id;
                aggregateGridView.Rows[i].Cells["DemandCount"].Value = am.Aggregate_Count;
                aggregateGridView.Rows[i].Cells["measurement"].Value = am.Measurement;
                aggregateGridView.Rows[i].Cells["aggregateTime"].Value = am.Create_Time;
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aggregateGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 鼠标进入事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aggregateGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                aggregateGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                aggregateGridView.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 鼠标移出事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aggregateGridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                aggregateGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                aggregateGridView.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }
        
        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_submit_Click(object sender, EventArgs e)
        {
            List<Aggregate_Material> selectedAM = new List<Aggregate_Material>();
            foreach (DataGridViewRow row in aggregateGridView.Rows)
            {
                if (row.Cells["num"].Value != null)
                {
                    if (row.Cells["num"].Value.ToString() == "True")
                    {
                        Aggregate_Material am = new Aggregate_Material();
                        am.Aggregate_ID = row.Cells["aggregateId"].Value.ToString();
                        am.Material_ID = row.Cells["materialId"].Value.ToString();
                        am.Material_Name = row.Cells["materialName"].Value.ToString();
                        am.Material_Group = row.Cells["materialGroup"].Value.ToString();
                        am.Factory_ID = row.Cells["factoryId"].Value.ToString();
                        am.Aggregate_Count = Convert.ToInt32(row.Cells["DemandCount"].Value.ToString());
                        am.Measurement = row.Cells["measurement"].Value.ToString();
                        selectedAM.Add(am);
                    }
                }

            }
            if (selectedAM.Count == 0)
            {
                MessageBox.Show("至少有一条记录");
                return;
            }
            //Summary_Demand_Search sds = new Summary_Demand_Search(selectedAM);
            //SingletonUserUI.addToUserUI(sds);
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_Click(object sender, EventArgs e)
        {
            string startTime = dtp_startTime.Text.ToString();
            string endTime = dtp_endTime.Text.ToString();
            List<Summary_Demand> summaryDemandList = summaryDemandBll.findDemandsInLimitTime(startTime, endTime);
            demandGridView.Rows.Clear();
            if (summaryDemandList.Count > demandGridView.Rows.Count)
            {
                demandGridView.Rows.Add(summaryDemandList.Count - demandGridView.Rows.Count);
            }
            for (int i = 0; i < summaryDemandList.Count; i++)
            {
                Summary_Demand sd = summaryDemandList.ElementAt(i);
                demandGridView.Rows[i].Cells["demandId"].Value = sd.Demand_ID;
                demandGridView.Rows[i].Cells["purchaseType"].Value = sd.Purchase_Type;
                demandGridView.Rows[i].Cells["proposer"].Value = sd.Proposer_ID;
                demandGridView.Rows[i].Cells["state"].Value = sd.State;
                demandGridView.Rows[i].Cells["createTime"].Value = sd.Create_Time;
            }
            fillMaterialGridView();
        }

        /// <summary>
        /// 根据选中的需求计划展示该需求计划对应的所有物料信息
        /// </summary>
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            int index = demandGridView.CurrentRow.Index;
            Object obj = demandGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            String demandid = demandGridView.Rows[index].Cells[0].Value.ToString();
            String item1 = " Demand_ID = '" + demandid + "'";
            //根据选中行id查找对应的需求计划
            summaryDemand = summaryDemandBll.findSummaryDemandByDemandID(demandid);
            List<Demand_Material> demands = demandMaterialBll.findDemandMaterials(demandid);
            if (demands.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < demands.Count; i++)
            {
                materialGridView.Rows[i].Cells["dmaterialId"].Value = demands.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["dmaterialName"].Value = demands.ElementAt(i).Material_Name;
                
                materialGridView.Rows[i].Cells["ddemandCount"].Value = demands.ElementAt(i).Demand_Count;
                materialGridView.Rows[i].Cells["dmeasurement"].Value = demands.ElementAt(i).Measurement;
            }
            

        }

        private void demandGridView_SelectionChanged(object sender, EventArgs e)
        {
            fillMaterialGridView();
            addall();
        }

        private void addall()
        {
            //合计
            int sum = 0;
            foreach (DataGridViewRow row in materialGridView.Rows)
            {
                sum += Convert.ToInt32(row.Cells["ddemandCount"].Value.ToString());
            }
            result.Text = sum.ToString(); 
        }
    }
}
