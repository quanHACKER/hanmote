﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class NewFramework_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.kj_xylx_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.kj_xysl_tb = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.kj_wlph_tb = new System.Windows.Forms.TextBox();
            this.kj_wlbz_tb = new System.Windows.Forms.TextBox();
            this.kj_qqjg_tb = new System.Windows.Forms.TextBox();
            this.kj_wlgg_tb = new System.Windows.Forms.TextBox();
            this.kj_gysbh_tb = new System.Windows.Forms.TextBox();
            this.kj_gysmc_tb = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.kj_xybh_tb = new System.Windows.Forms.TextBox();
            this.kj_gysh_tb = new System.Windows.Forms.TextBox();
            this.kj_xymc_tb = new System.Windows.Forms.TextBox();
            this.kj_xqdh_tb = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.kj_qysj_dt = new System.Windows.Forms.DateTimePicker();
            this.label29 = new System.Windows.Forms.Label();
            this.kj_wlbh2_cmb = new System.Windows.Forms.ComboBox();
            this.kj_wlmc2_cmb = new System.Windows.Forms.ComboBox();
            this.kj_panel2 = new System.Windows.Forms.Panel();
            this.kj_panel4 = new System.Windows.Forms.Panel();
            this.kj_cancel_bt = new System.Windows.Forms.Button();
            this.kj_return_bt = new System.Windows.Forms.Button();
            this.kj_submit_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.kj_panel2.SuspendLayout();
            this.kj_panel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 80);
            this.panel1.TabIndex = 156;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Location = new System.Drawing.Point(440, 51);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlfs_cmb.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(380, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(90, 51);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 20);
            this.cglx_cmb.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(30, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 11);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlmc_cmb.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(380, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 12;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 11);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlbh_cmb.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(30, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(30, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 20;
            this.label10.Text = "需求单号";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(30, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 25;
            this.label9.Text = "协议编号";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(380, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 27;
            this.label8.Text = "协议名称";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(30, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 29;
            this.label5.Text = "签约时间";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(380, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "协议类型";
            // 
            // kj_xylx_cmb
            // 
            this.kj_xylx_cmb.FormattingEnabled = true;
            this.kj_xylx_cmb.Location = new System.Drawing.Point(440, 113);
            this.kj_xylx_cmb.Name = "kj_xylx_cmb";
            this.kj_xylx_cmb.Size = new System.Drawing.Size(200, 20);
            this.kj_xylx_cmb.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(30, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 33;
            this.label14.Text = "供应商号";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(380, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 35;
            this.label13.Text = "协议数量";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(18, 367);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 37;
            this.label12.Text = "供应商编号";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(368, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 39;
            this.label11.Text = "供应商名称";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(15, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 15);
            this.label16.TabIndex = 47;
            this.label16.Text = "协议信息";
            // 
            // kj_xysl_tb
            // 
            this.kj_xysl_tb.Location = new System.Drawing.Point(440, 153);
            this.kj_xysl_tb.Name = "kj_xysl_tb";
            this.kj_xysl_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_xysl_tb.TabIndex = 52;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(380, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 56;
            this.label22.Text = "物料名称";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(30, 262);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 57;
            this.label21.Text = "物料牌号";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(380, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 58;
            this.label20.Text = "物料规格";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(30, 302);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 60;
            this.label19.Text = "物料标准";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(380, 302);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 61;
            this.label18.Text = "前期价格";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(15, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 15);
            this.label17.TabIndex = 62;
            this.label17.Text = "物料信息";
            // 
            // kj_wlph_tb
            // 
            this.kj_wlph_tb.Location = new System.Drawing.Point(90, 258);
            this.kj_wlph_tb.Name = "kj_wlph_tb";
            this.kj_wlph_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_wlph_tb.TabIndex = 65;
            // 
            // kj_wlbz_tb
            // 
            this.kj_wlbz_tb.Location = new System.Drawing.Point(90, 298);
            this.kj_wlbz_tb.Name = "kj_wlbz_tb";
            this.kj_wlbz_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_wlbz_tb.TabIndex = 66;
            // 
            // kj_qqjg_tb
            // 
            this.kj_qqjg_tb.Location = new System.Drawing.Point(440, 298);
            this.kj_qqjg_tb.Name = "kj_qqjg_tb";
            this.kj_qqjg_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_qqjg_tb.TabIndex = 67;
            // 
            // kj_wlgg_tb
            // 
            this.kj_wlgg_tb.Location = new System.Drawing.Point(440, 258);
            this.kj_wlgg_tb.Name = "kj_wlgg_tb";
            this.kj_wlgg_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_wlgg_tb.TabIndex = 68;
            // 
            // kj_gysbh_tb
            // 
            this.kj_gysbh_tb.Location = new System.Drawing.Point(90, 363);
            this.kj_gysbh_tb.Name = "kj_gysbh_tb";
            this.kj_gysbh_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_gysbh_tb.TabIndex = 70;
            // 
            // kj_gysmc_tb
            // 
            this.kj_gysmc_tb.Location = new System.Drawing.Point(440, 363);
            this.kj_gysmc_tb.Name = "kj_gysmc_tb";
            this.kj_gysmc_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_gysmc_tb.TabIndex = 71;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(30, 422);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 76;
            this.label25.Text = "审核状态";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(380, 422);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 77;
            this.label15.Text = "通过日期";
            // 
            // kj_xybh_tb
            // 
            this.kj_xybh_tb.Location = new System.Drawing.Point(90, 72);
            this.kj_xybh_tb.Name = "kj_xybh_tb";
            this.kj_xybh_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_xybh_tb.TabIndex = 148;
            // 
            // kj_gysh_tb
            // 
            this.kj_gysh_tb.Location = new System.Drawing.Point(90, 152);
            this.kj_gysh_tb.Name = "kj_gysh_tb";
            this.kj_gysh_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_gysh_tb.TabIndex = 150;
            // 
            // kj_xymc_tb
            // 
            this.kj_xymc_tb.Location = new System.Drawing.Point(440, 72);
            this.kj_xymc_tb.Name = "kj_xymc_tb";
            this.kj_xymc_tb.Size = new System.Drawing.Size(200, 21);
            this.kj_xymc_tb.TabIndex = 151;
            // 
            // kj_xqdh_tb
            // 
            this.kj_xqdh_tb.Location = new System.Drawing.Point(90, 32);
            this.kj_xqdh_tb.Name = "kj_xqdh_tb";
            this.kj_xqdh_tb.Size = new System.Drawing.Size(550, 21);
            this.kj_xqdh_tb.TabIndex = 152;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(-1, 337);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 15);
            this.label24.TabIndex = 153;
            this.label24.Text = "供应商信息";
            // 
            // kj_qysj_dt
            // 
            this.kj_qysj_dt.CustomFormat = "yyyy-MM-dd mm";
            this.kj_qysj_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.kj_qysj_dt.Location = new System.Drawing.Point(90, 113);
            this.kj_qysj_dt.MaxDate = new System.DateTime(2016, 12, 5, 0, 0, 0, 0);
            this.kj_qysj_dt.Name = "kj_qysj_dt";
            this.kj_qysj_dt.Size = new System.Drawing.Size(200, 21);
            this.kj_qysj_dt.TabIndex = 154;
            this.kj_qysj_dt.Value = new System.DateTime(2016, 12, 5, 0, 0, 0, 0);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(30, 222);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 155;
            this.label29.Text = "物料编号";
            // 
            // kj_wlbh2_cmb
            // 
            this.kj_wlbh2_cmb.FormattingEnabled = true;
            this.kj_wlbh2_cmb.ItemHeight = 12;
            this.kj_wlbh2_cmb.Location = new System.Drawing.Point(90, 218);
            this.kj_wlbh2_cmb.Name = "kj_wlbh2_cmb";
            this.kj_wlbh2_cmb.Size = new System.Drawing.Size(200, 20);
            this.kj_wlbh2_cmb.TabIndex = 156;
            // 
            // kj_wlmc2_cmb
            // 
            this.kj_wlmc2_cmb.FormattingEnabled = true;
            this.kj_wlmc2_cmb.Location = new System.Drawing.Point(440, 218);
            this.kj_wlmc2_cmb.Name = "kj_wlmc2_cmb";
            this.kj_wlmc2_cmb.Size = new System.Drawing.Size(200, 20);
            this.kj_wlmc2_cmb.TabIndex = 158;
            // 
            // kj_panel2
            // 
            this.kj_panel2.Controls.Add(this.kj_wlmc2_cmb);
            this.kj_panel2.Controls.Add(this.kj_wlbh2_cmb);
            this.kj_panel2.Controls.Add(this.label29);
            this.kj_panel2.Controls.Add(this.kj_qysj_dt);
            this.kj_panel2.Controls.Add(this.label24);
            this.kj_panel2.Controls.Add(this.kj_xqdh_tb);
            this.kj_panel2.Controls.Add(this.kj_xymc_tb);
            this.kj_panel2.Controls.Add(this.kj_gysh_tb);
            this.kj_panel2.Controls.Add(this.kj_xybh_tb);
            this.kj_panel2.Controls.Add(this.label15);
            this.kj_panel2.Controls.Add(this.label25);
            this.kj_panel2.Controls.Add(this.kj_gysmc_tb);
            this.kj_panel2.Controls.Add(this.kj_gysbh_tb);
            this.kj_panel2.Controls.Add(this.kj_wlgg_tb);
            this.kj_panel2.Controls.Add(this.kj_qqjg_tb);
            this.kj_panel2.Controls.Add(this.kj_wlbz_tb);
            this.kj_panel2.Controls.Add(this.kj_wlph_tb);
            this.kj_panel2.Controls.Add(this.label17);
            this.kj_panel2.Controls.Add(this.label18);
            this.kj_panel2.Controls.Add(this.label19);
            this.kj_panel2.Controls.Add(this.label20);
            this.kj_panel2.Controls.Add(this.label21);
            this.kj_panel2.Controls.Add(this.label22);
            this.kj_panel2.Controls.Add(this.kj_xysl_tb);
            this.kj_panel2.Controls.Add(this.label16);
            this.kj_panel2.Controls.Add(this.label11);
            this.kj_panel2.Controls.Add(this.label12);
            this.kj_panel2.Controls.Add(this.label13);
            this.kj_panel2.Controls.Add(this.label14);
            this.kj_panel2.Controls.Add(this.kj_xylx_cmb);
            this.kj_panel2.Controls.Add(this.label4);
            this.kj_panel2.Controls.Add(this.label5);
            this.kj_panel2.Controls.Add(this.label8);
            this.kj_panel2.Controls.Add(this.label9);
            this.kj_panel2.Controls.Add(this.label10);
            this.kj_panel2.Location = new System.Drawing.Point(9, 7);
            this.kj_panel2.Name = "kj_panel2";
            this.kj_panel2.Size = new System.Drawing.Size(655, 395);
            this.kj_panel2.TabIndex = 157;
            this.kj_panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // kj_panel4
            // 
            this.kj_panel4.Controls.Add(this.kj_cancel_bt);
            this.kj_panel4.Controls.Add(this.kj_return_bt);
            this.kj_panel4.Controls.Add(this.kj_submit_bt);
            this.kj_panel4.Location = new System.Drawing.Point(9, 400);
            this.kj_panel4.Name = "kj_panel4";
            this.kj_panel4.Size = new System.Drawing.Size(655, 60);
            this.kj_panel4.TabIndex = 159;
            // 
            // kj_cancel_bt
            // 
            this.kj_cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.kj_cancel_bt.Name = "kj_cancel_bt";
            this.kj_cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_cancel_bt.TabIndex = 49;
            this.kj_cancel_bt.Text = "取消";
            this.kj_cancel_bt.UseVisualStyleBackColor = true;
            // 
            // kj_return_bt
            // 
            this.kj_return_bt.Location = new System.Drawing.Point(492, 15);
            this.kj_return_bt.Name = "kj_return_bt";
            this.kj_return_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_return_bt.TabIndex = 48;
            this.kj_return_bt.Text = "返回";
            this.kj_return_bt.UseVisualStyleBackColor = true;
            this.kj_return_bt.Click += new System.EventHandler(this.kj_return_bt_Click);
            // 
            // kj_submit_bt
            // 
            this.kj_submit_bt.Location = new System.Drawing.Point(82, 15);
            this.kj_submit_bt.Name = "kj_submit_bt";
            this.kj_submit_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_submit_bt.TabIndex = 47;
            this.kj_submit_bt.Text = "提交";
            this.kj_submit_bt.UseVisualStyleBackColor = true;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(34, 20);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(231, 19);
            this.label1.TabIndex = 160;
            this.label1.Text = "新建需求计划(框架协议)";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.kj_panel4);
            this.panel4.Controls.Add(this.kj_panel2);
            this.panel4.Location = new System.Drawing.Point(12, 146);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(675, 483);
            this.panel4.TabIndex = 161;
            // 
            // NewFramework_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 706);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewFramework_Form";
            this.Text = "新建需求计划（框架协议）";
            this.Load += new System.EventHandler(this.NewFramework_Form_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.kj_panel2.ResumeLayout(false);
            this.kj_panel2.PerformLayout();
            this.kj_panel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox wlfs_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox kj_xylx_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox kj_xysl_tb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox kj_wlph_tb;
        private System.Windows.Forms.TextBox kj_wlbz_tb;
        private System.Windows.Forms.TextBox kj_qqjg_tb;
        private System.Windows.Forms.TextBox kj_wlgg_tb;
        private System.Windows.Forms.TextBox kj_gysbh_tb;
        private System.Windows.Forms.TextBox kj_gysmc_tb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox kj_xybh_tb;
        private System.Windows.Forms.TextBox kj_gysh_tb;
        private System.Windows.Forms.TextBox kj_xymc_tb;
        private System.Windows.Forms.TextBox kj_xqdh_tb;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker kj_qysj_dt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox kj_wlbh2_cmb;
        private System.Windows.Forms.ComboBox kj_wlmc2_cmb;
        private System.Windows.Forms.Panel kj_panel2;
        private System.Windows.Forms.Panel kj_panel4;
        private System.Windows.Forms.Button kj_cancel_bt;
        private System.Windows.Forms.Button kj_return_bt;
        private System.Windows.Forms.Button kj_submit_bt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
    }
}