﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.MD.MT;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.MDBll.General;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class EditStandardDemand :DockContent
    {
        //主界面
        public UserUI userUI;
        //通用信息逻辑层(获取所有工厂编号)
        private GeneralBLL generalBll;
        //需求计划逻辑层
        private Summary_DemandBLL summaryDemandBll;
        //需求物料逻辑层
        private Demand_MaterialBLL demandMaterialBll;
        //需求计划
        private Summary_Demand summaryDemand;

        private List<string> midsList = new List<string>();

        public EditStandardDemand(UserUI userUI, Summary_Demand summaryDemand)
        {
            InitializeComponent();
            this.userUI = userUI;
            generalBll = new GeneralBLL();
            summaryDemandBll = new Summary_DemandBLL();
            demandMaterialBll = new Demand_MaterialBLL();
            this.summaryDemand = summaryDemand;
            materialGridView.TopLeftHeaderCell.Value = "序号";
            initFactoryID();
            initDemandData(summaryDemand);
            initMaterialData(summaryDemand.Demand_ID);
        }

        /// <summary>
        /// 初始化工厂编号
        /// </summary>
        private void initFactoryID()
        {
            List<string> factoryID = generalBll.GetAllFactory();
            cmb_FactoryID.DataSource = factoryID;
            cmb_FactoryID.Text = summaryDemand.Factory_ID;
        }

        /// <summary>
        /// 初始化需求计划数据
        /// </summary>
        /// <param name="summaryDemand"></param>
        private void initDemandData(Summary_Demand summaryDemand)
        {
            purchaseTypeCB.Text = summaryDemand.Purchase_Type;
            logisticsCB.Text = summaryDemand.LogisticsMode;
            demandIdText.Text = summaryDemand.Demand_ID;
            departmentCB.Text = summaryDemand.Department;
            txt_Proposer.Text = summaryDemand.Proposer_ID;
            phoneText.Text = summaryDemand.PhoneNum;
            txt_ApplyTime.Text = summaryDemand.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// 初始化物料信息
        /// </summary>
        private void initMaterialData(string demandId)
        {
            //根据需求编号查找对应的物料信息
            List<Demand_Material> list = demandMaterialBll.findDemandMaterials(demandId);
            materialGridView.Rows.Clear();
            if (list != null)
            {
                int i = 0;
                foreach (Demand_Material dm in list)
                {
                    materialGridView.Rows.Add();
                    materialGridView.Rows[i].Cells["materialId"].Value = dm.Material_ID;
                    midsList.Add(dm.Material_ID);
                    materialGridView.Rows[i].Cells["materialName"].Value = dm.Material_Name;
                    materialGridView.Rows[i].Cells["materialGroup"].Value = dm.Material_Group;
                    materialGridView.Rows[i].Cells["applyNum"].Value = dm.Demand_Count;
                    materialGridView.Rows[i].Cells["measurement"].Value = dm.Measurement;
                    materialGridView.Rows[i].Cells["stockId"].Value = dm.Stock_ID;
                    materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = dm.DeliveryStartTime.ToString("yyyy/MM/dd");
                    materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = dm.DeliveryEndTime.ToString("yyyy/MM/dd");
                    materialGridView.Rows[i].Cells["operation"].Value = "删除";
                    i++;
                }
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 点击DataGridView中的删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                int index = materialGridView.CurrentRow.Index;   
                string materialId = materialGridView.Rows[index].Cells["materialId"].Value.ToString();
                midsList.Remove(materialId);
                if (MessageBox.Show("是否确定删除？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int result = demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, materialId);
                    materialGridView.Rows.RemoveAt(index);
                    if (result > 0)
                    {                        
                        MessageBox.Show("删除成功");
                    }
                }  
            }
        }

        /// <summary>
        /// 全部删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_allDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("是否一并删除需求计划", "提示", MessageBoxButtons.YesNoCancel);
            if ( result== DialogResult.Yes)
            {
                for (int i = 0; i < materialGridView.Rows.Count; i++)
                {
                    string mid = materialGridView.Rows[i].Cells["materialId"].Value.ToString();
                    demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, mid);
                }
                materialGridView.Rows.Clear();
                summaryDemandBll.deleteSummaryDemandById(summaryDemand.Demand_ID);
                this.Close();
            }
            else if(result == DialogResult.No)
            {
                for (int i = 0; i < materialGridView.Rows.Count; i++)
                {
                    string mid = materialGridView.Rows[i].Cells["materialId"].Value.ToString();
                    demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, mid);
                }
                materialGridView.Rows.Clear();
            }     
        }

        /// <summary>
        /// 批量添加物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {
            List<string> limitIds = new List<string>();
            foreach(DataGridViewRow dr in materialGridView.Rows)
            {
                string mid = dr.Cells["materialId"].Value.ToString();
                limitIds.Add(mid);
            }
            EditMaterialInfo editMaterialInfo = null;
            if (editMaterialInfo == null || editMaterialInfo.IsDisposed)
            {
                editMaterialInfo = new EditMaterialInfo(this,limitIds);
            }
            editMaterialInfo.Show();
        }

        /// <summary>
        /// 增添新的物料
        /// </summary>
        /// <param name="materialList"></param>
        public void fillMaterialBases(List<MaterialBase> materialList)
        {
            if (materialList != null)
            {
                int i = materialGridView.Rows.Count;
                foreach (MaterialBase mb in materialList)
                {
                    materialGridView.Rows.Add();
                    materialGridView.Rows[i].Cells["materialId"].Value = mb.Material_ID;
                    materialGridView.Rows[i].Cells["materialName"].Value = mb.Material_Name;
                    materialGridView.Rows[i].Cells["materialGroup"].Value = mb.Material_Group;
                    materialGridView.Rows[i].Cells["measurement"].Value = mb.Measurement;
                    materialGridView.Rows[i].Cells["operation"].Value = "删除";
                    i++;
                }
            }
        }

        /// <summary>
        /// 检查按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (materialGridView.Rows.Count == 0)
            {
                MessageBox.Show("需求计划包含的物料数量为0,请至少添加一个物料","提示");
                return;
            }
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                if (dgvr.Cells["applyNum"].Value.ToString().Equals(""))
                {
                    MessageBox.Show("申请数量不能为空", "提示");
                    return;
                }
            }
            if (phoneText.Text.Equals(""))
            {
                MessageBox.Show("联系电话不能为空", "提示");
                return;
            }
            submitBtn.Visible = true;
            MessageBox.Show("申请信息无误");
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            //更改需求计划
            Summary_Demand demand = new Summary_Demand();
            demand.Demand_ID = demandIdText.Text;
            demand.Purchase_Type = purchaseTypeCB.Text;
            demand.LogisticsMode = logisticsCB.Text;
            demand.Department = departmentCB.Text;
            demand.Proposer_ID = txt_Proposer.Text;
            demand.PhoneNum = phoneText.Text;
            demand.Create_Time = DateTime.Parse(txt_ApplyTime.Text);
            demand.State = "待审核";
            int result = summaryDemandBll.UpdateSummaryDemand(demand);
            //更改需求计划对应的物料
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                Demand_Material demand_material = new Demand_Material();
                demand_material.Demand_ID = demandIdText.Text;
                demand_material.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                demand_material.Material_Name = dgvr.Cells["materialName"].Value.ToString();
                demand_material.Material_Group = dgvr.Cells["materialGroup"].Value.ToString();
                demand_material.Demand_Count = Convert.ToInt32(dgvr.Cells["applyNum"].Value.ToString());
                demand_material.Measurement = dgvr.Cells["measurement"].Value.ToString();
                demand_material.Stock_ID = dgvr.Cells["stockId"].Value.ToString();
                demand_material.Factory_ID = cmb_FactoryID.Text;
                demand_material.DeliveryStartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                demand_material.DeliveryEndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                //将需求计划添加到数据库
                if (midsList.Contains(demand_material.Material_ID))
                {
                    int ures = demandMaterialBll.updateSummaryMaterials(demand_material, demand_material.Material_ID);
                }
                else
                {
                    demandMaterialBll.addSummaryMaterials(demand_material);
                }
            }
            MessageBox.Show("需求计划修改成功","提示");
            this.Close();
        }
    }
}
