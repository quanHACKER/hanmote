﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.ProcurementPlan;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class NewStandard_Form : DockContent
    {
        //采购类型
        Summary_Demand summary_Demand = new Summary_Demand();
        //查询工具
        Summary_DemandBLL demandBLL = new Summary_DemandBLL();
        //控件工具
        ConvenientTools Tools = new ConvenientTools();

        public UserUI userUI;
        public DataTable checkDt = null;
        Boolean isEdit = false;

        public NewStandard_Form(UserUI userUi)
        {
            InitializeComponent();
            this.userUI = userUi;
            addItemsToCMB();          
        }

        /// <summary>
        /// combobox自动加载items
        /// </summary>
        public void addItemsToCMB()
        {
            //初始化物料编号下拉框
            Tools.addItemsToComboBox(wlbh_cmb,"", "","Material_ID", "Material_Type");
            //初始化物料名称下拉框
            Tools.addItemsToComboBox(wlmc_cmb, "", "", "Material_Name", "Material_Type");
            //初始化申请部门下拉框
            Tools.addItemsToComboBox(sqbm_cmb, "", "", "Department", "Buyer");
            //初始化申请人下拉框
            Tools.addItemsToComboBox(sqr_cmb, "", "", "Buyer_Name", "Buyer");
            //生成一个需求单号
            this.xqdh_tb.Text = Tools.systemTimeToStr();
        }

        private void cglx_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cglx_cmb.SelectedItem.Equals("标准采购类型"))
            {
                panel3.Visible = true;
                panel5.Visible = false;
            }
            else if (cglx_cmb.SelectedItem.Equals("框架协议采购类型"))
            {
                panel3.Visible = false;
                panel5.Visible = true;
            }
        }

        /// <summary>
        /// 保存新建标准需求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_bt_Click(object sender, EventArgs e)
        {
            String demand_ID = this.xqdh_tb.Text.Trim();
            //判断需求单号是否合法
            if(demand_ID.Equals("")){
                //不合法
                MessageBox.Show("需求单号不能为空！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //判断需求单号是否存在
            Boolean sameDemand_ID = false;
            sameDemand_ID = demandBLL.SameStandardByDemand_ID(demand_ID);
            if (sameDemand_ID)
            {
                if (!isEdit)
                {
                    MessageBox.Show("对应的需求单号已存在，系统将重新分配！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    int DemandID = Convert.ToInt32(demand_ID.ToString().Substring(1, demand_ID.ToString().Length - 4));
                    DemandID++;
                    this.xqdh_tb.Text = demand_ID.ToString().Substring(0, 1) + DemandID.ToString();
                    return;
                }
            }
            summary_Demand.Demand_ID = this.xqdh_tb.Text.Trim();
            summary_Demand.Material_ID = this.wlbh_cmb.Text.Trim();
            summary_Demand.Material_Name = this.wlmc_cmb.Text.Trim();
            summary_Demand.Purchase_Type = this.cglx_cmb.Text.Trim();

            DataTable dt = demandBLL.FindMaterial(this.wlbh_cmb.Text.Trim(), "Material_Type");
            //物料类型
            summary_Demand.Material_Type = dt.Rows[0][3].ToString();
            summary_Demand.LogisticsMode = this.wlfs_cmb.Text.ToString();
            summary_Demand.Stock_ID = this.ckbh_cmb.Text.Trim();
            summary_Demand.Factory_ID = this.jldw_tb.Text.Trim();
            summary_Demand.Mini_Purchase_Count = Convert.ToInt32(this.mindhpl_tb.Text.Trim());
            summary_Demand.Demand_Count = Convert.ToInt32(this.xqsl_tb.Text.Trim());
            summary_Demand.Proposer_ID = this.sqr_cmb.Text.Trim();
            summary_Demand.Supplier_ID = null;
            summary_Demand.Contract_ID = null;
            summary_Demand.PhoneNum = this.lxdh_tb.Text.Trim();
            summary_Demand.Department = this.sqbm_cmb.Text.Trim();
            summary_Demand.Remarks = this.qtyq_rtb.Text.Trim();
            summary_Demand.State = "待审核";
            //summary_Demand.Pass_Date = DateTime.Now;

            int result = 0;
            if (isEdit)
            {
                summary_Demand.Update_Time = DateTime.Now; //更改时间
                result = demandBLL.editNewStandard(summary_Demand);
            }
            else
            {
                summary_Demand.Create_Time = DateTime.Now;//创建时间
                result = demandBLL.addNewStandard(summary_Demand);
            }
            if (result > 0)
            {
                if (isEdit)
                {
                    MessageBox.Show("修改成功！");
                }
                else
                {
                    MessageBox.Show("保存成功！");
                }
                this.Text = "新建需求计划：" + this.wlmc_cmb.Text.Trim();
                String item1 = " Demand_ID = '" + this.xqdh_tb.Text.Trim() + "'";
                String item2 = null;
                String item3 = null;
                checkDt = demandBLL.StandardByWLBH(item1, item2, item3);
                if (checkDt == null || checkDt.Rows.Count == 0)
                {
                    MessageBox.Show("未找到相关记录，请重新查询选择！");
                    return;
                }
                AuditSubmitStandard_Form auditSubmitStandard_Form = new AuditSubmitStandard_Form(userUI);
                auditSubmitStandard_Form.auditSubmitStandardByDemand_ID(checkDt);
                SingletonUserUI.addToUserUI(auditSubmitStandard_Form);
            }
            else
            {
                MessageBox.Show("保存失败！");
            }
        }

        /// <summary>
        /// 根据物料编号新建采购计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void newStandardByMaterial_ID(DataTable dt,Boolean isFromDt)
        {
            //需求ID自动加1
            //DataTable id_dt = DemandTool.FindStandardDemand_ID();
            //if (id_dt == null || id_dt.Rows.Count <= 0)
            //{
            //    return;
            //}
            //int DemandID = Convert.ToInt32(id_dt.Rows[0][0].ToString().Substring(4, id_dt.Rows[0][0].ToString().Length - 4));
            //DemandID ++;
            String str = Tools.systemTimeToStr();

            if (isFromDt)
            {
                this.xqdh_tb.Text = str;
                this.Text = "新建需求计划：" + dt.Rows[0][2].ToString();
                this.wlbh_cmb.Text = dt.Rows[0][1].ToString();
                this.wlmc_cmb.Text = dt.Rows[0][2].ToString();
                this.cglx_cmb.Text = dt.Rows[0][3].ToString();
                this.wlfs_cmb.Text = dt.Rows[0][4].ToString();
                this.ckbh_cmb.Text = dt.Rows[0][5].ToString();
                this.jldw_tb.Text = dt.Rows[0][6].ToString();
                this.mindhpl_tb.Text = dt.Rows[0][7].ToString();
                this.xqsl_tb.Text = dt.Rows[0][8].ToString();
                this.sqbm_cmb.Text = dt.Rows[0][21].ToString();
                this.sqr_cmb.Text = dt.Rows[0][9].ToString();
                this.lxdh_tb.Text = dt.Rows[0][19].ToString();
                this.sqrq_dt.Text = null;
                this.qtyq_rtb.Text = dt.Rows[0][12].ToString();
            }
            else 
            {
                this.xqdh_tb.Text = str;
                this.wlbh_cmb.Text = "";
                this.wlmc_cmb.Text = "";
                this.cglx_cmb.Text = "";
                this.wlfs_cmb.Text = "";
                this.ckbh_cmb.Text = "";
                this.jldw_tb.Text = "";
                this.mindhpl_tb.Text = "";
                this.xqsl_tb.Text = "";
                this.sqbm_cmb.Text = "";
                this.sqr_cmb.Text = "";
                this.lxdh_tb.Text = "";
                this.sqrq_dt.Text = "";
                this.qtyq_rtb.Text = "";
            }
        }


        /// <summary>
        /// 根据需求单号修改采购计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void editStandardByDemand_ID(DataTable dt)
        {
            isEdit = true;
            this.Text = "修改单号"+ dt.Rows[0][0].ToString();
            this.xqdh_tb.Text = dt.Rows[0][0].ToString();
            this.wlbh_cmb.Text = dt.Rows[0][1].ToString();
            this.wlmc_cmb.Text = dt.Rows[0][2].ToString();
            this.cglx_cmb.Text = dt.Rows[0][3].ToString();
            this.wlfs_cmb.Text = dt.Rows[0][4].ToString();
            this.ckbh_cmb.Text = dt.Rows[0][5].ToString();
            this.jldw_tb.Text = dt.Rows[0][6].ToString();
            this.mindhpl_tb.Text = dt.Rows[0][7].ToString();
            this.xqsl_tb.Text = dt.Rows[0][8].ToString();
            this.sqbm_cmb.Text = dt.Rows[0][21].ToString();
            this.sqr_cmb.Text = dt.Rows[0][9].ToString();
            this.lxdh_tb.Text = dt.Rows[0][19].ToString();
            this.sqrq_dt.Text = dt.Rows[0][17].ToString();
            this.qtyq_rtb.Text = dt.Rows[0][12].ToString();
        }

        /// <summary>
        /// 清空需求数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancel_bt_Click(object sender, EventArgs e)
        {
            this.xqdh_tb.Text = "";
            this.wlbh_cmb.Text = "";
            this.wlmc_cmb.Text = "";
            this.cglx_cmb.Text = "";
            this.wlfs_cmb.Text = "";
            this.ckbh_cmb.Text = "";
            this.jldw_tb.Text = "";
            this.mindhpl_tb.Text = "";
            this.xqsl_tb.Text = "";
            this.sqbm_cmb.Text = "";
            this.sqr_cmb.Text = "";
            this.lxdh_tb.Text = "";
            this.sqrq_dt.Text = "";
            this.qtyq_rtb.Text = "";
        }

        private void return_bt_Click(object sender, EventArgs e)
        {
            this.Close();
            //datagridview View = new datagridview();
            //View.Show();
        }

        /// <summary>
        /// 物料编号改变触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wlbh_cmb_TextChanged(object sender, EventArgs e)
        {
            String wlbh = this.wlbh_cmb.Text.ToString().Trim();
            DataTable dt = demandBLL.FindMaterial(wlbh, "Material_Type");
            DataTable dt1 = demandBLL.FindStandardDemand_ID();
            if (dt == null || dt.Rows.Count <= 0)
            {
                return;
            }
            //String str = Tools.systemTimeToStr();
            //this.xqdh_tb.Text = dt1.Rows[0][0].ToString().Substring(0, 1) + str;
            this.wlmc_cmb.Text = dt.Rows[0][1].ToString();
            this.ckbh_cmb.Text = "";
            Tools.addItemsToComboBox(this.ckbh_cmb, wlbh, "Material_ID", "Stock_ID", "Inventory");
            this.jldw_tb.Text = dt.Rows[0][4].ToString();
            this.wlgg.Text = dt.Rows[0][5].ToString();
        }
        // 物料名称改变触发事件
        private void wlmc_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            String wlmc = this.wlmc_cmb.Text.ToString().Trim();
            this.wlbh_cmb.Text = "";
            Tools.addItemsToComboBox(wlbh_cmb, wlmc, "Material_Name", "Material_ID", "Material_Type");
        }
        // 申请部门改变触发事件
        private void sqbm_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sqbm = this.sqbm_cmb.Text.ToString().Trim();
            Tools.addItemsToComboBox(sqr_cmb, sqbm, "Department", "Buyer_Name", "Buyer");
        }
        // 申请人改变触发事件
        private void sqr_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sqr = this.sqr_cmb.Text.ToString().Trim();
            ComboBox lxdh_cmb = new ComboBox();
            Tools.addItemsToComboBox(lxdh_cmb, sqr, "Buyer_Name", "Department", "Buyer");
            this.sqbm_cmb.Text = lxdh_cmb.Text.ToString().Trim();
            Tools.addItemsToComboBox(lxdh_cmb, sqr, "Buyer_Name", "Material_ID", "Buyer");
            this.lxdh_tb.Text = lxdh_cmb.Text.ToString().Trim();
        }
        //物料编号items复原
        private void label2_Click(object sender, EventArgs e)
        {
            Tools.addItemsToComboBox(wlbh_cmb, "", "", "Material_ID", "Material_Type");
        }
        //申请人items复原
        private void label13_Click(object sender, EventArgs e)
        {
            Tools.addItemsToComboBox(sqr_cmb, "", "", "Buyer_Name", "Buyer");
        }


        //选择日期写入sqrq的label
        private void sqrq_dt_ValueChanged(object sender, EventArgs e)
        {

            this.sqrq.Text = this.sqrq_dt.Value.ToShortDateString();
            /*
            if (this.sqrq_dt.Value.ToShortDateString().Equals(DateTime.Now.ToShortDateString()))
            {
                this.sqrq.Text = "";
            }
            else
            {
                this.sqrq.Text = this.sqrq_dt.Value.ToString();
            }
             */
        }

        private void kj_return_bt_Click(object sender, EventArgs e)
        {

        }

        


    }
}
