﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.MD.MT;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.MDBll.General;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class NewSummaryDemand : DockContent
    {
        //主界面
        public UserUI userUI;
        //工具类
        private ConvenientTools Tools = new ConvenientTools();

        private Summary_DemandBLL demandBll = new Summary_DemandBLL();
        private GeneralBLL generallBll = new GeneralBLL();

        public NewSummaryDemand(UserUI userUI)
        {
            InitializeComponent();
            this.userUI = userUI;
            initData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            //初始化需求单号
            demandIdText.Text = Tools.systemTimeToStr();
            materialGridView.TopLeftHeaderCell.Value = "序号";
            //初始化工厂编号
            initFactory();
        }

        private void initFactory()
        {
            List<string> factoryID = generallBll.GetAllFactory();
            cmb_factory.DataSource = factoryID;
            
        }


        /// <summary>
        /// 批量添加物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {
            MaterialInfo materialInfo = null;
            if (materialInfo == null|| materialInfo.IsDisposed)
            {
                materialInfo = new MaterialInfo(this);
            }
            materialInfo.Show();
        }

        //画行号
        private void materialGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 添加物料信息
        /// </summary>
        /// <param name="materialList"></param>
        public void fillMaterialBases(List<MaterialBase> materialList)
        {
            materialGridView.Rows.Clear();
            if (materialList != null)
            {
                int i = 0;
                foreach (MaterialBase mb in materialList)
                {
                    materialGridView.Rows.Add();
                    materialGridView.Rows[i].Cells["materialId"].Value = mb.Material_ID;
                    materialGridView.Rows[i].Cells["materialName"].Value = mb.Material_Name;
                    materialGridView.Rows[i].Cells["materialGroup"].Value = mb.Material_Group;
                    materialGridView.Rows[i].Cells["measurement"].Value = mb.Measurement;
                    i++;
                }
            }
        }

        /// <summary>
        /// 检查按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBtn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                if (dgvr.Cells["applyNum"].Value.ToString().Equals(""))
                {
                    MessageBox.Show("申请数量不能为空", "提示");
                    return;
                }
            }
            if (phoneText.Text.Equals(""))
            {
                MessageBox.Show("联系电话不能为空","提示");
                return;
            }
            submitBtn.Visible = true;
            MessageBox.Show("申请信息无误");
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            Summary_Demand demand = new Summary_Demand();
            Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
            demand.Demand_ID = demandIdText.Text;
            demand.Purchase_Type = purchaseTypeCB.Text;
            demand.LogisticsMode = logisticsCB.Text;
            demand.Department = departmentCB.Text;
            demand.Proposer_ID = txt_Proposer.Text;
            demand.PhoneNum = phoneText.Text;
            demand.Create_Time = DateTime.Parse(dtp_apply.Text);
            demand.Factory_ID = cmb_factory.Text;
            demand.State = "待审核";
            demandBll.addNewStandard(demand);
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                Demand_Material demand_material = new Demand_Material();
                demand_material.Demand_ID = demandIdText.Text;
                demand_material.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                demand_material.Material_Name = dgvr.Cells["materialName"].Value.ToString();
                demand_material.Material_Group = dgvr.Cells["materialGroup"].Value.ToString();
                demand_material.Demand_Count = Convert.ToInt32(dgvr.Cells["applyNum"].Value.ToString());
                demand_material.Measurement = dgvr.Cells["measurement"].Value.ToString();
                demand_material.Stock_ID = dgvr.Cells["stockId"].Value.ToString();
                demand_material.Factory_ID = cmb_factory.Text;
                demand_material.DeliveryStartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                demand_material.DeliveryEndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                //将需求计划添加到数据库
                demandMaterialBll.addSummaryMaterials(demand_material);
            }
            MessageBox.Show("需求计划新建成功","提示");
            this.Close();
        }

        private void chk_DeliveryTime_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_DeliveryTime.CheckState==CheckState.Checked)
            {
                for (int i = 1; i < materialGridView.Rows.Count; i++)
                {
                    materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = DateTime.Parse(materialGridView.Rows[0].Cells["DeliveryStartTime"].Value.ToString());
                    materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = DateTime.Parse(materialGridView.Rows[0].Cells["DeliveryEndTime"].Value.ToString());
                }
            }
            if (chk_DeliveryTime.CheckState == CheckState.Unchecked)
            {
                for (int i = 1; i < materialGridView.Rows.Count; i++)
                {
                    materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = null;
                    materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = null;
                }
            }
        }
    }
}
