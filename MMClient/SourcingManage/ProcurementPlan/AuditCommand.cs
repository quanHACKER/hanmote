﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class AuditCommand : DockContent
    {
        private AuditSubmitStandard_Form AuditSubmitStandard;
        public AuditCommand(AuditSubmitStandard_Form auditSubmitStandard)
        {
            InitializeComponent();
            this.AuditSubmitStandard = auditSubmitStandard;
        }

        private void submit_bt_Click(object sender, EventArgs e)
        {
            
            if (this.shzh_tb.Text.Trim().Equals("admin") && this.shkl_tb.Text.Trim().Equals("123456"))
            {
                this.Close();
                AuditSubmitStandard.judgeAudit();
            }
            else {
                MessageBox.Show("口令输入错误,审核未完成！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cancel_bt_Click(object sender, EventArgs e)
        {
            MessageBox.Show("审核未完成！");
            this.Close(); 
        }
    }
}
