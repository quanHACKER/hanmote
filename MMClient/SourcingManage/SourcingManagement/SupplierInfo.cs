﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SupplierInfo : Form
    {
        private GeneralBLL generalBll;
        private Inquiry_Form inquiry_Form;
        public SupplierInfo():this(null)
        {
        }

        public SupplierInfo(Inquiry_Form inquiry_Form)
        {
            InitializeComponent();
            generalBll = new GeneralBLL();
            this.inquiry_Form = inquiry_Form;
            dgv_suppliers.TopLeftHeaderCell.Value = "序号";
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            dgv_suppliers.Rows.Clear();
            List<SupplierBase> supplers = generalBll.GetAllSuppliers();
            if (supplers != null)
            {
                int i = 0;
                foreach (SupplierBase spb in supplers)
                {
                    dgv_suppliers.Rows.Add(1);
                    dgv_suppliers.Rows[i].Cells["supplierId"].Value = spb.Supplier_ID;
                    dgv_suppliers.Rows[i].Cells["supplierName"].Value = spb.Supplier_Name;
                    dgv_suppliers.Rows[i].Cells["supplierPhone"].Value = spb.Mobile_Phone1;
                    dgv_suppliers.Rows[i].Cells["email"].Value = spb.Email;
                    dgv_suppliers.Rows[i].Cells["country"].Value = spb.Nation;
                    i++;
                }
            }
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            List<string> supplierIDList = new List<string>();
            foreach (DataGridViewRow dgvr in dgv_suppliers.Rows)
            {
                if (dgvr.Selected)
                {
                    supplierIDList.Add(dgvr.Cells["supplierId"].Value.ToString());
                }
            }
            inquiry_Form.fillSupplierBases(supplierIDList);
            this.Close();
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_suppliers_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        
    }
}
