﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ShowTransact_Form : DockContent
    {
        public SourceBid bid;

        private SourceBLL sourceBll = new SourceBLL();
        private SourceBidBLL bidBll = new SourceBidBLL();

        public ShowTransact_Form()
        {
            InitializeComponent();
            initData();
        }

        private void initData()
        {
            sourceBidGridView.Rows.Clear();
            fillBidGridView(getIntByState("待谈判"));
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            sourceBidGridView.Rows.Clear();
            string state = cmb_TransactState.Text;
            int intState = getIntByState(state);
            fillBidGridView(intState);
        }

        private void fillBidGridView(int state)
        {
            List<SourceBid> list = bidBll.getSourceBidsByState(state);
            if (list.Count > sourceBidGridView.Rows.Count)
            {
                sourceBidGridView.Rows.Add(list.Count - sourceBidGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                sourceBidGridView.Rows[i].Cells["bidId"].Value = list.ElementAt(i).BidId;
                Source source = sourceBll.findSourceById(list.ElementAt(i).BidId);
                sourceBidGridView.Rows[i].Cells["bidName"].Value = source.Source_Name;
                SourceBid sb = bidBll.findBidByBidId(list.ElementAt(i).BidId);
                sourceBidGridView.Rows[i].Cells["createTime"].Value = sb.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
                sourceBidGridView.Rows[i].Cells["bidState"].Value = getStringByNumber(list.ElementAt(i).BidState);
            }
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待谈判"))
            {
                res = 4;
            }
            else if (state.Equals("谈判中"))
            {
                res = 5;
            }

            return res;
        }

        private string getStringByNumber(int num)
        {
            string str = null;
            if (num == 4)
            {
                str = "待谈判";
            }
            if (num == 5)
            {
                str = "谈判中";
            }
            return str;
        }

        /// <summary>
        /// 选中项改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sourceBidGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = sourceBidGridView.CurrentRow.Index;
            Object obj = sourceBidGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            //根据选中行id查找对应的需求计划
            bid = bidBll.findBidByBidId(obj.ToString());
        }
    }
}
