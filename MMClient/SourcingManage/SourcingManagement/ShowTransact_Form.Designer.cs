﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ShowTransact_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_TransactState = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sourceBidGridView = new System.Windows.Forms.DataGridView();
            this.bidId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_show = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceBidGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(251, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "谈判状态";
            // 
            // cmb_TransactState
            // 
            this.cmb_TransactState.FormattingEnabled = true;
            this.cmb_TransactState.Items.AddRange(new object[] {
            "待谈判",
            "谈判中"});
            this.cmb_TransactState.Location = new System.Drawing.Point(325, 29);
            this.cmb_TransactState.Name = "cmb_TransactState";
            this.cmb_TransactState.Size = new System.Drawing.Size(121, 20);
            this.cmb_TransactState.TabIndex = 1;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.sourceBidGridView);
            this.groupBox1.Location = new System.Drawing.Point(77, 73);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(815, 281);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "谈判列表";
            // 
            // sourceBidGridView
            // 
            this.sourceBidGridView.AllowUserToAddRows = false;
            this.sourceBidGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sourceBidGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bidId,
            this.bidName,
            this.createTime,
            this.bidState});
            this.sourceBidGridView.Location = new System.Drawing.Point(6, 20);
            this.sourceBidGridView.Name = "sourceBidGridView";
            this.sourceBidGridView.RowTemplate.Height = 23;
            this.sourceBidGridView.Size = new System.Drawing.Size(733, 231);
            this.sourceBidGridView.TabIndex = 0;
            this.sourceBidGridView.SelectionChanged += new System.EventHandler(this.sourceBidGridView_SelectionChanged);
            // 
            // bidId
            // 
            this.bidId.HeaderText = "招标编号";
            this.bidId.Name = "bidId";
            this.bidId.Width = 150;
            // 
            // bidName
            // 
            this.bidName.HeaderText = "招标名称";
            this.bidName.Name = "bidName";
            // 
            // createTime
            // 
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.Width = 150;
            // 
            // bidState
            // 
            this.bidState.HeaderText = "招标状态";
            this.bidState.Name = "bidState";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "招标编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "招标名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "创建时间";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // btn_show
            // 
            this.btn_show.Location = new System.Drawing.Point(492, 27);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(75, 23);
            this.btn_show.TabIndex = 3;
            this.btn_show.Text = "查看";
            this.btn_show.UseVisualStyleBackColor = true;
            this.btn_show.Click += new System.EventHandler(this.btn_show_Click);
            // 
            // ShowTransact_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 462);
            this.Controls.Add(this.btn_show);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmb_TransactState);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ShowTransact_Form";
            this.Text = "查看谈判";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sourceBidGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_TransactState;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView sourceBidGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidName;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidState;
    }
}