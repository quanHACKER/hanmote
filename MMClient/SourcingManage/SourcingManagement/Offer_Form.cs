﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.ProcurementPlan;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Offer_Form : DockContent
    {
        //询价单表头
        SourceInquiry inquiry_table = new SourceInquiry();
        SourceInquiry sourceInquiry = null;
        //询价单
        List<Enquiry> enquiryList = new List<Enquiry>();
        List<Enquiry> xjxList = new List<Enquiry>();
        List<Enquiry> gysList = new List<Enquiry>();

        //查询工具
        EnquiryBLL enquiryBLL = new EnquiryBLL();
        MaterialBLL mateiralBll = new MaterialBLL();
        Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
        SourceBLL sourceBll = new SourceBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        SourceSupplierMaterialBLL ssmBll = new SourceSupplierMaterialBLL();

        SourceInquiryBLL sourceInquiryBll = new SourceInquiryBLL();

        //控件工具
        ConvenientTools tools = new ConvenientTools();
        
        int count = 0;

 
        List<string> xjdhItems = null;

        public Offer_Form()
        {
            InitializeComponent();
            xjdhItems = tools.addItemsStringList("", "", "Inquiry_ID", "Inquiry_Table");
            //tools.addItemsToComboBox(cgzz_cmb, "", "", "Department", "Buyer");
            //tools.addItemsToComboBox(cgz_cmb, "", "", "Buyer_Name", "Buyer");
            //Inquiry_view.Focus();
            //Inquiry_view.CurrentCell = Inquiry_view.Rows[0].Cells[0];
            this.Activate();
        }

        public Offer_Form(SourceInquiry sourceInquiry)
        {
            InitializeComponent();
            this.sourceInquiry = sourceInquiry;
            InitHeadInfo(sourceInquiry.Source_ID);
            InitMaterialGridView(sourceInquiry.Source_ID);
            initSupplierId();
        }

        private void InitHeadInfo(string sourceId)
        {
            txt_InquiryId.Text = sourceId;
            Source source = sourceBll.findSourceById(sourceId);
            //txt_State.Text = source.Source_State;
            txt_PurchaseOrg.Text = source.PurchaseOrg;
            txt_PurchaseGroup.Text = source.PurchaseGroup;
            txt_createtime.Text = source.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
            SourceInquiry si = sourceInquiryBll.findSourceById(sourceId);
            txt_offerTime.Text = si.Offer_Time.ToString("yyyy-MM-dd hh:mm:ss");
        }

        private void initSupplierId()
        {
            List<SourceSupplier> list = sourceSupplierBll.getBidSuppliersByBidId(txt_InquiryId.Text);
            for (int i = 0; i < list.Count; i++)
            {
                cmb_SupplierId.Items.Add(list.ElementAt(i).Supplier_ID);
            }

        }


        /// <summary>
        /// 根据寻源编号初始化物料信息
        /// </summary>
        /// <param name="p"></param>
        private void InitMaterialGridView(string sourceId)
        {
            List<SourceMaterial> smList = sourceMaterialBll.findMaterialsBySId(sourceId);
            Material_view.Rows.Clear();
            if (smList.Count > Material_view.Rows.Count)
            {
                Material_view.Rows.Add(smList.Count - Material_view.Rows.Count);
            }
            for (int i = 0; i < smList.Count; i++)
            {
                SourceMaterial sm = smList.ElementAt(i);
                Material_view.Rows[i].Cells["materialId"].Value = sm.Material_ID;
                MaterialBase mb = mateiralBll.getMaterialBaseById(sm.Material_ID);
                Material_view.Rows[i].Cells["materialName"].Value = mb.Material_Name;
                Material_view.Rows[i].Cells["demand_count"].Value = sm.Demand_Count;
                Material_view.Rows[i].Cells["measurement"].Value = mb.Measurement;
                Summary_Demand summaryDemand = summaryDemandBll.findSummaryDemandByDemandID(sm.Demand_ID);
                Material_view.Rows[i].Cells["stockId"].Value = sm.StockId;
                Material_view.Rows[i].Cells["factoryId"].Value = summaryDemand.Factory_ID;
                Material_view.Rows[i].Cells["DeliveryStartTime"].Value = sm.DeliveryStartTime.ToString("yyyy-MM-dd ");
                Material_view.Rows[i].Cells["DeliveryEndTime"].Value = sm.DeliveryEndTime.ToString("yyyy-MM-dd ");
            }
        }

        
        /*
        private void Offer_Click(int rowindex)
        {
            if (Supplier_view.Rows[rowindex].Cells[8].Value == null
                || Supplier_view.Rows[rowindex].Cells[8].Value.ToString().Equals(""))
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.RestoreDirectory = true;
                fileDialog.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
                fileDialog.FilterIndex = 1;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string newName = tools.systemTimeToStr();
                    string oldPath = fileDialog.FileName;
                    string fileFormat = oldPath.Substring(oldPath.LastIndexOf(".")); //获取文件格式
                    String FilePath = oldPath.Substring(0, oldPath.LastIndexOf("\\")+1);//获取文件路径，不带文件名
                    string newPath = FilePath + newName + fileFormat;
                    // 改名方法
                    FileInfo fi = new FileInfo(oldPath);
                    fi.MoveTo(Path.Combine(newPath));
                    //文档路径写入数据库
                    List<Enquiry> supplier = new List<Enquiry>();
                    gysList.ElementAt(rowindex).Price = newPath;
                    supplier.Add(gysList.ElementAt(rowindex));
                    int result = enquiryBLL.updateEnquiryOffer(supplier, inquiry_table);
                    if (result > 0)
                        Supplier_view.Rows[rowindex].Cells[8].Value = newPath;
                    else
                        MessageBox.Show("报价单写入数据库失败！");
                }
            }
            else 
            {
                System.Diagnostics.Process.Start(gysList.ElementAt(rowindex).Price);
                //判断当前供应商是否已报价
                bool isNotOffer = false;
                for (int i = 0; i < xjxList.Count; i++)
                {
                    if (Material_view.Rows[i].Cells[5].Value == null ||
                        Material_view.Rows[i].Cells[5].Value.ToString().Equals(""))
                    {
                        isNotOffer = true;
                        break;
                    }
                }
                if (isNotOffer)
                {
                    DialogResult result = MessageBox.Show("报价单写入报价请按确认键，不写入请按取消键!", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (result == DialogResult.OK)
                    {
                        Workbook excel = new Workbook(gysList.ElementAt(rowindex).Price);
                        Worksheet sheet = excel.Worksheets[0];
                        Cells cells = sheet.Cells;
                        int rowcount = cells.MaxRow;
                        int columncount = cells.MaxColumn;
                        for (int i = 0; i < xjxList.Count; i++)
                        {
                            Material_view.Rows[i].Cells[5].Value = cells[3 + i, 4].StringValue.Trim();
                        }
                        saveOffer();
                    }
                }
            }
        }
        */
        #region 保存按钮事件


        
        #endregion

        /// <summary>
        /// 保存供应商的报价
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveOfferButton_Click(object sender, EventArgs e)
        {
            if (cmb_SupplierId.Text.Equals(""))
            {
                MessageBox.Show("请选择一个供应商");
                return;
            }
            String supplierId = cmb_SupplierId.Text;
            foreach (DataGridViewRow dr in Material_view.Rows)
            {
                if (dr.Cells["PriceNum"].Value != null && Convert.ToDouble(dr.Cells["PriceNum"].Value.ToString()) != 0)
                {
                    SourceSupplierMaterial ssm = new SourceSupplierMaterial();
                    ssm.Source_ID = sourceInquiry.Source_ID;
                    ssm.Supplier_ID = supplierId;
                    ssm.Material_ID = dr.Cells["materialId"].Value.ToString();
                    ssm.Price = float.Parse(dr.Cells["PriceNum"].Value.ToString());
                    int result= ssmBll.addSupplierMaterial(ssm);
                }
            }
            count++;
            MessageBox.Show("报价成功");
            if (count == cmb_SupplierId.Items.Count)
            {   
                txt_State.Text = "已报价";
                int res = sourceInquiryBll.updateSourceState(sourceInquiry.Source_ID, getIntByState(txt_State.Text));
                MessageBox.Show("报价完成");
                this.Close();
            }
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待维护"))
            {
                res = 0;
            }
            else if (state.Equals("待邀请"))
            {
                res = 1;
            }
            else if (state.Equals("待报价"))
            {
                res = 2;
            }
            else if (state.Equals("待比价"))
            {
                res = 3;
            }
            else if (state.Equals("已完成"))
            {
                res = 4;
            }
            return res;
        }

        private void btn_Show_Click(object sender, EventArgs e)
        {
            if (cmb_SupplierId.Text.Equals(""))
            {
                MessageBox.Show("请选择一个供应商");
                return;
            }
            string supplierId = cmb_SupplierId.Text;
            List<SourceSupplierMaterial> list =  ssmBll.getSSMBySIdSuId(txt_InquiryId.Text, supplierId);
            
            foreach (DataGridViewRow dr in Material_view.Rows)
            {
                string mid = dr.Cells["materialId"].Value.ToString();
                for (int i = 0; i < list.Count; i++)
                {
                    if (list.ElementAt(i).Material_ID.Equals(mid))
                    {
                        dr.Cells["PriceNum"].Value = list.ElementAt(i).Price;
                    }
                }
            }
        }
    }
}
