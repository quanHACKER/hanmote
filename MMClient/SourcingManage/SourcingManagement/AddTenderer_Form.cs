﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Bll.MDBll.General;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class AddTenderer_Form : Form
    {
        private Bidding_Form biddingForm;
        private SupplierBaseBLL supBaseBLL = new SupplierBaseBLL();
        private MaterialGroupBLL materialGroupBLL = new MaterialGroupBLL();
        private GeneralBLL generalBll = new GeneralBLL();

        public AddTenderer_Form()
        {
            InitializeComponent();
            initCatalogue();
        }

        public AddTenderer_Form(Bidding_Form biddingForm)
        {
            InitializeComponent();
            this.biddingForm = biddingForm;
            dgv_editSupplier.TopLeftHeaderCell.Value = "序号";
            initCatalogue();
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_editSupplier_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void initCatalogue()
        {
            List<MaterialGroup> list = materialGroupBLL.getAllMaterialGroup();
            for (int i = 0; i < list.Count; i++)
            {
                cmb_catalogue.Items.Add(list.ElementAt(i).MaterialGroupName);
            }
        }

        /// <summary>
        /// 搜索合格的供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            String item = cmb_catalogue.Text;//取出下拉列表的值
            dgv_editSupplier.Rows.Clear();
            List<SupplierBase> list = generalBll.GetAllSuppliers();
            if (list.Count > dgv_editSupplier.Rows.Count)
            {
                dgv_editSupplier.Rows.Add(list.Count - dgv_editSupplier.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                dgv_editSupplier.Rows[i].Cells["supplierId"].Value = list.ElementAt(i).Supplier_ID;
                dgv_editSupplier.Rows[i].Cells["supplierName"].Value = list.ElementAt(i).Supplier_Name;
                dgv_editSupplier.Rows[i].Cells["proposer"].Value = "";
                dgv_editSupplier.Rows[i].Cells["email"].Value = list.ElementAt(i).Email;
                dgv_editSupplier.Rows[i].Cells["address"].Value = list.ElementAt(i).Address;
            }
            

        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            List<SourceSupplier> supbList = new List<SourceSupplier>();
            foreach (DataGridViewRow dgvr in dgv_editSupplier.Rows)
            {
                if (dgvr.Selected)
                {
                    SourceSupplier supb = new SourceSupplier();
                    supb.Supplier_ID = dgvr.Cells["supplierId"].Value.ToString();
                    supb.Supplier_Name = dgvr.Cells["supplierName"].Value.ToString();
                    supb.Contact = dgvr.Cells["proposer"].Value.ToString();
                    supb.Email = dgvr.Cells["email"].Value.ToString();
                    supb.Address = dgvr.Cells["address"].Value.ToString();
                    supbList.Add(supb);
                }
            }
            biddingForm.fillMaterialBases(supbList);
            this.Close();
        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
