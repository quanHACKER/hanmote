﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.SourcingManage.ProcurementPlan;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Source_List_Search : DockContent
    {
        //货源清单
        List<Source_List> sourceList = new List<Source_List>();
        List<Source_List> xjxList = new List<Source_List>();
        //查询工具
        Source_ListBLL source_ListBLL = new Source_ListBLL();
        //控件工具
        ConvenientTools tools = new ConvenientTools();
        //DGV默认行数
        int DGVnumber = 10;
        //物料及供应商编号itemslist
        List<string> wlbhItems = null;
        List<string> gysbhItems = null;
        List<string> gcbhItems = null;
        List<string> cgzzItems = null;

        public Source_List_Search()
        {
            InitializeComponent();
            Init(source_view);
            tools.addItemsToCMB(wlbh_cmb, "", "", "Material_ID", "Material_Type");
            tools.addItemsToCMB(gc_cmb, "", "", "Factory_ID", "Inventory");
            tools.addItemsToCMB(gys_cmb, "", "", "Supplier_ID", "Supplier_Base");
            tools.addItemsToCMB(cgz_cmb, "", "", "Department", "Buyer");
            tools.addItemsToCMB(xy_cmb, "", "", "Agreement_ID", "Source_List");
        }


        private void Init(DataGridView view)
        {
            view.AllowUserToAddRows = true;
            view.AutoGenerateColumns = false;
            view.AutoSize = false;
            //view.RowHeadersVisible = false;
            //view.GridColor = System.Drawing.ColorTranslator.FromHtml("#F8F8FF");
            view.ColumnHeadersHeight = 60;
            view.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            view.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            view.EditMode = DataGridViewEditMode.EditOnEnter;

            DataGridViewCellPainting(view);
            DataGridViewDataSource_Load(view);
            view.Parent = this;
            view.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(view_EditingControlShowing);
            //view.CellPainting -= new DataGridViewCellPaintingEventHandler(view_CellPainting);
            //view.CellPainting += new DataGridViewCellPaintingEventHandler(view_CellPainting);
        }

        /// <summary>
        ///datagridview绘图
        /// </summary>
        private void DataGridViewCellPainting(DataGridView view)
        {
            for(int j=0;j<DGVnumber;j++)
                view.Rows.Add();
            view.AllowUserToAddRows = false;
        }

        /// <summary>
        ///datagridview绑定数据源
        /// </summary>
        private void DataGridViewDataSource_Load(DataGridView view)
        {  
            gysbhItems = tools.addItemsStringList("", "", "Supplier_ID", "Supplier_Base");

            cgzzItems = tools.addItemsStringList("", "", "Department", "Buyer");

        }


        /// <summary>
        /// 编辑单元格添加控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void view_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Control[] CS = null;
            Button B = null;
            DateTimePicker T = null;
            ComboBox CMB = null;
            #region 供应商编号添加详细按钮
            if (source_view.EditingControl != null)
            {
                CS = source_view.EditingPanel.Controls.Find("__BUTTON__", true);
                if (CS.Length > 0)
                    source_view.EditingPanel.Controls.Remove(CS[0]);
                if (source_view.CurrentCell.ColumnIndex == 3)
                {
                    B = new Button();
                    B.Name = "__BUTTON__";
                    B.BackColor = SystemColors.Control;
                    B.Text = "详";
                    B.ForeColor = Color.CornflowerBlue;
                    B.Width = 22;
                    B.Height = 22;
                    B.Parent = source_view.EditingPanel;
                    B.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    B.Click += new EventHandler(B_Click);
                }
            }
            #endregion

            #region 供应商编号添加Combobox
            if (source_view.EditingControl != null)
            {
                CS = source_view.EditingPanel.Controls.Find("__combobox__", true);
                if (CS.Length > 0)
                    source_view.EditingPanel.Controls.Remove(CS[0]);
                if (source_view.CurrentCell.ColumnIndex == 3)
                {
                    CMB = new ComboBox();
                    CMB.Name = "__combobox__";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gysbhItems.Count; n++)
                    {
                        CMB.Items.Add(gysbhItems.ElementAt(n));
                    }
                    if (source_view.CurrentCell.Value != null && !source_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = source_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = source_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            if (source_view.EditingControl != null)
            {
                CS = source_view.EditingPanel.Controls.Find("_combobox_", true);
                if (CS.Length > 0)
                    source_view.EditingPanel.Controls.Remove(CS[0]);
                if (source_view.CurrentCell.ColumnIndex == 4)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_combobox_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < cgzzItems.Count; n++)
                    {
                        CMB.Items.Add(cgzzItems.ElementAt(n));
                    }
                    if (source_view.CurrentCell.Value != null && !source_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = source_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = source_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion


            #region 有效日期添加日期控件

            CS = source_view.EditingPanel.Controls.Find("__DATETIME__", true);
            if (CS.Length > 0)
                source_view.EditingPanel.Controls.Remove(CS[0]);
            if (source_view.CurrentCell.ColumnIndex == 1 || source_view.CurrentCell.ColumnIndex == 2)
            {
                T = new DateTimePicker();
                T.Name = "__DATETIME__";
                T.BackColor = SystemColors.Window;
                T.Width = e.Control.Width;
                T.Height = e.Control.Height;
                T.Parent = source_view.EditingPanel;
                T.Dock = DockStyle.Right;
                e.Control.Dock = DockStyle.Fill;
                T.ValueChanged += new EventHandler(T_ValueChanged);
            }
            #endregion
        }

        /// <summary>
        /// COMBOBOX值改变触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CMB_TextChanged(object sender, EventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            string str = cmb.Text.ToString().Trim();
            if (str.Equals(""))
                return;
            if (source_view.EditingControl != null)
            {
                if (cmb.Name.Equals("__combobox__") && gysbhItems.Contains(str))
                {
                    for (int i = 0; i < source_view.Rows.Count; i++)
                    {
                        if (source_view.Rows[i].Cells[3].Value == null)
                            break;
                        if (source_view.Rows[i].Cells[3].Value.Equals(str) && i != source_view.CurrentCell.RowIndex)
                        {
                            source_view.CurrentCell.Value = "";
                            MessageBox.Show("物料编号：" + str + "已重复，请重新选择！");
                            return;
                        }
                    }
                    source_view.CurrentCell.Value = str;
                }
                else if (cmb.Name.Equals("_combobox_") && cgzzItems.Contains(str))
                {
                    source_view.CurrentCell.Value = str;
                }
            }
        }

        /// <summary>
        /// 编号列详细按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void B_Click(object sender, EventArgs e)
        {
            if(source_view.EditingControl!=null)
                MessageBox.Show(source_view.EditingControl.Text);
        }

        /// <summary>
        /// 交货日期列时间事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void T_ValueChanged(object sender, EventArgs e)
        {
            DateTimePicker time = sender as DateTimePicker;
            if (source_view.EditingControl != null)
            {
                if (source_view.CurrentCell.ColumnIndex == 1)
                    source_view.CurrentCell.Value = time.Value.ToShortDateString() + " 00:00:00";
                else if (source_view.CurrentCell.ColumnIndex == 2)
                    source_view.CurrentCell.Value = time.Value.ToShortDateString() + " 23:59:59";
            }
            DateTime time1 = tools.boxToTime(source_view.Rows[source_view.CurrentCell.RowIndex].Cells[1]);
            DateTime time2 = tools.boxToTime(source_view.Rows[source_view.CurrentCell.RowIndex].Cells[2]);
            if (source_view.EditingControl != null && source_view.CurrentCell.ColumnIndex == 1)
            {
                if (time2.Year!=1900 && time.Value.CompareTo(time2) > 0)
                {
                    source_view.Rows[source_view.CurrentCell.RowIndex].Cells[1].Value = "";
                    MessageBox.Show("有效起始日期必须早于有效截止日期！");
                }
            }
            if (source_view.EditingControl != null && source_view.CurrentCell.ColumnIndex==2)
            {
                if (time.Value.CompareTo(DateTime.Now) < 0)
                {
                    source_view.Rows[source_view.CurrentCell.RowIndex].Cells[2].Value = "";
                    MessageBox.Show("有效截止日期必须晚于系统当前日期！");
                }
                else if (time.Value.CompareTo(time1) < 0)
                {
                    source_view.Rows[source_view.CurrentCell.RowIndex].Cells[2].Value = "";
                    MessageBox.Show("有效截止日期必须晚于有效起始日期！");
                }
            }
        }

        //菜单栏panel1的状态控制
        private void panel1Contorls(PictureBox pb,Label lbl)
        {
            for (int i = 0; i < panel1.Controls.Count; i++)
            {
                if (panel1.Controls[i].Width == 35)
                {
                    panel1.Controls[i].Width = 40;
                    panel1.Controls[i].Height = 40;
                }
                else if (panel1.Controls[i].Font.Bold)
                {
                    //文本加粗
                    panel1.Controls[i].Font = new System.Drawing.Font(panel1.Controls[i].Font, panel1.Controls[i].Font.Style & ~FontStyle.Bold);
                }
            }
            lbl.Font = new System.Drawing.Font(lbl.Font, lbl.Font.Style | FontStyle.Bold);
            pb.Width = 35;
            pb.Height = 35;
        }

        //点击查看按钮控件状态改变
        private void readClick()
        {
            panel1Contorls(read_pb, read_lbl);
            source_Refresh();
            source_view.Enabled = true;
            saveButton.Visible = false;
        }
        private void read_pb_Click(object sender, EventArgs e)
        {
            readClick();
        }

        //点击维护按钮控件状态改变
        private void edit_pb_Click(object sender, EventArgs e)
        {
            panel1Contorls(edit_pb, edit_lbl);
            source_view.Enabled = false;
            saveButton.Visible = true;
        }

        //点击删除按钮控件状态改变
        private void delete_pb_Click(object sender, EventArgs e)
        {
            panel1Contorls(delete_pb, delete_lbl);
            source_view.Enabled = false;
            saveButton.Visible = true;
        }

        //关闭form事件
        private void close_pb_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void add_button_Click(object sender, EventArgs e)
        {
            source_view.Rows.Add();
        }

        private void remove_button_Click(object sender, EventArgs e)
        {
            for (int j = 1; j < source_view.Columns.Count; j++)
            {
                if (j == 7 || j == 8 || j == 9)
                {
                    DataGridViewCheckBoxCell check = source_view.CurrentRow.Cells[j] as DataGridViewCheckBoxCell;
                    check.Value = false;
                }
                else
                    source_view.CurrentRow.Cells[j].Value = "";
            }
        }

        /// <summary>
        /// 删除行按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewRemove(DataGridView view)
        {
            for (int i = 0; i < view.Rows.Count; i++)
            {
                for (int j = 1; j < view.Columns.Count; j++)
                {
                    if (j == 10)
                    {
                        DataGridViewCheckBoxCell check = source_view.Rows[i].Cells[j] as DataGridViewCheckBoxCell;
                        check.Value = false;
                    }
                    else
                        view.Rows[i].Cells[j].Value = "";
                }
            }
            if (view.Rows.Count > DGVnumber)
            {
                for (int i = view.Rows.Count; i > DGVnumber; i--)
                {
                    view.Rows.RemoveAt(i-1);
                }
            }
            else
            {
                for (int i = view.Rows.Count; i < DGVnumber; i++)
                {
                    view.Rows.Add();
                }
            }
        }

        //物料编号及工厂选值改变时刷新数据
        private void source_Refresh()
        {
            sourceList.Clear();
            Dictionary<string, string> conditions = new Dictionary<string,string>();
            if (!tools.cmbToString(wlbh_cmb).Equals(""))
                conditions.Add("1", "Material_ID like '%" + tools.cmbToString(wlbh_cmb) + "%' ");

            if (!tools.cmbToString(gc_cmb).Equals(""))
                conditions.Add("2", "Factory_ID like '%" + tools.cmbToString(gc_cmb) + "%' ");

            if (!tools.cmbToString(gys_cmb).Equals(""))
                conditions.Add("3", "Supplier_ID like '%" + tools.cmbToString(gys_cmb) + "%' ");

            if (!tools.cmbToString(cgz_cmb).Equals(""))
                conditions.Add("4", "Department like '%" + tools.cmbToString(cgz_cmb) + "%' ");

            if (xy_Button2.Checked == true)
                conditions.Add("5", "Agreement_ID is null or Agreement_ID = '' ");
            else if (xy_Button3.Checked == true)
                conditions.Add("5", "Agreement_ID is not null and Agreement_ID != ''");
            else if (!tools.cmbToString(xy_cmb).Equals("") && xy_Button2.Checked == false && xy_Button3.Checked == false)
                conditions.Add("5", "Agreement_ID like '%" + tools.cmbToString(xy_cmb) + "%' ");

            if(!tools.cmbToString(hyzt_cmb).Equals(""))
                conditions.Add("6", "State like '%" + tools.cmbToString(hyzt_cmb) + "%' ");

            if(begin_date.CustomFormat ==null)
                conditions.Add("7", "Effective_Date <= '" + begin_date.Value + "' ");

            if (end_date.CustomFormat == null)
                conditions.Add("8", "Expiration_Date >= '" + end_date.Value + "' ");

            if (update_date.CustomFormat == null)
                conditions.Add("9", "Create_Date >= '" + update_date.Value.Date + 
                    "' and Create_Date < '" + update_date.Value.Date.AddDays(1) + "' ");

            if (MRP_check.Checked == true)
                conditions.Add("0", "Spare1 = 'MRP' ");

            sourceList = source_ListBLL.findSource(conditions);
            if (sourceList.Count>0)
            {
                if (DGVnumber < sourceList.Count)
                    DGVnumber = sourceList.Count;
                else if (sourceList.Count<10)
                    DGVnumber = 10;
                viewRemove(source_view);
                for (int i = 0; i < sourceList.Count; i++)
                {
                    source_view.Rows[i].Cells[1].Value = sourceList.ElementAt(i).Material_ID;
                    source_view.Rows[i].Cells[2].Value = sourceList.ElementAt(i).Factory_ID;
                    source_view.Rows[i].Cells[3].Value = sourceList.ElementAt(i).Supplier_ID;
                    if (sourceList.ElementAt(i).Effective_Date.Year == 1900)
                        source_view.Rows[i].Cells[4].Value = "";
                    else
                        source_view.Rows[i].Cells[4].Value = sourceList.ElementAt(i).Effective_Date.ToString();
                    if (sourceList.ElementAt(i).Expiration_Date.Year == 9999)
                        source_view.Rows[i].Cells[5].Value = "";
                    else
                        source_view.Rows[i].Cells[5].Value = sourceList.ElementAt(i).Expiration_Date.ToString();

                    source_view.Rows[i].Cells[6].Value = sourceList.ElementAt(i).Department;
                    source_view.Rows[i].Cells[7].Value = sourceList.ElementAt(i).Buyer_Name;
                    source_view.Rows[i].Cells[8].Value = sourceList.ElementAt(i).Agreement_ID;
                    source_view.Rows[i].Cells[9].Value = sourceList.ElementAt(i).State;
                    if (sourceList.ElementAt(i).Spare1.Equals("MRP"))
                    {
                        DataGridViewCheckBoxCell checkBox = source_view.Rows[i].Cells[10] as DataGridViewCheckBoxCell;
                        checkBox.Value = true;
                    }
                    else
                    {
                        DataGridViewCheckBoxCell checkBox = source_view.Rows[i].Cells[10] as DataGridViewCheckBoxCell;
                        checkBox.Value = false;
                    }
                    source_view.Rows[i].Cells[12].Value = sourceList.ElementAt(i).Create_Date;
                }
            }
            else 
            {
                DGVnumber = 10;
                viewRemove(source_view);
                MessageBox.Show("未查询到匹配数据，请重新填写查询条件！");
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            source_Save();
            readClick();
        }

        //物料编号及工厂选值改变时刷新数据
        private void source_Save()
        {
            sourceList.Clear();
            List<Source_List> newList = new List<Source_List>();
            for (int i = 0; i < source_view.Rows.Count-1; i++)
            {
                if (source_view.Rows[i].Cells[3].Value != null && !source_view.Rows[i].Cells[3].Value.ToString().Equals(""))
                {
                    Source_List source = new Source_List();
                    source.Material_ID = tools.cmbToString(wlbh_cmb);
                    source.Factory_ID = tools.cmbToString(gc_cmb);
                    source.Supplier_ID = tools.boxToString(source_view.Rows[i].Cells[3]);
                    source.Effective_Date = tools.boxToTime(source_view.Rows[i].Cells[1]);
                    source.Expiration_Date = tools.boxToTime2(source_view.Rows[i].Cells[2]);
                    source.Create_Date = DateTime.Now;
                    source.Department = tools.boxToString(source_view.Rows[i].Cells[4]);
                    source.Buyer_Name = tools.boxToString(source_view.Rows[i].Cells[5]);
                    source.Agreement_ID = tools.boxToString(source_view.Rows[i].Cells[6]);
                    source.State = "";
                    DataGridViewCheckBoxCell check = source_view.Rows[i].Cells[7] as DataGridViewCheckBoxCell;
                    if((bool)check.EditedFormattedValue == true)
                        source.State = "Fix";
                    check = source_view.Rows[i].Cells[8] as DataGridViewCheckBoxCell;
                    if ((bool)check.EditedFormattedValue == true)
                        source.State = "Blk";
                    check = source_view.Rows[i].Cells[9] as DataGridViewCheckBoxCell;
                    if ((bool)check.EditedFormattedValue == true)
                        source.Spare1 = "MRP";
                    else
                        source.Spare1 = "";

                    newList.Add(source);
                } 
            }
            int result =0;
            if (newList.Count > 0)
                result = source_ListBLL.addNewSource_List(newList);
            if (result > 0)
                MessageBox.Show("物料：" + tools.cmbToString(wlbh_cmb) + " 工厂：" + tools.cmbToString(gc_cmb) + "源清单保存成功");
        }


#region dateTime控件格式设置
        private void begin_date_ValueChanged(object sender, EventArgs e)
        {
            this.begin_date.CustomFormat = null;
        }
        private void end_date_ValueChanged(object sender, EventArgs e)
        {
            this.end_date.CustomFormat = null;
        }
        private void update_date_ValueChanged(object sender, EventArgs e)
        {
            this.update_date.CustomFormat = null;
        }
        private void begin_lbl_Click(object sender, EventArgs e)
        {
            this.begin_date.CustomFormat = " ";
        }
        private void end_lbl_Click(object sender, EventArgs e)
        {
            this.end_date.CustomFormat = " ";
        }
        private void update_lbl_Click(object sender, EventArgs e)
        {
            this.update_date.CustomFormat = " ";
        }
#endregion


        //public void Export()
        //{
        //    Workbook workbook = new Workbook();
        //    Worksheet sheet = (Worksheet)workbook.Worksheets[0];//表0
        //    sheet.Name = "汉默特询价单" + wlbh_cmb.Text.ToString().Trim();
        //    Cells cells = sheet.Cells;//单元格
        //    for (int i = 0; i < 10; i++)
        //    {
        //        if (i == 0)
        //            cells.SetColumnWidthPixel(i, 105);//设置列宽
        //        else if (i == 1)
        //            cells.SetColumnWidthPixel(i, 115);//设置列宽
        //        else if (i == 2)
        //            cells.SetColumnWidthPixel(i, 90);//设置列宽
        //        else if (i == 3)
        //            cells.SetColumnWidthPixel(i, 65);//设置列宽
        //        else
        //            cells.SetColumnWidthPixel(i, 80);//设置列宽
        //    }
        //    for (int j = 0; j < xjxList.Count + gysList.Count + 6; j++)
        //    {
        //        cells.SetRowHeightPixel(j, 25);//设置行高 
        //        if (j == 2)
        //        {
        //            for (int i = 0; i < 10; i++)
        //                sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //        }
        //        else
        //        {
        //            for (int i = 0; i < 10; i++)
        //                sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, false));
        //        }
        //    }

        //    sheet.Cells[0, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //    sheet.Cells[0, 2].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //    sheet.Cells[0, 4].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //    sheet.Cells[0, 6].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //    sheet.Cells[xjxList.Count + 4, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
        //    sheet.Cells[xjxList.Count + 4, 1].SetStyle(tools.SettingCellStyle(workbook, cells, true));

        //    sheet.Cells[0, 0].PutValue("询价单号");
        //    sheet.Cells[0, 1].PutValue(inquiry_table.Inquiry_ID);
        //    sheet.Cells[0, 2].PutValue("采购组织");
        //    sheet.Cells[0, 3].PutValue(inquiry_table.Department);
        //    sheet.Cells[0, 4].PutValue("采购组");
        //    sheet.Cells[0, 5].PutValue(inquiry_table.Buyer_Name);
        //    sheet.Cells[0, 6].PutValue("报价期限");
        //    sheet.Cells[0, 7].PutValue(inquiry_table.Offer_Time.ToString().Trim());

        //    sheet.Cells[2, 0].PutValue("物料编号");
        //    sheet.Cells[2, 1].PutValue("物料名称");
        //    sheet.Cells[2, 2].PutValue("物料标准");
        //    sheet.Cells[2, 3].PutValue("询价数量");
        //    sheet.Cells[2, 4].PutValue("价格");
        //    sheet.Cells[2, 5].PutValue("单位");
        //    sheet.Cells[2, 6].PutValue("交货日期");
        //    sheet.Cells[2, 7].PutValue("备注1");
        //    sheet.Cells[2, 8].PutValue("备注2");
        //    sheet.Cells[2, 9].PutValue("备注3");
        //    for (int j = 0; j < xjxList.Count; j++)
        //    {
        //        sheet.Cells[j + 3, 0].PutValue(tools.boxToString(source_view.Rows[j].Cells[1]));
        //        sheet.Cells[j + 3, 1].PutValue(tools.boxToString(source_view.Rows[j].Cells[2]));
        //        sheet.Cells[j + 3, 2].PutValue(tools.boxToString(source_view.Rows[j].Cells[3]));
        //        sheet.Cells[j + 3, 3].PutValue(tools.boxToString(source_view.Rows[j].Cells[4]));
        //        sheet.Cells[j + 3, 4].PutValue("");
        //        sheet.Cells[j + 3, 5].PutValue(tools.boxToString(source_view.Rows[j].Cells[5]));
        //        sheet.Cells[j + 3, 6].PutValue(tools.boxToString(source_view.Rows[j].Cells[6]));
        //        sheet.Cells[j + 3, 7].PutValue("");
        //        sheet.Cells[j + 3, 8].PutValue("");
        //        sheet.Cells[j + 3, 9].PutValue("");
        //    }
        //    sheet.Cells[xjxList.Count + 4, 0].PutValue("供应商编号");
        //    sheet.Cells[xjxList.Count + 4, 1].PutValue("供应商名称");
        //    for (int j = 0; j < gysList.Count; j++)
        //    {
        //        sheet.Cells[j + xjxList.Count + 6, 0].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells[1]));
        //        sheet.Cells[j + xjxList.Count + 6, 1].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells[2]));
        //    }

        //    SaveFileDialog frm = new SaveFileDialog();
        //    frm.RestoreDirectory = true;//保存上次打开的路径
        //    frm.FileName = inquiry_table.Inquiry_ID;
        //    frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
        //    //点了保存按钮进入 
        //    string localFilePath = null;
        //    if (frm.ShowDialog() == DialogResult.OK)
        //    {
        //        localFilePath = frm.FileName.ToString(); //获得文件路径 
        //        string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径
        //        String FilePath = localFilePath.Substring(0, localFilePath.LastIndexOf("\\"));//获取文件路径，不带文件名 
        //        //fileNameExt = DateTime.Now.ToString() + fileNameExt;//给文件名前加上时间 
        //        //saveFileDialog1.FileName.Insert(1,"dameng");//在文件名里加字符 
        //        //System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();//输出文件
        //        workbook.Save(@localFilePath);
        //        MessageBox.Show("文件 " + fileNameExt + " 已成功保存至:" + FilePath, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    else
        //    {
        //        localFilePath = "E:\\" + inquiry_table.Inquiry_ID + ".xls";
        //        workbook.Save(@"E:\\" + ".xls");
        //        MessageBox.Show("文件已成功保存至: " + "E:\\", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
        //    }
        //    inquiry_table.Inquiry_Path = localFilePath;
        //    inquiry_table.State = "待邀请";
        //    int result = source_ListBLL.updateInquiry_Table(inquiry_table);
        //    if (result > 0)
        //    {
        //        MessageBox.Show("询价单保存成功！");
        //    }

        //}

        //生成询价单
        //private void scxj_button_Click(object sender, EventArgs e)
        //{
        //    Export();
        //}
        

    }
}
