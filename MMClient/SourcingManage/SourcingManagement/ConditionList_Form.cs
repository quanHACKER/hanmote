﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ConditionList_Form : DockContent
    {

        private ConditionBLL conditionBLL = new ConditionBLL();
        public ConditionType conditionType = null;
        private UserUI userUI;

        public ConditionList_Form()
        {
            InitializeComponent();
            init();
        }

        public ConditionList_Form(UserUI userUI, ConditionType conditionType)
        {
            // TODO: Complete member initialization
            this.userUI = userUI;
            this.conditionType = conditionType;
        }

        private void init()
        {
            List<ConditionType> list = conditionBLL.getConditions();
            if (list.Count > conditionGridView.Rows.Count)
            {
                conditionGridView.Rows.Add(list.Count - conditionGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                conditionGridView.Rows[i].Cells["conditionId"].Value = list.ElementAt(i).ConditionId;
                conditionGridView.Rows[i].Cells["conditionName"].Value = list.ElementAt(i).ConditionName;
                conditionGridView.Rows[i].Cells["conditionCategory"].Value = list.ElementAt(i).ConditionCategory;
                conditionGridView.Rows[i].Cells["roundType"].Value = list.ElementAt(i).RoundType;
                conditionGridView.Rows[i].Cells["pn"].Value = list.ElementAt(i).Pn;
            }
        }

        //删除条件类型
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            //删除条件类型
        }

        //选中的条件类型
        private void conditionGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = conditionGridView.CurrentRow.Index;
            Object obj = conditionGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            } 
            String conditionId = conditionGridView.Rows[index].Cells[0].Value.ToString();
            //根据选中行id查找对应的需求计划
            conditionType = conditionBLL.findConditionTypeByConditionID(conditionId);     
        }
    }
}
