﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class EditSourceList_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveButton = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_MaterialId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_FactoryId = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_MRPEnd = new System.Windows.Forms.TextBox();
            this.txt_MRPStart = new System.Windows.Forms.TextBox();
            this.chk_MRP = new System.Windows.Forms.CheckBox();
            this.chk_Blk = new System.Windows.Forms.CheckBox();
            this.chk_Fix = new System.Windows.Forms.CheckBox();
            this.txt_ppl = new System.Windows.Forms.TextBox();
            this.txt_protople = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroup = new System.Windows.Forms.TextBox();
            this.txt_SupplierId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dte_StartTime = new System.Windows.Forms.DateTimePicker();
            this.dte_EndTime = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveButton.Location = new System.Drawing.Point(694, 407);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(63, 23);
            this.saveButton.TabIndex = 195;
            this.saveButton.Text = "保存";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 196;
            this.label1.Text = "物料编号";
            // 
            // txt_MaterialId
            // 
            this.txt_MaterialId.Location = new System.Drawing.Point(83, 24);
            this.txt_MaterialId.Name = "txt_MaterialId";
            this.txt_MaterialId.ReadOnly = true;
            this.txt_MaterialId.Size = new System.Drawing.Size(156, 21);
            this.txt_MaterialId.TabIndex = 197;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 198;
            this.label2.Text = "工厂编号";
            // 
            // txt_FactoryId
            // 
            this.txt_FactoryId.Location = new System.Drawing.Point(84, 54);
            this.txt_FactoryId.Name = "txt_FactoryId";
            this.txt_FactoryId.ReadOnly = true;
            this.txt_FactoryId.Size = new System.Drawing.Size(100, 21);
            this.txt_FactoryId.TabIndex = 199;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dte_EndTime);
            this.groupBox1.Controls.Add(this.dte_StartTime);
            this.groupBox1.Controls.Add(this.txt_MRPEnd);
            this.groupBox1.Controls.Add(this.txt_MRPStart);
            this.groupBox1.Controls.Add(this.chk_MRP);
            this.groupBox1.Controls.Add(this.chk_Blk);
            this.groupBox1.Controls.Add(this.chk_Fix);
            this.groupBox1.Controls.Add(this.txt_ppl);
            this.groupBox1.Controls.Add(this.txt_protople);
            this.groupBox1.Controls.Add(this.txt_PurchaseGroup);
            this.groupBox1.Controls.Add(this.txt_SupplierId);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(27, 104);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(797, 269);
            this.groupBox1.TabIndex = 200;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "更改源清单信息";
            // 
            // txt_MRPEnd
            // 
            this.txt_MRPEnd.Location = new System.Drawing.Point(567, 170);
            this.txt_MRPEnd.Name = "txt_MRPEnd";
            this.txt_MRPEnd.Size = new System.Drawing.Size(67, 21);
            this.txt_MRPEnd.TabIndex = 4;
            // 
            // txt_MRPStart
            // 
            this.txt_MRPStart.Location = new System.Drawing.Point(459, 170);
            this.txt_MRPStart.Name = "txt_MRPStart";
            this.txt_MRPStart.Size = new System.Drawing.Size(68, 21);
            this.txt_MRPStart.TabIndex = 4;
            // 
            // chk_MRP
            // 
            this.chk_MRP.AutoSize = true;
            this.chk_MRP.Location = new System.Drawing.Point(567, 126);
            this.chk_MRP.Name = "chk_MRP";
            this.chk_MRP.Size = new System.Drawing.Size(42, 16);
            this.chk_MRP.TabIndex = 3;
            this.chk_MRP.Text = "MRP";
            this.chk_MRP.UseVisualStyleBackColor = true;
            // 
            // chk_Blk
            // 
            this.chk_Blk.AutoSize = true;
            this.chk_Blk.Location = new System.Drawing.Point(459, 126);
            this.chk_Blk.Name = "chk_Blk";
            this.chk_Blk.Size = new System.Drawing.Size(42, 16);
            this.chk_Blk.TabIndex = 3;
            this.chk_Blk.Text = "Blk";
            this.chk_Blk.UseVisualStyleBackColor = true;
            // 
            // chk_Fix
            // 
            this.chk_Fix.AutoSize = true;
            this.chk_Fix.Location = new System.Drawing.Point(335, 126);
            this.chk_Fix.Name = "chk_Fix";
            this.chk_Fix.Size = new System.Drawing.Size(42, 16);
            this.chk_Fix.TabIndex = 3;
            this.chk_Fix.Text = "Fix";
            this.chk_Fix.UseVisualStyleBackColor = true;
            // 
            // txt_ppl
            // 
            this.txt_ppl.Location = new System.Drawing.Point(88, 130);
            this.txt_ppl.Name = "txt_ppl";
            this.txt_ppl.Size = new System.Drawing.Size(115, 21);
            this.txt_ppl.TabIndex = 1;
            // 
            // txt_protople
            // 
            this.txt_protople.Location = new System.Drawing.Point(88, 164);
            this.txt_protople.Name = "txt_protople";
            this.txt_protople.ReadOnly = true;
            this.txt_protople.Size = new System.Drawing.Size(115, 21);
            this.txt_protople.TabIndex = 1;
            // 
            // txt_PurchaseGroup
            // 
            this.txt_PurchaseGroup.Location = new System.Drawing.Point(88, 89);
            this.txt_PurchaseGroup.Name = "txt_PurchaseGroup";
            this.txt_PurchaseGroup.ReadOnly = true;
            this.txt_PurchaseGroup.Size = new System.Drawing.Size(115, 21);
            this.txt_PurchaseGroup.TabIndex = 1;
            // 
            // txt_SupplierId
            // 
            this.txt_SupplierId.Location = new System.Drawing.Point(88, 38);
            this.txt_SupplierId.Name = "txt_SupplierId";
            this.txt_SupplierId.ReadOnly = true;
            this.txt_SupplierId.Size = new System.Drawing.Size(115, 21);
            this.txt_SupplierId.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "协议号";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "PPL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "采购组";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(533, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "____";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(333, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "MRP范围";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(333, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "有效结束时间";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "有效开始时间";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "供应商";
            // 
            // dte_StartTime
            // 
            this.dte_StartTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dte_StartTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dte_StartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_StartTime.Location = new System.Drawing.Point(459, 33);
            this.dte_StartTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dte_StartTime.Name = "dte_StartTime";
            this.dte_StartTime.Size = new System.Drawing.Size(175, 23);
            this.dte_StartTime.TabIndex = 189;
            // 
            // dte_EndTime
            // 
            this.dte_EndTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dte_EndTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dte_EndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_EndTime.Location = new System.Drawing.Point(459, 81);
            this.dte_EndTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dte_EndTime.Name = "dte_EndTime";
            this.dte_EndTime.Size = new System.Drawing.Size(175, 23);
            this.dte_EndTime.TabIndex = 190;
            // 
            // EditSourceList_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(878, 496);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.txt_FactoryId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_MaterialId);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "EditSourceList_Form";
            this.Text = "更改货源清单";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_MaterialId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_FactoryId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_PurchaseGroup;
        private System.Windows.Forms.TextBox txt_SupplierId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_MRPEnd;
        private System.Windows.Forms.TextBox txt_MRPStart;
        private System.Windows.Forms.CheckBox chk_MRP;
        private System.Windows.Forms.CheckBox chk_Blk;
        private System.Windows.Forms.CheckBox chk_Fix;
        private System.Windows.Forms.TextBox txt_ppl;
        private System.Windows.Forms.TextBox txt_protople;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dte_StartTime;
        private System.Windows.Forms.DateTimePicker dte_EndTime;
    }
}