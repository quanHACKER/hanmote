﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class NewSourceList_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_MaterialId = new System.Windows.Forms.TextBox();
            this.txt_FactoryId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_SaveSourceList = new System.Windows.Forms.Button();
            this.sourceListGridView = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseOrg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ppl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.protocolId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Inquiry_view7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Inquiry_view8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceListGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 43);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料编号";
            // 
            // txt_MaterialId
            // 
            this.txt_MaterialId.Location = new System.Drawing.Point(97, 40);
            this.txt_MaterialId.Name = "txt_MaterialId";
            this.txt_MaterialId.ReadOnly = true;
            this.txt_MaterialId.Size = new System.Drawing.Size(169, 21);
            this.txt_MaterialId.TabIndex = 1;
            // 
            // txt_FactoryId
            // 
            this.txt_FactoryId.Location = new System.Drawing.Point(389, 40);
            this.txt_FactoryId.Name = "txt_FactoryId";
            this.txt_FactoryId.ReadOnly = true;
            this.txt_FactoryId.Size = new System.Drawing.Size(100, 21);
            this.txt_FactoryId.TabIndex = 2;
            this.txt_FactoryId.TextChanged += new System.EventHandler(this.txt_FactoryId_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(330, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "工厂编号";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_SaveSourceList);
            this.groupBox1.Controls.Add(this.sourceListGridView);
            this.groupBox1.Location = new System.Drawing.Point(40, 77);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(932, 323);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "供应商信息";
            // 
            // btn_SaveSourceList
            // 
            this.btn_SaveSourceList.Location = new System.Drawing.Point(840, 294);
            this.btn_SaveSourceList.Name = "btn_SaveSourceList";
            this.btn_SaveSourceList.Size = new System.Drawing.Size(75, 23);
            this.btn_SaveSourceList.TabIndex = 183;
            this.btn_SaveSourceList.Text = "保存";
            this.btn_SaveSourceList.UseVisualStyleBackColor = true;
            this.btn_SaveSourceList.Click += new System.EventHandler(this.btn_SaveSourceList_Click);
            // 
            // sourceListGridView
            // 
            this.sourceListGridView.BackgroundColor = System.Drawing.Color.White;
            this.sourceListGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sourceListGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.startTime,
            this.endTime,
            this.supplierId,
            this.createTime,
            this.purchaseOrg,
            this.ppl,
            this.protocolId,
            this.Inquiry_view6,
            this.Inquiry_view7,
            this.Inquiry_view8,
            this.Column3});
            this.sourceListGridView.Location = new System.Drawing.Point(6, 20);
            this.sourceListGridView.Name = "sourceListGridView";
            this.sourceListGridView.RowHeadersVisible = false;
            this.sourceListGridView.RowTemplate.Height = 23;
            this.sourceListGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sourceListGridView.Size = new System.Drawing.Size(915, 255);
            this.sourceListGridView.TabIndex = 182;
            this.sourceListGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sourceListGridView_CellContentClick);
            // 
            // Column4
            // 
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 20;
            // 
            // startTime
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.startTime.DefaultCellStyle = dataGridViewCellStyle1;
            this.startTime.Frozen = true;
            this.startTime.HeaderText = "有效从";
            this.startTime.Name = "startTime";
            this.startTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // endTime
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.endTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.endTime.Frozen = true;
            this.endTime.HeaderText = "有效至";
            this.endTime.Name = "endTime";
            this.endTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // supplierId
            // 
            this.supplierId.Frozen = true;
            this.supplierId.HeaderText = "供应商";
            this.supplierId.Name = "supplierId";
            // 
            // createTime
            // 
            this.createTime.Frozen = true;
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            // 
            // purchaseOrg
            // 
            this.purchaseOrg.Frozen = true;
            this.purchaseOrg.HeaderText = "采购组织";
            this.purchaseOrg.Name = "purchaseOrg";
            this.purchaseOrg.Width = 80;
            // 
            // ppl
            // 
            this.ppl.Frozen = true;
            this.ppl.HeaderText = "PPL";
            this.ppl.Name = "ppl";
            this.ppl.Width = 80;
            // 
            // protocolId
            // 
            this.protocolId.Frozen = true;
            this.protocolId.HeaderText = "协议号";
            this.protocolId.Name = "protocolId";
            // 
            // Inquiry_view6
            // 
            this.Inquiry_view6.Frozen = true;
            this.Inquiry_view6.HeaderText = "Fix";
            this.Inquiry_view6.Name = "Inquiry_view6";
            this.Inquiry_view6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Inquiry_view6.Width = 40;
            // 
            // Inquiry_view7
            // 
            this.Inquiry_view7.Frozen = true;
            this.Inquiry_view7.HeaderText = "Blk";
            this.Inquiry_view7.Name = "Inquiry_view7";
            this.Inquiry_view7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Inquiry_view7.Width = 40;
            // 
            // Inquiry_view8
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.NullValue = false;
            this.Inquiry_view8.DefaultCellStyle = dataGridViewCellStyle3;
            this.Inquiry_view8.Frozen = true;
            this.Inquiry_view8.HeaderText = "MRP";
            this.Inquiry_view8.Name = "Inquiry_view8";
            this.Inquiry_view8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Inquiry_view8.Width = 40;
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "MRP范围";
            this.Column3.Name = "Column3";
            // 
            // NewSourceList_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 454);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_FactoryId);
            this.Controls.Add(this.txt_MaterialId);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewSourceList_Form";
            this.Text = "新建货源清单";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sourceListGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_MaterialId;
        private System.Windows.Forms.TextBox txt_FactoryId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView sourceListGridView;
        private System.Windows.Forms.Button btn_SaveSourceList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseOrg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ppl;
        private System.Windows.Forms.DataGridViewTextBoxColumn protocolId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Inquiry_view6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Inquiry_view7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Inquiry_view8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}