﻿namespace MMClient.SourcingManage.SourcingManagement.Inquiry
{
    partial class AllInquiry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Sure = new System.Windows.Forms.Button();
            this.cmb_inquiryState = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.inquiryGridView = new System.Windows.Forms.DataGridView();
            this.inquiryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InquiryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inquiryGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_Sure);
            this.groupBox1.Controls.Add(this.cmb_inquiryState);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.inquiryGridView);
            this.groupBox1.Location = new System.Drawing.Point(43, 26);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(825, 427);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "询价单列表";
            // 
            // btn_Sure
            // 
            this.btn_Sure.Location = new System.Drawing.Point(421, 31);
            this.btn_Sure.Name = "btn_Sure";
            this.btn_Sure.Size = new System.Drawing.Size(75, 23);
            this.btn_Sure.TabIndex = 3;
            this.btn_Sure.Text = "查看";
            this.btn_Sure.UseVisualStyleBackColor = true;
            this.btn_Sure.Click += new System.EventHandler(this.btn_Sure_Click);
            // 
            // cmb_inquiryState
            // 
            this.cmb_inquiryState.FormattingEnabled = true;
            this.cmb_inquiryState.Items.AddRange(new object[] {
            "待维护",
            "待邀请",
            "待报价",
            "待比价",
            "已完成"});
            this.cmb_inquiryState.Location = new System.Drawing.Point(257, 33);
            this.cmb_inquiryState.Name = "cmb_inquiryState";
            this.cmb_inquiryState.Size = new System.Drawing.Size(121, 20);
            this.cmb_inquiryState.TabIndex = 2;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 36);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "询价单状态";
            // 
            // inquiryGridView
            // 
            this.inquiryGridView.AllowUserToAddRows = false;
            this.inquiryGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.inquiryGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inquiryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.inquiryGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inquiryId,
            this.InquiryName,
            this.createTime});
            this.inquiryGridView.EnableHeadersVisualStyles = false;
            this.inquiryGridView.Location = new System.Drawing.Point(124, 82);
            this.inquiryGridView.Name = "inquiryGridView";
            this.inquiryGridView.RowTemplate.Height = 23;
            this.inquiryGridView.Size = new System.Drawing.Size(579, 262);
            this.inquiryGridView.TabIndex = 0;
            this.inquiryGridView.SelectionChanged += new System.EventHandler(this.inquiryGridView_SelectionChanged);
            // 
            // inquiryId
            // 
            this.inquiryId.HeaderText = "询价单号";
            this.inquiryId.Name = "inquiryId";
            this.inquiryId.Width = 150;
            // 
            // InquiryName
            // 
            this.InquiryName.HeaderText = "询价单名称";
            this.InquiryName.Name = "InquiryName";
            this.InquiryName.Width = 120;
            // 
            // createTime
            // 
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.Width = 150;
            // 
            // AllInquiry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 516);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AllInquiry";
            this.Text = "询价单列表                                                                            " +
    "                ";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inquiryGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView inquiryGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn inquiryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn InquiryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.Button btn_Sure;
        private System.Windows.Forms.ComboBox cmb_inquiryState;
        private System.Windows.Forms.Label label1;
    }
}