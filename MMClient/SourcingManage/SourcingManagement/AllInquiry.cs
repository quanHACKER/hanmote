﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement.Inquiry
{
    public partial class AllInquiry : DockContent
    {
        private SourceBLL sourceBll;
        private SourceInquiryBLL sourceInquiryBll;

        public SourceInquiry offerSource;
        public SourceInquiry compareSource;

        public AllInquiry()
        {
            InitializeComponent();
            sourceBll = new SourceBLL();
            sourceInquiryBll = new SourceInquiryBLL();
            initData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            string item = "询价";
            List<Source> sourceList = sourceBll.findAllSources(item);
            inquiryGridView.Rows.Clear();
            if (sourceList.Count > inquiryGridView.Rows.Count)
            {
                inquiryGridView.Rows.Add(sourceList.Count - inquiryGridView.Rows.Count);
            }
            for (int i = 0; i < sourceList.Count; i++)
            {
                Source source = sourceList.ElementAt(i);
                inquiryGridView.Rows[i].Cells["inquiryId"].Value = source.Source_ID;
                inquiryGridView.Rows[i].Cells["InquiryName"].Value = source.Source_Name;
                inquiryGridView.Rows[i].Cells["createTime"].Value = source.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        private void btn_Sure_Click(object sender, EventArgs e)
        {
            inquiryGridView.Rows.Clear();
            string state = cmb_inquiryState.Text;
            int intState = getIntByState(state);
            fillBidGridView(intState);
        }

        private void fillBidGridView(int state)
        {
            List<SourceInquiry> list = sourceInquiryBll.findSourcesByState(state);
            if (list.Count > inquiryGridView.Rows.Count)
            {
                inquiryGridView.Rows.Add(list.Count - inquiryGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                inquiryGridView.Rows[i].Cells["inquiryId"].Value = list.ElementAt(i).Source_ID;
                Source source = sourceBll.findSourceById(list.ElementAt(i).Source_ID);
                inquiryGridView.Rows[i].Cells["InquiryName"].Value = source.Source_Name;
                inquiryGridView.Rows[i].Cells["CreateTime"].Value = source.Create_Time.ToString("yyyy-MM-dd HH:mm:ss");
                // inquiryGridView.Rows[i].Cells["bidState"].Value = getStringByNumber(list.ElementAt(i).BidState);
            }
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待维护"))
            {
                res = 0;
            }
            else if (state.Equals("待邀请"))
            {
                res = 1;
            }
            else if (state.Equals("待报价"))
            {
                res = 2;
            }
            else if (state.Equals("待比价"))
            {
                res = 3;
            }
            else if (state.Equals("已完成"))
            {
                res = 4;
            }
            return res;
        }

        private void inquiryGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = inquiryGridView.CurrentRow.Index;
            Object obj = inquiryGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            String sourceId = inquiryGridView.Rows[index].Cells[0].Value.ToString();
            offerSource = sourceInquiryBll.findSourceById(sourceId);
            compareSource = sourceInquiryBll.findSourceById(sourceId);
        }
    }
}
