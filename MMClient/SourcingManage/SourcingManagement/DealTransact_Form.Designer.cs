﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class DealTransact_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_SupplierId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_BidName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_BidId = new System.Windows.Forms.TextBox();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_Cost = new System.Windows.Forms.TabPage();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_Description = new System.Windows.Forms.TabPage();
            this.itemsGridView = new System.Windows.Forms.DataGridView();
            this.item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierAnswer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Save = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_TransResult = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tab_Cost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.tab_Description.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_SupplierId
            // 
            this.txt_SupplierId.Location = new System.Drawing.Point(642, 12);
            this.txt_SupplierId.Name = "txt_SupplierId";
            this.txt_SupplierId.ReadOnly = true;
            this.txt_SupplierId.Size = new System.Drawing.Size(141, 21);
            this.txt_SupplierId.TabIndex = 219;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(586, 15);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 218;
            this.label1.Text = "供应商";
            // 
            // txt_BidName
            // 
            this.txt_BidName.Location = new System.Drawing.Point(364, 13);
            this.txt_BidName.Name = "txt_BidName";
            this.txt_BidName.ReadOnly = true;
            this.txt_BidName.Size = new System.Drawing.Size(151, 21);
            this.txt_BidName.TabIndex = 217;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(294, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 215;
            this.label3.Text = "招标名称";
            // 
            // txt_BidId
            // 
            this.txt_BidId.Location = new System.Drawing.Point(118, 13);
            this.txt_BidId.Name = "txt_BidId";
            this.txt_BidId.ReadOnly = true;
            this.txt_BidId.Size = new System.Drawing.Size(152, 21);
            this.txt_BidId.TabIndex = 216;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(50, 16);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 214;
            this.xjdh_lbl.Text = "招标编号";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_Cost);
            this.tabControl1.Controls.Add(this.tab_Description);
            this.tabControl1.Location = new System.Drawing.Point(48, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(978, 355);
            this.tabControl1.TabIndex = 213;
            // 
            // tab_Cost
            // 
            this.tab_Cost.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Cost.Controls.Add(this.materialGridView);
            this.tab_Cost.Location = new System.Drawing.Point(4, 22);
            this.tab_Cost.Name = "tab_Cost";
            this.tab_Cost.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Cost.Size = new System.Drawing.Size(970, 329);
            this.tab_Cost.TabIndex = 0;
            this.tab_Cost.Text = "成本";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.count,
            this.unit,
            this.targetPrice,
            this.targetTotal,
            this.supplierPrice,
            this.supplierTotal,
            this.anno});
            this.materialGridView.Location = new System.Drawing.Point(6, 6);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 23;
            this.materialGridView.Size = new System.Drawing.Size(958, 305);
            this.materialGridView.TabIndex = 0;
            // 
            // materialId
            // 
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            // 
            // materialName
            // 
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // count
            // 
            this.count.HeaderText = "数量";
            this.count.Name = "count";
            this.count.Width = 80;
            // 
            // unit
            // 
            this.unit.HeaderText = "单位";
            this.unit.Name = "unit";
            this.unit.Width = 60;
            // 
            // targetPrice
            // 
            this.targetPrice.HeaderText = "目标价格";
            this.targetPrice.Name = "targetPrice";
            // 
            // targetTotal
            // 
            this.targetTotal.HeaderText = "合价目标";
            this.targetTotal.Name = "targetTotal";
            // 
            // supplierPrice
            // 
            this.supplierPrice.HeaderText = "供应商接受价格";
            this.supplierPrice.Name = "supplierPrice";
            this.supplierPrice.Width = 120;
            // 
            // supplierTotal
            // 
            this.supplierTotal.HeaderText = "供应商接受总价";
            this.supplierTotal.Name = "supplierTotal";
            this.supplierTotal.Width = 120;
            // 
            // anno
            // 
            this.anno.HeaderText = "注释";
            this.anno.Name = "anno";
            // 
            // tab_Description
            // 
            this.tab_Description.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Description.Controls.Add(this.itemsGridView);
            this.tab_Description.Location = new System.Drawing.Point(4, 22);
            this.tab_Description.Name = "tab_Description";
            this.tab_Description.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Description.Size = new System.Drawing.Size(970, 329);
            this.tab_Description.TabIndex = 1;
            this.tab_Description.Text = "说明";
            // 
            // itemsGridView
            // 
            this.itemsGridView.AllowUserToAddRows = false;
            this.itemsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.item,
            this.goalDescription,
            this.supplierAnswer,
            this.note});
            this.itemsGridView.Location = new System.Drawing.Point(41, 35);
            this.itemsGridView.Name = "itemsGridView";
            this.itemsGridView.RowTemplate.Height = 23;
            this.itemsGridView.Size = new System.Drawing.Size(851, 215);
            this.itemsGridView.TabIndex = 0;
            // 
            // item
            // 
            this.item.HeaderText = "说明项";
            this.item.Name = "item";
            // 
            // goalDescription
            // 
            this.goalDescription.HeaderText = "目标描述";
            this.goalDescription.Name = "goalDescription";
            this.goalDescription.Width = 200;
            // 
            // supplierAnswer
            // 
            this.supplierAnswer.HeaderText = "供应商应答";
            this.supplierAnswer.Name = "supplierAnswer";
            this.supplierAnswer.Width = 200;
            // 
            // note
            // 
            this.note.HeaderText = "注释";
            this.note.Name = "note";
            this.note.Width = 200;
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(911, 412);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 213;
            this.btn_Save.Text = "保存";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(704, 417);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 220;
            this.label2.Text = "谈判结果";
            // 
            // cmb_TransResult
            // 
            this.cmb_TransResult.FormattingEnabled = true;
            this.cmb_TransResult.Items.AddRange(new object[] {
            "接受",
            "拒绝"});
            this.cmb_TransResult.Location = new System.Drawing.Point(763, 414);
            this.cmb_TransResult.Name = "cmb_TransResult";
            this.cmb_TransResult.Size = new System.Drawing.Size(121, 20);
            this.cmb_TransResult.TabIndex = 221;
            // 
            // DealTransact_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 458);
            this.Controls.Add(this.cmb_TransResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_SupplierId);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_BidName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_BidId);
            this.Controls.Add(this.xjdh_lbl);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "DealTransact_Form";
            this.Text = "处理谈判";
            this.tabControl1.ResumeLayout(false);
            this.tab_Cost.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.tab_Description.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemsGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_SupplierId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_BidName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_BidId;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_Cost;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TabPage tab_Description;
        private System.Windows.Forms.DataGridView itemsGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn item;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierAnswer;
        private System.Windows.Forms.DataGridViewTextBoxColumn note;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_TransResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn anno;
    }
}