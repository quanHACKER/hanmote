﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Source_List_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Source_List_Search));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.delete_lbl = new System.Windows.Forms.Label();
            this.delete_pb = new System.Windows.Forms.PictureBox();
            this.edit_lbl = new System.Windows.Forms.Label();
            this.edit_pb = new System.Windows.Forms.PictureBox();
            this.close_lbl = new System.Windows.Forms.Label();
            this.close_pb = new System.Windows.Forms.PictureBox();
            this.read_lbl = new System.Windows.Forms.Label();
            this.read_pb = new System.Windows.Forms.PictureBox();
            this.activate_tb = new System.Windows.Forms.TextBox();
            this.gc_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cgz_cmb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.hyzt_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.xy_cmb = new System.Windows.Forms.ComboBox();
            this.update_lbl = new System.Windows.Forms.Label();
            this.update_date = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.gys_cmb = new System.Windows.Forms.ComboBox();
            this.MRP_check = new System.Windows.Forms.CheckBox();
            this.end_lbl = new System.Windows.Forms.Label();
            this.end_date = new System.Windows.Forms.DateTimePicker();
            this.begin_lbl = new System.Windows.Forms.Label();
            this.begin_date = new System.Windows.Forms.DateTimePicker();
            this.xy_Button3 = new System.Windows.Forms.RadioButton();
            this.xy_Button2 = new System.Windows.Forms.RadioButton();
            this.xy_Button1 = new System.Windows.Forms.RadioButton();
            this.source_view = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveButton = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).BeginInit();
            this.SuspendLayout();
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.Location = new System.Drawing.Point(50, 9);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(160, 22);
            this.wlbh_cmb.TabIndex = 163;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(15, 14);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(29, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "物料";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.delete_lbl);
            this.panel1.Controls.Add(this.delete_pb);
            this.panel1.Controls.Add(this.edit_lbl);
            this.panel1.Controls.Add(this.edit_pb);
            this.panel1.Controls.Add(this.close_lbl);
            this.panel1.Controls.Add(this.close_pb);
            this.panel1.Controls.Add(this.read_lbl);
            this.panel1.Controls.Add(this.read_pb);
            this.panel1.Controls.Add(this.activate_tb);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(622, 63);
            this.panel1.TabIndex = 167;
            // 
            // delete_lbl
            // 
            this.delete_lbl.AutoSize = true;
            this.delete_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.delete_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.delete_lbl.Location = new System.Drawing.Point(10, 46);
            this.delete_lbl.Name = "delete_lbl";
            this.delete_lbl.Size = new System.Drawing.Size(29, 12);
            this.delete_lbl.TabIndex = 111;
            this.delete_lbl.Text = "删除";
            // 
            // delete_pb
            // 
            this.delete_pb.Image = ((System.Drawing.Image)(resources.GetObject("delete_pb.Image")));
            this.delete_pb.Location = new System.Drawing.Point(4, 3);
            this.delete_pb.Name = "delete_pb";
            this.delete_pb.Size = new System.Drawing.Size(40, 40);
            this.delete_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.delete_pb.TabIndex = 110;
            this.delete_pb.TabStop = false;
            this.delete_pb.Click += new System.EventHandler(this.delete_pb_Click);
            // 
            // edit_lbl
            // 
            this.edit_lbl.AutoSize = true;
            this.edit_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.edit_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.edit_lbl.Location = new System.Drawing.Point(50, 46);
            this.edit_lbl.Name = "edit_lbl";
            this.edit_lbl.Size = new System.Drawing.Size(29, 12);
            this.edit_lbl.TabIndex = 109;
            this.edit_lbl.Text = "更改";
            // 
            // edit_pb
            // 
            this.edit_pb.Image = ((System.Drawing.Image)(resources.GetObject("edit_pb.Image")));
            this.edit_pb.Location = new System.Drawing.Point(46, 3);
            this.edit_pb.Name = "edit_pb";
            this.edit_pb.Size = new System.Drawing.Size(40, 40);
            this.edit_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.edit_pb.TabIndex = 108;
            this.edit_pb.TabStop = false;
            this.edit_pb.Click += new System.EventHandler(this.edit_pb_Click);
            // 
            // close_lbl
            // 
            this.close_lbl.AutoSize = true;
            this.close_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.close_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.close_lbl.Location = new System.Drawing.Point(136, 46);
            this.close_lbl.Name = "close_lbl";
            this.close_lbl.Size = new System.Drawing.Size(29, 12);
            this.close_lbl.TabIndex = 106;
            this.close_lbl.Text = "关闭";
            // 
            // close_pb
            // 
            this.close_pb.Image = ((System.Drawing.Image)(resources.GetObject("close_pb.Image")));
            this.close_pb.Location = new System.Drawing.Point(130, 3);
            this.close_pb.Name = "close_pb";
            this.close_pb.Size = new System.Drawing.Size(40, 40);
            this.close_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.close_pb.TabIndex = 105;
            this.close_pb.TabStop = false;
            this.close_pb.Click += new System.EventHandler(this.close_pb_Click);
            // 
            // read_lbl
            // 
            this.read_lbl.AutoSize = true;
            this.read_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.read_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.read_lbl.Location = new System.Drawing.Point(94, 46);
            this.read_lbl.Name = "read_lbl";
            this.read_lbl.Size = new System.Drawing.Size(29, 12);
            this.read_lbl.TabIndex = 102;
            this.read_lbl.Text = "查询";
            // 
            // read_pb
            // 
            this.read_pb.Image = ((System.Drawing.Image)(resources.GetObject("read_pb.Image")));
            this.read_pb.Location = new System.Drawing.Point(88, 3);
            this.read_pb.Name = "read_pb";
            this.read_pb.Size = new System.Drawing.Size(40, 40);
            this.read_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.read_pb.TabIndex = 92;
            this.read_pb.TabStop = false;
            this.read_pb.Click += new System.EventHandler(this.read_pb_Click);
            // 
            // activate_tb
            // 
            this.activate_tb.BackColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.activate_tb.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.Location = new System.Drawing.Point(7, 5);
            this.activate_tb.Name = "activate_tb";
            this.activate_tb.Size = new System.Drawing.Size(100, 14);
            this.activate_tb.TabIndex = 107;
            // 
            // gc_cmb
            // 
            this.gc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gc_cmb.FormattingEnabled = true;
            this.gc_cmb.Location = new System.Drawing.Point(50, 37);
            this.gc_cmb.Name = "gc_cmb";
            this.gc_cmb.Size = new System.Drawing.Size(100, 22);
            this.gc_cmb.TabIndex = 170;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(15, 41);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "工厂";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(7, 158);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 15);
            this.label23.TabIndex = 176;
            this.label23.Text = "货源清单记录";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cgz_cmb);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.hyzt_cmb);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.xy_cmb);
            this.panel2.Controls.Add(this.update_lbl);
            this.panel2.Controls.Add(this.update_date);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.gys_cmb);
            this.panel2.Controls.Add(this.MRP_check);
            this.panel2.Controls.Add(this.end_lbl);
            this.panel2.Controls.Add(this.end_date);
            this.panel2.Controls.Add(this.begin_lbl);
            this.panel2.Controls.Add(this.begin_date);
            this.panel2.Controls.Add(this.xy_Button3);
            this.panel2.Controls.Add(this.xy_Button2);
            this.panel2.Controls.Add(this.xy_Button1);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.wlbh_cmb);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.gc_cmb);
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(912, 95);
            this.panel2.TabIndex = 180;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(443, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 199;
            this.label5.Text = "采购组";
            // 
            // cgz_cmb
            // 
            this.cgz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgz_cmb.FormattingEnabled = true;
            this.cgz_cmb.Location = new System.Drawing.Point(490, 9);
            this.cgz_cmb.Name = "cgz_cmb";
            this.cgz_cmb.Size = new System.Drawing.Size(100, 22);
            this.cgz_cmb.TabIndex = 200;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(431, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 197;
            this.label8.Text = "货源状态";
            // 
            // hyzt_cmb
            // 
            this.hyzt_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hyzt_cmb.FormattingEnabled = true;
            this.hyzt_cmb.Items.AddRange(new object[] {
            "Fix",
            "Blk",
            "已失效",
            "已冻结",
            "已删除"});
            this.hyzt_cmb.Location = new System.Drawing.Point(490, 65);
            this.hyzt_cmb.Name = "hyzt_cmb";
            this.hyzt_cmb.Size = new System.Drawing.Size(100, 22);
            this.hyzt_cmb.TabIndex = 198;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(455, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 195;
            this.label7.Text = "协议";
            // 
            // xy_cmb
            // 
            this.xy_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xy_cmb.FormattingEnabled = true;
            this.xy_cmb.Location = new System.Drawing.Point(490, 37);
            this.xy_cmb.Name = "xy_cmb";
            this.xy_cmb.Size = new System.Drawing.Size(100, 22);
            this.xy_cmb.TabIndex = 196;
            // 
            // update_lbl
            // 
            this.update_lbl.AutoSize = true;
            this.update_lbl.Location = new System.Drawing.Point(241, 70);
            this.update_lbl.Name = "update_lbl";
            this.update_lbl.Size = new System.Drawing.Size(53, 12);
            this.update_lbl.TabIndex = 194;
            this.update_lbl.Text = "更新日期";
            this.update_lbl.Click += new System.EventHandler(this.update_lbl_Click);
            // 
            // update_date
            // 
            this.update_date.CustomFormat = " ";
            this.update_date.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.update_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.update_date.Location = new System.Drawing.Point(300, 65);
            this.update_date.Name = "update_date";
            this.update_date.Size = new System.Drawing.Size(100, 23);
            this.update_date.TabIndex = 193;
            this.update_date.ValueChanged += new System.EventHandler(this.update_date_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 191;
            this.label2.Text = "供应商";
            // 
            // gys_cmb
            // 
            this.gys_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gys_cmb.FormattingEnabled = true;
            this.gys_cmb.Location = new System.Drawing.Point(50, 65);
            this.gys_cmb.Name = "gys_cmb";
            this.gys_cmb.Size = new System.Drawing.Size(120, 22);
            this.gys_cmb.TabIndex = 192;
            // 
            // MRP_check
            // 
            this.MRP_check.AutoSize = true;
            this.MRP_check.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MRP_check.Location = new System.Drawing.Point(745, 12);
            this.MRP_check.Name = "MRP_check";
            this.MRP_check.Size = new System.Drawing.Size(66, 16);
            this.MRP_check.TabIndex = 190;
            this.MRP_check.Text = "MRP标识";
            this.MRP_check.UseVisualStyleBackColor = true;
            // 
            // end_lbl
            // 
            this.end_lbl.AutoSize = true;
            this.end_lbl.Location = new System.Drawing.Point(253, 42);
            this.end_lbl.Name = "end_lbl";
            this.end_lbl.Size = new System.Drawing.Size(41, 12);
            this.end_lbl.TabIndex = 189;
            this.end_lbl.Text = "有效至";
            this.end_lbl.Click += new System.EventHandler(this.end_lbl_Click);
            // 
            // end_date
            // 
            this.end_date.CustomFormat = " ";
            this.end_date.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.end_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_date.Location = new System.Drawing.Point(300, 37);
            this.end_date.Name = "end_date";
            this.end_date.Size = new System.Drawing.Size(100, 23);
            this.end_date.TabIndex = 188;
            this.end_date.ValueChanged += new System.EventHandler(this.end_date_ValueChanged);
            // 
            // begin_lbl
            // 
            this.begin_lbl.AutoSize = true;
            this.begin_lbl.Location = new System.Drawing.Point(253, 14);
            this.begin_lbl.Name = "begin_lbl";
            this.begin_lbl.Size = new System.Drawing.Size(41, 12);
            this.begin_lbl.TabIndex = 187;
            this.begin_lbl.Text = "有效从";
            this.begin_lbl.Click += new System.EventHandler(this.begin_lbl_Click);
            // 
            // begin_date
            // 
            this.begin_date.CustomFormat = " ";
            this.begin_date.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.begin_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.begin_date.Location = new System.Drawing.Point(300, 9);
            this.begin_date.Name = "begin_date";
            this.begin_date.Size = new System.Drawing.Size(100, 23);
            this.begin_date.TabIndex = 186;
            this.begin_date.ValueChanged += new System.EventHandler(this.begin_date_ValueChanged);
            // 
            // xy_Button3
            // 
            this.xy_Button3.AutoSize = true;
            this.xy_Button3.Location = new System.Drawing.Point(626, 63);
            this.xy_Button3.Name = "xy_Button3";
            this.xy_Button3.Size = new System.Drawing.Size(83, 16);
            this.xy_Button3.TabIndex = 185;
            this.xy_Button3.TabStop = true;
            this.xy_Button3.Text = "仅框架协议";
            this.xy_Button3.UseVisualStyleBackColor = true;
            // 
            // xy_Button2
            // 
            this.xy_Button2.AutoSize = true;
            this.xy_Button2.Location = new System.Drawing.Point(626, 37);
            this.xy_Button2.Name = "xy_Button2";
            this.xy_Button2.Size = new System.Drawing.Size(95, 16);
            this.xy_Button2.TabIndex = 184;
            this.xy_Button2.TabStop = true;
            this.xy_Button2.Text = "排除框架协议";
            this.xy_Button2.UseVisualStyleBackColor = true;
            // 
            // xy_Button1
            // 
            this.xy_Button1.AutoSize = true;
            this.xy_Button1.Location = new System.Drawing.Point(626, 11);
            this.xy_Button1.Name = "xy_Button1";
            this.xy_Button1.Size = new System.Drawing.Size(71, 16);
            this.xy_Button1.TabIndex = 183;
            this.xy_Button1.TabStop = true;
            this.xy_Button1.Text = "所有记录";
            this.xy_Button1.UseVisualStyleBackColor = true;
            // 
            // source_view
            // 
            this.source_view.BackgroundColor = System.Drawing.Color.White;
            this.source_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.source_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.Column2,
            this.Inquiry_view3,
            this.Inquiry_view1,
            this.Inquiry_view2,
            this.Inquiry_view4,
            this.Inquiry_view11,
            this.Inquiry_view5,
            this.Inquiry_view6,
            this.Inquiry_view8,
            this.Column3,
            this.Column5});
            this.source_view.Location = new System.Drawing.Point(7, 180);
            this.source_view.Name = "source_view";
            this.source_view.RowHeadersVisible = false;
            this.source_view.RowTemplate.Height = 23;
            this.source_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.source_view.Size = new System.Drawing.Size(905, 255);
            this.source_view.TabIndex = 181;
            // 
            // Column4
            // 
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column4.Width = 20;
            // 
            // Column1
            // 
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "物料";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "工厂";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 60;
            // 
            // Inquiry_view3
            // 
            this.Inquiry_view3.Frozen = true;
            this.Inquiry_view3.HeaderText = "供应商";
            this.Inquiry_view3.Name = "Inquiry_view3";
            this.Inquiry_view3.ReadOnly = true;
            this.Inquiry_view3.Width = 80;
            // 
            // Inquiry_view1
            // 
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            this.Inquiry_view1.DefaultCellStyle = dataGridViewCellStyle37;
            this.Inquiry_view1.Frozen = true;
            this.Inquiry_view1.HeaderText = "有效从";
            this.Inquiry_view1.Name = "Inquiry_view1";
            this.Inquiry_view1.ReadOnly = true;
            this.Inquiry_view1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view1.Width = 80;
            // 
            // Inquiry_view2
            // 
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            this.Inquiry_view2.DefaultCellStyle = dataGridViewCellStyle38;
            this.Inquiry_view2.Frozen = true;
            this.Inquiry_view2.HeaderText = "有效至";
            this.Inquiry_view2.Name = "Inquiry_view2";
            this.Inquiry_view2.ReadOnly = true;
            this.Inquiry_view2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view2.Width = 80;
            // 
            // Inquiry_view4
            // 
            this.Inquiry_view4.Frozen = true;
            this.Inquiry_view4.HeaderText = "采购组";
            this.Inquiry_view4.Name = "Inquiry_view4";
            this.Inquiry_view4.ReadOnly = true;
            this.Inquiry_view4.Width = 80;
            // 
            // Inquiry_view11
            // 
            this.Inquiry_view11.Frozen = true;
            this.Inquiry_view11.HeaderText = "PP1";
            this.Inquiry_view11.Name = "Inquiry_view11";
            this.Inquiry_view11.ReadOnly = true;
            this.Inquiry_view11.Width = 40;
            // 
            // Inquiry_view5
            // 
            this.Inquiry_view5.Frozen = true;
            this.Inquiry_view5.HeaderText = "协议";
            this.Inquiry_view5.Name = "Inquiry_view5";
            this.Inquiry_view5.ReadOnly = true;
            this.Inquiry_view5.Width = 80;
            // 
            // Inquiry_view6
            // 
            this.Inquiry_view6.Frozen = true;
            this.Inquiry_view6.HeaderText = "货源状态";
            this.Inquiry_view6.Name = "Inquiry_view6";
            this.Inquiry_view6.ReadOnly = true;
            this.Inquiry_view6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view6.Width = 80;
            // 
            // Inquiry_view8
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.NullValue = false;
            this.Inquiry_view8.DefaultCellStyle = dataGridViewCellStyle39;
            this.Inquiry_view8.Frozen = true;
            this.Inquiry_view8.HeaderText = "MRP";
            this.Inquiry_view8.Name = "Inquiry_view8";
            this.Inquiry_view8.ReadOnly = true;
            this.Inquiry_view8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Inquiry_view8.Width = 40;
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "MRP范围";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "更新时间";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 80;
            // 
            // saveButton
            // 
            this.saveButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveButton.Location = new System.Drawing.Point(849, 153);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(63, 23);
            this.saveButton.TabIndex = 195;
            this.saveButton.Text = "批量保存";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // Source_List_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1046, 729);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.source_view);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Source_List_Search";
            this.Text = "货源清单记录查询";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label read_lbl;
        private System.Windows.Forms.PictureBox read_pb;
        private System.Windows.Forms.ComboBox gc_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView source_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Label close_lbl;
        private System.Windows.Forms.PictureBox close_pb;
        private System.Windows.Forms.TextBox activate_tb;
        private System.Windows.Forms.Label edit_lbl;
        private System.Windows.Forms.PictureBox edit_pb;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox hyzt_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox xy_cmb;
        private System.Windows.Forms.Label update_lbl;
        private System.Windows.Forms.DateTimePicker update_date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox gys_cmb;
        private System.Windows.Forms.CheckBox MRP_check;
        private System.Windows.Forms.Label end_lbl;
        private System.Windows.Forms.DateTimePicker end_date;
        private System.Windows.Forms.Label begin_lbl;
        private System.Windows.Forms.DateTimePicker begin_date;
        private System.Windows.Forms.RadioButton xy_Button3;
        private System.Windows.Forms.RadioButton xy_Button2;
        private System.Windows.Forms.RadioButton xy_Button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cgz_cmb;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Inquiry_view8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label delete_lbl;
        private System.Windows.Forms.PictureBox delete_pb;
    }
}