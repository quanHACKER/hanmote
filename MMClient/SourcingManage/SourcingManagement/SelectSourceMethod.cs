﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SelectSourceMethod_Form : DockContent
    {
        public UserUI userUI;

        public SelectSourceMethod_Form(UserUI userUi)
        {
            InitializeComponent();
            this.userUI = userUi;
        }

        /// <summary>
        /// 根据采购单号选择寻源方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void detailStandardByDemand_ID(DataTable dt)
        {
            this.xmName.Text = "关于" + dt.Rows[0][2].ToString() + "采购项目";
            this.Text = dt.Rows[0][0].ToString() + "选择寻源";
            this.xqdh_tb.Text = dt.Rows[0][0].ToString();
            this.wlbh_cmb.Text = dt.Rows[0][1].ToString();
            this.wlmc_cmb.Text = dt.Rows[0][2].ToString();
            this.cglx_cmb.Text = dt.Rows[0][3].ToString();
            this.cgjg_tb.Text = dt.Rows[0][8].ToString();
            this.cqbh_cmb.Text = dt.Rows[0][5].ToString();
            this.jldw_tb.Text = dt.Rows[0][6].ToString();
            this.xqsl_tb.Text = dt.Rows[0][8].ToString();
            this.sqbm_cmb.Text = dt.Rows[0][10].ToString();
            this.sqr_cmb.Text = dt.Rows[0][11].ToString();
            this.lxdh_tb.Text = dt.Rows[0][12].ToString();
            this.sqrq_dt.Text = dt.Rows[0][9].ToString();
            this.qtyq_rtb.Text = dt.Rows[0][13].ToString();
        }

        private void xyfs_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.xyfs_cmb.Text.Equals("正式招标法"))
            {
                this.zbff_cmb.Visible = false;
                this.lbl_zbff.Visible = false;
            }
            else 
            {
                this.zbff_cmb.Visible = true;
                this.lbl_zbff.Visible = true;
            }

        }

        private void cancel_bt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void submit_bt_Click(object sender, EventArgs e)
        {
            if (this.xyfs_cmb.Text.Equals("正式招标法") && this.zbff_cmb.Text.Equals("两段式招标"))
            {
                //TwiceMethod_Form TwiceMethod = new TwiceMethod_Form(userUI);
                ////TwiceMethod.detailStandardByDemand_ID(checkDt);
                //SingletonUserUI.addToUserUI(TwiceMethod);
            }
            else if (this.xyfs_cmb.Text.Equals("正式招标法") && this.zbff_cmb.Text.Equals("公开招标"))
            {
                GeneralMethod_Form GeneralMethod = new GeneralMethod_Form(userUI);
                //TwiceMethod.detailStandardByDemand_ID(checkDt);
                SingletonUserUI.addToUserUI(GeneralMethod);
            }
        }
    }
}
