﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class GeneralMethod_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.refresh_bt = new System.Windows.Forms.Button();
            this.fabu_bt = new System.Windows.Forms.Button();
            this.zbzl_bt = new System.Windows.Forms.Button();
            this.del_bt = new System.Windows.Forms.Button();
            this.save_bt = new System.Windows.Forms.Button();
            this.close_bt = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbkfrq_dt = new System.Windows.Forms.DateTimePicker();
            this.label43 = new System.Windows.Forms.Label();
            this.tbtjrq_dt = new System.Windows.Forms.DateTimePicker();
            this.label42 = new System.Windows.Forms.Label();
            this.tbksrq_dt = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tbmbz_tb = new System.Windows.Forms.TextBox();
            this.tbhxpz_cmb = new System.Windows.Forms.ComboBox();
            this.tbhb_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tbcgz_cmb = new System.Windows.Forms.ComboBox();
            this.tbcgzz_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.tbradd_bt = new System.Windows.Forms.Button();
            this.tbrdel_bt = new System.Windows.Forms.Button();
            this.tbrtz_bt = new System.Windows.Forms.Button();
            this.tbrbs_tb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label100 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox126 = new System.Windows.Forms.TextBox();
            this.textBox148 = new System.Windows.Forms.TextBox();
            this.textBox149 = new System.Windows.Forms.TextBox();
            this.textBox150 = new System.Windows.Forms.TextBox();
            this.textBox151 = new System.Windows.Forms.TextBox();
            this.textBox152 = new System.Windows.Forms.TextBox();
            this.textBox153 = new System.Windows.Forms.TextBox();
            this.textBox154 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.textBox155 = new System.Windows.Forms.TextBox();
            this.textBox156 = new System.Windows.Forms.TextBox();
            this.textBox157 = new System.Windows.Forms.TextBox();
            this.textBox158 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.textBox159 = new System.Windows.Forms.TextBox();
            this.textBox160 = new System.Windows.Forms.TextBox();
            this.textBox161 = new System.Windows.Forms.TextBox();
            this.textBox162 = new System.Windows.Forms.TextBox();
            this.textBox163 = new System.Windows.Forms.TextBox();
            this.textBox164 = new System.Windows.Forms.TextBox();
            this.textBox165 = new System.Windows.Forms.TextBox();
            this.textBox166 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.textBox111 = new System.Windows.Forms.TextBox();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.textBox127 = new System.Windows.Forms.TextBox();
            this.textBox128 = new System.Windows.Forms.TextBox();
            this.textBox129 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox130 = new System.Windows.Forms.TextBox();
            this.textBox131 = new System.Windows.Forms.TextBox();
            this.textBox132 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.textBox133 = new System.Windows.Forms.TextBox();
            this.textBox134 = new System.Windows.Forms.TextBox();
            this.textBox135 = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.textBox136 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox137 = new System.Windows.Forms.TextBox();
            this.textBox138 = new System.Windows.Forms.TextBox();
            this.textBox139 = new System.Windows.Forms.TextBox();
            this.textBox140 = new System.Windows.Forms.TextBox();
            this.textBox141 = new System.Windows.Forms.TextBox();
            this.textBox142 = new System.Windows.Forms.TextBox();
            this.textBox143 = new System.Windows.Forms.TextBox();
            this.textBox144 = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.textBox145 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.textBox146 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.textBox147 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox121 = new System.Windows.Forms.TextBox();
            this.textBox122 = new System.Windows.Forms.TextBox();
            this.textBox123 = new System.Windows.Forms.TextBox();
            this.textBox124 = new System.Windows.Forms.TextBox();
            this.textBox125 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.shp2_lbl = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.hlca_ed = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.zd_lbl = new System.Windows.Forms.Label();
            this.hlbc_ed = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.hlab_ed = new System.Windows.Forms.TextBox();
            this.hlca_zx = new System.Windows.Forms.TextBox();
            this.zx_lbl = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.qlbc_zd = new System.Windows.Forms.TextBox();
            this.qlbc_zx = new System.Windows.Forms.TextBox();
            this.hlbc_zx = new System.Windows.Forms.TextBox();
            this.hlbc_zd = new System.Windows.Forms.TextBox();
            this.hlab_zx = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox261 = new System.Windows.Forms.TextBox();
            this.textBox262 = new System.Windows.Forms.TextBox();
            this.textBox263 = new System.Windows.Forms.TextBox();
            this.textBox264 = new System.Windows.Forms.TextBox();
            this.textBox265 = new System.Windows.Forms.TextBox();
            this.textBox266 = new System.Windows.Forms.TextBox();
            this.textBox267 = new System.Windows.Forms.TextBox();
            this.textBox268 = new System.Windows.Forms.TextBox();
            this.textBox269 = new System.Windows.Forms.TextBox();
            this.textBox270 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox271 = new System.Windows.Forms.TextBox();
            this.textBox272 = new System.Windows.Forms.TextBox();
            this.textBox273 = new System.Windows.Forms.TextBox();
            this.textBox274 = new System.Windows.Forms.TextBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.textBox279 = new System.Windows.Forms.TextBox();
            this.textBox280 = new System.Windows.Forms.TextBox();
            this.textBox281 = new System.Windows.Forms.TextBox();
            this.textBox282 = new System.Windows.Forms.TextBox();
            this.textBox283 = new System.Windows.Forms.TextBox();
            this.textBox284 = new System.Windows.Forms.TextBox();
            this.textBox285 = new System.Windows.Forms.TextBox();
            this.textBox286 = new System.Windows.Forms.TextBox();
            this.textBox287 = new System.Windows.Forms.TextBox();
            this.textBox288 = new System.Windows.Forms.TextBox();
            this.textBox289 = new System.Windows.Forms.TextBox();
            this.textBox290 = new System.Windows.Forms.TextBox();
            this.textBox291 = new System.Windows.Forms.TextBox();
            this.textBox292 = new System.Windows.Forms.TextBox();
            this.textBox293 = new System.Windows.Forms.TextBox();
            this.textBox294 = new System.Windows.Forms.TextBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.textBox299 = new System.Windows.Forms.TextBox();
            this.textBox300 = new System.Windows.Forms.TextBox();
            this.textBox301 = new System.Windows.Forms.TextBox();
            this.textBox302 = new System.Windows.Forms.TextBox();
            this.textBox303 = new System.Windows.Forms.TextBox();
            this.textBox304 = new System.Windows.Forms.TextBox();
            this.textBox305 = new System.Windows.Forms.TextBox();
            this.textBox306 = new System.Windows.Forms.TextBox();
            this.textBox307 = new System.Windows.Forms.TextBox();
            this.textBox308 = new System.Windows.Forms.TextBox();
            this.textBox309 = new System.Windows.Forms.TextBox();
            this.textBox310 = new System.Windows.Forms.TextBox();
            this.textBox311 = new System.Windows.Forms.TextBox();
            this.textBox312 = new System.Windows.Forms.TextBox();
            this.textBox313 = new System.Windows.Forms.TextBox();
            this.textBox314 = new System.Windows.Forms.TextBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.textBox319 = new System.Windows.Forms.TextBox();
            this.textBox320 = new System.Windows.Forms.TextBox();
            this.textBox321 = new System.Windows.Forms.TextBox();
            this.textBox322 = new System.Windows.Forms.TextBox();
            this.textBox323 = new System.Windows.Forms.TextBox();
            this.textBox324 = new System.Windows.Forms.TextBox();
            this.textBox325 = new System.Windows.Forms.TextBox();
            this.textBox326 = new System.Windows.Forms.TextBox();
            this.textBox327 = new System.Windows.Forms.TextBox();
            this.textBox328 = new System.Windows.Forms.TextBox();
            this.textBox329 = new System.Windows.Forms.TextBox();
            this.textBox330 = new System.Windows.Forms.TextBox();
            this.textBox331 = new System.Windows.Forms.TextBox();
            this.textBox332 = new System.Windows.Forms.TextBox();
            this.textBox333 = new System.Windows.Forms.TextBox();
            this.textBox334 = new System.Windows.Forms.TextBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBox339 = new System.Windows.Forms.TextBox();
            this.textBox340 = new System.Windows.Forms.TextBox();
            this.textBox341 = new System.Windows.Forms.TextBox();
            this.textBox342 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox343 = new System.Windows.Forms.TextBox();
            this.textBox344 = new System.Windows.Forms.TextBox();
            this.textBox345 = new System.Windows.Forms.TextBox();
            this.textBox346 = new System.Windows.Forms.TextBox();
            this.textBox347 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.textBox348 = new System.Windows.Forms.TextBox();
            this.textBox349 = new System.Windows.Forms.TextBox();
            this.textBox350 = new System.Windows.Forms.TextBox();
            this.textBox351 = new System.Windows.Forms.TextBox();
            this.textBox352 = new System.Windows.Forms.TextBox();
            this.textBox353 = new System.Windows.Forms.TextBox();
            this.textBox354 = new System.Windows.Forms.TextBox();
            this.sjbg_panel = new System.Windows.Forms.Panel();
            this.textBox243 = new System.Windows.Forms.TextBox();
            this.textBox244 = new System.Windows.Forms.TextBox();
            this.textBox245 = new System.Windows.Forms.TextBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.textBox246 = new System.Windows.Forms.TextBox();
            this.textBox247 = new System.Windows.Forms.TextBox();
            this.textBox248 = new System.Windows.Forms.TextBox();
            this.textBox249 = new System.Windows.Forms.TextBox();
            this.textBox250 = new System.Windows.Forms.TextBox();
            this.textBox251 = new System.Windows.Forms.TextBox();
            this.textBox252 = new System.Windows.Forms.TextBox();
            this.textBox253 = new System.Windows.Forms.TextBox();
            this.textBox254 = new System.Windows.Forms.TextBox();
            this.textBox255 = new System.Windows.Forms.TextBox();
            this.textBox256 = new System.Windows.Forms.TextBox();
            this.textBox257 = new System.Windows.Forms.TextBox();
            this.textBox258 = new System.Windows.Forms.TextBox();
            this.textBox259 = new System.Windows.Forms.TextBox();
            this.textBox260 = new System.Windows.Forms.TextBox();
            this.textBox227 = new System.Windows.Forms.TextBox();
            this.textBox228 = new System.Windows.Forms.TextBox();
            this.textBox229 = new System.Windows.Forms.TextBox();
            this.textBox230 = new System.Windows.Forms.TextBox();
            this.textBox231 = new System.Windows.Forms.TextBox();
            this.textBox232 = new System.Windows.Forms.TextBox();
            this.textBox233 = new System.Windows.Forms.TextBox();
            this.textBox234 = new System.Windows.Forms.TextBox();
            this.textBox235 = new System.Windows.Forms.TextBox();
            this.textBox236 = new System.Windows.Forms.TextBox();
            this.textBox237 = new System.Windows.Forms.TextBox();
            this.textBox238 = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.textBox239 = new System.Windows.Forms.TextBox();
            this.textBox240 = new System.Windows.Forms.TextBox();
            this.textBox241 = new System.Windows.Forms.TextBox();
            this.textBox242 = new System.Windows.Forms.TextBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.textBox187 = new System.Windows.Forms.TextBox();
            this.textBox188 = new System.Windows.Forms.TextBox();
            this.textBox189 = new System.Windows.Forms.TextBox();
            this.textBox190 = new System.Windows.Forms.TextBox();
            this.textBox191 = new System.Windows.Forms.TextBox();
            this.textBox192 = new System.Windows.Forms.TextBox();
            this.textBox193 = new System.Windows.Forms.TextBox();
            this.textBox194 = new System.Windows.Forms.TextBox();
            this.textBox195 = new System.Windows.Forms.TextBox();
            this.textBox196 = new System.Windows.Forms.TextBox();
            this.textBox197 = new System.Windows.Forms.TextBox();
            this.textBox198 = new System.Windows.Forms.TextBox();
            this.textBox199 = new System.Windows.Forms.TextBox();
            this.textBox200 = new System.Windows.Forms.TextBox();
            this.textBox201 = new System.Windows.Forms.TextBox();
            this.textBox202 = new System.Windows.Forms.TextBox();
            this.textBox203 = new System.Windows.Forms.TextBox();
            this.textBox204 = new System.Windows.Forms.TextBox();
            this.textBox205 = new System.Windows.Forms.TextBox();
            this.textBox206 = new System.Windows.Forms.TextBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.textBox207 = new System.Windows.Forms.TextBox();
            this.textBox208 = new System.Windows.Forms.TextBox();
            this.textBox209 = new System.Windows.Forms.TextBox();
            this.textBox210 = new System.Windows.Forms.TextBox();
            this.textBox211 = new System.Windows.Forms.TextBox();
            this.textBox212 = new System.Windows.Forms.TextBox();
            this.textBox213 = new System.Windows.Forms.TextBox();
            this.textBox214 = new System.Windows.Forms.TextBox();
            this.textBox215 = new System.Windows.Forms.TextBox();
            this.textBox216 = new System.Windows.Forms.TextBox();
            this.textBox217 = new System.Windows.Forms.TextBox();
            this.textBox218 = new System.Windows.Forms.TextBox();
            this.textBox219 = new System.Windows.Forms.TextBox();
            this.textBox220 = new System.Windows.Forms.TextBox();
            this.textBox221 = new System.Windows.Forms.TextBox();
            this.textBox222 = new System.Windows.Forms.TextBox();
            this.textBox223 = new System.Windows.Forms.TextBox();
            this.textBox224 = new System.Windows.Forms.TextBox();
            this.textBox225 = new System.Windows.Forms.TextBox();
            this.textBox226 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.textBox167 = new System.Windows.Forms.TextBox();
            this.textBox168 = new System.Windows.Forms.TextBox();
            this.textBox169 = new System.Windows.Forms.TextBox();
            this.textBox170 = new System.Windows.Forms.TextBox();
            this.textBox171 = new System.Windows.Forms.TextBox();
            this.textBox172 = new System.Windows.Forms.TextBox();
            this.textBox173 = new System.Windows.Forms.TextBox();
            this.textBox174 = new System.Windows.Forms.TextBox();
            this.textBox175 = new System.Windows.Forms.TextBox();
            this.textBox176 = new System.Windows.Forms.TextBox();
            this.textBox177 = new System.Windows.Forms.TextBox();
            this.textBox178 = new System.Windows.Forms.TextBox();
            this.textBox179 = new System.Windows.Forms.TextBox();
            this.textBox180 = new System.Windows.Forms.TextBox();
            this.textBox181 = new System.Windows.Forms.TextBox();
            this.textBox182 = new System.Windows.Forms.TextBox();
            this.textBox183 = new System.Windows.Forms.TextBox();
            this.textBox184 = new System.Windows.Forms.TextBox();
            this.textBox185 = new System.Windows.Forms.TextBox();
            this.textBox186 = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.qlab_zd = new System.Windows.Forms.TextBox();
            this.shp1_lbl = new System.Windows.Forms.Label();
            this.shp3_lbl = new System.Windows.Forms.Label();
            this.hlca_zd = new System.Windows.Forms.TextBox();
            this.qlab_zx = new System.Windows.Forms.TextBox();
            this.qlca_zd = new System.Windows.Forms.TextBox();
            this.hlab_zd = new System.Windows.Forms.TextBox();
            this.qlca_zx = new System.Windows.Forms.TextBox();
            this.qlab_ed = new System.Windows.Forms.TextBox();
            this.qlca_ed = new System.Windows.Forms.TextBox();
            this.pgff_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cgjg_tb = new System.Windows.Forms.TextBox();
            this.cgdh_tb = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.lxdh_tb = new System.Windows.Forms.TextBox();
            this.xqsl_tb = new System.Windows.Forms.TextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sqbm_cmb = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.jldw_tb = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cqbh_cmb = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cgksrq_dt = new System.Windows.Forms.DateTimePicker();
            this.cgjsrq_dt = new System.Windows.Forms.DateTimePicker();
            this.hxcgpz_cmb = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.cgy1_cmb = new System.Windows.Forms.ComboBox();
            this.cgzz1_cmb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.zbff_cmb = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.xufs_cmb = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.pjff_cmb = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.label74 = new System.Windows.Forms.Label();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textBox112 = new System.Windows.Forms.TextBox();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.sjbg_panel.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.refresh_bt);
            this.panel1.Controls.Add(this.fabu_bt);
            this.panel1.Controls.Add(this.zbzl_bt);
            this.panel1.Controls.Add(this.del_bt);
            this.panel1.Controls.Add(this.save_bt);
            this.panel1.Controls.Add(this.close_bt);
            this.panel1.Location = new System.Drawing.Point(0, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 42);
            this.panel1.TabIndex = 86;
            // 
            // refresh_bt
            // 
            this.refresh_bt.Location = new System.Drawing.Point(255, 5);
            this.refresh_bt.Name = "refresh_bt";
            this.refresh_bt.Size = new System.Drawing.Size(60, 30);
            this.refresh_bt.TabIndex = 80;
            this.refresh_bt.Text = "刷新";
            this.refresh_bt.UseVisualStyleBackColor = true;
            // 
            // fabu_bt
            // 
            this.fabu_bt.Location = new System.Drawing.Point(175, 5);
            this.fabu_bt.Name = "fabu_bt";
            this.fabu_bt.Size = new System.Drawing.Size(60, 30);
            this.fabu_bt.TabIndex = 79;
            this.fabu_bt.Text = "发布";
            this.fabu_bt.UseVisualStyleBackColor = true;
            // 
            // zbzl_bt
            // 
            this.zbzl_bt.Location = new System.Drawing.Point(415, 5);
            this.zbzl_bt.Name = "zbzl_bt";
            this.zbzl_bt.Size = new System.Drawing.Size(80, 30);
            this.zbzl_bt.TabIndex = 78;
            this.zbzl_bt.Text = "招标总览";
            this.zbzl_bt.UseVisualStyleBackColor = true;
            // 
            // del_bt
            // 
            this.del_bt.Location = new System.Drawing.Point(335, 5);
            this.del_bt.Name = "del_bt";
            this.del_bt.Size = new System.Drawing.Size(60, 30);
            this.del_bt.TabIndex = 77;
            this.del_bt.Text = "删除";
            this.del_bt.UseVisualStyleBackColor = true;
            // 
            // save_bt
            // 
            this.save_bt.Location = new System.Drawing.Point(96, 5);
            this.save_bt.Name = "save_bt";
            this.save_bt.Size = new System.Drawing.Size(60, 30);
            this.save_bt.TabIndex = 76;
            this.save_bt.Text = "保存";
            this.save_bt.UseVisualStyleBackColor = true;
            // 
            // close_bt
            // 
            this.close_bt.Location = new System.Drawing.Point(16, 5);
            this.close_bt.Name = "close_bt";
            this.close_bt.Size = new System.Drawing.Size(60, 30);
            this.close_bt.TabIndex = 75;
            this.close_bt.Text = "关闭";
            this.close_bt.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "确定采购价格";
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(17, 82);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(751, 1950);
            this.tabControl1.TabIndex = 87;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tbkfrq_dt);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.tbtjrq_dt);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.tbksrq_dt);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tbmbz_tb);
            this.tabPage1.Controls.Add(this.tbhxpz_cmb);
            this.tabPage1.Controls.Add(this.tbhb_cmb);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.tbcgz_cmb);
            this.tabPage1.Controls.Add(this.tbcgzz_cmb);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(743, 1924);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "投标接收信息";
            // 
            // tbkfrq_dt
            // 
            this.tbkfrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbkfrq_dt.Location = new System.Drawing.Point(84, 284);
            this.tbkfrq_dt.Name = "tbkfrq_dt";
            this.tbkfrq_dt.Size = new System.Drawing.Size(160, 23);
            this.tbkfrq_dt.TabIndex = 173;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("宋体", 9F);
            this.label43.Location = new System.Drawing.Point(24, 288);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(53, 12);
            this.label43.TabIndex = 172;
            this.label43.Text = "开放日期";
            // 
            // tbtjrq_dt
            // 
            this.tbtjrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbtjrq_dt.Location = new System.Drawing.Point(84, 246);
            this.tbtjrq_dt.Name = "tbtjrq_dt";
            this.tbtjrq_dt.Size = new System.Drawing.Size(160, 23);
            this.tbtjrq_dt.TabIndex = 171;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("宋体", 9F);
            this.label42.Location = new System.Drawing.Point(24, 250);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(53, 12);
            this.label42.TabIndex = 170;
            this.label42.Text = "提交日期";
            // 
            // tbksrq_dt
            // 
            this.tbksrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbksrq_dt.Location = new System.Drawing.Point(84, 207);
            this.tbksrq_dt.Name = "tbksrq_dt";
            this.tbksrq_dt.Size = new System.Drawing.Size(160, 23);
            this.tbksrq_dt.TabIndex = 169;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(24, 211);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 168;
            this.label4.Text = "开始日期";
            // 
            // tbmbz_tb
            // 
            this.tbmbz_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbmbz_tb.Location = new System.Drawing.Point(396, 92);
            this.tbmbz_tb.Name = "tbmbz_tb";
            this.tbmbz_tb.Size = new System.Drawing.Size(160, 23);
            this.tbmbz_tb.TabIndex = 95;
            this.tbmbz_tb.Text = "2,250,000";
            // 
            // tbhxpz_cmb
            // 
            this.tbhxpz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbhxpz_cmb.FormattingEnabled = true;
            this.tbhxpz_cmb.ItemHeight = 14;
            this.tbhxpz_cmb.Location = new System.Drawing.Point(396, 131);
            this.tbhxpz_cmb.Name = "tbhxpz_cmb";
            this.tbhxpz_cmb.Size = new System.Drawing.Size(160, 22);
            this.tbhxpz_cmb.TabIndex = 93;
            this.tbhxpz_cmb.Text = "采购订单或合同";
            // 
            // tbhb_cmb
            // 
            this.tbhb_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbhb_cmb.FormattingEnabled = true;
            this.tbhb_cmb.ItemHeight = 14;
            this.tbhb_cmb.Location = new System.Drawing.Point(396, 55);
            this.tbhb_cmb.Name = "tbhb_cmb";
            this.tbhb_cmb.Size = new System.Drawing.Size(160, 22);
            this.tbhb_cmb.TabIndex = 92;
            this.tbhb_cmb.Text = "CNY";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(347, 96);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 91;
            this.label1.Text = "目标值";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(336, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 90;
            this.label11.Text = "后续凭证";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(359, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 89;
            this.label12.Text = "货币";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(336, 26);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(57, 12);
            this.label41.TabIndex = 88;
            this.label41.Text = "事件参数";
            // 
            // tbcgz_cmb
            // 
            this.tbcgz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbcgz_cmb.FormattingEnabled = true;
            this.tbcgz_cmb.ItemHeight = 14;
            this.tbcgz_cmb.Location = new System.Drawing.Point(84, 93);
            this.tbcgz_cmb.Name = "tbcgz_cmb";
            this.tbcgz_cmb.Size = new System.Drawing.Size(160, 22);
            this.tbcgz_cmb.TabIndex = 85;
            this.tbcgz_cmb.Text = "00081";
            // 
            // tbcgzz_cmb
            // 
            this.tbcgzz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbcgzz_cmb.FormattingEnabled = true;
            this.tbcgzz_cmb.ItemHeight = 14;
            this.tbcgzz_cmb.Location = new System.Drawing.Point(84, 55);
            this.tbcgzz_cmb.Name = "tbcgzz_cmb";
            this.tbcgzz_cmb.Size = new System.Drawing.Size(160, 22);
            this.tbcgzz_cmb.TabIndex = 83;
            this.tbcgzz_cmb.Text = "0008";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(24, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 12);
            this.label6.TabIndex = 62;
            this.label6.Text = "日期";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(35, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 60;
            this.label5.Text = "采购组";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(24, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 56;
            this.label3.Text = "采购组织";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(24, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "组织机构";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.tbradd_bt);
            this.tabPage2.Controls.Add(this.tbrdel_bt);
            this.tabPage2.Controls.Add(this.tbrtz_bt);
            this.tabPage2.Controls.Add(this.tbrbs_tb);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(743, 1924);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "投标人";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.checkBox6);
            this.panel8.Controls.Add(this.checkBox7);
            this.panel8.Controls.Add(this.checkBox8);
            this.panel8.Controls.Add(this.checkBox9);
            this.panel8.Controls.Add(this.label37);
            this.panel8.Controls.Add(this.textBox1);
            this.panel8.Controls.Add(this.textBox4);
            this.panel8.Controls.Add(this.textBox7);
            this.panel8.Controls.Add(this.textBox10);
            this.panel8.Controls.Add(this.label44);
            this.panel8.Controls.Add(this.textBox13);
            this.panel8.Controls.Add(this.textBox16);
            this.panel8.Controls.Add(this.textBox19);
            this.panel8.Controls.Add(this.textBox22);
            this.panel8.Controls.Add(this.label45);
            this.panel8.Controls.Add(this.textBox25);
            this.panel8.Controls.Add(this.textBox26);
            this.panel8.Controls.Add(this.textBox27);
            this.panel8.Controls.Add(this.textBox28);
            this.panel8.Controls.Add(this.label46);
            this.panel8.Controls.Add(this.textBox29);
            this.panel8.Controls.Add(this.textBox30);
            this.panel8.Controls.Add(this.textBox31);
            this.panel8.Controls.Add(this.textBox32);
            this.panel8.Controls.Add(this.textBox33);
            this.panel8.Controls.Add(this.label47);
            this.panel8.Controls.Add(this.label48);
            this.panel8.Controls.Add(this.label49);
            this.panel8.Controls.Add(this.textBox34);
            this.panel8.Controls.Add(this.textBox35);
            this.panel8.Controls.Add(this.textBox36);
            this.panel8.Controls.Add(this.textBox37);
            this.panel8.Controls.Add(this.textBox38);
            this.panel8.Controls.Add(this.textBox39);
            this.panel8.Controls.Add(this.textBox40);
            this.panel8.Controls.Add(this.textBox41);
            this.panel8.Controls.Add(this.textBox42);
            this.panel8.Controls.Add(this.textBox43);
            this.panel8.Controls.Add(this.textBox44);
            this.panel8.Location = new System.Drawing.Point(6, 51);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(712, 153);
            this.panel8.TabIndex = 219;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox6.Location = new System.Drawing.Point(23, 45);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 121;
            this.checkBox6.UseVisualStyleBackColor = false;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox7.Location = new System.Drawing.Point(23, 94);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 118;
            this.checkBox7.UseVisualStyleBackColor = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox8.Location = new System.Drawing.Point(23, 118);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 117;
            this.checkBox8.UseVisualStyleBackColor = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox9.Location = new System.Drawing.Point(23, 70);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 116;
            this.checkBox9.UseVisualStyleBackColor = false;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.SystemColors.Window;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label37.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(600, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 25);
            this.label37.TabIndex = 114;
            this.label37.Text = "投标人标识";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(600, 40);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 25);
            this.textBox1.TabIndex = 108;
            this.textBox1.Text = "1000000738";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Window;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox4.Location = new System.Drawing.Point(600, 88);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 25);
            this.textBox4.TabIndex = 110;
            this.textBox4.Text = "1000000795";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Window;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox7.Location = new System.Drawing.Point(600, 112);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 25);
            this.textBox7.TabIndex = 111;
            this.textBox7.Text = "1000000736";
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Window;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox10.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox10.Location = new System.Drawing.Point(600, 64);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 25);
            this.textBox10.TabIndex = 109;
            this.textBox10.Text = "1000000697";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.SystemColors.Window;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label44.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(501, 16);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 25);
            this.label44.TabIndex = 107;
            this.label44.Text = "公司标识";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.Window;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox13.Location = new System.Drawing.Point(501, 40);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 25);
            this.textBox13.TabIndex = 101;
            this.textBox13.Text = "3019";
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Window;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox16.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox16.Location = new System.Drawing.Point(501, 88);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 25);
            this.textBox16.TabIndex = 103;
            this.textBox16.Text = "3010";
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Window;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox19.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox19.Location = new System.Drawing.Point(501, 112);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 25);
            this.textBox19.TabIndex = 104;
            this.textBox19.Text = "3011";
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Window;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox22.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox22.Location = new System.Drawing.Point(501, 64);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(100, 25);
            this.textBox22.TabIndex = 102;
            this.textBox22.Text = "3012";
            this.textBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.SystemColors.Window;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label45.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label45.Location = new System.Drawing.Point(442, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(60, 25);
            this.label45.TabIndex = 100;
            this.label45.Text = "国家";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Window;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox25.Location = new System.Drawing.Point(442, 40);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(60, 25);
            this.textBox25.TabIndex = 94;
            this.textBox25.Text = "CN";
            this.textBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Window;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox26.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox26.Location = new System.Drawing.Point(442, 88);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(60, 25);
            this.textBox26.TabIndex = 96;
            this.textBox26.Text = "CN";
            this.textBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.SystemColors.Window;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox27.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox27.Location = new System.Drawing.Point(442, 112);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(60, 25);
            this.textBox27.TabIndex = 97;
            this.textBox27.Text = "CN";
            this.textBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Window;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox28.Location = new System.Drawing.Point(442, 64);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(60, 25);
            this.textBox28.TabIndex = 95;
            this.textBox28.Text = "CN";
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.SystemColors.Window;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label46.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label46.Location = new System.Drawing.Point(343, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(100, 25);
            this.label46.TabIndex = 93;
            this.label46.Text = "联系人";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Window;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox29.Location = new System.Drawing.Point(343, 40);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 25);
            this.textBox29.TabIndex = 87;
            this.textBox29.Text = "李四";
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Window;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox30.Location = new System.Drawing.Point(343, 88);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(100, 25);
            this.textBox30.TabIndex = 89;
            this.textBox30.Text = "赵六";
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Window;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox31.Location = new System.Drawing.Point(343, 112);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(100, 25);
            this.textBox31.TabIndex = 90;
            this.textBox31.Text = "吴七";
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Window;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox32.Location = new System.Drawing.Point(343, 64);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(100, 25);
            this.textBox32.TabIndex = 88;
            this.textBox32.Text = "王五";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Window;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox33.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox33.Location = new System.Drawing.Point(15, 40);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(30, 25);
            this.textBox33.TabIndex = 0;
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.Window;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(15, 16);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(30, 25);
            this.label47.TabIndex = 86;
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.SystemColors.Window;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label48.Location = new System.Drawing.Point(44, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(50, 25);
            this.label48.TabIndex = 86;
            this.label48.Text = "序号";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.SystemColors.Window;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label49.Location = new System.Drawing.Point(93, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(251, 25);
            this.label49.TabIndex = 86;
            this.label49.Text = "公司名称";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Window;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox34.Location = new System.Drawing.Point(93, 112);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(251, 25);
            this.textBox34.TabIndex = 14;
            this.textBox34.Text = "衡水利达液压设备有限公司";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Window;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox35.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox35.Location = new System.Drawing.Point(15, 88);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(30, 25);
            this.textBox35.TabIndex = 8;
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Window;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox36.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox36.Location = new System.Drawing.Point(44, 40);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(50, 25);
            this.textBox36.TabIndex = 1;
            this.textBox36.Text = "1";
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.Window;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox37.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox37.Location = new System.Drawing.Point(44, 88);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(50, 25);
            this.textBox37.TabIndex = 9;
            this.textBox37.Text = "3";
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Window;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox38.Location = new System.Drawing.Point(44, 112);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(50, 25);
            this.textBox38.TabIndex = 13;
            this.textBox38.Text = "4";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Window;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox39.Location = new System.Drawing.Point(93, 40);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(251, 25);
            this.textBox39.TabIndex = 2;
            this.textBox39.Text = "上海卓亚矿山机械有限公司";
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Window;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox40.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox40.Location = new System.Drawing.Point(15, 112);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(30, 25);
            this.textBox40.TabIndex = 12;
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Window;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox41.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox41.Location = new System.Drawing.Point(93, 88);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(251, 25);
            this.textBox41.TabIndex = 10;
            this.textBox41.Text = "洛阳命超机械铸件有限公司";
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.Window;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox42.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox42.Location = new System.Drawing.Point(15, 64);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(30, 25);
            this.textBox42.TabIndex = 4;
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.SystemColors.Window;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox43.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox43.Location = new System.Drawing.Point(44, 64);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(50, 25);
            this.textBox43.TabIndex = 5;
            this.textBox43.Text = "2";
            this.textBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Window;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox44.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox44.Location = new System.Drawing.Point(93, 64);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(251, 25);
            this.textBox44.TabIndex = 6;
            this.textBox44.Text = "辽宁朝阳纪元冶金矿山设备有限公司";
            this.textBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbradd_bt
            // 
            this.tbradd_bt.Location = new System.Drawing.Point(335, 18);
            this.tbradd_bt.Name = "tbradd_bt";
            this.tbradd_bt.Size = new System.Drawing.Size(60, 30);
            this.tbradd_bt.TabIndex = 79;
            this.tbradd_bt.Text = "添加";
            this.tbradd_bt.UseVisualStyleBackColor = true;
            // 
            // tbrdel_bt
            // 
            this.tbrdel_bt.Location = new System.Drawing.Point(506, 19);
            this.tbrdel_bt.Name = "tbrdel_bt";
            this.tbrdel_bt.Size = new System.Drawing.Size(60, 30);
            this.tbrdel_bt.TabIndex = 72;
            this.tbrdel_bt.Text = "删除";
            this.tbrdel_bt.UseVisualStyleBackColor = true;
            // 
            // tbrtz_bt
            // 
            this.tbrtz_bt.Location = new System.Drawing.Point(411, 19);
            this.tbrtz_bt.Name = "tbrtz_bt";
            this.tbrtz_bt.Size = new System.Drawing.Size(80, 30);
            this.tbrtz_bt.TabIndex = 71;
            this.tbrtz_bt.Text = "发送通知";
            this.tbrtz_bt.UseVisualStyleBackColor = true;
            // 
            // tbrbs_tb
            // 
            this.tbrbs_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbrbs_tb.Location = new System.Drawing.Point(159, 24);
            this.tbrbs_tb.Name = "tbrbs_tb";
            this.tbrbs_tb.Size = new System.Drawing.Size(160, 23);
            this.tbrbs_tb.TabIndex = 67;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(26, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 12);
            this.label9.TabIndex = 65;
            this.label9.Text = "按公司标识添加投标人";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel13);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Controls.Add(this.sjbg_panel);
            this.tabPage3.Controls.Add(this.pgff_cmb);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(743, 1924);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "评估方法";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.comboBox1);
            this.panel13.Controls.Add(this.label99);
            this.panel13.Location = new System.Drawing.Point(6, 609);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(565, 35);
            this.panel13.TabIndex = 226;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 14;
            this.comboBox1.Items.AddRange(new object[] {
            "加权平均法",
            "最低价格法",
            "最低所有权成本法"});
            this.comboBox1.Location = new System.Drawing.Point(75, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 22);
            this.comboBox1.TabIndex = 222;
            this.comboBox1.Text = "最低价格法";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("宋体", 9F);
            this.label99.Location = new System.Drawing.Point(16, 10);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(53, 12);
            this.label99.TabIndex = 221;
            this.label99.Text = "评估方法";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.comboBox3);
            this.panel3.Controls.Add(this.label100);
            this.panel3.Location = new System.Drawing.Point(6, 1313);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 33);
            this.panel3.TabIndex = 225;
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.ItemHeight = 14;
            this.comboBox3.Items.AddRange(new object[] {
            "加权平均法",
            "最低价格法",
            "最低所有权成本法"});
            this.comboBox3.Location = new System.Drawing.Point(69, 5);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(200, 22);
            this.comboBox3.TabIndex = 224;
            this.comboBox3.Text = "最低所有权成本法";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("宋体", 9F);
            this.label100.Location = new System.Drawing.Point(10, 9);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 223;
            this.label100.Text = "评估方法";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.textBox67);
            this.panel10.Controls.Add(this.textBox68);
            this.panel10.Controls.Add(this.textBox84);
            this.panel10.Controls.Add(this.textBox92);
            this.panel10.Controls.Add(this.textBox126);
            this.panel10.Controls.Add(this.textBox148);
            this.panel10.Controls.Add(this.textBox149);
            this.panel10.Controls.Add(this.textBox150);
            this.panel10.Controls.Add(this.textBox151);
            this.panel10.Controls.Add(this.textBox152);
            this.panel10.Controls.Add(this.textBox153);
            this.panel10.Controls.Add(this.textBox154);
            this.panel10.Controls.Add(this.label63);
            this.panel10.Controls.Add(this.label92);
            this.panel10.Controls.Add(this.label93);
            this.panel10.Controls.Add(this.label94);
            this.panel10.Controls.Add(this.textBox155);
            this.panel10.Controls.Add(this.textBox156);
            this.panel10.Controls.Add(this.textBox157);
            this.panel10.Controls.Add(this.textBox158);
            this.panel10.Controls.Add(this.label95);
            this.panel10.Controls.Add(this.textBox159);
            this.panel10.Controls.Add(this.textBox160);
            this.panel10.Controls.Add(this.textBox161);
            this.panel10.Controls.Add(this.textBox162);
            this.panel10.Controls.Add(this.textBox163);
            this.panel10.Controls.Add(this.textBox164);
            this.panel10.Controls.Add(this.textBox165);
            this.panel10.Controls.Add(this.textBox166);
            this.panel10.Controls.Add(this.label96);
            this.panel10.Controls.Add(this.label97);
            this.panel10.Controls.Add(this.label98);
            this.panel10.Controls.Add(this.textBox102);
            this.panel10.Controls.Add(this.textBox103);
            this.panel10.Controls.Add(this.textBox104);
            this.panel10.Controls.Add(this.textBox106);
            this.panel10.Controls.Add(this.textBox107);
            this.panel10.Controls.Add(this.textBox108);
            this.panel10.Controls.Add(this.textBox110);
            this.panel10.Controls.Add(this.textBox111);
            this.panel10.Controls.Add(this.textBox113);
            this.panel10.Controls.Add(this.textBox114);
            this.panel10.Controls.Add(this.textBox116);
            this.panel10.Controls.Add(this.textBox117);
            this.panel10.Controls.Add(this.textBox118);
            this.panel10.Controls.Add(this.textBox119);
            this.panel10.Controls.Add(this.textBox120);
            this.panel10.Controls.Add(this.textBox127);
            this.panel10.Controls.Add(this.textBox128);
            this.panel10.Controls.Add(this.textBox129);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Controls.Add(this.textBox130);
            this.panel10.Controls.Add(this.textBox131);
            this.panel10.Controls.Add(this.textBox132);
            this.panel10.Controls.Add(this.label81);
            this.panel10.Controls.Add(this.label82);
            this.panel10.Controls.Add(this.label83);
            this.panel10.Controls.Add(this.label84);
            this.panel10.Controls.Add(this.label85);
            this.panel10.Controls.Add(this.label86);
            this.panel10.Controls.Add(this.textBox133);
            this.panel10.Controls.Add(this.textBox134);
            this.panel10.Controls.Add(this.textBox135);
            this.panel10.Controls.Add(this.label87);
            this.panel10.Controls.Add(this.textBox136);
            this.panel10.Controls.Add(this.label88);
            this.panel10.Controls.Add(this.textBox137);
            this.panel10.Controls.Add(this.textBox138);
            this.panel10.Controls.Add(this.textBox139);
            this.panel10.Controls.Add(this.textBox140);
            this.panel10.Controls.Add(this.textBox141);
            this.panel10.Controls.Add(this.textBox142);
            this.panel10.Controls.Add(this.textBox143);
            this.panel10.Controls.Add(this.textBox144);
            this.panel10.Controls.Add(this.label89);
            this.panel10.Controls.Add(this.textBox145);
            this.panel10.Controls.Add(this.label90);
            this.panel10.Controls.Add(this.textBox146);
            this.panel10.Controls.Add(this.label91);
            this.panel10.Controls.Add(this.textBox147);
            this.panel10.Controls.Add(this.textBox94);
            this.panel10.Controls.Add(this.textBox95);
            this.panel10.Controls.Add(this.textBox96);
            this.panel10.Controls.Add(this.label28);
            this.panel10.Controls.Add(this.textBox98);
            this.panel10.Controls.Add(this.textBox99);
            this.panel10.Controls.Add(this.textBox100);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this.textBox70);
            this.panel10.Controls.Add(this.textBox74);
            this.panel10.Controls.Add(this.textBox121);
            this.panel10.Controls.Add(this.textBox122);
            this.panel10.Controls.Add(this.textBox123);
            this.panel10.Controls.Add(this.textBox124);
            this.panel10.Controls.Add(this.textBox125);
            this.panel10.Controls.Add(this.textBox85);
            this.panel10.Controls.Add(this.textBox86);
            this.panel10.Controls.Add(this.label73);
            this.panel10.Controls.Add(this.textBox87);
            this.panel10.Controls.Add(this.textBox88);
            this.panel10.Controls.Add(this.textBox89);
            this.panel10.Controls.Add(this.textBox90);
            this.panel10.Controls.Add(this.textBox91);
            this.panel10.Controls.Add(this.textBox77);
            this.panel10.Controls.Add(this.textBox78);
            this.panel10.Controls.Add(this.textBox79);
            this.panel10.Controls.Add(this.label72);
            this.panel10.Controls.Add(this.textBox80);
            this.panel10.Controls.Add(this.textBox81);
            this.panel10.Controls.Add(this.textBox82);
            this.panel10.Controls.Add(this.textBox83);
            this.panel10.Controls.Add(this.label68);
            this.panel10.Controls.Add(this.label69);
            this.panel10.Controls.Add(this.textBox69);
            this.panel10.Controls.Add(this.textBox71);
            this.panel10.Controls.Add(this.textBox72);
            this.panel10.Controls.Add(this.textBox73);
            this.panel10.Controls.Add(this.textBox75);
            this.panel10.Controls.Add(this.textBox76);
            this.panel10.Controls.Add(this.label70);
            this.panel10.Controls.Add(this.label71);
            this.panel10.Controls.Add(this.label55);
            this.panel10.Controls.Add(this.label62);
            this.panel10.Controls.Add(this.label64);
            this.panel10.Controls.Add(this.label65);
            this.panel10.Controls.Add(this.label66);
            this.panel10.Controls.Add(this.label67);
            this.panel10.Controls.Add(this.textBox61);
            this.panel10.Controls.Add(this.label54);
            this.panel10.Controls.Add(this.shp2_lbl);
            this.panel10.Controls.Add(this.label56);
            this.panel10.Controls.Add(this.hlca_ed);
            this.panel10.Controls.Add(this.label57);
            this.panel10.Controls.Add(this.zd_lbl);
            this.panel10.Controls.Add(this.hlbc_ed);
            this.panel10.Controls.Add(this.label58);
            this.panel10.Controls.Add(this.hlab_ed);
            this.panel10.Controls.Add(this.hlca_zx);
            this.panel10.Controls.Add(this.zx_lbl);
            this.panel10.Controls.Add(this.textBox62);
            this.panel10.Controls.Add(this.textBox63);
            this.panel10.Controls.Add(this.qlbc_zd);
            this.panel10.Controls.Add(this.qlbc_zx);
            this.panel10.Controls.Add(this.hlbc_zx);
            this.panel10.Controls.Add(this.hlbc_zd);
            this.panel10.Controls.Add(this.hlab_zx);
            this.panel10.Controls.Add(this.textBox64);
            this.panel10.Controls.Add(this.textBox65);
            this.panel10.Controls.Add(this.textBox66);
            this.panel10.Controls.Add(this.label59);
            this.panel10.Controls.Add(this.label60);
            this.panel10.Controls.Add(this.label61);
            this.panel10.Location = new System.Drawing.Point(6, 1346);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(712, 492);
            this.panel10.TabIndex = 220;
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint);
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.SystemColors.Window;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox67.Location = new System.Drawing.Point(620, 352);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(80, 25);
            this.textBox67.TabIndex = 345;
            this.textBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.SystemColors.Window;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox68.Location = new System.Drawing.Point(620, 376);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(80, 25);
            this.textBox68.TabIndex = 346;
            this.textBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.Window;
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox84.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox84.Location = new System.Drawing.Point(620, 328);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(80, 25);
            this.textBox84.TabIndex = 344;
            this.textBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.SystemColors.Window;
            this.textBox92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox92.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox92.Location = new System.Drawing.Point(620, 304);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(80, 25);
            this.textBox92.TabIndex = 343;
            this.textBox92.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox126
            // 
            this.textBox126.BackColor = System.Drawing.SystemColors.Window;
            this.textBox126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox126.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox126.Location = new System.Drawing.Point(322, 352);
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new System.Drawing.Size(62, 25);
            this.textBox126.TabIndex = 341;
            this.textBox126.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox148
            // 
            this.textBox148.BackColor = System.Drawing.SystemColors.Window;
            this.textBox148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox148.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox148.Location = new System.Drawing.Point(322, 304);
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new System.Drawing.Size(62, 25);
            this.textBox148.TabIndex = 339;
            this.textBox148.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox149
            // 
            this.textBox149.BackColor = System.Drawing.SystemColors.Window;
            this.textBox149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox149.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox149.Location = new System.Drawing.Point(322, 376);
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new System.Drawing.Size(62, 25);
            this.textBox149.TabIndex = 342;
            this.textBox149.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox150
            // 
            this.textBox150.BackColor = System.Drawing.SystemColors.Window;
            this.textBox150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox150.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox150.Location = new System.Drawing.Point(322, 328);
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new System.Drawing.Size(62, 25);
            this.textBox150.TabIndex = 340;
            this.textBox150.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox151
            // 
            this.textBox151.BackColor = System.Drawing.SystemColors.Window;
            this.textBox151.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox151.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox151.Location = new System.Drawing.Point(541, 352);
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new System.Drawing.Size(80, 25);
            this.textBox151.TabIndex = 337;
            this.textBox151.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox152
            // 
            this.textBox152.BackColor = System.Drawing.SystemColors.Window;
            this.textBox152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox152.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox152.Location = new System.Drawing.Point(541, 376);
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new System.Drawing.Size(80, 25);
            this.textBox152.TabIndex = 338;
            this.textBox152.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox153
            // 
            this.textBox153.BackColor = System.Drawing.SystemColors.Window;
            this.textBox153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox153.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox153.Location = new System.Drawing.Point(541, 328);
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new System.Drawing.Size(80, 25);
            this.textBox153.TabIndex = 336;
            this.textBox153.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox154
            // 
            this.textBox154.BackColor = System.Drawing.SystemColors.Window;
            this.textBox154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox154.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox154.Location = new System.Drawing.Point(541, 304);
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new System.Drawing.Size(80, 25);
            this.textBox154.TabIndex = 335;
            this.textBox154.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.SystemColors.Window;
            this.label63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label63.Location = new System.Drawing.Point(15, 304);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(105, 25);
            this.label63.TabIndex = 331;
            this.label63.Text = "预防性维护成本";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label92
            // 
            this.label92.BackColor = System.Drawing.SystemColors.Window;
            this.label92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label92.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label92.Location = new System.Drawing.Point(15, 376);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(105, 25);
            this.label92.TabIndex = 334;
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label93
            // 
            this.label93.BackColor = System.Drawing.SystemColors.Window;
            this.label93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label93.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label93.Location = new System.Drawing.Point(15, 352);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(105, 25);
            this.label93.TabIndex = 333;
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.SystemColors.Window;
            this.label94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label94.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label94.Location = new System.Drawing.Point(15, 328);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(105, 25);
            this.label94.TabIndex = 332;
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox155
            // 
            this.textBox155.BackColor = System.Drawing.SystemColors.Window;
            this.textBox155.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox155.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox155.Location = new System.Drawing.Point(462, 352);
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new System.Drawing.Size(80, 25);
            this.textBox155.TabIndex = 323;
            this.textBox155.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox156
            // 
            this.textBox156.BackColor = System.Drawing.SystemColors.Window;
            this.textBox156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox156.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox156.Location = new System.Drawing.Point(383, 352);
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new System.Drawing.Size(80, 25);
            this.textBox156.TabIndex = 322;
            this.textBox156.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox157
            // 
            this.textBox157.BackColor = System.Drawing.SystemColors.Window;
            this.textBox157.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox157.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox157.Location = new System.Drawing.Point(233, 352);
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new System.Drawing.Size(90, 25);
            this.textBox157.TabIndex = 321;
            this.textBox157.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox158
            // 
            this.textBox158.BackColor = System.Drawing.SystemColors.Window;
            this.textBox158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox158.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox158.Location = new System.Drawing.Point(462, 376);
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new System.Drawing.Size(80, 25);
            this.textBox158.TabIndex = 326;
            this.textBox158.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label95
            // 
            this.label95.BackColor = System.Drawing.SystemColors.Window;
            this.label95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label95.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label95.Location = new System.Drawing.Point(119, 304);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(115, 25);
            this.label95.TabIndex = 327;
            this.label95.Text = "采购和存货成本";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox159
            // 
            this.textBox159.BackColor = System.Drawing.SystemColors.Window;
            this.textBox159.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox159.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox159.Location = new System.Drawing.Point(462, 328);
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new System.Drawing.Size(80, 25);
            this.textBox159.TabIndex = 320;
            this.textBox159.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox160
            // 
            this.textBox160.BackColor = System.Drawing.SystemColors.Window;
            this.textBox160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox160.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox160.Location = new System.Drawing.Point(233, 304);
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new System.Drawing.Size(90, 25);
            this.textBox160.TabIndex = 315;
            this.textBox160.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox161
            // 
            this.textBox161.BackColor = System.Drawing.SystemColors.Window;
            this.textBox161.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox161.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox161.Location = new System.Drawing.Point(383, 304);
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new System.Drawing.Size(80, 25);
            this.textBox161.TabIndex = 316;
            this.textBox161.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox162
            // 
            this.textBox162.BackColor = System.Drawing.SystemColors.Window;
            this.textBox162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox162.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox162.Location = new System.Drawing.Point(383, 376);
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new System.Drawing.Size(80, 25);
            this.textBox162.TabIndex = 325;
            this.textBox162.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox163
            // 
            this.textBox163.BackColor = System.Drawing.SystemColors.Window;
            this.textBox163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox163.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox163.Location = new System.Drawing.Point(383, 328);
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new System.Drawing.Size(80, 25);
            this.textBox163.TabIndex = 319;
            this.textBox163.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox164
            // 
            this.textBox164.BackColor = System.Drawing.SystemColors.Window;
            this.textBox164.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox164.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox164.Location = new System.Drawing.Point(233, 376);
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new System.Drawing.Size(90, 25);
            this.textBox164.TabIndex = 324;
            this.textBox164.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox165
            // 
            this.textBox165.BackColor = System.Drawing.SystemColors.Window;
            this.textBox165.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox165.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox165.Location = new System.Drawing.Point(233, 328);
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new System.Drawing.Size(90, 25);
            this.textBox165.TabIndex = 318;
            this.textBox165.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox166
            // 
            this.textBox166.BackColor = System.Drawing.SystemColors.Window;
            this.textBox166.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox166.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox166.Location = new System.Drawing.Point(462, 304);
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new System.Drawing.Size(80, 25);
            this.textBox166.TabIndex = 317;
            this.textBox166.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label96
            // 
            this.label96.BackColor = System.Drawing.SystemColors.Window;
            this.label96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label96.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label96.Location = new System.Drawing.Point(119, 376);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 25);
            this.label96.TabIndex = 330;
            this.label96.Text = "停工期成本";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label97
            // 
            this.label97.BackColor = System.Drawing.SystemColors.Window;
            this.label97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label97.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label97.Location = new System.Drawing.Point(119, 352);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(115, 25);
            this.label97.TabIndex = 329;
            this.label97.Text = "维护工具设备成本";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label98
            // 
            this.label98.BackColor = System.Drawing.SystemColors.Window;
            this.label98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label98.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label98.Location = new System.Drawing.Point(119, 328);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(115, 25);
            this.label98.TabIndex = 328;
            this.label98.Text = "维护人员成本";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.SystemColors.Window;
            this.textBox102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox102.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox102.Location = new System.Drawing.Point(620, 184);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(80, 25);
            this.textBox102.TabIndex = 314;
            this.textBox102.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox103
            // 
            this.textBox103.BackColor = System.Drawing.SystemColors.Window;
            this.textBox103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox103.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox103.Location = new System.Drawing.Point(620, 136);
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(80, 25);
            this.textBox103.TabIndex = 312;
            this.textBox103.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox104
            // 
            this.textBox104.BackColor = System.Drawing.SystemColors.Window;
            this.textBox104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox104.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox104.Location = new System.Drawing.Point(620, 160);
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new System.Drawing.Size(80, 25);
            this.textBox104.TabIndex = 313;
            this.textBox104.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox106
            // 
            this.textBox106.BackColor = System.Drawing.SystemColors.Window;
            this.textBox106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox106.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox106.Location = new System.Drawing.Point(620, 112);
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new System.Drawing.Size(80, 25);
            this.textBox106.TabIndex = 311;
            this.textBox106.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox107
            // 
            this.textBox107.BackColor = System.Drawing.SystemColors.Window;
            this.textBox107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox107.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox107.Location = new System.Drawing.Point(620, 88);
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new System.Drawing.Size(80, 25);
            this.textBox107.TabIndex = 310;
            this.textBox107.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox108
            // 
            this.textBox108.BackColor = System.Drawing.SystemColors.Window;
            this.textBox108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox108.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox108.Location = new System.Drawing.Point(620, 64);
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new System.Drawing.Size(80, 25);
            this.textBox108.TabIndex = 309;
            this.textBox108.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox110
            // 
            this.textBox110.BackColor = System.Drawing.SystemColors.Window;
            this.textBox110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox110.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox110.Location = new System.Drawing.Point(322, 184);
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new System.Drawing.Size(62, 25);
            this.textBox110.TabIndex = 308;
            this.textBox110.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox111
            // 
            this.textBox111.BackColor = System.Drawing.SystemColors.Window;
            this.textBox111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox111.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox111.Location = new System.Drawing.Point(322, 136);
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new System.Drawing.Size(62, 25);
            this.textBox111.TabIndex = 306;
            this.textBox111.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox113
            // 
            this.textBox113.BackColor = System.Drawing.SystemColors.Window;
            this.textBox113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox113.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox113.Location = new System.Drawing.Point(322, 88);
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new System.Drawing.Size(62, 25);
            this.textBox113.TabIndex = 304;
            this.textBox113.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox114
            // 
            this.textBox114.BackColor = System.Drawing.SystemColors.Window;
            this.textBox114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox114.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox114.Location = new System.Drawing.Point(322, 160);
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new System.Drawing.Size(62, 25);
            this.textBox114.TabIndex = 307;
            this.textBox114.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox116
            // 
            this.textBox116.BackColor = System.Drawing.SystemColors.Window;
            this.textBox116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox116.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox116.Location = new System.Drawing.Point(322, 112);
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new System.Drawing.Size(62, 25);
            this.textBox116.TabIndex = 305;
            this.textBox116.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox117
            // 
            this.textBox117.BackColor = System.Drawing.SystemColors.Window;
            this.textBox117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox117.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox117.Location = new System.Drawing.Point(322, 64);
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new System.Drawing.Size(62, 25);
            this.textBox117.TabIndex = 303;
            this.textBox117.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox118
            // 
            this.textBox118.BackColor = System.Drawing.SystemColors.Window;
            this.textBox118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox118.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox118.Location = new System.Drawing.Point(541, 184);
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new System.Drawing.Size(80, 25);
            this.textBox118.TabIndex = 302;
            this.textBox118.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox119
            // 
            this.textBox119.BackColor = System.Drawing.SystemColors.Window;
            this.textBox119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox119.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox119.Location = new System.Drawing.Point(541, 136);
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new System.Drawing.Size(80, 25);
            this.textBox119.TabIndex = 300;
            this.textBox119.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox120
            // 
            this.textBox120.BackColor = System.Drawing.SystemColors.Window;
            this.textBox120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox120.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox120.Location = new System.Drawing.Point(541, 160);
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new System.Drawing.Size(80, 25);
            this.textBox120.TabIndex = 301;
            this.textBox120.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox127
            // 
            this.textBox127.BackColor = System.Drawing.SystemColors.Window;
            this.textBox127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox127.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox127.Location = new System.Drawing.Point(541, 112);
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new System.Drawing.Size(80, 25);
            this.textBox127.TabIndex = 299;
            this.textBox127.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox128
            // 
            this.textBox128.BackColor = System.Drawing.SystemColors.Window;
            this.textBox128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox128.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox128.Location = new System.Drawing.Point(541, 88);
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new System.Drawing.Size(80, 25);
            this.textBox128.TabIndex = 298;
            this.textBox128.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox129
            // 
            this.textBox129.BackColor = System.Drawing.SystemColors.Window;
            this.textBox129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox129.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox129.Location = new System.Drawing.Point(541, 64);
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new System.Drawing.Size(80, 25);
            this.textBox129.TabIndex = 297;
            this.textBox129.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.SystemColors.Window;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label36.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label36.Location = new System.Drawing.Point(15, 184);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 25);
            this.label36.TabIndex = 296;
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox130
            // 
            this.textBox130.BackColor = System.Drawing.SystemColors.Window;
            this.textBox130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox130.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox130.Location = new System.Drawing.Point(462, 184);
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new System.Drawing.Size(80, 25);
            this.textBox130.TabIndex = 294;
            this.textBox130.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox131
            // 
            this.textBox131.BackColor = System.Drawing.SystemColors.Window;
            this.textBox131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox131.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox131.Location = new System.Drawing.Point(383, 184);
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new System.Drawing.Size(80, 25);
            this.textBox131.TabIndex = 293;
            this.textBox131.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox132
            // 
            this.textBox132.BackColor = System.Drawing.SystemColors.Window;
            this.textBox132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox132.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox132.Location = new System.Drawing.Point(233, 184);
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new System.Drawing.Size(90, 25);
            this.textBox132.TabIndex = 292;
            this.textBox132.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.SystemColors.Window;
            this.label81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label81.Location = new System.Drawing.Point(119, 184);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(115, 25);
            this.label81.TabIndex = 295;
            this.label81.Text = "试运行成本";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label82
            // 
            this.label82.BackColor = System.Drawing.SystemColors.Window;
            this.label82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label82.Location = new System.Drawing.Point(15, 64);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(105, 25);
            this.label82.TabIndex = 287;
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label83
            // 
            this.label83.BackColor = System.Drawing.SystemColors.Window;
            this.label83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label83.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label83.Location = new System.Drawing.Point(15, 88);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(105, 25);
            this.label83.TabIndex = 288;
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label84
            // 
            this.label84.BackColor = System.Drawing.SystemColors.Window;
            this.label84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label84.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label84.Location = new System.Drawing.Point(15, 160);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(105, 25);
            this.label84.TabIndex = 291;
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label85
            // 
            this.label85.BackColor = System.Drawing.SystemColors.Window;
            this.label85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label85.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label85.Location = new System.Drawing.Point(15, 136);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(105, 25);
            this.label85.TabIndex = 290;
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label86
            // 
            this.label86.BackColor = System.Drawing.SystemColors.Window;
            this.label86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label86.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label86.Location = new System.Drawing.Point(15, 112);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(105, 25);
            this.label86.TabIndex = 289;
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox133
            // 
            this.textBox133.BackColor = System.Drawing.SystemColors.Window;
            this.textBox133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox133.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox133.Location = new System.Drawing.Point(462, 136);
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new System.Drawing.Size(80, 25);
            this.textBox133.TabIndex = 278;
            this.textBox133.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox134
            // 
            this.textBox134.BackColor = System.Drawing.SystemColors.Window;
            this.textBox134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox134.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox134.Location = new System.Drawing.Point(383, 136);
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new System.Drawing.Size(80, 25);
            this.textBox134.TabIndex = 277;
            this.textBox134.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox135
            // 
            this.textBox135.BackColor = System.Drawing.SystemColors.Window;
            this.textBox135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox135.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox135.Location = new System.Drawing.Point(233, 136);
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new System.Drawing.Size(90, 25);
            this.textBox135.TabIndex = 276;
            this.textBox135.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label87
            // 
            this.label87.BackColor = System.Drawing.SystemColors.Window;
            this.label87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label87.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label87.Location = new System.Drawing.Point(119, 64);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(115, 25);
            this.label87.TabIndex = 282;
            this.label87.Text = "采购执行时间成本";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox136
            // 
            this.textBox136.BackColor = System.Drawing.SystemColors.Window;
            this.textBox136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox136.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox136.Location = new System.Drawing.Point(462, 160);
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new System.Drawing.Size(80, 25);
            this.textBox136.TabIndex = 281;
            this.textBox136.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label88
            // 
            this.label88.BackColor = System.Drawing.SystemColors.Window;
            this.label88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label88.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label88.Location = new System.Drawing.Point(119, 88);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(115, 25);
            this.label88.TabIndex = 283;
            this.label88.Text = "通讯成本";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox137
            // 
            this.textBox137.BackColor = System.Drawing.SystemColors.Window;
            this.textBox137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox137.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox137.Location = new System.Drawing.Point(462, 112);
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new System.Drawing.Size(80, 25);
            this.textBox137.TabIndex = 275;
            this.textBox137.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox138
            // 
            this.textBox138.BackColor = System.Drawing.SystemColors.Window;
            this.textBox138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox138.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox138.Location = new System.Drawing.Point(233, 88);
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new System.Drawing.Size(90, 25);
            this.textBox138.TabIndex = 270;
            this.textBox138.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox139
            // 
            this.textBox139.BackColor = System.Drawing.SystemColors.Window;
            this.textBox139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox139.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox139.Location = new System.Drawing.Point(383, 88);
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new System.Drawing.Size(80, 25);
            this.textBox139.TabIndex = 271;
            this.textBox139.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox140
            // 
            this.textBox140.BackColor = System.Drawing.SystemColors.Window;
            this.textBox140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox140.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox140.Location = new System.Drawing.Point(383, 160);
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new System.Drawing.Size(80, 25);
            this.textBox140.TabIndex = 280;
            this.textBox140.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox141
            // 
            this.textBox141.BackColor = System.Drawing.SystemColors.Window;
            this.textBox141.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox141.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox141.Location = new System.Drawing.Point(383, 112);
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new System.Drawing.Size(80, 25);
            this.textBox141.TabIndex = 274;
            this.textBox141.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox142
            // 
            this.textBox142.BackColor = System.Drawing.SystemColors.Window;
            this.textBox142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox142.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox142.Location = new System.Drawing.Point(233, 160);
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new System.Drawing.Size(90, 25);
            this.textBox142.TabIndex = 279;
            this.textBox142.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox143
            // 
            this.textBox143.BackColor = System.Drawing.SystemColors.Window;
            this.textBox143.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox143.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox143.Location = new System.Drawing.Point(233, 112);
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new System.Drawing.Size(90, 25);
            this.textBox143.TabIndex = 273;
            this.textBox143.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox144
            // 
            this.textBox144.BackColor = System.Drawing.SystemColors.Window;
            this.textBox144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox144.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox144.Location = new System.Drawing.Point(462, 88);
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new System.Drawing.Size(80, 25);
            this.textBox144.TabIndex = 272;
            this.textBox144.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label89
            // 
            this.label89.BackColor = System.Drawing.SystemColors.Window;
            this.label89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label89.Location = new System.Drawing.Point(119, 160);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(115, 25);
            this.label89.TabIndex = 286;
            this.label89.Text = "安装成本";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox145
            // 
            this.textBox145.BackColor = System.Drawing.SystemColors.Window;
            this.textBox145.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox145.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox145.Location = new System.Drawing.Point(233, 64);
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new System.Drawing.Size(90, 25);
            this.textBox145.TabIndex = 267;
            this.textBox145.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label90
            // 
            this.label90.BackColor = System.Drawing.SystemColors.Window;
            this.label90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label90.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label90.Location = new System.Drawing.Point(119, 136);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(115, 25);
            this.label90.TabIndex = 285;
            this.label90.Text = "物流成本";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox146
            // 
            this.textBox146.BackColor = System.Drawing.SystemColors.Window;
            this.textBox146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox146.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox146.Location = new System.Drawing.Point(383, 64);
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new System.Drawing.Size(80, 25);
            this.textBox146.TabIndex = 268;
            this.textBox146.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.SystemColors.Window;
            this.label91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label91.Location = new System.Drawing.Point(119, 112);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(115, 25);
            this.label91.TabIndex = 284;
            this.label91.Text = "文件成本";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox147
            // 
            this.textBox147.BackColor = System.Drawing.SystemColors.Window;
            this.textBox147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox147.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox147.Location = new System.Drawing.Point(462, 64);
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new System.Drawing.Size(80, 25);
            this.textBox147.TabIndex = 269;
            this.textBox147.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.SystemColors.Window;
            this.textBox94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox94.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox94.Location = new System.Drawing.Point(620, 448);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(80, 25);
            this.textBox94.TabIndex = 266;
            this.textBox94.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.SystemColors.Window;
            this.textBox95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox95.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox95.Location = new System.Drawing.Point(322, 448);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(62, 25);
            this.textBox95.TabIndex = 265;
            this.textBox95.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox96
            // 
            this.textBox96.BackColor = System.Drawing.SystemColors.Window;
            this.textBox96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox96.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox96.Location = new System.Drawing.Point(541, 448);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(80, 25);
            this.textBox96.TabIndex = 264;
            this.textBox96.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.SystemColors.Window;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(15, 448);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 25);
            this.label28.TabIndex = 263;
            this.label28.Text = "其他成本";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.SystemColors.Window;
            this.textBox98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox98.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox98.Location = new System.Drawing.Point(462, 448);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(80, 25);
            this.textBox98.TabIndex = 261;
            this.textBox98.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.SystemColors.Window;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox99.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox99.Location = new System.Drawing.Point(383, 448);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(80, 25);
            this.textBox99.TabIndex = 260;
            this.textBox99.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox100
            // 
            this.textBox100.BackColor = System.Drawing.SystemColors.Window;
            this.textBox100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox100.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox100.Location = new System.Drawing.Point(233, 448);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(90, 25);
            this.textBox100.TabIndex = 259;
            this.textBox100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.SystemColors.Window;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label34.Location = new System.Drawing.Point(119, 448);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(115, 25);
            this.label34.TabIndex = 262;
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.SystemColors.Window;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox70.Location = new System.Drawing.Point(620, 400);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(80, 25);
            this.textBox70.TabIndex = 257;
            this.textBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.SystemColors.Window;
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox74.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox74.Location = new System.Drawing.Point(620, 424);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(80, 25);
            this.textBox74.TabIndex = 258;
            this.textBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox121
            // 
            this.textBox121.BackColor = System.Drawing.SystemColors.Window;
            this.textBox121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox121.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox121.Location = new System.Drawing.Point(620, 256);
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new System.Drawing.Size(80, 25);
            this.textBox121.TabIndex = 255;
            this.textBox121.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox122
            // 
            this.textBox122.BackColor = System.Drawing.SystemColors.Window;
            this.textBox122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox122.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox122.Location = new System.Drawing.Point(620, 280);
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new System.Drawing.Size(80, 25);
            this.textBox122.TabIndex = 256;
            this.textBox122.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox123
            // 
            this.textBox123.BackColor = System.Drawing.SystemColors.Window;
            this.textBox123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox123.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox123.Location = new System.Drawing.Point(620, 232);
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new System.Drawing.Size(80, 25);
            this.textBox123.TabIndex = 254;
            this.textBox123.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox124
            // 
            this.textBox124.BackColor = System.Drawing.SystemColors.Window;
            this.textBox124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox124.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox124.Location = new System.Drawing.Point(620, 40);
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new System.Drawing.Size(80, 25);
            this.textBox124.TabIndex = 251;
            this.textBox124.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox125
            // 
            this.textBox125.BackColor = System.Drawing.SystemColors.Window;
            this.textBox125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox125.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox125.Location = new System.Drawing.Point(620, 208);
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new System.Drawing.Size(80, 25);
            this.textBox125.TabIndex = 253;
            this.textBox125.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.SystemColors.Window;
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox85.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox85.Location = new System.Drawing.Point(322, 400);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(62, 25);
            this.textBox85.TabIndex = 249;
            this.textBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.SystemColors.Window;
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox86.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox86.Location = new System.Drawing.Point(322, 424);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(62, 25);
            this.textBox86.TabIndex = 250;
            this.textBox86.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label73
            // 
            this.label73.BackColor = System.Drawing.SystemColors.Window;
            this.label73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label73.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label73.Location = new System.Drawing.Point(322, 16);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(62, 25);
            this.label73.TabIndex = 248;
            this.label73.Text = "贴现率";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox87.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox87.Location = new System.Drawing.Point(322, 256);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(62, 25);
            this.textBox87.TabIndex = 246;
            this.textBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.Window;
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox88.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox88.Location = new System.Drawing.Point(322, 40);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(62, 25);
            this.textBox88.TabIndex = 242;
            this.textBox88.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox89.Location = new System.Drawing.Point(322, 208);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(62, 25);
            this.textBox89.TabIndex = 244;
            this.textBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Window;
            this.textBox90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox90.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox90.Location = new System.Drawing.Point(322, 280);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(62, 25);
            this.textBox90.TabIndex = 247;
            this.textBox90.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.SystemColors.Window;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox91.Location = new System.Drawing.Point(322, 232);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(62, 25);
            this.textBox91.TabIndex = 245;
            this.textBox91.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.SystemColors.Window;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox77.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox77.Location = new System.Drawing.Point(541, 400);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(80, 25);
            this.textBox77.TabIndex = 240;
            this.textBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.SystemColors.Window;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox78.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox78.Location = new System.Drawing.Point(541, 424);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(80, 25);
            this.textBox78.TabIndex = 241;
            this.textBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.Window;
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox79.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox79.Location = new System.Drawing.Point(541, 256);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(80, 25);
            this.textBox79.TabIndex = 237;
            this.textBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.SystemColors.Window;
            this.label72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label72.Location = new System.Drawing.Point(541, 16);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(80, 25);
            this.label72.TabIndex = 239;
            this.label72.Text = "N3";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.Window;
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox80.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox80.Location = new System.Drawing.Point(541, 280);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(80, 25);
            this.textBox80.TabIndex = 238;
            this.textBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.Window;
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox81.Location = new System.Drawing.Point(541, 232);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(80, 25);
            this.textBox81.TabIndex = 236;
            this.textBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.SystemColors.Window;
            this.textBox82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox82.Location = new System.Drawing.Point(541, 40);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(80, 25);
            this.textBox82.TabIndex = 233;
            this.textBox82.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.Window;
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox83.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox83.Location = new System.Drawing.Point(541, 208);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(80, 25);
            this.textBox83.TabIndex = 235;
            this.textBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.SystemColors.Window;
            this.label68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label68.Location = new System.Drawing.Point(15, 424);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(105, 25);
            this.label68.TabIndex = 232;
            this.label68.Text = "处置成本";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.SystemColors.Window;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label69.Location = new System.Drawing.Point(15, 400);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(105, 25);
            this.label69.TabIndex = 231;
            this.label69.Text = "修理成本";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.SystemColors.Window;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox69.Location = new System.Drawing.Point(462, 400);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(80, 25);
            this.textBox69.TabIndex = 223;
            this.textBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.SystemColors.Window;
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox71.Location = new System.Drawing.Point(383, 400);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(80, 25);
            this.textBox71.TabIndex = 222;
            this.textBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.SystemColors.Window;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox72.Location = new System.Drawing.Point(233, 400);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(90, 25);
            this.textBox72.TabIndex = 221;
            this.textBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.SystemColors.Window;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox73.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox73.Location = new System.Drawing.Point(462, 424);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(80, 25);
            this.textBox73.TabIndex = 227;
            this.textBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.SystemColors.Window;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox75.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox75.Location = new System.Drawing.Point(383, 424);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(80, 25);
            this.textBox75.TabIndex = 226;
            this.textBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.SystemColors.Window;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox76.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox76.Location = new System.Drawing.Point(233, 424);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(90, 25);
            this.textBox76.TabIndex = 225;
            this.textBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.SystemColors.Window;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label70.Location = new System.Drawing.Point(119, 424);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(115, 25);
            this.label70.TabIndex = 230;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.SystemColors.Window;
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label71.Location = new System.Drawing.Point(119, 400);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(115, 25);
            this.label71.TabIndex = 229;
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.Window;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label55.Location = new System.Drawing.Point(15, 16);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(105, 25);
            this.label55.TabIndex = 214;
            this.label55.Text = "成本类型";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.SystemColors.Window;
            this.label62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label62.Location = new System.Drawing.Point(15, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(105, 25);
            this.label62.TabIndex = 215;
            this.label62.Text = "采购成本";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.SystemColors.Window;
            this.label64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label64.Location = new System.Drawing.Point(15, 208);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(105, 25);
            this.label64.TabIndex = 217;
            this.label64.Text = "运营成本";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.SystemColors.Window;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label65.Location = new System.Drawing.Point(15, 280);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(105, 25);
            this.label65.TabIndex = 220;
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.SystemColors.Window;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label66.Location = new System.Drawing.Point(15, 256);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(105, 25);
            this.label66.TabIndex = 219;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.SystemColors.Window;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label67.Location = new System.Drawing.Point(15, 232);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(105, 25);
            this.label67.TabIndex = 218;
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox61.Location = new System.Drawing.Point(233, 40);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(90, 25);
            this.textBox61.TabIndex = 0;
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.Window;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label54.Location = new System.Drawing.Point(233, 16);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(90, 25);
            this.label54.TabIndex = 86;
            this.label54.Text = "设备生命周期";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // shp2_lbl
            // 
            this.shp2_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.shp2_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shp2_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shp2_lbl.Location = new System.Drawing.Point(383, 16);
            this.shp2_lbl.Name = "shp2_lbl";
            this.shp2_lbl.Size = new System.Drawing.Size(80, 25);
            this.shp2_lbl.TabIndex = 86;
            this.shp2_lbl.Text = "N1 ";
            this.shp2_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.Window;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label56.Location = new System.Drawing.Point(119, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(115, 25);
            this.label56.TabIndex = 167;
            this.label56.Text = "成本要素";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hlca_ed
            // 
            this.hlca_ed.BackColor = System.Drawing.SystemColors.Window;
            this.hlca_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlca_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlca_ed.Location = new System.Drawing.Point(462, 256);
            this.hlca_ed.Name = "hlca_ed";
            this.hlca_ed.Size = new System.Drawing.Size(80, 25);
            this.hlca_ed.TabIndex = 18;
            this.hlca_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.SystemColors.Window;
            this.label57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label57.Location = new System.Drawing.Point(462, 16);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(80, 25);
            this.label57.TabIndex = 86;
            this.label57.Text = "N2";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zd_lbl
            // 
            this.zd_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.zd_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zd_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zd_lbl.Location = new System.Drawing.Point(119, 40);
            this.zd_lbl.Name = "zd_lbl";
            this.zd_lbl.Size = new System.Drawing.Size(115, 25);
            this.zd_lbl.TabIndex = 168;
            this.zd_lbl.Text = "设备价格";
            this.zd_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hlbc_ed
            // 
            this.hlbc_ed.BackColor = System.Drawing.SystemColors.Window;
            this.hlbc_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlbc_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlbc_ed.Location = new System.Drawing.Point(383, 256);
            this.hlbc_ed.Name = "hlbc_ed";
            this.hlbc_ed.Size = new System.Drawing.Size(80, 25);
            this.hlbc_ed.TabIndex = 17;
            this.hlbc_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.SystemColors.Window;
            this.label58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label58.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label58.Location = new System.Drawing.Point(620, 16);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 25);
            this.label58.TabIndex = 86;
            this.label58.Text = "总成本";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hlab_ed
            // 
            this.hlab_ed.BackColor = System.Drawing.SystemColors.Window;
            this.hlab_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlab_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlab_ed.Location = new System.Drawing.Point(233, 256);
            this.hlab_ed.Name = "hlab_ed";
            this.hlab_ed.Size = new System.Drawing.Size(90, 25);
            this.hlab_ed.TabIndex = 16;
            this.hlab_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlca_zx
            // 
            this.hlca_zx.BackColor = System.Drawing.SystemColors.Window;
            this.hlca_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlca_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlca_zx.Location = new System.Drawing.Point(462, 280);
            this.hlca_zx.Name = "hlca_zx";
            this.hlca_zx.Size = new System.Drawing.Size(80, 25);
            this.hlca_zx.TabIndex = 22;
            this.hlca_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // zx_lbl
            // 
            this.zx_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.zx_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zx_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zx_lbl.Location = new System.Drawing.Point(119, 208);
            this.zx_lbl.Name = "zx_lbl";
            this.zx_lbl.Size = new System.Drawing.Size(115, 25);
            this.zx_lbl.TabIndex = 170;
            this.zx_lbl.Text = "操作人员培训成本";
            this.zx_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.SystemColors.Window;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox62.Location = new System.Drawing.Point(462, 232);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(80, 25);
            this.textBox62.TabIndex = 14;
            this.textBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.SystemColors.Window;
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox63.Location = new System.Drawing.Point(233, 208);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(90, 25);
            this.textBox63.TabIndex = 8;
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlbc_zd
            // 
            this.qlbc_zd.BackColor = System.Drawing.SystemColors.Window;
            this.qlbc_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlbc_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlbc_zd.Location = new System.Drawing.Point(383, 40);
            this.qlbc_zd.Name = "qlbc_zd";
            this.qlbc_zd.Size = new System.Drawing.Size(80, 25);
            this.qlbc_zd.TabIndex = 1;
            this.qlbc_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlbc_zx
            // 
            this.qlbc_zx.BackColor = System.Drawing.SystemColors.Window;
            this.qlbc_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlbc_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlbc_zx.Location = new System.Drawing.Point(383, 208);
            this.qlbc_zx.Name = "qlbc_zx";
            this.qlbc_zx.Size = new System.Drawing.Size(80, 25);
            this.qlbc_zx.TabIndex = 9;
            this.qlbc_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlbc_zx
            // 
            this.hlbc_zx.BackColor = System.Drawing.SystemColors.Window;
            this.hlbc_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlbc_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlbc_zx.Location = new System.Drawing.Point(383, 280);
            this.hlbc_zx.Name = "hlbc_zx";
            this.hlbc_zx.Size = new System.Drawing.Size(80, 25);
            this.hlbc_zx.TabIndex = 21;
            this.hlbc_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlbc_zd
            // 
            this.hlbc_zd.BackColor = System.Drawing.SystemColors.Window;
            this.hlbc_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlbc_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlbc_zd.Location = new System.Drawing.Point(383, 232);
            this.hlbc_zd.Name = "hlbc_zd";
            this.hlbc_zd.Size = new System.Drawing.Size(80, 25);
            this.hlbc_zd.TabIndex = 13;
            this.hlbc_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlab_zx
            // 
            this.hlab_zx.BackColor = System.Drawing.SystemColors.Window;
            this.hlab_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlab_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlab_zx.Location = new System.Drawing.Point(233, 280);
            this.hlab_zx.Name = "hlab_zx";
            this.hlab_zx.Size = new System.Drawing.Size(90, 25);
            this.hlab_zx.TabIndex = 20;
            this.hlab_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.Window;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox64.Location = new System.Drawing.Point(462, 40);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(80, 25);
            this.textBox64.TabIndex = 2;
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.SystemColors.Window;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox65.Location = new System.Drawing.Point(233, 232);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(90, 25);
            this.textBox65.TabIndex = 12;
            this.textBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.SystemColors.Window;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox66.Location = new System.Drawing.Point(462, 208);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(80, 25);
            this.textBox66.TabIndex = 10;
            this.textBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label59
            // 
            this.label59.BackColor = System.Drawing.SystemColors.Window;
            this.label59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label59.Location = new System.Drawing.Point(119, 280);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(115, 25);
            this.label59.TabIndex = 213;
            this.label59.Text = "消耗性成本";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.SystemColors.Window;
            this.label60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label60.Location = new System.Drawing.Point(119, 256);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(115, 25);
            this.label60.TabIndex = 212;
            this.label60.Text = "运行成本(水电等)";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.SystemColors.Window;
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label61.Location = new System.Drawing.Point(119, 232);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(115, 25);
            this.label61.TabIndex = 211;
            this.label61.Text = "设备操作人员成本";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.textBox8);
            this.panel9.Controls.Add(this.textBox9);
            this.panel9.Controls.Add(this.textBox11);
            this.panel9.Controls.Add(this.checkBox10);
            this.panel9.Controls.Add(this.checkBox11);
            this.panel9.Controls.Add(this.checkBox12);
            this.panel9.Controls.Add(this.textBox47);
            this.panel9.Controls.Add(this.textBox48);
            this.panel9.Controls.Add(this.textBox49);
            this.panel9.Controls.Add(this.textBox50);
            this.panel9.Controls.Add(this.textBox51);
            this.panel9.Controls.Add(this.textBox52);
            this.panel9.Controls.Add(this.textBox53);
            this.panel9.Controls.Add(this.textBox54);
            this.panel9.Controls.Add(this.textBox55);
            this.panel9.Controls.Add(this.textBox56);
            this.panel9.Controls.Add(this.textBox57);
            this.panel9.Controls.Add(this.textBox58);
            this.panel9.Controls.Add(this.textBox59);
            this.panel9.Controls.Add(this.textBox60);
            this.panel9.Controls.Add(this.textBox261);
            this.panel9.Controls.Add(this.textBox262);
            this.panel9.Controls.Add(this.textBox263);
            this.panel9.Controls.Add(this.textBox264);
            this.panel9.Controls.Add(this.textBox265);
            this.panel9.Controls.Add(this.textBox266);
            this.panel9.Controls.Add(this.textBox267);
            this.panel9.Controls.Add(this.textBox268);
            this.panel9.Controls.Add(this.textBox269);
            this.panel9.Controls.Add(this.textBox270);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.textBox271);
            this.panel9.Controls.Add(this.textBox272);
            this.panel9.Controls.Add(this.textBox273);
            this.panel9.Controls.Add(this.textBox274);
            this.panel9.Controls.Add(this.checkBox13);
            this.panel9.Controls.Add(this.checkBox29);
            this.panel9.Controls.Add(this.checkBox30);
            this.panel9.Controls.Add(this.checkBox31);
            this.panel9.Controls.Add(this.textBox279);
            this.panel9.Controls.Add(this.textBox280);
            this.panel9.Controls.Add(this.textBox281);
            this.panel9.Controls.Add(this.textBox282);
            this.panel9.Controls.Add(this.textBox283);
            this.panel9.Controls.Add(this.textBox284);
            this.panel9.Controls.Add(this.textBox285);
            this.panel9.Controls.Add(this.textBox286);
            this.panel9.Controls.Add(this.textBox287);
            this.panel9.Controls.Add(this.textBox288);
            this.panel9.Controls.Add(this.textBox289);
            this.panel9.Controls.Add(this.textBox290);
            this.panel9.Controls.Add(this.textBox291);
            this.panel9.Controls.Add(this.textBox292);
            this.panel9.Controls.Add(this.textBox293);
            this.panel9.Controls.Add(this.textBox294);
            this.panel9.Controls.Add(this.checkBox32);
            this.panel9.Controls.Add(this.checkBox33);
            this.panel9.Controls.Add(this.checkBox34);
            this.panel9.Controls.Add(this.checkBox35);
            this.panel9.Controls.Add(this.textBox299);
            this.panel9.Controls.Add(this.textBox300);
            this.panel9.Controls.Add(this.textBox301);
            this.panel9.Controls.Add(this.textBox302);
            this.panel9.Controls.Add(this.textBox303);
            this.panel9.Controls.Add(this.textBox304);
            this.panel9.Controls.Add(this.textBox305);
            this.panel9.Controls.Add(this.textBox306);
            this.panel9.Controls.Add(this.textBox307);
            this.panel9.Controls.Add(this.textBox308);
            this.panel9.Controls.Add(this.textBox309);
            this.panel9.Controls.Add(this.textBox310);
            this.panel9.Controls.Add(this.textBox311);
            this.panel9.Controls.Add(this.textBox312);
            this.panel9.Controls.Add(this.textBox313);
            this.panel9.Controls.Add(this.textBox314);
            this.panel9.Controls.Add(this.checkBox36);
            this.panel9.Controls.Add(this.checkBox37);
            this.panel9.Controls.Add(this.checkBox38);
            this.panel9.Controls.Add(this.checkBox39);
            this.panel9.Controls.Add(this.textBox319);
            this.panel9.Controls.Add(this.textBox320);
            this.panel9.Controls.Add(this.textBox321);
            this.panel9.Controls.Add(this.textBox322);
            this.panel9.Controls.Add(this.textBox323);
            this.panel9.Controls.Add(this.textBox324);
            this.panel9.Controls.Add(this.textBox325);
            this.panel9.Controls.Add(this.textBox326);
            this.panel9.Controls.Add(this.textBox327);
            this.panel9.Controls.Add(this.textBox328);
            this.panel9.Controls.Add(this.textBox329);
            this.panel9.Controls.Add(this.textBox330);
            this.panel9.Controls.Add(this.textBox331);
            this.panel9.Controls.Add(this.textBox332);
            this.panel9.Controls.Add(this.textBox333);
            this.panel9.Controls.Add(this.textBox334);
            this.panel9.Controls.Add(this.checkBox40);
            this.panel9.Controls.Add(this.checkBox41);
            this.panel9.Controls.Add(this.checkBox42);
            this.panel9.Controls.Add(this.checkBox43);
            this.panel9.Controls.Add(this.label51);
            this.panel9.Controls.Add(this.textBox339);
            this.panel9.Controls.Add(this.textBox340);
            this.panel9.Controls.Add(this.textBox341);
            this.panel9.Controls.Add(this.textBox342);
            this.panel9.Controls.Add(this.label52);
            this.panel9.Controls.Add(this.textBox343);
            this.panel9.Controls.Add(this.textBox344);
            this.panel9.Controls.Add(this.textBox345);
            this.panel9.Controls.Add(this.textBox346);
            this.panel9.Controls.Add(this.textBox347);
            this.panel9.Controls.Add(this.label53);
            this.panel9.Controls.Add(this.label102);
            this.panel9.Controls.Add(this.textBox348);
            this.panel9.Controls.Add(this.textBox349);
            this.panel9.Controls.Add(this.textBox350);
            this.panel9.Controls.Add(this.textBox351);
            this.panel9.Controls.Add(this.textBox352);
            this.panel9.Controls.Add(this.textBox353);
            this.panel9.Controls.Add(this.textBox354);
            this.panel9.Location = new System.Drawing.Point(6, 644);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(718, 537);
            this.panel9.TabIndex = 219;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Window;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox8.Location = new System.Drawing.Point(293, 448);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 25);
            this.textBox8.TabIndex = 369;
            this.textBox8.Text = "经营能力";
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Window;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox9.Location = new System.Drawing.Point(293, 472);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 25);
            this.textBox9.TabIndex = 370;
            this.textBox9.Text = "经营能力";
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Window;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox11.Location = new System.Drawing.Point(293, 424);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 25);
            this.textBox11.TabIndex = 368;
            this.textBox11.Text = "经营能力";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox10.Location = new System.Drawing.Point(23, 454);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 367;
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox11.Location = new System.Drawing.Point(23, 478);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 366;
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox12.Location = new System.Drawing.Point(23, 430);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 365;
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox47.Location = new System.Drawing.Point(501, 448);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(200, 25);
            this.textBox47.TabIndex = 360;
            this.textBox47.Text = "提供";
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox48.Location = new System.Drawing.Point(501, 472);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(200, 25);
            this.textBox48.TabIndex = 361;
            this.textBox48.Text = "提供";
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Window;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox49.Location = new System.Drawing.Point(501, 424);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(200, 25);
            this.textBox49.TabIndex = 359;
            this.textBox49.Text = "提供";
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.Window;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox50.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox50.Location = new System.Drawing.Point(392, 448);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(110, 25);
            this.textBox50.TabIndex = 357;
            this.textBox50.Text = "文本";
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.SystemColors.Window;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox51.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox51.Location = new System.Drawing.Point(392, 472);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(110, 25);
            this.textBox51.TabIndex = 358;
            this.textBox51.Text = "文本";
            this.textBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.SystemColors.Window;
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox52.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox52.Location = new System.Drawing.Point(392, 424);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(110, 25);
            this.textBox52.TabIndex = 356;
            this.textBox52.Text = "文本";
            this.textBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Window;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox53.Location = new System.Drawing.Point(44, 472);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(250, 25);
            this.textBox53.TabIndex = 355;
            this.textBox53.Text = "反应措施";
            this.textBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Window;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox54.Location = new System.Drawing.Point(15, 448);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(30, 25);
            this.textBox54.TabIndex = 352;
            this.textBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Window;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox55.Location = new System.Drawing.Point(15, 472);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(30, 25);
            this.textBox55.TabIndex = 354;
            this.textBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Window;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox56.Location = new System.Drawing.Point(44, 448);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(250, 25);
            this.textBox56.TabIndex = 353;
            this.textBox56.Text = "财务结构";
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Window;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox57.Location = new System.Drawing.Point(15, 424);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(30, 25);
            this.textBox57.TabIndex = 350;
            this.textBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.Window;
            this.textBox58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox58.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox58.Location = new System.Drawing.Point(44, 424);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(250, 25);
            this.textBox58.TabIndex = 351;
            this.textBox58.Text = "营业状况";
            this.textBox58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox59.Location = new System.Drawing.Point(293, 328);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(100, 25);
            this.textBox59.TabIndex = 346;
            this.textBox59.Text = "服务";
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Window;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox60.Location = new System.Drawing.Point(293, 376);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(100, 25);
            this.textBox60.TabIndex = 348;
            this.textBox60.Text = "技术能力";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox261
            // 
            this.textBox261.BackColor = System.Drawing.SystemColors.Window;
            this.textBox261.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox261.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox261.Location = new System.Drawing.Point(293, 400);
            this.textBox261.Name = "textBox261";
            this.textBox261.Size = new System.Drawing.Size(100, 25);
            this.textBox261.TabIndex = 349;
            this.textBox261.Text = "技术能力";
            this.textBox261.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox262
            // 
            this.textBox262.BackColor = System.Drawing.SystemColors.Window;
            this.textBox262.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox262.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox262.Location = new System.Drawing.Point(293, 352);
            this.textBox262.Name = "textBox262";
            this.textBox262.Size = new System.Drawing.Size(100, 25);
            this.textBox262.TabIndex = 347;
            this.textBox262.Text = "技术能力";
            this.textBox262.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox263
            // 
            this.textBox263.BackColor = System.Drawing.SystemColors.Window;
            this.textBox263.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox263.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox263.Location = new System.Drawing.Point(293, 232);
            this.textBox263.Name = "textBox263";
            this.textBox263.Size = new System.Drawing.Size(100, 25);
            this.textBox263.TabIndex = 342;
            this.textBox263.Text = "交货";
            this.textBox263.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox264
            // 
            this.textBox264.BackColor = System.Drawing.SystemColors.Window;
            this.textBox264.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox264.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox264.Location = new System.Drawing.Point(293, 280);
            this.textBox264.Name = "textBox264";
            this.textBox264.Size = new System.Drawing.Size(100, 25);
            this.textBox264.TabIndex = 344;
            this.textBox264.Text = "服务";
            this.textBox264.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox265
            // 
            this.textBox265.BackColor = System.Drawing.SystemColors.Window;
            this.textBox265.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox265.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox265.Location = new System.Drawing.Point(293, 304);
            this.textBox265.Name = "textBox265";
            this.textBox265.Size = new System.Drawing.Size(100, 25);
            this.textBox265.TabIndex = 345;
            this.textBox265.Text = "服务";
            this.textBox265.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox266
            // 
            this.textBox266.BackColor = System.Drawing.SystemColors.Window;
            this.textBox266.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox266.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox266.Location = new System.Drawing.Point(293, 256);
            this.textBox266.Name = "textBox266";
            this.textBox266.Size = new System.Drawing.Size(100, 25);
            this.textBox266.TabIndex = 343;
            this.textBox266.Text = "服务";
            this.textBox266.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox267
            // 
            this.textBox267.BackColor = System.Drawing.SystemColors.Window;
            this.textBox267.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox267.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox267.Location = new System.Drawing.Point(293, 136);
            this.textBox267.Name = "textBox267";
            this.textBox267.Size = new System.Drawing.Size(100, 25);
            this.textBox267.TabIndex = 338;
            this.textBox267.Text = "质量";
            this.textBox267.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox268
            // 
            this.textBox268.BackColor = System.Drawing.SystemColors.Window;
            this.textBox268.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox268.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox268.Location = new System.Drawing.Point(293, 184);
            this.textBox268.Name = "textBox268";
            this.textBox268.Size = new System.Drawing.Size(100, 25);
            this.textBox268.TabIndex = 340;
            this.textBox268.Text = "交货";
            this.textBox268.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox269
            // 
            this.textBox269.BackColor = System.Drawing.SystemColors.Window;
            this.textBox269.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox269.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox269.Location = new System.Drawing.Point(293, 208);
            this.textBox269.Name = "textBox269";
            this.textBox269.Size = new System.Drawing.Size(100, 25);
            this.textBox269.TabIndex = 341;
            this.textBox269.Text = "交货";
            this.textBox269.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox270
            // 
            this.textBox270.BackColor = System.Drawing.SystemColors.Window;
            this.textBox270.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox270.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox270.Location = new System.Drawing.Point(293, 160);
            this.textBox270.Name = "textBox270";
            this.textBox270.Size = new System.Drawing.Size(100, 25);
            this.textBox270.TabIndex = 339;
            this.textBox270.Text = "质量";
            this.textBox270.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.SystemColors.Window;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(293, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 25);
            this.label30.TabIndex = 337;
            this.label30.Text = "问题类别";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox271
            // 
            this.textBox271.BackColor = System.Drawing.SystemColors.Window;
            this.textBox271.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox271.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox271.Location = new System.Drawing.Point(293, 40);
            this.textBox271.Name = "textBox271";
            this.textBox271.Size = new System.Drawing.Size(100, 25);
            this.textBox271.TabIndex = 333;
            this.textBox271.Text = "价格";
            this.textBox271.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox272
            // 
            this.textBox272.BackColor = System.Drawing.SystemColors.Window;
            this.textBox272.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox272.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox272.Location = new System.Drawing.Point(293, 88);
            this.textBox272.Name = "textBox272";
            this.textBox272.Size = new System.Drawing.Size(100, 25);
            this.textBox272.TabIndex = 335;
            this.textBox272.Text = "质量";
            this.textBox272.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox273
            // 
            this.textBox273.BackColor = System.Drawing.SystemColors.Window;
            this.textBox273.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox273.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox273.Location = new System.Drawing.Point(293, 112);
            this.textBox273.Name = "textBox273";
            this.textBox273.Size = new System.Drawing.Size(100, 25);
            this.textBox273.TabIndex = 336;
            this.textBox273.Text = "质量";
            this.textBox273.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox274
            // 
            this.textBox274.BackColor = System.Drawing.SystemColors.Window;
            this.textBox274.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox274.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox274.Location = new System.Drawing.Point(293, 64);
            this.textBox274.Name = "textBox274";
            this.textBox274.Size = new System.Drawing.Size(100, 25);
            this.textBox274.TabIndex = 334;
            this.textBox274.Text = "价格";
            this.textBox274.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox13.Location = new System.Drawing.Point(23, 333);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 332;
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox29.Location = new System.Drawing.Point(23, 382);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(15, 14);
            this.checkBox29.TabIndex = 331;
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox30.Location = new System.Drawing.Point(23, 406);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(15, 14);
            this.checkBox30.TabIndex = 330;
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox31.Location = new System.Drawing.Point(23, 358);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(15, 14);
            this.checkBox31.TabIndex = 329;
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // textBox279
            // 
            this.textBox279.BackColor = System.Drawing.SystemColors.Window;
            this.textBox279.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox279.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox279.Location = new System.Drawing.Point(501, 328);
            this.textBox279.Name = "textBox279";
            this.textBox279.Size = new System.Drawing.Size(200, 25);
            this.textBox279.TabIndex = 321;
            this.textBox279.Text = "最高30%";
            this.textBox279.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox280
            // 
            this.textBox280.BackColor = System.Drawing.SystemColors.Window;
            this.textBox280.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox280.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox280.Location = new System.Drawing.Point(501, 376);
            this.textBox280.Name = "textBox280";
            this.textBox280.Size = new System.Drawing.Size(200, 25);
            this.textBox280.TabIndex = 323;
            this.textBox280.Text = "提供";
            this.textBox280.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox281
            // 
            this.textBox281.BackColor = System.Drawing.SystemColors.Window;
            this.textBox281.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox281.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox281.Location = new System.Drawing.Point(501, 400);
            this.textBox281.Name = "textBox281";
            this.textBox281.Size = new System.Drawing.Size(200, 25);
            this.textBox281.TabIndex = 324;
            this.textBox281.Text = "提供";
            this.textBox281.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox282
            // 
            this.textBox282.BackColor = System.Drawing.SystemColors.Window;
            this.textBox282.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox282.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox282.Location = new System.Drawing.Point(501, 352);
            this.textBox282.Name = "textBox282";
            this.textBox282.Size = new System.Drawing.Size(200, 25);
            this.textBox282.TabIndex = 322;
            this.textBox282.Text = "提供";
            this.textBox282.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox283
            // 
            this.textBox283.BackColor = System.Drawing.SystemColors.Window;
            this.textBox283.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox283.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox283.Location = new System.Drawing.Point(392, 328);
            this.textBox283.Name = "textBox283";
            this.textBox283.Size = new System.Drawing.Size(110, 25);
            this.textBox283.TabIndex = 317;
            this.textBox283.Text = "数量";
            this.textBox283.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox284
            // 
            this.textBox284.BackColor = System.Drawing.SystemColors.Window;
            this.textBox284.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox284.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox284.Location = new System.Drawing.Point(392, 376);
            this.textBox284.Name = "textBox284";
            this.textBox284.Size = new System.Drawing.Size(110, 25);
            this.textBox284.TabIndex = 319;
            this.textBox284.Text = "文本";
            this.textBox284.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox285
            // 
            this.textBox285.BackColor = System.Drawing.SystemColors.Window;
            this.textBox285.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox285.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox285.Location = new System.Drawing.Point(392, 400);
            this.textBox285.Name = "textBox285";
            this.textBox285.Size = new System.Drawing.Size(110, 25);
            this.textBox285.TabIndex = 320;
            this.textBox285.Text = "文本";
            this.textBox285.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox286
            // 
            this.textBox286.BackColor = System.Drawing.SystemColors.Window;
            this.textBox286.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox286.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox286.Location = new System.Drawing.Point(392, 352);
            this.textBox286.Name = "textBox286";
            this.textBox286.Size = new System.Drawing.Size(110, 25);
            this.textBox286.TabIndex = 318;
            this.textBox286.Text = "文本";
            this.textBox286.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox287
            // 
            this.textBox287.BackColor = System.Drawing.SystemColors.Window;
            this.textBox287.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox287.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox287.Location = new System.Drawing.Point(15, 328);
            this.textBox287.Name = "textBox287";
            this.textBox287.Size = new System.Drawing.Size(30, 25);
            this.textBox287.TabIndex = 309;
            this.textBox287.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox288
            // 
            this.textBox288.BackColor = System.Drawing.SystemColors.Window;
            this.textBox288.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox288.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox288.Location = new System.Drawing.Point(44, 400);
            this.textBox288.Name = "textBox288";
            this.textBox288.Size = new System.Drawing.Size(250, 25);
            this.textBox288.TabIndex = 316;
            this.textBox288.Text = "工作技术";
            this.textBox288.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox289
            // 
            this.textBox289.BackColor = System.Drawing.SystemColors.Window;
            this.textBox289.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox289.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox289.Location = new System.Drawing.Point(15, 376);
            this.textBox289.Name = "textBox289";
            this.textBox289.Size = new System.Drawing.Size(30, 25);
            this.textBox289.TabIndex = 313;
            this.textBox289.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox290
            // 
            this.textBox290.BackColor = System.Drawing.SystemColors.Window;
            this.textBox290.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox290.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox290.Location = new System.Drawing.Point(44, 328);
            this.textBox290.Name = "textBox290";
            this.textBox290.Size = new System.Drawing.Size(250, 25);
            this.textBox290.TabIndex = 310;
            this.textBox290.Text = "外包率";
            this.textBox290.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox291
            // 
            this.textBox291.BackColor = System.Drawing.SystemColors.Window;
            this.textBox291.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox291.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox291.Location = new System.Drawing.Point(15, 400);
            this.textBox291.Name = "textBox291";
            this.textBox291.Size = new System.Drawing.Size(30, 25);
            this.textBox291.TabIndex = 315;
            this.textBox291.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox292
            // 
            this.textBox292.BackColor = System.Drawing.SystemColors.Window;
            this.textBox292.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox292.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox292.Location = new System.Drawing.Point(44, 376);
            this.textBox292.Name = "textBox292";
            this.textBox292.Size = new System.Drawing.Size(250, 25);
            this.textBox292.TabIndex = 314;
            this.textBox292.Text = "检验设备";
            this.textBox292.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox293
            // 
            this.textBox293.BackColor = System.Drawing.SystemColors.Window;
            this.textBox293.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox293.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox293.Location = new System.Drawing.Point(15, 352);
            this.textBox293.Name = "textBox293";
            this.textBox293.Size = new System.Drawing.Size(30, 25);
            this.textBox293.TabIndex = 311;
            this.textBox293.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox294
            // 
            this.textBox294.BackColor = System.Drawing.SystemColors.Window;
            this.textBox294.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox294.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox294.Location = new System.Drawing.Point(44, 352);
            this.textBox294.Name = "textBox294";
            this.textBox294.Size = new System.Drawing.Size(250, 25);
            this.textBox294.TabIndex = 312;
            this.textBox294.Text = "机械设备";
            this.textBox294.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox32.Location = new System.Drawing.Point(23, 237);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(15, 14);
            this.checkBox32.TabIndex = 308;
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox33.Location = new System.Drawing.Point(23, 286);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(15, 14);
            this.checkBox33.TabIndex = 307;
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox34.Location = new System.Drawing.Point(23, 310);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(15, 14);
            this.checkBox34.TabIndex = 306;
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox35.Location = new System.Drawing.Point(23, 262);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(15, 14);
            this.checkBox35.TabIndex = 305;
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // textBox299
            // 
            this.textBox299.BackColor = System.Drawing.SystemColors.Window;
            this.textBox299.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox299.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox299.Location = new System.Drawing.Point(501, 232);
            this.textBox299.Name = "textBox299";
            this.textBox299.Size = new System.Drawing.Size(200, 25);
            this.textBox299.TabIndex = 297;
            this.textBox299.Text = "提供";
            this.textBox299.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox300
            // 
            this.textBox300.BackColor = System.Drawing.SystemColors.Window;
            this.textBox300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox300.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox300.Location = new System.Drawing.Point(501, 280);
            this.textBox300.Name = "textBox300";
            this.textBox300.Size = new System.Drawing.Size(200, 25);
            this.textBox300.TabIndex = 299;
            this.textBox300.Text = "提供";
            this.textBox300.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox301
            // 
            this.textBox301.BackColor = System.Drawing.SystemColors.Window;
            this.textBox301.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox301.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox301.Location = new System.Drawing.Point(501, 304);
            this.textBox301.Name = "textBox301";
            this.textBox301.Size = new System.Drawing.Size(200, 25);
            this.textBox301.TabIndex = 300;
            this.textBox301.Text = "24小时";
            this.textBox301.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox302
            // 
            this.textBox302.BackColor = System.Drawing.SystemColors.Window;
            this.textBox302.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox302.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox302.Location = new System.Drawing.Point(501, 256);
            this.textBox302.Name = "textBox302";
            this.textBox302.Size = new System.Drawing.Size(200, 25);
            this.textBox302.TabIndex = 298;
            this.textBox302.Text = "提供";
            this.textBox302.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox303
            // 
            this.textBox303.BackColor = System.Drawing.SystemColors.Window;
            this.textBox303.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox303.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox303.Location = new System.Drawing.Point(392, 232);
            this.textBox303.Name = "textBox303";
            this.textBox303.Size = new System.Drawing.Size(110, 25);
            this.textBox303.TabIndex = 293;
            this.textBox303.Text = "文本";
            this.textBox303.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox304
            // 
            this.textBox304.BackColor = System.Drawing.SystemColors.Window;
            this.textBox304.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox304.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox304.Location = new System.Drawing.Point(392, 280);
            this.textBox304.Name = "textBox304";
            this.textBox304.Size = new System.Drawing.Size(110, 25);
            this.textBox304.TabIndex = 295;
            this.textBox304.Text = "是/否字段";
            this.textBox304.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox305
            // 
            this.textBox305.BackColor = System.Drawing.SystemColors.Window;
            this.textBox305.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox305.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox305.Location = new System.Drawing.Point(392, 304);
            this.textBox305.Name = "textBox305";
            this.textBox305.Size = new System.Drawing.Size(110, 25);
            this.textBox305.TabIndex = 296;
            this.textBox305.Text = "数量";
            this.textBox305.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox306
            // 
            this.textBox306.BackColor = System.Drawing.SystemColors.Window;
            this.textBox306.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox306.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox306.Location = new System.Drawing.Point(392, 256);
            this.textBox306.Name = "textBox306";
            this.textBox306.Size = new System.Drawing.Size(110, 25);
            this.textBox306.TabIndex = 294;
            this.textBox306.Text = "是/否字段";
            this.textBox306.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox307
            // 
            this.textBox307.BackColor = System.Drawing.SystemColors.Window;
            this.textBox307.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox307.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox307.Location = new System.Drawing.Point(15, 232);
            this.textBox307.Name = "textBox307";
            this.textBox307.Size = new System.Drawing.Size(30, 25);
            this.textBox307.TabIndex = 285;
            this.textBox307.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox308
            // 
            this.textBox308.BackColor = System.Drawing.SystemColors.Window;
            this.textBox308.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox308.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox308.Location = new System.Drawing.Point(44, 304);
            this.textBox308.Name = "textBox308";
            this.textBox308.Size = new System.Drawing.Size(250, 25);
            this.textBox308.TabIndex = 292;
            this.textBox308.Text = "保养与修理的反应时间";
            this.textBox308.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox309
            // 
            this.textBox309.BackColor = System.Drawing.SystemColors.Window;
            this.textBox309.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox309.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox309.Location = new System.Drawing.Point(15, 280);
            this.textBox309.Name = "textBox309";
            this.textBox309.Size = new System.Drawing.Size(30, 25);
            this.textBox309.TabIndex = 289;
            this.textBox309.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox310
            // 
            this.textBox310.BackColor = System.Drawing.SystemColors.Window;
            this.textBox310.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox310.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox310.Location = new System.Drawing.Point(44, 232);
            this.textBox310.Name = "textBox310";
            this.textBox310.Size = new System.Drawing.Size(250, 25);
            this.textBox310.TabIndex = 286;
            this.textBox310.Text = "装运情况";
            this.textBox310.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox311
            // 
            this.textBox311.BackColor = System.Drawing.SystemColors.Window;
            this.textBox311.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox311.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox311.Location = new System.Drawing.Point(15, 304);
            this.textBox311.Name = "textBox311";
            this.textBox311.Size = new System.Drawing.Size(30, 25);
            this.textBox311.TabIndex = 291;
            this.textBox311.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox312
            // 
            this.textBox312.BackColor = System.Drawing.SystemColors.Window;
            this.textBox312.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox312.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox312.Location = new System.Drawing.Point(44, 280);
            this.textBox312.Name = "textBox312";
            this.textBox312.Size = new System.Drawing.Size(250, 25);
            this.textBox312.TabIndex = 290;
            this.textBox312.Text = "是否进行人员培训";
            this.textBox312.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox313
            // 
            this.textBox313.BackColor = System.Drawing.SystemColors.Window;
            this.textBox313.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox313.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox313.Location = new System.Drawing.Point(15, 256);
            this.textBox313.Name = "textBox313";
            this.textBox313.Size = new System.Drawing.Size(30, 25);
            this.textBox313.TabIndex = 287;
            this.textBox313.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox314
            // 
            this.textBox314.BackColor = System.Drawing.SystemColors.Window;
            this.textBox314.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox314.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox314.Location = new System.Drawing.Point(44, 256);
            this.textBox314.Name = "textBox314";
            this.textBox314.Size = new System.Drawing.Size(250, 25);
            this.textBox314.TabIndex = 288;
            this.textBox314.Text = "是否安装、调试等服务";
            this.textBox314.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox36.Location = new System.Drawing.Point(23, 141);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(15, 14);
            this.checkBox36.TabIndex = 284;
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox37.Location = new System.Drawing.Point(23, 190);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(15, 14);
            this.checkBox37.TabIndex = 283;
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox38.Location = new System.Drawing.Point(23, 214);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(15, 14);
            this.checkBox38.TabIndex = 282;
            this.checkBox38.UseVisualStyleBackColor = true;
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox39.Location = new System.Drawing.Point(23, 166);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(15, 14);
            this.checkBox39.TabIndex = 281;
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // textBox319
            // 
            this.textBox319.BackColor = System.Drawing.SystemColors.Window;
            this.textBox319.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox319.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox319.Location = new System.Drawing.Point(501, 136);
            this.textBox319.Name = "textBox319";
            this.textBox319.Size = new System.Drawing.Size(200, 25);
            this.textBox319.TabIndex = 273;
            this.textBox319.Text = "5年";
            this.textBox319.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox320
            // 
            this.textBox320.BackColor = System.Drawing.SystemColors.Window;
            this.textBox320.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox320.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox320.Location = new System.Drawing.Point(501, 184);
            this.textBox320.Name = "textBox320";
            this.textBox320.Size = new System.Drawing.Size(200, 25);
            this.textBox320.TabIndex = 275;
            this.textBox320.Text = "60天";
            this.textBox320.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox321
            // 
            this.textBox321.BackColor = System.Drawing.SystemColors.Window;
            this.textBox321.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox321.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox321.Location = new System.Drawing.Point(501, 208);
            this.textBox321.Name = "textBox321";
            this.textBox321.Size = new System.Drawing.Size(200, 25);
            this.textBox321.TabIndex = 276;
            this.textBox321.Text = "提供";
            this.textBox321.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox322
            // 
            this.textBox322.BackColor = System.Drawing.SystemColors.Window;
            this.textBox322.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox322.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox322.Location = new System.Drawing.Point(501, 160);
            this.textBox322.Name = "textBox322";
            this.textBox322.Size = new System.Drawing.Size(200, 25);
            this.textBox322.TabIndex = 274;
            this.textBox322.Text = "98%";
            this.textBox322.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox323
            // 
            this.textBox323.BackColor = System.Drawing.SystemColors.Window;
            this.textBox323.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox323.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox323.Location = new System.Drawing.Point(392, 136);
            this.textBox323.Name = "textBox323";
            this.textBox323.Size = new System.Drawing.Size(110, 25);
            this.textBox323.TabIndex = 269;
            this.textBox323.Text = "文本";
            this.textBox323.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox324
            // 
            this.textBox324.BackColor = System.Drawing.SystemColors.Window;
            this.textBox324.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox324.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox324.Location = new System.Drawing.Point(392, 184);
            this.textBox324.Name = "textBox324";
            this.textBox324.Size = new System.Drawing.Size(110, 25);
            this.textBox324.TabIndex = 271;
            this.textBox324.Text = "时间";
            this.textBox324.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox325
            // 
            this.textBox325.BackColor = System.Drawing.SystemColors.Window;
            this.textBox325.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox325.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox325.Location = new System.Drawing.Point(392, 208);
            this.textBox325.Name = "textBox325";
            this.textBox325.Size = new System.Drawing.Size(110, 25);
            this.textBox325.TabIndex = 272;
            this.textBox325.Text = "文本";
            this.textBox325.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox326
            // 
            this.textBox326.BackColor = System.Drawing.SystemColors.Window;
            this.textBox326.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox326.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox326.Location = new System.Drawing.Point(392, 160);
            this.textBox326.Name = "textBox326";
            this.textBox326.Size = new System.Drawing.Size(110, 25);
            this.textBox326.TabIndex = 270;
            this.textBox326.Text = "数量";
            this.textBox326.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox327
            // 
            this.textBox327.BackColor = System.Drawing.SystemColors.Window;
            this.textBox327.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox327.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox327.Location = new System.Drawing.Point(15, 136);
            this.textBox327.Name = "textBox327";
            this.textBox327.Size = new System.Drawing.Size(30, 25);
            this.textBox327.TabIndex = 261;
            this.textBox327.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox328
            // 
            this.textBox328.BackColor = System.Drawing.SystemColors.Window;
            this.textBox328.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox328.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox328.Location = new System.Drawing.Point(44, 208);
            this.textBox328.Name = "textBox328";
            this.textBox328.Size = new System.Drawing.Size(250, 25);
            this.textBox328.TabIndex = 268;
            this.textBox328.Text = "数量可靠性";
            this.textBox328.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox329
            // 
            this.textBox329.BackColor = System.Drawing.SystemColors.Window;
            this.textBox329.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox329.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox329.Location = new System.Drawing.Point(15, 184);
            this.textBox329.Name = "textBox329";
            this.textBox329.Size = new System.Drawing.Size(30, 25);
            this.textBox329.TabIndex = 265;
            this.textBox329.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox330
            // 
            this.textBox330.BackColor = System.Drawing.SystemColors.Window;
            this.textBox330.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox330.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox330.Location = new System.Drawing.Point(44, 136);
            this.textBox330.Name = "textBox330";
            this.textBox330.Size = new System.Drawing.Size(250, 25);
            this.textBox330.TabIndex = 262;
            this.textBox330.Text = "设备耐久性";
            this.textBox330.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox331
            // 
            this.textBox331.BackColor = System.Drawing.SystemColors.Window;
            this.textBox331.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox331.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox331.Location = new System.Drawing.Point(15, 208);
            this.textBox331.Name = "textBox331";
            this.textBox331.Size = new System.Drawing.Size(30, 25);
            this.textBox331.TabIndex = 267;
            this.textBox331.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox332
            // 
            this.textBox332.BackColor = System.Drawing.SystemColors.Window;
            this.textBox332.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox332.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox332.Location = new System.Drawing.Point(44, 184);
            this.textBox332.Name = "textBox332";
            this.textBox332.Size = new System.Drawing.Size(250, 25);
            this.textBox332.TabIndex = 266;
            this.textBox332.Text = "交货日期";
            this.textBox332.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox333
            // 
            this.textBox333.BackColor = System.Drawing.SystemColors.Window;
            this.textBox333.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox333.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox333.Location = new System.Drawing.Point(15, 160);
            this.textBox333.Name = "textBox333";
            this.textBox333.Size = new System.Drawing.Size(30, 25);
            this.textBox333.TabIndex = 263;
            this.textBox333.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox334
            // 
            this.textBox334.BackColor = System.Drawing.SystemColors.Window;
            this.textBox334.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox334.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox334.Location = new System.Drawing.Point(44, 160);
            this.textBox334.Name = "textBox334";
            this.textBox334.Size = new System.Drawing.Size(250, 25);
            this.textBox334.TabIndex = 264;
            this.textBox334.Text = "批次合格率";
            this.textBox334.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox40.Location = new System.Drawing.Point(23, 45);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(15, 14);
            this.checkBox40.TabIndex = 260;
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox41.Location = new System.Drawing.Point(23, 94);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(15, 14);
            this.checkBox41.TabIndex = 259;
            this.checkBox41.UseVisualStyleBackColor = true;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox42.Location = new System.Drawing.Point(23, 118);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(15, 14);
            this.checkBox42.TabIndex = 258;
            this.checkBox42.UseVisualStyleBackColor = true;
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox43.Location = new System.Drawing.Point(23, 70);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(15, 14);
            this.checkBox43.TabIndex = 257;
            this.checkBox43.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.Window;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label51.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label51.Location = new System.Drawing.Point(501, 16);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(200, 25);
            this.label51.TabIndex = 251;
            this.label51.Text = "最低标准";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox339
            // 
            this.textBox339.BackColor = System.Drawing.SystemColors.Window;
            this.textBox339.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox339.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox339.Location = new System.Drawing.Point(501, 40);
            this.textBox339.Name = "textBox339";
            this.textBox339.Size = new System.Drawing.Size(200, 25);
            this.textBox339.TabIndex = 247;
            this.textBox339.Text = "提供";
            this.textBox339.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox340
            // 
            this.textBox340.BackColor = System.Drawing.SystemColors.Window;
            this.textBox340.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox340.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox340.Location = new System.Drawing.Point(501, 88);
            this.textBox340.Name = "textBox340";
            this.textBox340.Size = new System.Drawing.Size(200, 25);
            this.textBox340.TabIndex = 249;
            this.textBox340.Text = "6个月";
            this.textBox340.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox341
            // 
            this.textBox341.BackColor = System.Drawing.SystemColors.Window;
            this.textBox341.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox341.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox341.Location = new System.Drawing.Point(501, 112);
            this.textBox341.Name = "textBox341";
            this.textBox341.Size = new System.Drawing.Size(200, 25);
            this.textBox341.TabIndex = 250;
            this.textBox341.Text = "最高2%";
            this.textBox341.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox342
            // 
            this.textBox342.BackColor = System.Drawing.SystemColors.Window;
            this.textBox342.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox342.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox342.Location = new System.Drawing.Point(501, 64);
            this.textBox342.Name = "textBox342";
            this.textBox342.Size = new System.Drawing.Size(200, 25);
            this.textBox342.TabIndex = 248;
            this.textBox342.Text = "提供";
            this.textBox342.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.SystemColors.Window;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label52.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label52.Location = new System.Drawing.Point(392, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(110, 25);
            this.label52.TabIndex = 246;
            this.label52.Text = "类型";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox343
            // 
            this.textBox343.BackColor = System.Drawing.SystemColors.Window;
            this.textBox343.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox343.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox343.Location = new System.Drawing.Point(392, 40);
            this.textBox343.Name = "textBox343";
            this.textBox343.Size = new System.Drawing.Size(110, 25);
            this.textBox343.TabIndex = 242;
            this.textBox343.Text = "数量";
            this.textBox343.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox344
            // 
            this.textBox344.BackColor = System.Drawing.SystemColors.Window;
            this.textBox344.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox344.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox344.Location = new System.Drawing.Point(392, 88);
            this.textBox344.Name = "textBox344";
            this.textBox344.Size = new System.Drawing.Size(110, 25);
            this.textBox344.TabIndex = 244;
            this.textBox344.Text = "数量";
            this.textBox344.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox345
            // 
            this.textBox345.BackColor = System.Drawing.SystemColors.Window;
            this.textBox345.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox345.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox345.Location = new System.Drawing.Point(392, 112);
            this.textBox345.Name = "textBox345";
            this.textBox345.Size = new System.Drawing.Size(110, 25);
            this.textBox345.TabIndex = 245;
            this.textBox345.Text = "数量";
            this.textBox345.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox346
            // 
            this.textBox346.BackColor = System.Drawing.SystemColors.Window;
            this.textBox346.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox346.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox346.Location = new System.Drawing.Point(392, 64);
            this.textBox346.Name = "textBox346";
            this.textBox346.Size = new System.Drawing.Size(110, 25);
            this.textBox346.TabIndex = 243;
            this.textBox346.Text = "数量";
            this.textBox346.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox347
            // 
            this.textBox347.BackColor = System.Drawing.SystemColors.Window;
            this.textBox347.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox347.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox347.Location = new System.Drawing.Point(15, 40);
            this.textBox347.Name = "textBox347";
            this.textBox347.Size = new System.Drawing.Size(30, 25);
            this.textBox347.TabIndex = 232;
            this.textBox347.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.Window;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.Location = new System.Drawing.Point(15, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(30, 25);
            this.label53.TabIndex = 240;
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label102
            // 
            this.label102.BackColor = System.Drawing.SystemColors.Window;
            this.label102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label102.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label102.Location = new System.Drawing.Point(44, 16);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(250, 25);
            this.label102.TabIndex = 241;
            this.label102.Text = "问题描述";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox348
            // 
            this.textBox348.BackColor = System.Drawing.SystemColors.Window;
            this.textBox348.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox348.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox348.Location = new System.Drawing.Point(44, 112);
            this.textBox348.Name = "textBox348";
            this.textBox348.Size = new System.Drawing.Size(250, 25);
            this.textBox348.TabIndex = 239;
            this.textBox348.Text = "低停工检修率";
            this.textBox348.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox349
            // 
            this.textBox349.BackColor = System.Drawing.SystemColors.Window;
            this.textBox349.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox349.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox349.Location = new System.Drawing.Point(15, 88);
            this.textBox349.Name = "textBox349";
            this.textBox349.Size = new System.Drawing.Size(30, 25);
            this.textBox349.TabIndex = 236;
            this.textBox349.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox350
            // 
            this.textBox350.BackColor = System.Drawing.SystemColors.Window;
            this.textBox350.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox350.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox350.Location = new System.Drawing.Point(44, 40);
            this.textBox350.Name = "textBox350";
            this.textBox350.Size = new System.Drawing.Size(250, 25);
            this.textBox350.TabIndex = 233;
            this.textBox350.Text = "当前价格";
            this.textBox350.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox351
            // 
            this.textBox351.BackColor = System.Drawing.SystemColors.Window;
            this.textBox351.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox351.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox351.Location = new System.Drawing.Point(15, 112);
            this.textBox351.Name = "textBox351";
            this.textBox351.Size = new System.Drawing.Size(30, 25);
            this.textBox351.TabIndex = 238;
            this.textBox351.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox352
            // 
            this.textBox352.BackColor = System.Drawing.SystemColors.Window;
            this.textBox352.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox352.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox352.Location = new System.Drawing.Point(44, 88);
            this.textBox352.Name = "textBox352";
            this.textBox352.Size = new System.Drawing.Size(250, 25);
            this.textBox352.TabIndex = 237;
            this.textBox352.Text = "无故障时间";
            this.textBox352.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox353
            // 
            this.textBox353.BackColor = System.Drawing.SystemColors.Window;
            this.textBox353.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox353.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox353.Location = new System.Drawing.Point(15, 64);
            this.textBox353.Name = "textBox353";
            this.textBox353.Size = new System.Drawing.Size(30, 25);
            this.textBox353.TabIndex = 234;
            this.textBox353.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox354
            // 
            this.textBox354.BackColor = System.Drawing.SystemColors.Window;
            this.textBox354.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox354.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox354.Location = new System.Drawing.Point(44, 64);
            this.textBox354.Name = "textBox354";
            this.textBox354.Size = new System.Drawing.Size(250, 25);
            this.textBox354.TabIndex = 235;
            this.textBox354.Text = "历史价格";
            this.textBox354.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sjbg_panel
            // 
            this.sjbg_panel.Controls.Add(this.textBox243);
            this.sjbg_panel.Controls.Add(this.textBox244);
            this.sjbg_panel.Controls.Add(this.textBox245);
            this.sjbg_panel.Controls.Add(this.checkBox26);
            this.sjbg_panel.Controls.Add(this.checkBox27);
            this.sjbg_panel.Controls.Add(this.checkBox28);
            this.sjbg_panel.Controls.Add(this.textBox246);
            this.sjbg_panel.Controls.Add(this.textBox247);
            this.sjbg_panel.Controls.Add(this.textBox248);
            this.sjbg_panel.Controls.Add(this.textBox249);
            this.sjbg_panel.Controls.Add(this.textBox250);
            this.sjbg_panel.Controls.Add(this.textBox251);
            this.sjbg_panel.Controls.Add(this.textBox252);
            this.sjbg_panel.Controls.Add(this.textBox253);
            this.sjbg_panel.Controls.Add(this.textBox254);
            this.sjbg_panel.Controls.Add(this.textBox255);
            this.sjbg_panel.Controls.Add(this.textBox256);
            this.sjbg_panel.Controls.Add(this.textBox257);
            this.sjbg_panel.Controls.Add(this.textBox258);
            this.sjbg_panel.Controls.Add(this.textBox259);
            this.sjbg_panel.Controls.Add(this.textBox260);
            this.sjbg_panel.Controls.Add(this.textBox227);
            this.sjbg_panel.Controls.Add(this.textBox228);
            this.sjbg_panel.Controls.Add(this.textBox229);
            this.sjbg_panel.Controls.Add(this.textBox230);
            this.sjbg_panel.Controls.Add(this.textBox231);
            this.sjbg_panel.Controls.Add(this.textBox232);
            this.sjbg_panel.Controls.Add(this.textBox233);
            this.sjbg_panel.Controls.Add(this.textBox234);
            this.sjbg_panel.Controls.Add(this.textBox235);
            this.sjbg_panel.Controls.Add(this.textBox236);
            this.sjbg_panel.Controls.Add(this.textBox237);
            this.sjbg_panel.Controls.Add(this.textBox238);
            this.sjbg_panel.Controls.Add(this.label101);
            this.sjbg_panel.Controls.Add(this.textBox239);
            this.sjbg_panel.Controls.Add(this.textBox240);
            this.sjbg_panel.Controls.Add(this.textBox241);
            this.sjbg_panel.Controls.Add(this.textBox242);
            this.sjbg_panel.Controls.Add(this.checkBox18);
            this.sjbg_panel.Controls.Add(this.checkBox19);
            this.sjbg_panel.Controls.Add(this.checkBox20);
            this.sjbg_panel.Controls.Add(this.checkBox21);
            this.sjbg_panel.Controls.Add(this.textBox187);
            this.sjbg_panel.Controls.Add(this.textBox188);
            this.sjbg_panel.Controls.Add(this.textBox189);
            this.sjbg_panel.Controls.Add(this.textBox190);
            this.sjbg_panel.Controls.Add(this.textBox191);
            this.sjbg_panel.Controls.Add(this.textBox192);
            this.sjbg_panel.Controls.Add(this.textBox193);
            this.sjbg_panel.Controls.Add(this.textBox194);
            this.sjbg_panel.Controls.Add(this.textBox195);
            this.sjbg_panel.Controls.Add(this.textBox196);
            this.sjbg_panel.Controls.Add(this.textBox197);
            this.sjbg_panel.Controls.Add(this.textBox198);
            this.sjbg_panel.Controls.Add(this.textBox199);
            this.sjbg_panel.Controls.Add(this.textBox200);
            this.sjbg_panel.Controls.Add(this.textBox201);
            this.sjbg_panel.Controls.Add(this.textBox202);
            this.sjbg_panel.Controls.Add(this.textBox203);
            this.sjbg_panel.Controls.Add(this.textBox204);
            this.sjbg_panel.Controls.Add(this.textBox205);
            this.sjbg_panel.Controls.Add(this.textBox206);
            this.sjbg_panel.Controls.Add(this.checkBox22);
            this.sjbg_panel.Controls.Add(this.checkBox23);
            this.sjbg_panel.Controls.Add(this.checkBox24);
            this.sjbg_panel.Controls.Add(this.checkBox25);
            this.sjbg_panel.Controls.Add(this.textBox207);
            this.sjbg_panel.Controls.Add(this.textBox208);
            this.sjbg_panel.Controls.Add(this.textBox209);
            this.sjbg_panel.Controls.Add(this.textBox210);
            this.sjbg_panel.Controls.Add(this.textBox211);
            this.sjbg_panel.Controls.Add(this.textBox212);
            this.sjbg_panel.Controls.Add(this.textBox213);
            this.sjbg_panel.Controls.Add(this.textBox214);
            this.sjbg_panel.Controls.Add(this.textBox215);
            this.sjbg_panel.Controls.Add(this.textBox216);
            this.sjbg_panel.Controls.Add(this.textBox217);
            this.sjbg_panel.Controls.Add(this.textBox218);
            this.sjbg_panel.Controls.Add(this.textBox219);
            this.sjbg_panel.Controls.Add(this.textBox220);
            this.sjbg_panel.Controls.Add(this.textBox221);
            this.sjbg_panel.Controls.Add(this.textBox222);
            this.sjbg_panel.Controls.Add(this.textBox223);
            this.sjbg_panel.Controls.Add(this.textBox224);
            this.sjbg_panel.Controls.Add(this.textBox225);
            this.sjbg_panel.Controls.Add(this.textBox226);
            this.sjbg_panel.Controls.Add(this.checkBox1);
            this.sjbg_panel.Controls.Add(this.checkBox15);
            this.sjbg_panel.Controls.Add(this.checkBox16);
            this.sjbg_panel.Controls.Add(this.checkBox17);
            this.sjbg_panel.Controls.Add(this.textBox167);
            this.sjbg_panel.Controls.Add(this.textBox168);
            this.sjbg_panel.Controls.Add(this.textBox169);
            this.sjbg_panel.Controls.Add(this.textBox170);
            this.sjbg_panel.Controls.Add(this.textBox171);
            this.sjbg_panel.Controls.Add(this.textBox172);
            this.sjbg_panel.Controls.Add(this.textBox173);
            this.sjbg_panel.Controls.Add(this.textBox174);
            this.sjbg_panel.Controls.Add(this.textBox175);
            this.sjbg_panel.Controls.Add(this.textBox176);
            this.sjbg_panel.Controls.Add(this.textBox177);
            this.sjbg_panel.Controls.Add(this.textBox178);
            this.sjbg_panel.Controls.Add(this.textBox179);
            this.sjbg_panel.Controls.Add(this.textBox180);
            this.sjbg_panel.Controls.Add(this.textBox181);
            this.sjbg_panel.Controls.Add(this.textBox182);
            this.sjbg_panel.Controls.Add(this.textBox183);
            this.sjbg_panel.Controls.Add(this.textBox184);
            this.sjbg_panel.Controls.Add(this.textBox185);
            this.sjbg_panel.Controls.Add(this.textBox186);
            this.sjbg_panel.Controls.Add(this.checkBox2);
            this.sjbg_panel.Controls.Add(this.checkBox5);
            this.sjbg_panel.Controls.Add(this.checkBox4);
            this.sjbg_panel.Controls.Add(this.checkBox3);
            this.sjbg_panel.Controls.Add(this.label35);
            this.sjbg_panel.Controls.Add(this.textBox20);
            this.sjbg_panel.Controls.Add(this.textBox21);
            this.sjbg_panel.Controls.Add(this.textBox23);
            this.sjbg_panel.Controls.Add(this.textBox24);
            this.sjbg_panel.Controls.Add(this.label32);
            this.sjbg_panel.Controls.Add(this.textBox14);
            this.sjbg_panel.Controls.Add(this.textBox15);
            this.sjbg_panel.Controls.Add(this.textBox17);
            this.sjbg_panel.Controls.Add(this.textBox18);
            this.sjbg_panel.Controls.Add(this.label29);
            this.sjbg_panel.Controls.Add(this.textBox2);
            this.sjbg_panel.Controls.Add(this.textBox3);
            this.sjbg_panel.Controls.Add(this.textBox5);
            this.sjbg_panel.Controls.Add(this.textBox6);
            this.sjbg_panel.Controls.Add(this.qlab_zd);
            this.sjbg_panel.Controls.Add(this.shp1_lbl);
            this.sjbg_panel.Controls.Add(this.shp3_lbl);
            this.sjbg_panel.Controls.Add(this.hlca_zd);
            this.sjbg_panel.Controls.Add(this.qlab_zx);
            this.sjbg_panel.Controls.Add(this.qlca_zd);
            this.sjbg_panel.Controls.Add(this.hlab_zd);
            this.sjbg_panel.Controls.Add(this.qlca_zx);
            this.sjbg_panel.Controls.Add(this.qlab_ed);
            this.sjbg_panel.Controls.Add(this.qlca_ed);
            this.sjbg_panel.Location = new System.Drawing.Point(6, 51);
            this.sjbg_panel.Name = "sjbg_panel";
            this.sjbg_panel.Size = new System.Drawing.Size(718, 507);
            this.sjbg_panel.TabIndex = 218;
            // 
            // textBox243
            // 
            this.textBox243.BackColor = System.Drawing.SystemColors.Window;
            this.textBox243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox243.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox243.Location = new System.Drawing.Point(293, 448);
            this.textBox243.Name = "textBox243";
            this.textBox243.Size = new System.Drawing.Size(100, 25);
            this.textBox243.TabIndex = 230;
            this.textBox243.Text = "经营能力";
            this.textBox243.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox244
            // 
            this.textBox244.BackColor = System.Drawing.SystemColors.Window;
            this.textBox244.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox244.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox244.Location = new System.Drawing.Point(293, 472);
            this.textBox244.Name = "textBox244";
            this.textBox244.Size = new System.Drawing.Size(100, 25);
            this.textBox244.TabIndex = 231;
            this.textBox244.Text = "经营能力";
            this.textBox244.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox245
            // 
            this.textBox245.BackColor = System.Drawing.SystemColors.Window;
            this.textBox245.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox245.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox245.Location = new System.Drawing.Point(293, 424);
            this.textBox245.Name = "textBox245";
            this.textBox245.Size = new System.Drawing.Size(100, 25);
            this.textBox245.TabIndex = 229;
            this.textBox245.Text = "经营能力";
            this.textBox245.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox26.Location = new System.Drawing.Point(23, 454);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(15, 14);
            this.checkBox26.TabIndex = 228;
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox27.Location = new System.Drawing.Point(23, 478);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(15, 14);
            this.checkBox27.TabIndex = 227;
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox28.Location = new System.Drawing.Point(23, 430);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(15, 14);
            this.checkBox28.TabIndex = 226;
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // textBox246
            // 
            this.textBox246.BackColor = System.Drawing.SystemColors.Window;
            this.textBox246.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox246.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox246.Location = new System.Drawing.Point(600, 448);
            this.textBox246.Name = "textBox246";
            this.textBox246.Size = new System.Drawing.Size(100, 25);
            this.textBox246.TabIndex = 224;
            this.textBox246.Text = "手工评估";
            this.textBox246.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox247
            // 
            this.textBox247.BackColor = System.Drawing.SystemColors.Window;
            this.textBox247.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox247.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox247.Location = new System.Drawing.Point(600, 472);
            this.textBox247.Name = "textBox247";
            this.textBox247.Size = new System.Drawing.Size(100, 25);
            this.textBox247.TabIndex = 225;
            this.textBox247.Text = "手工评估";
            this.textBox247.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox248
            // 
            this.textBox248.BackColor = System.Drawing.SystemColors.Window;
            this.textBox248.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox248.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox248.Location = new System.Drawing.Point(600, 424);
            this.textBox248.Name = "textBox248";
            this.textBox248.Size = new System.Drawing.Size(100, 25);
            this.textBox248.TabIndex = 223;
            this.textBox248.Text = "手工评估";
            this.textBox248.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox249
            // 
            this.textBox249.BackColor = System.Drawing.SystemColors.Window;
            this.textBox249.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox249.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox249.Location = new System.Drawing.Point(501, 448);
            this.textBox249.Name = "textBox249";
            this.textBox249.Size = new System.Drawing.Size(100, 25);
            this.textBox249.TabIndex = 221;
            this.textBox249.Text = "1.00";
            this.textBox249.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox250
            // 
            this.textBox250.BackColor = System.Drawing.SystemColors.Window;
            this.textBox250.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox250.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox250.Location = new System.Drawing.Point(501, 472);
            this.textBox250.Name = "textBox250";
            this.textBox250.Size = new System.Drawing.Size(100, 25);
            this.textBox250.TabIndex = 222;
            this.textBox250.Text = "1.00";
            this.textBox250.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox251
            // 
            this.textBox251.BackColor = System.Drawing.SystemColors.Window;
            this.textBox251.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox251.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox251.Location = new System.Drawing.Point(501, 424);
            this.textBox251.Name = "textBox251";
            this.textBox251.Size = new System.Drawing.Size(100, 25);
            this.textBox251.TabIndex = 220;
            this.textBox251.Text = "1.00";
            this.textBox251.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox252
            // 
            this.textBox252.BackColor = System.Drawing.SystemColors.Window;
            this.textBox252.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox252.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox252.Location = new System.Drawing.Point(392, 448);
            this.textBox252.Name = "textBox252";
            this.textBox252.Size = new System.Drawing.Size(110, 25);
            this.textBox252.TabIndex = 218;
            this.textBox252.Text = "文本";
            this.textBox252.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox253
            // 
            this.textBox253.BackColor = System.Drawing.SystemColors.Window;
            this.textBox253.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox253.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox253.Location = new System.Drawing.Point(392, 472);
            this.textBox253.Name = "textBox253";
            this.textBox253.Size = new System.Drawing.Size(110, 25);
            this.textBox253.TabIndex = 219;
            this.textBox253.Text = "文本";
            this.textBox253.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox254
            // 
            this.textBox254.BackColor = System.Drawing.SystemColors.Window;
            this.textBox254.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox254.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox254.Location = new System.Drawing.Point(392, 424);
            this.textBox254.Name = "textBox254";
            this.textBox254.Size = new System.Drawing.Size(110, 25);
            this.textBox254.TabIndex = 217;
            this.textBox254.Text = "文本";
            this.textBox254.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox255
            // 
            this.textBox255.BackColor = System.Drawing.SystemColors.Window;
            this.textBox255.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox255.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox255.Location = new System.Drawing.Point(44, 472);
            this.textBox255.Name = "textBox255";
            this.textBox255.Size = new System.Drawing.Size(250, 25);
            this.textBox255.TabIndex = 216;
            this.textBox255.Text = "反应措施";
            this.textBox255.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox256
            // 
            this.textBox256.BackColor = System.Drawing.SystemColors.Window;
            this.textBox256.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox256.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox256.Location = new System.Drawing.Point(15, 448);
            this.textBox256.Name = "textBox256";
            this.textBox256.Size = new System.Drawing.Size(30, 25);
            this.textBox256.TabIndex = 213;
            this.textBox256.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox257
            // 
            this.textBox257.BackColor = System.Drawing.SystemColors.Window;
            this.textBox257.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox257.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox257.Location = new System.Drawing.Point(15, 472);
            this.textBox257.Name = "textBox257";
            this.textBox257.Size = new System.Drawing.Size(30, 25);
            this.textBox257.TabIndex = 215;
            this.textBox257.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox258
            // 
            this.textBox258.BackColor = System.Drawing.SystemColors.Window;
            this.textBox258.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox258.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox258.Location = new System.Drawing.Point(44, 448);
            this.textBox258.Name = "textBox258";
            this.textBox258.Size = new System.Drawing.Size(250, 25);
            this.textBox258.TabIndex = 214;
            this.textBox258.Text = "财务结构";
            this.textBox258.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox259
            // 
            this.textBox259.BackColor = System.Drawing.SystemColors.Window;
            this.textBox259.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox259.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox259.Location = new System.Drawing.Point(15, 424);
            this.textBox259.Name = "textBox259";
            this.textBox259.Size = new System.Drawing.Size(30, 25);
            this.textBox259.TabIndex = 211;
            this.textBox259.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox260
            // 
            this.textBox260.BackColor = System.Drawing.SystemColors.Window;
            this.textBox260.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox260.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox260.Location = new System.Drawing.Point(44, 424);
            this.textBox260.Name = "textBox260";
            this.textBox260.Size = new System.Drawing.Size(250, 25);
            this.textBox260.TabIndex = 212;
            this.textBox260.Text = "营业状况";
            this.textBox260.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox227
            // 
            this.textBox227.BackColor = System.Drawing.SystemColors.Window;
            this.textBox227.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox227.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox227.Location = new System.Drawing.Point(293, 328);
            this.textBox227.Name = "textBox227";
            this.textBox227.Size = new System.Drawing.Size(100, 25);
            this.textBox227.TabIndex = 207;
            this.textBox227.Text = "服务";
            this.textBox227.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox228
            // 
            this.textBox228.BackColor = System.Drawing.SystemColors.Window;
            this.textBox228.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox228.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox228.Location = new System.Drawing.Point(293, 376);
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new System.Drawing.Size(100, 25);
            this.textBox228.TabIndex = 209;
            this.textBox228.Text = "技术能力";
            this.textBox228.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox229
            // 
            this.textBox229.BackColor = System.Drawing.SystemColors.Window;
            this.textBox229.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox229.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox229.Location = new System.Drawing.Point(293, 400);
            this.textBox229.Name = "textBox229";
            this.textBox229.Size = new System.Drawing.Size(100, 25);
            this.textBox229.TabIndex = 210;
            this.textBox229.Text = "技术能力";
            this.textBox229.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox230
            // 
            this.textBox230.BackColor = System.Drawing.SystemColors.Window;
            this.textBox230.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox230.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox230.Location = new System.Drawing.Point(293, 352);
            this.textBox230.Name = "textBox230";
            this.textBox230.Size = new System.Drawing.Size(100, 25);
            this.textBox230.TabIndex = 208;
            this.textBox230.Text = "技术能力";
            this.textBox230.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox231
            // 
            this.textBox231.BackColor = System.Drawing.SystemColors.Window;
            this.textBox231.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox231.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox231.Location = new System.Drawing.Point(293, 232);
            this.textBox231.Name = "textBox231";
            this.textBox231.Size = new System.Drawing.Size(100, 25);
            this.textBox231.TabIndex = 203;
            this.textBox231.Text = "交货";
            this.textBox231.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox232
            // 
            this.textBox232.BackColor = System.Drawing.SystemColors.Window;
            this.textBox232.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox232.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox232.Location = new System.Drawing.Point(293, 280);
            this.textBox232.Name = "textBox232";
            this.textBox232.Size = new System.Drawing.Size(100, 25);
            this.textBox232.TabIndex = 205;
            this.textBox232.Text = "服务";
            this.textBox232.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox233
            // 
            this.textBox233.BackColor = System.Drawing.SystemColors.Window;
            this.textBox233.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox233.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox233.Location = new System.Drawing.Point(293, 304);
            this.textBox233.Name = "textBox233";
            this.textBox233.Size = new System.Drawing.Size(100, 25);
            this.textBox233.TabIndex = 206;
            this.textBox233.Text = "服务";
            this.textBox233.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox234
            // 
            this.textBox234.BackColor = System.Drawing.SystemColors.Window;
            this.textBox234.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox234.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox234.Location = new System.Drawing.Point(293, 256);
            this.textBox234.Name = "textBox234";
            this.textBox234.Size = new System.Drawing.Size(100, 25);
            this.textBox234.TabIndex = 204;
            this.textBox234.Text = "服务";
            this.textBox234.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox235
            // 
            this.textBox235.BackColor = System.Drawing.SystemColors.Window;
            this.textBox235.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox235.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox235.Location = new System.Drawing.Point(293, 136);
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new System.Drawing.Size(100, 25);
            this.textBox235.TabIndex = 199;
            this.textBox235.Text = "质量";
            this.textBox235.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox236
            // 
            this.textBox236.BackColor = System.Drawing.SystemColors.Window;
            this.textBox236.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox236.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox236.Location = new System.Drawing.Point(293, 184);
            this.textBox236.Name = "textBox236";
            this.textBox236.Size = new System.Drawing.Size(100, 25);
            this.textBox236.TabIndex = 201;
            this.textBox236.Text = "交货";
            this.textBox236.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox237
            // 
            this.textBox237.BackColor = System.Drawing.SystemColors.Window;
            this.textBox237.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox237.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox237.Location = new System.Drawing.Point(293, 208);
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new System.Drawing.Size(100, 25);
            this.textBox237.TabIndex = 202;
            this.textBox237.Text = "交货";
            this.textBox237.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox238
            // 
            this.textBox238.BackColor = System.Drawing.SystemColors.Window;
            this.textBox238.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox238.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox238.Location = new System.Drawing.Point(293, 160);
            this.textBox238.Name = "textBox238";
            this.textBox238.Size = new System.Drawing.Size(100, 25);
            this.textBox238.TabIndex = 200;
            this.textBox238.Text = "质量";
            this.textBox238.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label101
            // 
            this.label101.BackColor = System.Drawing.SystemColors.Window;
            this.label101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label101.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label101.Location = new System.Drawing.Point(293, 16);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(100, 25);
            this.label101.TabIndex = 198;
            this.label101.Text = "问题类别";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox239
            // 
            this.textBox239.BackColor = System.Drawing.SystemColors.Window;
            this.textBox239.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox239.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox239.Location = new System.Drawing.Point(293, 40);
            this.textBox239.Name = "textBox239";
            this.textBox239.Size = new System.Drawing.Size(100, 25);
            this.textBox239.TabIndex = 194;
            this.textBox239.Text = "价格";
            this.textBox239.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox240
            // 
            this.textBox240.BackColor = System.Drawing.SystemColors.Window;
            this.textBox240.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox240.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox240.Location = new System.Drawing.Point(293, 88);
            this.textBox240.Name = "textBox240";
            this.textBox240.Size = new System.Drawing.Size(100, 25);
            this.textBox240.TabIndex = 196;
            this.textBox240.Text = "质量";
            this.textBox240.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox241
            // 
            this.textBox241.BackColor = System.Drawing.SystemColors.Window;
            this.textBox241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox241.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox241.Location = new System.Drawing.Point(293, 112);
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new System.Drawing.Size(100, 25);
            this.textBox241.TabIndex = 197;
            this.textBox241.Text = "质量";
            this.textBox241.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox242
            // 
            this.textBox242.BackColor = System.Drawing.SystemColors.Window;
            this.textBox242.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox242.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox242.Location = new System.Drawing.Point(293, 64);
            this.textBox242.Name = "textBox242";
            this.textBox242.Size = new System.Drawing.Size(100, 25);
            this.textBox242.TabIndex = 195;
            this.textBox242.Text = "价格";
            this.textBox242.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox18.Location = new System.Drawing.Point(23, 333);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 193;
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox19.Location = new System.Drawing.Point(23, 382);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(15, 14);
            this.checkBox19.TabIndex = 192;
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox20.Location = new System.Drawing.Point(23, 406);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 191;
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox21.Location = new System.Drawing.Point(23, 358);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(15, 14);
            this.checkBox21.TabIndex = 190;
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // textBox187
            // 
            this.textBox187.BackColor = System.Drawing.SystemColors.Window;
            this.textBox187.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox187.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox187.Location = new System.Drawing.Point(600, 328);
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new System.Drawing.Size(100, 25);
            this.textBox187.TabIndex = 186;
            this.textBox187.Text = "线性函数";
            this.textBox187.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox188
            // 
            this.textBox188.BackColor = System.Drawing.SystemColors.Window;
            this.textBox188.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox188.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox188.Location = new System.Drawing.Point(600, 376);
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new System.Drawing.Size(100, 25);
            this.textBox188.TabIndex = 188;
            this.textBox188.Text = "手工评估";
            this.textBox188.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox189
            // 
            this.textBox189.BackColor = System.Drawing.SystemColors.Window;
            this.textBox189.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox189.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox189.Location = new System.Drawing.Point(600, 400);
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new System.Drawing.Size(100, 25);
            this.textBox189.TabIndex = 189;
            this.textBox189.Text = "手工评估";
            this.textBox189.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox190
            // 
            this.textBox190.BackColor = System.Drawing.SystemColors.Window;
            this.textBox190.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox190.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox190.Location = new System.Drawing.Point(600, 352);
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new System.Drawing.Size(100, 25);
            this.textBox190.TabIndex = 187;
            this.textBox190.Text = "手工评估";
            this.textBox190.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox191
            // 
            this.textBox191.BackColor = System.Drawing.SystemColors.Window;
            this.textBox191.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox191.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox191.Location = new System.Drawing.Point(501, 328);
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new System.Drawing.Size(100, 25);
            this.textBox191.TabIndex = 182;
            this.textBox191.Text = "1.00";
            this.textBox191.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox192
            // 
            this.textBox192.BackColor = System.Drawing.SystemColors.Window;
            this.textBox192.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox192.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox192.Location = new System.Drawing.Point(501, 376);
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new System.Drawing.Size(100, 25);
            this.textBox192.TabIndex = 184;
            this.textBox192.Text = "1.00";
            this.textBox192.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox193
            // 
            this.textBox193.BackColor = System.Drawing.SystemColors.Window;
            this.textBox193.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox193.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox193.Location = new System.Drawing.Point(501, 400);
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new System.Drawing.Size(100, 25);
            this.textBox193.TabIndex = 185;
            this.textBox193.Text = "1.00";
            this.textBox193.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox194
            // 
            this.textBox194.BackColor = System.Drawing.SystemColors.Window;
            this.textBox194.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox194.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox194.Location = new System.Drawing.Point(501, 352);
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new System.Drawing.Size(100, 25);
            this.textBox194.TabIndex = 183;
            this.textBox194.Text = "1.00";
            this.textBox194.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox195
            // 
            this.textBox195.BackColor = System.Drawing.SystemColors.Window;
            this.textBox195.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox195.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox195.Location = new System.Drawing.Point(392, 328);
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new System.Drawing.Size(110, 25);
            this.textBox195.TabIndex = 178;
            this.textBox195.Text = "数量";
            this.textBox195.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox196
            // 
            this.textBox196.BackColor = System.Drawing.SystemColors.Window;
            this.textBox196.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox196.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox196.Location = new System.Drawing.Point(392, 376);
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new System.Drawing.Size(110, 25);
            this.textBox196.TabIndex = 180;
            this.textBox196.Text = "文本";
            this.textBox196.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox197
            // 
            this.textBox197.BackColor = System.Drawing.SystemColors.Window;
            this.textBox197.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox197.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox197.Location = new System.Drawing.Point(392, 400);
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new System.Drawing.Size(110, 25);
            this.textBox197.TabIndex = 181;
            this.textBox197.Text = "文本";
            this.textBox197.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox198
            // 
            this.textBox198.BackColor = System.Drawing.SystemColors.Window;
            this.textBox198.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox198.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox198.Location = new System.Drawing.Point(392, 352);
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new System.Drawing.Size(110, 25);
            this.textBox198.TabIndex = 179;
            this.textBox198.Text = "文本";
            this.textBox198.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox199
            // 
            this.textBox199.BackColor = System.Drawing.SystemColors.Window;
            this.textBox199.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox199.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox199.Location = new System.Drawing.Point(15, 328);
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new System.Drawing.Size(30, 25);
            this.textBox199.TabIndex = 170;
            this.textBox199.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox200
            // 
            this.textBox200.BackColor = System.Drawing.SystemColors.Window;
            this.textBox200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox200.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox200.Location = new System.Drawing.Point(44, 400);
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new System.Drawing.Size(250, 25);
            this.textBox200.TabIndex = 177;
            this.textBox200.Text = "工作技术";
            this.textBox200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox201
            // 
            this.textBox201.BackColor = System.Drawing.SystemColors.Window;
            this.textBox201.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox201.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox201.Location = new System.Drawing.Point(15, 376);
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new System.Drawing.Size(30, 25);
            this.textBox201.TabIndex = 174;
            this.textBox201.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox202
            // 
            this.textBox202.BackColor = System.Drawing.SystemColors.Window;
            this.textBox202.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox202.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox202.Location = new System.Drawing.Point(44, 328);
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new System.Drawing.Size(250, 25);
            this.textBox202.TabIndex = 171;
            this.textBox202.Text = "外包率";
            this.textBox202.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox203
            // 
            this.textBox203.BackColor = System.Drawing.SystemColors.Window;
            this.textBox203.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox203.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox203.Location = new System.Drawing.Point(15, 400);
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new System.Drawing.Size(30, 25);
            this.textBox203.TabIndex = 176;
            this.textBox203.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox204
            // 
            this.textBox204.BackColor = System.Drawing.SystemColors.Window;
            this.textBox204.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox204.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox204.Location = new System.Drawing.Point(44, 376);
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new System.Drawing.Size(250, 25);
            this.textBox204.TabIndex = 175;
            this.textBox204.Text = "检验设备";
            this.textBox204.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox205
            // 
            this.textBox205.BackColor = System.Drawing.SystemColors.Window;
            this.textBox205.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox205.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox205.Location = new System.Drawing.Point(15, 352);
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new System.Drawing.Size(30, 25);
            this.textBox205.TabIndex = 172;
            this.textBox205.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox206
            // 
            this.textBox206.BackColor = System.Drawing.SystemColors.Window;
            this.textBox206.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox206.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox206.Location = new System.Drawing.Point(44, 352);
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new System.Drawing.Size(250, 25);
            this.textBox206.TabIndex = 173;
            this.textBox206.Text = "机械设备";
            this.textBox206.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox22.Location = new System.Drawing.Point(23, 237);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(15, 14);
            this.checkBox22.TabIndex = 169;
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox23.Location = new System.Drawing.Point(23, 286);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(15, 14);
            this.checkBox23.TabIndex = 168;
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox24.Location = new System.Drawing.Point(23, 310);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(15, 14);
            this.checkBox24.TabIndex = 167;
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox25.Location = new System.Drawing.Point(23, 262);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(15, 14);
            this.checkBox25.TabIndex = 166;
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // textBox207
            // 
            this.textBox207.BackColor = System.Drawing.SystemColors.Window;
            this.textBox207.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox207.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox207.Location = new System.Drawing.Point(600, 232);
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new System.Drawing.Size(100, 25);
            this.textBox207.TabIndex = 162;
            this.textBox207.Text = "手工评估";
            this.textBox207.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox208
            // 
            this.textBox208.BackColor = System.Drawing.SystemColors.Window;
            this.textBox208.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox208.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox208.Location = new System.Drawing.Point(600, 280);
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new System.Drawing.Size(100, 25);
            this.textBox208.TabIndex = 164;
            this.textBox208.Text = "手工评估";
            this.textBox208.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox209
            // 
            this.textBox209.BackColor = System.Drawing.SystemColors.Window;
            this.textBox209.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox209.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox209.Location = new System.Drawing.Point(600, 304);
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new System.Drawing.Size(100, 25);
            this.textBox209.TabIndex = 165;
            this.textBox209.Text = "线性函数";
            this.textBox209.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox210
            // 
            this.textBox210.BackColor = System.Drawing.SystemColors.Window;
            this.textBox210.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox210.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox210.Location = new System.Drawing.Point(600, 256);
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new System.Drawing.Size(100, 25);
            this.textBox210.TabIndex = 163;
            this.textBox210.Text = "手工评估";
            this.textBox210.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox211
            // 
            this.textBox211.BackColor = System.Drawing.SystemColors.Window;
            this.textBox211.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox211.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox211.Location = new System.Drawing.Point(501, 232);
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new System.Drawing.Size(100, 25);
            this.textBox211.TabIndex = 158;
            this.textBox211.Text = "1.00";
            this.textBox211.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox212
            // 
            this.textBox212.BackColor = System.Drawing.SystemColors.Window;
            this.textBox212.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox212.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox212.Location = new System.Drawing.Point(501, 280);
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new System.Drawing.Size(100, 25);
            this.textBox212.TabIndex = 160;
            this.textBox212.Text = "1.00";
            this.textBox212.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox213
            // 
            this.textBox213.BackColor = System.Drawing.SystemColors.Window;
            this.textBox213.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox213.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox213.Location = new System.Drawing.Point(501, 304);
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new System.Drawing.Size(100, 25);
            this.textBox213.TabIndex = 161;
            this.textBox213.Text = "2.00";
            this.textBox213.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox214
            // 
            this.textBox214.BackColor = System.Drawing.SystemColors.Window;
            this.textBox214.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox214.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox214.Location = new System.Drawing.Point(501, 256);
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new System.Drawing.Size(100, 25);
            this.textBox214.TabIndex = 159;
            this.textBox214.Text = "1.00";
            this.textBox214.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox215
            // 
            this.textBox215.BackColor = System.Drawing.SystemColors.Window;
            this.textBox215.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox215.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox215.Location = new System.Drawing.Point(392, 232);
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new System.Drawing.Size(110, 25);
            this.textBox215.TabIndex = 154;
            this.textBox215.Text = "文本";
            this.textBox215.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox216
            // 
            this.textBox216.BackColor = System.Drawing.SystemColors.Window;
            this.textBox216.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox216.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox216.Location = new System.Drawing.Point(392, 280);
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new System.Drawing.Size(110, 25);
            this.textBox216.TabIndex = 156;
            this.textBox216.Text = "是/否字段";
            this.textBox216.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox217
            // 
            this.textBox217.BackColor = System.Drawing.SystemColors.Window;
            this.textBox217.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox217.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox217.Location = new System.Drawing.Point(392, 304);
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new System.Drawing.Size(110, 25);
            this.textBox217.TabIndex = 157;
            this.textBox217.Text = "数量";
            this.textBox217.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox218
            // 
            this.textBox218.BackColor = System.Drawing.SystemColors.Window;
            this.textBox218.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox218.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox218.Location = new System.Drawing.Point(392, 256);
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new System.Drawing.Size(110, 25);
            this.textBox218.TabIndex = 155;
            this.textBox218.Text = "是/否字段";
            this.textBox218.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox219
            // 
            this.textBox219.BackColor = System.Drawing.SystemColors.Window;
            this.textBox219.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox219.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox219.Location = new System.Drawing.Point(15, 232);
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new System.Drawing.Size(30, 25);
            this.textBox219.TabIndex = 146;
            this.textBox219.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox220
            // 
            this.textBox220.BackColor = System.Drawing.SystemColors.Window;
            this.textBox220.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox220.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox220.Location = new System.Drawing.Point(44, 304);
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new System.Drawing.Size(250, 25);
            this.textBox220.TabIndex = 153;
            this.textBox220.Text = "保养与修理的反应时间";
            this.textBox220.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox221
            // 
            this.textBox221.BackColor = System.Drawing.SystemColors.Window;
            this.textBox221.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox221.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox221.Location = new System.Drawing.Point(15, 280);
            this.textBox221.Name = "textBox221";
            this.textBox221.Size = new System.Drawing.Size(30, 25);
            this.textBox221.TabIndex = 150;
            this.textBox221.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox222
            // 
            this.textBox222.BackColor = System.Drawing.SystemColors.Window;
            this.textBox222.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox222.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox222.Location = new System.Drawing.Point(44, 232);
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new System.Drawing.Size(250, 25);
            this.textBox222.TabIndex = 147;
            this.textBox222.Text = "装运情况";
            this.textBox222.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox223
            // 
            this.textBox223.BackColor = System.Drawing.SystemColors.Window;
            this.textBox223.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox223.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox223.Location = new System.Drawing.Point(15, 304);
            this.textBox223.Name = "textBox223";
            this.textBox223.Size = new System.Drawing.Size(30, 25);
            this.textBox223.TabIndex = 152;
            this.textBox223.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox224
            // 
            this.textBox224.BackColor = System.Drawing.SystemColors.Window;
            this.textBox224.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox224.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox224.Location = new System.Drawing.Point(44, 280);
            this.textBox224.Name = "textBox224";
            this.textBox224.Size = new System.Drawing.Size(250, 25);
            this.textBox224.TabIndex = 151;
            this.textBox224.Text = "是否进行人员培训";
            this.textBox224.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox225
            // 
            this.textBox225.BackColor = System.Drawing.SystemColors.Window;
            this.textBox225.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox225.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox225.Location = new System.Drawing.Point(15, 256);
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new System.Drawing.Size(30, 25);
            this.textBox225.TabIndex = 148;
            this.textBox225.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox226
            // 
            this.textBox226.BackColor = System.Drawing.SystemColors.Window;
            this.textBox226.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox226.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox226.Location = new System.Drawing.Point(44, 256);
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new System.Drawing.Size(250, 25);
            this.textBox226.TabIndex = 149;
            this.textBox226.Text = "是否安装、调试等服务";
            this.textBox226.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox1.Location = new System.Drawing.Point(23, 141);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 145;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox15.Location = new System.Drawing.Point(23, 190);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 144;
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox16.Location = new System.Drawing.Point(23, 214);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 143;
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox17.Location = new System.Drawing.Point(23, 166);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 142;
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // textBox167
            // 
            this.textBox167.BackColor = System.Drawing.SystemColors.Window;
            this.textBox167.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox167.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox167.Location = new System.Drawing.Point(600, 136);
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new System.Drawing.Size(100, 25);
            this.textBox167.TabIndex = 138;
            this.textBox167.Text = "手工评估";
            this.textBox167.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox168
            // 
            this.textBox168.BackColor = System.Drawing.SystemColors.Window;
            this.textBox168.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox168.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox168.Location = new System.Drawing.Point(600, 184);
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new System.Drawing.Size(100, 25);
            this.textBox168.TabIndex = 140;
            this.textBox168.Text = "手工评估";
            this.textBox168.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox169
            // 
            this.textBox169.BackColor = System.Drawing.SystemColors.Window;
            this.textBox169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox169.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox169.Location = new System.Drawing.Point(600, 208);
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new System.Drawing.Size(100, 25);
            this.textBox169.TabIndex = 141;
            this.textBox169.Text = "手工评估";
            this.textBox169.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox170
            // 
            this.textBox170.BackColor = System.Drawing.SystemColors.Window;
            this.textBox170.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox170.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox170.Location = new System.Drawing.Point(600, 160);
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new System.Drawing.Size(100, 25);
            this.textBox170.TabIndex = 139;
            this.textBox170.Text = "线性函数";
            this.textBox170.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox171
            // 
            this.textBox171.BackColor = System.Drawing.SystemColors.Window;
            this.textBox171.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox171.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox171.Location = new System.Drawing.Point(501, 136);
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new System.Drawing.Size(100, 25);
            this.textBox171.TabIndex = 134;
            this.textBox171.Text = "2.00";
            this.textBox171.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox172
            // 
            this.textBox172.BackColor = System.Drawing.SystemColors.Window;
            this.textBox172.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox172.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox172.Location = new System.Drawing.Point(501, 184);
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new System.Drawing.Size(100, 25);
            this.textBox172.TabIndex = 136;
            this.textBox172.Text = "1.00";
            this.textBox172.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox173
            // 
            this.textBox173.BackColor = System.Drawing.SystemColors.Window;
            this.textBox173.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox173.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox173.Location = new System.Drawing.Point(501, 208);
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new System.Drawing.Size(100, 25);
            this.textBox173.TabIndex = 137;
            this.textBox173.Text = "1.00";
            this.textBox173.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox174
            // 
            this.textBox174.BackColor = System.Drawing.SystemColors.Window;
            this.textBox174.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox174.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox174.Location = new System.Drawing.Point(501, 160);
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new System.Drawing.Size(100, 25);
            this.textBox174.TabIndex = 135;
            this.textBox174.Text = "2.00";
            this.textBox174.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox175
            // 
            this.textBox175.BackColor = System.Drawing.SystemColors.Window;
            this.textBox175.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox175.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox175.Location = new System.Drawing.Point(392, 136);
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new System.Drawing.Size(110, 25);
            this.textBox175.TabIndex = 130;
            this.textBox175.Text = "文本";
            this.textBox175.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox176
            // 
            this.textBox176.BackColor = System.Drawing.SystemColors.Window;
            this.textBox176.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox176.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox176.Location = new System.Drawing.Point(392, 184);
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new System.Drawing.Size(110, 25);
            this.textBox176.TabIndex = 132;
            this.textBox176.Text = "时间";
            this.textBox176.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox177
            // 
            this.textBox177.BackColor = System.Drawing.SystemColors.Window;
            this.textBox177.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox177.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox177.Location = new System.Drawing.Point(392, 208);
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new System.Drawing.Size(110, 25);
            this.textBox177.TabIndex = 133;
            this.textBox177.Text = "文本";
            this.textBox177.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox178
            // 
            this.textBox178.BackColor = System.Drawing.SystemColors.Window;
            this.textBox178.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox178.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox178.Location = new System.Drawing.Point(392, 160);
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new System.Drawing.Size(110, 25);
            this.textBox178.TabIndex = 131;
            this.textBox178.Text = "数量";
            this.textBox178.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox179
            // 
            this.textBox179.BackColor = System.Drawing.SystemColors.Window;
            this.textBox179.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox179.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox179.Location = new System.Drawing.Point(15, 136);
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new System.Drawing.Size(30, 25);
            this.textBox179.TabIndex = 122;
            this.textBox179.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox180
            // 
            this.textBox180.BackColor = System.Drawing.SystemColors.Window;
            this.textBox180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox180.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox180.Location = new System.Drawing.Point(44, 208);
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new System.Drawing.Size(250, 25);
            this.textBox180.TabIndex = 129;
            this.textBox180.Text = "数量可靠性";
            this.textBox180.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox181
            // 
            this.textBox181.BackColor = System.Drawing.SystemColors.Window;
            this.textBox181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox181.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox181.Location = new System.Drawing.Point(15, 184);
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new System.Drawing.Size(30, 25);
            this.textBox181.TabIndex = 126;
            this.textBox181.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox182
            // 
            this.textBox182.BackColor = System.Drawing.SystemColors.Window;
            this.textBox182.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox182.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox182.Location = new System.Drawing.Point(44, 136);
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new System.Drawing.Size(250, 25);
            this.textBox182.TabIndex = 123;
            this.textBox182.Text = "设备耐久性";
            this.textBox182.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox183
            // 
            this.textBox183.BackColor = System.Drawing.SystemColors.Window;
            this.textBox183.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox183.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox183.Location = new System.Drawing.Point(15, 208);
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new System.Drawing.Size(30, 25);
            this.textBox183.TabIndex = 128;
            this.textBox183.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox184
            // 
            this.textBox184.BackColor = System.Drawing.SystemColors.Window;
            this.textBox184.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox184.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox184.Location = new System.Drawing.Point(44, 184);
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new System.Drawing.Size(250, 25);
            this.textBox184.TabIndex = 127;
            this.textBox184.Text = "交货日期";
            this.textBox184.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox185
            // 
            this.textBox185.BackColor = System.Drawing.SystemColors.Window;
            this.textBox185.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox185.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox185.Location = new System.Drawing.Point(15, 160);
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new System.Drawing.Size(30, 25);
            this.textBox185.TabIndex = 124;
            this.textBox185.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox186
            // 
            this.textBox186.BackColor = System.Drawing.SystemColors.Window;
            this.textBox186.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox186.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox186.Location = new System.Drawing.Point(44, 160);
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new System.Drawing.Size(250, 25);
            this.textBox186.TabIndex = 125;
            this.textBox186.Text = "批次合格率";
            this.textBox186.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox2.Location = new System.Drawing.Point(23, 45);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 121;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox5.Location = new System.Drawing.Point(23, 94);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 118;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox4.Location = new System.Drawing.Point(23, 118);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 117;
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox3.Location = new System.Drawing.Point(23, 70);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 116;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.SystemColors.Window;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label35.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(600, 16);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 25);
            this.label35.TabIndex = 114;
            this.label35.Text = "计分方式";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Window;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox20.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox20.Location = new System.Drawing.Point(600, 40);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 25);
            this.textBox20.TabIndex = 108;
            this.textBox20.Text = "线性函数";
            this.textBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox21.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox21.Location = new System.Drawing.Point(600, 88);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 25);
            this.textBox21.TabIndex = 110;
            this.textBox21.Text = "线性函数";
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Window;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox23.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox23.Location = new System.Drawing.Point(600, 112);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(100, 25);
            this.textBox23.TabIndex = 111;
            this.textBox23.Text = "线性函数";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox24.Location = new System.Drawing.Point(600, 64);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 25);
            this.textBox24.TabIndex = 109;
            this.textBox24.Text = "线性函数";
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.SystemColors.Window;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label32.Location = new System.Drawing.Point(501, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 25);
            this.label32.TabIndex = 107;
            this.label32.Text = "权重";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.SystemColors.Window;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox14.Location = new System.Drawing.Point(501, 40);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 25);
            this.textBox14.TabIndex = 101;
            this.textBox14.Text = "75.00";
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.Window;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox15.Location = new System.Drawing.Point(501, 88);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 25);
            this.textBox15.TabIndex = 103;
            this.textBox15.Text = "2.00";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.Window;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox17.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox17.Location = new System.Drawing.Point(501, 112);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 25);
            this.textBox17.TabIndex = 104;
            this.textBox17.Text = "2.00";
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.Window;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox18.Location = new System.Drawing.Point(501, 64);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 25);
            this.textBox18.TabIndex = 102;
            this.textBox18.Text = "3.00";
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.SystemColors.Window;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.Location = new System.Drawing.Point(392, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(110, 25);
            this.label29.TabIndex = 93;
            this.label29.Text = "类型";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox2.Location = new System.Drawing.Point(392, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(110, 25);
            this.textBox2.TabIndex = 87;
            this.textBox2.Text = "数量";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Window;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox3.Location = new System.Drawing.Point(392, 88);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(110, 25);
            this.textBox3.TabIndex = 89;
            this.textBox3.Text = "数量";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox5.Location = new System.Drawing.Point(392, 112);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(110, 25);
            this.textBox5.TabIndex = 90;
            this.textBox5.Text = "数量";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox6.Location = new System.Drawing.Point(392, 64);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(110, 25);
            this.textBox6.TabIndex = 88;
            this.textBox6.Text = "数量";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlab_zd
            // 
            this.qlab_zd.BackColor = System.Drawing.SystemColors.Window;
            this.qlab_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlab_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlab_zd.Location = new System.Drawing.Point(15, 40);
            this.qlab_zd.Name = "qlab_zd";
            this.qlab_zd.Size = new System.Drawing.Size(30, 25);
            this.qlab_zd.TabIndex = 0;
            this.qlab_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // shp1_lbl
            // 
            this.shp1_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.shp1_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shp1_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shp1_lbl.Location = new System.Drawing.Point(15, 16);
            this.shp1_lbl.Name = "shp1_lbl";
            this.shp1_lbl.Size = new System.Drawing.Size(30, 25);
            this.shp1_lbl.TabIndex = 86;
            this.shp1_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // shp3_lbl
            // 
            this.shp3_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.shp3_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shp3_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shp3_lbl.Location = new System.Drawing.Point(44, 16);
            this.shp3_lbl.Name = "shp3_lbl";
            this.shp3_lbl.Size = new System.Drawing.Size(250, 25);
            this.shp3_lbl.TabIndex = 86;
            this.shp3_lbl.Text = "问题描述";
            this.shp3_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hlca_zd
            // 
            this.hlca_zd.BackColor = System.Drawing.SystemColors.Window;
            this.hlca_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlca_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlca_zd.Location = new System.Drawing.Point(44, 112);
            this.hlca_zd.Name = "hlca_zd";
            this.hlca_zd.Size = new System.Drawing.Size(250, 25);
            this.hlca_zd.TabIndex = 14;
            this.hlca_zd.Text = "低停工检修率";
            this.hlca_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlab_zx
            // 
            this.qlab_zx.BackColor = System.Drawing.SystemColors.Window;
            this.qlab_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlab_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlab_zx.Location = new System.Drawing.Point(15, 88);
            this.qlab_zx.Name = "qlab_zx";
            this.qlab_zx.Size = new System.Drawing.Size(30, 25);
            this.qlab_zx.TabIndex = 8;
            this.qlab_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_zd
            // 
            this.qlca_zd.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_zd.Location = new System.Drawing.Point(44, 40);
            this.qlca_zd.Name = "qlca_zd";
            this.qlca_zd.Size = new System.Drawing.Size(250, 25);
            this.qlca_zd.TabIndex = 2;
            this.qlca_zd.Text = "当前价格";
            this.qlca_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlab_zd
            // 
            this.hlab_zd.BackColor = System.Drawing.SystemColors.Window;
            this.hlab_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlab_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlab_zd.Location = new System.Drawing.Point(15, 112);
            this.hlab_zd.Name = "hlab_zd";
            this.hlab_zd.Size = new System.Drawing.Size(30, 25);
            this.hlab_zd.TabIndex = 12;
            this.hlab_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_zx
            // 
            this.qlca_zx.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_zx.Location = new System.Drawing.Point(44, 88);
            this.qlca_zx.Name = "qlca_zx";
            this.qlca_zx.Size = new System.Drawing.Size(250, 25);
            this.qlca_zx.TabIndex = 10;
            this.qlca_zx.Text = "无故障时间";
            this.qlca_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlab_ed
            // 
            this.qlab_ed.BackColor = System.Drawing.SystemColors.Window;
            this.qlab_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlab_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlab_ed.Location = new System.Drawing.Point(15, 64);
            this.qlab_ed.Name = "qlab_ed";
            this.qlab_ed.Size = new System.Drawing.Size(30, 25);
            this.qlab_ed.TabIndex = 4;
            this.qlab_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_ed
            // 
            this.qlca_ed.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_ed.Location = new System.Drawing.Point(44, 64);
            this.qlca_ed.Name = "qlca_ed";
            this.qlca_ed.Size = new System.Drawing.Size(250, 25);
            this.qlca_ed.TabIndex = 6;
            this.qlca_ed.Text = "历史价格";
            this.qlca_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pgff_cmb
            // 
            this.pgff_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pgff_cmb.FormattingEnabled = true;
            this.pgff_cmb.ItemHeight = 14;
            this.pgff_cmb.Items.AddRange(new object[] {
            "加权平均法",
            "最低价格法",
            "最低所有权成本法"});
            this.pgff_cmb.Location = new System.Drawing.Point(85, 24);
            this.pgff_cmb.Name = "pgff_cmb";
            this.pgff_cmb.Size = new System.Drawing.Size(200, 22);
            this.pgff_cmb.TabIndex = 91;
            this.pgff_cmb.Text = "加权平均法";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(26, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 79;
            this.label14.Text = "评估方法";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.panel12);
            this.tabPage4.Controls.Add(this.panel2);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.panel4);
            this.tabPage4.Controls.Add(this.zbff_cmb);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.comboBox2);
            this.tabPage4.Controls.Add(this.xufs_cmb);
            this.tabPage4.Controls.Add(this.panel5);
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Controls.Add(this.pjff_cmb);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.panel6);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(743, 1924);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "项目详细信息";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.cgjg_tb);
            this.panel12.Controls.Add(this.cgdh_tb);
            this.panel12.Controls.Add(this.xqdh_tb);
            this.panel12.Controls.Add(this.label19);
            this.panel12.Controls.Add(this.cglx_cmb);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Controls.Add(this.wlmc_cmb);
            this.panel12.Controls.Add(this.label25);
            this.panel12.Controls.Add(this.wlbh_cmb);
            this.panel12.Controls.Add(this.label26);
            this.panel12.Location = new System.Drawing.Point(9, 88);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(655, 111);
            this.panel12.TabIndex = 221;
            // 
            // cgjg_tb
            // 
            this.cgjg_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgjg_tb.Location = new System.Drawing.Point(440, 87);
            this.cgjg_tb.Name = "cgjg_tb";
            this.cgjg_tb.Size = new System.Drawing.Size(200, 23);
            this.cgjg_tb.TabIndex = 165;
            this.cgjg_tb.Text = "1500";
            // 
            // cgdh_tb
            // 
            this.cgdh_tb.AutoSize = true;
            this.cgdh_tb.Font = new System.Drawing.Font("宋体", 9F);
            this.cgdh_tb.Location = new System.Drawing.Point(30, 11);
            this.cgdh_tb.Name = "cgdh_tb";
            this.cgdh_tb.Size = new System.Drawing.Size(53, 12);
            this.cgdh_tb.TabIndex = 151;
            this.cgdh_tb.Text = "采购单号";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqdh_tb.Location = new System.Drawing.Point(90, 7);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(550, 23);
            this.xqdh_tb.TabIndex = 150;
            this.xqdh_tb.Text = "PZSRM2015000279";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(380, 92);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 31;
            this.label19.Text = "采购价格";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(90, 88);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 22);
            this.cglx_cmb.TabIndex = 30;
            this.cglx_cmb.Text = "标准采购类型";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(30, 92);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 29;
            this.label21.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 48);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlmc_cmb.TabIndex = 28;
            this.wlmc_cmb.Text = "无缝三通325*4mm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(380, 52);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 27;
            this.label25.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 14;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 48);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlbh_cmb.TabIndex = 26;
            this.wlbh_cmb.Text = "F00000026";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(30, 52);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 25;
            this.label26.Text = "物料编号";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sqrq_dt);
            this.panel2.Controls.Add(this.lxdh_tb);
            this.panel2.Controls.Add(this.xqsl_tb);
            this.panel2.Controls.Add(this.qtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.sqr_cmb);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.sqbm_cmb);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.jldw_tb);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.cqbh_cmb);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Location = new System.Drawing.Point(9, 166);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 286);
            this.panel2.TabIndex = 222;
            // 
            // sqrq_dt
            // 
            this.sqrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqrq_dt.Location = new System.Drawing.Point(440, 91);
            this.sqrq_dt.Name = "sqrq_dt";
            this.sqrq_dt.Size = new System.Drawing.Size(200, 23);
            this.sqrq_dt.TabIndex = 167;
            // 
            // lxdh_tb
            // 
            this.lxdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lxdh_tb.Location = new System.Drawing.Point(90, 171);
            this.lxdh_tb.Name = "lxdh_tb";
            this.lxdh_tb.Size = new System.Drawing.Size(200, 23);
            this.lxdh_tb.TabIndex = 166;
            this.lxdh_tb.Text = "81805623";
            // 
            // xqsl_tb
            // 
            this.xqsl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqsl_tb.Location = new System.Drawing.Point(90, 91);
            this.xqsl_tb.Name = "xqsl_tb";
            this.xqsl_tb.Size = new System.Drawing.Size(200, 23);
            this.xqsl_tb.TabIndex = 165;
            this.xqsl_tb.Text = "TON";
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qtyq_rtb.Location = new System.Drawing.Point(90, 212);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(550, 60);
            this.qtyq_rtb.TabIndex = 163;
            this.qtyq_rtb.Text = "无";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(30, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(380, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 161;
            this.label7.Text = "申请日期";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(30, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 160;
            this.label8.Text = "联系电话";
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.Location = new System.Drawing.Point(440, 131);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqr_cmb.TabIndex = 159;
            this.sqr_cmb.Text = "李四";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(392, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 158;
            this.label13.Text = "申请人";
            // 
            // sqbm_cmb
            // 
            this.sqbm_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqbm_cmb.FormattingEnabled = true;
            this.sqbm_cmb.Location = new System.Drawing.Point(90, 131);
            this.sqbm_cmb.Name = "sqbm_cmb";
            this.sqbm_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqbm_cmb.TabIndex = 157;
            this.sqbm_cmb.Text = "申请部门B";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(30, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 156;
            this.label10.Text = "申请部门";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(30, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 155;
            this.label16.Text = "需求数量";
            // 
            // jldw_tb
            // 
            this.jldw_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.jldw_tb.FormattingEnabled = true;
            this.jldw_tb.Location = new System.Drawing.Point(440, 51);
            this.jldw_tb.Name = "jldw_tb";
            this.jldw_tb.Size = new System.Drawing.Size(200, 22);
            this.jldw_tb.TabIndex = 153;
            this.jldw_tb.Text = "1500";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(380, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 152;
            this.label17.Text = "计量单位";
            // 
            // cqbh_cmb
            // 
            this.cqbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cqbh_cmb.FormattingEnabled = true;
            this.cqbh_cmb.Location = new System.Drawing.Point(90, 51);
            this.cqbh_cmb.Name = "cqbh_cmb";
            this.cqbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.cqbh_cmb.TabIndex = 151;
            this.cqbh_cmb.Text = "2300";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(30, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 150;
            this.label18.Text = "仓库编号";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(194, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(286, 19);
            this.label20.TabIndex = 225;
            this.label20.Text = "关于无缝三通325*4mm采购项目";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cgksrq_dt);
            this.panel4.Controls.Add(this.cgjsrq_dt);
            this.panel4.Controls.Add(this.hxcgpz_cmb);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.cgy1_cmb);
            this.panel4.Controls.Add(this.cgzz1_cmb);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel4.Location = new System.Drawing.Point(-11, 444);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(774, 125);
            this.panel4.TabIndex = 226;
            // 
            // cgksrq_dt
            // 
            this.cgksrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgksrq_dt.Location = new System.Drawing.Point(110, 51);
            this.cgksrq_dt.Name = "cgksrq_dt";
            this.cgksrq_dt.Size = new System.Drawing.Size(200, 23);
            this.cgksrq_dt.TabIndex = 147;
            // 
            // cgjsrq_dt
            // 
            this.cgjsrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgjsrq_dt.Location = new System.Drawing.Point(460, 48);
            this.cgjsrq_dt.Name = "cgjsrq_dt";
            this.cgjsrq_dt.Size = new System.Drawing.Size(200, 23);
            this.cgjsrq_dt.TabIndex = 146;
            // 
            // hxcgpz_cmb
            // 
            this.hxcgpz_cmb.FormattingEnabled = true;
            this.hxcgpz_cmb.Location = new System.Drawing.Point(109, 92);
            this.hxcgpz_cmb.Name = "hxcgpz_cmb";
            this.hxcgpz_cmb.Size = new System.Drawing.Size(201, 22);
            this.hxcgpz_cmb.TabIndex = 138;
            this.hxcgpz_cmb.Text = "采购订单";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 9F);
            this.label40.Location = new System.Drawing.Point(26, 95);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 12);
            this.label40.TabIndex = 137;
            this.label40.Text = "后需采购凭证";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 9F);
            this.label38.Location = new System.Drawing.Point(377, 53);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 12);
            this.label38.TabIndex = 134;
            this.label38.Text = "采购结束日期";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("宋体", 9F);
            this.label39.Location = new System.Drawing.Point(26, 57);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 12);
            this.label39.TabIndex = 133;
            this.label39.Text = "采购开始日期";
            // 
            // cgy1_cmb
            // 
            this.cgy1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgy1_cmb.FormattingEnabled = true;
            this.cgy1_cmb.Location = new System.Drawing.Point(460, 11);
            this.cgy1_cmb.Name = "cgy1_cmb";
            this.cgy1_cmb.Size = new System.Drawing.Size(200, 22);
            this.cgy1_cmb.TabIndex = 123;
            this.cgy1_cmb.Text = "采购员A";
            // 
            // cgzz1_cmb
            // 
            this.cgzz1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz1_cmb.FormattingEnabled = true;
            this.cgzz1_cmb.Location = new System.Drawing.Point(109, 10);
            this.cgzz1_cmb.Name = "cgzz1_cmb";
            this.cgzz1_cmb.Size = new System.Drawing.Size(201, 22);
            this.cgzz1_cmb.TabIndex = 122;
            this.cgzz1_cmb.Text = "0008";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(413, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 121;
            this.label31.Text = "采购员";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(50, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 118;
            this.label33.Text = "采购组织";
            // 
            // zbff_cmb
            // 
            this.zbff_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zbff_cmb.FormattingEnabled = true;
            this.zbff_cmb.Location = new System.Drawing.Point(449, 629);
            this.zbff_cmb.Name = "zbff_cmb";
            this.zbff_cmb.Size = new System.Drawing.Size(200, 22);
            this.zbff_cmb.TabIndex = 230;
            this.zbff_cmb.Text = "两段式招标";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(389, 633);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 229;
            this.label22.Text = "招标方法";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(99, 629);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(200, 22);
            this.comboBox2.TabIndex = 228;
            this.comboBox2.Text = "正式招标方法";
            // 
            // xufs_cmb
            // 
            this.xufs_cmb.AutoSize = true;
            this.xufs_cmb.Font = new System.Drawing.Font("宋体", 9F);
            this.xufs_cmb.Location = new System.Drawing.Point(39, 633);
            this.xufs_cmb.Name = "xufs_cmb";
            this.xufs_cmb.Size = new System.Drawing.Size(53, 12);
            this.xufs_cmb.TabIndex = 227;
            this.xufs_cmb.Text = "寻源方式";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label23);
            this.panel5.Location = new System.Drawing.Point(1, 575);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(675, 31);
            this.panel5.TabIndex = 224;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(4, 4);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(136, 21);
            this.label23.TabIndex = 0;
            this.label23.Text = "选择寻源方式";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Silver;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label24);
            this.panel7.Location = new System.Drawing.Point(1, 51);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(675, 31);
            this.panel7.TabIndex = 223;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(4, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(136, 21);
            this.label24.TabIndex = 0;
            this.label24.Text = "项目基本信息";
            // 
            // pjff_cmb
            // 
            this.pjff_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pjff_cmb.FormattingEnabled = true;
            this.pjff_cmb.Location = new System.Drawing.Point(99, 672);
            this.pjff_cmb.Name = "pjff_cmb";
            this.pjff_cmb.Size = new System.Drawing.Size(200, 22);
            this.pjff_cmb.TabIndex = 184;
            this.pjff_cmb.Text = "加权平均法";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(39, 676);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 183;
            this.label27.Text = "评价方法";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cancel_bt);
            this.panel6.Controls.Add(this.submit_bt);
            this.panel6.Location = new System.Drawing.Point(24, 785);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(655, 60);
            this.panel6.TabIndex = 180;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(82, 15);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.checkBox14);
            this.panel11.Controls.Add(this.label74);
            this.panel11.Controls.Add(this.textBox93);
            this.panel11.Controls.Add(this.label75);
            this.panel11.Controls.Add(this.textBox97);
            this.panel11.Controls.Add(this.label76);
            this.panel11.Controls.Add(this.textBox101);
            this.panel11.Controls.Add(this.label77);
            this.panel11.Controls.Add(this.textBox105);
            this.panel11.Controls.Add(this.textBox109);
            this.panel11.Controls.Add(this.label78);
            this.panel11.Controls.Add(this.label79);
            this.panel11.Controls.Add(this.label80);
            this.panel11.Controls.Add(this.textBox112);
            this.panel11.Controls.Add(this.textBox115);
            this.panel11.Location = new System.Drawing.Point(9, 698);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(712, 81);
            this.panel11.TabIndex = 220;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox14.Location = new System.Drawing.Point(23, 45);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 121;
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.SystemColors.Window;
            this.label74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label74.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label74.Location = new System.Drawing.Point(600, 16);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(100, 25);
            this.label74.TabIndex = 114;
            this.label74.Text = "投标人标识";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox93
            // 
            this.textBox93.BackColor = System.Drawing.SystemColors.Window;
            this.textBox93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox93.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox93.Location = new System.Drawing.Point(600, 40);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(100, 25);
            this.textBox93.TabIndex = 108;
            this.textBox93.Text = "10000987";
            this.textBox93.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.SystemColors.Window;
            this.label75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label75.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label75.Location = new System.Drawing.Point(501, 16);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(100, 25);
            this.label75.TabIndex = 107;
            this.label75.Text = "公司标识";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.SystemColors.Window;
            this.textBox97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox97.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox97.Location = new System.Drawing.Point(501, 40);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(100, 25);
            this.textBox97.TabIndex = 101;
            this.textBox97.Text = "1234";
            this.textBox97.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.SystemColors.Window;
            this.label76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label76.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label76.Location = new System.Drawing.Point(442, 16);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(60, 25);
            this.label76.TabIndex = 100;
            this.label76.Text = "国家";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox101
            // 
            this.textBox101.BackColor = System.Drawing.SystemColors.Window;
            this.textBox101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox101.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox101.Location = new System.Drawing.Point(442, 40);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(60, 25);
            this.textBox101.TabIndex = 94;
            this.textBox101.Text = "CN";
            this.textBox101.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.SystemColors.Window;
            this.label77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label77.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label77.Location = new System.Drawing.Point(343, 16);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(100, 25);
            this.label77.TabIndex = 93;
            this.label77.Text = "联系人";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox105
            // 
            this.textBox105.BackColor = System.Drawing.SystemColors.Window;
            this.textBox105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox105.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox105.Location = new System.Drawing.Point(343, 40);
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new System.Drawing.Size(100, 25);
            this.textBox105.TabIndex = 87;
            this.textBox105.Text = "李四";
            this.textBox105.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox109
            // 
            this.textBox109.BackColor = System.Drawing.SystemColors.Window;
            this.textBox109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox109.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox109.Location = new System.Drawing.Point(15, 40);
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new System.Drawing.Size(30, 25);
            this.textBox109.TabIndex = 0;
            this.textBox109.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.SystemColors.Window;
            this.label78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label78.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label78.Location = new System.Drawing.Point(15, 16);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(30, 25);
            this.label78.TabIndex = 86;
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.SystemColors.Window;
            this.label79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label79.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label79.Location = new System.Drawing.Point(44, 16);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(50, 25);
            this.label79.TabIndex = 86;
            this.label79.Text = "序号";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label80
            // 
            this.label80.BackColor = System.Drawing.SystemColors.Window;
            this.label80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label80.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label80.Location = new System.Drawing.Point(93, 16);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(251, 25);
            this.label80.TabIndex = 86;
            this.label80.Text = "公司名称";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox112
            // 
            this.textBox112.BackColor = System.Drawing.SystemColors.Window;
            this.textBox112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox112.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox112.Location = new System.Drawing.Point(44, 40);
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new System.Drawing.Size(50, 25);
            this.textBox112.TabIndex = 1;
            this.textBox112.Text = "1";
            this.textBox112.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox115
            // 
            this.textBox115.BackColor = System.Drawing.SystemColors.Window;
            this.textBox115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox115.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox115.Location = new System.Drawing.Point(93, 40);
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new System.Drawing.Size(251, 25);
            this.textBox115.TabIndex = 2;
            this.textBox115.Text = "上海卓亚矿山机械有限公司";
            this.textBox115.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GeneralMethod_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(798, 749);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "GeneralMethod_Form";
            this.Text = "普通招标法";
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.sjbg_panel.ResumeLayout(false);
            this.sjbg_panel.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button refresh_bt;
        private System.Windows.Forms.Button fabu_bt;
        private System.Windows.Forms.Button zbzl_bt;
        private System.Windows.Forms.Button del_bt;
        private System.Windows.Forms.Button save_bt;
        private System.Windows.Forms.Button close_bt;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox tbcgz_cmb;
        private System.Windows.Forms.ComboBox tbcgzz_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button tbrdel_bt;
        private System.Windows.Forms.Button tbrtz_bt;
        private System.Windows.Forms.TextBox tbrbs_tb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox tbhxpz_cmb;
        private System.Windows.Forms.ComboBox tbhb_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbmbz_tb;
        private System.Windows.Forms.DateTimePicker tbkfrq_dt;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.DateTimePicker tbtjrq_dt;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.DateTimePicker tbksrq_dt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button tbradd_bt;
        private System.Windows.Forms.ComboBox pgff_cmb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.ComboBox pjff_cmb;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel sjbg_panel;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox qlab_zd;
        private System.Windows.Forms.Label shp1_lbl;
        private System.Windows.Forms.Label shp3_lbl;
        private System.Windows.Forms.TextBox hlca_zd;
        private System.Windows.Forms.TextBox qlab_zx;
        private System.Windows.Forms.TextBox qlca_zd;
        private System.Windows.Forms.TextBox hlab_zd;
        private System.Windows.Forms.TextBox qlca_zx;
        private System.Windows.Forms.TextBox qlab_ed;
        private System.Windows.Forms.TextBox qlca_ed;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label shp2_lbl;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox hlca_ed;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label zd_lbl;
        private System.Windows.Forms.TextBox hlbc_ed;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox hlab_ed;
        private System.Windows.Forms.TextBox hlca_zx;
        private System.Windows.Forms.Label zx_lbl;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox qlbc_zd;
        private System.Windows.Forms.TextBox qlbc_zx;
        private System.Windows.Forms.TextBox hlbc_zx;
        private System.Windows.Forms.TextBox hlbc_zd;
        private System.Windows.Forms.TextBox hlab_zx;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBox112;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox121;
        private System.Windows.Forms.TextBox textBox122;
        private System.Windows.Forms.TextBox textBox123;
        private System.Windows.Forms.TextBox textBox124;
        private System.Windows.Forms.TextBox textBox125;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker sqrq_dt;
        private System.Windows.Forms.TextBox lxdh_tb;
        private System.Windows.Forms.TextBox xqsl_tb;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox sqbm_cmb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox jldw_tb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cqbh_cmb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker cgksrq_dt;
        private System.Windows.Forms.DateTimePicker cgjsrq_dt;
        private System.Windows.Forms.ComboBox hxcgpz_cmb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cgy1_cmb;
        private System.Windows.Forms.ComboBox cgzz1_cmb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox zbff_cmb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label xufs_cmb;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox cgjg_tb;
        private System.Windows.Forms.Label cgdh_tb;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox126;
        private System.Windows.Forms.TextBox textBox148;
        private System.Windows.Forms.TextBox textBox149;
        private System.Windows.Forms.TextBox textBox150;
        private System.Windows.Forms.TextBox textBox151;
        private System.Windows.Forms.TextBox textBox152;
        private System.Windows.Forms.TextBox textBox153;
        private System.Windows.Forms.TextBox textBox154;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox textBox155;
        private System.Windows.Forms.TextBox textBox156;
        private System.Windows.Forms.TextBox textBox157;
        private System.Windows.Forms.TextBox textBox158;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox textBox159;
        private System.Windows.Forms.TextBox textBox160;
        private System.Windows.Forms.TextBox textBox161;
        private System.Windows.Forms.TextBox textBox162;
        private System.Windows.Forms.TextBox textBox163;
        private System.Windows.Forms.TextBox textBox164;
        private System.Windows.Forms.TextBox textBox165;
        private System.Windows.Forms.TextBox textBox166;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.TextBox textBox111;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.TextBox textBox127;
        private System.Windows.Forms.TextBox textBox128;
        private System.Windows.Forms.TextBox textBox129;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox130;
        private System.Windows.Forms.TextBox textBox131;
        private System.Windows.Forms.TextBox textBox132;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBox133;
        private System.Windows.Forms.TextBox textBox134;
        private System.Windows.Forms.TextBox textBox135;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textBox136;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox137;
        private System.Windows.Forms.TextBox textBox138;
        private System.Windows.Forms.TextBox textBox139;
        private System.Windows.Forms.TextBox textBox140;
        private System.Windows.Forms.TextBox textBox141;
        private System.Windows.Forms.TextBox textBox142;
        private System.Windows.Forms.TextBox textBox143;
        private System.Windows.Forms.TextBox textBox144;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox textBox145;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox textBox146;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textBox147;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox227;
        private System.Windows.Forms.TextBox textBox228;
        private System.Windows.Forms.TextBox textBox229;
        private System.Windows.Forms.TextBox textBox230;
        private System.Windows.Forms.TextBox textBox231;
        private System.Windows.Forms.TextBox textBox232;
        private System.Windows.Forms.TextBox textBox233;
        private System.Windows.Forms.TextBox textBox234;
        private System.Windows.Forms.TextBox textBox235;
        private System.Windows.Forms.TextBox textBox236;
        private System.Windows.Forms.TextBox textBox237;
        private System.Windows.Forms.TextBox textBox238;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBox239;
        private System.Windows.Forms.TextBox textBox240;
        private System.Windows.Forms.TextBox textBox241;
        private System.Windows.Forms.TextBox textBox242;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.TextBox textBox187;
        private System.Windows.Forms.TextBox textBox188;
        private System.Windows.Forms.TextBox textBox189;
        private System.Windows.Forms.TextBox textBox190;
        private System.Windows.Forms.TextBox textBox191;
        private System.Windows.Forms.TextBox textBox192;
        private System.Windows.Forms.TextBox textBox193;
        private System.Windows.Forms.TextBox textBox194;
        private System.Windows.Forms.TextBox textBox195;
        private System.Windows.Forms.TextBox textBox196;
        private System.Windows.Forms.TextBox textBox197;
        private System.Windows.Forms.TextBox textBox198;
        private System.Windows.Forms.TextBox textBox199;
        private System.Windows.Forms.TextBox textBox200;
        private System.Windows.Forms.TextBox textBox201;
        private System.Windows.Forms.TextBox textBox202;
        private System.Windows.Forms.TextBox textBox203;
        private System.Windows.Forms.TextBox textBox204;
        private System.Windows.Forms.TextBox textBox205;
        private System.Windows.Forms.TextBox textBox206;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.TextBox textBox207;
        private System.Windows.Forms.TextBox textBox208;
        private System.Windows.Forms.TextBox textBox209;
        private System.Windows.Forms.TextBox textBox210;
        private System.Windows.Forms.TextBox textBox211;
        private System.Windows.Forms.TextBox textBox212;
        private System.Windows.Forms.TextBox textBox213;
        private System.Windows.Forms.TextBox textBox214;
        private System.Windows.Forms.TextBox textBox215;
        private System.Windows.Forms.TextBox textBox216;
        private System.Windows.Forms.TextBox textBox217;
        private System.Windows.Forms.TextBox textBox218;
        private System.Windows.Forms.TextBox textBox219;
        private System.Windows.Forms.TextBox textBox220;
        private System.Windows.Forms.TextBox textBox221;
        private System.Windows.Forms.TextBox textBox222;
        private System.Windows.Forms.TextBox textBox223;
        private System.Windows.Forms.TextBox textBox224;
        private System.Windows.Forms.TextBox textBox225;
        private System.Windows.Forms.TextBox textBox226;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.TextBox textBox167;
        private System.Windows.Forms.TextBox textBox168;
        private System.Windows.Forms.TextBox textBox169;
        private System.Windows.Forms.TextBox textBox170;
        private System.Windows.Forms.TextBox textBox171;
        private System.Windows.Forms.TextBox textBox172;
        private System.Windows.Forms.TextBox textBox173;
        private System.Windows.Forms.TextBox textBox174;
        private System.Windows.Forms.TextBox textBox175;
        private System.Windows.Forms.TextBox textBox176;
        private System.Windows.Forms.TextBox textBox177;
        private System.Windows.Forms.TextBox textBox178;
        private System.Windows.Forms.TextBox textBox179;
        private System.Windows.Forms.TextBox textBox180;
        private System.Windows.Forms.TextBox textBox181;
        private System.Windows.Forms.TextBox textBox182;
        private System.Windows.Forms.TextBox textBox183;
        private System.Windows.Forms.TextBox textBox184;
        private System.Windows.Forms.TextBox textBox185;
        private System.Windows.Forms.TextBox textBox186;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox261;
        private System.Windows.Forms.TextBox textBox262;
        private System.Windows.Forms.TextBox textBox263;
        private System.Windows.Forms.TextBox textBox264;
        private System.Windows.Forms.TextBox textBox265;
        private System.Windows.Forms.TextBox textBox266;
        private System.Windows.Forms.TextBox textBox267;
        private System.Windows.Forms.TextBox textBox268;
        private System.Windows.Forms.TextBox textBox269;
        private System.Windows.Forms.TextBox textBox270;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox271;
        private System.Windows.Forms.TextBox textBox272;
        private System.Windows.Forms.TextBox textBox273;
        private System.Windows.Forms.TextBox textBox274;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.TextBox textBox279;
        private System.Windows.Forms.TextBox textBox280;
        private System.Windows.Forms.TextBox textBox281;
        private System.Windows.Forms.TextBox textBox282;
        private System.Windows.Forms.TextBox textBox283;
        private System.Windows.Forms.TextBox textBox284;
        private System.Windows.Forms.TextBox textBox285;
        private System.Windows.Forms.TextBox textBox286;
        private System.Windows.Forms.TextBox textBox287;
        private System.Windows.Forms.TextBox textBox288;
        private System.Windows.Forms.TextBox textBox289;
        private System.Windows.Forms.TextBox textBox290;
        private System.Windows.Forms.TextBox textBox291;
        private System.Windows.Forms.TextBox textBox292;
        private System.Windows.Forms.TextBox textBox293;
        private System.Windows.Forms.TextBox textBox294;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.TextBox textBox299;
        private System.Windows.Forms.TextBox textBox300;
        private System.Windows.Forms.TextBox textBox301;
        private System.Windows.Forms.TextBox textBox302;
        private System.Windows.Forms.TextBox textBox303;
        private System.Windows.Forms.TextBox textBox304;
        private System.Windows.Forms.TextBox textBox305;
        private System.Windows.Forms.TextBox textBox306;
        private System.Windows.Forms.TextBox textBox307;
        private System.Windows.Forms.TextBox textBox308;
        private System.Windows.Forms.TextBox textBox309;
        private System.Windows.Forms.TextBox textBox310;
        private System.Windows.Forms.TextBox textBox311;
        private System.Windows.Forms.TextBox textBox312;
        private System.Windows.Forms.TextBox textBox313;
        private System.Windows.Forms.TextBox textBox314;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.TextBox textBox319;
        private System.Windows.Forms.TextBox textBox320;
        private System.Windows.Forms.TextBox textBox321;
        private System.Windows.Forms.TextBox textBox322;
        private System.Windows.Forms.TextBox textBox323;
        private System.Windows.Forms.TextBox textBox324;
        private System.Windows.Forms.TextBox textBox325;
        private System.Windows.Forms.TextBox textBox326;
        private System.Windows.Forms.TextBox textBox327;
        private System.Windows.Forms.TextBox textBox328;
        private System.Windows.Forms.TextBox textBox329;
        private System.Windows.Forms.TextBox textBox330;
        private System.Windows.Forms.TextBox textBox331;
        private System.Windows.Forms.TextBox textBox332;
        private System.Windows.Forms.TextBox textBox333;
        private System.Windows.Forms.TextBox textBox334;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBox339;
        private System.Windows.Forms.TextBox textBox340;
        private System.Windows.Forms.TextBox textBox341;
        private System.Windows.Forms.TextBox textBox342;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox343;
        private System.Windows.Forms.TextBox textBox344;
        private System.Windows.Forms.TextBox textBox345;
        private System.Windows.Forms.TextBox textBox346;
        private System.Windows.Forms.TextBox textBox347;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox textBox348;
        private System.Windows.Forms.TextBox textBox349;
        private System.Windows.Forms.TextBox textBox350;
        private System.Windows.Forms.TextBox textBox351;
        private System.Windows.Forms.TextBox textBox352;
        private System.Windows.Forms.TextBox textBox353;
        private System.Windows.Forms.TextBox textBox354;
        private System.Windows.Forms.TextBox textBox243;
        private System.Windows.Forms.TextBox textBox244;
        private System.Windows.Forms.TextBox textBox245;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.TextBox textBox246;
        private System.Windows.Forms.TextBox textBox247;
        private System.Windows.Forms.TextBox textBox248;
        private System.Windows.Forms.TextBox textBox249;
        private System.Windows.Forms.TextBox textBox250;
        private System.Windows.Forms.TextBox textBox251;
        private System.Windows.Forms.TextBox textBox252;
        private System.Windows.Forms.TextBox textBox253;
        private System.Windows.Forms.TextBox textBox254;
        private System.Windows.Forms.TextBox textBox255;
        private System.Windows.Forms.TextBox textBox256;
        private System.Windows.Forms.TextBox textBox257;
        private System.Windows.Forms.TextBox textBox258;
        private System.Windows.Forms.TextBox textBox259;
        private System.Windows.Forms.TextBox textBox260;

    }
}