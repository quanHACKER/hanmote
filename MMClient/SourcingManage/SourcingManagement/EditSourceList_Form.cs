﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class EditSourceList_Form : DockContent
    {
        //货源清单
        List<SourceList> sourceList = new List<SourceList>();
        List<SourceList> xjxList = new List<SourceList>();
        //查询工具
        SourceListBLL sourceListBll = new SourceListBLL();
        //控件工具
        ConvenientTools tools = new ConvenientTools();
        List<string> cgzzItems = null;

        public SourceList oneSourceList = null;

        public EditSourceList_Form()
        {
            InitializeComponent();
           
        }

        public EditSourceList_Form(SourceList oneSourceList)
        {
            InitializeComponent();
            this.oneSourceList = oneSourceList;
            init();
        }

        public void init()
        {
            initHeadInfo();
            initBaseInfo(); 
        }

        private void initHeadInfo()
        {
            txt_MaterialId.Text = oneSourceList.Material_ID;
            txt_FactoryId.Text = oneSourceList.Factory_ID;
        }

        private void initBaseInfo()
        {
            txt_SupplierId.Text = oneSourceList.Supplier_ID;
            txt_PurchaseGroup.Text = oneSourceList.PurchaseOrg;
            txt_ppl.Text = oneSourceList.PPL;
            txt_protople.Text = oneSourceList.ProtocolId;
            dte_StartTime.Text = oneSourceList.StartTime.ToString("yyyy-MM-dd hh:mm:ss");
            dte_EndTime.Text = oneSourceList.EndTime.ToString("yyyy-MM-dd hh:mm:ss");
            if (oneSourceList.Fix == 1)
            {
                chk_Fix.Checked = true;
            }
            if (oneSourceList.Blk == 1)
            {
                chk_Blk.Checked = true;
            }
            if(oneSourceList.MRP == 1)
            {
                chk_MRP.Checked = true;
            }

        }

        /// <summary>
        /// 更改货源清单记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            SourceList sourceList = new SourceList();
            sourceList.SourceListId = oneSourceList.SourceListId;
            sourceList.Material_ID = txt_MaterialId.Text;
            sourceList.Factory_ID = txt_FactoryId.Text;
            sourceList.Supplier_ID = txt_SupplierId.Text;
            sourceList.PPL = txt_ppl.Text;
            sourceList.StartTime = DateTime.Parse(dte_StartTime.Text);
            sourceList.EndTime = DateTime.Parse(dte_EndTime.Text);
            if (chk_Fix.Checked == true)
            {
                sourceList.Fix = 1;
            }
            if(chk_Blk.Checked == true)
            {
                sourceList.Blk = 1;
            }
            if(chk_MRP.Checked == true)
            {
                sourceList.MRP = 1;
            }
            int res = sourceListBll.updateSourceList(sourceList);
            if (res > 0)
            {
                MessageBox.Show("货源清单更新成功");
                this.Close();
            }
        }
    }
}
