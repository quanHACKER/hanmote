﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using System.Text.RegularExpressions;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class TwiceMethod_Form : DockContent
    {
        public UserUI userUI;
        //需求表格生成行数
        int demandNum = 5;
        //被选中的行值
        int j = 0;
        Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
        public DataTable checkDt = null;

        public TwiceMethod_Form(UserUI userUi)
        {
            InitializeComponent();
            addItemsToCMB();
            DynamicDrawTable(5, "", "", false);
            this.userUI = userUi;
        }


                /// <summary>
        /// combobox自动加载items
        /// </summary>
        public void addItemsToCMB()
        {
            //需求单号自动加载至item
            DataTable findItem = newStandardDAL.AddItemsToCombobox("Supplier_ID", "Supplier_Base");
            if (findItem != null && findItem.Rows.Count > 0)
            {
                for (int n = 0; n < findItem.Rows.Count; n++)
                {
                    this.tbrbs_cmb.Items.Add(findItem.Rows[n][0].ToString());
                }
            }
            this.tbrbs_cmb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.tbrbs_cmb.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void TwiceMethod_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 动态画表格
        /// </summary>
        public void DynamicDrawTable(int item1, String item2, String item3, Boolean isFromSH)
        {
            if (isFromSH)
            {
                //displayOnThis(item1, item2, item3);
            }
            //查询需求计划table
            Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
            DataTable dt = new DataTable();
            dt = newStandardDAL.Supplier_Base(item1, item2, item3);
            if (dt == null || dt.Rows.Count == 0)
            {
                MessageBox.Show("未找到相关记录，请重新编辑查询词条！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (isFromSH)
                demandNum++;
            else
                demandNum = dt.Rows.Count;

            //第一个label的高
            int add_height = this.lbl_1.Height * demandNum;
            int high = this.lbl_1.Height * demandNum;
            //动态改变panel的高度
            if (isFromSH)
            {
                //动态改变panel的高度
                this.cgjhgl_panel.Height = this.cgjhgl_panel.Height + this.lbl_1.Height;
            }
            else
            {
                //动态改变panel的高度
                this.cgjhgl_panel.Height = this.cgjhgl_panel.Height + high;
            }
            //MoveDownPanel(high);

            //导航条label
            Label lbl_1_head = this.lbl_1;
            Label lbl_2_head = this.lbl_2;
            Label lbl_3_head = this.lbl_3;
            Label lbl_4_head = this.lbl_4;
            Label lbl_5_head = this.lbl_5;
            Label lbl_6_head = this.lbl_6;
            Label lbl_7_head = this.lbl_7;

            //label和textbox的高度
            int h = add_height / demandNum;
            for (int i = 1; i <= demandNum; i++)
            {
                if (isFromSH)
                {
                    i = demandNum;
                }
                //lbl_1下的label
                TextBox a_lbl = new TextBox();
                a_lbl.BorderStyle = BorderStyle.FixedSingle;
                a_lbl.BackColor = System.Drawing.SystemColors.Window;
                a_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                a_lbl.Name = "ch_" + i;
                a_lbl.TextAlign = HorizontalAlignment.Center;
                a_lbl.Width = lbl_1_head.Width;
                a_lbl.Height = h;
                a_lbl.Location = new Point(lbl_1_head.Location.X, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1);
                //添加选择框
                RadioButton a_cb = new RadioButton();
                a_cb.BringToFront();
                a_cb.AutoSize = true;
                a_cb.Name = "cb_" + i;
                a_cb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
                a_cb.Size = new System.Drawing.Size(15, 14);
                a_cb.TabIndex = 121;
                a_cb.UseVisualStyleBackColor = false;
                a_cb.Location = new Point(lbl_1_head.Location.X + lbl_1_head.Width / 4 + 1, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1 + h / 4);

                //lbl_2下的textbox
                TextBox a_txt = new TextBox();
                a_txt.BorderStyle = BorderStyle.FixedSingle;
                a_txt.BackColor = System.Drawing.SystemColors.Window;
                a_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                a_txt.Name = "a_" + i;
                a_txt.Text = i.ToString();
                a_txt.TextAlign = HorizontalAlignment.Center;
                a_txt.Width = lbl_2_head.Width;
                a_txt.Height = h;
                a_txt.Location = new Point(lbl_2_head.Location.X, lbl_2_head.Location.Y + lbl_2_head.Height + (h - 1) * (i - 1) - 1);

                //lbl_3下的textbox
                TextBox b_txt = new TextBox();
                b_txt.BorderStyle = BorderStyle.FixedSingle;
                b_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                b_txt.Name = "b_" + i;
                b_txt.TextAlign = HorizontalAlignment.Center;
                b_txt.Width = lbl_3_head.Width;
                b_txt.Height = h;
                b_txt.Location = new Point(lbl_3_head.Location.X, lbl_3_head.Location.Y + lbl_3_head.Height + (h - 1) * (i - 1) - 1);
                if (isFromSH)
                    b_txt.Text = dt.Rows[0][3].ToString();
                else 
                    b_txt.Text = dt.Rows[i - 1][3].ToString();

                //lbl_4下的textbox
                TextBox c_txt = new TextBox();
                c_txt.BorderStyle = BorderStyle.FixedSingle;
                c_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                c_txt.Name = "c_" + i;
                c_txt.TextAlign = HorizontalAlignment.Center;
                c_txt.Width = lbl_4_head.Width;
                c_txt.Height = h;
                c_txt.Location = new Point(lbl_4_head.Location.X, lbl_4_head.Location.Y + lbl_4_head.Height + (h - 1) * (i - 1) - 1);
                if (isFromSH)
                    c_txt.Text = dt.Rows[0][7].ToString();
                else
                    c_txt.Text = dt.Rows[i - 1][7].ToString();

                //lbl_5下的textbox
                TextBox d_txt = new TextBox();
                d_txt.BorderStyle = BorderStyle.FixedSingle;
                d_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                d_txt.Name = "d_" + i;
                d_txt.TextAlign = HorizontalAlignment.Center;
                d_txt.Width = lbl_5_head.Width;
                d_txt.Height = h;
                d_txt.Location = new Point(lbl_5_head.Location.X, lbl_5_head.Location.Y + lbl_5_head.Height + (h - 1) * (i - 1) - 1);
                if (isFromSH)
                    d_txt.Text = dt.Rows[0][5].ToString();
                else
                    d_txt.Text = dt.Rows[i - 1][5].ToString();


                //lbl_6下的textbox
                TextBox e_txt = new TextBox();
                e_txt.BorderStyle = BorderStyle.FixedSingle;
                e_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                e_txt.Name = "e_" + i;
                e_txt.TextAlign = HorizontalAlignment.Center;
                e_txt.Width = lbl_6_head.Width;
                e_txt.Height = h;
                e_txt.Location = new Point(lbl_6_head.Location.X, lbl_6_head.Location.Y + lbl_6_head.Height + (h - 1) * (i - 1) - 1);
                if (isFromSH)
                    e_txt.Text = dt.Rows[0][0].ToString();
                else
                    e_txt.Text = dt.Rows[i - 1][0].ToString();

                //lbl_7下的textbox
                TextBox f_txt = new TextBox();
                f_txt.BorderStyle = BorderStyle.FixedSingle;
                f_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                f_txt.Name = "f_" + i;
                f_txt.TextAlign = HorizontalAlignment.Center;
                f_txt.Width = lbl_7_head.Width;
                f_txt.Height = h;
                f_txt.Location = new Point(lbl_7_head.Location.X, lbl_7_head.Location.Y + lbl_7_head.Height + (h - 1) * (i - 1) - 1);
                if (isFromSH)
                    f_txt.Text = dt.Rows[0][1].ToString();
                else
                    f_txt.Text = dt.Rows[i - 1][1].ToString();


                //绑定事件
                /*
                a_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                b_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                c_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                a_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                b_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                c_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                a_txt.Validating += new CancelEventHandler(txt_Validating);
                b_txt.Validating += new CancelEventHandler(txt_Validating);
                c_txt.Validating += new CancelEventHandler(txt_Validating);
                */
                a_cb.CheckedChanged += new EventHandler(a_cb_CheckedChanged);

                //将控件加到窗体中
                this.cgjhgl_panel.Controls.Add(a_cb);
                this.cgjhgl_panel.Controls.Add(a_lbl);
                this.cgjhgl_panel.Controls.Add(a_txt);
                this.cgjhgl_panel.Controls.Add(b_txt);
                this.cgjhgl_panel.Controls.Add(c_txt);
                this.cgjhgl_panel.Controls.Add(d_txt);
                this.cgjhgl_panel.Controls.Add(e_txt);
                this.cgjhgl_panel.Controls.Add(f_txt);
            }
        }

        /// <summary>
        /// 选中项的采购信息数据
        /// </summary>
        private void a_cb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton cb = sender as RadioButton;

            //前一选中整行颜色变回
            if (j != 0)
            {
                this.cgjhgl_panel.Controls["ch_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["a_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["b_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["c_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["d_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["e_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.cgjhgl_panel.Controls["f_" + j].BackColor = System.Drawing.SystemColors.Window;
            }

            //选中的行值
            j = Convert.ToInt32(cb.Name.ToString().Substring(3, cb.Name.ToString().Length - 3));

            //选中整行变色
            this.cgjhgl_panel.Controls["ch_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["a_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["b_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["c_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["d_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["e_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cgjhgl_panel.Controls["f_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;

            //获取选中行的需求单号ID并读取信息
            string ID = this.cgjhgl_panel.Controls["b_" + j].Text;
            String item1 = " Demand_ID = '" + ID + "'";
            String item2 = null;
            String item3 = null;
            checkDt = newStandardDAL.StandardByWLBH(item1, item2, item3);
            if (checkDt == null || checkDt.Rows.Count == 0)
            {
                MessageBox.Show("未找到相关记录，请重新查询选择！");
                return;
            }
        }


        //表格自动生成
        private void query_bt_Click(object sender, EventArgs e)
        {
            j = 0;
            RemoveDrawTable(demandNum);
            String item1 = null;
            String item2 = null;
            String item3 = null;
            //if (!string.IsNullOrWhiteSpace(this.cgdh_cmb.Text))
            //{
            //    item1 = " Demand_ID = '" + this.cgdh_cmb.Text + "'";
            //}
            //if (!string.IsNullOrWhiteSpace(this.wlbh_cmb.Text))
            //{
            //    item2 = " Material_ID = '" + this.wlbh_cmb.Text + "'";
            //}
            //if (!string.IsNullOrWhiteSpace(this.xyfs_cmb.Text))
            //{
            //    if (this.xyfs_cmb.Text.Trim().Equals("未寻源"))
            //        item3 = " state = '审核通过'";
            //    if (this.xyfs_cmb.Text.Trim().StartsWith("正式方法"))
            //        item3 = " state = '确定采购价格'";
            //    if (this.xyfs_cmb.Text.Trim().Equals("询报价方法"))
            //        item3 = " state = '待审核'";
            //}
            DynamicDrawTable(5, item2, item3, false);
        }

        /// <summary>
        /// 动态删除表格
        /// </summary>
        public void RemoveDrawTable(int rowNum)
        {
            int high = rowNum * this.lbl_1.Height;
            if (rowNum != 0)
            {
                for (int i = 1; i <= rowNum; i++)
                {
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["ch_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["cb_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["a_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["b_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["c_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["d_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["e_" + i]);
                    this.cgjhgl_panel.Controls.Remove(this.cgjhgl_panel.Controls["f_" + i]);
                }
                //动态改变panel的高度
                this.cgjhgl_panel.Height = this.cgjhgl_panel.Height - high;
                this.Height = this.Height - high;
            }
        }


        /// <summary>
        /// 添加供应商
        /// </summary>
        private void tbradd_bt_Click(object sender, EventArgs e)
        {
            if (this.tbrbs_cmb.Text.Trim().Length == 0)
            {
                demandNum++;
                DynamicDrawTable(demandNum, "", "", false);
            }
            else 
            {
                DynamicDrawTable(demandNum, this.tbrbs_cmb.Text.Trim(), "", true);
            }
            
        }

    }
}
