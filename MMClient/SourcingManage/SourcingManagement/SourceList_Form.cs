﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.MDBll.MT;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SourceList_Form : DockContent
    {
        //货源清单
        List<SourceList> sourceList = new List<SourceList>();
        List<SourceList> xjxList = new List<SourceList>();
        //查询工具
        SourceListBLL source_ListBLL = new SourceListBLL();
        MaterialBLL materialBll = new MaterialBLL();
        //控件工具
        ConvenientTools tools = new ConvenientTools();
        public SourceList oneSourceList;

        public SourceList_Form()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            initMaterial();
            tools.addItemsToComboBox(cmb_Factory, "", "", "Factory_ID", "Inventory");
        }

        private void initMaterial()
        {
            List<string> list = materialBll.GetAllMaterialID();
            for (int i = 0; i < list.Count; i++)
            {
                cmb_Material.Items.Add(list.ElementAt(i));
            }
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            string materialId = cmb_Material.Text;
            string factoryId = cmb_Factory.Text;
            List<SourceList> list = source_ListBLL.getSourceList(materialId,factoryId);
            sourceListGridView.Rows.Clear();
            if (list.Count > sourceListGridView.Rows.Count)
            {
                sourceListGridView.Rows.Add(list.Count - sourceListGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                SourceList sl = list.ElementAt(i);
                sourceListGridView.Rows[i].Cells["sourceListId"].Value = sl.SourceListId;
                sourceListGridView.Rows[i].Cells["supplierId"].Value = sl.Supplier_ID;
                sourceListGridView.Rows[i].Cells["startTime"].Value = sl.StartTime.ToString("yyyy-MM-dd hh:mm:ss");
                sourceListGridView.Rows[i].Cells["endTime"].Value = sl.EndTime.ToString("yyyy-MM-dd hh:mm:ss") ;
                sourceListGridView.Rows[i].Cells["purchaseOrg"].Value = sl.PurchaseOrg;
                sourceListGridView.Rows[i].Cells["ppl"].Value = sl.PPL;
                if (sl.Fix == 1)
                {
                    sourceListGridView.Rows[i].Cells["fix"].Value = true;
                }
                if (sl.Blk == 1)
                {
                    sourceListGridView.Rows[i].Cells["blk"].Value = true;
                }
                if (sl.MRP == 1)
                {
                    sourceListGridView.Rows[i].Cells["mrp"].Value = true;
                }
            }

        }


        private void sourceListGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = sourceListGridView.CurrentRow.Index;
            Object obj = sourceListGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            //根据选中行id查找对应的需求计划
            oneSourceList = source_ListBLL.getSourceListBySId(obj.ToString());
            MessageBox.Show(oneSourceList.SourceListId);
        }
    }
}
