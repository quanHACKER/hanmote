﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Transact_Form : DockContent
    {
        private UserUI userUI;
        public SourceBid bid;

        private SourceBLL sourceBll = new SourceBLL();
        private SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        private SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        private TransactCostBLL transactCostBll = new TransactCostBLL();
        private TransactItemBLL transactItemBll = new TransactItemBLL();
        private BidSupplierWeightScoreBLL bidSWSBll = new BidSupplierWeightScoreBLL();
        private SourceBidBLL bidBLL = new SourceBidBLL();     

        public Transact_Form()
        {
            InitializeComponent();
        }

        public Transact_Form(UserUI userUI,SourceBid bid)
        {
            InitializeComponent();
            this.userUI = userUI;
            this.bid = bid;
            init();
           
        }

        private void init()
        {
            initBaseInfo();
            initMaterialInfo();
        }

        /// <summary>
        /// 初始化招标基本信息
        /// </summary>
        private void initBaseInfo()
        {
            txt_BidId.Text = bid.BidId;
            txt_BidName.Text = sourceBll.findSourceById(bid.BidId).Source_Name;
            //初始化供应商信息
            initSupplierInfo();
        }

        /// <summary>
        /// 自动选择供应商
        /// </summary>
        private void initSupplierInfo()
        {
            //先获取招标的评价标准
            int evalMethod = bid.EvalMethId;
            //不同的评价标准供应商排序标准不一样
            string result  = getSuppliers(evalMethod);
            txt_SupplierId.Text = result;
        }

        private string getSuppliers(int evalMethod)
        {
            string result = null;
            List<SourceSupplier> list = sourceSupplierBll.getAvailableSuppliersByBId(bid.BidId);
            //加权评分法
            if(evalMethod==1)
            {
                SortedList<int, string> sList = new SortedList<int, string>();
                int total = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    total = 0;
                    SourceSupplier ss = list.ElementAt(i);
                    List<BidSupplierWeightScore> swsList = bidSWSBll.getBidSWSByBIdSId(bid.BidId,ss.Supplier_ID);
                    for (int j = 0; j < swsList.Count; j++)
                    {
                        total += swsList.ElementAt(i).Score;
                    }
                    sList.Add(total,ss.Supplier_ID);
                }
                int max = 0;
                for (int i = 0; i < sList.Count; i++)
                {
                    if (sList.ElementAt(i).Key > max)
                    {
                        max = sList.ElementAt(i).Key;
                        result = sList.ElementAt(i).Value;
                    }
                    
                }
            }
            return result;
        }

        private void initMaterialInfo()
        {
            materialGridView.Rows.Clear();
            List<SourceMaterial> list = sourceMaterialBll.findMaterialsBySId(bid.BidId);
            if (list.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(list.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = list.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = list.ElementAt(i).Material_Name;
                materialGridView.Rows[i].Cells["count"].Value = list.ElementAt(i).Demand_Count;
                materialGridView.Rows[i].Cells["unit"].Value = list.ElementAt(i).Unit;
            } 
        }

        /// <summary>
        /// 保存企业端的目标价格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Save_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow dgvr in materialGridView.Rows)
            {
                TransactCost tsc = new TransactCost();
                tsc.BidId = txt_BidId.Text;
                tsc.SupplierId = txt_SupplierId.Text;
                tsc.MaterialId = dgvr.Cells["materialId"].Value.ToString();
                tsc.MaterialName = dgvr.Cells["materialName"].Value.ToString();
                int count = int.Parse(dgvr.Cells["count"].Value.ToString());
                tsc.Count = count;
                tsc.Unit = dgvr.Cells["unit"].Value.ToString();
                float targetPrice = float.Parse(dgvr.Cells["targetPrice"].Value.ToString());
                tsc.TargetPrice = targetPrice;
                tsc.TargetTotal = count * targetPrice;
                transactCostBll.addTransactCost(tsc);
            }
            
            //修改谈判状态
            int res = sourceSupplierBll.updateTransState(bid.BidId, txt_SupplierId.Text,2);

            //保存说明项
            foreach (DataGridViewRow dgvr in itemsGridView.Rows)
            {
                TransactItem tt = new TransactItem();
                tt.BidId = bid.BidId;
                tt.SupplierId = txt_SupplierId.Text;
                tt.ItemName = dgvr.Cells["item"].Value.ToString();
                tt.GoalDescription = dgvr.Cells["goalDescription"].Value.ToString();
                transactItemBll.addTransactItem(tt);
            }
            MessageBox.Show("谈判创建成功");
            bid.BidState = 5;
            bidBLL.updateBidState(bid);
            this.Close();
        }

        /// <summary>
        /// 添加说明项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Add_Click(object sender, EventArgs e)
        {
            string item = cmb_items.Text;
            itemsGridView.Rows.Add();
            int rowCount = itemsGridView.Rows.Count;
            DataGridViewRow row = itemsGridView.Rows[rowCount - 1];
            row.Cells[0].Value = item;

        }

        /// <summary>
        /// 删除说明项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            for (int i = this.itemsGridView.SelectedRows.Count; i > 0; i--)
            {
                itemsGridView.Rows.RemoveAt(itemsGridView.SelectedRows[i - 1].Index);
            }
        }
    }
}
