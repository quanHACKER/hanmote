﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class SubmitTechnical_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonDownloadTec = new System.Windows.Forms.Button();
            this.buttonUploadTec = new System.Windows.Forms.Button();
            this.buttonDownloadBid = new System.Windows.Forms.Button();
            this.buttonUploadBid = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(306, 229);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 30);
            this.button2.TabIndex = 165;
            this.button2.Text = "取消";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(117, 229);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 30);
            this.button1.TabIndex = 164;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonDownloadTec
            // 
            this.buttonDownloadTec.Location = new System.Drawing.Point(251, 151);
            this.buttonDownloadTec.Name = "buttonDownloadTec";
            this.buttonDownloadTec.Size = new System.Drawing.Size(60, 30);
            this.buttonDownloadTec.TabIndex = 163;
            this.buttonDownloadTec.Text = "下载";
            this.buttonDownloadTec.UseVisualStyleBackColor = true;
            this.buttonDownloadTec.Click += new System.EventHandler(this.buttonDownloadTec_Click);
            // 
            // buttonUploadTec
            // 
            this.buttonUploadTec.Location = new System.Drawing.Point(163, 151);
            this.buttonUploadTec.Name = "buttonUploadTec";
            this.buttonUploadTec.Size = new System.Drawing.Size(60, 30);
            this.buttonUploadTec.TabIndex = 162;
            this.buttonUploadTec.Text = "上传";
            this.buttonUploadTec.UseVisualStyleBackColor = true;
            this.buttonUploadTec.Click += new System.EventHandler(this.buttonUploadTec_Click);
            // 
            // buttonDownloadBid
            // 
            this.buttonDownloadBid.Location = new System.Drawing.Point(251, 93);
            this.buttonDownloadBid.Name = "buttonDownloadBid";
            this.buttonDownloadBid.Size = new System.Drawing.Size(60, 30);
            this.buttonDownloadBid.TabIndex = 161;
            this.buttonDownloadBid.Text = "下载";
            this.buttonDownloadBid.UseVisualStyleBackColor = true;
            this.buttonDownloadBid.Click += new System.EventHandler(this.buttonDownloadBid_Click);
            // 
            // buttonUploadBid
            // 
            this.buttonUploadBid.Location = new System.Drawing.Point(163, 93);
            this.buttonUploadBid.Name = "buttonUploadBid";
            this.buttonUploadBid.Size = new System.Drawing.Size(60, 30);
            this.buttonUploadBid.TabIndex = 160;
            this.buttonUploadBid.Text = "上传";
            this.buttonUploadBid.UseVisualStyleBackColor = true;
            this.buttonUploadBid.Click += new System.EventHandler(this.buttonUploadBid_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(332, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 12);
            this.label6.TabIndex = 159;
            this.label6.Text = "支持doc,doxc,excel,pdf";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(332, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 12);
            this.label4.TabIndex = 157;
            this.label4.Text = "支持doc,doxc,excel,pdf";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 156;
            this.label3.Text = "技术要求";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 155;
            this.label2.Text = "投标要求";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(85, 29);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(293, 19);
            this.label1.TabIndex = 154;
            this.label1.Text = "关于****采购项目一次投标要求";
            // 
            // SubmitTechnical_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 283);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonDownloadTec);
            this.Controls.Add(this.buttonUploadTec);
            this.Controls.Add(this.buttonDownloadBid);
            this.Controls.Add(this.buttonUploadBid);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SubmitTechnical_Form";
            this.Text = "供应商提交技术提案";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonUploadBid;
        private System.Windows.Forms.Button buttonDownloadBid;
        private System.Windows.Forms.Button buttonDownloadTec;
        private System.Windows.Forms.Button buttonUploadTec;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}