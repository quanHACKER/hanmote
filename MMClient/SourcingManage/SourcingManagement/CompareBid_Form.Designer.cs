﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class CompareBid_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BiddingTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txt_transType = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txt_currency = new System.Windows.Forms.TextBox();
            this.txt_timeZone = new System.Windows.Forms.TextBox();
            this.txt_limitTime = new System.Windows.Forms.TextBox();
            this.txt_startBeginTime = new System.Windows.Forms.TextBox();
            this.txt_EndTime = new System.Windows.Forms.TextBox();
            this.txt_StartTime = new System.Windows.Forms.TextBox();
            this.txt_commBidStartDate = new System.Windows.Forms.TextBox();
            this.txt_commBidStartTime = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txt_enrollStartDate = new System.Windows.Forms.TextBox();
            this.txt_enrollStartTime = new System.Windows.Forms.TextBox();
            this.txt_enrollEndDate = new System.Windows.Forms.TextBox();
            this.txt_enrollEndTime = new System.Windows.Forms.TextBox();
            this.txt_buyEndDate = new System.Windows.Forms.TextBox();
            this.txt_buyEndTime = new System.Windows.Forms.TextBox();
            this.txt_techBidStartDate = new System.Windows.Forms.TextBox();
            this.txt_techBidStartTime = new System.Windows.Forms.TextBox();
            this.txt_bidCost = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_purchaseGroup = new System.Windows.Forms.TextBox();
            this.txt_purchaseOrg = new System.Windows.Forms.TextBox();
            this.txt_displayType = new System.Windows.Forms.TextBox();
            this.txt_catalogue = new System.Windows.Forms.TextBox();
            this.txt_serviceType = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.txt_callerName = new System.Windows.Forms.TextBox();
            this.txt_callerId = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_ReviewerName = new System.Windows.Forms.TextBox();
            this.txt_reviewerId = new System.Windows.Forms.TextBox();
            this.txt_dropPointName = new System.Windows.Forms.TextBox();
            this.txt_dropPointId = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txt_receiverName = new System.Windows.Forms.TextBox();
            this.txt_receiverId = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_requesterName = new System.Windows.Forms.TextBox();
            this.txt_requesterId = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_addSupplier = new System.Windows.Forms.Button();
            this.SupplierGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidSupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidSupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidClassificationResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_longBidText = new System.Windows.Forms.RichTextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txt_approvalComment = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txt_shortBidText = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fileGridView = new System.Windows.Forms.DataGridView();
            this.fileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_upload = new System.Windows.Forms.Button();
            this.btn_ChooseFile = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.btn_Transact = new System.Windows.Forms.Button();
            this.tab_EvalMethod = new System.Windows.Forms.TabControl();
            this.tab_Lowprice = new System.Windows.Forms.TabPage();
            this.btn_LowPriceSave = new System.Windows.Forms.Button();
            this.cmb_LPSupplier = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.cmb_LPMaterialId = new System.Windows.Forms.ComboBox();
            this.supplierGridViewLP = new System.Windows.Forms.DataGridView();
            this.supplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_WeightScore = new System.Windows.Forms.TabPage();
            this.gbx_WeightInfo = new System.Windows.Forms.GroupBox();
            this.btn_SureSupplier = new System.Windows.Forms.Button();
            this.resSupplierGridView = new System.Windows.Forms.DataGridView();
            this.resultGridView = new System.Windows.Forms.DataGridView();
            this.btn_WeightSave = new System.Windows.Forms.Button();
            this.tab_LeastTotalCost = new System.Windows.Forms.TabPage();
            this.gbx_leastTotalCost = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LTPsupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.allCostGridView = new System.Windows.Forms.DataGridView();
            this.CostItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.First = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Second = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Third = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fifth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sixth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seventh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eighth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ninth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_ValueAsses = new System.Windows.Forms.TabPage();
            this.txt_EvalMethod = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tab_History = new System.Windows.Forms.TabPage();
            this.supplierHistoryView = new System.Windows.Forms.DataGridView();
            this.bidId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.cmb_AnalySupplier = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tab_Evaluate = new System.Windows.Forms.TabPage();
            this.grp_Condition = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Sure = new System.Windows.Forms.Button();
            this.gb_Supplier = new System.Windows.Forms.GroupBox();
            this.dgv_SupplierList = new System.Windows.Forms.DataGridView();
            this.sSelection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sIndustry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Standard = new System.Windows.Forms.GroupBox();
            this.Price_Score = new System.Windows.Forms.CheckBox();
            this.PriceLevel_Score = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.PriceHistory_Score = new System.Windows.Forms.CheckBox();
            this.Quality_Score = new System.Windows.Forms.CheckBox();
            this.GoodReceipt_Score = new System.Windows.Forms.CheckBox();
            this.QualityAudit_Score = new System.Windows.Forms.CheckBox();
            this.ComplaintAndReject_Score = new System.Windows.Forms.CheckBox();
            this.Delivery_Score = new System.Windows.Forms.CheckBox();
            this.OnTimeDelivery_Score = new System.Windows.Forms.CheckBox();
            this.ConfirmDate_Score = new System.Windows.Forms.CheckBox();
            this.QuantityReliability_Score = new System.Windows.Forms.CheckBox();
            this.Shipment_Score = new System.Windows.Forms.CheckBox();
            this.GeneralServiceAndSupport_Score = new System.Windows.Forms.CheckBox();
            this.ExternalService_Score = new System.Windows.Forms.CheckBox();
            this.lb_SecondStandard = new System.Windows.Forms.Label();
            this.lb_MainStandard = new System.Windows.Forms.Label();
            this.tab_PriceCompare = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.priceGridView = new System.Windows.Forms.DataGridView();
            this.recordInfoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.compareSupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.compareSupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Compare = new System.Windows.Forms.Button();
            this.cmb_MaterialId = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_ShowResult = new System.Windows.Forms.Button();
            this.cmb_ResSupplier = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbp_Condition = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.conditionGridView = new System.Windows.Forms.DataGridView();
            this.conditionNum = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.priceType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.calType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.disType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_NetPrice = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbp_TimeSection = new System.Windows.Forms.TabPage();
            this.timeGridView = new System.Windows.Forms.DataGridView();
            this.startTime = new Lib.Common.CommonControls.CalendarColumn();
            this.endTime = new Lib.Common.CommonControls.CalendarColumn();
            this.timePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbp_NumberSection = new System.Windows.Forms.TabPage();
            this.numberGridView = new System.Windows.Forms.DataGridView();
            this.startNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialGridview = new System.Windows.Forms.DataGridView();
            this.bidMaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidDemandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidDemandMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_BidName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_BidId = new System.Windows.Forms.TextBox();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txt_State = new System.Windows.Forms.TextBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.resSupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3.SuspendLayout();
            this.BiddingTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierGridView)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileGridView)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.tab_EvalMethod.SuspendLayout();
            this.tab_Lowprice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierGridViewLP)).BeginInit();
            this.tab_WeightScore.SuspendLayout();
            this.gbx_WeightInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resSupplierGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultGridView)).BeginInit();
            this.tab_LeastTotalCost.SuspendLayout();
            this.gbx_leastTotalCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allCostGridView)).BeginInit();
            this.tabPage11.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tab_History.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierHistoryView)).BeginInit();
            this.tab_Evaluate.SuspendLayout();
            this.grp_Condition.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gb_Supplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).BeginInit();
            this.gb_Standard.SuspendLayout();
            this.tab_PriceCompare.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.priceGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbp_Condition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).BeginInit();
            this.tbp_TimeSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeGridView)).BeginInit();
            this.tbp_NumberSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialGridview)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BiddingTab);
            this.panel3.Location = new System.Drawing.Point(12, 62);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(877, 542);
            this.panel3.TabIndex = 207;
            // 
            // BiddingTab
            // 
            this.BiddingTab.Controls.Add(this.tabPage1);
            this.BiddingTab.Controls.Add(this.tabPage6);
            this.BiddingTab.Controls.Add(this.tabPage3);
            this.BiddingTab.Controls.Add(this.tabPage4);
            this.BiddingTab.Controls.Add(this.tabPage8);
            this.BiddingTab.Controls.Add(this.tabPage11);
            this.BiddingTab.Controls.Add(this.tabPage10);
            this.BiddingTab.Controls.Add(this.tabPage2);
            this.BiddingTab.Location = new System.Drawing.Point(3, 0);
            this.BiddingTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BiddingTab.Name = "BiddingTab";
            this.BiddingTab.Padding = new System.Drawing.Point(12, 6);
            this.BiddingTab.SelectedIndex = 0;
            this.BiddingTab.Size = new System.Drawing.Size(942, 540);
            this.BiddingTab.TabIndex = 195;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.txt_transType);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.txt_currency);
            this.tabPage1.Controls.Add(this.txt_timeZone);
            this.tabPage1.Controls.Add(this.txt_limitTime);
            this.tabPage1.Controls.Add(this.txt_startBeginTime);
            this.tabPage1.Controls.Add(this.txt_EndTime);
            this.tabPage1.Controls.Add(this.txt_StartTime);
            this.tabPage1.Controls.Add(this.txt_commBidStartDate);
            this.tabPage1.Controls.Add(this.txt_commBidStartTime);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.txt_enrollStartDate);
            this.tabPage1.Controls.Add(this.txt_enrollStartTime);
            this.tabPage1.Controls.Add(this.txt_enrollEndDate);
            this.tabPage1.Controls.Add(this.txt_enrollEndTime);
            this.tabPage1.Controls.Add(this.txt_buyEndDate);
            this.tabPage1.Controls.Add(this.txt_buyEndTime);
            this.tabPage1.Controls.Add(this.txt_techBidStartDate);
            this.tabPage1.Controls.Add(this.txt_techBidStartTime);
            this.tabPage1.Controls.Add(this.txt_bidCost);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txt_purchaseGroup);
            this.tabPage1.Controls.Add(this.txt_purchaseOrg);
            this.tabPage1.Controls.Add(this.txt_displayType);
            this.tabPage1.Controls.Add(this.txt_catalogue);
            this.tabPage1.Controls.Add(this.txt_serviceType);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(934, 508);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本数据";
            // 
            // txt_transType
            // 
            this.txt_transType.Location = new System.Drawing.Point(119, 104);
            this.txt_transType.Name = "txt_transType";
            this.txt_transType.ReadOnly = true;
            this.txt_transType.Size = new System.Drawing.Size(100, 21);
            this.txt_transType.TabIndex = 231;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(23, 109);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 230;
            this.label46.Text = "交易类型";
            // 
            // txt_currency
            // 
            this.txt_currency.Location = new System.Drawing.Point(118, 356);
            this.txt_currency.Name = "txt_currency";
            this.txt_currency.ReadOnly = true;
            this.txt_currency.Size = new System.Drawing.Size(101, 21);
            this.txt_currency.TabIndex = 227;
            // 
            // txt_timeZone
            // 
            this.txt_timeZone.Location = new System.Drawing.Point(118, 320);
            this.txt_timeZone.Name = "txt_timeZone";
            this.txt_timeZone.ReadOnly = true;
            this.txt_timeZone.Size = new System.Drawing.Size(101, 21);
            this.txt_timeZone.TabIndex = 227;
            // 
            // txt_limitTime
            // 
            this.txt_limitTime.Location = new System.Drawing.Point(117, 287);
            this.txt_limitTime.Name = "txt_limitTime";
            this.txt_limitTime.ReadOnly = true;
            this.txt_limitTime.Size = new System.Drawing.Size(101, 21);
            this.txt_limitTime.TabIndex = 227;
            // 
            // txt_startBeginTime
            // 
            this.txt_startBeginTime.Location = new System.Drawing.Point(118, 260);
            this.txt_startBeginTime.Name = "txt_startBeginTime";
            this.txt_startBeginTime.ReadOnly = true;
            this.txt_startBeginTime.Size = new System.Drawing.Size(152, 21);
            this.txt_startBeginTime.TabIndex = 227;
            // 
            // txt_EndTime
            // 
            this.txt_EndTime.Location = new System.Drawing.Point(118, 229);
            this.txt_EndTime.Name = "txt_EndTime";
            this.txt_EndTime.ReadOnly = true;
            this.txt_EndTime.Size = new System.Drawing.Size(152, 21);
            this.txt_EndTime.TabIndex = 227;
            // 
            // txt_StartTime
            // 
            this.txt_StartTime.Location = new System.Drawing.Point(117, 202);
            this.txt_StartTime.Name = "txt_StartTime";
            this.txt_StartTime.ReadOnly = true;
            this.txt_StartTime.Size = new System.Drawing.Size(153, 21);
            this.txt_StartTime.TabIndex = 226;
            // 
            // txt_commBidStartDate
            // 
            this.txt_commBidStartDate.Location = new System.Drawing.Point(627, 45);
            this.txt_commBidStartDate.Name = "txt_commBidStartDate";
            this.txt_commBidStartDate.ReadOnly = true;
            this.txt_commBidStartDate.Size = new System.Drawing.Size(100, 21);
            this.txt_commBidStartDate.TabIndex = 225;
            // 
            // txt_commBidStartTime
            // 
            this.txt_commBidStartTime.Location = new System.Drawing.Point(627, 14);
            this.txt_commBidStartTime.Name = "txt_commBidStartTime";
            this.txt_commBidStartTime.ReadOnly = true;
            this.txt_commBidStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_commBidStartTime.TabIndex = 225;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(510, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(89, 12);
            this.label41.TabIndex = 224;
            this.label41.Text = "商务标开标时间";
            // 
            // txt_enrollStartDate
            // 
            this.txt_enrollStartDate.Location = new System.Drawing.Point(627, 295);
            this.txt_enrollStartDate.Name = "txt_enrollStartDate";
            this.txt_enrollStartDate.ReadOnly = true;
            this.txt_enrollStartDate.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollStartDate.TabIndex = 223;
            // 
            // txt_enrollStartTime
            // 
            this.txt_enrollStartTime.Location = new System.Drawing.Point(627, 262);
            this.txt_enrollStartTime.Name = "txt_enrollStartTime";
            this.txt_enrollStartTime.ReadOnly = true;
            this.txt_enrollStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollStartTime.TabIndex = 223;
            // 
            // txt_enrollEndDate
            // 
            this.txt_enrollEndDate.Location = new System.Drawing.Point(629, 229);
            this.txt_enrollEndDate.Name = "txt_enrollEndDate";
            this.txt_enrollEndDate.ReadOnly = true;
            this.txt_enrollEndDate.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollEndDate.TabIndex = 222;
            // 
            // txt_enrollEndTime
            // 
            this.txt_enrollEndTime.Location = new System.Drawing.Point(627, 196);
            this.txt_enrollEndTime.Name = "txt_enrollEndTime";
            this.txt_enrollEndTime.ReadOnly = true;
            this.txt_enrollEndTime.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollEndTime.TabIndex = 222;
            // 
            // txt_buyEndDate
            // 
            this.txt_buyEndDate.Location = new System.Drawing.Point(627, 169);
            this.txt_buyEndDate.Name = "txt_buyEndDate";
            this.txt_buyEndDate.ReadOnly = true;
            this.txt_buyEndDate.Size = new System.Drawing.Size(100, 21);
            this.txt_buyEndDate.TabIndex = 221;
            // 
            // txt_buyEndTime
            // 
            this.txt_buyEndTime.Location = new System.Drawing.Point(627, 140);
            this.txt_buyEndTime.Name = "txt_buyEndTime";
            this.txt_buyEndTime.ReadOnly = true;
            this.txt_buyEndTime.Size = new System.Drawing.Size(100, 21);
            this.txt_buyEndTime.TabIndex = 221;
            // 
            // txt_techBidStartDate
            // 
            this.txt_techBidStartDate.Location = new System.Drawing.Point(627, 106);
            this.txt_techBidStartDate.Name = "txt_techBidStartDate";
            this.txt_techBidStartDate.ReadOnly = true;
            this.txt_techBidStartDate.Size = new System.Drawing.Size(100, 21);
            this.txt_techBidStartDate.TabIndex = 220;
            // 
            // txt_techBidStartTime
            // 
            this.txt_techBidStartTime.Location = new System.Drawing.Point(627, 75);
            this.txt_techBidStartTime.Name = "txt_techBidStartTime";
            this.txt_techBidStartTime.ReadOnly = true;
            this.txt_techBidStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_techBidStartTime.TabIndex = 220;
            // 
            // txt_bidCost
            // 
            this.txt_bidCost.Location = new System.Drawing.Point(627, 331);
            this.txt_bidCost.Name = "txt_bidCost";
            this.txt_bidCost.ReadOnly = true;
            this.txt_bidCost.Size = new System.Drawing.Size(100, 21);
            this.txt_bidCost.TabIndex = 213;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(512, 331);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 212;
            this.label23.Text = "投标费";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(512, 302);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 12);
            this.label22.TabIndex = 211;
            this.label22.Text = "报名开始日期";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(510, 266);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 210;
            this.label21.Text = "报名开始时间";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(510, 238);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 12);
            this.label20.TabIndex = 209;
            this.label20.Text = "报名结束日期";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(510, 206);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 208;
            this.label19.Text = "报名结束时间";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(510, 178);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 12);
            this.label18.TabIndex = 206;
            this.label18.Text = "购买标书的截至日期";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(512, 144);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 12);
            this.label17.TabIndex = 204;
            this.label17.Text = "购买标书的截止时间";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(510, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 202;
            this.label4.Text = "技术标开标日期";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 200;
            this.label2.Text = "技术标开标时间";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(510, 54);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 199;
            this.label1.Text = "商务标开标日期";
            // 
            // txt_purchaseGroup
            // 
            this.txt_purchaseGroup.Location = new System.Drawing.Point(119, 168);
            this.txt_purchaseGroup.Name = "txt_purchaseGroup";
            this.txt_purchaseGroup.ReadOnly = true;
            this.txt_purchaseGroup.Size = new System.Drawing.Size(100, 21);
            this.txt_purchaseGroup.TabIndex = 196;
            // 
            // txt_purchaseOrg
            // 
            this.txt_purchaseOrg.Location = new System.Drawing.Point(119, 134);
            this.txt_purchaseOrg.Name = "txt_purchaseOrg";
            this.txt_purchaseOrg.ReadOnly = true;
            this.txt_purchaseOrg.Size = new System.Drawing.Size(100, 21);
            this.txt_purchaseOrg.TabIndex = 196;
            // 
            // txt_displayType
            // 
            this.txt_displayType.Location = new System.Drawing.Point(118, 76);
            this.txt_displayType.Name = "txt_displayType";
            this.txt_displayType.ReadOnly = true;
            this.txt_displayType.Size = new System.Drawing.Size(100, 21);
            this.txt_displayType.TabIndex = 196;
            // 
            // txt_catalogue
            // 
            this.txt_catalogue.Location = new System.Drawing.Point(117, 47);
            this.txt_catalogue.Name = "txt_catalogue";
            this.txt_catalogue.ReadOnly = true;
            this.txt_catalogue.Size = new System.Drawing.Size(100, 21);
            this.txt_catalogue.TabIndex = 196;
            // 
            // txt_serviceType
            // 
            this.txt_serviceType.Location = new System.Drawing.Point(117, 20);
            this.txt_serviceType.Name = "txt_serviceType";
            this.txt_serviceType.ReadOnly = true;
            this.txt_serviceType.Size = new System.Drawing.Size(100, 21);
            this.txt_serviceType.TabIndex = 196;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(23, 359);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 193;
            this.label15.Text = "货币";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(23, 326);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 191;
            this.label16.Text = "时区";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(23, 266);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 189;
            this.label14.Text = "起始日期";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(23, 206);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 187;
            this.label13.Text = "开始日期";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(23, 294);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 183;
            this.label11.Text = "限定期段的结束";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(23, 172);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 181;
            this.label10.Text = "采购组";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(23, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 179;
            this.label8.Text = "采购组织";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(22, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 177;
            this.label9.Text = "发布类型";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(22, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 175;
            this.label7.Text = "产品目录";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(23, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 173;
            this.label6.Text = "结束日期";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(22, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 171;
            this.label5.Text = "业务类型";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.txt_callerName);
            this.tabPage6.Controls.Add(this.txt_callerId);
            this.tabPage6.Controls.Add(this.label40);
            this.tabPage6.Controls.Add(this.txt_ReviewerName);
            this.tabPage6.Controls.Add(this.txt_reviewerId);
            this.tabPage6.Controls.Add(this.txt_dropPointName);
            this.tabPage6.Controls.Add(this.txt_dropPointId);
            this.tabPage6.Controls.Add(this.label39);
            this.tabPage6.Controls.Add(this.label38);
            this.tabPage6.Controls.Add(this.txt_receiverName);
            this.tabPage6.Controls.Add(this.txt_receiverId);
            this.tabPage6.Controls.Add(this.label37);
            this.tabPage6.Controls.Add(this.txt_requesterName);
            this.tabPage6.Controls.Add(this.txt_requesterId);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.label35);
            this.tabPage6.Controls.Add(this.label34);
            this.tabPage6.Controls.Add(this.label33);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(934, 508);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "合作伙伴";
            // 
            // txt_callerName
            // 
            this.txt_callerName.Location = new System.Drawing.Point(423, 254);
            this.txt_callerName.Name = "txt_callerName";
            this.txt_callerName.ReadOnly = true;
            this.txt_callerName.Size = new System.Drawing.Size(100, 21);
            this.txt_callerName.TabIndex = 15;
            // 
            // txt_callerId
            // 
            this.txt_callerId.Location = new System.Drawing.Point(205, 254);
            this.txt_callerId.Name = "txt_callerId";
            this.txt_callerId.ReadOnly = true;
            this.txt_callerId.Size = new System.Drawing.Size(100, 21);
            this.txt_callerId.TabIndex = 14;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(71, 263);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 12);
            this.label40.TabIndex = 13;
            this.label40.Text = "开标人";
            // 
            // txt_ReviewerName
            // 
            this.txt_ReviewerName.Location = new System.Drawing.Point(423, 212);
            this.txt_ReviewerName.Name = "txt_ReviewerName";
            this.txt_ReviewerName.ReadOnly = true;
            this.txt_ReviewerName.Size = new System.Drawing.Size(100, 21);
            this.txt_ReviewerName.TabIndex = 12;
            // 
            // txt_reviewerId
            // 
            this.txt_reviewerId.Location = new System.Drawing.Point(205, 212);
            this.txt_reviewerId.Name = "txt_reviewerId";
            this.txt_reviewerId.ReadOnly = true;
            this.txt_reviewerId.Size = new System.Drawing.Size(100, 21);
            this.txt_reviewerId.TabIndex = 12;
            // 
            // txt_dropPointName
            // 
            this.txt_dropPointName.Location = new System.Drawing.Point(423, 168);
            this.txt_dropPointName.Name = "txt_dropPointName";
            this.txt_dropPointName.ReadOnly = true;
            this.txt_dropPointName.Size = new System.Drawing.Size(100, 21);
            this.txt_dropPointName.TabIndex = 11;
            // 
            // txt_dropPointId
            // 
            this.txt_dropPointId.Location = new System.Drawing.Point(205, 165);
            this.txt_dropPointId.Name = "txt_dropPointId";
            this.txt_dropPointId.ReadOnly = true;
            this.txt_dropPointId.Size = new System.Drawing.Size(100, 21);
            this.txt_dropPointId.TabIndex = 10;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(71, 221);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 9;
            this.label39.Text = "评审专家";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(75, 168);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 9;
            this.label38.Text = "交货点";
            // 
            // txt_receiverName
            // 
            this.txt_receiverName.Location = new System.Drawing.Point(423, 120);
            this.txt_receiverName.Name = "txt_receiverName";
            this.txt_receiverName.ReadOnly = true;
            this.txt_receiverName.Size = new System.Drawing.Size(100, 21);
            this.txt_receiverName.TabIndex = 8;
            // 
            // txt_receiverId
            // 
            this.txt_receiverId.Location = new System.Drawing.Point(205, 120);
            this.txt_receiverId.Name = "txt_receiverId";
            this.txt_receiverId.ReadOnly = true;
            this.txt_receiverId.Size = new System.Drawing.Size(100, 21);
            this.txt_receiverId.TabIndex = 7;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(73, 130);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 12);
            this.label37.TabIndex = 6;
            this.label37.Text = "收货人";
            // 
            // txt_requesterName
            // 
            this.txt_requesterName.Location = new System.Drawing.Point(423, 79);
            this.txt_requesterName.Name = "txt_requesterName";
            this.txt_requesterName.ReadOnly = true;
            this.txt_requesterName.Size = new System.Drawing.Size(100, 21);
            this.txt_requesterName.TabIndex = 5;
            // 
            // txt_requesterId
            // 
            this.txt_requesterId.Location = new System.Drawing.Point(205, 79);
            this.txt_requesterId.Name = "txt_requesterId";
            this.txt_requesterId.ReadOnly = true;
            this.txt_requesterId.Size = new System.Drawing.Size(100, 21);
            this.txt_requesterId.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(452, 42);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 12);
            this.label36.TabIndex = 3;
            this.label36.Text = "名称";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(245, 43);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 12);
            this.label35.TabIndex = 2;
            this.label35.Text = "编号";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(69, 43);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 1;
            this.label34.Text = "功能";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(71, 82);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 0;
            this.label33.Text = "请求者";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.btn_addSupplier);
            this.tabPage3.Controls.Add(this.SupplierGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(934, 508);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "投标人";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(124, 297);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 180;
            this.button3.Text = "删除投标人";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btn_addSupplier
            // 
            this.btn_addSupplier.Location = new System.Drawing.Point(22, 297);
            this.btn_addSupplier.Name = "btn_addSupplier";
            this.btn_addSupplier.Size = new System.Drawing.Size(75, 23);
            this.btn_addSupplier.TabIndex = 179;
            this.btn_addSupplier.Text = "添加投标人";
            this.btn_addSupplier.UseVisualStyleBackColor = true;
            // 
            // SupplierGridView
            // 
            this.SupplierGridView.BackgroundColor = System.Drawing.Color.White;
            this.SupplierGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SupplierGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.bidSupplierId,
            this.bidSupplierName,
            this.bidClassificationResult,
            this.bidContact,
            this.bidEmail,
            this.bidAddress,
            this.bidState});
            this.SupplierGridView.Location = new System.Drawing.Point(22, 24);
            this.SupplierGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SupplierGridView.Name = "SupplierGridView";
            this.SupplierGridView.RowHeadersVisible = false;
            this.SupplierGridView.RowTemplate.Height = 23;
            this.SupplierGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SupplierGridView.Size = new System.Drawing.Size(845, 240);
            this.SupplierGridView.TabIndex = 178;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn26.Frozen = true;
            this.dataGridViewTextBoxColumn26.HeaderText = "";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn26.Width = 30;
            // 
            // bidSupplierId
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.bidSupplierId.DefaultCellStyle = dataGridViewCellStyle2;
            this.bidSupplierId.Frozen = true;
            this.bidSupplierId.HeaderText = "供应商编号";
            this.bidSupplierId.Name = "bidSupplierId";
            this.bidSupplierId.ReadOnly = true;
            this.bidSupplierId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // bidSupplierName
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.bidSupplierName.DefaultCellStyle = dataGridViewCellStyle3;
            this.bidSupplierName.Frozen = true;
            this.bidSupplierName.HeaderText = "供应商名称";
            this.bidSupplierName.Name = "bidSupplierName";
            this.bidSupplierName.ReadOnly = true;
            this.bidSupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidSupplierName.Width = 120;
            // 
            // bidClassificationResult
            // 
            this.bidClassificationResult.Frozen = true;
            this.bidClassificationResult.HeaderText = "供应商评级";
            this.bidClassificationResult.Name = "bidClassificationResult";
            // 
            // bidContact
            // 
            this.bidContact.Frozen = true;
            this.bidContact.HeaderText = "联系人";
            this.bidContact.Name = "bidContact";
            this.bidContact.ReadOnly = true;
            this.bidContact.Width = 95;
            // 
            // bidEmail
            // 
            this.bidEmail.Frozen = true;
            this.bidEmail.HeaderText = "电子信箱";
            this.bidEmail.Name = "bidEmail";
            this.bidEmail.ReadOnly = true;
            // 
            // bidAddress
            // 
            this.bidAddress.Frozen = true;
            this.bidAddress.HeaderText = "地址";
            this.bidAddress.Name = "bidAddress";
            this.bidAddress.ReadOnly = true;
            this.bidAddress.Width = 130;
            // 
            // bidState
            // 
            this.bidState.HeaderText = "投标状态";
            this.bidState.Name = "bidState";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(934, 508);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "凭证";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Location = new System.Drawing.Point(517, 310);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(417, 170);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "协同";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(32, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "创建";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_longBidText);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.txt_approvalComment);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.txt_shortBidText);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Location = new System.Drawing.Point(48, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(886, 271);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "文本总览";
            // 
            // txt_longBidText
            // 
            this.txt_longBidText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_longBidText.Location = new System.Drawing.Point(215, 145);
            this.txt_longBidText.Name = "txt_longBidText";
            this.txt_longBidText.Size = new System.Drawing.Size(611, 96);
            this.txt_longBidText.TabIndex = 7;
            this.txt_longBidText.Text = "";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(26, 148);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 6;
            this.label32.Text = "投标文本";
            // 
            // txt_approvalComment
            // 
            this.txt_approvalComment.Location = new System.Drawing.Point(215, 105);
            this.txt_approvalComment.Name = "txt_approvalComment";
            this.txt_approvalComment.Size = new System.Drawing.Size(611, 21);
            this.txt_approvalComment.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(28, 105);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 4;
            this.label31.Text = "批准注释";
            // 
            // txt_shortBidText
            // 
            this.txt_shortBidText.Location = new System.Drawing.Point(215, 65);
            this.txt_shortBidText.Name = "txt_shortBidText";
            this.txt_shortBidText.Size = new System.Drawing.Size(611, 21);
            this.txt_shortBidText.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(28, 68);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 2;
            this.label30.Text = "投标文本";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(213, 33);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 1;
            this.label29.Text = "文本预览";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(28, 33);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "文本类型";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.fileGridView);
            this.groupBox1.Controls.Add(this.btn_upload);
            this.groupBox1.Controls.Add(this.btn_ChooseFile);
            this.groupBox1.Location = new System.Drawing.Point(48, 310);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(463, 227);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "附件";
            // 
            // fileGridView
            // 
            this.fileGridView.AllowUserToAddRows = false;
            this.fileGridView.AllowUserToResizeColumns = false;
            this.fileGridView.AllowUserToResizeRows = false;
            this.fileGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileGridView.ColumnHeadersHeight = 21;
            this.fileGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.fileGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileName,
            this.fileSize});
            this.fileGridView.EnableHeadersVisualStyles = false;
            this.fileGridView.Location = new System.Drawing.Point(10, 17);
            this.fileGridView.Name = "fileGridView";
            this.fileGridView.RowTemplate.Height = 23;
            this.fileGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.fileGridView.Size = new System.Drawing.Size(364, 150);
            this.fileGridView.TabIndex = 3;
            // 
            // fileName
            // 
            this.fileName.HeaderText = "文件名称";
            this.fileName.Name = "fileName";
            this.fileName.Width = 180;
            // 
            // fileSize
            // 
            this.fileSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fileSize.HeaderText = "文件大小";
            this.fileSize.Name = "fileSize";
            // 
            // btn_upload
            // 
            this.btn_upload.Location = new System.Drawing.Point(380, 144);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(75, 23);
            this.btn_upload.TabIndex = 1;
            this.btn_upload.Text = "上传";
            this.btn_upload.UseVisualStyleBackColor = true;
            // 
            // btn_ChooseFile
            // 
            this.btn_ChooseFile.Location = new System.Drawing.Point(382, 20);
            this.btn_ChooseFile.Name = "btn_ChooseFile";
            this.btn_ChooseFile.Size = new System.Drawing.Size(75, 23);
            this.btn_ChooseFile.TabIndex = 2;
            this.btn_ChooseFile.Text = "选择文件";
            this.btn_ChooseFile.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage8.Controls.Add(this.btn_Transact);
            this.tabPage8.Controls.Add(this.tab_EvalMethod);
            this.tabPage8.Controls.Add(this.txt_EvalMethod);
            this.tabPage8.Controls.Add(this.label27);
            this.tabPage8.Location = new System.Drawing.Point(4, 28);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(934, 508);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "评价标准";
            // 
            // btn_Transact
            // 
            this.btn_Transact.Location = new System.Drawing.Point(694, 13);
            this.btn_Transact.Name = "btn_Transact";
            this.btn_Transact.Size = new System.Drawing.Size(75, 23);
            this.btn_Transact.TabIndex = 4;
            this.btn_Transact.Text = "进入谈判";
            this.btn_Transact.UseVisualStyleBackColor = true;
            this.btn_Transact.Click += new System.EventHandler(this.btn_Transact_Click);
            // 
            // tab_EvalMethod
            // 
            this.tab_EvalMethod.Controls.Add(this.tab_Lowprice);
            this.tab_EvalMethod.Controls.Add(this.tab_WeightScore);
            this.tab_EvalMethod.Controls.Add(this.tab_LeastTotalCost);
            this.tab_EvalMethod.Controls.Add(this.tab_ValueAsses);
            this.tab_EvalMethod.Location = new System.Drawing.Point(24, 51);
            this.tab_EvalMethod.Name = "tab_EvalMethod";
            this.tab_EvalMethod.SelectedIndex = 0;
            this.tab_EvalMethod.Size = new System.Drawing.Size(880, 454);
            this.tab_EvalMethod.TabIndex = 7;
            // 
            // tab_Lowprice
            // 
            this.tab_Lowprice.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Lowprice.Controls.Add(this.btn_LowPriceSave);
            this.tab_Lowprice.Controls.Add(this.cmb_LPSupplier);
            this.tab_Lowprice.Controls.Add(this.label52);
            this.tab_Lowprice.Controls.Add(this.button4);
            this.tab_Lowprice.Controls.Add(this.label51);
            this.tab_Lowprice.Controls.Add(this.cmb_LPMaterialId);
            this.tab_Lowprice.Controls.Add(this.supplierGridViewLP);
            this.tab_Lowprice.Location = new System.Drawing.Point(4, 22);
            this.tab_Lowprice.Name = "tab_Lowprice";
            this.tab_Lowprice.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Lowprice.Size = new System.Drawing.Size(872, 428);
            this.tab_Lowprice.TabIndex = 0;
            this.tab_Lowprice.Text = "最低价格法";
            // 
            // btn_LowPriceSave
            // 
            this.btn_LowPriceSave.Location = new System.Drawing.Point(619, 271);
            this.btn_LowPriceSave.Name = "btn_LowPriceSave";
            this.btn_LowPriceSave.Size = new System.Drawing.Size(75, 23);
            this.btn_LowPriceSave.TabIndex = 9;
            this.btn_LowPriceSave.Text = "保存";
            this.btn_LowPriceSave.UseVisualStyleBackColor = true;
            this.btn_LowPriceSave.Click += new System.EventHandler(this.btn_LowPriceSave_Click);
            // 
            // cmb_LPSupplier
            // 
            this.cmb_LPSupplier.FormattingEnabled = true;
            this.cmb_LPSupplier.Location = new System.Drawing.Point(457, 274);
            this.cmb_LPSupplier.Name = "cmb_LPSupplier";
            this.cmb_LPSupplier.Size = new System.Drawing.Size(121, 20);
            this.cmb_LPSupplier.TabIndex = 8;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(410, 277);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 12);
            this.label52.TabIndex = 7;
            this.label52.Text = "供应商";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(301, 34);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "查看";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(57, 39);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(53, 12);
            this.label51.TabIndex = 5;
            this.label51.Text = "物料编号";
            // 
            // cmb_LPMaterialId
            // 
            this.cmb_LPMaterialId.FormattingEnabled = true;
            this.cmb_LPMaterialId.Location = new System.Drawing.Point(129, 36);
            this.cmb_LPMaterialId.Name = "cmb_LPMaterialId";
            this.cmb_LPMaterialId.Size = new System.Drawing.Size(121, 20);
            this.cmb_LPMaterialId.TabIndex = 4;
            // 
            // supplierGridViewLP
            // 
            this.supplierGridViewLP.AllowUserToAddRows = false;
            this.supplierGridViewLP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.supplierGridViewLP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supplierId,
            this.supplierCount,
            this.supplierPrice,
            this.supplierTotal,
            this.rate});
            this.supplierGridViewLP.Location = new System.Drawing.Point(59, 91);
            this.supplierGridViewLP.Name = "supplierGridViewLP";
            this.supplierGridViewLP.RowTemplate.Height = 23;
            this.supplierGridViewLP.Size = new System.Drawing.Size(635, 150);
            this.supplierGridViewLP.TabIndex = 3;
            // 
            // supplierId
            // 
            this.supplierId.HeaderText = "供应商编号";
            this.supplierId.Name = "supplierId";
            // 
            // supplierCount
            // 
            this.supplierCount.HeaderText = "供应数量";
            this.supplierCount.Name = "supplierCount";
            // 
            // supplierPrice
            // 
            this.supplierPrice.HeaderText = "供应商价格";
            this.supplierPrice.Name = "supplierPrice";
            // 
            // supplierTotal
            // 
            this.supplierTotal.HeaderText = "供应总价";
            this.supplierTotal.Name = "supplierTotal";
            // 
            // rate
            // 
            this.rate.HeaderText = "占比";
            this.rate.Name = "rate";
            // 
            // tab_WeightScore
            // 
            this.tab_WeightScore.BackColor = System.Drawing.SystemColors.Control;
            this.tab_WeightScore.Controls.Add(this.gbx_WeightInfo);
            this.tab_WeightScore.Location = new System.Drawing.Point(4, 22);
            this.tab_WeightScore.Name = "tab_WeightScore";
            this.tab_WeightScore.Padding = new System.Windows.Forms.Padding(3);
            this.tab_WeightScore.Size = new System.Drawing.Size(872, 428);
            this.tab_WeightScore.TabIndex = 1;
            this.tab_WeightScore.Text = "加权评分法";
            // 
            // gbx_WeightInfo
            // 
            this.gbx_WeightInfo.BackColor = System.Drawing.SystemColors.Control;
            this.gbx_WeightInfo.Controls.Add(this.btn_SureSupplier);
            this.gbx_WeightInfo.Controls.Add(this.resSupplierGridView);
            this.gbx_WeightInfo.Controls.Add(this.resultGridView);
            this.gbx_WeightInfo.Controls.Add(this.btn_WeightSave);
            this.gbx_WeightInfo.Location = new System.Drawing.Point(9, 9);
            this.gbx_WeightInfo.Name = "gbx_WeightInfo";
            this.gbx_WeightInfo.Size = new System.Drawing.Size(833, 413);
            this.gbx_WeightInfo.TabIndex = 4;
            this.gbx_WeightInfo.TabStop = false;
            // 
            // btn_SureSupplier
            // 
            this.btn_SureSupplier.Location = new System.Drawing.Point(625, 272);
            this.btn_SureSupplier.Name = "btn_SureSupplier";
            this.btn_SureSupplier.Size = new System.Drawing.Size(75, 23);
            this.btn_SureSupplier.TabIndex = 3;
            this.btn_SureSupplier.Text = "确定供应商";
            this.btn_SureSupplier.UseVisualStyleBackColor = true;
            this.btn_SureSupplier.Click += new System.EventHandler(this.btn_SureSupplier_Click);
            // 
            // resSupplierGridView
            // 
            this.resSupplierGridView.AllowUserToAddRows = false;
            this.resSupplierGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resSupplierGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num,
            this.resSupplierId});
            this.resSupplierGridView.Location = new System.Drawing.Point(534, 20);
            this.resSupplierGridView.Name = "resSupplierGridView";
            this.resSupplierGridView.RowTemplate.Height = 23;
            this.resSupplierGridView.Size = new System.Drawing.Size(293, 158);
            this.resSupplierGridView.TabIndex = 2;
            // 
            // resultGridView
            // 
            this.resultGridView.AllowUserToAddRows = false;
            this.resultGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultGridView.Location = new System.Drawing.Point(6, 20);
            this.resultGridView.Name = "resultGridView";
            this.resultGridView.RowTemplate.Height = 23;
            this.resultGridView.Size = new System.Drawing.Size(459, 275);
            this.resultGridView.TabIndex = 0;
            // 
            // btn_WeightSave
            // 
            this.btn_WeightSave.Location = new System.Drawing.Point(737, 272);
            this.btn_WeightSave.Name = "btn_WeightSave";
            this.btn_WeightSave.Size = new System.Drawing.Size(75, 23);
            this.btn_WeightSave.TabIndex = 1;
            this.btn_WeightSave.Text = "保存";
            this.btn_WeightSave.UseVisualStyleBackColor = true;
            this.btn_WeightSave.Click += new System.EventHandler(this.btn_WeightSave_Click);
            // 
            // tab_LeastTotalCost
            // 
            this.tab_LeastTotalCost.BackColor = System.Drawing.SystemColors.Control;
            this.tab_LeastTotalCost.Controls.Add(this.gbx_leastTotalCost);
            this.tab_LeastTotalCost.Location = new System.Drawing.Point(4, 22);
            this.tab_LeastTotalCost.Name = "tab_LeastTotalCost";
            this.tab_LeastTotalCost.Padding = new System.Windows.Forms.Padding(3);
            this.tab_LeastTotalCost.Size = new System.Drawing.Size(872, 428);
            this.tab_LeastTotalCost.TabIndex = 2;
            this.tab_LeastTotalCost.Text = "最低所有权总成本法";
            // 
            // gbx_leastTotalCost
            // 
            this.gbx_leastTotalCost.Controls.Add(this.dataGridView1);
            this.gbx_leastTotalCost.Controls.Add(this.button2);
            this.gbx_leastTotalCost.Controls.Add(this.allCostGridView);
            this.gbx_leastTotalCost.Location = new System.Drawing.Point(-4, 6);
            this.gbx_leastTotalCost.Name = "gbx_leastTotalCost";
            this.gbx_leastTotalCost.Size = new System.Drawing.Size(843, 416);
            this.gbx_leastTotalCost.TabIndex = 10;
            this.gbx_leastTotalCost.TabStop = false;
            this.gbx_leastTotalCost.Text = "最低所有权总成本";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LTPsupplierId,
            this.totalPrice});
            this.dataGridView1.Location = new System.Drawing.Point(164, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(369, 125);
            this.dataGridView1.TabIndex = 19;
            // 
            // LTPsupplierId
            // 
            this.LTPsupplierId.HeaderText = "供应商编号";
            this.LTPsupplierId.Name = "LTPsupplierId";
            // 
            // totalPrice
            // 
            this.totalPrice.HeaderText = "总价格";
            this.totalPrice.Name = "totalPrice";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(715, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // allCostGridView
            // 
            this.allCostGridView.AllowUserToAddRows = false;
            this.allCostGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.allCostGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allCostGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CostItem,
            this.First,
            this.Second,
            this.Third,
            this.forth,
            this.fifth,
            this.sixth,
            this.seventh,
            this.eighth,
            this.ninth,
            this.tenth});
            this.allCostGridView.Location = new System.Drawing.Point(10, 155);
            this.allCostGridView.Name = "allCostGridView";
            this.allCostGridView.RowTemplate.Height = 23;
            this.allCostGridView.Size = new System.Drawing.Size(830, 232);
            this.allCostGridView.TabIndex = 10;
            // 
            // CostItem
            // 
            this.CostItem.HeaderText = "成本项";
            this.CostItem.Name = "CostItem";
            this.CostItem.Width = 80;
            // 
            // First
            // 
            this.First.HeaderText = "第一年";
            this.First.Name = "First";
            this.First.Width = 70;
            // 
            // Second
            // 
            this.Second.HeaderText = "第二年";
            this.Second.Name = "Second";
            this.Second.Width = 70;
            // 
            // Third
            // 
            this.Third.HeaderText = "第三年";
            this.Third.Name = "Third";
            this.Third.Width = 70;
            // 
            // forth
            // 
            this.forth.HeaderText = "第四年";
            this.forth.Name = "forth";
            this.forth.Width = 70;
            // 
            // fifth
            // 
            this.fifth.HeaderText = "第五年";
            this.fifth.Name = "fifth";
            this.fifth.Width = 70;
            // 
            // sixth
            // 
            this.sixth.HeaderText = "第六年";
            this.sixth.Name = "sixth";
            this.sixth.Width = 70;
            // 
            // seventh
            // 
            this.seventh.HeaderText = "第七年";
            this.seventh.Name = "seventh";
            this.seventh.Width = 70;
            // 
            // eighth
            // 
            this.eighth.HeaderText = "第八年";
            this.eighth.Name = "eighth";
            this.eighth.Width = 70;
            // 
            // ninth
            // 
            this.ninth.HeaderText = "第九年";
            this.ninth.Name = "ninth";
            this.ninth.Width = 70;
            // 
            // tenth
            // 
            this.tenth.HeaderText = "第十年";
            this.tenth.Name = "tenth";
            this.tenth.Width = 70;
            // 
            // tab_ValueAsses
            // 
            this.tab_ValueAsses.BackColor = System.Drawing.SystemColors.Control;
            this.tab_ValueAsses.Location = new System.Drawing.Point(4, 22);
            this.tab_ValueAsses.Name = "tab_ValueAsses";
            this.tab_ValueAsses.Size = new System.Drawing.Size(872, 428);
            this.tab_ValueAsses.TabIndex = 3;
            this.tab_ValueAsses.Text = "价值评估法";
            // 
            // txt_EvalMethod
            // 
            this.txt_EvalMethod.Location = new System.Drawing.Point(87, 15);
            this.txt_EvalMethod.Name = "txt_EvalMethod";
            this.txt_EvalMethod.ReadOnly = true;
            this.txt_EvalMethod.Size = new System.Drawing.Size(125, 21);
            this.txt_EvalMethod.TabIndex = 6;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(22, 18);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 5;
            this.label27.Text = "评估方法";
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage11.Controls.Add(this.tabControl2);
            this.tabPage11.Location = new System.Drawing.Point(4, 28);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(934, 508);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "分析";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tab_History);
            this.tabControl2.Controls.Add(this.tab_Evaluate);
            this.tabControl2.Controls.Add(this.tab_PriceCompare);
            this.tabControl2.Location = new System.Drawing.Point(6, 13);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(864, 475);
            this.tabControl2.TabIndex = 4;
            // 
            // tab_History
            // 
            this.tab_History.BackColor = System.Drawing.SystemColors.Control;
            this.tab_History.Controls.Add(this.supplierHistoryView);
            this.tab_History.Controls.Add(this.button5);
            this.tab_History.Controls.Add(this.cmb_AnalySupplier);
            this.tab_History.Controls.Add(this.label24);
            this.tab_History.Location = new System.Drawing.Point(4, 22);
            this.tab_History.Name = "tab_History";
            this.tab_History.Padding = new System.Windows.Forms.Padding(3);
            this.tab_History.Size = new System.Drawing.Size(856, 449);
            this.tab_History.TabIndex = 0;
            this.tab_History.Text = "投标人历史";
            // 
            // supplierHistoryView
            // 
            this.supplierHistoryView.AllowUserToAddRows = false;
            this.supplierHistoryView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.supplierHistoryView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bidId,
            this.bidName,
            this.CreateTime,
            this.bidResult});
            this.supplierHistoryView.Location = new System.Drawing.Point(76, 83);
            this.supplierHistoryView.Name = "supplierHistoryView";
            this.supplierHistoryView.RowTemplate.Height = 23;
            this.supplierHistoryView.Size = new System.Drawing.Size(643, 295);
            this.supplierHistoryView.TabIndex = 3;
            // 
            // bidId
            // 
            this.bidId.HeaderText = "招标编号";
            this.bidId.Name = "bidId";
            this.bidId.Width = 120;
            // 
            // bidName
            // 
            this.bidName.HeaderText = "招标名称";
            this.bidName.Name = "bidName";
            // 
            // CreateTime
            // 
            this.CreateTime.HeaderText = "创建时间";
            this.CreateTime.Name = "CreateTime";
            this.CreateTime.Width = 120;
            // 
            // bidResult
            // 
            this.bidResult.HeaderText = "招标结果";
            this.bidResult.Name = "bidResult";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(391, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "查看";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cmb_AnalySupplier
            // 
            this.cmb_AnalySupplier.FormattingEnabled = true;
            this.cmb_AnalySupplier.Location = new System.Drawing.Point(226, 21);
            this.cmb_AnalySupplier.Name = "cmb_AnalySupplier";
            this.cmb_AnalySupplier.Size = new System.Drawing.Size(121, 20);
            this.cmb_AnalySupplier.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(158, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "供应商编号";
            // 
            // tab_Evaluate
            // 
            this.tab_Evaluate.Controls.Add(this.grp_Condition);
            this.tab_Evaluate.Location = new System.Drawing.Point(4, 22);
            this.tab_Evaluate.Name = "tab_Evaluate";
            this.tab_Evaluate.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Evaluate.Size = new System.Drawing.Size(856, 449);
            this.tab_Evaluate.TabIndex = 1;
            this.tab_Evaluate.Text = "供应商评估";
            this.tab_Evaluate.UseVisualStyleBackColor = true;
            // 
            // grp_Condition
            // 
            this.grp_Condition.Controls.Add(this.groupBox5);
            this.grp_Condition.Location = new System.Drawing.Point(-84, -27);
            this.grp_Condition.Name = "grp_Condition";
            this.grp_Condition.Size = new System.Drawing.Size(1024, 503);
            this.grp_Condition.TabIndex = 4;
            this.grp_Condition.TabStop = false;
            this.grp_Condition.Text = "条件设置";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn_Clear);
            this.groupBox5.Controls.Add(this.btn_Sure);
            this.groupBox5.Controls.Add(this.gb_Supplier);
            this.groupBox5.Controls.Add(this.gb_Standard);
            this.groupBox5.Location = new System.Drawing.Point(91, 34);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(843, 436);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "条件设置";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(751, 397);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 23);
            this.btn_Clear.TabIndex = 38;
            this.btn_Clear.Text = "xiaochu";
            this.btn_Clear.UseVisualStyleBackColor = true;
            // 
            // btn_Sure
            // 
            this.btn_Sure.Location = new System.Drawing.Point(648, 397);
            this.btn_Sure.Name = "btn_Sure";
            this.btn_Sure.Size = new System.Drawing.Size(75, 23);
            this.btn_Sure.TabIndex = 37;
            this.btn_Sure.Text = "queding";
            this.btn_Sure.UseVisualStyleBackColor = true;
            this.btn_Sure.Click += new System.EventHandler(this.btn_Sure_Click);
            // 
            // gb_Supplier
            // 
            this.gb_Supplier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Supplier.Controls.Add(this.dgv_SupplierList);
            this.gb_Supplier.Location = new System.Drawing.Point(269, 19);
            this.gb_Supplier.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Name = "gb_Supplier";
            this.gb_Supplier.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Size = new System.Drawing.Size(559, 367);
            this.gb_Supplier.TabIndex = 36;
            this.gb_Supplier.TabStop = false;
            this.gb_Supplier.Text = "选择供应商";
            // 
            // dgv_SupplierList
            // 
            this.dgv_SupplierList.AllowUserToAddRows = false;
            this.dgv_SupplierList.AllowUserToDeleteRows = false;
            this.dgv_SupplierList.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_SupplierList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_SupplierList.ColumnHeadersHeight = 25;
            this.dgv_SupplierList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sSelection,
            this.sId,
            this.sName,
            this.sIndustry,
            this.sAddress});
            this.dgv_SupplierList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierList.EnableHeadersVisualStyles = false;
            this.dgv_SupplierList.Location = new System.Drawing.Point(2, 16);
            this.dgv_SupplierList.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_SupplierList.MultiSelect = false;
            this.dgv_SupplierList.Name = "dgv_SupplierList";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierList.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_SupplierList.RowHeadersWidth = 47;
            this.dgv_SupplierList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.Height = 27;
            this.dgv_SupplierList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierList.Size = new System.Drawing.Size(555, 349);
            this.dgv_SupplierList.TabIndex = 35;
            // 
            // sSelection
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = false;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.sSelection.DefaultCellStyle = dataGridViewCellStyle5;
            this.sSelection.HeaderText = "OK";
            this.sSelection.Name = "sSelection";
            this.sSelection.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sSelection.Width = 30;
            // 
            // sId
            // 
            this.sId.DataPropertyName = "Supplier_ID";
            this.sId.HeaderText = "供应商编号";
            this.sId.Name = "sId";
            this.sId.ReadOnly = true;
            this.sId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sName
            // 
            this.sName.DataPropertyName = "Supplier_Name";
            this.sName.HeaderText = "供应商名称";
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sName.Width = 150;
            // 
            // sIndustry
            // 
            this.sIndustry.DataPropertyName = "SupplierIndustry_Id";
            this.sIndustry.HeaderText = "行业";
            this.sIndustry.Name = "sIndustry";
            this.sIndustry.ReadOnly = true;
            this.sIndustry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sAddress
            // 
            this.sAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sAddress.DataPropertyName = "Address";
            this.sAddress.HeaderText = "供应商地址";
            this.sAddress.Name = "sAddress";
            this.sAddress.ReadOnly = true;
            this.sAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // gb_Standard
            // 
            this.gb_Standard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gb_Standard.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Standard.Controls.Add(this.Price_Score);
            this.gb_Standard.Controls.Add(this.PriceLevel_Score);
            this.gb_Standard.Controls.Add(this.label26);
            this.gb_Standard.Controls.Add(this.label47);
            this.gb_Standard.Controls.Add(this.label48);
            this.gb_Standard.Controls.Add(this.label49);
            this.gb_Standard.Controls.Add(this.label50);
            this.gb_Standard.Controls.Add(this.PriceHistory_Score);
            this.gb_Standard.Controls.Add(this.Quality_Score);
            this.gb_Standard.Controls.Add(this.GoodReceipt_Score);
            this.gb_Standard.Controls.Add(this.QualityAudit_Score);
            this.gb_Standard.Controls.Add(this.ComplaintAndReject_Score);
            this.gb_Standard.Controls.Add(this.Delivery_Score);
            this.gb_Standard.Controls.Add(this.OnTimeDelivery_Score);
            this.gb_Standard.Controls.Add(this.ConfirmDate_Score);
            this.gb_Standard.Controls.Add(this.QuantityReliability_Score);
            this.gb_Standard.Controls.Add(this.Shipment_Score);
            this.gb_Standard.Controls.Add(this.GeneralServiceAndSupport_Score);
            this.gb_Standard.Controls.Add(this.ExternalService_Score);
            this.gb_Standard.Controls.Add(this.lb_SecondStandard);
            this.gb_Standard.Controls.Add(this.lb_MainStandard);
            this.gb_Standard.Location = new System.Drawing.Point(6, 16);
            this.gb_Standard.Name = "gb_Standard";
            this.gb_Standard.Size = new System.Drawing.Size(258, 430);
            this.gb_Standard.TabIndex = 13;
            this.gb_Standard.TabStop = false;
            this.gb_Standard.Text = "标准选择";
            // 
            // Price_Score
            // 
            this.Price_Score.AutoSize = true;
            this.Price_Score.Checked = true;
            this.Price_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Price_Score.Location = new System.Drawing.Point(19, 69);
            this.Price_Score.Name = "Price_Score";
            this.Price_Score.Size = new System.Drawing.Size(48, 16);
            this.Price_Score.TabIndex = 33;
            this.Price_Score.Text = "价格";
            this.Price_Score.UseVisualStyleBackColor = true;
            // 
            // PriceLevel_Score
            // 
            this.PriceLevel_Score.AutoSize = true;
            this.PriceLevel_Score.Location = new System.Drawing.Point(120, 58);
            this.PriceLevel_Score.Name = "PriceLevel_Score";
            this.PriceLevel_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceLevel_Score.TabIndex = 31;
            this.PriceLevel_Score.Text = "价格水平";
            this.PriceLevel_Score.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Location = new System.Drawing.Point(8, 44);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(244, 1);
            this.label26.TabIndex = 14;
            // 
            // label47
            // 
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Location = new System.Drawing.Point(8, 108);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(244, 1);
            this.label47.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Location = new System.Drawing.Point(8, 369);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(244, 1);
            this.label48.TabIndex = 32;
            // 
            // label49
            // 
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label49.Location = new System.Drawing.Point(8, 314);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(244, 1);
            this.label49.TabIndex = 30;
            // 
            // label50
            // 
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Location = new System.Drawing.Point(8, 206);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(244, 1);
            this.label50.TabIndex = 24;
            // 
            // PriceHistory_Score
            // 
            this.PriceHistory_Score.AutoSize = true;
            this.PriceHistory_Score.Location = new System.Drawing.Point(120, 82);
            this.PriceHistory_Score.Name = "PriceHistory_Score";
            this.PriceHistory_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceHistory_Score.TabIndex = 16;
            this.PriceHistory_Score.Text = "价格历史";
            this.PriceHistory_Score.UseVisualStyleBackColor = true;
            // 
            // Quality_Score
            // 
            this.Quality_Score.AutoSize = true;
            this.Quality_Score.Checked = true;
            this.Quality_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Quality_Score.Location = new System.Drawing.Point(19, 149);
            this.Quality_Score.Name = "Quality_Score";
            this.Quality_Score.Size = new System.Drawing.Size(48, 16);
            this.Quality_Score.TabIndex = 15;
            this.Quality_Score.Text = "质量";
            this.Quality_Score.UseVisualStyleBackColor = true;
            // 
            // GoodReceipt_Score
            // 
            this.GoodReceipt_Score.AutoSize = true;
            this.GoodReceipt_Score.Location = new System.Drawing.Point(120, 120);
            this.GoodReceipt_Score.Name = "GoodReceipt_Score";
            this.GoodReceipt_Score.Size = new System.Drawing.Size(48, 16);
            this.GoodReceipt_Score.TabIndex = 17;
            this.GoodReceipt_Score.Text = "收货";
            this.GoodReceipt_Score.UseVisualStyleBackColor = true;
            // 
            // QualityAudit_Score
            // 
            this.QualityAudit_Score.AutoSize = true;
            this.QualityAudit_Score.Location = new System.Drawing.Point(120, 147);
            this.QualityAudit_Score.Name = "QualityAudit_Score";
            this.QualityAudit_Score.Size = new System.Drawing.Size(72, 16);
            this.QualityAudit_Score.TabIndex = 20;
            this.QualityAudit_Score.Text = "质量审计";
            this.QualityAudit_Score.UseVisualStyleBackColor = true;
            // 
            // ComplaintAndReject_Score
            // 
            this.ComplaintAndReject_Score.AutoSize = true;
            this.ComplaintAndReject_Score.Location = new System.Drawing.Point(120, 174);
            this.ComplaintAndReject_Score.Name = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.Size = new System.Drawing.Size(102, 16);
            this.ComplaintAndReject_Score.TabIndex = 19;
            this.ComplaintAndReject_Score.Text = "抱怨/拒绝水平";
            this.ComplaintAndReject_Score.UseVisualStyleBackColor = true;
            // 
            // Delivery_Score
            // 
            this.Delivery_Score.AutoSize = true;
            this.Delivery_Score.Checked = true;
            this.Delivery_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Delivery_Score.Location = new System.Drawing.Point(19, 253);
            this.Delivery_Score.Name = "Delivery_Score";
            this.Delivery_Score.Size = new System.Drawing.Size(48, 16);
            this.Delivery_Score.TabIndex = 21;
            this.Delivery_Score.Text = "交货";
            this.Delivery_Score.UseVisualStyleBackColor = true;
            // 
            // OnTimeDelivery_Score
            // 
            this.OnTimeDelivery_Score.AutoSize = true;
            this.OnTimeDelivery_Score.Location = new System.Drawing.Point(120, 213);
            this.OnTimeDelivery_Score.Name = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.Size = new System.Drawing.Size(108, 16);
            this.OnTimeDelivery_Score.TabIndex = 22;
            this.OnTimeDelivery_Score.Text = "按时交货的表现";
            this.OnTimeDelivery_Score.UseVisualStyleBackColor = true;
            // 
            // ConfirmDate_Score
            // 
            this.ConfirmDate_Score.AutoSize = true;
            this.ConfirmDate_Score.Location = new System.Drawing.Point(120, 239);
            this.ConfirmDate_Score.Name = "ConfirmDate_Score";
            this.ConfirmDate_Score.Size = new System.Drawing.Size(72, 16);
            this.ConfirmDate_Score.TabIndex = 28;
            this.ConfirmDate_Score.Text = "确认日期";
            this.ConfirmDate_Score.UseVisualStyleBackColor = true;
            // 
            // QuantityReliability_Score
            // 
            this.QuantityReliability_Score.AutoSize = true;
            this.QuantityReliability_Score.Location = new System.Drawing.Point(120, 264);
            this.QuantityReliability_Score.Name = "QuantityReliability_Score";
            this.QuantityReliability_Score.Size = new System.Drawing.Size(84, 16);
            this.QuantityReliability_Score.TabIndex = 27;
            this.QuantityReliability_Score.Text = "数量可靠性";
            this.QuantityReliability_Score.UseVisualStyleBackColor = true;
            // 
            // Shipment_Score
            // 
            this.Shipment_Score.AutoSize = true;
            this.Shipment_Score.Location = new System.Drawing.Point(120, 288);
            this.Shipment_Score.Name = "Shipment_Score";
            this.Shipment_Score.Size = new System.Drawing.Size(72, 16);
            this.Shipment_Score.TabIndex = 29;
            this.Shipment_Score.Text = "装运须知";
            this.Shipment_Score.UseVisualStyleBackColor = true;
            // 
            // GeneralServiceAndSupport_Score
            // 
            this.GeneralServiceAndSupport_Score.AutoSize = true;
            this.GeneralServiceAndSupport_Score.Checked = true;
            this.GeneralServiceAndSupport_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GeneralServiceAndSupport_Score.Location = new System.Drawing.Point(19, 331);
            this.GeneralServiceAndSupport_Score.Name = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.Size = new System.Drawing.Size(102, 16);
            this.GeneralServiceAndSupport_Score.TabIndex = 26;
            this.GeneralServiceAndSupport_Score.Text = "一般服务/支持";
            this.GeneralServiceAndSupport_Score.UseVisualStyleBackColor = true;
            // 
            // ExternalService_Score
            // 
            this.ExternalService_Score.AutoSize = true;
            this.ExternalService_Score.Checked = true;
            this.ExternalService_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ExternalService_Score.Location = new System.Drawing.Point(19, 388);
            this.ExternalService_Score.Name = "ExternalService_Score";
            this.ExternalService_Score.Size = new System.Drawing.Size(72, 16);
            this.ExternalService_Score.TabIndex = 25;
            this.ExternalService_Score.Text = "外部服务";
            this.ExternalService_Score.UseVisualStyleBackColor = true;
            // 
            // lb_SecondStandard
            // 
            this.lb_SecondStandard.AutoSize = true;
            this.lb_SecondStandard.Location = new System.Drawing.Point(127, 22);
            this.lb_SecondStandard.Name = "lb_SecondStandard";
            this.lb_SecondStandard.Size = new System.Drawing.Size(41, 12);
            this.lb_SecondStandard.TabIndex = 13;
            this.lb_SecondStandard.Text = "次标准";
            // 
            // lb_MainStandard
            // 
            this.lb_MainStandard.AutoSize = true;
            this.lb_MainStandard.Location = new System.Drawing.Point(22, 22);
            this.lb_MainStandard.Name = "lb_MainStandard";
            this.lb_MainStandard.Size = new System.Drawing.Size(41, 12);
            this.lb_MainStandard.TabIndex = 12;
            this.lb_MainStandard.Text = "主标准";
            // 
            // tab_PriceCompare
            // 
            this.tab_PriceCompare.BackColor = System.Drawing.SystemColors.Control;
            this.tab_PriceCompare.Controls.Add(this.groupBox3);
            this.tab_PriceCompare.Controls.Add(this.btn_Compare);
            this.tab_PriceCompare.Controls.Add(this.cmb_MaterialId);
            this.tab_PriceCompare.Controls.Add(this.label25);
            this.tab_PriceCompare.Location = new System.Drawing.Point(4, 22);
            this.tab_PriceCompare.Name = "tab_PriceCompare";
            this.tab_PriceCompare.Padding = new System.Windows.Forms.Padding(3);
            this.tab_PriceCompare.Size = new System.Drawing.Size(856, 449);
            this.tab_PriceCompare.TabIndex = 2;
            this.tab_PriceCompare.Text = "价格比较";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.priceGridView);
            this.groupBox3.Location = new System.Drawing.Point(18, 90);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(805, 282);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "价格信息";
            // 
            // priceGridView
            // 
            this.priceGridView.AllowUserToAddRows = false;
            this.priceGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.priceGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.recordInfoId,
            this.compareSupplierId,
            this.compareSupplierName,
            this.price,
            this.priceTime});
            this.priceGridView.Location = new System.Drawing.Point(6, 34);
            this.priceGridView.Name = "priceGridView";
            this.priceGridView.RowTemplate.Height = 23;
            this.priceGridView.Size = new System.Drawing.Size(706, 150);
            this.priceGridView.TabIndex = 0;
            // 
            // recordInfoId
            // 
            this.recordInfoId.HeaderText = "采购信息记录编号";
            this.recordInfoId.Name = "recordInfoId";
            this.recordInfoId.Width = 150;
            // 
            // compareSupplierId
            // 
            this.compareSupplierId.HeaderText = "供应商编号";
            this.compareSupplierId.Name = "compareSupplierId";
            // 
            // compareSupplierName
            // 
            this.compareSupplierName.HeaderText = "供应商名称";
            this.compareSupplierName.Name = "compareSupplierName";
            // 
            // PriceNum
            // 
            this.price.HeaderText = "历史价格";
            this.price.Name = "PriceNum";
            // 
            // priceTime
            // 
            this.priceTime.HeaderText = "创建时间";
            this.priceTime.Name = "priceTime";
            this.priceTime.Width = 140;
            // 
            // btn_Compare
            // 
            this.btn_Compare.Location = new System.Drawing.Point(382, 30);
            this.btn_Compare.Name = "btn_Compare";
            this.btn_Compare.Size = new System.Drawing.Size(75, 23);
            this.btn_Compare.TabIndex = 2;
            this.btn_Compare.Text = "比较";
            this.btn_Compare.UseVisualStyleBackColor = true;
            this.btn_Compare.Click += new System.EventHandler(this.btn_Compare_Click);
            // 
            // cmb_MaterialId
            // 
            this.cmb_MaterialId.FormattingEnabled = true;
            this.cmb_MaterialId.Location = new System.Drawing.Point(221, 33);
            this.cmb_MaterialId.Name = "cmb_MaterialId";
            this.cmb_MaterialId.Size = new System.Drawing.Size(121, 20);
            this.cmb_MaterialId.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(151, 36);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "物料信息";
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(4, 28);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(934, 508);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "批准预览";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.btn_ShowResult);
            this.tabPage2.Controls.Add(this.cmb_ResSupplier);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.tabControl1);
            this.tabPage2.Controls.Add(this.MaterialGridview);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(934, 508);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "项目数据";
            // 
            // btn_ShowResult
            // 
            this.btn_ShowResult.Location = new System.Drawing.Point(775, 424);
            this.btn_ShowResult.Name = "btn_ShowResult";
            this.btn_ShowResult.Size = new System.Drawing.Size(75, 23);
            this.btn_ShowResult.TabIndex = 208;
            this.btn_ShowResult.Text = "确定";
            this.btn_ShowResult.UseVisualStyleBackColor = true;
            this.btn_ShowResult.Click += new System.EventHandler(this.btn_ShowResult_Click);
            // 
            // cmb_ResSupplier
            // 
            this.cmb_ResSupplier.FormattingEnabled = true;
            this.cmb_ResSupplier.Location = new System.Drawing.Point(746, 248);
            this.cmb_ResSupplier.Name = "cmb_ResSupplier";
            this.cmb_ResSupplier.Size = new System.Drawing.Size(121, 20);
            this.cmb_ResSupplier.TabIndex = 207;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(699, 251);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(41, 12);
            this.label44.TabIndex = 206;
            this.label44.Text = "供应商";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbp_Condition);
            this.tabControl1.Controls.Add(this.tbp_TimeSection);
            this.tabControl1.Controls.Add(this.tbp_NumberSection);
            this.tabControl1.Location = new System.Drawing.Point(18, 223);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(655, 255);
            this.tabControl1.TabIndex = 205;
            // 
            // tbp_Condition
            // 
            this.tbp_Condition.BackColor = System.Drawing.SystemColors.Control;
            this.tbp_Condition.Controls.Add(this.textBox3);
            this.tbp_Condition.Controls.Add(this.conditionGridView);
            this.tbp_Condition.Controls.Add(this.label12);
            this.tbp_Condition.Controls.Add(this.txt_NetPrice);
            this.tbp_Condition.Controls.Add(this.label43);
            this.tbp_Condition.Location = new System.Drawing.Point(4, 22);
            this.tbp_Condition.Name = "tbp_Condition";
            this.tbp_Condition.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_Condition.Size = new System.Drawing.Size(647, 229);
            this.tbp_Condition.TabIndex = 0;
            this.tbp_Condition.Text = "条件定价";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(441, 187);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 21);
            this.textBox3.TabIndex = 11;
            // 
            // conditionGridView
            // 
            this.conditionGridView.AllowUserToAddRows = false;
            this.conditionGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.conditionGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.conditionGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.conditionNum,
            this.priceType,
            this.calType,
            this.disType,
            this.pn,
            this.number});
            this.conditionGridView.Location = new System.Drawing.Point(18, 6);
            this.conditionGridView.Name = "conditionGridView";
            this.conditionGridView.RowTemplate.Height = 23;
            this.conditionGridView.Size = new System.Drawing.Size(523, 165);
            this.conditionGridView.TabIndex = 0;
            // 
            // conditionNum
            // 
            this.conditionNum.HeaderText = "条件类型";
            this.conditionNum.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "折上折",
            "附加费用",
            "运费"});
            this.conditionNum.Name = "conditionNum";
            this.conditionNum.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.conditionNum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // priceType
            // 
            this.priceType.HeaderText = "定价类型";
            this.priceType.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.priceType.Name = "priceType";
            this.priceType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.priceType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.priceType.Width = 80;
            // 
            // calType
            // 
            this.calType.HeaderText = "计算类型";
            this.calType.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.calType.Name = "calType";
            this.calType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.calType.Width = 80;
            // 
            // disType
            // 
            this.disType.HeaderText = "舍入规则";
            this.disType.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.disType.Name = "disType";
            this.disType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.disType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.disType.Width = 80;
            // 
            // pn
            // 
            this.pn.HeaderText = "正/负";
            this.pn.Items.AddRange(new object[] {
            "+",
            "-"});
            this.pn.Name = "pn";
            this.pn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.pn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.pn.Width = 60;
            // 
            // number
            // 
            this.number.HeaderText = "值/比例";
            this.number.Name = "number";
            this.number.Width = 80;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(394, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 10;
            this.label12.Text = "有效价";
            // 
            // txt_NetPrice
            // 
            this.txt_NetPrice.Location = new System.Drawing.Point(290, 187);
            this.txt_NetPrice.Name = "txt_NetPrice";
            this.txt_NetPrice.Size = new System.Drawing.Size(76, 21);
            this.txt_NetPrice.TabIndex = 6;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(255, 190);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 12);
            this.label43.TabIndex = 5;
            this.label43.Text = "净价";
            // 
            // tbp_TimeSection
            // 
            this.tbp_TimeSection.BackColor = System.Drawing.SystemColors.Control;
            this.tbp_TimeSection.Controls.Add(this.timeGridView);
            this.tbp_TimeSection.Location = new System.Drawing.Point(4, 22);
            this.tbp_TimeSection.Name = "tbp_TimeSection";
            this.tbp_TimeSection.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_TimeSection.Size = new System.Drawing.Size(647, 229);
            this.tbp_TimeSection.TabIndex = 1;
            this.tbp_TimeSection.Text = "时间区间定价";
            // 
            // timeGridView
            // 
            this.timeGridView.AllowUserToAddRows = false;
            this.timeGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.timeGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.timeGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.startTime,
            this.endTime,
            this.timePrice});
            this.timeGridView.Location = new System.Drawing.Point(50, 19);
            this.timeGridView.Name = "timeGridView";
            this.timeGridView.RowTemplate.Height = 23;
            this.timeGridView.Size = new System.Drawing.Size(358, 125);
            this.timeGridView.TabIndex = 0;
            // 
            // startTime
            // 
            this.startTime.HeaderText = "开始时间";
            this.startTime.Name = "startTime";
            this.startTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.startTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // endTime
            // 
            this.endTime.HeaderText = "结束时间";
            this.endTime.Name = "endTime";
            this.endTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.endTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // timePrice
            // 
            this.timePrice.HeaderText = "价格";
            this.timePrice.Name = "timePrice";
            // 
            // tbp_NumberSection
            // 
            this.tbp_NumberSection.BackColor = System.Drawing.SystemColors.Control;
            this.tbp_NumberSection.Controls.Add(this.numberGridView);
            this.tbp_NumberSection.Location = new System.Drawing.Point(4, 22);
            this.tbp_NumberSection.Name = "tbp_NumberSection";
            this.tbp_NumberSection.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_NumberSection.Size = new System.Drawing.Size(647, 229);
            this.tbp_NumberSection.TabIndex = 2;
            this.tbp_NumberSection.Text = "数量区间定价";
            // 
            // numberGridView
            // 
            this.numberGridView.AllowUserToAddRows = false;
            this.numberGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.numberGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.numberGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.startNumber,
            this.endNumber,
            this.numberPrice});
            this.numberGridView.Location = new System.Drawing.Point(96, 34);
            this.numberGridView.Name = "numberGridView";
            this.numberGridView.RowTemplate.Height = 23;
            this.numberGridView.Size = new System.Drawing.Size(356, 125);
            this.numberGridView.TabIndex = 0;
            // 
            // startNumber
            // 
            this.startNumber.HeaderText = "开始数量";
            this.startNumber.Name = "startNumber";
            // 
            // endNumber
            // 
            this.endNumber.HeaderText = "结束数量";
            this.endNumber.Name = "endNumber";
            // 
            // numberPrice
            // 
            this.numberPrice.HeaderText = "价格";
            this.numberPrice.Name = "numberPrice";
            // 
            // MaterialGridview
            // 
            this.MaterialGridview.AllowUserToAddRows = false;
            this.MaterialGridview.BackgroundColor = System.Drawing.SystemColors.Control;
            this.MaterialGridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MaterialGridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bidMaterialId,
            this.bidMaterialName,
            this.bidDemandCount,
            this.bidDemandMeasurement,
            this.factoryId,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            this.MaterialGridview.Location = new System.Drawing.Point(18, 16);
            this.MaterialGridview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaterialGridview.Name = "MaterialGridview";
            this.MaterialGridview.RowHeadersVisible = false;
            this.MaterialGridview.RowTemplate.Height = 23;
            this.MaterialGridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MaterialGridview.Size = new System.Drawing.Size(852, 190);
            this.MaterialGridview.TabIndex = 200;
            // 
            // bidMaterialId
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.bidMaterialId.DefaultCellStyle = dataGridViewCellStyle8;
            this.bidMaterialId.Frozen = true;
            this.bidMaterialId.HeaderText = "物料编号";
            this.bidMaterialId.Name = "bidMaterialId";
            this.bidMaterialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidMaterialId.Width = 140;
            // 
            // bidMaterialName
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.bidMaterialName.DefaultCellStyle = dataGridViewCellStyle9;
            this.bidMaterialName.Frozen = true;
            this.bidMaterialName.HeaderText = "物料名称";
            this.bidMaterialName.Name = "bidMaterialName";
            this.bidMaterialName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidMaterialName.Width = 120;
            // 
            // bidDemandCount
            // 
            this.bidDemandCount.Frozen = true;
            this.bidDemandCount.HeaderText = "数量";
            this.bidDemandCount.Name = "bidDemandCount";
            this.bidDemandCount.Width = 80;
            // 
            // bidDemandMeasurement
            // 
            this.bidDemandMeasurement.Frozen = true;
            this.bidDemandMeasurement.HeaderText = "单位";
            this.bidDemandMeasurement.Name = "bidDemandMeasurement";
            this.bidDemandMeasurement.ReadOnly = true;
            this.bidDemandMeasurement.Width = 60;
            // 
            // factoryId
            // 
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            // 
            // stockId
            // 
            this.stockId.HeaderText = "库存编号";
            this.stockId.Name = "stockId";
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交易开始日期";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交易结束日期";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            // 
            // txt_BidName
            // 
            this.txt_BidName.Location = new System.Drawing.Point(335, 23);
            this.txt_BidName.Name = "txt_BidName";
            this.txt_BidName.ReadOnly = true;
            this.txt_BidName.Size = new System.Drawing.Size(151, 21);
            this.txt_BidName.TabIndex = 206;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(276, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 204;
            this.label3.Text = "招标名称";
            // 
            // txt_BidId
            // 
            this.txt_BidId.Location = new System.Drawing.Point(75, 23);
            this.txt_BidId.Name = "txt_BidId";
            this.txt_BidId.ReadOnly = true;
            this.txt_BidId.Size = new System.Drawing.Size(152, 21);
            this.txt_BidId.TabIndex = 205;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(15, 26);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 203;
            this.xjdh_lbl.Text = "招标编号";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(529, 26);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(29, 12);
            this.label45.TabIndex = 208;
            this.label45.Text = "状态";
            // 
            // txt_State
            // 
            this.txt_State.Location = new System.Drawing.Point(564, 23);
            this.txt_State.Name = "txt_State";
            this.txt_State.ReadOnly = true;
            this.txt_State.Size = new System.Drawing.Size(80, 21);
            this.txt_State.TabIndex = 209;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "条件类型";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "折上折",
            "附加费用",
            "运费"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "定价类型";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 80;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "计算类型";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 80;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "舍入规则";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 80;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "正/负";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "+",
            "-"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 60;
            // 
            // num
            // 
            this.num.HeaderText = "选中";
            this.num.Name = "num";
            this.num.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // resSupplierId
            // 
            this.resSupplierId.HeaderText = "供应商编号";
            this.resSupplierId.Name = "resSupplierId";
            // 
            // CompareBid_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 615);
            this.Controls.Add(this.txt_State);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txt_BidName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_BidId);
            this.Controls.Add(this.xjdh_lbl);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CompareBid_Form";
            this.Text = "进行评标";
            this.panel3.ResumeLayout(false);
            this.BiddingTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SupplierGridView)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileGridView)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tab_EvalMethod.ResumeLayout(false);
            this.tab_Lowprice.ResumeLayout(false);
            this.tab_Lowprice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierGridViewLP)).EndInit();
            this.tab_WeightScore.ResumeLayout(false);
            this.gbx_WeightInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resSupplierGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultGridView)).EndInit();
            this.tab_LeastTotalCost.ResumeLayout(false);
            this.gbx_leastTotalCost.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allCostGridView)).EndInit();
            this.tabPage11.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tab_History.ResumeLayout(false);
            this.tab_History.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierHistoryView)).EndInit();
            this.tab_Evaluate.ResumeLayout(false);
            this.grp_Condition.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.gb_Supplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).EndInit();
            this.gb_Standard.ResumeLayout(false);
            this.gb_Standard.PerformLayout();
            this.tab_PriceCompare.ResumeLayout(false);
            this.tab_PriceCompare.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.priceGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tbp_Condition.ResumeLayout(false);
            this.tbp_Condition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).EndInit();
            this.tbp_TimeSection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeGridView)).EndInit();
            this.tbp_NumberSection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numberGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialGridview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl BiddingTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txt_currency;
        private System.Windows.Forms.TextBox txt_timeZone;
        private System.Windows.Forms.TextBox txt_limitTime;
        private System.Windows.Forms.TextBox txt_startBeginTime;
        private System.Windows.Forms.TextBox txt_EndTime;
        private System.Windows.Forms.TextBox txt_StartTime;
        private System.Windows.Forms.TextBox txt_commBidStartDate;
        private System.Windows.Forms.TextBox txt_commBidStartTime;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txt_enrollStartDate;
        private System.Windows.Forms.TextBox txt_enrollStartTime;
        private System.Windows.Forms.TextBox txt_enrollEndDate;
        private System.Windows.Forms.TextBox txt_enrollEndTime;
        private System.Windows.Forms.TextBox txt_buyEndDate;
        private System.Windows.Forms.TextBox txt_buyEndTime;
        private System.Windows.Forms.TextBox txt_techBidStartDate;
        private System.Windows.Forms.TextBox txt_techBidStartTime;
        private System.Windows.Forms.TextBox txt_bidCost;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_purchaseGroup;
        private System.Windows.Forms.TextBox txt_purchaseOrg;
        private System.Windows.Forms.TextBox txt_displayType;
        private System.Windows.Forms.TextBox txt_catalogue;
        private System.Windows.Forms.TextBox txt_serviceType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox txt_callerName;
        private System.Windows.Forms.TextBox txt_callerId;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txt_ReviewerName;
        private System.Windows.Forms.TextBox txt_reviewerId;
        private System.Windows.Forms.TextBox txt_dropPointName;
        private System.Windows.Forms.TextBox txt_dropPointId;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txt_receiverName;
        private System.Windows.Forms.TextBox txt_receiverId;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txt_requesterName;
        private System.Windows.Forms.TextBox txt_requesterId;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_addSupplier;
        private System.Windows.Forms.DataGridView SupplierGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidSupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidSupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidClassificationResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidContact;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidState;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox txt_longBidText;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txt_approvalComment;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txt_shortBidText;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView fileGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSize;
        private System.Windows.Forms.Button btn_upload;
        private System.Windows.Forms.Button btn_ChooseFile;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabControl tab_EvalMethod;
        private System.Windows.Forms.TabPage tab_Lowprice;
        private System.Windows.Forms.TabPage tab_WeightScore;
        private System.Windows.Forms.GroupBox gbx_WeightInfo;
        private System.Windows.Forms.TabPage tab_LeastTotalCost;
        private System.Windows.Forms.TabPage tab_ValueAsses;
        private System.Windows.Forms.TextBox txt_EvalMethod;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView MaterialGridview;
        private System.Windows.Forms.TextBox txt_BidName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_BidId;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.DataGridView resultGridView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbp_Condition;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.DataGridView conditionGridView;
        private System.Windows.Forms.DataGridViewComboBoxColumn conditionNum;
        private System.Windows.Forms.DataGridViewComboBoxColumn priceType;
        private System.Windows.Forms.DataGridViewComboBoxColumn calType;
        private System.Windows.Forms.DataGridViewComboBoxColumn disType;
        private System.Windows.Forms.DataGridViewComboBoxColumn pn;
        private System.Windows.Forms.DataGridViewTextBoxColumn number;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_NetPrice;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tbp_TimeSection;
        private System.Windows.Forms.DataGridView timeGridView;
        private Lib.Common.CommonControls.CalendarColumn startTime;
        private Lib.Common.CommonControls.CalendarColumn endTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn timePrice;
        private System.Windows.Forms.TabPage tbp_NumberSection;
        private System.Windows.Forms.DataGridView numberGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn startNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn endNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberPrice;
        private System.Windows.Forms.ComboBox cmb_ResSupplier;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.Button btn_ShowResult;
        private System.Windows.Forms.GroupBox gbx_leastTotalCost;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView allCostGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn First;
        private System.Windows.Forms.DataGridViewTextBoxColumn Second;
        private System.Windows.Forms.DataGridViewTextBoxColumn Third;
        private System.Windows.Forms.DataGridViewTextBoxColumn forth;
        private System.Windows.Forms.DataGridViewTextBoxColumn fifth;
        private System.Windows.Forms.DataGridViewTextBoxColumn sixth;
        private System.Windows.Forms.DataGridViewTextBoxColumn seventh;
        private System.Windows.Forms.DataGridViewTextBoxColumn eighth;
        private System.Windows.Forms.DataGridViewTextBoxColumn ninth;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenth;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txt_State;
        private System.Windows.Forms.TextBox txt_transType;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button btn_Transact;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tab_History;
        private System.Windows.Forms.DataGridView supplierHistoryView;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidResult;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox cmb_AnalySupplier;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tab_Evaluate;
        private System.Windows.Forms.TabPage tab_PriceCompare;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btn_Compare;
        private System.Windows.Forms.ComboBox cmb_MaterialId;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView priceGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn recordInfoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn compareSupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn compareSupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceTime;
        private System.Windows.Forms.GroupBox grp_Condition;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox gb_Standard;
        private System.Windows.Forms.CheckBox Price_Score;
        private System.Windows.Forms.CheckBox PriceLevel_Score;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.CheckBox PriceHistory_Score;
        private System.Windows.Forms.CheckBox Quality_Score;
        private System.Windows.Forms.CheckBox GoodReceipt_Score;
        private System.Windows.Forms.CheckBox QualityAudit_Score;
        private System.Windows.Forms.CheckBox ComplaintAndReject_Score;
        private System.Windows.Forms.CheckBox Delivery_Score;
        private System.Windows.Forms.CheckBox OnTimeDelivery_Score;
        private System.Windows.Forms.CheckBox ConfirmDate_Score;
        private System.Windows.Forms.CheckBox QuantityReliability_Score;
        private System.Windows.Forms.CheckBox Shipment_Score;
        private System.Windows.Forms.CheckBox GeneralServiceAndSupport_Score;
        private System.Windows.Forms.CheckBox ExternalService_Score;
        private System.Windows.Forms.Label lb_SecondStandard;
        private System.Windows.Forms.Label lb_MainStandard;
        private System.Windows.Forms.GroupBox gb_Supplier;
        private System.Windows.Forms.DataGridView dgv_SupplierList;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sSelection;
        private System.Windows.Forms.DataGridViewTextBoxColumn sId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sIndustry;
        private System.Windows.Forms.DataGridViewTextBoxColumn sAddress;
        private System.Windows.Forms.Button btn_Sure;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidMaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidDemandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidDemandMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox cmb_LPMaterialId;
        private System.Windows.Forms.DataGridView supplierGridViewLP;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btn_LowPriceSave;
        private System.Windows.Forms.ComboBox cmb_LPSupplier;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button btn_WeightSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LTPsupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPrice;
        private System.Windows.Forms.DataGridView resSupplierGridView;
        private System.Windows.Forms.Button btn_SureSupplier;
        private System.Windows.Forms.DataGridViewCheckBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn resSupplierId;
    }
}