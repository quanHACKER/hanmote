﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class EditCondition_Form :DockContent
    {
        private ConditionBLL conditionBLL = new ConditionBLL();

        public EditCondition_Form()
        {
            InitializeComponent();
        }

        private void init()
        {

        }

        //保存
        private void button1_Click(object sender, EventArgs e)
        {
            ConditionType condition = new ConditionType();
            condition.ConditionId = txt_ConditionId.Text;
            condition.ConditionName = txt_ConditionName.Text;
            condition.ConditionCategory = cmb_Conditioncategory.SelectedText;
            condition.RoundType = cmb_RoundType.SelectedText;
            condition.Pn = cmb_Pn.SelectedText;
            condition.Description = rtb_Description.Text;
            conditionBLL.updateCondition(condition);
        }
    }
}
