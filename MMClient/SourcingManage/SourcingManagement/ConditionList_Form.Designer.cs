﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ConditionList_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.conditionGridView = new System.Windows.Forms.DataGridView();
            this.conditionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conditionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conditionCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_Delete);
            this.groupBox1.Controls.Add(this.conditionGridView);
            this.groupBox1.Location = new System.Drawing.Point(26, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(822, 279);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件类型列表";
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(727, 31);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.btn_Delete.TabIndex = 1;
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // conditionGridView
            // 
            this.conditionGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.conditionGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.conditionGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.conditionId,
            this.conditionName,
            this.conditionCategory,
            this.calType,
            this.roundType,
            this.pn});
            this.conditionGridView.Location = new System.Drawing.Point(31, 31);
            this.conditionGridView.Name = "conditionGridView";
            this.conditionGridView.RowTemplate.Height = 23;
            this.conditionGridView.Size = new System.Drawing.Size(657, 203);
            this.conditionGridView.TabIndex = 0;
            this.conditionGridView.SelectionChanged += new System.EventHandler(this.conditionGridView_SelectionChanged);
            // 
            // conditionId
            // 
            this.conditionId.HeaderText = "条件编号";
            this.conditionId.Name = "conditionId";
            // 
            // conditionName
            // 
            this.conditionName.HeaderText = "条件名称";
            this.conditionName.Name = "conditionName";
            // 
            // conditionCategory
            // 
            this.conditionCategory.HeaderText = "定价类别";
            this.conditionCategory.Name = "conditionCategory";
            // 
            // calType
            // 
            this.calType.HeaderText = "计算类型";
            this.calType.Name = "calType";
            // 
            // roundType
            // 
            this.roundType.HeaderText = "四舍五入";
            this.roundType.Name = "roundType";
            // 
            // pn
            // 
            this.pn.HeaderText = "正/负";
            this.pn.Name = "pn";
            // 
            // ConditionList_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 437);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ConditionList_Form";
            this.Text = "条件类型列表";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView conditionGridView;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn conditionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn conditionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn conditionCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn calType;
        private System.Windows.Forms.DataGridViewTextBoxColumn roundType;
        private System.Windows.Forms.DataGridViewTextBoxColumn pn;
    }
}