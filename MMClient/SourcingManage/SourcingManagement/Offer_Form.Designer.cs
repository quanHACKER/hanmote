﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Offer_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_InquiryId = new System.Windows.Forms.TextBox();
            this.Material_view = new System.Windows.Forms.DataGridView();
            this.saveOfferButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_PurchaseGroup = new System.Windows.Forms.TextBox();
            this.txt_PurchaseOrg = new System.Windows.Forms.TextBox();
            this.txt_State = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_createtime = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmb_SupplierId = new System.Windows.Forms.ComboBox();
            this.btn_Show = new System.Windows.Forms.Button();
            this.txt_offerTime = new System.Windows.Forms.TextBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Inquiry_view0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demand_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Material_view)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(5, 14);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "询价单号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(5, 52);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "采购组织";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(208, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 171;
            this.label2.Text = "采购组";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(382, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 173;
            this.label4.Text = "报价期限";
            // 
            // txt_InquiryId
            // 
            this.txt_InquiryId.Location = new System.Drawing.Point(65, 9);
            this.txt_InquiryId.Name = "txt_InquiryId";
            this.txt_InquiryId.ReadOnly = true;
            this.txt_InquiryId.Size = new System.Drawing.Size(131, 21);
            this.txt_InquiryId.TabIndex = 179;
            // 
            // Material_view
            // 
            this.Material_view.AllowUserToAddRows = false;
            this.Material_view.BackgroundColor = System.Drawing.SystemColors.Control;
            this.Material_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Material_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Inquiry_view0,
            this.materialId,
            this.materialName,
            this.demand_count,
            this.measurement,
            this.price,
            this.factoryId,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            this.Material_view.EnableHeadersVisualStyles = false;
            this.Material_view.Location = new System.Drawing.Point(8, 20);
            this.Material_view.Name = "Material_view";
            this.Material_view.RowHeadersVisible = false;
            this.Material_view.RowTemplate.Height = 23;
            this.Material_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Material_view.Size = new System.Drawing.Size(828, 179);
            this.Material_view.TabIndex = 181;
            // 
            // saveOfferButton
            // 
            this.saveOfferButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveOfferButton.Location = new System.Drawing.Point(796, 427);
            this.saveOfferButton.Name = "saveOfferButton";
            this.saveOfferButton.Size = new System.Drawing.Size(63, 23);
            this.saveOfferButton.TabIndex = 194;
            this.saveOfferButton.Text = "保存";
            this.saveOfferButton.UseVisualStyleBackColor = true;
            this.saveOfferButton.Click += new System.EventHandler(this.saveOfferButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txt_offerTime);
            this.panel2.Controls.Add(this.txt_PurchaseGroup);
            this.panel2.Controls.Add(this.txt_PurchaseOrg);
            this.panel2.Controls.Add(this.txt_State);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txt_InquiryId);
            this.panel2.Controls.Add(this.txt_createtime);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(23, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(683, 84);
            this.panel2.TabIndex = 180;
            // 
            // txt_PurchaseGroup
            // 
            this.txt_PurchaseGroup.Location = new System.Drawing.Point(255, 46);
            this.txt_PurchaseGroup.Name = "txt_PurchaseGroup";
            this.txt_PurchaseGroup.ReadOnly = true;
            this.txt_PurchaseGroup.Size = new System.Drawing.Size(100, 21);
            this.txt_PurchaseGroup.TabIndex = 183;
            // 
            // txt_PurchaseOrg
            // 
            this.txt_PurchaseOrg.Location = new System.Drawing.Point(65, 44);
            this.txt_PurchaseOrg.Name = "txt_PurchaseOrg";
            this.txt_PurchaseOrg.ReadOnly = true;
            this.txt_PurchaseOrg.Size = new System.Drawing.Size(114, 21);
            this.txt_PurchaseOrg.TabIndex = 182;
            // 
            // txt_State
            // 
            this.txt_State.Location = new System.Drawing.Point(246, 8);
            this.txt_State.Name = "txt_State";
            this.txt_State.ReadOnly = true;
            this.txt_State.Size = new System.Drawing.Size(86, 21);
            this.txt_State.TabIndex = 181;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 180;
            this.label3.Text = "状态";
            // 
            // txt_createtime
            // 
            this.txt_createtime.Location = new System.Drawing.Point(442, 9);
            this.txt_createtime.Name = "txt_createtime";
            this.txt_createtime.ReadOnly = true;
            this.txt_createtime.Size = new System.Drawing.Size(157, 21);
            this.txt_createtime.TabIndex = 178;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(382, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 157;
            this.label32.Text = "询价时间";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Material_view);
            this.groupBox2.Location = new System.Drawing.Point(23, 183);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(869, 223);
            this.groupBox2.TabIndex = 196;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料列表";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 197;
            this.label5.Text = "供应商编号";
            // 
            // cmb_SupplierId
            // 
            this.cmb_SupplierId.FormattingEnabled = true;
            this.cmb_SupplierId.Location = new System.Drawing.Point(107, 137);
            this.cmb_SupplierId.Name = "cmb_SupplierId";
            this.cmb_SupplierId.Size = new System.Drawing.Size(121, 20);
            this.cmb_SupplierId.TabIndex = 198;
            // 
            // btn_Show
            // 
            this.btn_Show.Location = new System.Drawing.Point(269, 137);
            this.btn_Show.Name = "btn_Show";
            this.btn_Show.Size = new System.Drawing.Size(75, 23);
            this.btn_Show.TabIndex = 199;
            this.btn_Show.Text = "确定";
            this.btn_Show.UseVisualStyleBackColor = true;
            this.btn_Show.Click += new System.EventHandler(this.btn_Show_Click);
            // 
            // txt_offerTime
            // 
            this.txt_offerTime.Location = new System.Drawing.Point(442, 43);
            this.txt_offerTime.Name = "txt_offerTime";
            this.txt_offerTime.ReadOnly = true;
            this.txt_offerTime.Size = new System.Drawing.Size(157, 21);
            this.txt_offerTime.TabIndex = 184;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // Inquiry_view0
            // 
            this.Inquiry_view0.Frozen = true;
            this.Inquiry_view0.HeaderText = "";
            this.Inquiry_view0.Name = "Inquiry_view0";
            this.Inquiry_view0.ReadOnly = true;
            this.Inquiry_view0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Inquiry_view0.Width = 30;
            // 
            // materialId
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle17;
            this.materialId.Frozen = true;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.ReadOnly = true;
            this.materialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // materialName
            // 
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle18;
            this.materialName.Frozen = true;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            this.materialName.ReadOnly = true;
            this.materialName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // demand_count
            // 
            this.demand_count.Frozen = true;
            this.demand_count.HeaderText = "数量";
            this.demand_count.Name = "demand_count";
            this.demand_count.ReadOnly = true;
            this.demand_count.Width = 60;
            // 
            // measurement
            // 
            this.measurement.Frozen = true;
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.ReadOnly = true;
            this.measurement.Width = 60;
            // 
            // PriceNum
            // 
            this.price.Frozen = true;
            this.price.HeaderText = "价格";
            this.price.Name = "PriceNum";
            this.price.Width = 60;
            // 
            // factoryId
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            this.factoryId.DefaultCellStyle = dataGridViewCellStyle19;
            this.factoryId.Frozen = true;
            this.factoryId.HeaderText = "工厂";
            this.factoryId.Name = "factoryId";
            this.factoryId.ReadOnly = true;
            this.factoryId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.factoryId.Width = 60;
            // 
            // stockId
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.stockId.DefaultCellStyle = dataGridViewCellStyle20;
            this.stockId.Frozen = true;
            this.stockId.HeaderText = "仓库";
            this.stockId.Name = "stockId";
            this.stockId.ReadOnly = true;
            this.stockId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.stockId.Width = 60;
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.Frozen = true;
            this.DeliveryStartTime.HeaderText = "交货开始日期";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交货结束日期";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Width = 120;
            // 
            // Offer_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(975, 492);
            this.Controls.Add(this.btn_Show);
            this.Controls.Add(this.cmb_SupplierId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.saveOfferButton);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Offer_Form";
            this.Text = "执行报价";
            ((System.ComponentModel.ISupportInitialize)(this.Material_view)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView Material_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Button saveOfferButton;
        private System.Windows.Forms.TextBox txt_InquiryId;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txt_State;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_createtime;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txt_PurchaseGroup;
        private System.Windows.Forms.TextBox txt_PurchaseOrg;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmb_SupplierId;
        private System.Windows.Forms.Button btn_Show;
        private System.Windows.Forms.TextBox txt_offerTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view0;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn demand_count;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
    }
}