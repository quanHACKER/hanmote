﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class TwiceMethod_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cgjhgl_panel = new System.Windows.Forms.Panel();
            this.lbl_7 = new System.Windows.Forms.Label();
            this.lbl_6 = new System.Windows.Forms.Label();
            this.lbl_5 = new System.Windows.Forms.Label();
            this.lbl_4 = new System.Windows.Forms.Label();
            this.lbl_1 = new System.Windows.Forms.Label();
            this.lbl_2 = new System.Windows.Forms.Label();
            this.lbl_3 = new System.Windows.Forms.Label();
            this.tbradd_bt = new System.Windows.Forms.Button();
            this.tbrdel_bt = new System.Windows.Forms.Button();
            this.tbrtz_bt = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.refresh_bt = new System.Windows.Forms.Button();
            this.zbzl_bt = new System.Windows.Forms.Button();
            this.del_bt = new System.Windows.Forms.Button();
            this.save_bt = new System.Windows.Forms.Button();
            this.close_bt = new System.Windows.Forms.Button();
            this.tbrbs_cmb = new System.Windows.Forms.ComboBox();
            this.cgjhgl_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cgjhgl_panel
            // 
            this.cgjhgl_panel.Controls.Add(this.lbl_7);
            this.cgjhgl_panel.Controls.Add(this.lbl_6);
            this.cgjhgl_panel.Controls.Add(this.lbl_5);
            this.cgjhgl_panel.Controls.Add(this.lbl_4);
            this.cgjhgl_panel.Controls.Add(this.lbl_1);
            this.cgjhgl_panel.Controls.Add(this.lbl_2);
            this.cgjhgl_panel.Controls.Add(this.lbl_3);
            this.cgjhgl_panel.Location = new System.Drawing.Point(12, 124);
            this.cgjhgl_panel.Name = "cgjhgl_panel";
            this.cgjhgl_panel.Size = new System.Drawing.Size(712, 153);
            this.cgjhgl_panel.TabIndex = 225;
            // 
            // lbl_7
            // 
            this.lbl_7.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_7.Location = new System.Drawing.Point(600, 16);
            this.lbl_7.Name = "lbl_7";
            this.lbl_7.Size = new System.Drawing.Size(100, 25);
            this.lbl_7.TabIndex = 114;
            this.lbl_7.Text = "投标人标识";
            this.lbl_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_6
            // 
            this.lbl_6.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_6.Location = new System.Drawing.Point(501, 16);
            this.lbl_6.Name = "lbl_6";
            this.lbl_6.Size = new System.Drawing.Size(100, 25);
            this.lbl_6.TabIndex = 107;
            this.lbl_6.Text = "公司标识";
            this.lbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_5
            // 
            this.lbl_5.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_5.Location = new System.Drawing.Point(442, 16);
            this.lbl_5.Name = "lbl_5";
            this.lbl_5.Size = new System.Drawing.Size(60, 25);
            this.lbl_5.TabIndex = 100;
            this.lbl_5.Text = "国家";
            this.lbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_4
            // 
            this.lbl_4.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_4.Location = new System.Drawing.Point(343, 16);
            this.lbl_4.Name = "lbl_4";
            this.lbl_4.Size = new System.Drawing.Size(100, 25);
            this.lbl_4.TabIndex = 93;
            this.lbl_4.Text = "联系人";
            this.lbl_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_1
            // 
            this.lbl_1.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_1.Location = new System.Drawing.Point(15, 16);
            this.lbl_1.Name = "lbl_1";
            this.lbl_1.Size = new System.Drawing.Size(30, 25);
            this.lbl_1.TabIndex = 86;
            this.lbl_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_2
            // 
            this.lbl_2.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_2.Location = new System.Drawing.Point(44, 16);
            this.lbl_2.Name = "lbl_2";
            this.lbl_2.Size = new System.Drawing.Size(50, 25);
            this.lbl_2.TabIndex = 86;
            this.lbl_2.Text = "序号";
            this.lbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_3
            // 
            this.lbl_3.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_3.Location = new System.Drawing.Point(93, 16);
            this.lbl_3.Name = "lbl_3";
            this.lbl_3.Size = new System.Drawing.Size(251, 25);
            this.lbl_3.TabIndex = 86;
            this.lbl_3.Text = "公司名称";
            this.lbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbradd_bt
            // 
            this.tbradd_bt.Location = new System.Drawing.Point(341, 87);
            this.tbradd_bt.Name = "tbradd_bt";
            this.tbradd_bt.Size = new System.Drawing.Size(60, 30);
            this.tbradd_bt.TabIndex = 224;
            this.tbradd_bt.Text = "添加";
            this.tbradd_bt.UseVisualStyleBackColor = true;
            this.tbradd_bt.Click += new System.EventHandler(this.tbradd_bt_Click);
            // 
            // tbrdel_bt
            // 
            this.tbrdel_bt.Location = new System.Drawing.Point(512, 88);
            this.tbrdel_bt.Name = "tbrdel_bt";
            this.tbrdel_bt.Size = new System.Drawing.Size(60, 30);
            this.tbrdel_bt.TabIndex = 223;
            this.tbrdel_bt.Text = "删除";
            this.tbrdel_bt.UseVisualStyleBackColor = true;
            // 
            // tbrtz_bt
            // 
            this.tbrtz_bt.Location = new System.Drawing.Point(417, 88);
            this.tbrtz_bt.Name = "tbrtz_bt";
            this.tbrtz_bt.Size = new System.Drawing.Size(80, 30);
            this.tbrtz_bt.TabIndex = 222;
            this.tbrtz_bt.Text = "发送通知";
            this.tbrtz_bt.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(32, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 12);
            this.label9.TabIndex = 220;
            this.label9.Text = "按公司标识添加投标人";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.refresh_bt);
            this.panel1.Controls.Add(this.zbzl_bt);
            this.panel1.Controls.Add(this.del_bt);
            this.panel1.Controls.Add(this.save_bt);
            this.panel1.Controls.Add(this.close_bt);
            this.panel1.Location = new System.Drawing.Point(12, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 42);
            this.panel1.TabIndex = 87;
            // 
            // refresh_bt
            // 
            this.refresh_bt.Location = new System.Drawing.Point(177, 5);
            this.refresh_bt.Name = "refresh_bt";
            this.refresh_bt.Size = new System.Drawing.Size(60, 30);
            this.refresh_bt.TabIndex = 80;
            this.refresh_bt.Text = "刷新";
            this.refresh_bt.UseVisualStyleBackColor = true;
            // 
            // zbzl_bt
            // 
            this.zbzl_bt.Location = new System.Drawing.Point(337, 5);
            this.zbzl_bt.Name = "zbzl_bt";
            this.zbzl_bt.Size = new System.Drawing.Size(80, 30);
            this.zbzl_bt.TabIndex = 78;
            this.zbzl_bt.Text = "招标总览";
            this.zbzl_bt.UseVisualStyleBackColor = true;
            // 
            // del_bt
            // 
            this.del_bt.Location = new System.Drawing.Point(257, 5);
            this.del_bt.Name = "del_bt";
            this.del_bt.Size = new System.Drawing.Size(60, 30);
            this.del_bt.TabIndex = 77;
            this.del_bt.Text = "删除";
            this.del_bt.UseVisualStyleBackColor = true;
            // 
            // save_bt
            // 
            this.save_bt.Location = new System.Drawing.Point(96, 5);
            this.save_bt.Name = "save_bt";
            this.save_bt.Size = new System.Drawing.Size(60, 30);
            this.save_bt.TabIndex = 76;
            this.save_bt.Text = "保存";
            this.save_bt.UseVisualStyleBackColor = true;
            // 
            // close_bt
            // 
            this.close_bt.Location = new System.Drawing.Point(16, 5);
            this.close_bt.Name = "close_bt";
            this.close_bt.Size = new System.Drawing.Size(60, 30);
            this.close_bt.TabIndex = 75;
            this.close_bt.Text = "关闭";
            this.close_bt.UseVisualStyleBackColor = true;
            // 
            // tbrbs_cmb
            // 
            this.tbrbs_cmb.FormattingEnabled = true;
            this.tbrbs_cmb.Location = new System.Drawing.Point(165, 93);
            this.tbrbs_cmb.Name = "tbrbs_cmb";
            this.tbrbs_cmb.Size = new System.Drawing.Size(160, 20);
            this.tbrbs_cmb.TabIndex = 226;
            // 
            // TwiceMethod_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(938, 658);
            this.Controls.Add(this.tbrbs_cmb);
            this.Controls.Add(this.cgjhgl_panel);
            this.Controls.Add(this.tbradd_bt);
            this.Controls.Add(this.tbrdel_bt);
            this.Controls.Add(this.tbrtz_bt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "TwiceMethod_Form";
            this.Text = "两段式招标";
            this.Load += new System.EventHandler(this.TwiceMethod_Load);
            this.cgjhgl_panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button refresh_bt;
        private System.Windows.Forms.Button zbzl_bt;
        private System.Windows.Forms.Button del_bt;
        private System.Windows.Forms.Button save_bt;
        private System.Windows.Forms.Button close_bt;
        private System.Windows.Forms.Button tbradd_bt;
        private System.Windows.Forms.Button tbrdel_bt;
        private System.Windows.Forms.Button tbrtz_bt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel cgjhgl_panel;
        private System.Windows.Forms.Label lbl_7;
        private System.Windows.Forms.Label lbl_6;
        private System.Windows.Forms.Label lbl_5;
        private System.Windows.Forms.Label lbl_4;
        private System.Windows.Forms.Label lbl_1;
        private System.Windows.Forms.Label lbl_2;
        private System.Windows.Forms.Label lbl_3;
        private System.Windows.Forms.ComboBox tbrbs_cmb;
    }
}