﻿namespace MMClient.SourcingManage.SourcingManagement.SPReport
{
    partial class Frm_SPCompareRadarChartResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pl_Chart = new System.Windows.Forms.Panel();
            this.viewChart = new ChartDirector.WinChartViewer();
            this.pl_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewChart)).BeginInit();
            this.SuspendLayout();
            // 
            // pl_Chart
            // 
            this.pl_Chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pl_Chart.Controls.Add(this.viewChart);
            this.pl_Chart.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pl_Chart.Location = new System.Drawing.Point(18, 48);
            this.pl_Chart.Margin = new System.Windows.Forms.Padding(0);
            this.pl_Chart.Name = "pl_Chart";
            this.pl_Chart.Size = new System.Drawing.Size(1267, 733);
            this.pl_Chart.TabIndex = 0;
            this.pl_Chart.Paint += new System.Windows.Forms.PaintEventHandler(this.pl_Chart_Paint);
            // 
            // viewChart
            // 
            this.viewChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.viewChart.Location = new System.Drawing.Point(3, 3);
            this.viewChart.Name = "viewChart";
            this.viewChart.Size = new System.Drawing.Size(342, 226);
            this.viewChart.TabIndex = 0;
            this.viewChart.TabStop = false;
            // 
            // Frm_SPCompareRadarChartResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 790);
            this.Controls.Add(this.pl_Chart);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frm_SPCompareRadarChartResult";
            this.Text = "供应商对比雷达图";
            this.Load += new System.EventHandler(this.Frm_SPCompareRadarChartResult_Load);
            this.pl_Chart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pl_Chart;
        private ChartDirector.WinChartViewer viewChart;
    }
}