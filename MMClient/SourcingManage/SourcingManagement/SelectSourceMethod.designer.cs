﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class SelectSourceMethod_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel6 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.zbff_cmb = new System.Windows.Forms.ComboBox();
            this.lbl_zbff = new System.Windows.Forms.Label();
            this.xyfs_cmb = new System.Windows.Forms.ComboBox();
            this.xufs_cmb = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cgksrq_dt = new System.Windows.Forms.DateTimePicker();
            this.cgjsrq_dt = new System.Windows.Forms.DateTimePicker();
            this.hxcgpz_cmb = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.cgy1_cmb = new System.Windows.Forms.ComboBox();
            this.cgzz1_cmb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.xmName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cgjg_tb = new System.Windows.Forms.TextBox();
            this.cgdh_tb = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.lxdh_tb = new System.Windows.Forms.TextBox();
            this.xqsl_tb = new System.Windows.Forms.TextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sqbm_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.jldw_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cqbh_cmb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cancel_bt);
            this.panel6.Controls.Add(this.submit_bt);
            this.panel6.Location = new System.Drawing.Point(28, 682);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(655, 60);
            this.panel6.TabIndex = 169;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(374, 15);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            this.cancel_bt.Click += new System.EventHandler(this.cancel_bt_Click);
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(82, 15);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "确定";
            this.submit_bt.UseVisualStyleBackColor = true;
            this.submit_bt.Click += new System.EventHandler(this.submit_bt_Click);
            // 
            // zbff_cmb
            // 
            this.zbff_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zbff_cmb.FormattingEnabled = true;
            this.zbff_cmb.Items.AddRange(new object[] {
            "两段式招标",
            "限制性招标",
            "公开招标"});
            this.zbff_cmb.Location = new System.Drawing.Point(460, 646);
            this.zbff_cmb.Name = "zbff_cmb";
            this.zbff_cmb.Size = new System.Drawing.Size(200, 22);
            this.zbff_cmb.TabIndex = 168;
            this.zbff_cmb.Text = "两段式招标";
            // 
            // lbl_zbff
            // 
            this.lbl_zbff.AutoSize = true;
            this.lbl_zbff.Font = new System.Drawing.Font("宋体", 9F);
            this.lbl_zbff.Location = new System.Drawing.Point(400, 650);
            this.lbl_zbff.Name = "lbl_zbff";
            this.lbl_zbff.Size = new System.Drawing.Size(53, 12);
            this.lbl_zbff.TabIndex = 167;
            this.lbl_zbff.Text = "招标方法";
            // 
            // xyfs_cmb
            // 
            this.xyfs_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xyfs_cmb.FormattingEnabled = true;
            this.xyfs_cmb.Items.AddRange(new object[] {
            "正式招标法",
            "非正式招标法",
            "询报价方法",
            "电子市场"});
            this.xyfs_cmb.Location = new System.Drawing.Point(110, 646);
            this.xyfs_cmb.Name = "xyfs_cmb";
            this.xyfs_cmb.Size = new System.Drawing.Size(200, 22);
            this.xyfs_cmb.TabIndex = 166;
            this.xyfs_cmb.Text = "正式招标法";
            this.xyfs_cmb.SelectedIndexChanged += new System.EventHandler(this.xyfs_cmb_SelectedIndexChanged);
            // 
            // xufs_cmb
            // 
            this.xufs_cmb.AutoSize = true;
            this.xufs_cmb.Font = new System.Drawing.Font("宋体", 9F);
            this.xufs_cmb.Location = new System.Drawing.Point(50, 650);
            this.xufs_cmb.Name = "xufs_cmb";
            this.xufs_cmb.Size = new System.Drawing.Size(53, 12);
            this.xufs_cmb.TabIndex = 165;
            this.xufs_cmb.Text = "寻源方式";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(12, 592);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(675, 31);
            this.panel5.TabIndex = 159;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(4, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "选择寻源方式";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cgksrq_dt);
            this.panel4.Controls.Add(this.cgjsrq_dt);
            this.panel4.Controls.Add(this.hxcgpz_cmb);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.cgy1_cmb);
            this.panel4.Controls.Add(this.cgzz1_cmb);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel4.Location = new System.Drawing.Point(0, 461);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(774, 125);
            this.panel4.TabIndex = 164;
            // 
            // cgksrq_dt
            // 
            this.cgksrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgksrq_dt.Location = new System.Drawing.Point(110, 51);
            this.cgksrq_dt.Name = "cgksrq_dt";
            this.cgksrq_dt.Size = new System.Drawing.Size(200, 23);
            this.cgksrq_dt.TabIndex = 147;
            // 
            // cgjsrq_dt
            // 
            this.cgjsrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgjsrq_dt.Location = new System.Drawing.Point(460, 48);
            this.cgjsrq_dt.Name = "cgjsrq_dt";
            this.cgjsrq_dt.Size = new System.Drawing.Size(200, 23);
            this.cgjsrq_dt.TabIndex = 146;
            // 
            // hxcgpz_cmb
            // 
            this.hxcgpz_cmb.FormattingEnabled = true;
            this.hxcgpz_cmb.Location = new System.Drawing.Point(109, 92);
            this.hxcgpz_cmb.Name = "hxcgpz_cmb";
            this.hxcgpz_cmb.Size = new System.Drawing.Size(201, 22);
            this.hxcgpz_cmb.TabIndex = 138;
            this.hxcgpz_cmb.Text = "采购订单";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 9F);
            this.label40.Location = new System.Drawing.Point(26, 95);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 12);
            this.label40.TabIndex = 137;
            this.label40.Text = "后需采购凭证";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 9F);
            this.label38.Location = new System.Drawing.Point(377, 53);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 12);
            this.label38.TabIndex = 134;
            this.label38.Text = "采购结束日期";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("宋体", 9F);
            this.label39.Location = new System.Drawing.Point(26, 57);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 12);
            this.label39.TabIndex = 133;
            this.label39.Text = "采购开始日期";
            // 
            // cgy1_cmb
            // 
            this.cgy1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgy1_cmb.FormattingEnabled = true;
            this.cgy1_cmb.Location = new System.Drawing.Point(460, 11);
            this.cgy1_cmb.Name = "cgy1_cmb";
            this.cgy1_cmb.Size = new System.Drawing.Size(200, 22);
            this.cgy1_cmb.TabIndex = 123;
            this.cgy1_cmb.Text = "采购员A";
            // 
            // cgzz1_cmb
            // 
            this.cgzz1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz1_cmb.FormattingEnabled = true;
            this.cgzz1_cmb.Location = new System.Drawing.Point(109, 10);
            this.cgzz1_cmb.Name = "cgzz1_cmb";
            this.cgzz1_cmb.Size = new System.Drawing.Size(201, 22);
            this.cgzz1_cmb.TabIndex = 122;
            this.cgzz1_cmb.Text = "0008";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(413, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 121;
            this.label31.Text = "采购员";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(50, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 118;
            this.label33.Text = "采购组织";
            // 
            // xmName
            // 
            this.xmName.AutoSize = true;
            this.xmName.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xmName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xmName.Location = new System.Drawing.Point(168, 29);
            this.xmName.Name = "xmName";
            this.xmName.Size = new System.Drawing.Size(173, 19);
            this.xmName.TabIndex = 160;
            this.xmName.Text = "关于****采购项目";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(12, 68);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(675, 31);
            this.panel3.TabIndex = 158;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(136, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "项目基本信息";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cgjg_tb);
            this.panel1.Controls.Add(this.cgdh_tb);
            this.panel1.Controls.Add(this.xqdh_tb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(20, 105);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 111);
            this.panel1.TabIndex = 156;
            // 
            // cgjg_tb
            // 
            this.cgjg_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgjg_tb.Location = new System.Drawing.Point(440, 87);
            this.cgjg_tb.Name = "cgjg_tb";
            this.cgjg_tb.Size = new System.Drawing.Size(200, 23);
            this.cgjg_tb.TabIndex = 165;
            this.cgjg_tb.Text = "1500";
            // 
            // cgdh_tb
            // 
            this.cgdh_tb.AutoSize = true;
            this.cgdh_tb.Font = new System.Drawing.Font("宋体", 9F);
            this.cgdh_tb.Location = new System.Drawing.Point(30, 11);
            this.cgdh_tb.Name = "cgdh_tb";
            this.cgdh_tb.Size = new System.Drawing.Size(53, 12);
            this.cgdh_tb.TabIndex = 151;
            this.cgdh_tb.Text = "采购单号";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqdh_tb.Location = new System.Drawing.Point(90, 7);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(550, 23);
            this.xqdh_tb.TabIndex = 150;
            this.xqdh_tb.Text = "PZSRM2015000279";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(380, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "采购价格";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(90, 88);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 22);
            this.cglx_cmb.TabIndex = 30;
            this.cglx_cmb.Text = "标准采购类型";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(30, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 48);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlmc_cmb.TabIndex = 28;
            this.wlmc_cmb.Text = "无缝三通325*4mm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(380, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 14;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 48);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlbh_cmb.TabIndex = 26;
            this.wlbh_cmb.Text = "F00000026";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(30, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sqrq_dt);
            this.panel2.Controls.Add(this.lxdh_tb);
            this.panel2.Controls.Add(this.xqsl_tb);
            this.panel2.Controls.Add(this.qtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.sqr_cmb);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.sqbm_cmb);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.jldw_tb);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cqbh_cmb);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(20, 183);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 286);
            this.panel2.TabIndex = 157;
            // 
            // sqrq_dt
            // 
            this.sqrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqrq_dt.Location = new System.Drawing.Point(440, 91);
            this.sqrq_dt.Name = "sqrq_dt";
            this.sqrq_dt.Size = new System.Drawing.Size(200, 23);
            this.sqrq_dt.TabIndex = 167;
            // 
            // lxdh_tb
            // 
            this.lxdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lxdh_tb.Location = new System.Drawing.Point(90, 171);
            this.lxdh_tb.Name = "lxdh_tb";
            this.lxdh_tb.Size = new System.Drawing.Size(200, 23);
            this.lxdh_tb.TabIndex = 166;
            this.lxdh_tb.Text = "81805623";
            // 
            // xqsl_tb
            // 
            this.xqsl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqsl_tb.Location = new System.Drawing.Point(90, 91);
            this.xqsl_tb.Name = "xqsl_tb";
            this.xqsl_tb.Size = new System.Drawing.Size(200, 23);
            this.xqsl_tb.TabIndex = 165;
            this.xqsl_tb.Text = "TON";
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qtyq_rtb.Location = new System.Drawing.Point(90, 212);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(550, 60);
            this.qtyq_rtb.TabIndex = 163;
            this.qtyq_rtb.Text = "无";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(30, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(380, 95);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 161;
            this.label11.Text = "申请日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(30, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 160;
            this.label12.Text = "联系电话";
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.Location = new System.Drawing.Point(440, 131);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqr_cmb.TabIndex = 159;
            this.sqr_cmb.Text = "李四";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(392, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 158;
            this.label13.Text = "申请人";
            // 
            // sqbm_cmb
            // 
            this.sqbm_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqbm_cmb.FormattingEnabled = true;
            this.sqbm_cmb.Location = new System.Drawing.Point(90, 131);
            this.sqbm_cmb.Name = "sqbm_cmb";
            this.sqbm_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqbm_cmb.TabIndex = 157;
            this.sqbm_cmb.Text = "申请部门B";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(30, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 156;
            this.label14.Text = "申请部门";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(30, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 155;
            this.label4.Text = "需求数量";
            // 
            // jldw_tb
            // 
            this.jldw_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.jldw_tb.FormattingEnabled = true;
            this.jldw_tb.Location = new System.Drawing.Point(440, 51);
            this.jldw_tb.Name = "jldw_tb";
            this.jldw_tb.Size = new System.Drawing.Size(200, 22);
            this.jldw_tb.TabIndex = 153;
            this.jldw_tb.Text = "1500";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(380, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 152;
            this.label8.Text = "计量单位";
            // 
            // cqbh_cmb
            // 
            this.cqbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cqbh_cmb.FormattingEnabled = true;
            this.cqbh_cmb.Location = new System.Drawing.Point(90, 51);
            this.cqbh_cmb.Name = "cqbh_cmb";
            this.cqbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.cqbh_cmb.TabIndex = 151;
            this.cqbh_cmb.Text = "2300";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(30, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 150;
            this.label9.Text = "仓库编号";
            // 
            // SelectSourceMethod_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(772, 856);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.zbff_cmb);
            this.Controls.Add(this.lbl_zbff);
            this.Controls.Add(this.xyfs_cmb);
            this.Controls.Add(this.xufs_cmb);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.xmName);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SelectSourceMethod_Form";
            this.Text = "选择寻源方法";
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker sqrq_dt;
        private System.Windows.Forms.TextBox lxdh_tb;
        private System.Windows.Forms.TextBox xqsl_tb;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox sqbm_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox jldw_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cqbh_cmb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label cgdh_tb;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.Label xmName;
        private System.Windows.Forms.TextBox cgjg_tb;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker cgksrq_dt;
        private System.Windows.Forms.DateTimePicker cgjsrq_dt;
        private System.Windows.Forms.ComboBox hxcgpz_cmb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cgy1_cmb;
        private System.Windows.Forms.ComboBox cgzz1_cmb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox zbff_cmb;
        private System.Windows.Forms.Label lbl_zbff;
        private System.Windows.Forms.ComboBox xyfs_cmb;
        private System.Windows.Forms.Label xufs_cmb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button submit_bt;
    }
}