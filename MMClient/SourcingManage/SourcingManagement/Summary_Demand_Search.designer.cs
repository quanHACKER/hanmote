﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Summary_Demand_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.source_view = new System.Windows.Forms.DataGridView();
            this.num = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refresh_button = new System.Windows.Forms.Button();
            this.select_button = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.grp_no = new System.Windows.Forms.GroupBox();
            this.noGridView = new System.Windows.Forms.DataGridView();
            this.noMaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noDemandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nSFactoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nsStockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noSourceStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noSourceEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Only = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).BeginInit();
            this.grp_no.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.noGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // source_view
            // 
            this.source_view.AllowUserToAddRows = false;
            this.source_view.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.source_view.BackgroundColor = System.Drawing.SystemColors.Control;
            this.source_view.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.source_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.source_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num,
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.count,
            this.measurement,
            this.factoryId,
            this.stockId,
            this.Column3,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.source_view.DefaultCellStyle = dataGridViewCellStyle5;
            this.source_view.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.source_view.EnableHeadersVisualStyles = false;
            this.source_view.Location = new System.Drawing.Point(8, 21);
            this.source_view.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.source_view.Name = "source_view";
            this.source_view.RowHeadersVisible = false;
            this.source_view.RowTemplate.Height = 23;
            this.source_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.source_view.Size = new System.Drawing.Size(1244, 268);
            this.source_view.TabIndex = 181;
            // 
            // num
            // 
            this.num.Frozen = true;
            this.num.HeaderText = "";
            this.num.Name = "num";
            this.num.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.num.Width = 20;
            // 
            // materialId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle1;
            this.materialId.Frozen = true;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.ReadOnly = true;
            this.materialId.Width = 120;
            // 
            // materialName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialName.Frozen = true;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // materialGroup
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle3;
            this.materialGroup.Frozen = true;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            this.materialGroup.ReadOnly = true;
            this.materialGroup.Width = 65;
            // 
            // count
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.count.DefaultCellStyle = dataGridViewCellStyle4;
            this.count.Frozen = true;
            this.count.HeaderText = "数量";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            this.count.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.count.Width = 55;
            // 
            // measurement
            // 
            this.measurement.Frozen = true;
            this.measurement.HeaderText = "计量单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 80;
            // 
            // factoryId
            // 
            this.factoryId.Frozen = true;
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            this.factoryId.Width = 80;
            // 
            // stockId
            // 
            this.stockId.Frozen = true;
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            this.stockId.Width = 80;
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "采购状态";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 80;
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交货开始日期";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.ReadOnly = true;
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交货结束日期";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Width = 120;
            // 
            // refresh_button
            // 
            this.refresh_button.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.refresh_button.Location = new System.Drawing.Point(1029, 14);
            this.refresh_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.refresh_button.Name = "refresh_button";
            this.refresh_button.Size = new System.Drawing.Size(53, 29);
            this.refresh_button.TabIndex = 197;
            this.refresh_button.Text = "刷新";
            this.refresh_button.UseVisualStyleBackColor = true;
            // 
            // select_button
            // 
            this.select_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.select_button.Location = new System.Drawing.Point(1112, 14);
            this.select_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.select_button.Name = "select_button";
            this.select_button.Size = new System.Drawing.Size(121, 29);
            this.select_button.TabIndex = 199;
            this.select_button.Text = "加到工作范围";
            this.select_button.UseVisualStyleBackColor = true;
            this.select_button.Click += new System.EventHandler(this.select_button_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button2.Location = new System.Drawing.Point(60, 262);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 30);
            this.button2.TabIndex = 204;
            this.button2.Text = "创建询价";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button3.Location = new System.Drawing.Point(196, 262);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 30);
            this.button3.TabIndex = 204;
            this.button3.Text = "创建招标";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button4.Location = new System.Drawing.Point(337, 262);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 30);
            this.button4.TabIndex = 204;
            this.button4.Text = "创建拍卖";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // grp_no
            // 
            this.grp_no.Controls.Add(this.noGridView);
            this.grp_no.Controls.Add(this.btn_Only);
            this.grp_no.Controls.Add(this.button4);
            this.grp_no.Controls.Add(this.button3);
            this.grp_no.Controls.Add(this.button2);
            this.grp_no.Location = new System.Drawing.Point(16, 385);
            this.grp_no.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grp_no.Name = "grp_no";
            this.grp_no.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grp_no.Size = new System.Drawing.Size(1275, 335);
            this.grp_no.TabIndex = 207;
            this.grp_no.TabStop = false;
            this.grp_no.Text = "待寻源";
            // 
            // noGridView
            // 
            this.noGridView.AllowUserToAddRows = false;
            this.noGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.noGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.noGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noMaterialId,
            this.noMaterialName,
            this.noDemandCount,
            this.unit,
            this.nSFactoryId,
            this.nsStockId,
            this.noSourceStartTime,
            this.noSourceEndTime});
            this.noGridView.Location = new System.Drawing.Point(8, 25);
            this.noGridView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.noGridView.Name = "noGridView";
            this.noGridView.RowTemplate.Height = 23;
            this.noGridView.Size = new System.Drawing.Size(1244, 231);
            this.noGridView.TabIndex = 206;
            // 
            // noMaterialId
            // 
            this.noMaterialId.HeaderText = "物料编号";
            this.noMaterialId.Name = "noMaterialId";
            this.noMaterialId.Width = 120;
            // 
            // noMaterialName
            // 
            this.noMaterialName.HeaderText = "物料名称";
            this.noMaterialName.Name = "noMaterialName";
            // 
            // noDemandCount
            // 
            this.noDemandCount.HeaderText = "需求数量";
            this.noDemandCount.Name = "noDemandCount";
            // 
            // unit
            // 
            this.unit.HeaderText = "单位";
            this.unit.Name = "unit";
            // 
            // nSFactoryId
            // 
            this.nSFactoryId.HeaderText = "工厂编号";
            this.nSFactoryId.Name = "nSFactoryId";
            // 
            // nsStockId
            // 
            this.nsStockId.HeaderText = "仓库编号";
            this.nsStockId.Name = "nsStockId";
            // 
            // noSourceStartTime
            // 
            this.noSourceStartTime.HeaderText = "交易开始日期";
            this.noSourceStartTime.Name = "noSourceStartTime";
            this.noSourceStartTime.Width = 120;
            // 
            // noSourceEndTime
            // 
            this.noSourceEndTime.HeaderText = "交易结束日期";
            this.noSourceEndTime.Name = "noSourceEndTime";
            this.noSourceEndTime.Width = 120;
            // 
            // btn_Only
            // 
            this.btn_Only.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Only.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.btn_Only.Location = new System.Drawing.Point(460, 262);
            this.btn_Only.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Only.Name = "btn_Only";
            this.btn_Only.Size = new System.Drawing.Size(100, 30);
            this.btn_Only.TabIndex = 204;
            this.btn_Only.Text = "独家供货";
            this.btn_Only.UseVisualStyleBackColor = true;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.source_view);
            this.groupBox1.Location = new System.Drawing.Point(16, 50);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1275, 328);
            this.groupBox1.TabIndex = 208;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "采购需求总览";
            // 
            // Summary_Demand_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1307, 862);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grp_no);
            this.Controls.Add(this.select_button);
            this.Controls.Add(this.refresh_button);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Summary_Demand_Search";
            this.Text = "执行货源";
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).EndInit();
            this.grp_no.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.noGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView source_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Button refresh_button;
        private System.Windows.Forms.Button select_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox grp_no;
        private System.Windows.Forms.DataGridView noGridView;
        private System.Windows.Forms.Button btn_Only;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn noMaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn noMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn noDemandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn nSFactoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn nsStockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn noSourceStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn noSourceEndTime;
    }
}