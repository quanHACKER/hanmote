﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ShowBid_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bidsGridView = new System.Windows.Forms.DataGridView();
            this.bidId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidCreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_State = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bidsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.bidsGridView);
            this.groupBox1.Location = new System.Drawing.Point(44, 82);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(819, 311);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "招标历史";
            // 
            // bidsGridView
            // 
            this.bidsGridView.AllowUserToAddRows = false;
            this.bidsGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.bidsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bidsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bidId,
            this.bidName,
            this.startTime,
            this.bidCreateTime,
            this.bidState});
            this.bidsGridView.Location = new System.Drawing.Point(34, 20);
            this.bidsGridView.Name = "bidsGridView";
            this.bidsGridView.RowTemplate.Height = 23;
            this.bidsGridView.Size = new System.Drawing.Size(734, 265);
            this.bidsGridView.TabIndex = 0;
            this.bidsGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.bidsGridView_RowPostPaint);
            this.bidsGridView.SelectionChanged += new System.EventHandler(this.bidsGridView_SelectionChanged);
            // 
            // bidId
            // 
            this.bidId.HeaderText = "招标编号";
            this.bidId.Name = "bidId";
            this.bidId.Width = 150;
            // 
            // bidName
            // 
            this.bidName.HeaderText = "招标名称";
            this.bidName.Name = "bidName";
            this.bidName.Width = 120;
            // 
            // startTime
            // 
            this.startTime.HeaderText = "开标时间";
            this.startTime.Name = "startTime";
            this.startTime.Width = 150;
            // 
            // bidCreateTime
            // 
            this.bidCreateTime.HeaderText = "创建时间";
            this.bidCreateTime.Name = "bidCreateTime";
            this.bidCreateTime.Width = 150;
            // 
            // bidState
            // 
            this.bidState.HeaderText = "状态";
            this.bidState.Name = "bidState";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(239, 39);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "招标状态";
            // 
            // cmb_State
            // 
            this.cmb_State.FormattingEnabled = true;
            this.cmb_State.Items.AddRange(new object[] {
            "待发布",
            "待报价",
            "待评标",
            "已完成"});
            this.cmb_State.Location = new System.Drawing.Point(298, 36);
            this.cmb_State.Name = "cmb_State";
            this.cmb_State.Size = new System.Drawing.Size(121, 20);
            this.cmb_State.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(467, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "查看";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ShowBid_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmb_State);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ShowBid_Form";
            this.Text = "招标历史";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bidsGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView bidsGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidName;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidCreateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_State;
        private System.Windows.Forms.Button button1;
    }
}