﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Condition_Form : DockContent
    {
        public Condition_Form()
        {
            InitializeComponent();
        }

        //生成净价
        private void button1_Click(object sender, EventArgs e)
        {
            double price = 0;
            //找出基本价格
            foreach (DataGridViewRow dgvr in conditionGridView.Rows)
            {
                if (dgvr.Cells["priceType"].Value.Equals("价格"))
                {
                    price += Convert.ToDouble(dgvr.Cells["number"].Value.ToString());
                    //四舍五入
                    if (dgvr.Cells["disType"].Value.Equals("四舍五入"))
                    {
                        price = Math.Round(price,2);
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较高值"))
                    {
                        price = Math.Ceiling(price);
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较低值"))
                    {
                        price = Math.Floor(price);
                    }
                }
            }
            //折扣或者附加费用
            foreach (DataGridViewRow dgvr in conditionGridView.Rows)
            {
                if (dgvr.Cells["priceType"].Value.Equals("折扣"))
                {
                    double temp = 0;
                    if (dgvr.Cells["disType"].Value.Equals("四舍五入"))
                    {
                        temp = Math.Round(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()),1);
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较高值"))
                    {
                        temp = Math.Ceiling(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较低值"))
                    {
                        temp = Math.Floor(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    price = price*(temp/100);
                }
                if (dgvr.Cells["priceType"].Value.Equals("附加费"))
                {
                    double tem = 0;
                    if (dgvr.Cells["disType"].Value.Equals("四舍五入"))
                    {
                        tem = Math.Round(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()), 2);
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较高值"))
                    {
                        tem = Math.Ceiling(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较低值"))
                    {
                        tem = Math.Floor(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    price = price - tem;
                }
            }
            //税收
            foreach (DataGridViewRow dgvr in conditionGridView.Rows)
            {
                if (dgvr.Cells["priceType"].Value.Equals("税收"))
                {
                    double temp = 0;
                    if (dgvr.Cells["disType"].Value.Equals("四舍五入"))
                    {
                        temp = Math.Round(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()), 2);
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较高值"))
                    {
                        temp = Math.Ceiling(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    else if (dgvr.Cells["disType"].Value.Equals("较低值"))
                    {
                        temp = Math.Floor(Convert.ToDouble(dgvr.Cells["number"].Value.ToString()));
                    }
                    price = price * (1 + temp / 100);
                }
            }
            txt_NetPrice.Text = Math.Round(price,2).ToString();
        }

        //添加一行
        private void button2_Click(object sender, EventArgs e)
        {
            conditionGridView.Rows.Add();
        }

        //删除一行
        private void button3_Click(object sender, EventArgs e)
        {
            conditionGridView.Rows.RemoveAt(conditionGridView.SelectedRows.Count);
        }
    
    }
}
