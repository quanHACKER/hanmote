﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;


namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class AddCondition_Form : DockContent
    {

        private ConditionBLL conditionBLL = new ConditionBLL();

        public AddCondition_Form()
        {
            InitializeComponent();
        }

        //添加条件类型
        private void button1_Click(object sender, EventArgs e)
        {
            ConditionType condition = new ConditionType();
            condition.ConditionId = txt_ConditionId.Text;
            condition.ConditionName = txt_ConditionName.Text;
            condition.ConditionCategory = cmb_ConditionCategory.SelectedItem.ToString();
            condition.CalType = cmb_CalType.SelectedItem.ToString();
            condition.RoundType = cmb_RoundType.SelectedItem.ToString();
            condition.Pn = cmb_Pn.SelectedItem.ToString();
            condition.Description = rtb_Description.Text;
            int res = conditionBLL.addCondition(condition);
            if (res > 0)
            {
                MessageBox.Show("条件类型添加成功");
            }
        }
    }
}
