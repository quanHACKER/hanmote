﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SubmitTechnical_Form : DockContent
    {
        public SubmitTechnical_Form()
        {
            InitializeComponent();
        }

        private void buttonUploadBid_Click(object sender, EventArgs e)
        {
            UploadFileForm uploadFileForm = new UploadFileForm();
            uploadFileForm.Show();
        }

        private void buttonDownloadBid_Click(object sender, EventArgs e)
        {
            DownloadFileForm downloadFileForm = new DownloadFileForm();
            downloadFileForm.Show();
        }

        private void buttonUploadTec_Click(object sender, EventArgs e)
        {
            UploadFileForm uploadFileForm = new UploadFileForm();
            uploadFileForm.Show();
        }

        private void buttonDownloadTec_Click(object sender, EventArgs e)
        {
            DownloadFileForm downloadFileForm = new DownloadFileForm();
            downloadFileForm.Show();
        }
    }
}
