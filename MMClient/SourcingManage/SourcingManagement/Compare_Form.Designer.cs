﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Compare_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label32 = new System.Windows.Forms.Label();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_createtime = new System.Windows.Forms.TextBox();
            this.txt_InquiryId = new System.Windows.Forms.TextBox();
            this.txt_State = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.saveOfferButton = new System.Windows.Forms.Button();
            this.compare_panel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.buyplan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.supplier_combox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.material_combox = new System.Windows.Forms.ComboBox();
            this.compare_View = new System.Windows.Forms.DataGridView();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.txt_PurchaseOrg = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroup = new System.Windows.Forms.TextBox();
            this.txt_OfferTime = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.compare_panel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compare_View)).BeginInit();
            this.SuspendLayout();
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(470, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 157;
            this.label32.Text = "询价时间";
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(5, 14);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "询价编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(5, 52);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "采购组织";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(277, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 171;
            this.label2.Text = "采购组";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(469, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 173;
            this.label4.Text = "报价期限";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txt_OfferTime);
            this.panel2.Controls.Add(this.txt_PurchaseGroup);
            this.panel2.Controls.Add(this.txt_PurchaseOrg);
            this.panel2.Controls.Add(this.txt_createtime);
            this.panel2.Controls.Add(this.txt_InquiryId);
            this.panel2.Controls.Add(this.txt_State);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(12, 11);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(726, 84);
            this.panel2.TabIndex = 180;
            // 
            // txt_createtime
            // 
            this.txt_createtime.Location = new System.Drawing.Point(535, 12);
            this.txt_createtime.Name = "txt_createtime";
            this.txt_createtime.ReadOnly = true;
            this.txt_createtime.Size = new System.Drawing.Size(145, 21);
            this.txt_createtime.TabIndex = 181;
            // 
            // txt_InquiryId
            // 
            this.txt_InquiryId.Location = new System.Drawing.Point(65, 13);
            this.txt_InquiryId.Name = "txt_InquiryId";
            this.txt_InquiryId.ReadOnly = true;
            this.txt_InquiryId.Size = new System.Drawing.Size(148, 21);
            this.txt_InquiryId.TabIndex = 180;
            // 
            // txt_State
            // 
            this.txt_State.Location = new System.Drawing.Point(324, 12);
            this.txt_State.Name = "txt_State";
            this.txt_State.ReadOnly = true;
            this.txt_State.Size = new System.Drawing.Size(100, 21);
            this.txt_State.TabIndex = 179;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(289, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 178;
            this.label16.Text = "状态";
            // 
            // saveOfferButton
            // 
            this.saveOfferButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveOfferButton.Location = new System.Drawing.Point(944, 154);
            this.saveOfferButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveOfferButton.Name = "saveOfferButton";
            this.saveOfferButton.Size = new System.Drawing.Size(63, 22);
            this.saveOfferButton.TabIndex = 194;
            this.saveOfferButton.Text = "批量保存";
            this.saveOfferButton.UseVisualStyleBackColor = true;
            // 
            // compare_panel
            // 
            this.compare_panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.compare_panel.Controls.Add(this.groupBox1);
            this.compare_panel.Controls.Add(this.compare_View);
            this.compare_panel.Location = new System.Drawing.Point(12, 110);
            this.compare_panel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.compare_panel.Name = "compare_panel";
            this.compare_panel.Size = new System.Drawing.Size(1501, 546);
            this.compare_panel.TabIndex = 195;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.buyplan);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.supplier_combox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.material_combox);
            this.groupBox1.Location = new System.Drawing.Point(878, 45);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(386, 293);
            this.groupBox1.TabIndex = 192;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料-供应商";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(143, 271);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 12);
            this.label14.TabIndex = 196;
            this.label14.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(37, 271);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 12);
            this.label13.TabIndex = 195;
            this.label13.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(159, 271);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 194;
            this.label12.Text = "种物料";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(55, 271);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 193;
            this.label11.Text = "种物料，已确定";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 271);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 192;
            this.label10.Text = "共有";
            // 
            // buyplan
            // 
            this.buyplan.Location = new System.Drawing.Point(78, 78);
            this.buyplan.Margin = new System.Windows.Forms.Padding(2);
            this.buyplan.Name = "buyplan";
            this.buyplan.ReadOnly = true;
            this.buyplan.Size = new System.Drawing.Size(114, 21);
            this.buyplan.TabIndex = 189;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(207, 122);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 12);
            this.label9.TabIndex = 191;
            // 
            // supplier_combox
            // 
            this.supplier_combox.FormattingEnabled = true;
            this.supplier_combox.Location = new System.Drawing.Point(78, 119);
            this.supplier_combox.Margin = new System.Windows.Forms.Padding(2);
            this.supplier_combox.Name = "supplier_combox";
            this.supplier_combox.Size = new System.Drawing.Size(114, 20);
            this.supplier_combox.TabIndex = 183;
            this.supplier_combox.SelectedIndexChanged += new System.EventHandler(this.supplier_combox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(207, 45);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 12);
            this.label8.TabIndex = 190;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 184;
            this.label3.Text = "确定供应商";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(135, 152);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 25);
            this.button1.TabIndex = 185;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 81);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 188;
            this.label7.Text = "采购计划";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 42);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 186;
            this.label5.Text = "物料";
            // 
            // material_combox
            // 
            this.material_combox.FormattingEnabled = true;
            this.material_combox.Location = new System.Drawing.Point(78, 42);
            this.material_combox.Margin = new System.Windows.Forms.Padding(2);
            this.material_combox.Name = "material_combox";
            this.material_combox.Size = new System.Drawing.Size(114, 20);
            this.material_combox.TabIndex = 187;
            this.material_combox.SelectedIndexChanged += new System.EventHandler(this.marerial_combox_SelectedIndexChanged);
            // 
            // compare_View
            // 
            this.compare_View.AllowUserToAddRows = false;
            this.compare_View.BackgroundColor = System.Drawing.SystemColors.Control;
            this.compare_View.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.compare_View.ColumnHeadersVisible = false;
            this.compare_View.Location = new System.Drawing.Point(14, 14);
            this.compare_View.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.compare_View.Name = "compare_View";
            this.compare_View.RowHeadersVisible = false;
            this.compare_View.RowTemplate.Height = 23;
            this.compare_View.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.compare_View.Size = new System.Drawing.Size(850, 510);
            this.compare_View.TabIndex = 182;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // txt_PurchaseOrg
            // 
            this.txt_PurchaseOrg.Location = new System.Drawing.Point(65, 52);
            this.txt_PurchaseOrg.Name = "txt_PurchaseOrg";
            this.txt_PurchaseOrg.ReadOnly = true;
            this.txt_PurchaseOrg.Size = new System.Drawing.Size(148, 21);
            this.txt_PurchaseOrg.TabIndex = 182;
            // 
            // txt_PurchaseGroup
            // 
            this.txt_PurchaseGroup.Location = new System.Drawing.Point(325, 48);
            this.txt_PurchaseGroup.Name = "txt_PurchaseGroup";
            this.txt_PurchaseGroup.ReadOnly = true;
            this.txt_PurchaseGroup.Size = new System.Drawing.Size(100, 21);
            this.txt_PurchaseGroup.TabIndex = 183;
            // 
            // txt_OfferTime
            // 
            this.txt_OfferTime.Location = new System.Drawing.Point(535, 54);
            this.txt_OfferTime.Name = "txt_OfferTime";
            this.txt_OfferTime.ReadOnly = true;
            this.txt_OfferTime.Size = new System.Drawing.Size(145, 21);
            this.txt_OfferTime.TabIndex = 184;
            // 
            // Compare_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1117, 723);
            this.Controls.Add(this.compare_panel);
            this.Controls.Add(this.saveOfferButton);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Compare_Form";
            this.Text = "采购比价";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.compare_panel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compare_View)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Button saveOfferButton;
        private System.Windows.Forms.Panel compare_panel;
        private System.Windows.Forms.DataGridView compare_View;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox supplier_combox;
        private System.Windows.Forms.ComboBox material_combox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox buyplan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_State;
        private System.Windows.Forms.TextBox txt_InquiryId;
        private System.Windows.Forms.TextBox txt_createtime;
        private System.Windows.Forms.TextBox txt_PurchaseGroup;
        private System.Windows.Forms.TextBox txt_PurchaseOrg;
        private System.Windows.Forms.TextBox txt_OfferTime;
    }
}