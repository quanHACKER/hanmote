﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.SourceingRecord;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Bll.SourcingManage;
using Lib.Model.MD.SP;
using Lib.Bll.MDBll.SP;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Compare_Form : DockContent
    {
        //询价单表头
        SourceInquiry inquiry_table = new SourceInquiry();
        

        //询价单
        List<Enquiry> enquiryList = new List<Enquiry>();
        List<SourceMaterial> xjxList = null;
        List<SourceSupplier> gysList = null;
        List<SourceSupplierMaterial> ssmList = null;

        //查询工具
        EnquiryBLL enquiryBLL = new EnquiryBLL();
        MaterialBLL materialBll = new MaterialBLL();
        Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
        SourceBLL sourceBll = new SourceBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        SourceSupplierMaterialBLL ssmBll = new SourceSupplierMaterialBLL();
        SourceItemBLL sourceItemBll = new SourceItemBLL();
        SourceResultBLL sourceIMBll = new SourceResultBLL();
        RecordInfoBLL recordInfoBll = new RecordInfoBLL();
        SourceListBLL sourceListBll = new SourceListBLL();

        SupplierBaseBLL supplierBaseBLL = new SupplierBaseBLL();
        SourceInquiryBLL sourceInquiryBll = new SourceInquiryBLL();

        //控件工具
        ConvenientTools tools = new ConvenientTools();


        //选中物料总数
        private int total;
        private int done;
        private SourceInquiry sourceInquiry;

        public Compare_Form()
        {
            InitializeComponent();
            //xjdh_cmb.Text = xjdhItems.ElementAt(xjdhItems.Count-1);
        }

        public Compare_Form(SourceInquiry source)
        {
            InitializeComponent();
            this.sourceInquiry = source;
            InitHeadInfo(source.Source_ID);
            initSourceMaterial(source.Source_ID);
            initSourceSupplier(source.Source_ID);
            initSourceSupplierMaterial(source.Source_ID);
            addCompare_view();

        }

        private void InitHeadInfo(string sourceId)
        {
            Source source = sourceBll.findSourceById(sourceId);
            txt_InquiryId.Text = source.Source_ID;
            //txt_State.Text = source.Source_State;
            txt_PurchaseOrg.Text = source.PurchaseOrg;
            txt_PurchaseGroup.Text = source.PurchaseGroup;
            txt_createtime.Text = source.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
            SourceInquiry si = sourceInquiryBll.findSourceById(sourceId);
            txt_OfferTime.Text = si.Offer_Time.ToString("yyyy-MM-dd hh:mm:ss");

        }

        private void initSourceMaterial(string sourceId)
        {
           xjxList = sourceMaterialBll.findMaterialsBySId(sourceId);
           total = xjxList.Count;
           this.label13.Text = total.ToString();
        }

        private void initSourceSupplier(string sourceId)
        {
            gysList = sourceSupplierBll.findSuppliersBySId(sourceId);
        }

        private void initSourceSupplierMaterial(string sourceId)
        {
            ssmList = ssmBll.findSSMBySId(sourceId);

        }

        /// <summary>
        /// 加载比价表格
        /// </summary>
        private void addCompare_view()
        {
            compare_View.EndEdit();
            for (int i = compare_View.Rows.Count; i > 0; i--)
            {
                compare_View.Rows.RemoveAt(i - 1);
            }
            for (int i = compare_View.Columns.Count; i > 0; i--)
            {
                compare_View.Columns.RemoveAt(i - 1);
            }
            compare_View.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            compare_View.EditMode = DataGridViewEditMode.EditOnEnter;

            //生成一个列
            DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
            textColumn.Width = 120;
            compare_View.Columns.Add(textColumn);
            textColumn = new DataGridViewTextBoxColumn();
            textColumn.Width = 100;
            compare_View.Columns.Add(textColumn);
            for (int i = 0; i < gysList.Count; i++)
            {
                textColumn = new DataGridViewTextBoxColumn();
                textColumn.Width = 100;
                compare_View.Columns.Add(textColumn);
            }

            for (int i = 0; i < (xjxList.Count + 3) * 3; i++)
            {
                compare_View.Rows.Add();
                if (i % 3 == 0)
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.GhostWhite;
                else if (i % 3 == 1)
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.AliceBlue;
                else
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.Azure;
            }
            addCompare_viewData();
            addComboxItems();
        }

        /// <summary>
        /// 填充物料和供应商信息
        /// </summary>
        private void addComboxItems()
        {
            for (int i = 0; i < xjxList.Count; i++)
            {
                if (!material_combox.Items.Contains(xjxList.ElementAt(i).Material_ID))
                {
                    material_combox.Items.Add(xjxList.ElementAt(i).Material_ID);
                }

                
            }
            for (int i = 0; i < gysList.Count; i++)
            {
                if(!supplier_combox.Items.Contains(gysList.ElementAt(i).Supplier_ID))
                {
                    supplier_combox.Items.Add(gysList.ElementAt(i).Supplier_ID);
                }                
            }

        }

        /// <summary>
        /// 加载比价数据
        /// </summary>
        private void addCompare_viewData()
        {
            //第一列
            compare_View.Rows[0].Cells[0].Value = "物料";
            compare_View.Rows[1].Cells[0].Value = "文本";
            compare_View.Rows[2].Cells[0].Value = "基本单位中的数量";
            compare_View.Rows[(1+xjxList.Count)*3+1].Cells[0].Value = "总报价";
            //第二列
            compare_View.Rows[0].Cells[1].Value = "报价：";
            compare_View.Rows[1].Cells[1].Value = "投标人：";
            compare_View.Rows[2].Cells[1].Value = "投标邀请号：";
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                compare_View.Rows[(i + 1) * 3 + 0].Cells[1].Value = "值：";
                compare_View.Rows[(i + 1) * 3 + 1].Cells[1].Value = "价格：";
                compare_View.Rows[(i + 1) * 3 + 2].Cells[1].Value = "排列：";
            }
            //供应商信息
            for (int i = 0; i < gysList.Count; i++)
            {
                //if (gysList.ElementAt(i).Price == null || gysList.ElementAt(i).Price.Equals(""))
                //    compare_View.Rows[0].Cells[i + 2].Value = "";
                //else
                //    compare_View.Rows[0].Cells[i + 2].Value = gysList.ElementAt(i).Price;
                compare_View.Rows[0].Cells[i + 2].Value = "10.0";
                compare_View.Rows[1].Cells[i + 2].Value = gysList.ElementAt(i).Supplier_ID;
                compare_View.Rows[2].Cells[i + 2].Value = sourceInquiry.Source_ID;
            }
            //物料信息
            for (int i = 0; i < xjxList.Count; i++)
            {
                SourceMaterial sm = xjxList.ElementAt(i);
                compare_View.Rows[(i + 1) * 3 + 0].Cells[0].Value = sm.Material_ID;
                compare_View.Rows[(i + 1) * 3 + 1].Cells[0].Value = sm.Material_Name;
                //compare_View.Rows[(i + 1) * 3 + 2].Cells[0].Value =
                    //Inquiry_view.Rows[i].Cells[4].Value.ToString().Trim() + "  " + Inquiry_view.Rows[i].Cells[6].Value.ToString().Trim();
                compare_View.Rows[(i + 1) * 3 + 2].Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            //值、价格
            for (int i = 0; i < gysList.Count; i++)
            {
                
                string supplierId = gysList.ElementAt(i).Supplier_ID;
                //Supplier_view_Click(i);
                for (int j = 0; j < xjxList.Count; j++)
                {
                    SourceMaterial sm = xjxList.ElementAt(j);
                    compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Value = "";
                    for (int k = 0; k < ssmList.Count; k++)
                    {
                        SourceSupplierMaterial ssm = ssmList.ElementAt(k);
                        if (ssm.Supplier_ID.Equals(supplierId) && ssm.Material_ID.Equals(sm.Material_ID))
                        {
                            compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Value = sm.Demand_Count * ssm.Price;
                            compare_View.Rows[(j + 1) * 3 + 1].Cells[i + 2].Value = ssm.Price;
                        }
                    }
                    compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    compare_View.Rows[(j + 1) * 3 + 1].Cells[i + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;  
                }
            }

            //排列
            double[,] percent = new double[xjxList.Count + 1, gysList.Count];
            int[,] rank = new int[xjxList.Count + 1, gysList.Count];
            //计算百分比
            for (int i = 0; i < xjxList.Count; i++)
            {
                int count = 0;
                double sum = 0.0;
                for (int j = 0; j < gysList.Count; j++)
                {
                    if (tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]) > 0)
                    {
                        count++;
                        sum += tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]);
                    }
                }
                if (count > 0)
                {
                    sum = sum / (double)count;
                    for(int j = 0; j < gysList.Count; j++)
                    {
                        percent[i, j] = tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]) / sum;
                    }
                }
            }
            //计算报价总和
            double[] supplierSum = new double[gysList.Count];
            for (int j = 0; j < gysList.Count; j++)
            {
                supplierSum[j] = 0.0;
                for (int i = 0; i < xjxList.Count; i++)
                {
                    supplierSum[j] += tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]);
                }
                if (supplierSum[j]<=0)
                    compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Value = "";
                else
                    compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Value = supplierSum[j];
                compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            int supCount = 0;
            double supSum = 0.0;
            for (int j = 0; j < gysList.Count; j++)
            {
                if (supplierSum[j] > 0)
                {
                    supCount++;
                    supSum += supplierSum[j];
                }
            }
            if (supCount > 0)
            {
                supSum = supSum / (double)supCount;
                for (int j = 0; j < gysList.Count; j++)
                {
                    percent[xjxList.Count, j] = tools.boxToDouble(compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2]) / supSum;
                }
            }
            //排名
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                for (int j = 0; j < gysList.Count; j++)
                {
                    rank[i,j]=1;
                    for (int k = 0; k < gysList.Count; k++)
                    {
                        if (percent[i, j] > percent[i, k])
                            rank[i, j]++;
                    }
                }
            }
            //显示排列
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                for (int j = 0; j < gysList.Count; j++)
                {
                    compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    if (percent[i, j] > 0)
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Value =
                            rank[i, j] + "    " + Math.Round(percent[i, j],2)*100 + " " + "%";
                    else
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Value = "";
                    if (rank[i, j] == 1)
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Style.ForeColor = Color.Red;
                }
            }
        }

        //物料编号改变
        private void marerial_combox_SelectedIndexChanged(object sender, EventArgs e)
        {
            String materialid = this.material_combox.Text.ToString().Trim(); 
            MaterialBase mb = materialBll.getMaterialBaseById(materialid);
            this.label8.Text = mb.Material_Name;
        }

        private void supplier_combox_SelectedIndexChanged(object sender, EventArgs e)
        {
            String supperid = this.supplier_combox.Text.ToString().Trim();
            for (int i = 0; i < gysList.Count; i++)
            {
                if (gysList.ElementAt(i).Supplier_ID.Equals(supperid))
                {
                    EnquiryDAL ed = new EnquiryDAL();
                    DataTable dt = ed.FindSupplierById(supperid);
                    this.label9.Text = dt.Rows[0][3].ToString();
                    break;
                }
            }
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.material_combox.SelectedIndex < 0|| this.supplier_combox.SelectedIndex<0)
            {
                MessageBox.Show("请选择物料编号和供应商编号");
                return;
            }
            int simresu = 0;
            string supplier_id = this.supplier_combox.Text;
            string sourceID = txt_InquiryId.Text;
            //int result = sd.editDemandById(this.buyplan.Text, supplier_id, inquiry_id);
            /*
            SourceItem si  = sourceItemBll.ExistsBySSId(sourceID, supplier_id);
            int res = 0;
            int simresu = 0;
            //寻源子项表中没有表示该供应商的记录存在
            if (si == null)
            {
                si = new SourceItem();
                si.Item_ID = "TX"+tools.systemTimeToStr();
                si.Source_ID = sourceID;
                si.Supplier_ID = supplier_id;
                SourceMaterial tempSm = sourceMaterialBll.findSMBySIdMId(sourceID, material_combox.Text);
                Summary_Demand tempSD = summaryDemandBll.findSummaryDemandByDemandID(tempSm.Demand_ID);
                //需求的工厂
                si.Factory_ID = tempSD.Factory_ID;
                si.State = 0;
                //添加子项
                res = sourceItemBll.addSourceItem(si);
            }
            **/
                SourceResult sim = new SourceResult();
                sim.Item_ID = "TX" + tools.systemTimeToStr();
                sim.Source_ID = sourceID;
                sim.Supplier_ID = supplier_id;
                sim.Material_ID = material_combox.Text;
                MaterialBase mb = materialBll.getMaterialBaseById(material_combox.Text);
                sim.Material_Name = mb.Material_Name;
                sim.Material_Group = mb.Material_Group;
                sim.Measurement = mb.Measurement;
                sim.Net_Price_Unit = mb.Measurement;
                for(int i=0;i<xjxList.Count;i++)
                {
                    SourceMaterial  sm = xjxList.ElementAt(i);
                    if(sm.Material_ID.Equals(material_combox.Text))
                    {
                        sim.Buy_Number = sm.Demand_Count;
                        sim.Provider_Number = sm.Demand_Count;
                        break;
                    }
                }
                for(int i=0;i<ssmList.Count;i++)
                {
                    SourceSupplierMaterial ssm = ssmList.ElementAt(i);
                    if(ssm.Supplier_ID.Equals(supplier_id) && ssm.Material_ID.Equals(material_combox.Text))
                    {
                        sim.Net_Price = ssm.Price;
                        break;
                    }
                }
                
                simresu = sourceIMBll.addSourceResult(sim);

                RecordInfo ri = new RecordInfo();
                ri.RecordInfoId = tools.systemTimeToStr();//生成一个采购信息记录编号
                ri.SupplierId = supplier_id;
                SupplierBase sb = supplierBaseBLL.GetSupplierBasicInformation(ri.SupplierId);
                ri.SupplierName = sb.Supplier_Name;//根据供应商编号查询供应商名称
                Source source = sourceBll.findSourceById(sourceID);
                ri.PurchaseOrg = source.PurchaseOrg;
                ri.PurchaseGroup = source.PurchaseGroup;
                ri.MaterialId = material_combox.Text;
                ri.MaterialName = mb.Material_Name;
                for (int i = 0; i < xjxList.Count; i++)
                {
                    SourceMaterial sm = xjxList.ElementAt(i);
                    if(sm.Material_ID.Equals(ri.MaterialId)){
                        ri.FactoryId = sm.FactoryId;
                        ri.StockId = sm.Source_ID;
                    }
                }
                ri.DemandCount = sim.Buy_Number;
                ri.FromType = 1;
                ri.FromId = sourceID;
                ri.ContactState = "无效";
                ri.NetPrice = sim.Net_Price;
            SourceMaterial tsm = sourceMaterialBll.findSMBySIdMId(sourceID,ri.MaterialId);
            ri.StartTime = tsm.DeliveryStartTime;
            ri.EndTime = tsm.DeliveryEndTime;
                recordInfoBll.addRecordInfo(ri);

            SourceList sl = new SourceList();
            sl.SourceListId = tools.systemTimeToStr();
            sl.Supplier_ID = ri.SupplierId;
            sl.Material_ID = ri.MaterialId;
            sl.Supplier_ID = ri.SupplierId;
            sl.Factory_ID = tsm.FactoryId;
            sl.Blk = 0;
            sl.MRP = 0;
            sl.PurchaseOrg = ri.PurchaseOrg;
            sl.StartTime = ri.StartTime;
            sl.EndTime = ri.EndTime;
            sourceListBll.addSourceList(sl);
            MessageBox.Show("货源清单记录成功");

            if (simresu > 0)
            {
                done++;
                label14.Text = done.ToString();
                if (done < total)
                {
                    MessageBox.Show("更新成功");  
                }
                else
                {
                    String inid = this.txt_InquiryId.Text.ToString();
                    int res = 4;

                    //更新数据库
                    int re = sourceInquiryBll.updateSourceState(sourceInquiry.Source_ID, res);
                    if (re > 0)
                    {
                        MessageBox.Show(this.txt_InquiryId.Text + "比价完成");
                    }
                    else
                    {
                        MessageBox.Show("更新数据表失败","提示");
                    }
                }
                
            }
            else
            {
                MessageBox.Show("更新失败");
            }

        }

        
    }
}
