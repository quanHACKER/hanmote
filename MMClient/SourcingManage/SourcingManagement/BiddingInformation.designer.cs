﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class BiddingInformation_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.pgff_cmb = new System.Windows.Forms.ComboBox();
            this.sjbg_panel = new System.Windows.Forms.Panel();
            this.shp3_lbl = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.hlca_zd = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox142 = new System.Windows.Forms.TextBox();
            this.textBox141 = new System.Windows.Forms.TextBox();
            this.textBox140 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox139 = new System.Windows.Forms.TextBox();
            this.textBox146 = new System.Windows.Forms.TextBox();
            this.textBox145 = new System.Windows.Forms.TextBox();
            this.textBox144 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.textBox143 = new System.Windows.Forms.TextBox();
            this.textBox150 = new System.Windows.Forms.TextBox();
            this.textBox149 = new System.Windows.Forms.TextBox();
            this.textBox148 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.textBox147 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.textBox158 = new System.Windows.Forms.TextBox();
            this.textBox157 = new System.Windows.Forms.TextBox();
            this.textBox156 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.textBox155 = new System.Windows.Forms.TextBox();
            this.textBox154 = new System.Windows.Forms.TextBox();
            this.textBox153 = new System.Windows.Forms.TextBox();
            this.textBox152 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.textBox151 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.qlca_ed = new System.Windows.Forms.TextBox();
            this.qlca_zx = new System.Windows.Forms.TextBox();
            this.qlca_zd = new System.Windows.Forms.TextBox();
            this.textBox290 = new System.Windows.Forms.TextBox();
            this.textBox289 = new System.Windows.Forms.TextBox();
            this.textBox288 = new System.Windows.Forms.TextBox();
            this.textBox287 = new System.Windows.Forms.TextBox();
            this.textBox286 = new System.Windows.Forms.TextBox();
            this.textBox285 = new System.Windows.Forms.TextBox();
            this.textBox284 = new System.Windows.Forms.TextBox();
            this.textBox283 = new System.Windows.Forms.TextBox();
            this.textBox282 = new System.Windows.Forms.TextBox();
            this.textBox281 = new System.Windows.Forms.TextBox();
            this.textBox280 = new System.Windows.Forms.TextBox();
            this.textBox279 = new System.Windows.Forms.TextBox();
            this.textBox242 = new System.Windows.Forms.TextBox();
            this.textBox239 = new System.Windows.Forms.TextBox();
            this.textBox236 = new System.Windows.Forms.TextBox();
            this.textBox233 = new System.Windows.Forms.TextBox();
            this.textBox306 = new System.Windows.Forms.TextBox();
            this.textBox305 = new System.Windows.Forms.TextBox();
            this.textBox304 = new System.Windows.Forms.TextBox();
            this.textBox303 = new System.Windows.Forms.TextBox();
            this.textBox302 = new System.Windows.Forms.TextBox();
            this.textBox301 = new System.Windows.Forms.TextBox();
            this.textBox300 = new System.Windows.Forms.TextBox();
            this.textBox299 = new System.Windows.Forms.TextBox();
            this.textBox298 = new System.Windows.Forms.TextBox();
            this.textBox297 = new System.Windows.Forms.TextBox();
            this.textBox296 = new System.Windows.Forms.TextBox();
            this.textBox295 = new System.Windows.Forms.TextBox();
            this.textBox294 = new System.Windows.Forms.TextBox();
            this.textBox293 = new System.Windows.Forms.TextBox();
            this.textBox292 = new System.Windows.Forms.TextBox();
            this.textBox291 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox327 = new System.Windows.Forms.TextBox();
            this.textBox326 = new System.Windows.Forms.TextBox();
            this.textBox325 = new System.Windows.Forms.TextBox();
            this.textBox324 = new System.Windows.Forms.TextBox();
            this.textBox323 = new System.Windows.Forms.TextBox();
            this.textBox322 = new System.Windows.Forms.TextBox();
            this.textBox321 = new System.Windows.Forms.TextBox();
            this.textBox320 = new System.Windows.Forms.TextBox();
            this.textBox319 = new System.Windows.Forms.TextBox();
            this.textBox318 = new System.Windows.Forms.TextBox();
            this.textBox317 = new System.Windows.Forms.TextBox();
            this.textBox316 = new System.Windows.Forms.TextBox();
            this.textBox315 = new System.Windows.Forms.TextBox();
            this.textBox314 = new System.Windows.Forms.TextBox();
            this.textBox313 = new System.Windows.Forms.TextBox();
            this.textBox312 = new System.Windows.Forms.TextBox();
            this.textBox311 = new System.Windows.Forms.TextBox();
            this.textBox310 = new System.Windows.Forms.TextBox();
            this.textBox309 = new System.Windows.Forms.TextBox();
            this.textBox308 = new System.Windows.Forms.TextBox();
            this.textBox307 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox367 = new System.Windows.Forms.TextBox();
            this.textBox366 = new System.Windows.Forms.TextBox();
            this.textBox365 = new System.Windows.Forms.TextBox();
            this.textBox364 = new System.Windows.Forms.TextBox();
            this.textBox363 = new System.Windows.Forms.TextBox();
            this.textBox362 = new System.Windows.Forms.TextBox();
            this.textBox361 = new System.Windows.Forms.TextBox();
            this.textBox360 = new System.Windows.Forms.TextBox();
            this.textBox359 = new System.Windows.Forms.TextBox();
            this.textBox358 = new System.Windows.Forms.TextBox();
            this.textBox355 = new System.Windows.Forms.TextBox();
            this.textBox352 = new System.Windows.Forms.TextBox();
            this.textBox348 = new System.Windows.Forms.TextBox();
            this.textBox347 = new System.Windows.Forms.TextBox();
            this.textBox346 = new System.Windows.Forms.TextBox();
            this.textBox345 = new System.Windows.Forms.TextBox();
            this.textBox344 = new System.Windows.Forms.TextBox();
            this.textBox343 = new System.Windows.Forms.TextBox();
            this.textBox342 = new System.Windows.Forms.TextBox();
            this.textBox341 = new System.Windows.Forms.TextBox();
            this.textBox340 = new System.Windows.Forms.TextBox();
            this.textBox331 = new System.Windows.Forms.TextBox();
            this.textBox417 = new System.Windows.Forms.TextBox();
            this.textBox416 = new System.Windows.Forms.TextBox();
            this.textBox415 = new System.Windows.Forms.TextBox();
            this.textBox408 = new System.Windows.Forms.TextBox();
            this.textBox407 = new System.Windows.Forms.TextBox();
            this.textBox406 = new System.Windows.Forms.TextBox();
            this.textBox405 = new System.Windows.Forms.TextBox();
            this.textBox404 = new System.Windows.Forms.TextBox();
            this.textBox403 = new System.Windows.Forms.TextBox();
            this.textBox402 = new System.Windows.Forms.TextBox();
            this.textBox401 = new System.Windows.Forms.TextBox();
            this.textBox400 = new System.Windows.Forms.TextBox();
            this.textBox390 = new System.Windows.Forms.TextBox();
            this.textBox389 = new System.Windows.Forms.TextBox();
            this.textBox388 = new System.Windows.Forms.TextBox();
            this.textBox387 = new System.Windows.Forms.TextBox();
            this.textBox386 = new System.Windows.Forms.TextBox();
            this.textBox385 = new System.Windows.Forms.TextBox();
            this.textBox384 = new System.Windows.Forms.TextBox();
            this.textBox383 = new System.Windows.Forms.TextBox();
            this.textBox382 = new System.Windows.Forms.TextBox();
            this.textBox414 = new System.Windows.Forms.TextBox();
            this.textBox399 = new System.Windows.Forms.TextBox();
            this.textBox398 = new System.Windows.Forms.TextBox();
            this.textBox397 = new System.Windows.Forms.TextBox();
            this.textBox381 = new System.Windows.Forms.TextBox();
            this.textBox380 = new System.Windows.Forms.TextBox();
            this.textBox379 = new System.Windows.Forms.TextBox();
            this.textBox357 = new System.Windows.Forms.TextBox();
            this.textBox356 = new System.Windows.Forms.TextBox();
            this.textBox339 = new System.Windows.Forms.TextBox();
            this.textBox338 = new System.Windows.Forms.TextBox();
            this.textBox337 = new System.Windows.Forms.TextBox();
            this.textBox412 = new System.Windows.Forms.TextBox();
            this.textBox375 = new System.Windows.Forms.TextBox();
            this.textBox374 = new System.Windows.Forms.TextBox();
            this.textBox373 = new System.Windows.Forms.TextBox();
            this.textBox372 = new System.Windows.Forms.TextBox();
            this.textBox371 = new System.Windows.Forms.TextBox();
            this.textBox370 = new System.Windows.Forms.TextBox();
            this.textBox333 = new System.Windows.Forms.TextBox();
            this.textBox332 = new System.Windows.Forms.TextBox();
            this.textBox330 = new System.Windows.Forms.TextBox();
            this.textBox329 = new System.Windows.Forms.TextBox();
            this.textBox328 = new System.Windows.Forms.TextBox();
            this.textBox413 = new System.Windows.Forms.TextBox();
            this.textBox396 = new System.Windows.Forms.TextBox();
            this.textBox395 = new System.Windows.Forms.TextBox();
            this.textBox394 = new System.Windows.Forms.TextBox();
            this.textBox378 = new System.Windows.Forms.TextBox();
            this.textBox377 = new System.Windows.Forms.TextBox();
            this.textBox376 = new System.Windows.Forms.TextBox();
            this.textBox354 = new System.Windows.Forms.TextBox();
            this.textBox353 = new System.Windows.Forms.TextBox();
            this.textBox336 = new System.Windows.Forms.TextBox();
            this.textBox335 = new System.Windows.Forms.TextBox();
            this.textBox334 = new System.Windows.Forms.TextBox();
            this.textBox418 = new System.Windows.Forms.TextBox();
            this.textBox411 = new System.Windows.Forms.TextBox();
            this.textBox410 = new System.Windows.Forms.TextBox();
            this.textBox409 = new System.Windows.Forms.TextBox();
            this.textBox393 = new System.Windows.Forms.TextBox();
            this.textBox392 = new System.Windows.Forms.TextBox();
            this.textBox391 = new System.Windows.Forms.TextBox();
            this.textBox369 = new System.Windows.Forms.TextBox();
            this.textBox368 = new System.Windows.Forms.TextBox();
            this.textBox351 = new System.Windows.Forms.TextBox();
            this.textBox350 = new System.Windows.Forms.TextBox();
            this.textBox349 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox478 = new System.Windows.Forms.TextBox();
            this.textBox477 = new System.Windows.Forms.TextBox();
            this.textBox476 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox475 = new System.Windows.Forms.TextBox();
            this.textBox474 = new System.Windows.Forms.TextBox();
            this.textBox473 = new System.Windows.Forms.TextBox();
            this.textBox472 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox471 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox470 = new System.Windows.Forms.TextBox();
            this.textBox469 = new System.Windows.Forms.TextBox();
            this.textBox468 = new System.Windows.Forms.TextBox();
            this.textBox467 = new System.Windows.Forms.TextBox();
            this.textBox466 = new System.Windows.Forms.TextBox();
            this.textBox465 = new System.Windows.Forms.TextBox();
            this.textBox464 = new System.Windows.Forms.TextBox();
            this.textBox463 = new System.Windows.Forms.TextBox();
            this.textBox462 = new System.Windows.Forms.TextBox();
            this.textBox461 = new System.Windows.Forms.TextBox();
            this.textBox460 = new System.Windows.Forms.TextBox();
            this.textBox459 = new System.Windows.Forms.TextBox();
            this.textBox458 = new System.Windows.Forms.TextBox();
            this.textBox457 = new System.Windows.Forms.TextBox();
            this.textBox456 = new System.Windows.Forms.TextBox();
            this.textBox455 = new System.Windows.Forms.TextBox();
            this.textBox454 = new System.Windows.Forms.TextBox();
            this.textBox453 = new System.Windows.Forms.TextBox();
            this.textBox452 = new System.Windows.Forms.TextBox();
            this.textBox451 = new System.Windows.Forms.TextBox();
            this.textBox450 = new System.Windows.Forms.TextBox();
            this.textBox449 = new System.Windows.Forms.TextBox();
            this.textBox448 = new System.Windows.Forms.TextBox();
            this.textBox447 = new System.Windows.Forms.TextBox();
            this.textBox446 = new System.Windows.Forms.TextBox();
            this.textBox445 = new System.Windows.Forms.TextBox();
            this.textBox444 = new System.Windows.Forms.TextBox();
            this.textBox443 = new System.Windows.Forms.TextBox();
            this.textBox442 = new System.Windows.Forms.TextBox();
            this.textBox441 = new System.Windows.Forms.TextBox();
            this.textBox440 = new System.Windows.Forms.TextBox();
            this.textBox439 = new System.Windows.Forms.TextBox();
            this.textBox438 = new System.Windows.Forms.TextBox();
            this.textBox437 = new System.Windows.Forms.TextBox();
            this.textBox436 = new System.Windows.Forms.TextBox();
            this.textBox435 = new System.Windows.Forms.TextBox();
            this.textBox434 = new System.Windows.Forms.TextBox();
            this.textBox433 = new System.Windows.Forms.TextBox();
            this.textBox432 = new System.Windows.Forms.TextBox();
            this.textBox431 = new System.Windows.Forms.TextBox();
            this.textBox430 = new System.Windows.Forms.TextBox();
            this.textBox429 = new System.Windows.Forms.TextBox();
            this.textBox428 = new System.Windows.Forms.TextBox();
            this.textBox427 = new System.Windows.Forms.TextBox();
            this.textBox426 = new System.Windows.Forms.TextBox();
            this.textBox425 = new System.Windows.Forms.TextBox();
            this.textBox424 = new System.Windows.Forms.TextBox();
            this.textBox423 = new System.Windows.Forms.TextBox();
            this.textBox422 = new System.Windows.Forms.TextBox();
            this.textBox421 = new System.Windows.Forms.TextBox();
            this.textBox420 = new System.Windows.Forms.TextBox();
            this.textBox419 = new System.Windows.Forms.TextBox();
            this.zdjgf_panel = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox159 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox186 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox226 = new System.Windows.Forms.TextBox();
            this.textBox224 = new System.Windows.Forms.TextBox();
            this.textBox222 = new System.Windows.Forms.TextBox();
            this.textBox220 = new System.Windows.Forms.TextBox();
            this.textBox206 = new System.Windows.Forms.TextBox();
            this.textBox204 = new System.Windows.Forms.TextBox();
            this.textBox202 = new System.Windows.Forms.TextBox();
            this.textBox200 = new System.Windows.Forms.TextBox();
            this.textBox260 = new System.Windows.Forms.TextBox();
            this.textBox258 = new System.Windows.Forms.TextBox();
            this.textBox255 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox198 = new System.Windows.Forms.TextBox();
            this.textBox197 = new System.Windows.Forms.TextBox();
            this.textBox196 = new System.Windows.Forms.TextBox();
            this.textBox195 = new System.Windows.Forms.TextBox();
            this.textBox194 = new System.Windows.Forms.TextBox();
            this.textBox192 = new System.Windows.Forms.TextBox();
            this.textBox189 = new System.Windows.Forms.TextBox();
            this.textBox188 = new System.Windows.Forms.TextBox();
            this.textBox185 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox231 = new System.Windows.Forms.TextBox();
            this.textBox230 = new System.Windows.Forms.TextBox();
            this.textBox229 = new System.Windows.Forms.TextBox();
            this.textBox228 = new System.Windows.Forms.TextBox();
            this.textBox227 = new System.Windows.Forms.TextBox();
            this.textBox225 = new System.Windows.Forms.TextBox();
            this.textBox223 = new System.Windows.Forms.TextBox();
            this.textBox221 = new System.Windows.Forms.TextBox();
            this.textBox219 = new System.Windows.Forms.TextBox();
            this.textBox218 = new System.Windows.Forms.TextBox();
            this.textBox217 = new System.Windows.Forms.TextBox();
            this.textBox216 = new System.Windows.Forms.TextBox();
            this.textBox215 = new System.Windows.Forms.TextBox();
            this.textBox214 = new System.Windows.Forms.TextBox();
            this.textBox213 = new System.Windows.Forms.TextBox();
            this.textBox212 = new System.Windows.Forms.TextBox();
            this.textBox211 = new System.Windows.Forms.TextBox();
            this.textBox210 = new System.Windows.Forms.TextBox();
            this.textBox209 = new System.Windows.Forms.TextBox();
            this.textBox208 = new System.Windows.Forms.TextBox();
            this.textBox207 = new System.Windows.Forms.TextBox();
            this.textBox205 = new System.Windows.Forms.TextBox();
            this.textBox203 = new System.Windows.Forms.TextBox();
            this.textBox199 = new System.Windows.Forms.TextBox();
            this.textBox278 = new System.Windows.Forms.TextBox();
            this.textBox277 = new System.Windows.Forms.TextBox();
            this.textBox276 = new System.Windows.Forms.TextBox();
            this.textBox275 = new System.Windows.Forms.TextBox();
            this.textBox274 = new System.Windows.Forms.TextBox();
            this.textBox273 = new System.Windows.Forms.TextBox();
            this.textBox272 = new System.Windows.Forms.TextBox();
            this.textBox271 = new System.Windows.Forms.TextBox();
            this.textBox270 = new System.Windows.Forms.TextBox();
            this.textBox269 = new System.Windows.Forms.TextBox();
            this.textBox268 = new System.Windows.Forms.TextBox();
            this.textBox267 = new System.Windows.Forms.TextBox();
            this.textBox266 = new System.Windows.Forms.TextBox();
            this.textBox265 = new System.Windows.Forms.TextBox();
            this.textBox264 = new System.Windows.Forms.TextBox();
            this.textBox263 = new System.Windows.Forms.TextBox();
            this.textBox262 = new System.Windows.Forms.TextBox();
            this.textBox261 = new System.Windows.Forms.TextBox();
            this.textBox259 = new System.Windows.Forms.TextBox();
            this.textBox257 = new System.Windows.Forms.TextBox();
            this.textBox256 = new System.Windows.Forms.TextBox();
            this.textBox254 = new System.Windows.Forms.TextBox();
            this.textBox253 = new System.Windows.Forms.TextBox();
            this.textBox252 = new System.Windows.Forms.TextBox();
            this.textBox251 = new System.Windows.Forms.TextBox();
            this.textBox250 = new System.Windows.Forms.TextBox();
            this.textBox249 = new System.Windows.Forms.TextBox();
            this.textBox248 = new System.Windows.Forms.TextBox();
            this.textBox247 = new System.Windows.Forms.TextBox();
            this.textBox246 = new System.Windows.Forms.TextBox();
            this.textBox245 = new System.Windows.Forms.TextBox();
            this.textBox244 = new System.Windows.Forms.TextBox();
            this.textBox243 = new System.Windows.Forms.TextBox();
            this.textBox241 = new System.Windows.Forms.TextBox();
            this.textBox240 = new System.Windows.Forms.TextBox();
            this.textBox238 = new System.Windows.Forms.TextBox();
            this.textBox237 = new System.Windows.Forms.TextBox();
            this.textBox235 = new System.Windows.Forms.TextBox();
            this.textBox234 = new System.Windows.Forms.TextBox();
            this.textBox232 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.zbs_tb = new System.Windows.Forms.TextBox();
            this.zdsyqcbf_panel = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.zx_lbl = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.zd_lbl = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.shp2_lbl = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox191 = new System.Windows.Forms.TextBox();
            this.hlab_zx = new System.Windows.Forms.TextBox();
            this.textBox190 = new System.Windows.Forms.TextBox();
            this.hlab_ed = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox187 = new System.Windows.Forms.TextBox();
            this.textBox177 = new System.Windows.Forms.TextBox();
            this.textBox176 = new System.Windows.Forms.TextBox();
            this.textBox133 = new System.Windows.Forms.TextBox();
            this.textBox128 = new System.Windows.Forms.TextBox();
            this.textBox125 = new System.Windows.Forms.TextBox();
            this.textBox124 = new System.Windows.Forms.TextBox();
            this.textBox112 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox132 = new System.Windows.Forms.TextBox();
            this.textBox165 = new System.Windows.Forms.TextBox();
            this.textBox164 = new System.Windows.Forms.TextBox();
            this.textBox160 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox201 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox193 = new System.Windows.Forms.TextBox();
            this.textBox129 = new System.Windows.Forms.TextBox();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.textBox111 = new System.Windows.Forms.TextBox();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox173 = new System.Windows.Forms.TextBox();
            this.textBox172 = new System.Windows.Forms.TextBox();
            this.textBox171 = new System.Windows.Forms.TextBox();
            this.textBox166 = new System.Windows.Forms.TextBox();
            this.textBox163 = new System.Windows.Forms.TextBox();
            this.textBox162 = new System.Windows.Forms.TextBox();
            this.textBox161 = new System.Windows.Forms.TextBox();
            this.textBox137 = new System.Windows.Forms.TextBox();
            this.textBox136 = new System.Windows.Forms.TextBox();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox183 = new System.Windows.Forms.TextBox();
            this.textBox182 = new System.Windows.Forms.TextBox();
            this.textBox181 = new System.Windows.Forms.TextBox();
            this.textBox180 = new System.Windows.Forms.TextBox();
            this.textBox179 = new System.Windows.Forms.TextBox();
            this.textBox178 = new System.Windows.Forms.TextBox();
            this.textBox175 = new System.Windows.Forms.TextBox();
            this.textBox174 = new System.Windows.Forms.TextBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox184 = new System.Windows.Forms.TextBox();
            this.textBox170 = new System.Windows.Forms.TextBox();
            this.textBox169 = new System.Windows.Forms.TextBox();
            this.textBox168 = new System.Windows.Forms.TextBox();
            this.textBox167 = new System.Windows.Forms.TextBox();
            this.textBox122 = new System.Windows.Forms.TextBox();
            this.textBox121 = new System.Windows.Forms.TextBox();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.query_bt = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.textBox138 = new System.Windows.Forms.TextBox();
            this.textBox135 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox134 = new System.Windows.Forms.TextBox();
            this.textBox131 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox130 = new System.Windows.Forms.TextBox();
            this.textBox127 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox126 = new System.Windows.Forms.TextBox();
            this.textBox123 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gsbz_tb = new System.Windows.Forms.TextBox();
            this.zt_tb = new System.Windows.Forms.TextBox();
            this.RFxbh_tb = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.close_bt = new System.Windows.Forms.Button();
            this.save_bt = new System.Windows.Forms.Button();
            this.send_bt = new System.Windows.Forms.Button();
            this.detail_bt = new System.Windows.Forms.Button();
            this.refresh_bt = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabPage3.SuspendLayout();
            this.sjbg_panel.SuspendLayout();
            this.zdjgf_panel.SuspendLayout();
            this.zdsyqcbf_panel.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.zdsyqcbf_panel);
            this.tabPage3.Controls.Add(this.zbs_tb);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.zdjgf_panel);
            this.tabPage3.Controls.Add(this.sjbg_panel);
            this.tabPage3.Controls.Add(this.pgff_cmb);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(903, 1790);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "投标信息";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(19, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 79;
            this.label14.Text = "评估方法";
            // 
            // pgff_cmb
            // 
            this.pgff_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pgff_cmb.FormattingEnabled = true;
            this.pgff_cmb.ItemHeight = 14;
            this.pgff_cmb.Items.AddRange(new object[] {
            "加权平均法",
            "最低价格法",
            "最低所有权成本法"});
            this.pgff_cmb.Location = new System.Drawing.Point(78, 13);
            this.pgff_cmb.Name = "pgff_cmb";
            this.pgff_cmb.Size = new System.Drawing.Size(200, 22);
            this.pgff_cmb.TabIndex = 91;
            this.pgff_cmb.Text = "加权平均法";
            this.pgff_cmb.SelectedIndexChanged += new System.EventHandler(this.pgff_cmb_SelectedIndexChanged);
            // 
            // sjbg_panel
            // 
            this.sjbg_panel.Controls.Add(this.textBox419);
            this.sjbg_panel.Controls.Add(this.textBox420);
            this.sjbg_panel.Controls.Add(this.textBox421);
            this.sjbg_panel.Controls.Add(this.textBox422);
            this.sjbg_panel.Controls.Add(this.textBox423);
            this.sjbg_panel.Controls.Add(this.textBox424);
            this.sjbg_panel.Controls.Add(this.textBox425);
            this.sjbg_panel.Controls.Add(this.textBox426);
            this.sjbg_panel.Controls.Add(this.textBox427);
            this.sjbg_panel.Controls.Add(this.textBox428);
            this.sjbg_panel.Controls.Add(this.textBox429);
            this.sjbg_panel.Controls.Add(this.textBox430);
            this.sjbg_panel.Controls.Add(this.textBox431);
            this.sjbg_panel.Controls.Add(this.textBox432);
            this.sjbg_panel.Controls.Add(this.textBox433);
            this.sjbg_panel.Controls.Add(this.textBox434);
            this.sjbg_panel.Controls.Add(this.textBox435);
            this.sjbg_panel.Controls.Add(this.textBox436);
            this.sjbg_panel.Controls.Add(this.textBox437);
            this.sjbg_panel.Controls.Add(this.textBox438);
            this.sjbg_panel.Controls.Add(this.textBox439);
            this.sjbg_panel.Controls.Add(this.textBox440);
            this.sjbg_panel.Controls.Add(this.textBox441);
            this.sjbg_panel.Controls.Add(this.textBox442);
            this.sjbg_panel.Controls.Add(this.textBox443);
            this.sjbg_panel.Controls.Add(this.textBox444);
            this.sjbg_panel.Controls.Add(this.textBox445);
            this.sjbg_panel.Controls.Add(this.textBox446);
            this.sjbg_panel.Controls.Add(this.textBox447);
            this.sjbg_panel.Controls.Add(this.textBox448);
            this.sjbg_panel.Controls.Add(this.textBox449);
            this.sjbg_panel.Controls.Add(this.textBox450);
            this.sjbg_panel.Controls.Add(this.textBox451);
            this.sjbg_panel.Controls.Add(this.textBox452);
            this.sjbg_panel.Controls.Add(this.textBox453);
            this.sjbg_panel.Controls.Add(this.textBox454);
            this.sjbg_panel.Controls.Add(this.textBox455);
            this.sjbg_panel.Controls.Add(this.textBox456);
            this.sjbg_panel.Controls.Add(this.textBox457);
            this.sjbg_panel.Controls.Add(this.textBox458);
            this.sjbg_panel.Controls.Add(this.textBox459);
            this.sjbg_panel.Controls.Add(this.textBox460);
            this.sjbg_panel.Controls.Add(this.textBox461);
            this.sjbg_panel.Controls.Add(this.textBox462);
            this.sjbg_panel.Controls.Add(this.textBox463);
            this.sjbg_panel.Controls.Add(this.textBox464);
            this.sjbg_panel.Controls.Add(this.textBox465);
            this.sjbg_panel.Controls.Add(this.textBox466);
            this.sjbg_panel.Controls.Add(this.textBox467);
            this.sjbg_panel.Controls.Add(this.textBox468);
            this.sjbg_panel.Controls.Add(this.textBox469);
            this.sjbg_panel.Controls.Add(this.textBox470);
            this.sjbg_panel.Controls.Add(this.label38);
            this.sjbg_panel.Controls.Add(this.textBox471);
            this.sjbg_panel.Controls.Add(this.label39);
            this.sjbg_panel.Controls.Add(this.textBox472);
            this.sjbg_panel.Controls.Add(this.textBox473);
            this.sjbg_panel.Controls.Add(this.textBox474);
            this.sjbg_panel.Controls.Add(this.textBox475);
            this.sjbg_panel.Controls.Add(this.label40);
            this.sjbg_panel.Controls.Add(this.textBox476);
            this.sjbg_panel.Controls.Add(this.textBox477);
            this.sjbg_panel.Controls.Add(this.textBox478);
            this.sjbg_panel.Controls.Add(this.label50);
            this.sjbg_panel.Controls.Add(this.textBox349);
            this.sjbg_panel.Controls.Add(this.textBox350);
            this.sjbg_panel.Controls.Add(this.textBox351);
            this.sjbg_panel.Controls.Add(this.textBox368);
            this.sjbg_panel.Controls.Add(this.textBox369);
            this.sjbg_panel.Controls.Add(this.textBox391);
            this.sjbg_panel.Controls.Add(this.textBox392);
            this.sjbg_panel.Controls.Add(this.textBox393);
            this.sjbg_panel.Controls.Add(this.textBox409);
            this.sjbg_panel.Controls.Add(this.textBox410);
            this.sjbg_panel.Controls.Add(this.textBox411);
            this.sjbg_panel.Controls.Add(this.textBox418);
            this.sjbg_panel.Controls.Add(this.textBox334);
            this.sjbg_panel.Controls.Add(this.textBox335);
            this.sjbg_panel.Controls.Add(this.textBox336);
            this.sjbg_panel.Controls.Add(this.textBox353);
            this.sjbg_panel.Controls.Add(this.textBox354);
            this.sjbg_panel.Controls.Add(this.textBox376);
            this.sjbg_panel.Controls.Add(this.textBox377);
            this.sjbg_panel.Controls.Add(this.textBox378);
            this.sjbg_panel.Controls.Add(this.textBox394);
            this.sjbg_panel.Controls.Add(this.textBox395);
            this.sjbg_panel.Controls.Add(this.textBox396);
            this.sjbg_panel.Controls.Add(this.textBox413);
            this.sjbg_panel.Controls.Add(this.textBox328);
            this.sjbg_panel.Controls.Add(this.textBox329);
            this.sjbg_panel.Controls.Add(this.textBox330);
            this.sjbg_panel.Controls.Add(this.textBox332);
            this.sjbg_panel.Controls.Add(this.textBox333);
            this.sjbg_panel.Controls.Add(this.textBox370);
            this.sjbg_panel.Controls.Add(this.textBox371);
            this.sjbg_panel.Controls.Add(this.textBox372);
            this.sjbg_panel.Controls.Add(this.textBox373);
            this.sjbg_panel.Controls.Add(this.textBox374);
            this.sjbg_panel.Controls.Add(this.textBox375);
            this.sjbg_panel.Controls.Add(this.textBox412);
            this.sjbg_panel.Controls.Add(this.textBox337);
            this.sjbg_panel.Controls.Add(this.textBox338);
            this.sjbg_panel.Controls.Add(this.textBox339);
            this.sjbg_panel.Controls.Add(this.textBox356);
            this.sjbg_panel.Controls.Add(this.textBox357);
            this.sjbg_panel.Controls.Add(this.textBox379);
            this.sjbg_panel.Controls.Add(this.textBox380);
            this.sjbg_panel.Controls.Add(this.textBox381);
            this.sjbg_panel.Controls.Add(this.textBox397);
            this.sjbg_panel.Controls.Add(this.textBox398);
            this.sjbg_panel.Controls.Add(this.textBox399);
            this.sjbg_panel.Controls.Add(this.textBox414);
            this.sjbg_panel.Controls.Add(this.textBox382);
            this.sjbg_panel.Controls.Add(this.textBox383);
            this.sjbg_panel.Controls.Add(this.textBox384);
            this.sjbg_panel.Controls.Add(this.textBox385);
            this.sjbg_panel.Controls.Add(this.textBox386);
            this.sjbg_panel.Controls.Add(this.textBox387);
            this.sjbg_panel.Controls.Add(this.textBox388);
            this.sjbg_panel.Controls.Add(this.textBox389);
            this.sjbg_panel.Controls.Add(this.textBox390);
            this.sjbg_panel.Controls.Add(this.textBox400);
            this.sjbg_panel.Controls.Add(this.textBox401);
            this.sjbg_panel.Controls.Add(this.textBox402);
            this.sjbg_panel.Controls.Add(this.textBox403);
            this.sjbg_panel.Controls.Add(this.textBox404);
            this.sjbg_panel.Controls.Add(this.textBox405);
            this.sjbg_panel.Controls.Add(this.textBox406);
            this.sjbg_panel.Controls.Add(this.textBox407);
            this.sjbg_panel.Controls.Add(this.textBox408);
            this.sjbg_panel.Controls.Add(this.textBox415);
            this.sjbg_panel.Controls.Add(this.textBox416);
            this.sjbg_panel.Controls.Add(this.textBox417);
            this.sjbg_panel.Controls.Add(this.textBox331);
            this.sjbg_panel.Controls.Add(this.textBox340);
            this.sjbg_panel.Controls.Add(this.textBox341);
            this.sjbg_panel.Controls.Add(this.textBox342);
            this.sjbg_panel.Controls.Add(this.textBox343);
            this.sjbg_panel.Controls.Add(this.textBox344);
            this.sjbg_panel.Controls.Add(this.textBox345);
            this.sjbg_panel.Controls.Add(this.textBox346);
            this.sjbg_panel.Controls.Add(this.textBox347);
            this.sjbg_panel.Controls.Add(this.textBox348);
            this.sjbg_panel.Controls.Add(this.textBox352);
            this.sjbg_panel.Controls.Add(this.textBox355);
            this.sjbg_panel.Controls.Add(this.textBox358);
            this.sjbg_panel.Controls.Add(this.textBox359);
            this.sjbg_panel.Controls.Add(this.textBox360);
            this.sjbg_panel.Controls.Add(this.textBox361);
            this.sjbg_panel.Controls.Add(this.textBox362);
            this.sjbg_panel.Controls.Add(this.textBox363);
            this.sjbg_panel.Controls.Add(this.textBox364);
            this.sjbg_panel.Controls.Add(this.textBox365);
            this.sjbg_panel.Controls.Add(this.textBox366);
            this.sjbg_panel.Controls.Add(this.textBox367);
            this.sjbg_panel.Controls.Add(this.textBox21);
            this.sjbg_panel.Controls.Add(this.textBox23);
            this.sjbg_panel.Controls.Add(this.textBox24);
            this.sjbg_panel.Controls.Add(this.textBox307);
            this.sjbg_panel.Controls.Add(this.textBox308);
            this.sjbg_panel.Controls.Add(this.textBox309);
            this.sjbg_panel.Controls.Add(this.textBox310);
            this.sjbg_panel.Controls.Add(this.textBox311);
            this.sjbg_panel.Controls.Add(this.textBox312);
            this.sjbg_panel.Controls.Add(this.textBox313);
            this.sjbg_panel.Controls.Add(this.textBox314);
            this.sjbg_panel.Controls.Add(this.textBox315);
            this.sjbg_panel.Controls.Add(this.textBox316);
            this.sjbg_panel.Controls.Add(this.textBox317);
            this.sjbg_panel.Controls.Add(this.textBox318);
            this.sjbg_panel.Controls.Add(this.textBox319);
            this.sjbg_panel.Controls.Add(this.textBox320);
            this.sjbg_panel.Controls.Add(this.textBox321);
            this.sjbg_panel.Controls.Add(this.textBox322);
            this.sjbg_panel.Controls.Add(this.textBox323);
            this.sjbg_panel.Controls.Add(this.textBox324);
            this.sjbg_panel.Controls.Add(this.textBox325);
            this.sjbg_panel.Controls.Add(this.textBox326);
            this.sjbg_panel.Controls.Add(this.textBox327);
            this.sjbg_panel.Controls.Add(this.textBox14);
            this.sjbg_panel.Controls.Add(this.textBox15);
            this.sjbg_panel.Controls.Add(this.textBox18);
            this.sjbg_panel.Controls.Add(this.textBox291);
            this.sjbg_panel.Controls.Add(this.textBox292);
            this.sjbg_panel.Controls.Add(this.textBox293);
            this.sjbg_panel.Controls.Add(this.textBox294);
            this.sjbg_panel.Controls.Add(this.textBox295);
            this.sjbg_panel.Controls.Add(this.textBox296);
            this.sjbg_panel.Controls.Add(this.textBox297);
            this.sjbg_panel.Controls.Add(this.textBox298);
            this.sjbg_panel.Controls.Add(this.textBox299);
            this.sjbg_panel.Controls.Add(this.textBox300);
            this.sjbg_panel.Controls.Add(this.textBox301);
            this.sjbg_panel.Controls.Add(this.textBox302);
            this.sjbg_panel.Controls.Add(this.textBox303);
            this.sjbg_panel.Controls.Add(this.textBox304);
            this.sjbg_panel.Controls.Add(this.textBox305);
            this.sjbg_panel.Controls.Add(this.textBox306);
            this.sjbg_panel.Controls.Add(this.textBox233);
            this.sjbg_panel.Controls.Add(this.textBox236);
            this.sjbg_panel.Controls.Add(this.textBox239);
            this.sjbg_panel.Controls.Add(this.textBox242);
            this.sjbg_panel.Controls.Add(this.textBox279);
            this.sjbg_panel.Controls.Add(this.textBox280);
            this.sjbg_panel.Controls.Add(this.textBox281);
            this.sjbg_panel.Controls.Add(this.textBox282);
            this.sjbg_panel.Controls.Add(this.textBox283);
            this.sjbg_panel.Controls.Add(this.textBox284);
            this.sjbg_panel.Controls.Add(this.textBox285);
            this.sjbg_panel.Controls.Add(this.textBox286);
            this.sjbg_panel.Controls.Add(this.textBox287);
            this.sjbg_panel.Controls.Add(this.textBox288);
            this.sjbg_panel.Controls.Add(this.textBox289);
            this.sjbg_panel.Controls.Add(this.textBox290);
            this.sjbg_panel.Controls.Add(this.qlca_zd);
            this.sjbg_panel.Controls.Add(this.qlca_zx);
            this.sjbg_panel.Controls.Add(this.qlca_ed);
            this.sjbg_panel.Controls.Add(this.textBox20);
            this.sjbg_panel.Controls.Add(this.label35);
            this.sjbg_panel.Controls.Add(this.textBox151);
            this.sjbg_panel.Controls.Add(this.label83);
            this.sjbg_panel.Controls.Add(this.textBox152);
            this.sjbg_panel.Controls.Add(this.textBox153);
            this.sjbg_panel.Controls.Add(this.textBox154);
            this.sjbg_panel.Controls.Add(this.textBox155);
            this.sjbg_panel.Controls.Add(this.label84);
            this.sjbg_panel.Controls.Add(this.textBox156);
            this.sjbg_panel.Controls.Add(this.textBox157);
            this.sjbg_panel.Controls.Add(this.textBox158);
            this.sjbg_panel.Controls.Add(this.label85);
            this.sjbg_panel.Controls.Add(this.textBox147);
            this.sjbg_panel.Controls.Add(this.label82);
            this.sjbg_panel.Controls.Add(this.textBox148);
            this.sjbg_panel.Controls.Add(this.textBox149);
            this.sjbg_panel.Controls.Add(this.textBox150);
            this.sjbg_panel.Controls.Add(this.textBox143);
            this.sjbg_panel.Controls.Add(this.label81);
            this.sjbg_panel.Controls.Add(this.textBox144);
            this.sjbg_panel.Controls.Add(this.textBox145);
            this.sjbg_panel.Controls.Add(this.textBox146);
            this.sjbg_panel.Controls.Add(this.textBox139);
            this.sjbg_panel.Controls.Add(this.label41);
            this.sjbg_panel.Controls.Add(this.textBox140);
            this.sjbg_panel.Controls.Add(this.textBox141);
            this.sjbg_panel.Controls.Add(this.textBox142);
            this.sjbg_panel.Controls.Add(this.label42);
            this.sjbg_panel.Controls.Add(this.label43);
            this.sjbg_panel.Controls.Add(this.textBox17);
            this.sjbg_panel.Controls.Add(this.textBox5);
            this.sjbg_panel.Controls.Add(this.hlca_zd);
            this.sjbg_panel.Controls.Add(this.label32);
            this.sjbg_panel.Controls.Add(this.label29);
            this.sjbg_panel.Controls.Add(this.textBox2);
            this.sjbg_panel.Controls.Add(this.textBox3);
            this.sjbg_panel.Controls.Add(this.textBox6);
            this.sjbg_panel.Controls.Add(this.shp3_lbl);
            this.sjbg_panel.Location = new System.Drawing.Point(6, 40);
            this.sjbg_panel.Name = "sjbg_panel";
            this.sjbg_panel.Size = new System.Drawing.Size(880, 552);
            this.sjbg_panel.TabIndex = 218;
            // 
            // shp3_lbl
            // 
            this.shp3_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.shp3_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shp3_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shp3_lbl.Location = new System.Drawing.Point(15, 36);
            this.shp3_lbl.Name = "shp3_lbl";
            this.shp3_lbl.Size = new System.Drawing.Size(253, 25);
            this.shp3_lbl.TabIndex = 86;
            this.shp3_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox6.Location = new System.Drawing.Point(336, 108);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(70, 25);
            this.textBox6.TabIndex = 88;
            this.textBox6.Text = "2.7";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Window;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox3.Location = new System.Drawing.Point(336, 132);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(70, 25);
            this.textBox3.TabIndex = 89;
            this.textBox3.Text = "2";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox2.Location = new System.Drawing.Point(336, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(70, 25);
            this.textBox2.TabIndex = 87;
            this.textBox2.Text = "67.5";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.SystemColors.Window;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.Location = new System.Drawing.Point(336, 36);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 25);
            this.label29.TabIndex = 93;
            this.label29.Text = "最大计分";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.SystemColors.Window;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label32.Location = new System.Drawing.Point(267, 36);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 25);
            this.label32.TabIndex = 107;
            this.label32.Text = "权重%";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hlca_zd
            // 
            this.hlca_zd.BackColor = System.Drawing.SystemColors.Window;
            this.hlca_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlca_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlca_zd.Location = new System.Drawing.Point(15, 60);
            this.hlca_zd.Name = "hlca_zd";
            this.hlca_zd.Size = new System.Drawing.Size(253, 25);
            this.hlca_zd.TabIndex = 115;
            this.hlca_zd.Text = "总计";
            this.hlca_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox5.Location = new System.Drawing.Point(336, 60);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(70, 25);
            this.textBox5.TabIndex = 116;
            this.textBox5.Text = "89.7";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.Window;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox17.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox17.Location = new System.Drawing.Point(267, 60);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(70, 25);
            this.textBox17.TabIndex = 117;
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.SystemColors.Window;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label43.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label43.Location = new System.Drawing.Point(15, 12);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(391, 25);
            this.label43.TabIndex = 119;
            this.label43.Text = "概览";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.SystemColors.Window;
            this.label42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label42.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label42.Location = new System.Drawing.Point(405, 12);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(148, 25);
            this.label42.TabIndex = 120;
            this.label42.Text = "SP002013";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox142
            // 
            this.textBox142.BackColor = System.Drawing.SystemColors.Window;
            this.textBox142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox142.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox142.Location = new System.Drawing.Point(405, 108);
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new System.Drawing.Size(50, 25);
            this.textBox142.TabIndex = 122;
            this.textBox142.Text = "480000";
            this.textBox142.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox141
            // 
            this.textBox141.BackColor = System.Drawing.SystemColors.Window;
            this.textBox141.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox141.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox141.Location = new System.Drawing.Point(405, 132);
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new System.Drawing.Size(50, 25);
            this.textBox141.TabIndex = 123;
            this.textBox141.Text = "5";
            this.textBox141.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox140
            // 
            this.textBox140.BackColor = System.Drawing.SystemColors.Window;
            this.textBox140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox140.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox140.Location = new System.Drawing.Point(405, 84);
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new System.Drawing.Size(50, 25);
            this.textBox140.TabIndex = 121;
            this.textBox140.Text = "500000";
            this.textBox140.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.SystemColors.Window;
            this.label41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label41.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label41.Location = new System.Drawing.Point(405, 36);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(50, 25);
            this.label41.TabIndex = 124;
            this.label41.Text = "值";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox139
            // 
            this.textBox139.BackColor = System.Drawing.SystemColors.Window;
            this.textBox139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox139.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox139.Location = new System.Drawing.Point(405, 60);
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new System.Drawing.Size(50, 25);
            this.textBox139.TabIndex = 125;
            this.textBox139.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox146
            // 
            this.textBox146.BackColor = System.Drawing.SystemColors.Window;
            this.textBox146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox146.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox146.Location = new System.Drawing.Point(454, 108);
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new System.Drawing.Size(50, 25);
            this.textBox146.TabIndex = 127;
            this.textBox146.Text = "80";
            this.textBox146.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox145
            // 
            this.textBox145.BackColor = System.Drawing.SystemColors.Window;
            this.textBox145.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox145.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox145.Location = new System.Drawing.Point(454, 132);
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new System.Drawing.Size(50, 25);
            this.textBox145.TabIndex = 128;
            this.textBox145.Text = "100";
            this.textBox145.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox144
            // 
            this.textBox144.BackColor = System.Drawing.SystemColors.Window;
            this.textBox144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox144.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox144.Location = new System.Drawing.Point(454, 84);
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new System.Drawing.Size(50, 25);
            this.textBox144.TabIndex = 126;
            this.textBox144.Text = "90";
            this.textBox144.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.SystemColors.Window;
            this.label81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label81.Location = new System.Drawing.Point(454, 36);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(50, 25);
            this.label81.TabIndex = 129;
            this.label81.Text = "评估";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox143
            // 
            this.textBox143.BackColor = System.Drawing.SystemColors.Window;
            this.textBox143.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox143.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox143.Location = new System.Drawing.Point(454, 60);
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new System.Drawing.Size(50, 25);
            this.textBox143.TabIndex = 130;
            this.textBox143.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox150
            // 
            this.textBox150.BackColor = System.Drawing.SystemColors.Window;
            this.textBox150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox150.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox150.Location = new System.Drawing.Point(503, 108);
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new System.Drawing.Size(50, 25);
            this.textBox150.TabIndex = 132;
            this.textBox150.Text = "2.4";
            this.textBox150.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox149
            // 
            this.textBox149.BackColor = System.Drawing.SystemColors.Window;
            this.textBox149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox149.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox149.Location = new System.Drawing.Point(503, 132);
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new System.Drawing.Size(50, 25);
            this.textBox149.TabIndex = 133;
            this.textBox149.Text = "2";
            this.textBox149.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox148
            // 
            this.textBox148.BackColor = System.Drawing.SystemColors.Window;
            this.textBox148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox148.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox148.Location = new System.Drawing.Point(503, 84);
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new System.Drawing.Size(50, 25);
            this.textBox148.TabIndex = 131;
            this.textBox148.Text = "67.5";
            this.textBox148.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label82
            // 
            this.label82.BackColor = System.Drawing.SystemColors.Window;
            this.label82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label82.Location = new System.Drawing.Point(503, 36);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(50, 25);
            this.label82.TabIndex = 134;
            this.label82.Text = "分";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox147
            // 
            this.textBox147.BackColor = System.Drawing.SystemColors.Window;
            this.textBox147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox147.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox147.Location = new System.Drawing.Point(503, 60);
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new System.Drawing.Size(50, 25);
            this.textBox147.TabIndex = 135;
            this.textBox147.Text = "89.7";
            this.textBox147.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label85
            // 
            this.label85.BackColor = System.Drawing.SystemColors.Window;
            this.label85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label85.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label85.Location = new System.Drawing.Point(552, 12);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(148, 25);
            this.label85.TabIndex = 136;
            this.label85.Text = "SP002025";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox158
            // 
            this.textBox158.BackColor = System.Drawing.SystemColors.Window;
            this.textBox158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox158.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox158.Location = new System.Drawing.Point(552, 108);
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new System.Drawing.Size(50, 25);
            this.textBox158.TabIndex = 138;
            this.textBox158.Text = "490000";
            this.textBox158.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox157
            // 
            this.textBox157.BackColor = System.Drawing.SystemColors.Window;
            this.textBox157.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox157.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox157.Location = new System.Drawing.Point(552, 132);
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new System.Drawing.Size(50, 25);
            this.textBox157.TabIndex = 139;
            this.textBox157.Text = "5";
            this.textBox157.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox156
            // 
            this.textBox156.BackColor = System.Drawing.SystemColors.Window;
            this.textBox156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox156.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox156.Location = new System.Drawing.Point(552, 84);
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new System.Drawing.Size(50, 25);
            this.textBox156.TabIndex = 137;
            this.textBox156.Text = "520000";
            this.textBox156.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label84
            // 
            this.label84.BackColor = System.Drawing.SystemColors.Window;
            this.label84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label84.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label84.Location = new System.Drawing.Point(552, 36);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(50, 25);
            this.label84.TabIndex = 140;
            this.label84.Text = "值";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox155
            // 
            this.textBox155.BackColor = System.Drawing.SystemColors.Window;
            this.textBox155.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox155.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox155.Location = new System.Drawing.Point(552, 60);
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new System.Drawing.Size(50, 25);
            this.textBox155.TabIndex = 141;
            this.textBox155.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox154
            // 
            this.textBox154.BackColor = System.Drawing.SystemColors.Window;
            this.textBox154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox154.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox154.Location = new System.Drawing.Point(601, 108);
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new System.Drawing.Size(50, 25);
            this.textBox154.TabIndex = 143;
            this.textBox154.Text = "90";
            this.textBox154.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox153
            // 
            this.textBox153.BackColor = System.Drawing.SystemColors.Window;
            this.textBox153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox153.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox153.Location = new System.Drawing.Point(601, 132);
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new System.Drawing.Size(50, 25);
            this.textBox153.TabIndex = 144;
            this.textBox153.Text = "100";
            this.textBox153.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox152
            // 
            this.textBox152.BackColor = System.Drawing.SystemColors.Window;
            this.textBox152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox152.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox152.Location = new System.Drawing.Point(601, 84);
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new System.Drawing.Size(50, 25);
            this.textBox152.TabIndex = 142;
            this.textBox152.Text = "80";
            this.textBox152.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label83
            // 
            this.label83.BackColor = System.Drawing.SystemColors.Window;
            this.label83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label83.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label83.Location = new System.Drawing.Point(601, 36);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(50, 25);
            this.label83.TabIndex = 145;
            this.label83.Text = "评估";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox151
            // 
            this.textBox151.BackColor = System.Drawing.SystemColors.Window;
            this.textBox151.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox151.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox151.Location = new System.Drawing.Point(601, 60);
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new System.Drawing.Size(50, 25);
            this.textBox151.TabIndex = 146;
            this.textBox151.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.SystemColors.Window;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label35.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(650, 36);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 25);
            this.label35.TabIndex = 150;
            this.label35.Text = "分";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Window;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox20.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox20.Location = new System.Drawing.Point(650, 60);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(50, 25);
            this.textBox20.TabIndex = 151;
            this.textBox20.Text = "82.8";
            this.textBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_ed
            // 
            this.qlca_ed.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_ed.Location = new System.Drawing.Point(15, 108);
            this.qlca_ed.Name = "qlca_ed";
            this.qlca_ed.Size = new System.Drawing.Size(253, 25);
            this.qlca_ed.TabIndex = 218;
            this.qlca_ed.Text = "历史价格";
            this.qlca_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_zx
            // 
            this.qlca_zx.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_zx.Location = new System.Drawing.Point(15, 132);
            this.qlca_zx.Name = "qlca_zx";
            this.qlca_zx.Size = new System.Drawing.Size(253, 25);
            this.qlca_zx.TabIndex = 219;
            this.qlca_zx.Text = "无故障时间";
            this.qlca_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // qlca_zd
            // 
            this.qlca_zd.BackColor = System.Drawing.SystemColors.Window;
            this.qlca_zd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qlca_zd.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qlca_zd.Location = new System.Drawing.Point(15, 84);
            this.qlca_zd.Name = "qlca_zd";
            this.qlca_zd.Size = new System.Drawing.Size(253, 25);
            this.qlca_zd.TabIndex = 217;
            this.qlca_zd.Text = "当前价格";
            this.qlca_zd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox290
            // 
            this.textBox290.BackColor = System.Drawing.SystemColors.Window;
            this.textBox290.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox290.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox290.Location = new System.Drawing.Point(15, 156);
            this.textBox290.Name = "textBox290";
            this.textBox290.Size = new System.Drawing.Size(253, 25);
            this.textBox290.TabIndex = 220;
            this.textBox290.Text = "低停工检修率";
            this.textBox290.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox289
            // 
            this.textBox289.BackColor = System.Drawing.SystemColors.Window;
            this.textBox289.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox289.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox289.Location = new System.Drawing.Point(15, 204);
            this.textBox289.Name = "textBox289";
            this.textBox289.Size = new System.Drawing.Size(253, 25);
            this.textBox289.TabIndex = 222;
            this.textBox289.Text = "批次合格率";
            this.textBox289.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox288
            // 
            this.textBox288.BackColor = System.Drawing.SystemColors.Window;
            this.textBox288.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox288.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox288.Location = new System.Drawing.Point(15, 228);
            this.textBox288.Name = "textBox288";
            this.textBox288.Size = new System.Drawing.Size(253, 25);
            this.textBox288.TabIndex = 223;
            this.textBox288.Text = "交货日期";
            this.textBox288.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox287
            // 
            this.textBox287.BackColor = System.Drawing.SystemColors.Window;
            this.textBox287.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox287.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox287.Location = new System.Drawing.Point(15, 180);
            this.textBox287.Name = "textBox287";
            this.textBox287.Size = new System.Drawing.Size(253, 25);
            this.textBox287.TabIndex = 221;
            this.textBox287.Text = "设备耐久性";
            this.textBox287.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox286
            // 
            this.textBox286.BackColor = System.Drawing.SystemColors.Window;
            this.textBox286.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox286.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox286.Location = new System.Drawing.Point(15, 252);
            this.textBox286.Name = "textBox286";
            this.textBox286.Size = new System.Drawing.Size(253, 25);
            this.textBox286.TabIndex = 224;
            this.textBox286.Text = "数量可靠性";
            this.textBox286.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox285
            // 
            this.textBox285.BackColor = System.Drawing.SystemColors.Window;
            this.textBox285.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox285.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox285.Location = new System.Drawing.Point(15, 300);
            this.textBox285.Name = "textBox285";
            this.textBox285.Size = new System.Drawing.Size(253, 25);
            this.textBox285.TabIndex = 226;
            this.textBox285.Text = "是否安装、调试等服务";
            this.textBox285.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox284
            // 
            this.textBox284.BackColor = System.Drawing.SystemColors.Window;
            this.textBox284.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox284.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox284.Location = new System.Drawing.Point(15, 324);
            this.textBox284.Name = "textBox284";
            this.textBox284.Size = new System.Drawing.Size(253, 25);
            this.textBox284.TabIndex = 227;
            this.textBox284.Text = "是否进行人员培训";
            this.textBox284.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox283
            // 
            this.textBox283.BackColor = System.Drawing.SystemColors.Window;
            this.textBox283.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox283.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox283.Location = new System.Drawing.Point(15, 276);
            this.textBox283.Name = "textBox283";
            this.textBox283.Size = new System.Drawing.Size(253, 25);
            this.textBox283.TabIndex = 225;
            this.textBox283.Text = "装运情况";
            this.textBox283.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox282
            // 
            this.textBox282.BackColor = System.Drawing.SystemColors.Window;
            this.textBox282.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox282.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox282.Location = new System.Drawing.Point(15, 348);
            this.textBox282.Name = "textBox282";
            this.textBox282.Size = new System.Drawing.Size(253, 25);
            this.textBox282.TabIndex = 228;
            this.textBox282.Text = "保养与修理的反应时间";
            this.textBox282.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox281
            // 
            this.textBox281.BackColor = System.Drawing.SystemColors.Window;
            this.textBox281.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox281.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox281.Location = new System.Drawing.Point(15, 396);
            this.textBox281.Name = "textBox281";
            this.textBox281.Size = new System.Drawing.Size(253, 25);
            this.textBox281.TabIndex = 230;
            this.textBox281.Text = "机械设备";
            this.textBox281.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox280
            // 
            this.textBox280.BackColor = System.Drawing.SystemColors.Window;
            this.textBox280.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox280.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox280.Location = new System.Drawing.Point(15, 420);
            this.textBox280.Name = "textBox280";
            this.textBox280.Size = new System.Drawing.Size(253, 25);
            this.textBox280.TabIndex = 231;
            this.textBox280.Text = "检验设备";
            this.textBox280.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox279
            // 
            this.textBox279.BackColor = System.Drawing.SystemColors.Window;
            this.textBox279.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox279.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox279.Location = new System.Drawing.Point(15, 372);
            this.textBox279.Name = "textBox279";
            this.textBox279.Size = new System.Drawing.Size(253, 25);
            this.textBox279.TabIndex = 229;
            this.textBox279.Text = "外包率";
            this.textBox279.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox242
            // 
            this.textBox242.BackColor = System.Drawing.SystemColors.Window;
            this.textBox242.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox242.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox242.Location = new System.Drawing.Point(15, 444);
            this.textBox242.Name = "textBox242";
            this.textBox242.Size = new System.Drawing.Size(253, 25);
            this.textBox242.TabIndex = 232;
            this.textBox242.Text = "工作技术";
            this.textBox242.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox239
            // 
            this.textBox239.BackColor = System.Drawing.SystemColors.Window;
            this.textBox239.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox239.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox239.Location = new System.Drawing.Point(15, 468);
            this.textBox239.Name = "textBox239";
            this.textBox239.Size = new System.Drawing.Size(253, 25);
            this.textBox239.TabIndex = 233;
            this.textBox239.Text = "营业状况";
            this.textBox239.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox236
            // 
            this.textBox236.BackColor = System.Drawing.SystemColors.Window;
            this.textBox236.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox236.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox236.Location = new System.Drawing.Point(15, 492);
            this.textBox236.Name = "textBox236";
            this.textBox236.Size = new System.Drawing.Size(253, 25);
            this.textBox236.TabIndex = 234;
            this.textBox236.Text = "财务结构";
            this.textBox236.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox233
            // 
            this.textBox233.BackColor = System.Drawing.SystemColors.Window;
            this.textBox233.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox233.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox233.Location = new System.Drawing.Point(15, 516);
            this.textBox233.Name = "textBox233";
            this.textBox233.Size = new System.Drawing.Size(253, 25);
            this.textBox233.TabIndex = 235;
            this.textBox233.Text = "反应措施";
            this.textBox233.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox306
            // 
            this.textBox306.BackColor = System.Drawing.SystemColors.Window;
            this.textBox306.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox306.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox306.Location = new System.Drawing.Point(267, 108);
            this.textBox306.Name = "textBox306";
            this.textBox306.Size = new System.Drawing.Size(70, 25);
            this.textBox306.TabIndex = 237;
            this.textBox306.Text = "3.00";
            this.textBox306.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox305
            // 
            this.textBox305.BackColor = System.Drawing.SystemColors.Window;
            this.textBox305.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox305.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox305.Location = new System.Drawing.Point(267, 156);
            this.textBox305.Name = "textBox305";
            this.textBox305.Size = new System.Drawing.Size(70, 25);
            this.textBox305.TabIndex = 239;
            this.textBox305.Text = "2.00";
            this.textBox305.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox304
            // 
            this.textBox304.BackColor = System.Drawing.SystemColors.Window;
            this.textBox304.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox304.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox304.Location = new System.Drawing.Point(267, 132);
            this.textBox304.Name = "textBox304";
            this.textBox304.Size = new System.Drawing.Size(70, 25);
            this.textBox304.TabIndex = 238;
            this.textBox304.Text = "2.00";
            this.textBox304.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox303
            // 
            this.textBox303.BackColor = System.Drawing.SystemColors.Window;
            this.textBox303.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox303.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox303.Location = new System.Drawing.Point(267, 84);
            this.textBox303.Name = "textBox303";
            this.textBox303.Size = new System.Drawing.Size(70, 25);
            this.textBox303.TabIndex = 236;
            this.textBox303.Text = "75.00";
            this.textBox303.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox302
            // 
            this.textBox302.BackColor = System.Drawing.SystemColors.Window;
            this.textBox302.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox302.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox302.Location = new System.Drawing.Point(267, 204);
            this.textBox302.Name = "textBox302";
            this.textBox302.Size = new System.Drawing.Size(70, 25);
            this.textBox302.TabIndex = 241;
            this.textBox302.Text = "2.00";
            this.textBox302.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox301
            // 
            this.textBox301.BackColor = System.Drawing.SystemColors.Window;
            this.textBox301.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox301.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox301.Location = new System.Drawing.Point(267, 252);
            this.textBox301.Name = "textBox301";
            this.textBox301.Size = new System.Drawing.Size(70, 25);
            this.textBox301.TabIndex = 243;
            this.textBox301.Text = "1.00";
            this.textBox301.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox300
            // 
            this.textBox300.BackColor = System.Drawing.SystemColors.Window;
            this.textBox300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox300.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox300.Location = new System.Drawing.Point(267, 228);
            this.textBox300.Name = "textBox300";
            this.textBox300.Size = new System.Drawing.Size(70, 25);
            this.textBox300.TabIndex = 242;
            this.textBox300.Text = "1.00";
            this.textBox300.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox299
            // 
            this.textBox299.BackColor = System.Drawing.SystemColors.Window;
            this.textBox299.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox299.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox299.Location = new System.Drawing.Point(267, 180);
            this.textBox299.Name = "textBox299";
            this.textBox299.Size = new System.Drawing.Size(70, 25);
            this.textBox299.TabIndex = 240;
            this.textBox299.Text = "2.00";
            this.textBox299.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox298
            // 
            this.textBox298.BackColor = System.Drawing.SystemColors.Window;
            this.textBox298.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox298.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox298.Location = new System.Drawing.Point(267, 300);
            this.textBox298.Name = "textBox298";
            this.textBox298.Size = new System.Drawing.Size(70, 25);
            this.textBox298.TabIndex = 245;
            this.textBox298.Text = "1.00";
            this.textBox298.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox297
            // 
            this.textBox297.BackColor = System.Drawing.SystemColors.Window;
            this.textBox297.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox297.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox297.Location = new System.Drawing.Point(267, 348);
            this.textBox297.Name = "textBox297";
            this.textBox297.Size = new System.Drawing.Size(70, 25);
            this.textBox297.TabIndex = 247;
            this.textBox297.Text = "2.00";
            this.textBox297.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox296
            // 
            this.textBox296.BackColor = System.Drawing.SystemColors.Window;
            this.textBox296.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox296.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox296.Location = new System.Drawing.Point(267, 324);
            this.textBox296.Name = "textBox296";
            this.textBox296.Size = new System.Drawing.Size(70, 25);
            this.textBox296.TabIndex = 246;
            this.textBox296.Text = "1.00";
            this.textBox296.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox295
            // 
            this.textBox295.BackColor = System.Drawing.SystemColors.Window;
            this.textBox295.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox295.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox295.Location = new System.Drawing.Point(267, 276);
            this.textBox295.Name = "textBox295";
            this.textBox295.Size = new System.Drawing.Size(70, 25);
            this.textBox295.TabIndex = 244;
            this.textBox295.Text = "1.00";
            this.textBox295.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox294
            // 
            this.textBox294.BackColor = System.Drawing.SystemColors.Window;
            this.textBox294.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox294.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox294.Location = new System.Drawing.Point(267, 396);
            this.textBox294.Name = "textBox294";
            this.textBox294.Size = new System.Drawing.Size(70, 25);
            this.textBox294.TabIndex = 249;
            this.textBox294.Text = "1.00";
            this.textBox294.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox293
            // 
            this.textBox293.BackColor = System.Drawing.SystemColors.Window;
            this.textBox293.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox293.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox293.Location = new System.Drawing.Point(267, 444);
            this.textBox293.Name = "textBox293";
            this.textBox293.Size = new System.Drawing.Size(70, 25);
            this.textBox293.TabIndex = 251;
            this.textBox293.Text = "1.00";
            this.textBox293.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox292
            // 
            this.textBox292.BackColor = System.Drawing.SystemColors.Window;
            this.textBox292.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox292.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox292.Location = new System.Drawing.Point(267, 420);
            this.textBox292.Name = "textBox292";
            this.textBox292.Size = new System.Drawing.Size(70, 25);
            this.textBox292.TabIndex = 250;
            this.textBox292.Text = "1.00";
            this.textBox292.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox291
            // 
            this.textBox291.BackColor = System.Drawing.SystemColors.Window;
            this.textBox291.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox291.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox291.Location = new System.Drawing.Point(267, 372);
            this.textBox291.Name = "textBox291";
            this.textBox291.Size = new System.Drawing.Size(70, 25);
            this.textBox291.TabIndex = 248;
            this.textBox291.Text = "1.00";
            this.textBox291.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.Window;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox18.Location = new System.Drawing.Point(267, 468);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(70, 25);
            this.textBox18.TabIndex = 252;
            this.textBox18.Text = "1.00";
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.Window;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox15.Location = new System.Drawing.Point(267, 516);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(70, 25);
            this.textBox15.TabIndex = 254;
            this.textBox15.Text = "1.00";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.SystemColors.Window;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox14.Location = new System.Drawing.Point(267, 492);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(70, 25);
            this.textBox14.TabIndex = 253;
            this.textBox14.Text = "1.00";
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox327
            // 
            this.textBox327.BackColor = System.Drawing.SystemColors.Window;
            this.textBox327.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox327.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox327.Location = new System.Drawing.Point(336, 180);
            this.textBox327.Name = "textBox327";
            this.textBox327.Size = new System.Drawing.Size(70, 25);
            this.textBox327.TabIndex = 256;
            this.textBox327.Text = "1.8";
            this.textBox327.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox326
            // 
            this.textBox326.BackColor = System.Drawing.SystemColors.Window;
            this.textBox326.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox326.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox326.Location = new System.Drawing.Point(336, 204);
            this.textBox326.Name = "textBox326";
            this.textBox326.Size = new System.Drawing.Size(70, 25);
            this.textBox326.TabIndex = 257;
            this.textBox326.Text = "2";
            this.textBox326.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox325
            // 
            this.textBox325.BackColor = System.Drawing.SystemColors.Window;
            this.textBox325.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox325.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox325.Location = new System.Drawing.Point(336, 156);
            this.textBox325.Name = "textBox325";
            this.textBox325.Size = new System.Drawing.Size(70, 25);
            this.textBox325.TabIndex = 255;
            this.textBox325.Text = "1.8";
            this.textBox325.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox324
            // 
            this.textBox324.BackColor = System.Drawing.SystemColors.Window;
            this.textBox324.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox324.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox324.Location = new System.Drawing.Point(405, 180);
            this.textBox324.Name = "textBox324";
            this.textBox324.Size = new System.Drawing.Size(50, 25);
            this.textBox324.TabIndex = 259;
            this.textBox324.Text = "10";
            this.textBox324.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox323
            // 
            this.textBox323.BackColor = System.Drawing.SystemColors.Window;
            this.textBox323.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox323.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox323.Location = new System.Drawing.Point(405, 204);
            this.textBox323.Name = "textBox323";
            this.textBox323.Size = new System.Drawing.Size(50, 25);
            this.textBox323.TabIndex = 260;
            this.textBox323.Text = "98%";
            this.textBox323.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox322
            // 
            this.textBox322.BackColor = System.Drawing.SystemColors.Window;
            this.textBox322.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox322.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox322.Location = new System.Drawing.Point(405, 156);
            this.textBox322.Name = "textBox322";
            this.textBox322.Size = new System.Drawing.Size(50, 25);
            this.textBox322.TabIndex = 258;
            this.textBox322.Text = "5% ";
            this.textBox322.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox321
            // 
            this.textBox321.BackColor = System.Drawing.SystemColors.Window;
            this.textBox321.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox321.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox321.Location = new System.Drawing.Point(454, 180);
            this.textBox321.Name = "textBox321";
            this.textBox321.Size = new System.Drawing.Size(50, 25);
            this.textBox321.TabIndex = 262;
            this.textBox321.Text = "80";
            this.textBox321.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox320
            // 
            this.textBox320.BackColor = System.Drawing.SystemColors.Window;
            this.textBox320.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox320.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox320.Location = new System.Drawing.Point(454, 204);
            this.textBox320.Name = "textBox320";
            this.textBox320.Size = new System.Drawing.Size(50, 25);
            this.textBox320.TabIndex = 263;
            this.textBox320.Text = "100";
            this.textBox320.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox319
            // 
            this.textBox319.BackColor = System.Drawing.SystemColors.Window;
            this.textBox319.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox319.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox319.Location = new System.Drawing.Point(454, 156);
            this.textBox319.Name = "textBox319";
            this.textBox319.Size = new System.Drawing.Size(50, 25);
            this.textBox319.TabIndex = 261;
            this.textBox319.Text = "80";
            this.textBox319.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox318
            // 
            this.textBox318.BackColor = System.Drawing.SystemColors.Window;
            this.textBox318.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox318.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox318.Location = new System.Drawing.Point(503, 180);
            this.textBox318.Name = "textBox318";
            this.textBox318.Size = new System.Drawing.Size(50, 25);
            this.textBox318.TabIndex = 265;
            this.textBox318.Text = "1.6";
            this.textBox318.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox317
            // 
            this.textBox317.BackColor = System.Drawing.SystemColors.Window;
            this.textBox317.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox317.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox317.Location = new System.Drawing.Point(503, 204);
            this.textBox317.Name = "textBox317";
            this.textBox317.Size = new System.Drawing.Size(50, 25);
            this.textBox317.TabIndex = 266;
            this.textBox317.Text = "2";
            this.textBox317.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox316
            // 
            this.textBox316.BackColor = System.Drawing.SystemColors.Window;
            this.textBox316.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox316.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox316.Location = new System.Drawing.Point(503, 156);
            this.textBox316.Name = "textBox316";
            this.textBox316.Size = new System.Drawing.Size(50, 25);
            this.textBox316.TabIndex = 264;
            this.textBox316.Text = "1.6";
            this.textBox316.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox315
            // 
            this.textBox315.BackColor = System.Drawing.SystemColors.Window;
            this.textBox315.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox315.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox315.Location = new System.Drawing.Point(552, 180);
            this.textBox315.Name = "textBox315";
            this.textBox315.Size = new System.Drawing.Size(50, 25);
            this.textBox315.TabIndex = 268;
            this.textBox315.Text = "12";
            this.textBox315.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox314
            // 
            this.textBox314.BackColor = System.Drawing.SystemColors.Window;
            this.textBox314.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox314.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox314.Location = new System.Drawing.Point(552, 204);
            this.textBox314.Name = "textBox314";
            this.textBox314.Size = new System.Drawing.Size(50, 25);
            this.textBox314.TabIndex = 269;
            this.textBox314.Text = "98.5%";
            this.textBox314.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox313
            // 
            this.textBox313.BackColor = System.Drawing.SystemColors.Window;
            this.textBox313.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox313.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox313.Location = new System.Drawing.Point(552, 156);
            this.textBox313.Name = "textBox313";
            this.textBox313.Size = new System.Drawing.Size(50, 25);
            this.textBox313.TabIndex = 267;
            this.textBox313.Text = "3%";
            this.textBox313.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox312
            // 
            this.textBox312.BackColor = System.Drawing.SystemColors.Window;
            this.textBox312.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox312.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox312.Location = new System.Drawing.Point(601, 180);
            this.textBox312.Name = "textBox312";
            this.textBox312.Size = new System.Drawing.Size(50, 25);
            this.textBox312.TabIndex = 271;
            this.textBox312.Text = "90";
            this.textBox312.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox311
            // 
            this.textBox311.BackColor = System.Drawing.SystemColors.Window;
            this.textBox311.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox311.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox311.Location = new System.Drawing.Point(601, 204);
            this.textBox311.Name = "textBox311";
            this.textBox311.Size = new System.Drawing.Size(50, 25);
            this.textBox311.TabIndex = 272;
            this.textBox311.Text = "100";
            this.textBox311.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox310
            // 
            this.textBox310.BackColor = System.Drawing.SystemColors.Window;
            this.textBox310.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox310.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox310.Location = new System.Drawing.Point(601, 156);
            this.textBox310.Name = "textBox310";
            this.textBox310.Size = new System.Drawing.Size(50, 25);
            this.textBox310.TabIndex = 270;
            this.textBox310.Text = "90";
            this.textBox310.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox309
            // 
            this.textBox309.BackColor = System.Drawing.SystemColors.Window;
            this.textBox309.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox309.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox309.Location = new System.Drawing.Point(650, 108);
            this.textBox309.Name = "textBox309";
            this.textBox309.Size = new System.Drawing.Size(50, 25);
            this.textBox309.TabIndex = 274;
            this.textBox309.Text = "2.7";
            this.textBox309.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox308
            // 
            this.textBox308.BackColor = System.Drawing.SystemColors.Window;
            this.textBox308.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox308.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox308.Location = new System.Drawing.Point(650, 132);
            this.textBox308.Name = "textBox308";
            this.textBox308.Size = new System.Drawing.Size(50, 25);
            this.textBox308.TabIndex = 275;
            this.textBox308.Text = "2";
            this.textBox308.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox307
            // 
            this.textBox307.BackColor = System.Drawing.SystemColors.Window;
            this.textBox307.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox307.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox307.Location = new System.Drawing.Point(650, 84);
            this.textBox307.Name = "textBox307";
            this.textBox307.Size = new System.Drawing.Size(50, 25);
            this.textBox307.TabIndex = 273;
            this.textBox307.Text = "60";
            this.textBox307.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox24.Location = new System.Drawing.Point(650, 180);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(50, 25);
            this.textBox24.TabIndex = 277;
            this.textBox24.Text = "1.8";
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Window;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox23.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox23.Location = new System.Drawing.Point(650, 204);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(50, 25);
            this.textBox23.TabIndex = 278;
            this.textBox23.Text = "2";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox21.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox21.Location = new System.Drawing.Point(650, 156);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(50, 25);
            this.textBox21.TabIndex = 276;
            this.textBox21.Text = "1.8";
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox367
            // 
            this.textBox367.BackColor = System.Drawing.SystemColors.Window;
            this.textBox367.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox367.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox367.Location = new System.Drawing.Point(336, 228);
            this.textBox367.Name = "textBox367";
            this.textBox367.Size = new System.Drawing.Size(70, 25);
            this.textBox367.TabIndex = 279;
            this.textBox367.Text = "0.9";
            this.textBox367.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox366
            // 
            this.textBox366.BackColor = System.Drawing.SystemColors.Window;
            this.textBox366.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox366.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox366.Location = new System.Drawing.Point(405, 252);
            this.textBox366.Name = "textBox366";
            this.textBox366.Size = new System.Drawing.Size(50, 25);
            this.textBox366.TabIndex = 283;
            this.textBox366.Text = "提供";
            this.textBox366.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox365
            // 
            this.textBox365.BackColor = System.Drawing.SystemColors.Window;
            this.textBox365.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox365.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox365.Location = new System.Drawing.Point(405, 276);
            this.textBox365.Name = "textBox365";
            this.textBox365.Size = new System.Drawing.Size(50, 25);
            this.textBox365.TabIndex = 284;
            this.textBox365.Text = "提供";
            this.textBox365.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox364
            // 
            this.textBox364.BackColor = System.Drawing.SystemColors.Window;
            this.textBox364.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox364.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox364.Location = new System.Drawing.Point(405, 228);
            this.textBox364.Name = "textBox364";
            this.textBox364.Size = new System.Drawing.Size(50, 25);
            this.textBox364.TabIndex = 282;
            this.textBox364.Text = "30";
            this.textBox364.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox363
            // 
            this.textBox363.BackColor = System.Drawing.SystemColors.Window;
            this.textBox363.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox363.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox363.Location = new System.Drawing.Point(454, 252);
            this.textBox363.Name = "textBox363";
            this.textBox363.Size = new System.Drawing.Size(50, 25);
            this.textBox363.TabIndex = 286;
            this.textBox363.Text = "80";
            this.textBox363.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox362
            // 
            this.textBox362.BackColor = System.Drawing.SystemColors.Window;
            this.textBox362.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox362.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox362.Location = new System.Drawing.Point(454, 276);
            this.textBox362.Name = "textBox362";
            this.textBox362.Size = new System.Drawing.Size(50, 25);
            this.textBox362.TabIndex = 287;
            this.textBox362.Text = "100";
            this.textBox362.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox361
            // 
            this.textBox361.BackColor = System.Drawing.SystemColors.Window;
            this.textBox361.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox361.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox361.Location = new System.Drawing.Point(454, 228);
            this.textBox361.Name = "textBox361";
            this.textBox361.Size = new System.Drawing.Size(50, 25);
            this.textBox361.TabIndex = 285;
            this.textBox361.Text = "90";
            this.textBox361.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox360
            // 
            this.textBox360.BackColor = System.Drawing.SystemColors.Window;
            this.textBox360.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox360.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox360.Location = new System.Drawing.Point(503, 252);
            this.textBox360.Name = "textBox360";
            this.textBox360.Size = new System.Drawing.Size(50, 25);
            this.textBox360.TabIndex = 289;
            this.textBox360.Text = "0.8";
            this.textBox360.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox359
            // 
            this.textBox359.BackColor = System.Drawing.SystemColors.Window;
            this.textBox359.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox359.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox359.Location = new System.Drawing.Point(503, 276);
            this.textBox359.Name = "textBox359";
            this.textBox359.Size = new System.Drawing.Size(50, 25);
            this.textBox359.TabIndex = 290;
            this.textBox359.Text = "1";
            this.textBox359.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox358
            // 
            this.textBox358.BackColor = System.Drawing.SystemColors.Window;
            this.textBox358.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox358.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox358.Location = new System.Drawing.Point(503, 228);
            this.textBox358.Name = "textBox358";
            this.textBox358.Size = new System.Drawing.Size(50, 25);
            this.textBox358.TabIndex = 288;
            this.textBox358.Text = "0.9";
            this.textBox358.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox355
            // 
            this.textBox355.BackColor = System.Drawing.SystemColors.Window;
            this.textBox355.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox355.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox355.Location = new System.Drawing.Point(552, 228);
            this.textBox355.Name = "textBox355";
            this.textBox355.Size = new System.Drawing.Size(50, 25);
            this.textBox355.TabIndex = 291;
            this.textBox355.Text = "40";
            this.textBox355.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox352
            // 
            this.textBox352.BackColor = System.Drawing.SystemColors.Window;
            this.textBox352.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox352.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox352.Location = new System.Drawing.Point(601, 228);
            this.textBox352.Name = "textBox352";
            this.textBox352.Size = new System.Drawing.Size(50, 25);
            this.textBox352.TabIndex = 294;
            this.textBox352.Text = "80";
            this.textBox352.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox348
            // 
            this.textBox348.BackColor = System.Drawing.SystemColors.Window;
            this.textBox348.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox348.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox348.Location = new System.Drawing.Point(405, 324);
            this.textBox348.Name = "textBox348";
            this.textBox348.Size = new System.Drawing.Size(50, 25);
            this.textBox348.TabIndex = 301;
            this.textBox348.Text = "是";
            this.textBox348.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox347
            // 
            this.textBox347.BackColor = System.Drawing.SystemColors.Window;
            this.textBox347.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox347.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox347.Location = new System.Drawing.Point(405, 348);
            this.textBox347.Name = "textBox347";
            this.textBox347.Size = new System.Drawing.Size(50, 25);
            this.textBox347.TabIndex = 302;
            this.textBox347.Text = "1";
            this.textBox347.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox346
            // 
            this.textBox346.BackColor = System.Drawing.SystemColors.Window;
            this.textBox346.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox346.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox346.Location = new System.Drawing.Point(405, 300);
            this.textBox346.Name = "textBox346";
            this.textBox346.Size = new System.Drawing.Size(50, 25);
            this.textBox346.TabIndex = 300;
            this.textBox346.Text = "是";
            this.textBox346.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox345
            // 
            this.textBox345.BackColor = System.Drawing.SystemColors.Window;
            this.textBox345.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox345.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox345.Location = new System.Drawing.Point(454, 324);
            this.textBox345.Name = "textBox345";
            this.textBox345.Size = new System.Drawing.Size(50, 25);
            this.textBox345.TabIndex = 304;
            this.textBox345.Text = "80";
            this.textBox345.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox344
            // 
            this.textBox344.BackColor = System.Drawing.SystemColors.Window;
            this.textBox344.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox344.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox344.Location = new System.Drawing.Point(454, 348);
            this.textBox344.Name = "textBox344";
            this.textBox344.Size = new System.Drawing.Size(50, 25);
            this.textBox344.TabIndex = 305;
            this.textBox344.Text = "100";
            this.textBox344.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox343
            // 
            this.textBox343.BackColor = System.Drawing.SystemColors.Window;
            this.textBox343.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox343.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox343.Location = new System.Drawing.Point(454, 300);
            this.textBox343.Name = "textBox343";
            this.textBox343.Size = new System.Drawing.Size(50, 25);
            this.textBox343.TabIndex = 303;
            this.textBox343.Text = "80";
            this.textBox343.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox342
            // 
            this.textBox342.BackColor = System.Drawing.SystemColors.Window;
            this.textBox342.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox342.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox342.Location = new System.Drawing.Point(503, 324);
            this.textBox342.Name = "textBox342";
            this.textBox342.Size = new System.Drawing.Size(50, 25);
            this.textBox342.TabIndex = 307;
            this.textBox342.Text = "0.8";
            this.textBox342.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox341
            // 
            this.textBox341.BackColor = System.Drawing.SystemColors.Window;
            this.textBox341.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox341.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox341.Location = new System.Drawing.Point(503, 348);
            this.textBox341.Name = "textBox341";
            this.textBox341.Size = new System.Drawing.Size(50, 25);
            this.textBox341.TabIndex = 308;
            this.textBox341.Text = "2";
            this.textBox341.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox340
            // 
            this.textBox340.BackColor = System.Drawing.SystemColors.Window;
            this.textBox340.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox340.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox340.Location = new System.Drawing.Point(503, 300);
            this.textBox340.Name = "textBox340";
            this.textBox340.Size = new System.Drawing.Size(50, 25);
            this.textBox340.TabIndex = 306;
            this.textBox340.Text = "0.8";
            this.textBox340.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox331
            // 
            this.textBox331.BackColor = System.Drawing.SystemColors.Window;
            this.textBox331.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox331.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox331.Location = new System.Drawing.Point(650, 228);
            this.textBox331.Name = "textBox331";
            this.textBox331.Size = new System.Drawing.Size(50, 25);
            this.textBox331.TabIndex = 315;
            this.textBox331.Text = "0.8";
            this.textBox331.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox417
            // 
            this.textBox417.BackColor = System.Drawing.SystemColors.Window;
            this.textBox417.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox417.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox417.Location = new System.Drawing.Point(405, 372);
            this.textBox417.Name = "textBox417";
            this.textBox417.Size = new System.Drawing.Size(50, 25);
            this.textBox417.TabIndex = 322;
            this.textBox417.Text = "20%";
            this.textBox417.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox416
            // 
            this.textBox416.BackColor = System.Drawing.SystemColors.Window;
            this.textBox416.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox416.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox416.Location = new System.Drawing.Point(454, 372);
            this.textBox416.Name = "textBox416";
            this.textBox416.Size = new System.Drawing.Size(50, 25);
            this.textBox416.TabIndex = 323;
            this.textBox416.Text = "100";
            this.textBox416.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox415
            // 
            this.textBox415.BackColor = System.Drawing.SystemColors.Window;
            this.textBox415.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox415.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox415.Location = new System.Drawing.Point(503, 372);
            this.textBox415.Name = "textBox415";
            this.textBox415.Size = new System.Drawing.Size(50, 25);
            this.textBox415.TabIndex = 324;
            this.textBox415.Text = "1";
            this.textBox415.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox408
            // 
            this.textBox408.BackColor = System.Drawing.SystemColors.Window;
            this.textBox408.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox408.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox408.Location = new System.Drawing.Point(405, 420);
            this.textBox408.Name = "textBox408";
            this.textBox408.Size = new System.Drawing.Size(50, 25);
            this.textBox408.TabIndex = 332;
            this.textBox408.Text = "提供";
            this.textBox408.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox407
            // 
            this.textBox407.BackColor = System.Drawing.SystemColors.Window;
            this.textBox407.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox407.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox407.Location = new System.Drawing.Point(405, 444);
            this.textBox407.Name = "textBox407";
            this.textBox407.Size = new System.Drawing.Size(50, 25);
            this.textBox407.TabIndex = 333;
            this.textBox407.Text = "提供";
            this.textBox407.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox406
            // 
            this.textBox406.BackColor = System.Drawing.SystemColors.Window;
            this.textBox406.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox406.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox406.Location = new System.Drawing.Point(405, 396);
            this.textBox406.Name = "textBox406";
            this.textBox406.Size = new System.Drawing.Size(50, 25);
            this.textBox406.TabIndex = 331;
            this.textBox406.Text = "提供";
            this.textBox406.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox405
            // 
            this.textBox405.BackColor = System.Drawing.SystemColors.Window;
            this.textBox405.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox405.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox405.Location = new System.Drawing.Point(454, 420);
            this.textBox405.Name = "textBox405";
            this.textBox405.Size = new System.Drawing.Size(50, 25);
            this.textBox405.TabIndex = 335;
            this.textBox405.Text = "80";
            this.textBox405.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox404
            // 
            this.textBox404.BackColor = System.Drawing.SystemColors.Window;
            this.textBox404.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox404.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox404.Location = new System.Drawing.Point(454, 444);
            this.textBox404.Name = "textBox404";
            this.textBox404.Size = new System.Drawing.Size(50, 25);
            this.textBox404.TabIndex = 336;
            this.textBox404.Text = "100";
            this.textBox404.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox403
            // 
            this.textBox403.BackColor = System.Drawing.SystemColors.Window;
            this.textBox403.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox403.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox403.Location = new System.Drawing.Point(454, 396);
            this.textBox403.Name = "textBox403";
            this.textBox403.Size = new System.Drawing.Size(50, 25);
            this.textBox403.TabIndex = 334;
            this.textBox403.Text = "90";
            this.textBox403.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox402
            // 
            this.textBox402.BackColor = System.Drawing.SystemColors.Window;
            this.textBox402.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox402.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox402.Location = new System.Drawing.Point(503, 420);
            this.textBox402.Name = "textBox402";
            this.textBox402.Size = new System.Drawing.Size(50, 25);
            this.textBox402.TabIndex = 338;
            this.textBox402.Text = "0.8";
            this.textBox402.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox401
            // 
            this.textBox401.BackColor = System.Drawing.SystemColors.Window;
            this.textBox401.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox401.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox401.Location = new System.Drawing.Point(503, 444);
            this.textBox401.Name = "textBox401";
            this.textBox401.Size = new System.Drawing.Size(50, 25);
            this.textBox401.TabIndex = 339;
            this.textBox401.Text = "1";
            this.textBox401.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox400
            // 
            this.textBox400.BackColor = System.Drawing.SystemColors.Window;
            this.textBox400.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox400.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox400.Location = new System.Drawing.Point(503, 396);
            this.textBox400.Name = "textBox400";
            this.textBox400.Size = new System.Drawing.Size(50, 25);
            this.textBox400.TabIndex = 337;
            this.textBox400.Text = "0.9";
            this.textBox400.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox390
            // 
            this.textBox390.BackColor = System.Drawing.SystemColors.Window;
            this.textBox390.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox390.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox390.Location = new System.Drawing.Point(405, 492);
            this.textBox390.Name = "textBox390";
            this.textBox390.Size = new System.Drawing.Size(50, 25);
            this.textBox390.TabIndex = 350;
            this.textBox390.Text = "提供";
            this.textBox390.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox389
            // 
            this.textBox389.BackColor = System.Drawing.SystemColors.Window;
            this.textBox389.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox389.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox389.Location = new System.Drawing.Point(405, 516);
            this.textBox389.Name = "textBox389";
            this.textBox389.Size = new System.Drawing.Size(50, 25);
            this.textBox389.TabIndex = 351;
            this.textBox389.Text = "提供";
            this.textBox389.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox388
            // 
            this.textBox388.BackColor = System.Drawing.SystemColors.Window;
            this.textBox388.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox388.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox388.Location = new System.Drawing.Point(405, 468);
            this.textBox388.Name = "textBox388";
            this.textBox388.Size = new System.Drawing.Size(50, 25);
            this.textBox388.TabIndex = 349;
            this.textBox388.Text = "提供";
            this.textBox388.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox387
            // 
            this.textBox387.BackColor = System.Drawing.SystemColors.Window;
            this.textBox387.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox387.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox387.Location = new System.Drawing.Point(454, 492);
            this.textBox387.Name = "textBox387";
            this.textBox387.Size = new System.Drawing.Size(50, 25);
            this.textBox387.TabIndex = 353;
            this.textBox387.Text = "80";
            this.textBox387.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox386
            // 
            this.textBox386.BackColor = System.Drawing.SystemColors.Window;
            this.textBox386.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox386.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox386.Location = new System.Drawing.Point(454, 516);
            this.textBox386.Name = "textBox386";
            this.textBox386.Size = new System.Drawing.Size(50, 25);
            this.textBox386.TabIndex = 354;
            this.textBox386.Text = "100";
            this.textBox386.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox385
            // 
            this.textBox385.BackColor = System.Drawing.SystemColors.Window;
            this.textBox385.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox385.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox385.Location = new System.Drawing.Point(454, 468);
            this.textBox385.Name = "textBox385";
            this.textBox385.Size = new System.Drawing.Size(50, 25);
            this.textBox385.TabIndex = 352;
            this.textBox385.Text = "80";
            this.textBox385.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox384
            // 
            this.textBox384.BackColor = System.Drawing.SystemColors.Window;
            this.textBox384.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox384.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox384.Location = new System.Drawing.Point(503, 492);
            this.textBox384.Name = "textBox384";
            this.textBox384.Size = new System.Drawing.Size(50, 25);
            this.textBox384.TabIndex = 356;
            this.textBox384.Text = "0.8";
            this.textBox384.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox383
            // 
            this.textBox383.BackColor = System.Drawing.SystemColors.Window;
            this.textBox383.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox383.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox383.Location = new System.Drawing.Point(503, 516);
            this.textBox383.Name = "textBox383";
            this.textBox383.Size = new System.Drawing.Size(50, 25);
            this.textBox383.TabIndex = 357;
            this.textBox383.Text = "1";
            this.textBox383.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox382
            // 
            this.textBox382.BackColor = System.Drawing.SystemColors.Window;
            this.textBox382.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox382.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox382.Location = new System.Drawing.Point(503, 468);
            this.textBox382.Name = "textBox382";
            this.textBox382.Size = new System.Drawing.Size(50, 25);
            this.textBox382.TabIndex = 355;
            this.textBox382.Text = "0.8";
            this.textBox382.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox414
            // 
            this.textBox414.BackColor = System.Drawing.SystemColors.Window;
            this.textBox414.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox414.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox414.Location = new System.Drawing.Point(552, 252);
            this.textBox414.Name = "textBox414";
            this.textBox414.Size = new System.Drawing.Size(50, 25);
            this.textBox414.TabIndex = 370;
            this.textBox414.Text = "提供";
            this.textBox414.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox399
            // 
            this.textBox399.BackColor = System.Drawing.SystemColors.Window;
            this.textBox399.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox399.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox399.Location = new System.Drawing.Point(552, 276);
            this.textBox399.Name = "textBox399";
            this.textBox399.Size = new System.Drawing.Size(50, 25);
            this.textBox399.TabIndex = 371;
            this.textBox399.Text = "提供";
            this.textBox399.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox398
            // 
            this.textBox398.BackColor = System.Drawing.SystemColors.Window;
            this.textBox398.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox398.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox398.Location = new System.Drawing.Point(552, 324);
            this.textBox398.Name = "textBox398";
            this.textBox398.Size = new System.Drawing.Size(50, 25);
            this.textBox398.TabIndex = 373;
            this.textBox398.Text = "是";
            this.textBox398.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox397
            // 
            this.textBox397.BackColor = System.Drawing.SystemColors.Window;
            this.textBox397.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox397.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox397.Location = new System.Drawing.Point(552, 348);
            this.textBox397.Name = "textBox397";
            this.textBox397.Size = new System.Drawing.Size(50, 25);
            this.textBox397.TabIndex = 374;
            this.textBox397.Text = "1";
            this.textBox397.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox381
            // 
            this.textBox381.BackColor = System.Drawing.SystemColors.Window;
            this.textBox381.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox381.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox381.Location = new System.Drawing.Point(552, 300);
            this.textBox381.Name = "textBox381";
            this.textBox381.Size = new System.Drawing.Size(50, 25);
            this.textBox381.TabIndex = 372;
            this.textBox381.Text = "是";
            this.textBox381.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox380
            // 
            this.textBox380.BackColor = System.Drawing.SystemColors.Window;
            this.textBox380.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox380.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox380.Location = new System.Drawing.Point(552, 372);
            this.textBox380.Name = "textBox380";
            this.textBox380.Size = new System.Drawing.Size(50, 25);
            this.textBox380.TabIndex = 375;
            this.textBox380.Text = "20%";
            this.textBox380.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox379
            // 
            this.textBox379.BackColor = System.Drawing.SystemColors.Window;
            this.textBox379.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox379.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox379.Location = new System.Drawing.Point(552, 420);
            this.textBox379.Name = "textBox379";
            this.textBox379.Size = new System.Drawing.Size(50, 25);
            this.textBox379.TabIndex = 377;
            this.textBox379.Text = "提供";
            this.textBox379.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox357
            // 
            this.textBox357.BackColor = System.Drawing.SystemColors.Window;
            this.textBox357.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox357.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox357.Location = new System.Drawing.Point(552, 444);
            this.textBox357.Name = "textBox357";
            this.textBox357.Size = new System.Drawing.Size(50, 25);
            this.textBox357.TabIndex = 378;
            this.textBox357.Text = "提供";
            this.textBox357.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox356
            // 
            this.textBox356.BackColor = System.Drawing.SystemColors.Window;
            this.textBox356.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox356.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox356.Location = new System.Drawing.Point(552, 396);
            this.textBox356.Name = "textBox356";
            this.textBox356.Size = new System.Drawing.Size(50, 25);
            this.textBox356.TabIndex = 376;
            this.textBox356.Text = "提供";
            this.textBox356.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox339
            // 
            this.textBox339.BackColor = System.Drawing.SystemColors.Window;
            this.textBox339.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox339.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox339.Location = new System.Drawing.Point(552, 492);
            this.textBox339.Name = "textBox339";
            this.textBox339.Size = new System.Drawing.Size(50, 25);
            this.textBox339.TabIndex = 380;
            this.textBox339.Text = "提供";
            this.textBox339.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox338
            // 
            this.textBox338.BackColor = System.Drawing.SystemColors.Window;
            this.textBox338.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox338.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox338.Location = new System.Drawing.Point(552, 516);
            this.textBox338.Name = "textBox338";
            this.textBox338.Size = new System.Drawing.Size(50, 25);
            this.textBox338.TabIndex = 381;
            this.textBox338.Text = "提供";
            this.textBox338.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox337
            // 
            this.textBox337.BackColor = System.Drawing.SystemColors.Window;
            this.textBox337.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox337.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox337.Location = new System.Drawing.Point(552, 468);
            this.textBox337.Name = "textBox337";
            this.textBox337.Size = new System.Drawing.Size(50, 25);
            this.textBox337.TabIndex = 379;
            this.textBox337.Text = "提供";
            this.textBox337.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox412
            // 
            this.textBox412.BackColor = System.Drawing.SystemColors.Window;
            this.textBox412.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox412.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox412.Location = new System.Drawing.Point(650, 252);
            this.textBox412.Name = "textBox412";
            this.textBox412.Size = new System.Drawing.Size(50, 25);
            this.textBox412.TabIndex = 382;
            this.textBox412.Text = "0.8";
            this.textBox412.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox375
            // 
            this.textBox375.BackColor = System.Drawing.SystemColors.Window;
            this.textBox375.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox375.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox375.Location = new System.Drawing.Point(650, 276);
            this.textBox375.Name = "textBox375";
            this.textBox375.Size = new System.Drawing.Size(50, 25);
            this.textBox375.TabIndex = 383;
            this.textBox375.Text = "1";
            this.textBox375.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox374
            // 
            this.textBox374.BackColor = System.Drawing.SystemColors.Window;
            this.textBox374.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox374.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox374.Location = new System.Drawing.Point(650, 324);
            this.textBox374.Name = "textBox374";
            this.textBox374.Size = new System.Drawing.Size(50, 25);
            this.textBox374.TabIndex = 385;
            this.textBox374.Text = "0.8";
            this.textBox374.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox373
            // 
            this.textBox373.BackColor = System.Drawing.SystemColors.Window;
            this.textBox373.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox373.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox373.Location = new System.Drawing.Point(650, 348);
            this.textBox373.Name = "textBox373";
            this.textBox373.Size = new System.Drawing.Size(50, 25);
            this.textBox373.TabIndex = 386;
            this.textBox373.Text = "2";
            this.textBox373.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox372
            // 
            this.textBox372.BackColor = System.Drawing.SystemColors.Window;
            this.textBox372.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox372.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox372.Location = new System.Drawing.Point(650, 300);
            this.textBox372.Name = "textBox372";
            this.textBox372.Size = new System.Drawing.Size(50, 25);
            this.textBox372.TabIndex = 384;
            this.textBox372.Text = "0.8";
            this.textBox372.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox371
            // 
            this.textBox371.BackColor = System.Drawing.SystemColors.Window;
            this.textBox371.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox371.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox371.Location = new System.Drawing.Point(650, 372);
            this.textBox371.Name = "textBox371";
            this.textBox371.Size = new System.Drawing.Size(50, 25);
            this.textBox371.TabIndex = 387;
            this.textBox371.Text = "1";
            this.textBox371.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox370
            // 
            this.textBox370.BackColor = System.Drawing.SystemColors.Window;
            this.textBox370.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox370.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox370.Location = new System.Drawing.Point(650, 420);
            this.textBox370.Name = "textBox370";
            this.textBox370.Size = new System.Drawing.Size(50, 25);
            this.textBox370.TabIndex = 389;
            this.textBox370.Text = "0.8";
            this.textBox370.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox333
            // 
            this.textBox333.BackColor = System.Drawing.SystemColors.Window;
            this.textBox333.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox333.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox333.Location = new System.Drawing.Point(650, 444);
            this.textBox333.Name = "textBox333";
            this.textBox333.Size = new System.Drawing.Size(50, 25);
            this.textBox333.TabIndex = 390;
            this.textBox333.Text = "1";
            this.textBox333.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox332
            // 
            this.textBox332.BackColor = System.Drawing.SystemColors.Window;
            this.textBox332.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox332.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox332.Location = new System.Drawing.Point(650, 396);
            this.textBox332.Name = "textBox332";
            this.textBox332.Size = new System.Drawing.Size(50, 25);
            this.textBox332.TabIndex = 388;
            this.textBox332.Text = "0.9";
            this.textBox332.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox330
            // 
            this.textBox330.BackColor = System.Drawing.SystemColors.Window;
            this.textBox330.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox330.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox330.Location = new System.Drawing.Point(650, 492);
            this.textBox330.Name = "textBox330";
            this.textBox330.Size = new System.Drawing.Size(50, 25);
            this.textBox330.TabIndex = 392;
            this.textBox330.Text = "0.8";
            this.textBox330.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox329
            // 
            this.textBox329.BackColor = System.Drawing.SystemColors.Window;
            this.textBox329.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox329.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox329.Location = new System.Drawing.Point(650, 516);
            this.textBox329.Name = "textBox329";
            this.textBox329.Size = new System.Drawing.Size(50, 25);
            this.textBox329.TabIndex = 393;
            this.textBox329.Text = "1";
            this.textBox329.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox328
            // 
            this.textBox328.BackColor = System.Drawing.SystemColors.Window;
            this.textBox328.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox328.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox328.Location = new System.Drawing.Point(650, 468);
            this.textBox328.Name = "textBox328";
            this.textBox328.Size = new System.Drawing.Size(50, 25);
            this.textBox328.TabIndex = 391;
            this.textBox328.Text = "0.8";
            this.textBox328.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox413
            // 
            this.textBox413.BackColor = System.Drawing.SystemColors.Window;
            this.textBox413.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox413.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox413.Location = new System.Drawing.Point(601, 252);
            this.textBox413.Name = "textBox413";
            this.textBox413.Size = new System.Drawing.Size(50, 25);
            this.textBox413.TabIndex = 394;
            this.textBox413.Text = "80";
            this.textBox413.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox396
            // 
            this.textBox396.BackColor = System.Drawing.SystemColors.Window;
            this.textBox396.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox396.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox396.Location = new System.Drawing.Point(601, 276);
            this.textBox396.Name = "textBox396";
            this.textBox396.Size = new System.Drawing.Size(50, 25);
            this.textBox396.TabIndex = 395;
            this.textBox396.Text = "100";
            this.textBox396.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox395
            // 
            this.textBox395.BackColor = System.Drawing.SystemColors.Window;
            this.textBox395.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox395.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox395.Location = new System.Drawing.Point(601, 324);
            this.textBox395.Name = "textBox395";
            this.textBox395.Size = new System.Drawing.Size(50, 25);
            this.textBox395.TabIndex = 397;
            this.textBox395.Text = "80";
            this.textBox395.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox394
            // 
            this.textBox394.BackColor = System.Drawing.SystemColors.Window;
            this.textBox394.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox394.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox394.Location = new System.Drawing.Point(601, 348);
            this.textBox394.Name = "textBox394";
            this.textBox394.Size = new System.Drawing.Size(50, 25);
            this.textBox394.TabIndex = 398;
            this.textBox394.Text = "100";
            this.textBox394.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox378
            // 
            this.textBox378.BackColor = System.Drawing.SystemColors.Window;
            this.textBox378.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox378.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox378.Location = new System.Drawing.Point(601, 300);
            this.textBox378.Name = "textBox378";
            this.textBox378.Size = new System.Drawing.Size(50, 25);
            this.textBox378.TabIndex = 396;
            this.textBox378.Text = "80";
            this.textBox378.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox377
            // 
            this.textBox377.BackColor = System.Drawing.SystemColors.Window;
            this.textBox377.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox377.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox377.Location = new System.Drawing.Point(601, 372);
            this.textBox377.Name = "textBox377";
            this.textBox377.Size = new System.Drawing.Size(50, 25);
            this.textBox377.TabIndex = 399;
            this.textBox377.Text = "100";
            this.textBox377.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox376
            // 
            this.textBox376.BackColor = System.Drawing.SystemColors.Window;
            this.textBox376.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox376.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox376.Location = new System.Drawing.Point(601, 420);
            this.textBox376.Name = "textBox376";
            this.textBox376.Size = new System.Drawing.Size(50, 25);
            this.textBox376.TabIndex = 401;
            this.textBox376.Text = "80";
            this.textBox376.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox354
            // 
            this.textBox354.BackColor = System.Drawing.SystemColors.Window;
            this.textBox354.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox354.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox354.Location = new System.Drawing.Point(601, 444);
            this.textBox354.Name = "textBox354";
            this.textBox354.Size = new System.Drawing.Size(50, 25);
            this.textBox354.TabIndex = 402;
            this.textBox354.Text = "100";
            this.textBox354.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox353
            // 
            this.textBox353.BackColor = System.Drawing.SystemColors.Window;
            this.textBox353.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox353.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox353.Location = new System.Drawing.Point(601, 396);
            this.textBox353.Name = "textBox353";
            this.textBox353.Size = new System.Drawing.Size(50, 25);
            this.textBox353.TabIndex = 400;
            this.textBox353.Text = "90";
            this.textBox353.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox336
            // 
            this.textBox336.BackColor = System.Drawing.SystemColors.Window;
            this.textBox336.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox336.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox336.Location = new System.Drawing.Point(601, 492);
            this.textBox336.Name = "textBox336";
            this.textBox336.Size = new System.Drawing.Size(50, 25);
            this.textBox336.TabIndex = 404;
            this.textBox336.Text = "80";
            this.textBox336.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox335
            // 
            this.textBox335.BackColor = System.Drawing.SystemColors.Window;
            this.textBox335.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox335.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox335.Location = new System.Drawing.Point(601, 516);
            this.textBox335.Name = "textBox335";
            this.textBox335.Size = new System.Drawing.Size(50, 25);
            this.textBox335.TabIndex = 405;
            this.textBox335.Text = "100";
            this.textBox335.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox334
            // 
            this.textBox334.BackColor = System.Drawing.SystemColors.Window;
            this.textBox334.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox334.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox334.Location = new System.Drawing.Point(601, 468);
            this.textBox334.Name = "textBox334";
            this.textBox334.Size = new System.Drawing.Size(50, 25);
            this.textBox334.TabIndex = 403;
            this.textBox334.Text = "80";
            this.textBox334.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox418
            // 
            this.textBox418.BackColor = System.Drawing.SystemColors.Window;
            this.textBox418.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox418.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox418.Location = new System.Drawing.Point(336, 252);
            this.textBox418.Name = "textBox418";
            this.textBox418.Size = new System.Drawing.Size(70, 25);
            this.textBox418.TabIndex = 406;
            this.textBox418.Text = "0.8";
            this.textBox418.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox411
            // 
            this.textBox411.BackColor = System.Drawing.SystemColors.Window;
            this.textBox411.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox411.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox411.Location = new System.Drawing.Point(336, 276);
            this.textBox411.Name = "textBox411";
            this.textBox411.Size = new System.Drawing.Size(70, 25);
            this.textBox411.TabIndex = 407;
            this.textBox411.Text = "1";
            this.textBox411.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox410
            // 
            this.textBox410.BackColor = System.Drawing.SystemColors.Window;
            this.textBox410.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox410.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox410.Location = new System.Drawing.Point(336, 324);
            this.textBox410.Name = "textBox410";
            this.textBox410.Size = new System.Drawing.Size(70, 25);
            this.textBox410.TabIndex = 409;
            this.textBox410.Text = "0.8";
            this.textBox410.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox409
            // 
            this.textBox409.BackColor = System.Drawing.SystemColors.Window;
            this.textBox409.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox409.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox409.Location = new System.Drawing.Point(336, 348);
            this.textBox409.Name = "textBox409";
            this.textBox409.Size = new System.Drawing.Size(70, 25);
            this.textBox409.TabIndex = 410;
            this.textBox409.Text = "2";
            this.textBox409.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox393
            // 
            this.textBox393.BackColor = System.Drawing.SystemColors.Window;
            this.textBox393.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox393.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox393.Location = new System.Drawing.Point(336, 300);
            this.textBox393.Name = "textBox393";
            this.textBox393.Size = new System.Drawing.Size(70, 25);
            this.textBox393.TabIndex = 408;
            this.textBox393.Text = "0.8";
            this.textBox393.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox392
            // 
            this.textBox392.BackColor = System.Drawing.SystemColors.Window;
            this.textBox392.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox392.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox392.Location = new System.Drawing.Point(336, 372);
            this.textBox392.Name = "textBox392";
            this.textBox392.Size = new System.Drawing.Size(70, 25);
            this.textBox392.TabIndex = 411;
            this.textBox392.Text = "1";
            this.textBox392.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox391
            // 
            this.textBox391.BackColor = System.Drawing.SystemColors.Window;
            this.textBox391.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox391.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox391.Location = new System.Drawing.Point(336, 420);
            this.textBox391.Name = "textBox391";
            this.textBox391.Size = new System.Drawing.Size(70, 25);
            this.textBox391.TabIndex = 413;
            this.textBox391.Text = "0.8";
            this.textBox391.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox369
            // 
            this.textBox369.BackColor = System.Drawing.SystemColors.Window;
            this.textBox369.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox369.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox369.Location = new System.Drawing.Point(336, 444);
            this.textBox369.Name = "textBox369";
            this.textBox369.Size = new System.Drawing.Size(70, 25);
            this.textBox369.TabIndex = 414;
            this.textBox369.Text = "1";
            this.textBox369.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox368
            // 
            this.textBox368.BackColor = System.Drawing.SystemColors.Window;
            this.textBox368.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox368.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox368.Location = new System.Drawing.Point(336, 396);
            this.textBox368.Name = "textBox368";
            this.textBox368.Size = new System.Drawing.Size(70, 25);
            this.textBox368.TabIndex = 412;
            this.textBox368.Text = "0.9";
            this.textBox368.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox351
            // 
            this.textBox351.BackColor = System.Drawing.SystemColors.Window;
            this.textBox351.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox351.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox351.Location = new System.Drawing.Point(336, 492);
            this.textBox351.Name = "textBox351";
            this.textBox351.Size = new System.Drawing.Size(70, 25);
            this.textBox351.TabIndex = 416;
            this.textBox351.Text = "0.8";
            this.textBox351.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox350
            // 
            this.textBox350.BackColor = System.Drawing.SystemColors.Window;
            this.textBox350.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox350.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox350.Location = new System.Drawing.Point(336, 516);
            this.textBox350.Name = "textBox350";
            this.textBox350.Size = new System.Drawing.Size(70, 25);
            this.textBox350.TabIndex = 417;
            this.textBox350.Text = "1";
            this.textBox350.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox349
            // 
            this.textBox349.BackColor = System.Drawing.SystemColors.Window;
            this.textBox349.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox349.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox349.Location = new System.Drawing.Point(336, 468);
            this.textBox349.Name = "textBox349";
            this.textBox349.Size = new System.Drawing.Size(70, 25);
            this.textBox349.TabIndex = 415;
            this.textBox349.Text = "0.8";
            this.textBox349.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.SystemColors.Window;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label50.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label50.Location = new System.Drawing.Point(699, 12);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(148, 25);
            this.label50.TabIndex = 418;
            this.label50.Text = "SP002034";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox478
            // 
            this.textBox478.BackColor = System.Drawing.SystemColors.Window;
            this.textBox478.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox478.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox478.Location = new System.Drawing.Point(699, 108);
            this.textBox478.Name = "textBox478";
            this.textBox478.Size = new System.Drawing.Size(50, 25);
            this.textBox478.TabIndex = 420;
            this.textBox478.Text = "490000";
            this.textBox478.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox477
            // 
            this.textBox477.BackColor = System.Drawing.SystemColors.Window;
            this.textBox477.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox477.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox477.Location = new System.Drawing.Point(699, 132);
            this.textBox477.Name = "textBox477";
            this.textBox477.Size = new System.Drawing.Size(50, 25);
            this.textBox477.TabIndex = 421;
            this.textBox477.Text = "5";
            this.textBox477.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox476
            // 
            this.textBox476.BackColor = System.Drawing.SystemColors.Window;
            this.textBox476.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox476.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox476.Location = new System.Drawing.Point(699, 84);
            this.textBox476.Name = "textBox476";
            this.textBox476.Size = new System.Drawing.Size(50, 25);
            this.textBox476.TabIndex = 419;
            this.textBox476.Text = "510000";
            this.textBox476.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.SystemColors.Window;
            this.label40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label40.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.Location = new System.Drawing.Point(699, 36);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(50, 25);
            this.label40.TabIndex = 422;
            this.label40.Text = "值";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox475
            // 
            this.textBox475.BackColor = System.Drawing.SystemColors.Window;
            this.textBox475.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox475.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox475.Location = new System.Drawing.Point(699, 60);
            this.textBox475.Name = "textBox475";
            this.textBox475.Size = new System.Drawing.Size(50, 25);
            this.textBox475.TabIndex = 423;
            this.textBox475.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox474
            // 
            this.textBox474.BackColor = System.Drawing.SystemColors.Window;
            this.textBox474.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox474.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox474.Location = new System.Drawing.Point(748, 108);
            this.textBox474.Name = "textBox474";
            this.textBox474.Size = new System.Drawing.Size(50, 25);
            this.textBox474.TabIndex = 425;
            this.textBox474.Text = "90";
            this.textBox474.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox473
            // 
            this.textBox473.BackColor = System.Drawing.SystemColors.Window;
            this.textBox473.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox473.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox473.Location = new System.Drawing.Point(748, 132);
            this.textBox473.Name = "textBox473";
            this.textBox473.Size = new System.Drawing.Size(50, 25);
            this.textBox473.TabIndex = 426;
            this.textBox473.Text = "100";
            this.textBox473.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox472
            // 
            this.textBox472.BackColor = System.Drawing.SystemColors.Window;
            this.textBox472.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox472.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox472.Location = new System.Drawing.Point(748, 84);
            this.textBox472.Name = "textBox472";
            this.textBox472.Size = new System.Drawing.Size(50, 25);
            this.textBox472.TabIndex = 424;
            this.textBox472.Text = "88";
            this.textBox472.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.SystemColors.Window;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label39.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label39.Location = new System.Drawing.Point(748, 36);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(50, 25);
            this.label39.TabIndex = 427;
            this.label39.Text = "评估";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox471
            // 
            this.textBox471.BackColor = System.Drawing.SystemColors.Window;
            this.textBox471.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox471.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox471.Location = new System.Drawing.Point(748, 60);
            this.textBox471.Name = "textBox471";
            this.textBox471.Size = new System.Drawing.Size(50, 25);
            this.textBox471.TabIndex = 428;
            this.textBox471.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.SystemColors.Window;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label38.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label38.Location = new System.Drawing.Point(797, 36);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 25);
            this.label38.TabIndex = 429;
            this.label38.Text = "分";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox470
            // 
            this.textBox470.BackColor = System.Drawing.SystemColors.Window;
            this.textBox470.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox470.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox470.Location = new System.Drawing.Point(797, 60);
            this.textBox470.Name = "textBox470";
            this.textBox470.Size = new System.Drawing.Size(50, 25);
            this.textBox470.TabIndex = 430;
            this.textBox470.Text = "88.8";
            this.textBox470.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox469
            // 
            this.textBox469.BackColor = System.Drawing.SystemColors.Window;
            this.textBox469.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox469.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox469.Location = new System.Drawing.Point(699, 180);
            this.textBox469.Name = "textBox469";
            this.textBox469.Size = new System.Drawing.Size(50, 25);
            this.textBox469.TabIndex = 432;
            this.textBox469.Text = "12";
            this.textBox469.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox468
            // 
            this.textBox468.BackColor = System.Drawing.SystemColors.Window;
            this.textBox468.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox468.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox468.Location = new System.Drawing.Point(699, 204);
            this.textBox468.Name = "textBox468";
            this.textBox468.Size = new System.Drawing.Size(50, 25);
            this.textBox468.TabIndex = 433;
            this.textBox468.Text = "98.5%";
            this.textBox468.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox467
            // 
            this.textBox467.BackColor = System.Drawing.SystemColors.Window;
            this.textBox467.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox467.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox467.Location = new System.Drawing.Point(699, 156);
            this.textBox467.Name = "textBox467";
            this.textBox467.Size = new System.Drawing.Size(50, 25);
            this.textBox467.TabIndex = 431;
            this.textBox467.Text = "3%";
            this.textBox467.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox466
            // 
            this.textBox466.BackColor = System.Drawing.SystemColors.Window;
            this.textBox466.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox466.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox466.Location = new System.Drawing.Point(748, 180);
            this.textBox466.Name = "textBox466";
            this.textBox466.Size = new System.Drawing.Size(50, 25);
            this.textBox466.TabIndex = 435;
            this.textBox466.Text = "90";
            this.textBox466.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox465
            // 
            this.textBox465.BackColor = System.Drawing.SystemColors.Window;
            this.textBox465.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox465.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox465.Location = new System.Drawing.Point(748, 204);
            this.textBox465.Name = "textBox465";
            this.textBox465.Size = new System.Drawing.Size(50, 25);
            this.textBox465.TabIndex = 436;
            this.textBox465.Text = "100";
            this.textBox465.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox464
            // 
            this.textBox464.BackColor = System.Drawing.SystemColors.Window;
            this.textBox464.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox464.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox464.Location = new System.Drawing.Point(748, 156);
            this.textBox464.Name = "textBox464";
            this.textBox464.Size = new System.Drawing.Size(50, 25);
            this.textBox464.TabIndex = 434;
            this.textBox464.Text = "90";
            this.textBox464.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox463
            // 
            this.textBox463.BackColor = System.Drawing.SystemColors.Window;
            this.textBox463.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox463.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox463.Location = new System.Drawing.Point(797, 108);
            this.textBox463.Name = "textBox463";
            this.textBox463.Size = new System.Drawing.Size(50, 25);
            this.textBox463.TabIndex = 438;
            this.textBox463.Text = "2.7";
            this.textBox463.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox462
            // 
            this.textBox462.BackColor = System.Drawing.SystemColors.Window;
            this.textBox462.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox462.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox462.Location = new System.Drawing.Point(797, 132);
            this.textBox462.Name = "textBox462";
            this.textBox462.Size = new System.Drawing.Size(50, 25);
            this.textBox462.TabIndex = 439;
            this.textBox462.Text = "2";
            this.textBox462.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox461
            // 
            this.textBox461.BackColor = System.Drawing.SystemColors.Window;
            this.textBox461.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox461.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox461.Location = new System.Drawing.Point(797, 84);
            this.textBox461.Name = "textBox461";
            this.textBox461.Size = new System.Drawing.Size(50, 25);
            this.textBox461.TabIndex = 437;
            this.textBox461.Text = "66";
            this.textBox461.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox460
            // 
            this.textBox460.BackColor = System.Drawing.SystemColors.Window;
            this.textBox460.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox460.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox460.Location = new System.Drawing.Point(797, 180);
            this.textBox460.Name = "textBox460";
            this.textBox460.Size = new System.Drawing.Size(50, 25);
            this.textBox460.TabIndex = 441;
            this.textBox460.Text = "1.8";
            this.textBox460.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox459
            // 
            this.textBox459.BackColor = System.Drawing.SystemColors.Window;
            this.textBox459.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox459.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox459.Location = new System.Drawing.Point(797, 204);
            this.textBox459.Name = "textBox459";
            this.textBox459.Size = new System.Drawing.Size(50, 25);
            this.textBox459.TabIndex = 442;
            this.textBox459.Text = "2";
            this.textBox459.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox458
            // 
            this.textBox458.BackColor = System.Drawing.SystemColors.Window;
            this.textBox458.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox458.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox458.Location = new System.Drawing.Point(797, 156);
            this.textBox458.Name = "textBox458";
            this.textBox458.Size = new System.Drawing.Size(50, 25);
            this.textBox458.TabIndex = 440;
            this.textBox458.Text = "1.8";
            this.textBox458.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox457
            // 
            this.textBox457.BackColor = System.Drawing.SystemColors.Window;
            this.textBox457.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox457.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox457.Location = new System.Drawing.Point(699, 228);
            this.textBox457.Name = "textBox457";
            this.textBox457.Size = new System.Drawing.Size(50, 25);
            this.textBox457.TabIndex = 443;
            this.textBox457.Text = "40";
            this.textBox457.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox456
            // 
            this.textBox456.BackColor = System.Drawing.SystemColors.Window;
            this.textBox456.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox456.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox456.Location = new System.Drawing.Point(748, 228);
            this.textBox456.Name = "textBox456";
            this.textBox456.Size = new System.Drawing.Size(50, 25);
            this.textBox456.TabIndex = 444;
            this.textBox456.Text = "80";
            this.textBox456.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox455
            // 
            this.textBox455.BackColor = System.Drawing.SystemColors.Window;
            this.textBox455.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox455.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox455.Location = new System.Drawing.Point(797, 228);
            this.textBox455.Name = "textBox455";
            this.textBox455.Size = new System.Drawing.Size(50, 25);
            this.textBox455.TabIndex = 445;
            this.textBox455.Text = "0.8";
            this.textBox455.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox454
            // 
            this.textBox454.BackColor = System.Drawing.SystemColors.Window;
            this.textBox454.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox454.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox454.Location = new System.Drawing.Point(699, 252);
            this.textBox454.Name = "textBox454";
            this.textBox454.Size = new System.Drawing.Size(50, 25);
            this.textBox454.TabIndex = 446;
            this.textBox454.Text = "提供";
            this.textBox454.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox453
            // 
            this.textBox453.BackColor = System.Drawing.SystemColors.Window;
            this.textBox453.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox453.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox453.Location = new System.Drawing.Point(699, 276);
            this.textBox453.Name = "textBox453";
            this.textBox453.Size = new System.Drawing.Size(50, 25);
            this.textBox453.TabIndex = 447;
            this.textBox453.Text = "提供";
            this.textBox453.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox452
            // 
            this.textBox452.BackColor = System.Drawing.SystemColors.Window;
            this.textBox452.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox452.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox452.Location = new System.Drawing.Point(699, 324);
            this.textBox452.Name = "textBox452";
            this.textBox452.Size = new System.Drawing.Size(50, 25);
            this.textBox452.TabIndex = 449;
            this.textBox452.Text = "是";
            this.textBox452.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox451
            // 
            this.textBox451.BackColor = System.Drawing.SystemColors.Window;
            this.textBox451.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox451.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox451.Location = new System.Drawing.Point(699, 348);
            this.textBox451.Name = "textBox451";
            this.textBox451.Size = new System.Drawing.Size(50, 25);
            this.textBox451.TabIndex = 450;
            this.textBox451.Text = "1";
            this.textBox451.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox450
            // 
            this.textBox450.BackColor = System.Drawing.SystemColors.Window;
            this.textBox450.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox450.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox450.Location = new System.Drawing.Point(699, 300);
            this.textBox450.Name = "textBox450";
            this.textBox450.Size = new System.Drawing.Size(50, 25);
            this.textBox450.TabIndex = 448;
            this.textBox450.Text = "是";
            this.textBox450.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox449
            // 
            this.textBox449.BackColor = System.Drawing.SystemColors.Window;
            this.textBox449.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox449.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox449.Location = new System.Drawing.Point(699, 372);
            this.textBox449.Name = "textBox449";
            this.textBox449.Size = new System.Drawing.Size(50, 25);
            this.textBox449.TabIndex = 451;
            this.textBox449.Text = "20%";
            this.textBox449.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox448
            // 
            this.textBox448.BackColor = System.Drawing.SystemColors.Window;
            this.textBox448.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox448.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox448.Location = new System.Drawing.Point(699, 420);
            this.textBox448.Name = "textBox448";
            this.textBox448.Size = new System.Drawing.Size(50, 25);
            this.textBox448.TabIndex = 453;
            this.textBox448.Text = "提供";
            this.textBox448.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox447
            // 
            this.textBox447.BackColor = System.Drawing.SystemColors.Window;
            this.textBox447.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox447.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox447.Location = new System.Drawing.Point(699, 444);
            this.textBox447.Name = "textBox447";
            this.textBox447.Size = new System.Drawing.Size(50, 25);
            this.textBox447.TabIndex = 454;
            this.textBox447.Text = "提供";
            this.textBox447.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox446
            // 
            this.textBox446.BackColor = System.Drawing.SystemColors.Window;
            this.textBox446.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox446.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox446.Location = new System.Drawing.Point(699, 396);
            this.textBox446.Name = "textBox446";
            this.textBox446.Size = new System.Drawing.Size(50, 25);
            this.textBox446.TabIndex = 452;
            this.textBox446.Text = "提供";
            this.textBox446.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox445
            // 
            this.textBox445.BackColor = System.Drawing.SystemColors.Window;
            this.textBox445.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox445.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox445.Location = new System.Drawing.Point(699, 492);
            this.textBox445.Name = "textBox445";
            this.textBox445.Size = new System.Drawing.Size(50, 25);
            this.textBox445.TabIndex = 456;
            this.textBox445.Text = "提供";
            this.textBox445.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox444
            // 
            this.textBox444.BackColor = System.Drawing.SystemColors.Window;
            this.textBox444.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox444.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox444.Location = new System.Drawing.Point(699, 516);
            this.textBox444.Name = "textBox444";
            this.textBox444.Size = new System.Drawing.Size(50, 25);
            this.textBox444.TabIndex = 457;
            this.textBox444.Text = "提供";
            this.textBox444.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox443
            // 
            this.textBox443.BackColor = System.Drawing.SystemColors.Window;
            this.textBox443.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox443.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox443.Location = new System.Drawing.Point(699, 468);
            this.textBox443.Name = "textBox443";
            this.textBox443.Size = new System.Drawing.Size(50, 25);
            this.textBox443.TabIndex = 455;
            this.textBox443.Text = "提供";
            this.textBox443.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox442
            // 
            this.textBox442.BackColor = System.Drawing.SystemColors.Window;
            this.textBox442.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox442.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox442.Location = new System.Drawing.Point(797, 252);
            this.textBox442.Name = "textBox442";
            this.textBox442.Size = new System.Drawing.Size(50, 25);
            this.textBox442.TabIndex = 458;
            this.textBox442.Text = "0.8";
            this.textBox442.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox441
            // 
            this.textBox441.BackColor = System.Drawing.SystemColors.Window;
            this.textBox441.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox441.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox441.Location = new System.Drawing.Point(797, 276);
            this.textBox441.Name = "textBox441";
            this.textBox441.Size = new System.Drawing.Size(50, 25);
            this.textBox441.TabIndex = 459;
            this.textBox441.Text = "1";
            this.textBox441.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox440
            // 
            this.textBox440.BackColor = System.Drawing.SystemColors.Window;
            this.textBox440.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox440.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox440.Location = new System.Drawing.Point(797, 324);
            this.textBox440.Name = "textBox440";
            this.textBox440.Size = new System.Drawing.Size(50, 25);
            this.textBox440.TabIndex = 461;
            this.textBox440.Text = "0.8";
            this.textBox440.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox439
            // 
            this.textBox439.BackColor = System.Drawing.SystemColors.Window;
            this.textBox439.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox439.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox439.Location = new System.Drawing.Point(797, 348);
            this.textBox439.Name = "textBox439";
            this.textBox439.Size = new System.Drawing.Size(50, 25);
            this.textBox439.TabIndex = 462;
            this.textBox439.Text = "2";
            this.textBox439.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox438
            // 
            this.textBox438.BackColor = System.Drawing.SystemColors.Window;
            this.textBox438.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox438.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox438.Location = new System.Drawing.Point(797, 300);
            this.textBox438.Name = "textBox438";
            this.textBox438.Size = new System.Drawing.Size(50, 25);
            this.textBox438.TabIndex = 460;
            this.textBox438.Text = "0.8";
            this.textBox438.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox437
            // 
            this.textBox437.BackColor = System.Drawing.SystemColors.Window;
            this.textBox437.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox437.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox437.Location = new System.Drawing.Point(797, 372);
            this.textBox437.Name = "textBox437";
            this.textBox437.Size = new System.Drawing.Size(50, 25);
            this.textBox437.TabIndex = 463;
            this.textBox437.Text = "1";
            this.textBox437.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox436
            // 
            this.textBox436.BackColor = System.Drawing.SystemColors.Window;
            this.textBox436.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox436.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox436.Location = new System.Drawing.Point(797, 420);
            this.textBox436.Name = "textBox436";
            this.textBox436.Size = new System.Drawing.Size(50, 25);
            this.textBox436.TabIndex = 465;
            this.textBox436.Text = "0.8";
            this.textBox436.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox435
            // 
            this.textBox435.BackColor = System.Drawing.SystemColors.Window;
            this.textBox435.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox435.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox435.Location = new System.Drawing.Point(797, 444);
            this.textBox435.Name = "textBox435";
            this.textBox435.Size = new System.Drawing.Size(50, 25);
            this.textBox435.TabIndex = 466;
            this.textBox435.Text = "1";
            this.textBox435.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox434
            // 
            this.textBox434.BackColor = System.Drawing.SystemColors.Window;
            this.textBox434.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox434.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox434.Location = new System.Drawing.Point(797, 396);
            this.textBox434.Name = "textBox434";
            this.textBox434.Size = new System.Drawing.Size(50, 25);
            this.textBox434.TabIndex = 464;
            this.textBox434.Text = "0.9";
            this.textBox434.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox433
            // 
            this.textBox433.BackColor = System.Drawing.SystemColors.Window;
            this.textBox433.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox433.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox433.Location = new System.Drawing.Point(797, 492);
            this.textBox433.Name = "textBox433";
            this.textBox433.Size = new System.Drawing.Size(50, 25);
            this.textBox433.TabIndex = 468;
            this.textBox433.Text = "0.8";
            this.textBox433.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox432
            // 
            this.textBox432.BackColor = System.Drawing.SystemColors.Window;
            this.textBox432.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox432.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox432.Location = new System.Drawing.Point(797, 516);
            this.textBox432.Name = "textBox432";
            this.textBox432.Size = new System.Drawing.Size(50, 25);
            this.textBox432.TabIndex = 469;
            this.textBox432.Text = "1";
            this.textBox432.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox431
            // 
            this.textBox431.BackColor = System.Drawing.SystemColors.Window;
            this.textBox431.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox431.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox431.Location = new System.Drawing.Point(797, 468);
            this.textBox431.Name = "textBox431";
            this.textBox431.Size = new System.Drawing.Size(50, 25);
            this.textBox431.TabIndex = 467;
            this.textBox431.Text = "0.8";
            this.textBox431.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox430
            // 
            this.textBox430.BackColor = System.Drawing.SystemColors.Window;
            this.textBox430.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox430.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox430.Location = new System.Drawing.Point(748, 252);
            this.textBox430.Name = "textBox430";
            this.textBox430.Size = new System.Drawing.Size(50, 25);
            this.textBox430.TabIndex = 470;
            this.textBox430.Text = "80";
            this.textBox430.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox429
            // 
            this.textBox429.BackColor = System.Drawing.SystemColors.Window;
            this.textBox429.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox429.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox429.Location = new System.Drawing.Point(748, 276);
            this.textBox429.Name = "textBox429";
            this.textBox429.Size = new System.Drawing.Size(50, 25);
            this.textBox429.TabIndex = 471;
            this.textBox429.Text = "100";
            this.textBox429.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox428
            // 
            this.textBox428.BackColor = System.Drawing.SystemColors.Window;
            this.textBox428.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox428.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox428.Location = new System.Drawing.Point(748, 324);
            this.textBox428.Name = "textBox428";
            this.textBox428.Size = new System.Drawing.Size(50, 25);
            this.textBox428.TabIndex = 473;
            this.textBox428.Text = "80";
            this.textBox428.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox427
            // 
            this.textBox427.BackColor = System.Drawing.SystemColors.Window;
            this.textBox427.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox427.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox427.Location = new System.Drawing.Point(748, 348);
            this.textBox427.Name = "textBox427";
            this.textBox427.Size = new System.Drawing.Size(50, 25);
            this.textBox427.TabIndex = 474;
            this.textBox427.Text = "100";
            this.textBox427.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox426
            // 
            this.textBox426.BackColor = System.Drawing.SystemColors.Window;
            this.textBox426.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox426.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox426.Location = new System.Drawing.Point(748, 300);
            this.textBox426.Name = "textBox426";
            this.textBox426.Size = new System.Drawing.Size(50, 25);
            this.textBox426.TabIndex = 472;
            this.textBox426.Text = "80";
            this.textBox426.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox425
            // 
            this.textBox425.BackColor = System.Drawing.SystemColors.Window;
            this.textBox425.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox425.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox425.Location = new System.Drawing.Point(748, 372);
            this.textBox425.Name = "textBox425";
            this.textBox425.Size = new System.Drawing.Size(50, 25);
            this.textBox425.TabIndex = 475;
            this.textBox425.Text = "100";
            this.textBox425.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox424
            // 
            this.textBox424.BackColor = System.Drawing.SystemColors.Window;
            this.textBox424.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox424.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox424.Location = new System.Drawing.Point(748, 420);
            this.textBox424.Name = "textBox424";
            this.textBox424.Size = new System.Drawing.Size(50, 25);
            this.textBox424.TabIndex = 477;
            this.textBox424.Text = "80";
            this.textBox424.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox423
            // 
            this.textBox423.BackColor = System.Drawing.SystemColors.Window;
            this.textBox423.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox423.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox423.Location = new System.Drawing.Point(748, 444);
            this.textBox423.Name = "textBox423";
            this.textBox423.Size = new System.Drawing.Size(50, 25);
            this.textBox423.TabIndex = 478;
            this.textBox423.Text = "100";
            this.textBox423.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox422
            // 
            this.textBox422.BackColor = System.Drawing.SystemColors.Window;
            this.textBox422.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox422.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox422.Location = new System.Drawing.Point(748, 396);
            this.textBox422.Name = "textBox422";
            this.textBox422.Size = new System.Drawing.Size(50, 25);
            this.textBox422.TabIndex = 476;
            this.textBox422.Text = "90";
            this.textBox422.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox421
            // 
            this.textBox421.BackColor = System.Drawing.SystemColors.Window;
            this.textBox421.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox421.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox421.Location = new System.Drawing.Point(748, 492);
            this.textBox421.Name = "textBox421";
            this.textBox421.Size = new System.Drawing.Size(50, 25);
            this.textBox421.TabIndex = 480;
            this.textBox421.Text = "80";
            this.textBox421.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox420
            // 
            this.textBox420.BackColor = System.Drawing.SystemColors.Window;
            this.textBox420.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox420.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox420.Location = new System.Drawing.Point(748, 516);
            this.textBox420.Name = "textBox420";
            this.textBox420.Size = new System.Drawing.Size(50, 25);
            this.textBox420.TabIndex = 481;
            this.textBox420.Text = "100";
            this.textBox420.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox419
            // 
            this.textBox419.BackColor = System.Drawing.SystemColors.Window;
            this.textBox419.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox419.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox419.Location = new System.Drawing.Point(748, 468);
            this.textBox419.Name = "textBox419";
            this.textBox419.Size = new System.Drawing.Size(50, 25);
            this.textBox419.TabIndex = 479;
            this.textBox419.Text = "80";
            this.textBox419.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // zdjgf_panel
            // 
            this.zdjgf_panel.Controls.Add(this.textBox232);
            this.zdjgf_panel.Controls.Add(this.textBox234);
            this.zdjgf_panel.Controls.Add(this.textBox235);
            this.zdjgf_panel.Controls.Add(this.textBox237);
            this.zdjgf_panel.Controls.Add(this.textBox238);
            this.zdjgf_panel.Controls.Add(this.textBox240);
            this.zdjgf_panel.Controls.Add(this.textBox241);
            this.zdjgf_panel.Controls.Add(this.textBox243);
            this.zdjgf_panel.Controls.Add(this.textBox244);
            this.zdjgf_panel.Controls.Add(this.textBox245);
            this.zdjgf_panel.Controls.Add(this.textBox246);
            this.zdjgf_panel.Controls.Add(this.textBox247);
            this.zdjgf_panel.Controls.Add(this.textBox248);
            this.zdjgf_panel.Controls.Add(this.textBox249);
            this.zdjgf_panel.Controls.Add(this.textBox250);
            this.zdjgf_panel.Controls.Add(this.textBox251);
            this.zdjgf_panel.Controls.Add(this.textBox252);
            this.zdjgf_panel.Controls.Add(this.textBox253);
            this.zdjgf_panel.Controls.Add(this.textBox254);
            this.zdjgf_panel.Controls.Add(this.textBox256);
            this.zdjgf_panel.Controls.Add(this.textBox257);
            this.zdjgf_panel.Controls.Add(this.textBox259);
            this.zdjgf_panel.Controls.Add(this.textBox261);
            this.zdjgf_panel.Controls.Add(this.textBox262);
            this.zdjgf_panel.Controls.Add(this.textBox263);
            this.zdjgf_panel.Controls.Add(this.textBox264);
            this.zdjgf_panel.Controls.Add(this.textBox265);
            this.zdjgf_panel.Controls.Add(this.textBox266);
            this.zdjgf_panel.Controls.Add(this.textBox267);
            this.zdjgf_panel.Controls.Add(this.textBox268);
            this.zdjgf_panel.Controls.Add(this.textBox269);
            this.zdjgf_panel.Controls.Add(this.textBox270);
            this.zdjgf_panel.Controls.Add(this.textBox271);
            this.zdjgf_panel.Controls.Add(this.textBox272);
            this.zdjgf_panel.Controls.Add(this.textBox273);
            this.zdjgf_panel.Controls.Add(this.textBox274);
            this.zdjgf_panel.Controls.Add(this.textBox275);
            this.zdjgf_panel.Controls.Add(this.textBox276);
            this.zdjgf_panel.Controls.Add(this.textBox277);
            this.zdjgf_panel.Controls.Add(this.textBox278);
            this.zdjgf_panel.Controls.Add(this.textBox199);
            this.zdjgf_panel.Controls.Add(this.textBox203);
            this.zdjgf_panel.Controls.Add(this.textBox205);
            this.zdjgf_panel.Controls.Add(this.textBox207);
            this.zdjgf_panel.Controls.Add(this.textBox208);
            this.zdjgf_panel.Controls.Add(this.textBox209);
            this.zdjgf_panel.Controls.Add(this.textBox210);
            this.zdjgf_panel.Controls.Add(this.textBox211);
            this.zdjgf_panel.Controls.Add(this.textBox212);
            this.zdjgf_panel.Controls.Add(this.textBox213);
            this.zdjgf_panel.Controls.Add(this.textBox214);
            this.zdjgf_panel.Controls.Add(this.textBox215);
            this.zdjgf_panel.Controls.Add(this.textBox216);
            this.zdjgf_panel.Controls.Add(this.textBox217);
            this.zdjgf_panel.Controls.Add(this.textBox218);
            this.zdjgf_panel.Controls.Add(this.textBox219);
            this.zdjgf_panel.Controls.Add(this.textBox221);
            this.zdjgf_panel.Controls.Add(this.textBox223);
            this.zdjgf_panel.Controls.Add(this.textBox225);
            this.zdjgf_panel.Controls.Add(this.textBox227);
            this.zdjgf_panel.Controls.Add(this.textBox228);
            this.zdjgf_panel.Controls.Add(this.textBox229);
            this.zdjgf_panel.Controls.Add(this.textBox230);
            this.zdjgf_panel.Controls.Add(this.textBox231);
            this.zdjgf_panel.Controls.Add(this.textBox56);
            this.zdjgf_panel.Controls.Add(this.textBox57);
            this.zdjgf_panel.Controls.Add(this.textBox60);
            this.zdjgf_panel.Controls.Add(this.textBox185);
            this.zdjgf_panel.Controls.Add(this.textBox188);
            this.zdjgf_panel.Controls.Add(this.textBox189);
            this.zdjgf_panel.Controls.Add(this.textBox192);
            this.zdjgf_panel.Controls.Add(this.textBox194);
            this.zdjgf_panel.Controls.Add(this.textBox195);
            this.zdjgf_panel.Controls.Add(this.textBox196);
            this.zdjgf_panel.Controls.Add(this.textBox197);
            this.zdjgf_panel.Controls.Add(this.textBox198);
            this.zdjgf_panel.Controls.Add(this.textBox53);
            this.zdjgf_panel.Controls.Add(this.textBox54);
            this.zdjgf_panel.Controls.Add(this.textBox55);
            this.zdjgf_panel.Controls.Add(this.label23);
            this.zdjgf_panel.Controls.Add(this.label24);
            this.zdjgf_panel.Controls.Add(this.label25);
            this.zdjgf_panel.Controls.Add(this.label33);
            this.zdjgf_panel.Controls.Add(this.textBox255);
            this.zdjgf_panel.Controls.Add(this.textBox258);
            this.zdjgf_panel.Controls.Add(this.textBox260);
            this.zdjgf_panel.Controls.Add(this.textBox200);
            this.zdjgf_panel.Controls.Add(this.textBox202);
            this.zdjgf_panel.Controls.Add(this.textBox204);
            this.zdjgf_panel.Controls.Add(this.textBox206);
            this.zdjgf_panel.Controls.Add(this.textBox220);
            this.zdjgf_panel.Controls.Add(this.textBox222);
            this.zdjgf_panel.Controls.Add(this.textBox224);
            this.zdjgf_panel.Controls.Add(this.textBox226);
            this.zdjgf_panel.Controls.Add(this.textBox7);
            this.zdjgf_panel.Controls.Add(this.textBox8);
            this.zdjgf_panel.Controls.Add(this.textBox9);
            this.zdjgf_panel.Controls.Add(this.textBox186);
            this.zdjgf_panel.Controls.Add(this.textBox11);
            this.zdjgf_panel.Controls.Add(this.textBox47);
            this.zdjgf_panel.Controls.Add(this.textBox48);
            this.zdjgf_panel.Controls.Add(this.textBox52);
            this.zdjgf_panel.Controls.Add(this.label88);
            this.zdjgf_panel.Controls.Add(this.label53);
            this.zdjgf_panel.Controls.Add(this.label86);
            this.zdjgf_panel.Controls.Add(this.textBox58);
            this.zdjgf_panel.Controls.Add(this.textBox59);
            this.zdjgf_panel.Controls.Add(this.textBox159);
            this.zdjgf_panel.Controls.Add(this.textBox12);
            this.zdjgf_panel.Controls.Add(this.textBox45);
            this.zdjgf_panel.Controls.Add(this.textBox46);
            this.zdjgf_panel.Controls.Add(this.textBox49);
            this.zdjgf_panel.Controls.Add(this.textBox50);
            this.zdjgf_panel.Controls.Add(this.textBox51);
            this.zdjgf_panel.Controls.Add(this.label30);
            this.zdjgf_panel.Controls.Add(this.label51);
            this.zdjgf_panel.Location = new System.Drawing.Point(6, 598);
            this.zdjgf_panel.Name = "zdjgf_panel";
            this.zdjgf_panel.Size = new System.Drawing.Size(712, 587);
            this.zdjgf_panel.TabIndex = 219;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.Window;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label51.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label51.Location = new System.Drawing.Point(15, 12);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(250, 25);
            this.label51.TabIndex = 275;
            this.label51.Text = "供应商";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.SystemColors.Window;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(15, 36);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(250, 25);
            this.label30.TabIndex = 279;
            this.label30.Text = "是否已确定供应商资格";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.SystemColors.Window;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox51.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox51.Location = new System.Drawing.Point(264, 60);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(100, 25);
            this.textBox51.TabIndex = 281;
            this.textBox51.Text = "是";
            this.textBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.Window;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox50.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox50.Location = new System.Drawing.Point(264, 84);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(100, 25);
            this.textBox50.TabIndex = 282;
            this.textBox50.Text = "是";
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Window;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox49.Location = new System.Drawing.Point(264, 36);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(100, 25);
            this.textBox49.TabIndex = 280;
            this.textBox49.Text = "是";
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Window;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox46.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox46.Location = new System.Drawing.Point(363, 60);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(100, 25);
            this.textBox46.TabIndex = 285;
            this.textBox46.Text = "是";
            this.textBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Window;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox45.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox45.Location = new System.Drawing.Point(363, 84);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(100, 25);
            this.textBox45.TabIndex = 286;
            this.textBox45.Text = "是";
            this.textBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Window;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox12.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox12.Location = new System.Drawing.Point(363, 36);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 25);
            this.textBox12.TabIndex = 284;
            this.textBox12.Text = "是";
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox159
            // 
            this.textBox159.BackColor = System.Drawing.SystemColors.Window;
            this.textBox159.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox159.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox159.Location = new System.Drawing.Point(462, 60);
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new System.Drawing.Size(100, 25);
            this.textBox159.TabIndex = 289;
            this.textBox159.Text = "是";
            this.textBox159.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox59.Location = new System.Drawing.Point(462, 84);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(100, 25);
            this.textBox59.TabIndex = 290;
            this.textBox59.Text = "是";
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.Window;
            this.textBox58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox58.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox58.ForeColor = System.Drawing.Color.Red;
            this.textBox58.Location = new System.Drawing.Point(462, 36);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(100, 25);
            this.textBox58.TabIndex = 288;
            this.textBox58.Text = "否";
            this.textBox58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label86
            // 
            this.label86.BackColor = System.Drawing.SystemColors.Window;
            this.label86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label86.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label86.Location = new System.Drawing.Point(15, 516);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(250, 25);
            this.label86.TabIndex = 295;
            this.label86.Text = "价格";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.Window;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.Location = new System.Drawing.Point(15, 540);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(250, 25);
            this.label53.TabIndex = 299;
            this.label53.Text = "选择";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label88
            // 
            this.label88.BackColor = System.Drawing.SystemColors.Window;
            this.label88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label88.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label88.Location = new System.Drawing.Point(15, 60);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(30, 457);
            this.label88.TabIndex = 300;
            this.label88.Text = "满足最低要求";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.SystemColors.Window;
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox52.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox52.Location = new System.Drawing.Point(44, 84);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(221, 25);
            this.textBox52.TabIndex = 302;
            this.textBox52.Text = "历史价格";
            this.textBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox48.Location = new System.Drawing.Point(44, 108);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(221, 25);
            this.textBox48.TabIndex = 303;
            this.textBox48.Text = "无故障时间";
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox47.Location = new System.Drawing.Point(44, 60);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(221, 25);
            this.textBox47.TabIndex = 301;
            this.textBox47.Text = "当前价格";
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Window;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox11.Location = new System.Drawing.Point(44, 132);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(221, 25);
            this.textBox11.TabIndex = 304;
            this.textBox11.Text = "低停工检修率";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox186
            // 
            this.textBox186.BackColor = System.Drawing.SystemColors.Window;
            this.textBox186.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox186.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox186.Location = new System.Drawing.Point(44, 180);
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new System.Drawing.Size(221, 25);
            this.textBox186.TabIndex = 306;
            this.textBox186.Text = "批次合格率";
            this.textBox186.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Window;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox9.Location = new System.Drawing.Point(44, 204);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(221, 25);
            this.textBox9.TabIndex = 307;
            this.textBox9.Text = "交货日期";
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Window;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox8.Location = new System.Drawing.Point(44, 156);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(221, 25);
            this.textBox8.TabIndex = 305;
            this.textBox8.Text = "设备耐久性";
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Window;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox7.Location = new System.Drawing.Point(44, 228);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(221, 25);
            this.textBox7.TabIndex = 308;
            this.textBox7.Text = "数量可靠性";
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox226
            // 
            this.textBox226.BackColor = System.Drawing.SystemColors.Window;
            this.textBox226.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox226.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox226.Location = new System.Drawing.Point(44, 276);
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new System.Drawing.Size(221, 25);
            this.textBox226.TabIndex = 310;
            this.textBox226.Text = "是否安装、调试等服务";
            this.textBox226.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox224
            // 
            this.textBox224.BackColor = System.Drawing.SystemColors.Window;
            this.textBox224.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox224.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox224.Location = new System.Drawing.Point(44, 300);
            this.textBox224.Name = "textBox224";
            this.textBox224.Size = new System.Drawing.Size(221, 25);
            this.textBox224.TabIndex = 311;
            this.textBox224.Text = "是否进行人员培训";
            this.textBox224.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox222
            // 
            this.textBox222.BackColor = System.Drawing.SystemColors.Window;
            this.textBox222.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox222.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox222.Location = new System.Drawing.Point(44, 252);
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new System.Drawing.Size(221, 25);
            this.textBox222.TabIndex = 309;
            this.textBox222.Text = "装运情况";
            this.textBox222.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox220
            // 
            this.textBox220.BackColor = System.Drawing.SystemColors.Window;
            this.textBox220.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox220.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox220.Location = new System.Drawing.Point(44, 324);
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new System.Drawing.Size(221, 25);
            this.textBox220.TabIndex = 312;
            this.textBox220.Text = "保养与修理的反应时间";
            this.textBox220.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox206
            // 
            this.textBox206.BackColor = System.Drawing.SystemColors.Window;
            this.textBox206.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox206.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox206.Location = new System.Drawing.Point(44, 372);
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new System.Drawing.Size(221, 25);
            this.textBox206.TabIndex = 314;
            this.textBox206.Text = "机械设备";
            this.textBox206.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox204
            // 
            this.textBox204.BackColor = System.Drawing.SystemColors.Window;
            this.textBox204.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox204.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox204.Location = new System.Drawing.Point(44, 396);
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new System.Drawing.Size(221, 25);
            this.textBox204.TabIndex = 315;
            this.textBox204.Text = "检验设备";
            this.textBox204.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox202
            // 
            this.textBox202.BackColor = System.Drawing.SystemColors.Window;
            this.textBox202.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox202.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox202.Location = new System.Drawing.Point(44, 348);
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new System.Drawing.Size(221, 25);
            this.textBox202.TabIndex = 313;
            this.textBox202.Text = "外包率";
            this.textBox202.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox200
            // 
            this.textBox200.BackColor = System.Drawing.SystemColors.Window;
            this.textBox200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox200.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox200.Location = new System.Drawing.Point(44, 420);
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new System.Drawing.Size(221, 25);
            this.textBox200.TabIndex = 316;
            this.textBox200.Text = "工作技术";
            this.textBox200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox260
            // 
            this.textBox260.BackColor = System.Drawing.SystemColors.Window;
            this.textBox260.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox260.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox260.Location = new System.Drawing.Point(44, 444);
            this.textBox260.Name = "textBox260";
            this.textBox260.Size = new System.Drawing.Size(221, 25);
            this.textBox260.TabIndex = 317;
            this.textBox260.Text = "营业状况";
            this.textBox260.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox258
            // 
            this.textBox258.BackColor = System.Drawing.SystemColors.Window;
            this.textBox258.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox258.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox258.Location = new System.Drawing.Point(44, 468);
            this.textBox258.Name = "textBox258";
            this.textBox258.Size = new System.Drawing.Size(221, 25);
            this.textBox258.TabIndex = 318;
            this.textBox258.Text = "财务结构";
            this.textBox258.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox255
            // 
            this.textBox255.BackColor = System.Drawing.SystemColors.Window;
            this.textBox255.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox255.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox255.Location = new System.Drawing.Point(44, 492);
            this.textBox255.Name = "textBox255";
            this.textBox255.Size = new System.Drawing.Size(221, 25);
            this.textBox255.TabIndex = 319;
            this.textBox255.Text = "反应措施";
            this.textBox255.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.SystemColors.Window;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label33.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label33.Location = new System.Drawing.Point(561, 12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 25);
            this.label33.TabIndex = 321;
            this.label33.Text = "SP002039";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.SystemColors.Window;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.Location = new System.Drawing.Point(363, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 25);
            this.label25.TabIndex = 320;
            this.label25.Text = "SP002025";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.SystemColors.Window;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(264, 12);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 25);
            this.label24.TabIndex = 322;
            this.label24.Text = "SP002013";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.SystemColors.Window;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(462, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 25);
            this.label23.TabIndex = 323;
            this.label23.Text = "SP002034";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Window;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox55.Location = new System.Drawing.Point(561, 60);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(100, 25);
            this.textBox55.TabIndex = 325;
            this.textBox55.Text = "是";
            this.textBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Window;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox54.Location = new System.Drawing.Point(561, 84);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(100, 25);
            this.textBox54.TabIndex = 326;
            this.textBox54.Text = "是";
            this.textBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Window;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox53.Location = new System.Drawing.Point(561, 36);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(100, 25);
            this.textBox53.TabIndex = 324;
            this.textBox53.Text = "是";
            this.textBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox198
            // 
            this.textBox198.BackColor = System.Drawing.SystemColors.Window;
            this.textBox198.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox198.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox198.Location = new System.Drawing.Point(264, 132);
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new System.Drawing.Size(100, 25);
            this.textBox198.TabIndex = 328;
            this.textBox198.Text = "是";
            this.textBox198.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox197
            // 
            this.textBox197.BackColor = System.Drawing.SystemColors.Window;
            this.textBox197.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox197.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox197.Location = new System.Drawing.Point(264, 156);
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new System.Drawing.Size(100, 25);
            this.textBox197.TabIndex = 329;
            this.textBox197.Text = "是";
            this.textBox197.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox196
            // 
            this.textBox196.BackColor = System.Drawing.SystemColors.Window;
            this.textBox196.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox196.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox196.Location = new System.Drawing.Point(264, 108);
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new System.Drawing.Size(100, 25);
            this.textBox196.TabIndex = 327;
            this.textBox196.Text = "是";
            this.textBox196.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox195
            // 
            this.textBox195.BackColor = System.Drawing.SystemColors.Window;
            this.textBox195.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox195.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox195.Location = new System.Drawing.Point(363, 132);
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new System.Drawing.Size(100, 25);
            this.textBox195.TabIndex = 331;
            this.textBox195.Text = "是";
            this.textBox195.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox194
            // 
            this.textBox194.BackColor = System.Drawing.SystemColors.Window;
            this.textBox194.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox194.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox194.Location = new System.Drawing.Point(363, 156);
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new System.Drawing.Size(100, 25);
            this.textBox194.TabIndex = 332;
            this.textBox194.Text = "是";
            this.textBox194.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox192
            // 
            this.textBox192.BackColor = System.Drawing.SystemColors.Window;
            this.textBox192.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox192.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox192.Location = new System.Drawing.Point(363, 108);
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new System.Drawing.Size(100, 25);
            this.textBox192.TabIndex = 330;
            this.textBox192.Text = "是";
            this.textBox192.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox189
            // 
            this.textBox189.BackColor = System.Drawing.SystemColors.Window;
            this.textBox189.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox189.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox189.ForeColor = System.Drawing.Color.Red;
            this.textBox189.Location = new System.Drawing.Point(462, 132);
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new System.Drawing.Size(100, 25);
            this.textBox189.TabIndex = 334;
            this.textBox189.Text = "否";
            this.textBox189.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox188
            // 
            this.textBox188.BackColor = System.Drawing.SystemColors.Window;
            this.textBox188.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox188.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox188.Location = new System.Drawing.Point(462, 156);
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new System.Drawing.Size(100, 25);
            this.textBox188.TabIndex = 335;
            this.textBox188.Text = "是";
            this.textBox188.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox185
            // 
            this.textBox185.BackColor = System.Drawing.SystemColors.Window;
            this.textBox185.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox185.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox185.Location = new System.Drawing.Point(462, 108);
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new System.Drawing.Size(100, 25);
            this.textBox185.TabIndex = 333;
            this.textBox185.Text = "是";
            this.textBox185.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Window;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox60.Location = new System.Drawing.Point(561, 132);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(100, 25);
            this.textBox60.TabIndex = 337;
            this.textBox60.Text = "是";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Window;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox57.ForeColor = System.Drawing.Color.Red;
            this.textBox57.Location = new System.Drawing.Point(561, 156);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(100, 25);
            this.textBox57.TabIndex = 338;
            this.textBox57.Text = "否";
            this.textBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Window;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox56.Location = new System.Drawing.Point(561, 108);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(100, 25);
            this.textBox56.TabIndex = 336;
            this.textBox56.Text = "是";
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox231
            // 
            this.textBox231.BackColor = System.Drawing.SystemColors.Window;
            this.textBox231.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox231.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox231.Location = new System.Drawing.Point(264, 204);
            this.textBox231.Name = "textBox231";
            this.textBox231.Size = new System.Drawing.Size(100, 25);
            this.textBox231.TabIndex = 340;
            this.textBox231.Text = "是";
            this.textBox231.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox230
            // 
            this.textBox230.BackColor = System.Drawing.SystemColors.Window;
            this.textBox230.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox230.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox230.Location = new System.Drawing.Point(264, 228);
            this.textBox230.Name = "textBox230";
            this.textBox230.Size = new System.Drawing.Size(100, 25);
            this.textBox230.TabIndex = 341;
            this.textBox230.Text = "是";
            this.textBox230.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox229
            // 
            this.textBox229.BackColor = System.Drawing.SystemColors.Window;
            this.textBox229.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox229.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox229.Location = new System.Drawing.Point(264, 180);
            this.textBox229.Name = "textBox229";
            this.textBox229.Size = new System.Drawing.Size(100, 25);
            this.textBox229.TabIndex = 339;
            this.textBox229.Text = "是";
            this.textBox229.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox228
            // 
            this.textBox228.BackColor = System.Drawing.SystemColors.Window;
            this.textBox228.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox228.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox228.ForeColor = System.Drawing.Color.Red;
            this.textBox228.Location = new System.Drawing.Point(363, 204);
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new System.Drawing.Size(100, 25);
            this.textBox228.TabIndex = 343;
            this.textBox228.Text = "否";
            this.textBox228.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox227
            // 
            this.textBox227.BackColor = System.Drawing.SystemColors.Window;
            this.textBox227.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox227.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox227.Location = new System.Drawing.Point(363, 228);
            this.textBox227.Name = "textBox227";
            this.textBox227.Size = new System.Drawing.Size(100, 25);
            this.textBox227.TabIndex = 344;
            this.textBox227.Text = "是";
            this.textBox227.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox225
            // 
            this.textBox225.BackColor = System.Drawing.SystemColors.Window;
            this.textBox225.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox225.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox225.Location = new System.Drawing.Point(363, 180);
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new System.Drawing.Size(100, 25);
            this.textBox225.TabIndex = 342;
            this.textBox225.Text = "是";
            this.textBox225.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox223
            // 
            this.textBox223.BackColor = System.Drawing.SystemColors.Window;
            this.textBox223.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox223.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox223.Location = new System.Drawing.Point(462, 204);
            this.textBox223.Name = "textBox223";
            this.textBox223.Size = new System.Drawing.Size(100, 25);
            this.textBox223.TabIndex = 346;
            this.textBox223.Text = "是";
            this.textBox223.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox221
            // 
            this.textBox221.BackColor = System.Drawing.SystemColors.Window;
            this.textBox221.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox221.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox221.Location = new System.Drawing.Point(462, 228);
            this.textBox221.Name = "textBox221";
            this.textBox221.Size = new System.Drawing.Size(100, 25);
            this.textBox221.TabIndex = 347;
            this.textBox221.Text = "是";
            this.textBox221.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox219
            // 
            this.textBox219.BackColor = System.Drawing.SystemColors.Window;
            this.textBox219.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox219.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox219.ForeColor = System.Drawing.Color.Red;
            this.textBox219.Location = new System.Drawing.Point(462, 180);
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new System.Drawing.Size(100, 25);
            this.textBox219.TabIndex = 345;
            this.textBox219.Text = "否";
            this.textBox219.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox218
            // 
            this.textBox218.BackColor = System.Drawing.SystemColors.Window;
            this.textBox218.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox218.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox218.Location = new System.Drawing.Point(561, 204);
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new System.Drawing.Size(100, 25);
            this.textBox218.TabIndex = 349;
            this.textBox218.Text = "是";
            this.textBox218.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox217
            // 
            this.textBox217.BackColor = System.Drawing.SystemColors.Window;
            this.textBox217.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox217.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox217.Location = new System.Drawing.Point(561, 228);
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new System.Drawing.Size(100, 25);
            this.textBox217.TabIndex = 350;
            this.textBox217.Text = "是";
            this.textBox217.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox216
            // 
            this.textBox216.BackColor = System.Drawing.SystemColors.Window;
            this.textBox216.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox216.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox216.Location = new System.Drawing.Point(561, 180);
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new System.Drawing.Size(100, 25);
            this.textBox216.TabIndex = 348;
            this.textBox216.Text = "是";
            this.textBox216.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox215
            // 
            this.textBox215.BackColor = System.Drawing.SystemColors.Window;
            this.textBox215.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox215.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox215.Location = new System.Drawing.Point(264, 276);
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new System.Drawing.Size(100, 25);
            this.textBox215.TabIndex = 352;
            this.textBox215.Text = "是";
            this.textBox215.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox214
            // 
            this.textBox214.BackColor = System.Drawing.SystemColors.Window;
            this.textBox214.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox214.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox214.Location = new System.Drawing.Point(264, 300);
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new System.Drawing.Size(100, 25);
            this.textBox214.TabIndex = 353;
            this.textBox214.Text = "是";
            this.textBox214.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox213
            // 
            this.textBox213.BackColor = System.Drawing.SystemColors.Window;
            this.textBox213.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox213.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox213.Location = new System.Drawing.Point(264, 252);
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new System.Drawing.Size(100, 25);
            this.textBox213.TabIndex = 351;
            this.textBox213.Text = "是";
            this.textBox213.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox212
            // 
            this.textBox212.BackColor = System.Drawing.SystemColors.Window;
            this.textBox212.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox212.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox212.Location = new System.Drawing.Point(363, 276);
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new System.Drawing.Size(100, 25);
            this.textBox212.TabIndex = 355;
            this.textBox212.Text = "是";
            this.textBox212.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox211
            // 
            this.textBox211.BackColor = System.Drawing.SystemColors.Window;
            this.textBox211.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox211.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox211.Location = new System.Drawing.Point(363, 300);
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new System.Drawing.Size(100, 25);
            this.textBox211.TabIndex = 356;
            this.textBox211.Text = "是";
            this.textBox211.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox210
            // 
            this.textBox210.BackColor = System.Drawing.SystemColors.Window;
            this.textBox210.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox210.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox210.Location = new System.Drawing.Point(363, 252);
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new System.Drawing.Size(100, 25);
            this.textBox210.TabIndex = 354;
            this.textBox210.Text = "是";
            this.textBox210.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox209
            // 
            this.textBox209.BackColor = System.Drawing.SystemColors.Window;
            this.textBox209.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox209.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox209.Location = new System.Drawing.Point(462, 276);
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new System.Drawing.Size(100, 25);
            this.textBox209.TabIndex = 358;
            this.textBox209.Text = "是";
            this.textBox209.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox208
            // 
            this.textBox208.BackColor = System.Drawing.SystemColors.Window;
            this.textBox208.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox208.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox208.Location = new System.Drawing.Point(462, 300);
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new System.Drawing.Size(100, 25);
            this.textBox208.TabIndex = 359;
            this.textBox208.Text = "是";
            this.textBox208.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox207
            // 
            this.textBox207.BackColor = System.Drawing.SystemColors.Window;
            this.textBox207.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox207.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox207.Location = new System.Drawing.Point(462, 252);
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new System.Drawing.Size(100, 25);
            this.textBox207.TabIndex = 357;
            this.textBox207.Text = "是";
            this.textBox207.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox205
            // 
            this.textBox205.BackColor = System.Drawing.SystemColors.Window;
            this.textBox205.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox205.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox205.Location = new System.Drawing.Point(561, 276);
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new System.Drawing.Size(100, 25);
            this.textBox205.TabIndex = 361;
            this.textBox205.Text = "是";
            this.textBox205.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox203
            // 
            this.textBox203.BackColor = System.Drawing.SystemColors.Window;
            this.textBox203.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox203.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox203.Location = new System.Drawing.Point(561, 300);
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new System.Drawing.Size(100, 25);
            this.textBox203.TabIndex = 362;
            this.textBox203.Text = "是";
            this.textBox203.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox199
            // 
            this.textBox199.BackColor = System.Drawing.SystemColors.Window;
            this.textBox199.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox199.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox199.ForeColor = System.Drawing.Color.Red;
            this.textBox199.Location = new System.Drawing.Point(561, 252);
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new System.Drawing.Size(100, 25);
            this.textBox199.TabIndex = 360;
            this.textBox199.Text = "否";
            this.textBox199.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox278
            // 
            this.textBox278.BackColor = System.Drawing.SystemColors.Window;
            this.textBox278.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox278.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox278.Location = new System.Drawing.Point(264, 324);
            this.textBox278.Name = "textBox278";
            this.textBox278.Size = new System.Drawing.Size(100, 25);
            this.textBox278.TabIndex = 363;
            this.textBox278.Text = "是";
            this.textBox278.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox277
            // 
            this.textBox277.BackColor = System.Drawing.SystemColors.Window;
            this.textBox277.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox277.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox277.Location = new System.Drawing.Point(264, 348);
            this.textBox277.Name = "textBox277";
            this.textBox277.Size = new System.Drawing.Size(100, 25);
            this.textBox277.TabIndex = 364;
            this.textBox277.Text = "是";
            this.textBox277.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox276
            // 
            this.textBox276.BackColor = System.Drawing.SystemColors.Window;
            this.textBox276.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox276.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox276.Location = new System.Drawing.Point(363, 324);
            this.textBox276.Name = "textBox276";
            this.textBox276.Size = new System.Drawing.Size(100, 25);
            this.textBox276.TabIndex = 365;
            this.textBox276.Text = "是";
            this.textBox276.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox275
            // 
            this.textBox275.BackColor = System.Drawing.SystemColors.Window;
            this.textBox275.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox275.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox275.ForeColor = System.Drawing.Color.Red;
            this.textBox275.Location = new System.Drawing.Point(363, 348);
            this.textBox275.Name = "textBox275";
            this.textBox275.Size = new System.Drawing.Size(100, 25);
            this.textBox275.TabIndex = 366;
            this.textBox275.Text = "否";
            this.textBox275.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox274
            // 
            this.textBox274.BackColor = System.Drawing.SystemColors.Window;
            this.textBox274.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox274.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox274.Location = new System.Drawing.Point(462, 324);
            this.textBox274.Name = "textBox274";
            this.textBox274.Size = new System.Drawing.Size(100, 25);
            this.textBox274.TabIndex = 367;
            this.textBox274.Text = "是";
            this.textBox274.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox273
            // 
            this.textBox273.BackColor = System.Drawing.SystemColors.Window;
            this.textBox273.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox273.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox273.Location = new System.Drawing.Point(462, 348);
            this.textBox273.Name = "textBox273";
            this.textBox273.Size = new System.Drawing.Size(100, 25);
            this.textBox273.TabIndex = 368;
            this.textBox273.Text = "是";
            this.textBox273.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox272
            // 
            this.textBox272.BackColor = System.Drawing.SystemColors.Window;
            this.textBox272.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox272.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox272.Location = new System.Drawing.Point(561, 324);
            this.textBox272.Name = "textBox272";
            this.textBox272.Size = new System.Drawing.Size(100, 25);
            this.textBox272.TabIndex = 369;
            this.textBox272.Text = "是";
            this.textBox272.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox271
            // 
            this.textBox271.BackColor = System.Drawing.SystemColors.Window;
            this.textBox271.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox271.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox271.Location = new System.Drawing.Point(561, 348);
            this.textBox271.Name = "textBox271";
            this.textBox271.Size = new System.Drawing.Size(100, 25);
            this.textBox271.TabIndex = 370;
            this.textBox271.Text = "是";
            this.textBox271.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox270
            // 
            this.textBox270.BackColor = System.Drawing.SystemColors.Window;
            this.textBox270.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox270.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox270.Location = new System.Drawing.Point(264, 396);
            this.textBox270.Name = "textBox270";
            this.textBox270.Size = new System.Drawing.Size(100, 25);
            this.textBox270.TabIndex = 372;
            this.textBox270.Text = "是";
            this.textBox270.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox269
            // 
            this.textBox269.BackColor = System.Drawing.SystemColors.Window;
            this.textBox269.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox269.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox269.Location = new System.Drawing.Point(264, 420);
            this.textBox269.Name = "textBox269";
            this.textBox269.Size = new System.Drawing.Size(100, 25);
            this.textBox269.TabIndex = 373;
            this.textBox269.Text = "是";
            this.textBox269.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox268
            // 
            this.textBox268.BackColor = System.Drawing.SystemColors.Window;
            this.textBox268.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox268.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox268.Location = new System.Drawing.Point(264, 372);
            this.textBox268.Name = "textBox268";
            this.textBox268.Size = new System.Drawing.Size(100, 25);
            this.textBox268.TabIndex = 371;
            this.textBox268.Text = "是";
            this.textBox268.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox267
            // 
            this.textBox267.BackColor = System.Drawing.SystemColors.Window;
            this.textBox267.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox267.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox267.Location = new System.Drawing.Point(363, 396);
            this.textBox267.Name = "textBox267";
            this.textBox267.Size = new System.Drawing.Size(100, 25);
            this.textBox267.TabIndex = 375;
            this.textBox267.Text = "是";
            this.textBox267.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox266
            // 
            this.textBox266.BackColor = System.Drawing.SystemColors.Window;
            this.textBox266.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox266.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox266.Location = new System.Drawing.Point(363, 420);
            this.textBox266.Name = "textBox266";
            this.textBox266.Size = new System.Drawing.Size(100, 25);
            this.textBox266.TabIndex = 376;
            this.textBox266.Text = "是";
            this.textBox266.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox265
            // 
            this.textBox265.BackColor = System.Drawing.SystemColors.Window;
            this.textBox265.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox265.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox265.Location = new System.Drawing.Point(363, 372);
            this.textBox265.Name = "textBox265";
            this.textBox265.Size = new System.Drawing.Size(100, 25);
            this.textBox265.TabIndex = 374;
            this.textBox265.Text = "是";
            this.textBox265.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox264
            // 
            this.textBox264.BackColor = System.Drawing.SystemColors.Window;
            this.textBox264.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox264.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox264.Location = new System.Drawing.Point(462, 396);
            this.textBox264.Name = "textBox264";
            this.textBox264.Size = new System.Drawing.Size(100, 25);
            this.textBox264.TabIndex = 378;
            this.textBox264.Text = "是";
            this.textBox264.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox263
            // 
            this.textBox263.BackColor = System.Drawing.SystemColors.Window;
            this.textBox263.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox263.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox263.Location = new System.Drawing.Point(462, 420);
            this.textBox263.Name = "textBox263";
            this.textBox263.Size = new System.Drawing.Size(100, 25);
            this.textBox263.TabIndex = 379;
            this.textBox263.Text = "是";
            this.textBox263.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox262
            // 
            this.textBox262.BackColor = System.Drawing.SystemColors.Window;
            this.textBox262.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox262.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox262.Location = new System.Drawing.Point(462, 372);
            this.textBox262.Name = "textBox262";
            this.textBox262.Size = new System.Drawing.Size(100, 25);
            this.textBox262.TabIndex = 377;
            this.textBox262.Text = "是";
            this.textBox262.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox261
            // 
            this.textBox261.BackColor = System.Drawing.SystemColors.Window;
            this.textBox261.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox261.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox261.Location = new System.Drawing.Point(561, 396);
            this.textBox261.Name = "textBox261";
            this.textBox261.Size = new System.Drawing.Size(100, 25);
            this.textBox261.TabIndex = 381;
            this.textBox261.Text = "是";
            this.textBox261.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox259
            // 
            this.textBox259.BackColor = System.Drawing.SystemColors.Window;
            this.textBox259.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox259.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox259.Location = new System.Drawing.Point(561, 420);
            this.textBox259.Name = "textBox259";
            this.textBox259.Size = new System.Drawing.Size(100, 25);
            this.textBox259.TabIndex = 382;
            this.textBox259.Text = "是";
            this.textBox259.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox257
            // 
            this.textBox257.BackColor = System.Drawing.SystemColors.Window;
            this.textBox257.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox257.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox257.ForeColor = System.Drawing.Color.Red;
            this.textBox257.Location = new System.Drawing.Point(561, 372);
            this.textBox257.Name = "textBox257";
            this.textBox257.Size = new System.Drawing.Size(100, 25);
            this.textBox257.TabIndex = 380;
            this.textBox257.Text = "否";
            this.textBox257.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox256
            // 
            this.textBox256.BackColor = System.Drawing.SystemColors.Window;
            this.textBox256.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox256.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox256.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox256.Location = new System.Drawing.Point(264, 468);
            this.textBox256.Name = "textBox256";
            this.textBox256.Size = new System.Drawing.Size(100, 25);
            this.textBox256.TabIndex = 384;
            this.textBox256.Text = "是";
            this.textBox256.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox254
            // 
            this.textBox254.BackColor = System.Drawing.SystemColors.Window;
            this.textBox254.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox254.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox254.Location = new System.Drawing.Point(264, 492);
            this.textBox254.Name = "textBox254";
            this.textBox254.Size = new System.Drawing.Size(100, 25);
            this.textBox254.TabIndex = 385;
            this.textBox254.Text = "是";
            this.textBox254.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox253
            // 
            this.textBox253.BackColor = System.Drawing.SystemColors.Window;
            this.textBox253.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox253.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox253.Location = new System.Drawing.Point(264, 444);
            this.textBox253.Name = "textBox253";
            this.textBox253.Size = new System.Drawing.Size(100, 25);
            this.textBox253.TabIndex = 383;
            this.textBox253.Text = "是";
            this.textBox253.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox252
            // 
            this.textBox252.BackColor = System.Drawing.SystemColors.Window;
            this.textBox252.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox252.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox252.Location = new System.Drawing.Point(363, 468);
            this.textBox252.Name = "textBox252";
            this.textBox252.Size = new System.Drawing.Size(100, 25);
            this.textBox252.TabIndex = 387;
            this.textBox252.Text = "是";
            this.textBox252.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox251
            // 
            this.textBox251.BackColor = System.Drawing.SystemColors.Window;
            this.textBox251.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox251.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox251.ForeColor = System.Drawing.Color.Red;
            this.textBox251.Location = new System.Drawing.Point(363, 492);
            this.textBox251.Name = "textBox251";
            this.textBox251.Size = new System.Drawing.Size(100, 25);
            this.textBox251.TabIndex = 388;
            this.textBox251.Text = "否";
            this.textBox251.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox250
            // 
            this.textBox250.BackColor = System.Drawing.SystemColors.Window;
            this.textBox250.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox250.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox250.Location = new System.Drawing.Point(363, 444);
            this.textBox250.Name = "textBox250";
            this.textBox250.Size = new System.Drawing.Size(100, 25);
            this.textBox250.TabIndex = 386;
            this.textBox250.Text = "是";
            this.textBox250.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox249
            // 
            this.textBox249.BackColor = System.Drawing.SystemColors.Window;
            this.textBox249.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox249.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox249.Location = new System.Drawing.Point(462, 468);
            this.textBox249.Name = "textBox249";
            this.textBox249.Size = new System.Drawing.Size(100, 25);
            this.textBox249.TabIndex = 390;
            this.textBox249.Text = "是";
            this.textBox249.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox248
            // 
            this.textBox248.BackColor = System.Drawing.SystemColors.Window;
            this.textBox248.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox248.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox248.Location = new System.Drawing.Point(462, 492);
            this.textBox248.Name = "textBox248";
            this.textBox248.Size = new System.Drawing.Size(100, 25);
            this.textBox248.TabIndex = 391;
            this.textBox248.Text = "是";
            this.textBox248.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox247
            // 
            this.textBox247.BackColor = System.Drawing.SystemColors.Window;
            this.textBox247.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox247.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox247.ForeColor = System.Drawing.Color.Red;
            this.textBox247.Location = new System.Drawing.Point(462, 444);
            this.textBox247.Name = "textBox247";
            this.textBox247.Size = new System.Drawing.Size(100, 25);
            this.textBox247.TabIndex = 389;
            this.textBox247.Text = "否";
            this.textBox247.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox246
            // 
            this.textBox246.BackColor = System.Drawing.SystemColors.Window;
            this.textBox246.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox246.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox246.Location = new System.Drawing.Point(561, 468);
            this.textBox246.Name = "textBox246";
            this.textBox246.Size = new System.Drawing.Size(100, 25);
            this.textBox246.TabIndex = 393;
            this.textBox246.Text = "是";
            this.textBox246.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox245
            // 
            this.textBox245.BackColor = System.Drawing.SystemColors.Window;
            this.textBox245.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox245.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox245.Location = new System.Drawing.Point(561, 492);
            this.textBox245.Name = "textBox245";
            this.textBox245.Size = new System.Drawing.Size(100, 25);
            this.textBox245.TabIndex = 394;
            this.textBox245.Text = "是";
            this.textBox245.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox244
            // 
            this.textBox244.BackColor = System.Drawing.SystemColors.Window;
            this.textBox244.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox244.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox244.Location = new System.Drawing.Point(561, 444);
            this.textBox244.Name = "textBox244";
            this.textBox244.Size = new System.Drawing.Size(100, 25);
            this.textBox244.TabIndex = 392;
            this.textBox244.Text = "是";
            this.textBox244.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox243
            // 
            this.textBox243.BackColor = System.Drawing.SystemColors.Window;
            this.textBox243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox243.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox243.Location = new System.Drawing.Point(264, 540);
            this.textBox243.Name = "textBox243";
            this.textBox243.Size = new System.Drawing.Size(100, 25);
            this.textBox243.TabIndex = 396;
            this.textBox243.Text = "是";
            this.textBox243.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox241
            // 
            this.textBox241.BackColor = System.Drawing.SystemColors.Window;
            this.textBox241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox241.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox241.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox241.Location = new System.Drawing.Point(264, 516);
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new System.Drawing.Size(100, 25);
            this.textBox241.TabIndex = 395;
            this.textBox241.Text = "500000";
            this.textBox241.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox240
            // 
            this.textBox240.BackColor = System.Drawing.SystemColors.Window;
            this.textBox240.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox240.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox240.Location = new System.Drawing.Point(363, 540);
            this.textBox240.Name = "textBox240";
            this.textBox240.Size = new System.Drawing.Size(100, 25);
            this.textBox240.TabIndex = 399;
            this.textBox240.Text = "否";
            this.textBox240.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox238
            // 
            this.textBox238.BackColor = System.Drawing.SystemColors.Window;
            this.textBox238.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox238.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox238.Location = new System.Drawing.Point(363, 516);
            this.textBox238.Name = "textBox238";
            this.textBox238.Size = new System.Drawing.Size(100, 25);
            this.textBox238.TabIndex = 398;
            this.textBox238.Text = "520000";
            this.textBox238.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox237
            // 
            this.textBox237.BackColor = System.Drawing.SystemColors.Window;
            this.textBox237.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox237.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox237.Location = new System.Drawing.Point(462, 540);
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new System.Drawing.Size(100, 25);
            this.textBox237.TabIndex = 402;
            this.textBox237.Text = "否";
            this.textBox237.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox235
            // 
            this.textBox235.BackColor = System.Drawing.SystemColors.Window;
            this.textBox235.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox235.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox235.Location = new System.Drawing.Point(462, 516);
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new System.Drawing.Size(100, 25);
            this.textBox235.TabIndex = 401;
            this.textBox235.Text = "510000";
            this.textBox235.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox234
            // 
            this.textBox234.BackColor = System.Drawing.SystemColors.Window;
            this.textBox234.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox234.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox234.Location = new System.Drawing.Point(561, 540);
            this.textBox234.Name = "textBox234";
            this.textBox234.Size = new System.Drawing.Size(100, 25);
            this.textBox234.TabIndex = 405;
            this.textBox234.Text = "否";
            this.textBox234.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox232
            // 
            this.textBox232.BackColor = System.Drawing.SystemColors.Window;
            this.textBox232.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox232.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox232.Location = new System.Drawing.Point(561, 516);
            this.textBox232.Name = "textBox232";
            this.textBox232.Size = new System.Drawing.Size(100, 25);
            this.textBox232.TabIndex = 404;
            this.textBox232.Text = "505000";
            this.textBox232.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(359, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 222;
            this.label12.Text = "中标商";
            // 
            // zbs_tb
            // 
            this.zbs_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zbs_tb.Location = new System.Drawing.Point(406, 14);
            this.zbs_tb.Name = "zbs_tb";
            this.zbs_tb.Size = new System.Drawing.Size(200, 23);
            this.zbs_tb.TabIndex = 223;
            this.zbs_tb.Text = "SP002013";
            // 
            // zdsyqcbf_panel
            // 
            this.zdsyqcbf_panel.Controls.Add(this.textBox67);
            this.zdsyqcbf_panel.Controls.Add(this.textBox68);
            this.zdsyqcbf_panel.Controls.Add(this.textBox84);
            this.zdsyqcbf_panel.Controls.Add(this.textBox92);
            this.zdsyqcbf_panel.Controls.Add(this.textBox99);
            this.zdsyqcbf_panel.Controls.Add(this.textBox102);
            this.zdsyqcbf_panel.Controls.Add(this.textBox103);
            this.zdsyqcbf_panel.Controls.Add(this.textBox104);
            this.zdsyqcbf_panel.Controls.Add(this.textBox106);
            this.zdsyqcbf_panel.Controls.Add(this.textBox107);
            this.zdsyqcbf_panel.Controls.Add(this.textBox108);
            this.zdsyqcbf_panel.Controls.Add(this.textBox115);
            this.zdsyqcbf_panel.Controls.Add(this.textBox121);
            this.zdsyqcbf_panel.Controls.Add(this.textBox122);
            this.zdsyqcbf_panel.Controls.Add(this.textBox167);
            this.zdsyqcbf_panel.Controls.Add(this.textBox168);
            this.zdsyqcbf_panel.Controls.Add(this.textBox169);
            this.zdsyqcbf_panel.Controls.Add(this.textBox170);
            this.zdsyqcbf_panel.Controls.Add(this.textBox184);
            this.zdsyqcbf_panel.Controls.Add(this.textBox65);
            this.zdsyqcbf_panel.Controls.Add(this.textBox66);
            this.zdsyqcbf_panel.Controls.Add(this.textBox69);
            this.zdsyqcbf_panel.Controls.Add(this.textBox70);
            this.zdsyqcbf_panel.Controls.Add(this.textBox76);
            this.zdsyqcbf_panel.Controls.Add(this.textBox77);
            this.zdsyqcbf_panel.Controls.Add(this.textBox78);
            this.zdsyqcbf_panel.Controls.Add(this.textBox101);
            this.zdsyqcbf_panel.Controls.Add(this.textBox118);
            this.zdsyqcbf_panel.Controls.Add(this.textBox119);
            this.zdsyqcbf_panel.Controls.Add(this.textBox120);
            this.zdsyqcbf_panel.Controls.Add(this.textBox174);
            this.zdsyqcbf_panel.Controls.Add(this.textBox175);
            this.zdsyqcbf_panel.Controls.Add(this.textBox178);
            this.zdsyqcbf_panel.Controls.Add(this.textBox179);
            this.zdsyqcbf_panel.Controls.Add(this.textBox180);
            this.zdsyqcbf_panel.Controls.Add(this.textBox181);
            this.zdsyqcbf_panel.Controls.Add(this.textBox182);
            this.zdsyqcbf_panel.Controls.Add(this.textBox183);
            this.zdsyqcbf_panel.Controls.Add(this.textBox71);
            this.zdsyqcbf_panel.Controls.Add(this.textBox74);
            this.zdsyqcbf_panel.Controls.Add(this.textBox75);
            this.zdsyqcbf_panel.Controls.Add(this.textBox79);
            this.zdsyqcbf_panel.Controls.Add(this.textBox82);
            this.zdsyqcbf_panel.Controls.Add(this.textBox86);
            this.zdsyqcbf_panel.Controls.Add(this.textBox87);
            this.zdsyqcbf_panel.Controls.Add(this.textBox95);
            this.zdsyqcbf_panel.Controls.Add(this.textBox98);
            this.zdsyqcbf_panel.Controls.Add(this.textBox105);
            this.zdsyqcbf_panel.Controls.Add(this.textBox136);
            this.zdsyqcbf_panel.Controls.Add(this.textBox137);
            this.zdsyqcbf_panel.Controls.Add(this.textBox161);
            this.zdsyqcbf_panel.Controls.Add(this.textBox162);
            this.zdsyqcbf_panel.Controls.Add(this.textBox163);
            this.zdsyqcbf_panel.Controls.Add(this.textBox166);
            this.zdsyqcbf_panel.Controls.Add(this.textBox171);
            this.zdsyqcbf_panel.Controls.Add(this.textBox172);
            this.zdsyqcbf_panel.Controls.Add(this.textBox173);
            this.zdsyqcbf_panel.Controls.Add(this.textBox72);
            this.zdsyqcbf_panel.Controls.Add(this.textBox80);
            this.zdsyqcbf_panel.Controls.Add(this.textBox83);
            this.zdsyqcbf_panel.Controls.Add(this.textBox85);
            this.zdsyqcbf_panel.Controls.Add(this.textBox88);
            this.zdsyqcbf_panel.Controls.Add(this.textBox89);
            this.zdsyqcbf_panel.Controls.Add(this.textBox90);
            this.zdsyqcbf_panel.Controls.Add(this.textBox91);
            this.zdsyqcbf_panel.Controls.Add(this.textBox93);
            this.zdsyqcbf_panel.Controls.Add(this.textBox94);
            this.zdsyqcbf_panel.Controls.Add(this.textBox97);
            this.zdsyqcbf_panel.Controls.Add(this.textBox109);
            this.zdsyqcbf_panel.Controls.Add(this.textBox110);
            this.zdsyqcbf_panel.Controls.Add(this.textBox111);
            this.zdsyqcbf_panel.Controls.Add(this.textBox113);
            this.zdsyqcbf_panel.Controls.Add(this.textBox114);
            this.zdsyqcbf_panel.Controls.Add(this.textBox116);
            this.zdsyqcbf_panel.Controls.Add(this.textBox117);
            this.zdsyqcbf_panel.Controls.Add(this.textBox129);
            this.zdsyqcbf_panel.Controls.Add(this.textBox193);
            this.zdsyqcbf_panel.Controls.Add(this.label27);
            this.zdsyqcbf_panel.Controls.Add(this.textBox201);
            this.zdsyqcbf_panel.Controls.Add(this.label31);
            this.zdsyqcbf_panel.Controls.Add(this.textBox16);
            this.zdsyqcbf_panel.Controls.Add(this.textBox19);
            this.zdsyqcbf_panel.Controls.Add(this.textBox26);
            this.zdsyqcbf_panel.Controls.Add(this.textBox27);
            this.zdsyqcbf_panel.Controls.Add(this.textBox30);
            this.zdsyqcbf_panel.Controls.Add(this.textBox31);
            this.zdsyqcbf_panel.Controls.Add(this.textBox34);
            this.zdsyqcbf_panel.Controls.Add(this.textBox35);
            this.zdsyqcbf_panel.Controls.Add(this.textBox37);
            this.zdsyqcbf_panel.Controls.Add(this.textBox38);
            this.zdsyqcbf_panel.Controls.Add(this.textBox40);
            this.zdsyqcbf_panel.Controls.Add(this.textBox41);
            this.zdsyqcbf_panel.Controls.Add(this.textBox61);
            this.zdsyqcbf_panel.Controls.Add(this.textBox62);
            this.zdsyqcbf_panel.Controls.Add(this.textBox63);
            this.zdsyqcbf_panel.Controls.Add(this.textBox64);
            this.zdsyqcbf_panel.Controls.Add(this.textBox73);
            this.zdsyqcbf_panel.Controls.Add(this.label26);
            this.zdsyqcbf_panel.Controls.Add(this.textBox81);
            this.zdsyqcbf_panel.Controls.Add(this.textBox96);
            this.zdsyqcbf_panel.Controls.Add(this.textBox160);
            this.zdsyqcbf_panel.Controls.Add(this.textBox164);
            this.zdsyqcbf_panel.Controls.Add(this.textBox165);
            this.zdsyqcbf_panel.Controls.Add(this.textBox132);
            this.zdsyqcbf_panel.Controls.Add(this.textBox100);
            this.zdsyqcbf_panel.Controls.Add(this.textBox112);
            this.zdsyqcbf_panel.Controls.Add(this.textBox124);
            this.zdsyqcbf_panel.Controls.Add(this.textBox125);
            this.zdsyqcbf_panel.Controls.Add(this.textBox128);
            this.zdsyqcbf_panel.Controls.Add(this.textBox133);
            this.zdsyqcbf_panel.Controls.Add(this.textBox176);
            this.zdsyqcbf_panel.Controls.Add(this.textBox177);
            this.zdsyqcbf_panel.Controls.Add(this.textBox187);
            this.zdsyqcbf_panel.Controls.Add(this.label54);
            this.zdsyqcbf_panel.Controls.Add(this.hlab_ed);
            this.zdsyqcbf_panel.Controls.Add(this.textBox190);
            this.zdsyqcbf_panel.Controls.Add(this.hlab_zx);
            this.zdsyqcbf_panel.Controls.Add(this.textBox191);
            this.zdsyqcbf_panel.Controls.Add(this.label19);
            this.zdsyqcbf_panel.Controls.Add(this.label63);
            this.zdsyqcbf_panel.Controls.Add(this.label92);
            this.zdsyqcbf_panel.Controls.Add(this.label93);
            this.zdsyqcbf_panel.Controls.Add(this.label94);
            this.zdsyqcbf_panel.Controls.Add(this.label95);
            this.zdsyqcbf_panel.Controls.Add(this.label96);
            this.zdsyqcbf_panel.Controls.Add(this.label97);
            this.zdsyqcbf_panel.Controls.Add(this.label98);
            this.zdsyqcbf_panel.Controls.Add(this.label36);
            this.zdsyqcbf_panel.Controls.Add(this.label7);
            this.zdsyqcbf_panel.Controls.Add(this.label8);
            this.zdsyqcbf_panel.Controls.Add(this.label10);
            this.zdsyqcbf_panel.Controls.Add(this.label13);
            this.zdsyqcbf_panel.Controls.Add(this.label15);
            this.zdsyqcbf_panel.Controls.Add(this.label16);
            this.zdsyqcbf_panel.Controls.Add(this.label17);
            this.zdsyqcbf_panel.Controls.Add(this.label18);
            this.zdsyqcbf_panel.Controls.Add(this.label89);
            this.zdsyqcbf_panel.Controls.Add(this.label90);
            this.zdsyqcbf_panel.Controls.Add(this.label91);
            this.zdsyqcbf_panel.Controls.Add(this.label28);
            this.zdsyqcbf_panel.Controls.Add(this.label34);
            this.zdsyqcbf_panel.Controls.Add(this.label72);
            this.zdsyqcbf_panel.Controls.Add(this.label68);
            this.zdsyqcbf_panel.Controls.Add(this.label69);
            this.zdsyqcbf_panel.Controls.Add(this.label70);
            this.zdsyqcbf_panel.Controls.Add(this.label71);
            this.zdsyqcbf_panel.Controls.Add(this.label55);
            this.zdsyqcbf_panel.Controls.Add(this.label62);
            this.zdsyqcbf_panel.Controls.Add(this.label64);
            this.zdsyqcbf_panel.Controls.Add(this.label65);
            this.zdsyqcbf_panel.Controls.Add(this.label66);
            this.zdsyqcbf_panel.Controls.Add(this.label67);
            this.zdsyqcbf_panel.Controls.Add(this.shp2_lbl);
            this.zdsyqcbf_panel.Controls.Add(this.label56);
            this.zdsyqcbf_panel.Controls.Add(this.label57);
            this.zdsyqcbf_panel.Controls.Add(this.zd_lbl);
            this.zdsyqcbf_panel.Controls.Add(this.label58);
            this.zdsyqcbf_panel.Controls.Add(this.zx_lbl);
            this.zdsyqcbf_panel.Controls.Add(this.label59);
            this.zdsyqcbf_panel.Controls.Add(this.label60);
            this.zdsyqcbf_panel.Controls.Add(this.label61);
            this.zdsyqcbf_panel.Location = new System.Drawing.Point(6, 1191);
            this.zdsyqcbf_panel.Name = "zdsyqcbf_panel";
            this.zdsyqcbf_panel.Size = new System.Drawing.Size(726, 547);
            this.zdsyqcbf_panel.TabIndex = 224;
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.SystemColors.Window;
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label61.Location = new System.Drawing.Point(119, 253);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(115, 25);
            this.label61.TabIndex = 211;
            this.label61.Text = "设备操作人员成本";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.SystemColors.Window;
            this.label60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label60.Location = new System.Drawing.Point(119, 277);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(115, 25);
            this.label60.TabIndex = 212;
            this.label60.Text = "运行成本(水电等)";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.BackColor = System.Drawing.SystemColors.Window;
            this.label59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label59.Location = new System.Drawing.Point(119, 301);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(115, 25);
            this.label59.TabIndex = 213;
            this.label59.Text = "消耗性成本";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zx_lbl
            // 
            this.zx_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.zx_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zx_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zx_lbl.Location = new System.Drawing.Point(119, 229);
            this.zx_lbl.Name = "zx_lbl";
            this.zx_lbl.Size = new System.Drawing.Size(115, 25);
            this.zx_lbl.TabIndex = 170;
            this.zx_lbl.Text = "操作人员培训成本";
            this.zx_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.SystemColors.Window;
            this.label58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label58.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label58.Location = new System.Drawing.Point(620, 37);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 25);
            this.label58.TabIndex = 86;
            this.label58.Text = "SP002039";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zd_lbl
            // 
            this.zd_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.zd_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zd_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zd_lbl.Location = new System.Drawing.Point(119, 61);
            this.zd_lbl.Name = "zd_lbl";
            this.zd_lbl.Size = new System.Drawing.Size(115, 25);
            this.zd_lbl.TabIndex = 168;
            this.zd_lbl.Text = "设备价格";
            this.zd_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.SystemColors.Window;
            this.label57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label57.Location = new System.Drawing.Point(462, 37);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(80, 25);
            this.label57.TabIndex = 86;
            this.label57.Text = "SP002025";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.Window;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label56.Location = new System.Drawing.Point(119, 12);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(115, 50);
            this.label56.TabIndex = 167;
            this.label56.Text = "成本要素";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // shp2_lbl
            // 
            this.shp2_lbl.BackColor = System.Drawing.SystemColors.Window;
            this.shp2_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shp2_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shp2_lbl.Location = new System.Drawing.Point(383, 37);
            this.shp2_lbl.Name = "shp2_lbl";
            this.shp2_lbl.Size = new System.Drawing.Size(80, 25);
            this.shp2_lbl.TabIndex = 86;
            this.shp2_lbl.Text = "SP002013";
            this.shp2_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.SystemColors.Window;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label67.Location = new System.Drawing.Point(15, 253);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(105, 25);
            this.label67.TabIndex = 218;
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.SystemColors.Window;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label66.Location = new System.Drawing.Point(15, 277);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(105, 25);
            this.label66.TabIndex = 219;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.SystemColors.Window;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label65.Location = new System.Drawing.Point(15, 301);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(105, 25);
            this.label65.TabIndex = 220;
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.SystemColors.Window;
            this.label64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label64.Location = new System.Drawing.Point(15, 229);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(105, 25);
            this.label64.TabIndex = 217;
            this.label64.Text = "运营成本";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.SystemColors.Window;
            this.label62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label62.Location = new System.Drawing.Point(15, 61);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(105, 25);
            this.label62.TabIndex = 215;
            this.label62.Text = "采购成本";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.Window;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label55.Location = new System.Drawing.Point(15, 12);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(105, 50);
            this.label55.TabIndex = 214;
            this.label55.Text = "成本类型";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.SystemColors.Window;
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label71.Location = new System.Drawing.Point(119, 421);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(115, 25);
            this.label71.TabIndex = 229;
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.SystemColors.Window;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label70.Location = new System.Drawing.Point(119, 445);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(115, 25);
            this.label70.TabIndex = 230;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.SystemColors.Window;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label69.Location = new System.Drawing.Point(15, 421);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(105, 25);
            this.label69.TabIndex = 231;
            this.label69.Text = "修理成本";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.SystemColors.Window;
            this.label68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label68.Location = new System.Drawing.Point(15, 445);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(105, 25);
            this.label68.TabIndex = 232;
            this.label68.Text = "处置成本";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.SystemColors.Window;
            this.label72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label72.Location = new System.Drawing.Point(541, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(80, 25);
            this.label72.TabIndex = 239;
            this.label72.Text = "SP002034";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.SystemColors.Window;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label34.Location = new System.Drawing.Point(119, 469);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(115, 25);
            this.label34.TabIndex = 262;
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.SystemColors.Window;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(15, 469);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 25);
            this.label28.TabIndex = 263;
            this.label28.Text = "其他成本";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.SystemColors.Window;
            this.label91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label91.Location = new System.Drawing.Point(119, 133);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(115, 25);
            this.label91.TabIndex = 284;
            this.label91.Text = "文件成本";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label90
            // 
            this.label90.BackColor = System.Drawing.SystemColors.Window;
            this.label90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label90.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label90.Location = new System.Drawing.Point(119, 157);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(115, 25);
            this.label90.TabIndex = 285;
            this.label90.Text = "物流成本";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label89
            // 
            this.label89.BackColor = System.Drawing.SystemColors.Window;
            this.label89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label89.Location = new System.Drawing.Point(119, 181);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(115, 25);
            this.label89.TabIndex = 286;
            this.label89.Text = "安装成本";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.SystemColors.Window;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(119, 109);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 25);
            this.label18.TabIndex = 283;
            this.label18.Text = "通讯成本";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.SystemColors.Window;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(119, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(115, 25);
            this.label17.TabIndex = 282;
            this.label17.Text = "采购执行时间成本";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.SystemColors.Window;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(15, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 25);
            this.label16.TabIndex = 289;
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.SystemColors.Window;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(15, 157);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 25);
            this.label15.TabIndex = 290;
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.Window;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(15, 181);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 25);
            this.label13.TabIndex = 291;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.Window;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(15, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 25);
            this.label10.TabIndex = 288;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Window;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(15, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 25);
            this.label8.TabIndex = 287;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Window;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(119, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 25);
            this.label7.TabIndex = 295;
            this.label7.Text = "试运行成本";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.SystemColors.Window;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label36.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label36.Location = new System.Drawing.Point(15, 205);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 25);
            this.label36.TabIndex = 296;
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label98
            // 
            this.label98.BackColor = System.Drawing.SystemColors.Window;
            this.label98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label98.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label98.Location = new System.Drawing.Point(119, 349);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(115, 25);
            this.label98.TabIndex = 328;
            this.label98.Text = "维护人员成本";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label97
            // 
            this.label97.BackColor = System.Drawing.SystemColors.Window;
            this.label97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label97.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label97.Location = new System.Drawing.Point(119, 373);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(115, 25);
            this.label97.TabIndex = 329;
            this.label97.Text = "维护工具设备成本";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label96
            // 
            this.label96.BackColor = System.Drawing.SystemColors.Window;
            this.label96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label96.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label96.Location = new System.Drawing.Point(119, 397);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 25);
            this.label96.TabIndex = 330;
            this.label96.Text = "停工期成本";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label95
            // 
            this.label95.BackColor = System.Drawing.SystemColors.Window;
            this.label95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label95.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label95.Location = new System.Drawing.Point(119, 325);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(115, 25);
            this.label95.TabIndex = 327;
            this.label95.Text = "采购和存货成本";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.SystemColors.Window;
            this.label94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label94.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label94.Location = new System.Drawing.Point(15, 349);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(105, 25);
            this.label94.TabIndex = 332;
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label93
            // 
            this.label93.BackColor = System.Drawing.SystemColors.Window;
            this.label93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label93.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label93.Location = new System.Drawing.Point(15, 373);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(105, 25);
            this.label93.TabIndex = 333;
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label92
            // 
            this.label92.BackColor = System.Drawing.SystemColors.Window;
            this.label92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label92.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label92.Location = new System.Drawing.Point(15, 397);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(105, 25);
            this.label92.TabIndex = 334;
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.SystemColors.Window;
            this.label63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label63.Location = new System.Drawing.Point(15, 325);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(105, 25);
            this.label63.TabIndex = 331;
            this.label63.Text = "预防性维护成本";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.SystemColors.Window;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(383, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(317, 26);
            this.label19.TabIndex = 347;
            this.label19.Text = "净现值";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox191
            // 
            this.textBox191.BackColor = System.Drawing.SystemColors.Window;
            this.textBox191.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox191.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox191.Location = new System.Drawing.Point(233, 253);
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new System.Drawing.Size(90, 25);
            this.textBox191.TabIndex = 377;
            this.textBox191.Text = "3";
            this.textBox191.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlab_zx
            // 
            this.hlab_zx.BackColor = System.Drawing.SystemColors.Window;
            this.hlab_zx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlab_zx.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlab_zx.Location = new System.Drawing.Point(233, 301);
            this.hlab_zx.Name = "hlab_zx";
            this.hlab_zx.Size = new System.Drawing.Size(90, 25);
            this.hlab_zx.TabIndex = 379;
            this.hlab_zx.Text = "3";
            this.hlab_zx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox190
            // 
            this.textBox190.BackColor = System.Drawing.SystemColors.Window;
            this.textBox190.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox190.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox190.Location = new System.Drawing.Point(233, 229);
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new System.Drawing.Size(90, 25);
            this.textBox190.TabIndex = 376;
            this.textBox190.Text = "1";
            this.textBox190.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hlab_ed
            // 
            this.hlab_ed.BackColor = System.Drawing.SystemColors.Window;
            this.hlab_ed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hlab_ed.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hlab_ed.Location = new System.Drawing.Point(233, 277);
            this.hlab_ed.Name = "hlab_ed";
            this.hlab_ed.Size = new System.Drawing.Size(90, 25);
            this.hlab_ed.TabIndex = 378;
            this.hlab_ed.Text = "3";
            this.hlab_ed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.Window;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label54.Location = new System.Drawing.Point(233, 12);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(90, 50);
            this.label54.TabIndex = 380;
            this.label54.Text = "设备生命周期";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox187
            // 
            this.textBox187.BackColor = System.Drawing.SystemColors.Window;
            this.textBox187.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox187.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox187.Location = new System.Drawing.Point(233, 61);
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new System.Drawing.Size(90, 25);
            this.textBox187.TabIndex = 375;
            this.textBox187.Text = "1";
            this.textBox187.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox177
            // 
            this.textBox177.BackColor = System.Drawing.SystemColors.Window;
            this.textBox177.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox177.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox177.Location = new System.Drawing.Point(233, 445);
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new System.Drawing.Size(90, 25);
            this.textBox177.TabIndex = 382;
            this.textBox177.Text = "3";
            this.textBox177.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox176
            // 
            this.textBox176.BackColor = System.Drawing.SystemColors.Window;
            this.textBox176.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox176.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox176.Location = new System.Drawing.Point(233, 421);
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new System.Drawing.Size(90, 25);
            this.textBox176.TabIndex = 381;
            this.textBox176.Text = "3";
            this.textBox176.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox133
            // 
            this.textBox133.BackColor = System.Drawing.SystemColors.Window;
            this.textBox133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox133.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox133.Location = new System.Drawing.Point(233, 469);
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new System.Drawing.Size(90, 25);
            this.textBox133.TabIndex = 391;
            this.textBox133.Text = "3";
            this.textBox133.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox128
            // 
            this.textBox128.BackColor = System.Drawing.SystemColors.Window;
            this.textBox128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox128.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox128.Location = new System.Drawing.Point(233, 85);
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new System.Drawing.Size(90, 25);
            this.textBox128.TabIndex = 393;
            this.textBox128.Text = "1";
            this.textBox128.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox125
            // 
            this.textBox125.BackColor = System.Drawing.SystemColors.Window;
            this.textBox125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox125.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox125.Location = new System.Drawing.Point(233, 133);
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new System.Drawing.Size(90, 25);
            this.textBox125.TabIndex = 395;
            this.textBox125.Text = "1";
            this.textBox125.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox124
            // 
            this.textBox124.BackColor = System.Drawing.SystemColors.Window;
            this.textBox124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox124.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox124.Location = new System.Drawing.Point(233, 181);
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new System.Drawing.Size(90, 25);
            this.textBox124.TabIndex = 397;
            this.textBox124.Text = "1";
            this.textBox124.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox112
            // 
            this.textBox112.BackColor = System.Drawing.SystemColors.Window;
            this.textBox112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox112.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox112.Location = new System.Drawing.Point(233, 109);
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new System.Drawing.Size(90, 25);
            this.textBox112.TabIndex = 394;
            this.textBox112.Text = "3";
            this.textBox112.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox100
            // 
            this.textBox100.BackColor = System.Drawing.SystemColors.Window;
            this.textBox100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox100.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox100.Location = new System.Drawing.Point(233, 157);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(90, 25);
            this.textBox100.TabIndex = 396;
            this.textBox100.Text = "1";
            this.textBox100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox132
            // 
            this.textBox132.BackColor = System.Drawing.SystemColors.Window;
            this.textBox132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox132.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox132.Location = new System.Drawing.Point(233, 205);
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new System.Drawing.Size(90, 25);
            this.textBox132.TabIndex = 398;
            this.textBox132.Text = "1";
            this.textBox132.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox165
            // 
            this.textBox165.BackColor = System.Drawing.SystemColors.Window;
            this.textBox165.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox165.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox165.Location = new System.Drawing.Point(233, 349);
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new System.Drawing.Size(90, 25);
            this.textBox165.TabIndex = 406;
            this.textBox165.Text = "3";
            this.textBox165.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox164
            // 
            this.textBox164.BackColor = System.Drawing.SystemColors.Window;
            this.textBox164.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox164.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox164.Location = new System.Drawing.Point(233, 397);
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new System.Drawing.Size(90, 25);
            this.textBox164.TabIndex = 408;
            this.textBox164.Text = "3";
            this.textBox164.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox160
            // 
            this.textBox160.BackColor = System.Drawing.SystemColors.Window;
            this.textBox160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox160.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox160.Location = new System.Drawing.Point(233, 325);
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new System.Drawing.Size(90, 25);
            this.textBox160.TabIndex = 405;
            this.textBox160.Text = "3";
            this.textBox160.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox96
            // 
            this.textBox96.BackColor = System.Drawing.SystemColors.Window;
            this.textBox96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox96.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox96.Location = new System.Drawing.Point(233, 373);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(90, 25);
            this.textBox96.TabIndex = 407;
            this.textBox96.Text = "3";
            this.textBox96.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.Window;
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox81.Location = new System.Drawing.Point(322, 61);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(62, 25);
            this.textBox81.TabIndex = 413;
            this.textBox81.Text = "5%";
            this.textBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.SystemColors.Window;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.Location = new System.Drawing.Point(322, 12);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 50);
            this.label26.TabIndex = 414;
            this.label26.Text = "贴现率";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.SystemColors.Window;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox73.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox73.Location = new System.Drawing.Point(322, 85);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(62, 25);
            this.textBox73.TabIndex = 415;
            this.textBox73.Text = "5%";
            this.textBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.Window;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox64.Location = new System.Drawing.Point(322, 133);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(62, 25);
            this.textBox64.TabIndex = 417;
            this.textBox64.Text = "5%";
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.SystemColors.Window;
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox63.Location = new System.Drawing.Point(322, 109);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(62, 25);
            this.textBox63.TabIndex = 416;
            this.textBox63.Text = "5%";
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.SystemColors.Window;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox62.Location = new System.Drawing.Point(322, 157);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(62, 25);
            this.textBox62.TabIndex = 418;
            this.textBox62.Text = "5%";
            this.textBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox61.Location = new System.Drawing.Point(322, 181);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(62, 25);
            this.textBox61.TabIndex = 419;
            this.textBox61.Text = "5%";
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Window;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox41.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox41.Location = new System.Drawing.Point(322, 229);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(62, 25);
            this.textBox41.TabIndex = 421;
            this.textBox41.Text = "5%";
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Window;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox40.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox40.Location = new System.Drawing.Point(322, 205);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(62, 25);
            this.textBox40.TabIndex = 420;
            this.textBox40.Text = "5%";
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Window;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox38.Location = new System.Drawing.Point(322, 253);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(62, 25);
            this.textBox38.TabIndex = 422;
            this.textBox38.Text = "5%";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.Window;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox37.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox37.Location = new System.Drawing.Point(322, 277);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(62, 25);
            this.textBox37.TabIndex = 423;
            this.textBox37.Text = "5%";
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Window;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox35.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox35.Location = new System.Drawing.Point(322, 325);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(62, 25);
            this.textBox35.TabIndex = 425;
            this.textBox35.Text = "5%";
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Window;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox34.Location = new System.Drawing.Point(322, 301);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(62, 25);
            this.textBox34.TabIndex = 424;
            this.textBox34.Text = "5%";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Window;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox31.Location = new System.Drawing.Point(322, 349);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(62, 25);
            this.textBox31.TabIndex = 426;
            this.textBox31.Text = "5%";
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Window;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox30.Location = new System.Drawing.Point(322, 373);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(62, 25);
            this.textBox30.TabIndex = 427;
            this.textBox30.Text = "5%";
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.SystemColors.Window;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox27.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox27.Location = new System.Drawing.Point(322, 421);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(62, 25);
            this.textBox27.TabIndex = 429;
            this.textBox27.Text = "5%";
            this.textBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Window;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox26.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox26.Location = new System.Drawing.Point(322, 397);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(62, 25);
            this.textBox26.TabIndex = 428;
            this.textBox26.Text = "5%";
            this.textBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Window;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox19.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox19.Location = new System.Drawing.Point(322, 469);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(62, 25);
            this.textBox19.TabIndex = 431;
            this.textBox19.Text = "5%";
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Window;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox16.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox16.Location = new System.Drawing.Point(322, 445);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(62, 25);
            this.textBox16.TabIndex = 430;
            this.textBox16.Text = "5%";
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.SystemColors.Window;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label31.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(119, 493);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 25);
            this.label31.TabIndex = 435;
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox201
            // 
            this.textBox201.BackColor = System.Drawing.SystemColors.Window;
            this.textBox201.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox201.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox201.Location = new System.Drawing.Point(233, 493);
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new System.Drawing.Size(90, 25);
            this.textBox201.TabIndex = 432;
            this.textBox201.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.SystemColors.Window;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label27.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(15, 493);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 25);
            this.label27.TabIndex = 436;
            this.label27.Text = "成本总计";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox193
            // 
            this.textBox193.BackColor = System.Drawing.SystemColors.Window;
            this.textBox193.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox193.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox193.Location = new System.Drawing.Point(322, 493);
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new System.Drawing.Size(62, 25);
            this.textBox193.TabIndex = 440;
            this.textBox193.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox129
            // 
            this.textBox129.BackColor = System.Drawing.SystemColors.Window;
            this.textBox129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox129.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox129.Location = new System.Drawing.Point(383, 229);
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new System.Drawing.Size(80, 25);
            this.textBox129.TabIndex = 442;
            this.textBox129.Text = "9524";
            this.textBox129.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox117
            // 
            this.textBox117.BackColor = System.Drawing.SystemColors.Window;
            this.textBox117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox117.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox117.Location = new System.Drawing.Point(383, 61);
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new System.Drawing.Size(80, 25);
            this.textBox117.TabIndex = 441;
            this.textBox117.Text = "476190";
            this.textBox117.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox116
            // 
            this.textBox116.BackColor = System.Drawing.SystemColors.Window;
            this.textBox116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox116.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox116.Location = new System.Drawing.Point(383, 253);
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new System.Drawing.Size(80, 25);
            this.textBox116.TabIndex = 443;
            this.textBox116.Text = "27232";
            this.textBox116.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox114
            // 
            this.textBox114.BackColor = System.Drawing.SystemColors.Window;
            this.textBox114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox114.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox114.Location = new System.Drawing.Point(383, 301);
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new System.Drawing.Size(80, 25);
            this.textBox114.TabIndex = 445;
            this.textBox114.Text = "13616";
            this.textBox114.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox113
            // 
            this.textBox113.BackColor = System.Drawing.SystemColors.Window;
            this.textBox113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox113.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox113.Location = new System.Drawing.Point(383, 277);
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new System.Drawing.Size(80, 25);
            this.textBox113.TabIndex = 444;
            this.textBox113.Text = "16339";
            this.textBox113.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox111
            // 
            this.textBox111.BackColor = System.Drawing.SystemColors.Window;
            this.textBox111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox111.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox111.Location = new System.Drawing.Point(383, 445);
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new System.Drawing.Size(80, 25);
            this.textBox111.TabIndex = 447;
            this.textBox111.Text = "5446";
            this.textBox111.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox110
            // 
            this.textBox110.BackColor = System.Drawing.SystemColors.Window;
            this.textBox110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox110.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox110.Location = new System.Drawing.Point(383, 421);
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new System.Drawing.Size(80, 25);
            this.textBox110.TabIndex = 446;
            this.textBox110.Text = "5446";
            this.textBox110.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox109
            // 
            this.textBox109.BackColor = System.Drawing.SystemColors.Window;
            this.textBox109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox109.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox109.Location = new System.Drawing.Point(383, 469);
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new System.Drawing.Size(80, 25);
            this.textBox109.TabIndex = 448;
            this.textBox109.Text = "5446";
            this.textBox109.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.SystemColors.Window;
            this.textBox97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox97.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox97.Location = new System.Drawing.Point(383, 85);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(80, 25);
            this.textBox97.TabIndex = 449;
            this.textBox97.Text = "1905";
            this.textBox97.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.SystemColors.Window;
            this.textBox94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox94.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox94.Location = new System.Drawing.Point(383, 109);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(80, 25);
            this.textBox94.TabIndex = 450;
            this.textBox94.Text = "8170";
            this.textBox94.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox93
            // 
            this.textBox93.BackColor = System.Drawing.SystemColors.Window;
            this.textBox93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox93.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox93.Location = new System.Drawing.Point(383, 133);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(80, 25);
            this.textBox93.TabIndex = 451;
            this.textBox93.Text = "4762";
            this.textBox93.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.SystemColors.Window;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox91.Location = new System.Drawing.Point(383, 181);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(80, 25);
            this.textBox91.TabIndex = 453;
            this.textBox91.Text = "1905";
            this.textBox91.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Window;
            this.textBox90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox90.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox90.Location = new System.Drawing.Point(383, 157);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(80, 25);
            this.textBox90.TabIndex = 452;
            this.textBox90.Text = "4762";
            this.textBox90.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox89.Location = new System.Drawing.Point(383, 205);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(80, 25);
            this.textBox89.TabIndex = 454;
            this.textBox89.Text = "1905";
            this.textBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.Window;
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox88.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox88.Location = new System.Drawing.Point(383, 325);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(80, 25);
            this.textBox88.TabIndex = 455;
            this.textBox88.Text = "27232";
            this.textBox88.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.SystemColors.Window;
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox85.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox85.Location = new System.Drawing.Point(383, 349);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(80, 25);
            this.textBox85.TabIndex = 456;
            this.textBox85.Text = "8170";
            this.textBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.Window;
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox83.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox83.Location = new System.Drawing.Point(383, 397);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(80, 25);
            this.textBox83.TabIndex = 458;
            this.textBox83.Text = "2723";
            this.textBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.Window;
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox80.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox80.Location = new System.Drawing.Point(383, 373);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(80, 25);
            this.textBox80.TabIndex = 457;
            this.textBox80.Text = "5446";
            this.textBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.SystemColors.Window;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox72.ForeColor = System.Drawing.Color.Red;
            this.textBox72.Location = new System.Drawing.Point(383, 493);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(80, 25);
            this.textBox72.TabIndex = 459;
            this.textBox72.Text = "626222";
            this.textBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox173
            // 
            this.textBox173.BackColor = System.Drawing.SystemColors.Window;
            this.textBox173.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox173.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox173.Location = new System.Drawing.Point(462, 229);
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new System.Drawing.Size(80, 25);
            this.textBox173.TabIndex = 461;
            this.textBox173.Text = "9524";
            this.textBox173.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox172
            // 
            this.textBox172.BackColor = System.Drawing.SystemColors.Window;
            this.textBox172.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox172.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox172.Location = new System.Drawing.Point(462, 61);
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new System.Drawing.Size(80, 25);
            this.textBox172.TabIndex = 460;
            this.textBox172.Text = "495238";
            this.textBox172.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox171
            // 
            this.textBox171.BackColor = System.Drawing.SystemColors.Window;
            this.textBox171.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox171.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox171.Location = new System.Drawing.Point(462, 253);
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new System.Drawing.Size(80, 25);
            this.textBox171.TabIndex = 462;
            this.textBox171.Text = "27232";
            this.textBox171.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox166
            // 
            this.textBox166.BackColor = System.Drawing.SystemColors.Window;
            this.textBox166.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox166.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox166.Location = new System.Drawing.Point(462, 301);
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new System.Drawing.Size(80, 25);
            this.textBox166.TabIndex = 464;
            this.textBox166.Text = "13616";
            this.textBox166.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox163
            // 
            this.textBox163.BackColor = System.Drawing.SystemColors.Window;
            this.textBox163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox163.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox163.Location = new System.Drawing.Point(462, 277);
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new System.Drawing.Size(80, 25);
            this.textBox163.TabIndex = 463;
            this.textBox163.Text = "16339";
            this.textBox163.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox162
            // 
            this.textBox162.BackColor = System.Drawing.SystemColors.Window;
            this.textBox162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox162.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox162.Location = new System.Drawing.Point(462, 445);
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new System.Drawing.Size(80, 25);
            this.textBox162.TabIndex = 466;
            this.textBox162.Text = "5446";
            this.textBox162.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox161
            // 
            this.textBox161.BackColor = System.Drawing.SystemColors.Window;
            this.textBox161.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox161.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox161.Location = new System.Drawing.Point(462, 421);
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new System.Drawing.Size(80, 25);
            this.textBox161.TabIndex = 465;
            this.textBox161.Text = "5446";
            this.textBox161.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox137
            // 
            this.textBox137.BackColor = System.Drawing.SystemColors.Window;
            this.textBox137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox137.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox137.Location = new System.Drawing.Point(462, 469);
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new System.Drawing.Size(80, 25);
            this.textBox137.TabIndex = 467;
            this.textBox137.Text = "5446";
            this.textBox137.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox136
            // 
            this.textBox136.BackColor = System.Drawing.SystemColors.Window;
            this.textBox136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox136.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox136.Location = new System.Drawing.Point(462, 85);
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new System.Drawing.Size(80, 25);
            this.textBox136.TabIndex = 468;
            this.textBox136.Text = "1905";
            this.textBox136.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox105
            // 
            this.textBox105.BackColor = System.Drawing.SystemColors.Window;
            this.textBox105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox105.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox105.Location = new System.Drawing.Point(462, 109);
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new System.Drawing.Size(80, 25);
            this.textBox105.TabIndex = 469;
            this.textBox105.Text = "8170";
            this.textBox105.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.SystemColors.Window;
            this.textBox98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox98.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox98.Location = new System.Drawing.Point(462, 133);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(80, 25);
            this.textBox98.TabIndex = 470;
            this.textBox98.Text = "952";
            this.textBox98.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.SystemColors.Window;
            this.textBox95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox95.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox95.Location = new System.Drawing.Point(462, 181);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(80, 25);
            this.textBox95.TabIndex = 472;
            this.textBox95.Text = "1905";
            this.textBox95.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox87.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox87.Location = new System.Drawing.Point(462, 157);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(80, 25);
            this.textBox87.TabIndex = 471;
            this.textBox87.Text = "4762";
            this.textBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.SystemColors.Window;
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox86.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox86.Location = new System.Drawing.Point(462, 205);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(80, 25);
            this.textBox86.TabIndex = 473;
            this.textBox86.Text = "1905";
            this.textBox86.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.SystemColors.Window;
            this.textBox82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox82.Location = new System.Drawing.Point(462, 325);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(80, 25);
            this.textBox82.TabIndex = 474;
            this.textBox82.Text = "13616";
            this.textBox82.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.Window;
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox79.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox79.Location = new System.Drawing.Point(462, 349);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(80, 25);
            this.textBox79.TabIndex = 475;
            this.textBox79.Text = "8170";
            this.textBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.SystemColors.Window;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox75.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox75.Location = new System.Drawing.Point(462, 397);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(80, 25);
            this.textBox75.TabIndex = 477;
            this.textBox75.Text = "2723";
            this.textBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.SystemColors.Window;
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox74.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox74.Location = new System.Drawing.Point(462, 373);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(80, 25);
            this.textBox74.TabIndex = 476;
            this.textBox74.Text = "5446";
            this.textBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.SystemColors.Window;
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox71.Location = new System.Drawing.Point(462, 493);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(80, 25);
            this.textBox71.TabIndex = 478;
            this.textBox71.Text = "627844";
            this.textBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox183
            // 
            this.textBox183.BackColor = System.Drawing.SystemColors.Window;
            this.textBox183.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox183.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox183.Location = new System.Drawing.Point(541, 229);
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new System.Drawing.Size(80, 25);
            this.textBox183.TabIndex = 480;
            this.textBox183.Text = "9524";
            this.textBox183.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox182
            // 
            this.textBox182.BackColor = System.Drawing.SystemColors.Window;
            this.textBox182.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox182.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox182.Location = new System.Drawing.Point(541, 61);
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new System.Drawing.Size(80, 25);
            this.textBox182.TabIndex = 479;
            this.textBox182.Text = "485174";
            this.textBox182.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox181
            // 
            this.textBox181.BackColor = System.Drawing.SystemColors.Window;
            this.textBox181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox181.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox181.Location = new System.Drawing.Point(541, 253);
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new System.Drawing.Size(80, 25);
            this.textBox181.TabIndex = 481;
            this.textBox181.Text = "27232";
            this.textBox181.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox180
            // 
            this.textBox180.BackColor = System.Drawing.SystemColors.Window;
            this.textBox180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox180.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox180.Location = new System.Drawing.Point(541, 301);
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new System.Drawing.Size(80, 25);
            this.textBox180.TabIndex = 483;
            this.textBox180.Text = "13616";
            this.textBox180.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox179
            // 
            this.textBox179.BackColor = System.Drawing.SystemColors.Window;
            this.textBox179.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox179.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox179.Location = new System.Drawing.Point(541, 277);
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new System.Drawing.Size(80, 25);
            this.textBox179.TabIndex = 482;
            this.textBox179.Text = "16339";
            this.textBox179.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox178
            // 
            this.textBox178.BackColor = System.Drawing.SystemColors.Window;
            this.textBox178.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox178.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox178.Location = new System.Drawing.Point(541, 445);
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new System.Drawing.Size(80, 25);
            this.textBox178.TabIndex = 485;
            this.textBox178.Text = "5446";
            this.textBox178.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox175
            // 
            this.textBox175.BackColor = System.Drawing.SystemColors.Window;
            this.textBox175.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox175.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox175.Location = new System.Drawing.Point(541, 421);
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new System.Drawing.Size(80, 25);
            this.textBox175.TabIndex = 484;
            this.textBox175.Text = "5446";
            this.textBox175.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox174
            // 
            this.textBox174.BackColor = System.Drawing.SystemColors.Window;
            this.textBox174.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox174.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox174.Location = new System.Drawing.Point(541, 469);
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new System.Drawing.Size(80, 25);
            this.textBox174.TabIndex = 486;
            this.textBox174.Text = "5446";
            this.textBox174.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox120
            // 
            this.textBox120.BackColor = System.Drawing.SystemColors.Window;
            this.textBox120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox120.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox120.Location = new System.Drawing.Point(541, 85);
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new System.Drawing.Size(80, 25);
            this.textBox120.TabIndex = 487;
            this.textBox120.Text = "1905";
            this.textBox120.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox119
            // 
            this.textBox119.BackColor = System.Drawing.SystemColors.Window;
            this.textBox119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox119.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox119.Location = new System.Drawing.Point(541, 109);
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new System.Drawing.Size(80, 25);
            this.textBox119.TabIndex = 488;
            this.textBox119.Text = "8170";
            this.textBox119.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox118
            // 
            this.textBox118.BackColor = System.Drawing.SystemColors.Window;
            this.textBox118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox118.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox118.Location = new System.Drawing.Point(541, 133);
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new System.Drawing.Size(80, 25);
            this.textBox118.TabIndex = 489;
            this.textBox118.Text = "952";
            this.textBox118.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox101
            // 
            this.textBox101.BackColor = System.Drawing.SystemColors.Window;
            this.textBox101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox101.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox101.Location = new System.Drawing.Point(541, 181);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(80, 25);
            this.textBox101.TabIndex = 491;
            this.textBox101.Text = "1905";
            this.textBox101.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.SystemColors.Window;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox78.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox78.Location = new System.Drawing.Point(541, 157);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(80, 25);
            this.textBox78.TabIndex = 490;
            this.textBox78.Text = "4762";
            this.textBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.SystemColors.Window;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox77.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox77.Location = new System.Drawing.Point(541, 205);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(80, 25);
            this.textBox77.TabIndex = 492;
            this.textBox77.Text = "1905";
            this.textBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.SystemColors.Window;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox76.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox76.Location = new System.Drawing.Point(541, 325);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(80, 25);
            this.textBox76.TabIndex = 493;
            this.textBox76.Text = "16339";
            this.textBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.SystemColors.Window;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox70.Location = new System.Drawing.Point(541, 349);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(80, 25);
            this.textBox70.TabIndex = 494;
            this.textBox70.Text = "8170";
            this.textBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.SystemColors.Window;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox69.Location = new System.Drawing.Point(541, 397);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(80, 25);
            this.textBox69.TabIndex = 496;
            this.textBox69.Text = "2723";
            this.textBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.SystemColors.Window;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox66.Location = new System.Drawing.Point(541, 373);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(80, 25);
            this.textBox66.TabIndex = 495;
            this.textBox66.Text = "5446";
            this.textBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.SystemColors.Window;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox65.Location = new System.Drawing.Point(541, 493);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(80, 25);
            this.textBox65.TabIndex = 497;
            this.textBox65.Text = "626490";
            this.textBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox184
            // 
            this.textBox184.BackColor = System.Drawing.SystemColors.Window;
            this.textBox184.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox184.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox184.Location = new System.Drawing.Point(620, 229);
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new System.Drawing.Size(80, 25);
            this.textBox184.TabIndex = 499;
            this.textBox184.Text = "9524";
            this.textBox184.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox170
            // 
            this.textBox170.BackColor = System.Drawing.SystemColors.Window;
            this.textBox170.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox170.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox170.Location = new System.Drawing.Point(620, 61);
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new System.Drawing.Size(80, 25);
            this.textBox170.TabIndex = 498;
            this.textBox170.Text = "495238";
            this.textBox170.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox169
            // 
            this.textBox169.BackColor = System.Drawing.SystemColors.Window;
            this.textBox169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox169.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox169.Location = new System.Drawing.Point(620, 253);
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new System.Drawing.Size(80, 25);
            this.textBox169.TabIndex = 500;
            this.textBox169.Text = "27232";
            this.textBox169.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox168
            // 
            this.textBox168.BackColor = System.Drawing.SystemColors.Window;
            this.textBox168.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox168.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox168.Location = new System.Drawing.Point(620, 301);
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new System.Drawing.Size(80, 25);
            this.textBox168.TabIndex = 502;
            this.textBox168.Text = "13616";
            this.textBox168.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox167
            // 
            this.textBox167.BackColor = System.Drawing.SystemColors.Window;
            this.textBox167.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox167.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox167.Location = new System.Drawing.Point(620, 277);
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new System.Drawing.Size(80, 25);
            this.textBox167.TabIndex = 501;
            this.textBox167.Text = "21786";
            this.textBox167.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox122
            // 
            this.textBox122.BackColor = System.Drawing.SystemColors.Window;
            this.textBox122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox122.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox122.Location = new System.Drawing.Point(620, 445);
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new System.Drawing.Size(80, 25);
            this.textBox122.TabIndex = 504;
            this.textBox122.Text = "5446";
            this.textBox122.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox121
            // 
            this.textBox121.BackColor = System.Drawing.SystemColors.Window;
            this.textBox121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox121.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox121.Location = new System.Drawing.Point(620, 421);
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new System.Drawing.Size(80, 25);
            this.textBox121.TabIndex = 503;
            this.textBox121.Text = "5446";
            this.textBox121.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox115
            // 
            this.textBox115.BackColor = System.Drawing.SystemColors.Window;
            this.textBox115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox115.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox115.Location = new System.Drawing.Point(620, 469);
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new System.Drawing.Size(80, 25);
            this.textBox115.TabIndex = 505;
            this.textBox115.Text = "5446";
            this.textBox115.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox108
            // 
            this.textBox108.BackColor = System.Drawing.SystemColors.Window;
            this.textBox108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox108.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox108.Location = new System.Drawing.Point(620, 85);
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new System.Drawing.Size(80, 25);
            this.textBox108.TabIndex = 506;
            this.textBox108.Text = "1905";
            this.textBox108.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox107
            // 
            this.textBox107.BackColor = System.Drawing.SystemColors.Window;
            this.textBox107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox107.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox107.Location = new System.Drawing.Point(620, 109);
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new System.Drawing.Size(80, 25);
            this.textBox107.TabIndex = 507;
            this.textBox107.Text = "8170";
            this.textBox107.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox106
            // 
            this.textBox106.BackColor = System.Drawing.SystemColors.Window;
            this.textBox106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox106.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox106.Location = new System.Drawing.Point(620, 133);
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new System.Drawing.Size(80, 25);
            this.textBox106.TabIndex = 508;
            this.textBox106.Text = "952";
            this.textBox106.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox104
            // 
            this.textBox104.BackColor = System.Drawing.SystemColors.Window;
            this.textBox104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox104.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox104.Location = new System.Drawing.Point(620, 181);
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new System.Drawing.Size(80, 25);
            this.textBox104.TabIndex = 510;
            this.textBox104.Text = "1905";
            this.textBox104.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox103
            // 
            this.textBox103.BackColor = System.Drawing.SystemColors.Window;
            this.textBox103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox103.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox103.Location = new System.Drawing.Point(620, 157);
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(80, 25);
            this.textBox103.TabIndex = 509;
            this.textBox103.Text = "4762";
            this.textBox103.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.SystemColors.Window;
            this.textBox102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox102.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox102.Location = new System.Drawing.Point(620, 205);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(80, 25);
            this.textBox102.TabIndex = 511;
            this.textBox102.Text = "1905";
            this.textBox102.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.SystemColors.Window;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox99.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox99.Location = new System.Drawing.Point(620, 325);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(80, 25);
            this.textBox99.TabIndex = 512;
            this.textBox99.Text = "21786";
            this.textBox99.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.SystemColors.Window;
            this.textBox92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox92.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox92.Location = new System.Drawing.Point(620, 349);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(80, 25);
            this.textBox92.TabIndex = 513;
            this.textBox92.Text = "8170";
            this.textBox92.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.Window;
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox84.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox84.Location = new System.Drawing.Point(620, 397);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(80, 25);
            this.textBox84.TabIndex = 515;
            this.textBox84.Text = "2723";
            this.textBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.SystemColors.Window;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox68.Location = new System.Drawing.Point(620, 373);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(80, 25);
            this.textBox68.TabIndex = 514;
            this.textBox68.Text = "5446";
            this.textBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.SystemColors.Window;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox67.Location = new System.Drawing.Point(620, 493);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(80, 25);
            this.textBox67.TabIndex = 516;
            this.textBox67.Text = "627174";
            this.textBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.RFxbh_tb);
            this.tabPage2.Controls.Add(this.zt_tb);
            this.tabPage2.Controls.Add(this.gsbz_tb);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.query_bt);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(903, 1790);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "供应商回复情况";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(17, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 65;
            this.label9.Text = "公司标识";
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(722, 18);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(80, 30);
            this.query_bt.TabIndex = 71;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.textBox123);
            this.panel8.Controls.Add(this.textBox126);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Controls.Add(this.textBox127);
            this.panel8.Controls.Add(this.textBox130);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.textBox131);
            this.panel8.Controls.Add(this.textBox134);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.textBox135);
            this.panel8.Controls.Add(this.textBox138);
            this.panel8.Controls.Add(this.checkBox6);
            this.panel8.Controls.Add(this.checkBox9);
            this.panel8.Controls.Add(this.label37);
            this.panel8.Controls.Add(this.textBox1);
            this.panel8.Controls.Add(this.textBox10);
            this.panel8.Controls.Add(this.label44);
            this.panel8.Controls.Add(this.textBox13);
            this.panel8.Controls.Add(this.textBox22);
            this.panel8.Controls.Add(this.label45);
            this.panel8.Controls.Add(this.textBox25);
            this.panel8.Controls.Add(this.textBox28);
            this.panel8.Controls.Add(this.label46);
            this.panel8.Controls.Add(this.textBox29);
            this.panel8.Controls.Add(this.textBox32);
            this.panel8.Controls.Add(this.textBox33);
            this.panel8.Controls.Add(this.label47);
            this.panel8.Controls.Add(this.label48);
            this.panel8.Controls.Add(this.label49);
            this.panel8.Controls.Add(this.textBox36);
            this.panel8.Controls.Add(this.textBox39);
            this.panel8.Controls.Add(this.textBox42);
            this.panel8.Controls.Add(this.textBox43);
            this.panel8.Controls.Add(this.textBox44);
            this.panel8.Location = new System.Drawing.Point(0, 51);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(860, 153);
            this.panel8.TabIndex = 219;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Window;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox44.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox44.Location = new System.Drawing.Point(73, 64);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(230, 25);
            this.textBox44.TabIndex = 6;
            this.textBox44.Text = "辽宁纪元冶金矿山设备制造有限公司";
            this.textBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.SystemColors.Window;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox43.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox43.Location = new System.Drawing.Point(34, 64);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(40, 25);
            this.textBox43.TabIndex = 5;
            this.textBox43.Text = "2";
            this.textBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.Window;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox42.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox42.Location = new System.Drawing.Point(5, 64);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(30, 25);
            this.textBox42.TabIndex = 4;
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Window;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox39.Location = new System.Drawing.Point(73, 40);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(230, 25);
            this.textBox39.TabIndex = 2;
            this.textBox39.Text = "上海卓亚矿山机械有限公司";
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Window;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox36.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox36.Location = new System.Drawing.Point(34, 40);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(40, 25);
            this.textBox36.TabIndex = 1;
            this.textBox36.Text = "1";
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.SystemColors.Window;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label49.Location = new System.Drawing.Point(73, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(230, 25);
            this.label49.TabIndex = 86;
            this.label49.Text = "公司名称";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.SystemColors.Window;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label48.Location = new System.Drawing.Point(34, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(40, 25);
            this.label48.TabIndex = 86;
            this.label48.Text = "序号";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.Window;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(5, 16);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(30, 25);
            this.label47.TabIndex = 86;
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Window;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox33.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox33.Location = new System.Drawing.Point(5, 40);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(30, 25);
            this.textBox33.TabIndex = 0;
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Window;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox32.Location = new System.Drawing.Point(302, 64);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(60, 25);
            this.textBox32.TabIndex = 88;
            this.textBox32.Text = "李四";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Window;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox29.Location = new System.Drawing.Point(302, 40);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(60, 25);
            this.textBox29.TabIndex = 87;
            this.textBox29.Text = "张三";
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.SystemColors.Window;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label46.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label46.Location = new System.Drawing.Point(302, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(60, 25);
            this.label46.TabIndex = 93;
            this.label46.Text = "联系人";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Window;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox28.Location = new System.Drawing.Point(361, 64);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(40, 25);
            this.textBox28.TabIndex = 95;
            this.textBox28.Text = "CN";
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Window;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox25.Location = new System.Drawing.Point(361, 40);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(40, 25);
            this.textBox25.TabIndex = 94;
            this.textBox25.Text = "CN";
            this.textBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.SystemColors.Window;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label45.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label45.Location = new System.Drawing.Point(361, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 25);
            this.label45.TabIndex = 100;
            this.label45.Text = "国家";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Window;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox22.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox22.Location = new System.Drawing.Point(400, 64);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(65, 25);
            this.textBox22.TabIndex = 102;
            this.textBox22.Text = "1278";
            this.textBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.Window;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox13.Location = new System.Drawing.Point(400, 40);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(65, 25);
            this.textBox13.TabIndex = 101;
            this.textBox13.Text = "1234";
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.SystemColors.Window;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label44.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(400, 16);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(65, 25);
            this.label44.TabIndex = 107;
            this.label44.Text = "公司标识";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Window;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox10.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox10.Location = new System.Drawing.Point(464, 64);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(80, 25);
            this.textBox10.TabIndex = 109;
            this.textBox10.Text = "10000021";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(464, 40);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(80, 25);
            this.textBox1.TabIndex = 108;
            this.textBox1.Text = "10000987";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.SystemColors.Window;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label37.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(464, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(80, 25);
            this.label37.TabIndex = 114;
            this.label37.Text = "投标人标识";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox9.Location = new System.Drawing.Point(13, 70);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 116;
            this.checkBox9.UseVisualStyleBackColor = false;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox6.Location = new System.Drawing.Point(13, 45);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 121;
            this.checkBox6.UseVisualStyleBackColor = false;
            // 
            // textBox138
            // 
            this.textBox138.BackColor = System.Drawing.SystemColors.Window;
            this.textBox138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox138.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox138.Location = new System.Drawing.Point(543, 64);
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new System.Drawing.Size(80, 25);
            this.textBox138.TabIndex = 123;
            this.textBox138.Text = "20003574";
            this.textBox138.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox135
            // 
            this.textBox135.BackColor = System.Drawing.SystemColors.Window;
            this.textBox135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox135.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox135.Location = new System.Drawing.Point(543, 40);
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new System.Drawing.Size(80, 25);
            this.textBox135.TabIndex = 122;
            this.textBox135.Text = "100059823";
            this.textBox135.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Window;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(543, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 25);
            this.label6.TabIndex = 126;
            this.label6.Text = "RFx编号";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox134
            // 
            this.textBox134.BackColor = System.Drawing.SystemColors.Window;
            this.textBox134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox134.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox134.Location = new System.Drawing.Point(622, 64);
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new System.Drawing.Size(65, 25);
            this.textBox134.TabIndex = 128;
            this.textBox134.Text = "处理中";
            this.textBox134.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox131
            // 
            this.textBox131.BackColor = System.Drawing.SystemColors.Window;
            this.textBox131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox131.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox131.Location = new System.Drawing.Point(622, 40);
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new System.Drawing.Size(65, 25);
            this.textBox131.TabIndex = 127;
            this.textBox131.Text = "处理中";
            this.textBox131.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Window;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(622, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 25);
            this.label5.TabIndex = 131;
            this.label5.Text = "状态";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox130
            // 
            this.textBox130.BackColor = System.Drawing.SystemColors.Window;
            this.textBox130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox130.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox130.Location = new System.Drawing.Point(686, 64);
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new System.Drawing.Size(80, 25);
            this.textBox130.TabIndex = 133;
            this.textBox130.Text = "2015/01/03";
            this.textBox130.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox127
            // 
            this.textBox127.BackColor = System.Drawing.SystemColors.Window;
            this.textBox127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox127.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox127.Location = new System.Drawing.Point(686, 40);
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new System.Drawing.Size(80, 25);
            this.textBox127.TabIndex = 132;
            this.textBox127.Text = "2015/01/02";
            this.textBox127.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Window;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(686, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 25);
            this.label4.TabIndex = 136;
            this.label4.Text = "提交日期";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox126
            // 
            this.textBox126.BackColor = System.Drawing.SystemColors.Window;
            this.textBox126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox126.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox126.Location = new System.Drawing.Point(765, 64);
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new System.Drawing.Size(90, 25);
            this.textBox126.TabIndex = 138;
            this.textBox126.Text = "20";
            this.textBox126.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox123
            // 
            this.textBox123.BackColor = System.Drawing.SystemColors.Window;
            this.textBox123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox123.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox123.Location = new System.Drawing.Point(765, 40);
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new System.Drawing.Size(90, 25);
            this.textBox123.TabIndex = 137;
            this.textBox123.Text = "12";
            this.textBox123.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Window;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(765, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 25);
            this.label3.TabIndex = 141;
            this.label3.Text = "剩余时间(天)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(483, 28);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 220;
            this.label1.Text = "状态";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(247, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 222;
            this.label2.Text = "RFx编号";
            // 
            // gsbz_tb
            // 
            this.gsbz_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gsbz_tb.Location = new System.Drawing.Point(76, 24);
            this.gsbz_tb.Name = "gsbz_tb";
            this.gsbz_tb.Size = new System.Drawing.Size(150, 23);
            this.gsbz_tb.TabIndex = 67;
            // 
            // zt_tb
            // 
            this.zt_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zt_tb.Location = new System.Drawing.Point(518, 24);
            this.zt_tb.Name = "zt_tb";
            this.zt_tb.Size = new System.Drawing.Size(150, 23);
            this.zt_tb.TabIndex = 221;
            // 
            // RFxbh_tb
            // 
            this.RFxbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RFxbh_tb.Location = new System.Drawing.Point(297, 24);
            this.RFxbh_tb.Name = "RFxbh_tb";
            this.RFxbh_tb.Size = new System.Drawing.Size(150, 23);
            this.RFxbh_tb.TabIndex = 223;
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "确定采购价格";
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(20, 74);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(911, 1816);
            this.tabControl1.TabIndex = 89;
            // 
            // close_bt
            // 
            this.close_bt.Location = new System.Drawing.Point(16, 5);
            this.close_bt.Name = "close_bt";
            this.close_bt.Size = new System.Drawing.Size(60, 30);
            this.close_bt.TabIndex = 75;
            this.close_bt.Text = "关闭";
            this.close_bt.UseVisualStyleBackColor = true;
            // 
            // save_bt
            // 
            this.save_bt.Location = new System.Drawing.Point(96, 5);
            this.save_bt.Name = "save_bt";
            this.save_bt.Size = new System.Drawing.Size(60, 30);
            this.save_bt.TabIndex = 76;
            this.save_bt.Text = "保存";
            this.save_bt.UseVisualStyleBackColor = true;
            // 
            // send_bt
            // 
            this.send_bt.Location = new System.Drawing.Point(255, 5);
            this.send_bt.Name = "send_bt";
            this.send_bt.Size = new System.Drawing.Size(80, 30);
            this.send_bt.TabIndex = 78;
            this.send_bt.Text = "发送通知";
            this.send_bt.UseVisualStyleBackColor = true;
            // 
            // detail_bt
            // 
            this.detail_bt.Location = new System.Drawing.Point(175, 5);
            this.detail_bt.Name = "detail_bt";
            this.detail_bt.Size = new System.Drawing.Size(60, 30);
            this.detail_bt.TabIndex = 79;
            this.detail_bt.Text = "明细";
            this.detail_bt.UseVisualStyleBackColor = true;
            // 
            // refresh_bt
            // 
            this.refresh_bt.Location = new System.Drawing.Point(355, 5);
            this.refresh_bt.Name = "refresh_bt";
            this.refresh_bt.Size = new System.Drawing.Size(60, 30);
            this.refresh_bt.TabIndex = 80;
            this.refresh_bt.Text = "刷新";
            this.refresh_bt.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.refresh_bt);
            this.panel1.Controls.Add(this.detail_bt);
            this.panel1.Controls.Add(this.send_bt);
            this.panel1.Controls.Add(this.save_bt);
            this.panel1.Controls.Add(this.close_bt);
            this.panel1.Location = new System.Drawing.Point(3, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 42);
            this.panel1.TabIndex = 88;
            // 
            // BiddingInformation_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(960, 588);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "BiddingInformation_Form";
            this.Text = "投标信息";
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.sjbg_panel.ResumeLayout(false);
            this.sjbg_panel.PerformLayout();
            this.zdjgf_panel.ResumeLayout(false);
            this.zdjgf_panel.PerformLayout();
            this.zdsyqcbf_panel.ResumeLayout(false);
            this.zdsyqcbf_panel.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel zdsyqcbf_panel;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.TextBox textBox121;
        private System.Windows.Forms.TextBox textBox122;
        private System.Windows.Forms.TextBox textBox167;
        private System.Windows.Forms.TextBox textBox168;
        private System.Windows.Forms.TextBox textBox169;
        private System.Windows.Forms.TextBox textBox170;
        private System.Windows.Forms.TextBox textBox184;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.TextBox textBox174;
        private System.Windows.Forms.TextBox textBox175;
        private System.Windows.Forms.TextBox textBox178;
        private System.Windows.Forms.TextBox textBox179;
        private System.Windows.Forms.TextBox textBox180;
        private System.Windows.Forms.TextBox textBox181;
        private System.Windows.Forms.TextBox textBox182;
        private System.Windows.Forms.TextBox textBox183;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox136;
        private System.Windows.Forms.TextBox textBox137;
        private System.Windows.Forms.TextBox textBox161;
        private System.Windows.Forms.TextBox textBox162;
        private System.Windows.Forms.TextBox textBox163;
        private System.Windows.Forms.TextBox textBox166;
        private System.Windows.Forms.TextBox textBox171;
        private System.Windows.Forms.TextBox textBox172;
        private System.Windows.Forms.TextBox textBox173;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.TextBox textBox111;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox129;
        private System.Windows.Forms.TextBox textBox193;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox201;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox160;
        private System.Windows.Forms.TextBox textBox164;
        private System.Windows.Forms.TextBox textBox165;
        private System.Windows.Forms.TextBox textBox132;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox112;
        private System.Windows.Forms.TextBox textBox124;
        private System.Windows.Forms.TextBox textBox125;
        private System.Windows.Forms.TextBox textBox128;
        private System.Windows.Forms.TextBox textBox133;
        private System.Windows.Forms.TextBox textBox176;
        private System.Windows.Forms.TextBox textBox177;
        private System.Windows.Forms.TextBox textBox187;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox hlab_ed;
        private System.Windows.Forms.TextBox textBox190;
        private System.Windows.Forms.TextBox hlab_zx;
        private System.Windows.Forms.TextBox textBox191;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label shp2_lbl;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label zd_lbl;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label zx_lbl;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox zbs_tb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel zdjgf_panel;
        private System.Windows.Forms.TextBox textBox232;
        private System.Windows.Forms.TextBox textBox234;
        private System.Windows.Forms.TextBox textBox235;
        private System.Windows.Forms.TextBox textBox237;
        private System.Windows.Forms.TextBox textBox238;
        private System.Windows.Forms.TextBox textBox240;
        private System.Windows.Forms.TextBox textBox241;
        private System.Windows.Forms.TextBox textBox243;
        private System.Windows.Forms.TextBox textBox244;
        private System.Windows.Forms.TextBox textBox245;
        private System.Windows.Forms.TextBox textBox246;
        private System.Windows.Forms.TextBox textBox247;
        private System.Windows.Forms.TextBox textBox248;
        private System.Windows.Forms.TextBox textBox249;
        private System.Windows.Forms.TextBox textBox250;
        private System.Windows.Forms.TextBox textBox251;
        private System.Windows.Forms.TextBox textBox252;
        private System.Windows.Forms.TextBox textBox253;
        private System.Windows.Forms.TextBox textBox254;
        private System.Windows.Forms.TextBox textBox256;
        private System.Windows.Forms.TextBox textBox257;
        private System.Windows.Forms.TextBox textBox259;
        private System.Windows.Forms.TextBox textBox261;
        private System.Windows.Forms.TextBox textBox262;
        private System.Windows.Forms.TextBox textBox263;
        private System.Windows.Forms.TextBox textBox264;
        private System.Windows.Forms.TextBox textBox265;
        private System.Windows.Forms.TextBox textBox266;
        private System.Windows.Forms.TextBox textBox267;
        private System.Windows.Forms.TextBox textBox268;
        private System.Windows.Forms.TextBox textBox269;
        private System.Windows.Forms.TextBox textBox270;
        private System.Windows.Forms.TextBox textBox271;
        private System.Windows.Forms.TextBox textBox272;
        private System.Windows.Forms.TextBox textBox273;
        private System.Windows.Forms.TextBox textBox274;
        private System.Windows.Forms.TextBox textBox275;
        private System.Windows.Forms.TextBox textBox276;
        private System.Windows.Forms.TextBox textBox277;
        private System.Windows.Forms.TextBox textBox278;
        private System.Windows.Forms.TextBox textBox199;
        private System.Windows.Forms.TextBox textBox203;
        private System.Windows.Forms.TextBox textBox205;
        private System.Windows.Forms.TextBox textBox207;
        private System.Windows.Forms.TextBox textBox208;
        private System.Windows.Forms.TextBox textBox209;
        private System.Windows.Forms.TextBox textBox210;
        private System.Windows.Forms.TextBox textBox211;
        private System.Windows.Forms.TextBox textBox212;
        private System.Windows.Forms.TextBox textBox213;
        private System.Windows.Forms.TextBox textBox214;
        private System.Windows.Forms.TextBox textBox215;
        private System.Windows.Forms.TextBox textBox216;
        private System.Windows.Forms.TextBox textBox217;
        private System.Windows.Forms.TextBox textBox218;
        private System.Windows.Forms.TextBox textBox219;
        private System.Windows.Forms.TextBox textBox221;
        private System.Windows.Forms.TextBox textBox223;
        private System.Windows.Forms.TextBox textBox225;
        private System.Windows.Forms.TextBox textBox227;
        private System.Windows.Forms.TextBox textBox228;
        private System.Windows.Forms.TextBox textBox229;
        private System.Windows.Forms.TextBox textBox230;
        private System.Windows.Forms.TextBox textBox231;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox185;
        private System.Windows.Forms.TextBox textBox188;
        private System.Windows.Forms.TextBox textBox189;
        private System.Windows.Forms.TextBox textBox192;
        private System.Windows.Forms.TextBox textBox194;
        private System.Windows.Forms.TextBox textBox195;
        private System.Windows.Forms.TextBox textBox196;
        private System.Windows.Forms.TextBox textBox197;
        private System.Windows.Forms.TextBox textBox198;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox255;
        private System.Windows.Forms.TextBox textBox258;
        private System.Windows.Forms.TextBox textBox260;
        private System.Windows.Forms.TextBox textBox200;
        private System.Windows.Forms.TextBox textBox202;
        private System.Windows.Forms.TextBox textBox204;
        private System.Windows.Forms.TextBox textBox206;
        private System.Windows.Forms.TextBox textBox220;
        private System.Windows.Forms.TextBox textBox222;
        private System.Windows.Forms.TextBox textBox224;
        private System.Windows.Forms.TextBox textBox226;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox186;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox159;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel sjbg_panel;
        private System.Windows.Forms.TextBox textBox419;
        private System.Windows.Forms.TextBox textBox420;
        private System.Windows.Forms.TextBox textBox421;
        private System.Windows.Forms.TextBox textBox422;
        private System.Windows.Forms.TextBox textBox423;
        private System.Windows.Forms.TextBox textBox424;
        private System.Windows.Forms.TextBox textBox425;
        private System.Windows.Forms.TextBox textBox426;
        private System.Windows.Forms.TextBox textBox427;
        private System.Windows.Forms.TextBox textBox428;
        private System.Windows.Forms.TextBox textBox429;
        private System.Windows.Forms.TextBox textBox430;
        private System.Windows.Forms.TextBox textBox431;
        private System.Windows.Forms.TextBox textBox432;
        private System.Windows.Forms.TextBox textBox433;
        private System.Windows.Forms.TextBox textBox434;
        private System.Windows.Forms.TextBox textBox435;
        private System.Windows.Forms.TextBox textBox436;
        private System.Windows.Forms.TextBox textBox437;
        private System.Windows.Forms.TextBox textBox438;
        private System.Windows.Forms.TextBox textBox439;
        private System.Windows.Forms.TextBox textBox440;
        private System.Windows.Forms.TextBox textBox441;
        private System.Windows.Forms.TextBox textBox442;
        private System.Windows.Forms.TextBox textBox443;
        private System.Windows.Forms.TextBox textBox444;
        private System.Windows.Forms.TextBox textBox445;
        private System.Windows.Forms.TextBox textBox446;
        private System.Windows.Forms.TextBox textBox447;
        private System.Windows.Forms.TextBox textBox448;
        private System.Windows.Forms.TextBox textBox449;
        private System.Windows.Forms.TextBox textBox450;
        private System.Windows.Forms.TextBox textBox451;
        private System.Windows.Forms.TextBox textBox452;
        private System.Windows.Forms.TextBox textBox453;
        private System.Windows.Forms.TextBox textBox454;
        private System.Windows.Forms.TextBox textBox455;
        private System.Windows.Forms.TextBox textBox456;
        private System.Windows.Forms.TextBox textBox457;
        private System.Windows.Forms.TextBox textBox458;
        private System.Windows.Forms.TextBox textBox459;
        private System.Windows.Forms.TextBox textBox460;
        private System.Windows.Forms.TextBox textBox461;
        private System.Windows.Forms.TextBox textBox462;
        private System.Windows.Forms.TextBox textBox463;
        private System.Windows.Forms.TextBox textBox464;
        private System.Windows.Forms.TextBox textBox465;
        private System.Windows.Forms.TextBox textBox466;
        private System.Windows.Forms.TextBox textBox467;
        private System.Windows.Forms.TextBox textBox468;
        private System.Windows.Forms.TextBox textBox469;
        private System.Windows.Forms.TextBox textBox470;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox471;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox472;
        private System.Windows.Forms.TextBox textBox473;
        private System.Windows.Forms.TextBox textBox474;
        private System.Windows.Forms.TextBox textBox475;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox476;
        private System.Windows.Forms.TextBox textBox477;
        private System.Windows.Forms.TextBox textBox478;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox349;
        private System.Windows.Forms.TextBox textBox350;
        private System.Windows.Forms.TextBox textBox351;
        private System.Windows.Forms.TextBox textBox368;
        private System.Windows.Forms.TextBox textBox369;
        private System.Windows.Forms.TextBox textBox391;
        private System.Windows.Forms.TextBox textBox392;
        private System.Windows.Forms.TextBox textBox393;
        private System.Windows.Forms.TextBox textBox409;
        private System.Windows.Forms.TextBox textBox410;
        private System.Windows.Forms.TextBox textBox411;
        private System.Windows.Forms.TextBox textBox418;
        private System.Windows.Forms.TextBox textBox334;
        private System.Windows.Forms.TextBox textBox335;
        private System.Windows.Forms.TextBox textBox336;
        private System.Windows.Forms.TextBox textBox353;
        private System.Windows.Forms.TextBox textBox354;
        private System.Windows.Forms.TextBox textBox376;
        private System.Windows.Forms.TextBox textBox377;
        private System.Windows.Forms.TextBox textBox378;
        private System.Windows.Forms.TextBox textBox394;
        private System.Windows.Forms.TextBox textBox395;
        private System.Windows.Forms.TextBox textBox396;
        private System.Windows.Forms.TextBox textBox413;
        private System.Windows.Forms.TextBox textBox328;
        private System.Windows.Forms.TextBox textBox329;
        private System.Windows.Forms.TextBox textBox330;
        private System.Windows.Forms.TextBox textBox332;
        private System.Windows.Forms.TextBox textBox333;
        private System.Windows.Forms.TextBox textBox370;
        private System.Windows.Forms.TextBox textBox371;
        private System.Windows.Forms.TextBox textBox372;
        private System.Windows.Forms.TextBox textBox373;
        private System.Windows.Forms.TextBox textBox374;
        private System.Windows.Forms.TextBox textBox375;
        private System.Windows.Forms.TextBox textBox412;
        private System.Windows.Forms.TextBox textBox337;
        private System.Windows.Forms.TextBox textBox338;
        private System.Windows.Forms.TextBox textBox339;
        private System.Windows.Forms.TextBox textBox356;
        private System.Windows.Forms.TextBox textBox357;
        private System.Windows.Forms.TextBox textBox379;
        private System.Windows.Forms.TextBox textBox380;
        private System.Windows.Forms.TextBox textBox381;
        private System.Windows.Forms.TextBox textBox397;
        private System.Windows.Forms.TextBox textBox398;
        private System.Windows.Forms.TextBox textBox399;
        private System.Windows.Forms.TextBox textBox414;
        private System.Windows.Forms.TextBox textBox382;
        private System.Windows.Forms.TextBox textBox383;
        private System.Windows.Forms.TextBox textBox384;
        private System.Windows.Forms.TextBox textBox385;
        private System.Windows.Forms.TextBox textBox386;
        private System.Windows.Forms.TextBox textBox387;
        private System.Windows.Forms.TextBox textBox388;
        private System.Windows.Forms.TextBox textBox389;
        private System.Windows.Forms.TextBox textBox390;
        private System.Windows.Forms.TextBox textBox400;
        private System.Windows.Forms.TextBox textBox401;
        private System.Windows.Forms.TextBox textBox402;
        private System.Windows.Forms.TextBox textBox403;
        private System.Windows.Forms.TextBox textBox404;
        private System.Windows.Forms.TextBox textBox405;
        private System.Windows.Forms.TextBox textBox406;
        private System.Windows.Forms.TextBox textBox407;
        private System.Windows.Forms.TextBox textBox408;
        private System.Windows.Forms.TextBox textBox415;
        private System.Windows.Forms.TextBox textBox416;
        private System.Windows.Forms.TextBox textBox417;
        private System.Windows.Forms.TextBox textBox331;
        private System.Windows.Forms.TextBox textBox340;
        private System.Windows.Forms.TextBox textBox341;
        private System.Windows.Forms.TextBox textBox342;
        private System.Windows.Forms.TextBox textBox343;
        private System.Windows.Forms.TextBox textBox344;
        private System.Windows.Forms.TextBox textBox345;
        private System.Windows.Forms.TextBox textBox346;
        private System.Windows.Forms.TextBox textBox347;
        private System.Windows.Forms.TextBox textBox348;
        private System.Windows.Forms.TextBox textBox352;
        private System.Windows.Forms.TextBox textBox355;
        private System.Windows.Forms.TextBox textBox358;
        private System.Windows.Forms.TextBox textBox359;
        private System.Windows.Forms.TextBox textBox360;
        private System.Windows.Forms.TextBox textBox361;
        private System.Windows.Forms.TextBox textBox362;
        private System.Windows.Forms.TextBox textBox363;
        private System.Windows.Forms.TextBox textBox364;
        private System.Windows.Forms.TextBox textBox365;
        private System.Windows.Forms.TextBox textBox366;
        private System.Windows.Forms.TextBox textBox367;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox307;
        private System.Windows.Forms.TextBox textBox308;
        private System.Windows.Forms.TextBox textBox309;
        private System.Windows.Forms.TextBox textBox310;
        private System.Windows.Forms.TextBox textBox311;
        private System.Windows.Forms.TextBox textBox312;
        private System.Windows.Forms.TextBox textBox313;
        private System.Windows.Forms.TextBox textBox314;
        private System.Windows.Forms.TextBox textBox315;
        private System.Windows.Forms.TextBox textBox316;
        private System.Windows.Forms.TextBox textBox317;
        private System.Windows.Forms.TextBox textBox318;
        private System.Windows.Forms.TextBox textBox319;
        private System.Windows.Forms.TextBox textBox320;
        private System.Windows.Forms.TextBox textBox321;
        private System.Windows.Forms.TextBox textBox322;
        private System.Windows.Forms.TextBox textBox323;
        private System.Windows.Forms.TextBox textBox324;
        private System.Windows.Forms.TextBox textBox325;
        private System.Windows.Forms.TextBox textBox326;
        private System.Windows.Forms.TextBox textBox327;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox291;
        private System.Windows.Forms.TextBox textBox292;
        private System.Windows.Forms.TextBox textBox293;
        private System.Windows.Forms.TextBox textBox294;
        private System.Windows.Forms.TextBox textBox295;
        private System.Windows.Forms.TextBox textBox296;
        private System.Windows.Forms.TextBox textBox297;
        private System.Windows.Forms.TextBox textBox298;
        private System.Windows.Forms.TextBox textBox299;
        private System.Windows.Forms.TextBox textBox300;
        private System.Windows.Forms.TextBox textBox301;
        private System.Windows.Forms.TextBox textBox302;
        private System.Windows.Forms.TextBox textBox303;
        private System.Windows.Forms.TextBox textBox304;
        private System.Windows.Forms.TextBox textBox305;
        private System.Windows.Forms.TextBox textBox306;
        private System.Windows.Forms.TextBox textBox233;
        private System.Windows.Forms.TextBox textBox236;
        private System.Windows.Forms.TextBox textBox239;
        private System.Windows.Forms.TextBox textBox242;
        private System.Windows.Forms.TextBox textBox279;
        private System.Windows.Forms.TextBox textBox280;
        private System.Windows.Forms.TextBox textBox281;
        private System.Windows.Forms.TextBox textBox282;
        private System.Windows.Forms.TextBox textBox283;
        private System.Windows.Forms.TextBox textBox284;
        private System.Windows.Forms.TextBox textBox285;
        private System.Windows.Forms.TextBox textBox286;
        private System.Windows.Forms.TextBox textBox287;
        private System.Windows.Forms.TextBox textBox288;
        private System.Windows.Forms.TextBox textBox289;
        private System.Windows.Forms.TextBox textBox290;
        private System.Windows.Forms.TextBox qlca_zd;
        private System.Windows.Forms.TextBox qlca_zx;
        private System.Windows.Forms.TextBox qlca_ed;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox151;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox textBox152;
        private System.Windows.Forms.TextBox textBox153;
        private System.Windows.Forms.TextBox textBox154;
        private System.Windows.Forms.TextBox textBox155;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textBox156;
        private System.Windows.Forms.TextBox textBox157;
        private System.Windows.Forms.TextBox textBox158;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox textBox147;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBox148;
        private System.Windows.Forms.TextBox textBox149;
        private System.Windows.Forms.TextBox textBox150;
        private System.Windows.Forms.TextBox textBox143;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBox144;
        private System.Windows.Forms.TextBox textBox145;
        private System.Windows.Forms.TextBox textBox146;
        private System.Windows.Forms.TextBox textBox139;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox140;
        private System.Windows.Forms.TextBox textBox141;
        private System.Windows.Forms.TextBox textBox142;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox hlca_zd;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label shp3_lbl;
        private System.Windows.Forms.ComboBox pgff_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox RFxbh_tb;
        private System.Windows.Forms.TextBox zt_tb;
        private System.Windows.Forms.TextBox gsbz_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox123;
        private System.Windows.Forms.TextBox textBox126;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox127;
        private System.Windows.Forms.TextBox textBox130;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox131;
        private System.Windows.Forms.TextBox textBox134;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox135;
        private System.Windows.Forms.TextBox textBox138;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button close_bt;
        private System.Windows.Forms.Button save_bt;
        private System.Windows.Forms.Button send_bt;
        private System.Windows.Forms.Button detail_bt;
        private System.Windows.Forms.Button refresh_bt;
        private System.Windows.Forms.Panel panel1;

    }
}