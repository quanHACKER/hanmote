﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
//using LumiSoft.Net.POP3.Client;
//using LumiSoft.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.MD.SP;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.MDBll.General;
using MMClient.SourcingManage;
using MMClient.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Bll.SourcingManage.SourceingRecord;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Inquiry_Form : DockContent
    { 
        //询价单表头
        SourceInquiry inquiry_table = new SourceInquiry();
        string sourceId = null;
        string sourcePath = null;
        //询价单
        List<Enquiry> enquiryList = new List<Enquiry>();
        List<Summary_Demand> demandList = new List<Summary_Demand>();
        List<Demand_Material> xjxList = new List<Demand_Material>();
        List<SupplierBase> gysList = new List<SupplierBase>();


        //查询工具
        EnquiryBLL enquiryBLL = new EnquiryBLL();
        GeneralBLL generalBll = new GeneralBLL();
        SourceBLL sourceBll = new SourceBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();

        SourceInquiryBLL sourceInquiryBll = new SourceInquiryBLL();
        PurchaseOrgBLL purchaseOrgBLL = new PurchaseOrgBLL();
        PurchaseGroupBLL purchaseGroupBLL = new PurchaseGroupBLL();
        Summary_Demand summaryDemand = null;

        //控件工具
        ConvenientTools tools = new ConvenientTools();
        //物料及供应商编号itemslist
        List<string> wlbhItems = null;
        List<string> gysbhItems = null;
        List<string> xjdhItems = null;
        //物料及供应商名称itemslist
        List<string> wlmcItems = null;
        List<string> gysmcItems = null;
        List<string> gcItems = null;
        List<string> ckItems = null;

        public Inquiry_Form() 
        {
            InitializeComponent();
            Init(Supplier_view);
            Init(Inquiry_view);
            initPurchase();
            tools.addItemsToCMB(cmb_purchaseOrg, "", "", "Department", "Buyer");
            tools.addItemsToCMB(cmb_purchaseGroup, "", "", "Buyer_Name", "Buyer");
        }

        /// <summary>
        /// 传入需要寻源的需求计划
        /// </summary>
        /// <param name="demandMaterials"></param>
        public Inquiry_Form(List<Demand_Material> demandMaterials,Summary_Demand summaryDemand)
        {
            InitializeComponent();
            Init(Supplier_view);
            Init(Inquiry_view);
            initPurchase();
            this.summaryDemand = summaryDemand;
            txt_state.Text = "待维护";
            txt_inquiryId.Text = "M" + tools.systemTimeToStr();
            sourceId = txt_inquiryId.Text;
            xjxList = demandMaterials;
            //xjdhItems = tools.addItemsStringList("", "", "Inquiry_ID", "Inquiry_Table");
            //tools.addItemsToCMB(xjdh_cmb, "", "", "Inquiry_ID", "Inquiry_Table");
            //tools.addItemsToCMB(cgzz_cmb, "", "", "Department", "Buyer");
            //tools.addItemsToCMB(cgz_cmb, "", "", "Buyer_Name", "Buyer");

            addItems(demandMaterials);
        }

        private void initPurchase()
        {
            List<PurchaseOrg> list = purchaseOrgBLL.getAllPurchaseOrg();
            for (int i = 0; i < list.Count; i++)
            {
                cmb_purchaseOrg.Items.Add(list.ElementAt(i).PurchaseOrgName);
            }
            List<PurchaseGroup> listGroup = purchaseGroupBLL.getAllPurchaseGroup();
            for (int i = 0; i < listGroup.Count; i++)
            {
                cmb_purchaseGroup.Items.Add(listGroup.ElementAt(i).PurchaseGroupName);
            }
        }

        /// <summary>
        /// 添加表格元素项
        /// </summary>
        /// <param name="demand"></param>
        private void addItems(List<Demand_Material> demand)
        {
            
            Inquiry_view.Rows.Clear();
            if(demand.Count>Inquiry_view.Rows.Count)
            {
                Inquiry_view.Rows.Add(demand.Count - Inquiry_view.Rows.Count);
            }
            for (int i = 0; i < demand.Count; i++)
            {
                Inquiry_view.Rows[i].Cells["materialId"].Value = demand.ElementAt(i).Material_ID;
                Inquiry_view.Rows[i].Cells["materialName"].Value = demand.ElementAt(i).Material_Name;
                Inquiry_view.Rows[i].Cells["materialGroup"].Value = demand.ElementAt(i).Material_Group;
                Inquiry_view.Rows[i].Cells["count"].Value = demand.ElementAt(i).Demand_Count;
                Inquiry_view.Rows[i].Cells["measurement"].Value = demand.ElementAt(i).Measurement;
                Inquiry_view.Rows[i].Cells["factoryId"].Value = summaryDemand.Factory_ID;
                Inquiry_view.Rows[i].Cells["stockId"].Value = demand.ElementAt(i).Stock_ID;
                Inquiry_view.Rows[i].Cells["DeliveryStartTime"].Value = demand.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                Inquiry_view.Rows[i].Cells["DeliveryEndTime"].Value = demand.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
                //CellEndEdit(Inquiry_view, i, 1);
                //Inquiry_view.Rows[i].Cells[6].Value = "";//交货日期
                //Inquiry_view.Rows[i].Cells[8].Value = demand.ElementAt(i).Factory_ID;
                //Inquiry_view.Rows[i].Cells[9].Value = demand.ElementAt(i).Stock_ID;
            }
        }

        private void Init(DataGridView view)
        {
            //DataGridViewDataSource_Load(view);
        }

        /// <summary>
        ///datagridview绑定数据源
        /// </summary>
        private void DataGridViewDataSource_Load(DataGridView view)
        {  
            wlbhItems = tools.addItemsStringList("", "", "Material_ID", "Material_Type");

            wlmcItems = tools.addItemsStringList("", "", "Material_Name", "Material_Type");

            gysbhItems = tools.addItemsStringList("", "", "Supplier_ID", "Supplier_Base");

            gysmcItems = tools.addItemsStringList("", "", "Supplier_Name", "Supplier_Base");

            gcItems = tools.addItemsStringList("", "", "Factory_ID", "Inventory");

            ckItems = tools.addItemsStringList("", "", "Stock_ID", "Inventory");
        }

        /// <summary>
        /// 合并单元格重绘表格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void view_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            if (e.RowIndex == 1 && e.ColumnIndex == 1)
            {

                Brush backColorBrush = new SolidBrush(e.CellStyle.BackColor);

                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

                //绘制背景色(被选中状态下)  

                if (e.State == (DataGridViewElementStates.Displayed | DataGridViewElementStates.Selected | DataGridViewElementStates.Visible))

                    e.PaintBackground(e.CellBounds, false);

                //分别绘制原文本和现在改变颜色的文本  

                Brush fontColor = new SolidBrush(e.CellStyle.ForeColor);

                // e.Graphics.DrawString("", this.Font, fontColor, e.CellBounds, StringFormat.GenericDefault);

                //绘制下边框线

                Brush gridBrush = new SolidBrush(this.Supplier_view.GridColor);

                Pen pen = new Pen(gridBrush);

                e.Graphics.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Bottom - 1,

                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);

                DataGridViewCell cell = this.Supplier_view.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.Value = "";

                e.Handled = true;

            }

            if (e.RowIndex == 1 && e.ColumnIndex == 2)
            {

                Brush backColorBrush = new SolidBrush(e.CellStyle.BackColor);

                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

                //绘制背景色(被选中状态下)  

                if (e.State == (DataGridViewElementStates.Displayed | DataGridViewElementStates.Selected | DataGridViewElementStates.Visible))

                    e.PaintBackground(e.CellBounds, true);

                //分别绘制原文本和现在改变颜色的文本  

                Brush fontColor = new SolidBrush(e.CellStyle.ForeColor);

                // e.Graphics.DrawString("", this.Font, fontColor, e.CellBounds, StringFormat.GenericDefault);

                //绘制下边框线

                Brush gridBrush = new SolidBrush(this.Supplier_view.GridColor);

                Pen pen = new Pen(gridBrush);

                e.Graphics.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Bottom - 1,

                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);

                //绘制右边框线

                e.Graphics.DrawLine(pen, e.CellBounds.Right - 1,

                e.CellBounds.Top, e.CellBounds.Right - 1,

                e.CellBounds.Bottom - 1);

                DataGridViewCell cell = this.Supplier_view.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.Value = "";

                cell.Tag = "ccccc";

                Rectangle rectanle = e.CellBounds;

                rectanle.X = rectanle.X - 15;

                rectanle.Y = rectanle.Y + 5;

                //这里需要注意的是我没有用 e.Graphics 原因是这个只能在当前单元格绘制

                //而我是在DataGridView上面建一个绘图Graphics对象这样就可以看上去在两个单元格里面绘制了

                //这里你们也可以自己试一试

                Graphics graphics = this.Supplier_view.CreateGraphics();

                //分别绘制原文本和现在改变颜色的文本  

                graphics.DrawString("cccc", this.Font, new SolidBrush(e.CellStyle.ForeColor), rectanle, StringFormat.GenericDefault);

                e.Handled = true;

            }

        }


        /// <summary>
        /// 编辑单元格添加控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void view_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Control[] CS = null;
            Button B = null;
            DateTimePicker T = null;
            ComboBox CMB = null;
            #region 物料/供应商编号添加详细按钮
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("__BUTTON__", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 1)
                {
                    B = new Button();
                    B.Name = "__BUTTON__";
                    B.BackColor = SystemColors.Control;
                    B.Text = "详";
                    B.ForeColor = Color.CornflowerBlue;
                    B.Width = 22;
                    B.Height = 22;
                    B.Parent = Inquiry_view.EditingPanel;
                    B.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    B.Click += new EventHandler(B_Click);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("__BUTTON__", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 1)
                //if (view.CurrentCell.ColumnIndex == 0) 
                {
                    B = new Button();
                    B.Name = "__BUTTON__";
                    B.BackColor = SystemColors.Control;
                    B.Text = "详";
                    B.ForeColor = Color.CornflowerBlue;
                    B.Width = 22;
                    B.Height = 22;
                    B.Parent = Supplier_view.EditingPanel;
                    B.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    B.Click += new EventHandler(B_Click);
                }
            }
            #endregion

            #region 物料/供应商编号添加Combobox
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("__combobox__", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 1)
                {
                    CMB = new ComboBox();
                    CMB.Name = "__combobox__";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < wlbhItems.Count; n++)
                    {
                        CMB.Items.Add(wlbhItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("__combobox__", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 1)
                {
                    CMB = new ComboBox();
                    CMB.Name = "__combobox__";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gysbhItems.Count; n++)
                    {
                        CMB.Items.Add(gysbhItems.ElementAt(n));
                    }
                    if (Supplier_view.CurrentCell.Value != null && !Supplier_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Supplier_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Supplier_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion

            #region 物料/供应商名称添加Combobox
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("_combobox_", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 2)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_combobox_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < wlmcItems.Count; n++)
                    {
                        CMB.Items.Add(wlmcItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("_combobox_", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 2)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_combobox_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gysmcItems.Count; n++)
                    {
                        CMB.Items.Add(gysmcItems.ElementAt(n));
                    }
                    if (Supplier_view.CurrentCell.Value != null && !Supplier_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Supplier_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Supplier_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion

            #region 交货日期添加日期控件

            CS = Inquiry_view.EditingPanel.Controls.Find("__DATETIME__", true);
            if (CS.Length > 0)
                Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
            if (Inquiry_view.CurrentCell.ColumnIndex == 6)
            {
                T = new DateTimePicker();
                T.Name = "__DATETIME__";
                T.BackColor = SystemColors.Window;
                T.Width = e.Control.Width;
                T.Height = e.Control.Height;
                T.Parent = Inquiry_view.EditingPanel;
                T.Dock = DockStyle.Right;
                e.Control.Dock = DockStyle.Fill;
                T.ValueChanged += new EventHandler(T_ValueChanged);
            }
            #endregion

            #region 工厂、仓库添加combobox
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("_factory_", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 8)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_factory_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gcItems.Count; n++)
                    {
                        CMB.Items.Add(gcItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
                CS = Inquiry_view.EditingPanel.Controls.Find("_store_", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 9)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_store_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < ckItems.Count; n++)
                    {
                        CMB.Items.Add(ckItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion
        }


        /// <summary>
        /// 编号列详细按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void B_Click(object sender, EventArgs e)
        {
            if (Supplier_view.EditingControl!=null)
                MessageBox.Show(Supplier_view.EditingControl.Text);
            if(Inquiry_view.EditingControl!=null)
                MessageBox.Show(Inquiry_view.EditingControl.Text);
        }

        /// <summary>
        /// 交货日期列时间事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void T_ValueChanged(object sender, EventArgs e)
        {
            DateTimePicker time = sender as DateTimePicker;
            if (Inquiry_view.EditingControl != null)
            {
                if (tools.dateDiff(time.Value, dte_OfferTime.Value).Days >= 1 && time.Value.CompareTo(dte_OfferTime.Value) > 0)
                {
                    Inquiry_view.CurrentCell.Value = time.Value.ToString();
                }
                else
                    MessageBox.Show("交货日期必须晚于报价期限至少一天！");
            }
        }
        
        /// <summary>
        /// COMBOBOX值改变触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CMB_TextChanged(object sender, EventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            string str = cmb.Text.ToString().Trim();
            if (str.Equals(""))
                return;
            if (Inquiry_view.EditingControl != null)
            {
                if (cmb.Name.Equals("_combobox_") && wlmcItems.Contains(str))
                {
                    Inquiry_view.CurrentCell.Value = str;
                }
                else if (cmb.Name.Equals("__combobox__") && wlbhItems.Contains(str))
                {
                    for (int i = 0; i < Inquiry_view.Rows.Count; i++)
                    {
                        if (Inquiry_view.Rows[i].Cells[1].Value == null)
                            break;
                        if (Inquiry_view.Rows[i].Cells[1].Value.Equals(str) && i != Inquiry_view.CurrentCell.RowIndex)
                        {
                            Inquiry_view.CurrentCell.Value = "";
                            MessageBox.Show("物料编号：" + str + "已重复，请重新选择！");
                            return;
                        }
                    }
                    Inquiry_view.CurrentCell.Value = str;
                    ReadDataByID(Inquiry_view, str, Inquiry_view.CurrentCell.RowIndex);
                }
                else if (cmb.Name.Equals("_factory_") && gcItems.Contains(str))
                {
                    Inquiry_view.CurrentCell.Value = str;
                    ckItems = tools.addItemsStringList(str, "Factory_ID", "Stock_ID", "Inventory");
                    if(ckItems.Count>0)
                        Inquiry_view.Rows[Inquiry_view.CurrentCell.RowIndex].Cells[9].Value = ckItems.ElementAt(0);
                    else
                        Inquiry_view.Rows[Inquiry_view.CurrentCell.RowIndex].Cells[9].Value = "";
                }
                else if (cmb.Name.Equals("_store_") && ckItems.Contains(str))
                {
                    Inquiry_view.CurrentCell.Value = str;
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                if (cmb.Name.Equals("_combobox_") && gysmcItems.Contains(str))
                {
                    Supplier_view.CurrentCell.Value = str;
                }
                else if (cmb.Name.Equals("__combobox__") && gysbhItems.Contains(str))
                {
                    for (int i = 0; i < Supplier_view.Rows.Count; i++)
                    {
                        if (Supplier_view.Rows[i].Cells[1].Value == null)
                            break;
                        if (Supplier_view.Rows[i].Cells[1].Value.Equals(str) && i != Supplier_view.CurrentCell.RowIndex)
                        {
                            Supplier_view.CurrentCell.Value = "";
                            MessageBox.Show("供应商编号：" + str + "已重复，请重新选择！");
                            return;
                        }
                    }
                    Supplier_view.CurrentCell.Value = str;
                    ReadDataByID(Supplier_view, str, Supplier_view.CurrentCell.RowIndex);
                }
            } 
        }


        /// <summary>
        /// 参考采购按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckcg_button_Click(object sender, EventArgs e)
        {
            if (cgdh_cmb.Visible == false)
            {
                cgdh_cmb.Visible = true;
                //xjdh_cmb.Text = "A" + tools.systemTimeToStr();
            }
            else
            {
                cgdh_cmb.Visible = false;
                //xjdh_cmb.Text = "M" + tools.systemTimeToStr();
            }
            Summary_Demand_Search summary_Demand_Search = new Summary_Demand_Search();
            summary_Demand_Search.Show();
            //SingletonUserUI.addToUserUI(summary_Demand_Search);
        }


        /// <summary>
        /// DataGridView单元格编辑完成事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellEndEdit(DataGridView view, int RowIndex, int ColumnIndex)
        {
            //物料编号行改变数据处理
            if (ColumnIndex == 1 && view.Rows[RowIndex].Cells[ColumnIndex].Value != null)
            {
                DataGridViewTextBoxCell cmb = view.Rows[RowIndex].Cells[ColumnIndex] as DataGridViewTextBoxCell;
                if (cmb == null || cmb.Value == null)
                    return;
                string str = cmb.Value.ToString().Trim();
                if (view.Name.Equals("Inquiry_view") && !wlbhItems.Contains(str))
                {
                    view.Rows[RowIndex].Cells[ColumnIndex].Value = "";
                    return;
                }
                else if (view.Name.Equals("Supplier_view") && !gysbhItems.Contains(str))
                {
                    view.Rows[RowIndex].Cells[ColumnIndex].Value = ""; ;
                    return;
                }
                if (str.Length > 0)
                {
                    ReadDataByID(view, str, RowIndex);
                }
                return;
            }

            //名称列与itemslist匹配
            if (ColumnIndex == 2)
            {
                DataGridViewTextBoxCell cmb = view.Rows[RowIndex].Cells[ColumnIndex] as DataGridViewTextBoxCell;
                if (cmb == null || cmb.Value == null)
                    return;
                string str = cmb.Value.ToString().Trim();
                if (!wlmcItems.Contains(str))
                {
                    view.CurrentCell.Value = "";
                }
            }

            //数量列只能输入数字
            if (ColumnIndex == 4)
            {
                if (view.Name.Equals("Inquiry_view"))
                {
                    DataGridViewTextBoxCell checkBox = (DataGridViewTextBoxCell)view.Rows[RowIndex].Cells[4];
                    if (checkBox.Value != null)
                        tools.onlyNumber(checkBox);
                    return;
                }
            }

            //工厂仓库信息
            //if (ColumnIndex == 8)
            //{
            //    string str = view.Rows[RowIndex].Cells[ColumnIndex].Value.ToString().Trim();
            //    DataGridViewComboBoxColumn cmbox = view.Columns["Inquiry_view9"] as DataGridViewComboBoxColumn;
            //    List<string> items = null;
            //    if (str != null && !str.Equals(""))
            //    {
            //        items = tools.addItemsStringList(str, "Factory_ID", "Stock_ID", "Inventory");
            //        if (items.Count > 0)
            //        {
            //            cmbox.DataSource = items;
            //            view.Rows[RowIndex].Cells[9].Value = items.ElementAt(0);
            //        }
            //    }
            //    else
            //    {
            //        cmbox.DataSource = items;
            //        view.Rows[RowIndex].Cells[9].Value = "";
            //    }
            //}
        }
        
        private void Inquiry_view_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CellEndEdit(Inquiry_view, e.RowIndex, e.ColumnIndex);
        }
        private void Supplier_view_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CellEndEdit(Supplier_view, e.RowIndex, e.ColumnIndex);
        }

        /// <summary>
        /// DataGridView根据编号读取数据
        /// </summary>
        /// <param name="view">DGV</param>
        /// <param name="str">编号</param>
        /// <param name="row">行号</param>
        private void ReadDataByID(DataGridView view, string str, int row)
        {
            Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
            DataTable dt = new DataTable();
            if (view.Name.Equals("Inquiry_view"))
            {
                //物料类型
                dt = newStandardDAL.FindMaterial(str, "Material_Type");
                view.Rows[row].Cells[2].Value = dt.Rows[0][1].ToString();//物料名称
                view.Rows[row].Cells[3].Value = dt.Rows[0][5].ToString();//物料标准
                view.Rows[row].Cells[5].Value = dt.Rows[0][4].ToString();//单位
                view.Rows[row].Cells[7].Value = dt.Rows[0][2].ToString();//物料组 
            }
            else
            {
                //供应商基本信息
                dt = newStandardDAL.Supplier_Base(1, str, "Supplier_Base");
                view.Rows[row].Cells[2].Value = dt.Rows[0][3].ToString();
                view.Rows[row].Cells[3].Value = dt.Rows[0][17].ToString();
                view.Rows[row].Cells[4].Value = dt.Rows[0][25].ToString();
                view.Rows[row].Cells[5].Value = dt.Rows[0][5].ToString();
                view.Rows[row].Cells[6].Value = dt.Rows[0][6].ToString();
                view.Rows[row].Cells[7].Value = dt.Rows[0][7].ToString();
            }
        }


        // 行选中值改变后的处理事件
        private void CellValueChanged(DataGridView view, object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DataGridViewRow DGVrow = view.Rows[e.RowIndex] as DataGridViewRow;
                DataGridViewCheckBoxCell checkBox = (DataGridViewCheckBoxCell)view.Rows[e.RowIndex].Cells[0];
                if ((bool)checkBox.EditedFormattedValue == true)
                {
                    if (view.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (view.Name.Equals("Inquiry_view"))
                        {
                            MessageBox.Show("第" + e.RowIndex + "行物料编号不能为空！");
                        }
                        else
                        {
                            MessageBox.Show("第" + e.RowIndex + "行供应商编号不能为空！");
                        }
                        checkBox.Value = false;
                        return;
                    }
                    DGVrow.DefaultCellStyle.BackColor = Color.LightSkyBlue;
                }
                else
                {
                    DGVrow.DefaultCellStyle.BackColor = Color.White;
                }
                return;
            }
        }


        #region 保存按钮事件

        private void getSourceMaterials()
        {
        }
        
        private void getSourceSuppliers()
        {

        }
        #endregion


        /// <summary>
        /// 根据寻源编号打开询价单
        /// </summary>
        /// <param name="inquiryTable"></param>
        public void openByXJDH(SourceInquiry inquiryTable)
        {
            inquiry_table = inquiryTable;
            this.txt_inquiryId.Text = inquiryTable.Source_ID;
        }

        public void CreateFromDemand(List<Summary_Demand> demand)
        {
            if (demand.Count <= 0)
                return;
            demandList = demand;
            xjxList.Clear();
            gysList.Clear();
            Enquiry enquiry = new Enquiry();
            CopyDemandToEnquiry(demandList.ElementAt(0));

            MessageBox.Show(demandList.Count.ToString());
            for (int i = 0; i < demandList.Count; i++)
            {
                for (int j = 0; j < xjxList.Count; j++)
                {
                    if (demandList.ElementAt(i).Material_ID.Equals(xjxList.ElementAt(j).Material_ID))
                        break;
                    if (j == xjxList.Count-1)
                    {
                        CopyDemandToEnquiry(demandList.ElementAt(i));
                    }
                }
            }
                MessageBox.Show(xjxList.Count.ToString());
        }
        /// <summary>
        /// 根据采购需求计划创建招标或询价
        /// </summary>
        /// <param name="demand"></param>
        public void CopyDemandToEnquiry(Summary_Demand demand)
        {
            Enquiry enquiry = new Enquiry();
            enquiry.Material_ID = demand.Material_ID;
        }

        public void Export()
        {
            Workbook workbook = new Workbook();
            Worksheet sheet = (Worksheet)workbook.Worksheets[0];//表0
            sheet.Name = "汉默特询价单" + txt_inquiryId.Text.ToString().Trim();
            Cells cells = sheet.Cells;//单元格
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                    cells.SetColumnWidthPixel(i, 105);//设置列宽
                else if (i == 1)
                    cells.SetColumnWidthPixel(i, 115);
                else if (i == 2)
                    cells.SetColumnWidthPixel(i, 90);
                else if (i == 3)
                    cells.SetColumnWidthPixel(i, 65);
                else
                    cells.SetColumnWidthPixel(i, 80);
            }
            for (int j = 0; j < xjxList.Count + gysList.Count + 6; j++)
            {
                cells.SetRowHeightPixel(j, 25);//设置行高 
                if (j == 2)
                {
                    for (int i = 0; i < 10; i++)
                        sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, true));
                }
                else
                {
                    for (int i = 0; i < 10; i++)
                        sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, false));
                }
            }

            sheet.Cells[0, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 2].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 4].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 6].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[xjxList.Count + 4, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[xjxList.Count + 4, 1].SetStyle(tools.SettingCellStyle(workbook, cells, true));

            sheet.Cells[0, 0].PutValue("询价单号");
            sheet.Cells[0, 1].PutValue(inquiry_table.Source_ID);
            sheet.Cells[0, 2].PutValue("采购组织");
            sheet.Cells[0, 3].PutValue(inquiry_table.Transaction_Type);
            sheet.Cells[0, 4].PutValue("报价期限");
            sheet.Cells[0, 5].PutValue(inquiry_table.Offer_Time.ToString().Trim());

            sheet.Cells[2, 0].PutValue("物料编号");
            sheet.Cells[2, 1].PutValue("物料名称");
            sheet.Cells[2, 2].PutValue("物料标准");
            sheet.Cells[2, 3].PutValue("询价数量");
            sheet.Cells[2, 4].PutValue("价格");
            sheet.Cells[2, 5].PutValue("单位");
            sheet.Cells[2, 6].PutValue("交货日期");
            sheet.Cells[2, 7].PutValue("备注1");
            sheet.Cells[2, 8].PutValue("备注2");
            sheet.Cells[2, 9].PutValue("备注3");
            for (int j = 0; j < xjxList.Count; j++)
            {
                sheet.Cells[j + 3, 0].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells["materialId"]));
                sheet.Cells[j + 3, 1].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells["materialName"]));
                sheet.Cells[j + 3, 2].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[3]));
                sheet.Cells[j + 3, 3].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[4]));
                sheet.Cells[j + 3, 4].PutValue("");
                sheet.Cells[j + 3, 5].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[5]));
                sheet.Cells[j + 3, 6].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[6]));
                sheet.Cells[j + 3, 7].PutValue("");
                sheet.Cells[j + 3, 8].PutValue("");
                sheet.Cells[j + 3, 9].PutValue("");
            }
            sheet.Cells[xjxList.Count + 4, 0].PutValue("供应商编号");
            sheet.Cells[xjxList.Count + 4, 1].PutValue("供应商名称");
            for (int j = 0; j < gysList.Count; j++)
            {
                sheet.Cells[j + xjxList.Count + 5, 0].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells["supplierId"]));
                sheet.Cells[j + xjxList.Count + 5, 1].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells["supplierName"]));
            }

            SaveFileDialog frm = new SaveFileDialog();
            frm.RestoreDirectory = true;//保存上次打开的路径
            frm.FileName = inquiry_table.Source_ID;
            frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
            //点了保存按钮进入 
            string localFilePath = null;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                localFilePath = frm.FileName.ToString(); //获得文件路径 
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径
                String FilePath = localFilePath.Substring(0, localFilePath.LastIndexOf("\\"));//获取文件路径，不带文件名 
                //fileNameExt = DateTime.Now.ToString() + fileNameExt;//给文件名前加上时间 
                //saveFileDialog1.FileName.Insert(1,"dameng");//在文件名里加字符 
                //System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();//输出文件
                workbook.Save(@localFilePath);
                MessageBox.Show("文件 " + fileNameExt + " 已成功保存至:" + FilePath, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                localFilePath = "E:\\" + inquiry_table.Source_ID + ".xls";
                workbook.Save(@"E:\\" + inquiry_table.Source_ID + ".xls");
                MessageBox.Show("文件已成功保存至: " + "E:\\", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);  
            }
            inquiry_table.Inquiry_Path = localFilePath;
            txt_state.Text = "待邀请";
            sourcePath = localFilePath;
            int resultState = sourceInquiryBll.updateSourceState(sourceId, getIntByState(txt_state.Text));
            int resultPath = sourceBll.updateSourcePath(sourceId, sourcePath);
            if (resultState >0 && resultPath > 0)
            {
                MessageBox.Show("询价单保存成功！");
            }
            scxj_button.Visible = false;
            send_button.Visible = true;

        }

        //发送邀请
        private void send_button_Click(object sender, EventArgs e)
        {
            MailMessage message = new MailMessage();
            //获取供应商列表中的邮件地址
            string address = null;
            for (int i = 0; i < gysList.Count; i++)
            {
                address = Supplier_view.Rows[i].Cells["email"].Value.ToString().Trim();
                if (tools.isMailAddress(address))
                    message.To.Add(address);
            }
            Attachment att = new Attachment(inquiry_table.Inquiry_Path); 
            message.Attachments.Add(att);//添加附件
            bool send = tools.SendEmail(message);
            att.Dispose();
            txt_state.Text = "待报价";
            int result = sourceInquiryBll.updateSourceState(sourceId, getIntByState(txt_state.Text));
            
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待维护"))
            {
                res = 0;
            }
            else if (state.Equals("待邀请"))
            {
                res = 1;
            }
            else if (state.Equals("待报价"))
            {
                res = 2;
            }
            else if (state.Equals("待比价"))
            {
                res = 3;
            }
            else if (state.Equals("已完成"))
            {
                res = 4;
            }
            return res;
        }

        //生成询价单
        private void scxj_button_Click(object sender, EventArgs e)
        {
            Export();
        }


        /// <summary>
        /// 查看询价信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckxj_button_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(inquiry_table.Inquiry_Path);
        }

        /// <summary>
        /// 添加供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addSupplier_Click(object sender, EventArgs e)
        {
            SupplierInfo supplierInfo = null;
            if (supplierInfo == null || supplierInfo.IsDisposed)
            {
                supplierInfo = new SupplierInfo(this);
            }
            supplierInfo.Show();
        }

        /// <summary>
        /// 填充供应商信息
        /// </summary>
        /// <param name="supplierIds"></param>
        public void fillSupplierBases(List<string> supplierIds)
        {
            Supplier_view.Rows.Clear();
            List<SupplierBase> supplierBaseList = generalBll.GetAllSuppliersByIdsLimit(supplierIds);
            gysList = supplierBaseList;
            if (supplierBaseList != null)
            {
                int i = 0;
                foreach (SupplierBase spb in supplierBaseList)
                {
                    Supplier_view.Rows.Add();
                    Supplier_view.Rows[i].Cells["supplierId"].Value = spb.Supplier_ID;
                    Supplier_view.Rows[i].Cells["supplierName"].Value = spb.Supplier_Name;
                    Supplier_view.Rows[i].Cells["mobilePhone"].Value = spb.Mobile_Phone1 ;
                    Supplier_view.Rows[i].Cells["email"].Value = spb.Email;
                    Supplier_view.Rows[i].Cells["address"].Value = spb.Address;
                    i++;
                }
            }
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Source source = new Source();
            source.Source_ID = txt_inquiryId.Text;
            source.Source_Name = txt_InquiryName.Text;
            source.Source_Type = "询价";
            source.PurchaseOrg = cmb_purchaseOrg.Text;
            source.PurchaseGroup = cmb_purchaseGroup.Text;
            source.Delivery_Time = DateTime.Parse(dte_OfferTime.Text);
            source.Source_State = txt_state.Text;
            //添加寻源记录
            int result = sourceBll.addSource(source);
            //添加寻源详细信息
            SourceInquiry it = new SourceInquiry();
            it.Source_ID = txt_inquiryId.Text;
            it.Offer_Time = DateTime.Parse(dte_OfferTime.Text);
            it.State = 2;
            int resIn = sourceInquiryBll.addNewInquiry_Table(it);
            //添加此次寻源对应的所有物料
            foreach (DataGridViewRow dr in Inquiry_view.Rows)
            {
                 SourceMaterial sm = new SourceMaterial();
                 sm.Source_ID = txt_inquiryId.Text;
                 sm.Material_ID = dr.Cells["materialId"].Value.ToString();
                 sm.Material_Name = dr.Cells["materialName"].Value.ToString();
                 sm.Demand_Count = Convert.ToInt32(dr.Cells["count"].Value.ToString());
                 sm.Demand_ID = summaryDemand.Demand_ID;
                sm.FactoryId = dr.Cells["factoryId"].Value.ToString();
                sm.StockId = dr.Cells["stockId"].Value.ToString();
                sm.DeliveryStartTime = DateTime.Parse(dr.Cells["DeliveryStartTime"].Value.ToString());
                sm.DeliveryEndTime = DateTime.Parse(dr.Cells["DeliveryEndTime"].Value.ToString());
                int resm = sourceMaterialBll.addSourceMaterials(sm);
                 if (resm < 1)
                 {
                     MessageBox.Show("添加寻源包含的物料时发生错误");
                  }
            }
            //添加此次寻源对应的所有供应商
            foreach (DataGridViewRow dr in Supplier_view.Rows)
            {
                SourceSupplier ss = new SourceSupplier();
                ss.Source_ID = txt_inquiryId.Text;
                ss.Supplier_ID = dr.Cells["supplierId"].Value.ToString();
                int ress = sourceSupplierBll.addSourceSupplier(ss);
                if (ress < 1)
                {
                    MessageBox.Show("添加寻源包含的供应商时发生错误");
                }
            }
            if (resIn > 0)
            {
                MessageBox.Show("添加成功");
            }
        }

        /// <summary>
        /// 检查数据的合法性
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_check_Click(object sender, EventArgs e)
        {
            if (txt_InquiryName.Text.Equals(""))
            {
                MessageBox.Show("请输入询价单名称！");
                return;
            }
            if (Supplier_view.Rows.Count<1)
            {
                MessageBox.Show("请选择需要询价的供应商");
                return;
            }
            if (DateTime.Parse(dte_OfferTime.Text) <= DateTime.Now)
            {
                MessageBox.Show("报价日期应晚于询价日期");
                return;
            }
            button1.Visible = true;
        }
    }
}
