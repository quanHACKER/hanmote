﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class NewSourceList_Form : DockContent
    {
        
        SourceListBLL sourceListBll = new SourceListBLL();

        public NewSourceList_Form()
        {
            InitializeComponent();
            init();
        }

        //初始化
        private void init()
        {
            initMaterial();
            initSupplier();
        }

        //初始化物料信息
        private void initMaterial()
        {
            txt_MaterialId.Text = "";
            txt_FactoryId.Text = "";
        }

        //初始化供应商信息
        private void initSupplier()
        {

        }

        //保存货源清单
        private void btn_SaveSourceList_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in sourceListGridView.Rows)
            {
                SourceList sl = new SourceList();
                sl.Material_ID = txt_MaterialId.Text;
                sl.Factory_ID = txt_FactoryId.Text;
                sl.Supplier_ID = dgvr.Cells["supplierId"].ToString();
                sl.StartTime = DateTime.Parse(dgvr.Cells["startTime"].ToString());
                sl.EndTime = DateTime.Parse(dgvr.Cells["endTime"].ToString());
                sl.CreateTime = DateTime.Now;
                sl.Fix = 0;
                sl.Blk = 0;
                sl.MRP = 0;
                if(dgvr.Cells["fix"].Selected)
                {
                    sl.Fix =1;
                }else if(dgvr.Cells["blk"].Selected){
                    sl.Blk = 1;
                }
                sourceListBll.addSourceList(sl);
            }
        }

        private void sourceListGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_FactoryId_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
