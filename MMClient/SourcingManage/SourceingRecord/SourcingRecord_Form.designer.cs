﻿namespace MMClient.SourcingManage.SourceingRecord
{
    partial class SourcingRecord_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.maintain_bt = new System.Windows.Forms.Button();
            this.create_bt = new System.Windows.Forms.Button();
            this.refresh_bt = new System.Windows.Forms.Button();
            this.query_bt = new System.Windows.Forms.Button();
            this.next_bt = new System.Windows.Forms.Button();
            this.close_bt = new System.Windows.Forms.Button();
            this.gys_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.xxlb_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cgzz_tb = new System.Windows.Forms.TextBox();
            this.gc_tb = new System.Windows.Forms.TextBox();
            this.cgxxjl_tb = new System.Windows.Forms.TextBox();
            this.wlbh_tb = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.maintain_bt);
            this.panel1.Controls.Add(this.create_bt);
            this.panel1.Controls.Add(this.refresh_bt);
            this.panel1.Controls.Add(this.query_bt);
            this.panel1.Controls.Add(this.next_bt);
            this.panel1.Controls.Add(this.close_bt);
            this.panel1.Location = new System.Drawing.Point(12, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 42);
            this.panel1.TabIndex = 89;
            // 
            // maintain_bt
            // 
            this.maintain_bt.Location = new System.Drawing.Point(331, 5);
            this.maintain_bt.Name = "maintain_bt";
            this.maintain_bt.Size = new System.Drawing.Size(60, 30);
            this.maintain_bt.TabIndex = 82;
            this.maintain_bt.Text = "维护";
            this.maintain_bt.UseVisualStyleBackColor = true;
            // 
            // create_bt
            // 
            this.create_bt.Location = new System.Drawing.Point(254, 5);
            this.create_bt.Name = "create_bt";
            this.create_bt.Size = new System.Drawing.Size(60, 30);
            this.create_bt.TabIndex = 81;
            this.create_bt.Text = "新建";
            this.create_bt.UseVisualStyleBackColor = true;
            // 
            // refresh_bt
            // 
            this.refresh_bt.Location = new System.Drawing.Point(407, 5);
            this.refresh_bt.Name = "refresh_bt";
            this.refresh_bt.Size = new System.Drawing.Size(60, 30);
            this.refresh_bt.TabIndex = 80;
            this.refresh_bt.Text = "刷新";
            this.refresh_bt.UseVisualStyleBackColor = true;
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(175, 5);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(60, 30);
            this.query_bt.TabIndex = 79;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            // 
            // next_bt
            // 
            this.next_bt.Location = new System.Drawing.Point(96, 5);
            this.next_bt.Name = "next_bt";
            this.next_bt.Size = new System.Drawing.Size(60, 30);
            this.next_bt.TabIndex = 76;
            this.next_bt.Text = "下一步";
            this.next_bt.UseVisualStyleBackColor = true;
            // 
            // close_bt
            // 
            this.close_bt.Location = new System.Drawing.Point(16, 5);
            this.close_bt.Name = "close_bt";
            this.close_bt.Size = new System.Drawing.Size(60, 30);
            this.close_bt.TabIndex = 75;
            this.close_bt.Text = "关闭";
            this.close_bt.UseVisualStyleBackColor = true;
            // 
            // gys_cmb
            // 
            this.gys_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gys_cmb.FormattingEnabled = true;
            this.gys_cmb.ItemHeight = 14;
            this.gys_cmb.Location = new System.Drawing.Point(98, 123);
            this.gys_cmb.Name = "gys_cmb";
            this.gys_cmb.Size = new System.Drawing.Size(200, 22);
            this.gys_cmb.TabIndex = 91;
            this.gys_cmb.Text = "2000002";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(10, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 90;
            this.label2.Text = "供应商";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(8, 83);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(129, 19);
            this.label1.TabIndex = 154;
            this.label1.Text = "寻源信息记录";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(10, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 155;
            this.label3.Text = "物料编号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(10, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 159;
            this.label4.Text = "工厂";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(10, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 157;
            this.label5.Text = "采购组织";
            // 
            // xxlb_cmb
            // 
            this.xxlb_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xxlb_cmb.FormattingEnabled = true;
            this.xxlb_cmb.ItemHeight = 14;
            this.xxlb_cmb.Location = new System.Drawing.Point(98, 305);
            this.xxlb_cmb.Name = "xxlb_cmb";
            this.xxlb_cmb.Size = new System.Drawing.Size(200, 22);
            this.xxlb_cmb.TabIndex = 164;
            this.xxlb_cmb.Text = "标准采购类型";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(10, 310);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 163;
            this.label6.Text = "信息类别";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(10, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 161;
            this.label7.Text = "采购信息记录";
            // 
            // cgzz_tb
            // 
            this.cgzz_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz_tb.Location = new System.Drawing.Point(98, 196);
            this.cgzz_tb.Name = "cgzz_tb";
            this.cgzz_tb.Size = new System.Drawing.Size(100, 23);
            this.cgzz_tb.TabIndex = 165;
            this.cgzz_tb.Text = "2000";
            // 
            // gc_tb
            // 
            this.gc_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gc_tb.Location = new System.Drawing.Point(98, 233);
            this.gc_tb.Name = "gc_tb";
            this.gc_tb.Size = new System.Drawing.Size(100, 23);
            this.gc_tb.TabIndex = 166;
            this.gc_tb.Text = "2001";
            // 
            // cgxxjl_tb
            // 
            this.cgxxjl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgxxjl_tb.Location = new System.Drawing.Point(98, 270);
            this.cgxxjl_tb.Name = "cgxxjl_tb";
            this.cgxxjl_tb.Size = new System.Drawing.Size(200, 23);
            this.cgxxjl_tb.TabIndex = 167;
            this.cgxxjl_tb.Text = "530000000000004";
            // 
            // wlbh_tb
            // 
            this.wlbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_tb.Location = new System.Drawing.Point(98, 159);
            this.wlbh_tb.Name = "wlbh_tb";
            this.wlbh_tb.Size = new System.Drawing.Size(200, 23);
            this.wlbh_tb.TabIndex = 168;
            this.wlbh_tb.Text = "F00000026";
            // 
            // SourcingRecord_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(808, 576);
            this.Controls.Add(this.wlbh_tb);
            this.Controls.Add(this.cgxxjl_tb);
            this.Controls.Add(this.gc_tb);
            this.Controls.Add(this.cgzz_tb);
            this.Controls.Add(this.xxlb_cmb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gys_cmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SourcingRecord_Form";
            this.Text = "寻源信息记录";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button refresh_bt;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Button next_bt;
        private System.Windows.Forms.Button close_bt;
        private System.Windows.Forms.ComboBox gys_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox xxlb_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox cgzz_tb;
        private System.Windows.Forms.TextBox gc_tb;
        private System.Windows.Forms.TextBox cgxxjl_tb;
        private System.Windows.Forms.TextBox wlbh_tb;
        private System.Windows.Forms.Button maintain_bt;
        private System.Windows.Forms.Button create_bt;
    }
}