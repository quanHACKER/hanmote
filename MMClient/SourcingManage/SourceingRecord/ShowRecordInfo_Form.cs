﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;

namespace MMClient.SourcingManage.SourceingRecord
{
    public partial class ShowRecordInfo_Form :DockContent
    {

        RecordInfoBLL recordInfoBLL = new RecordInfoBLL();
        public ShowRecordInfo_Form()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            List<RecordInfo> list = recordInfoBLL.getAllRecordInfos();
            if (list.Count > recordGridView.Rows.Count)
            {
                recordGridView.Rows.Add(list.Count - recordGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                recordGridView.Rows[i].Cells["recordInfoId"].Value = list.ElementAt(i).RecordInfoId;
                recordGridView.Rows[i].Cells["supplierId"].Value = list.ElementAt(i).SupplierId;
                recordGridView.Rows[i].Cells["SupplierName"].Value = list.ElementAt(i).SupplierName;
                recordGridView.Rows[i].Cells["materialId"].Value = list.ElementAt(i).MaterialId;
                recordGridView.Rows[i].Cells["MaterialName"].Value = list.ElementAt(i).MaterialName;
                recordGridView.Rows[i].Cells["NetPrice"].Value = list.ElementAt(i).NetPrice;
                recordGridView.Rows[i].Cells["FromType"].Value = getFromStrByFromInt(list.ElementAt(i).FromType);
                recordGridView.Rows[i].Cells["FromId"].Value = list.ElementAt(i).FromId;
                recordGridView.Rows[i].Cells["CreatTime"].Value = list.ElementAt(i).CreatTime.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        private string getFromStrByFromInt(int num)
        {
            string str = "";
            if (num == 0)
            {
                str = "询价";
            }else if(num==1){
                str = "招标";
            }else if(num==2){
                str = "竞价";
            }
            else if (num == 3)
            {
                str = "合同";
            }
            else
            {
                str = "订单";
            }

            return str;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("功能还在完善中......");
            return;
        }
    }
}
