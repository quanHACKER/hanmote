﻿namespace MMClient.SourcingManage.SourceingRecord
{
    partial class MRPCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MRPCount));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label11 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.bjqx_dt = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.xjsj_dt = new System.Windows.Forms.DateTimePicker();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.close_pb = new System.Windows.Forms.PictureBox();
            this.examine_pb = new System.Windows.Forms.PictureBox();
            this.edit_pb = new System.Windows.Forms.PictureBox();
            this.delete_pb = new System.Windows.Forms.PictureBox();
            this.detail_pb = new System.Windows.Forms.PictureBox();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.source_view = new System.Windows.Forms.DataGridView();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.youxiao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examine_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.Location = new System.Drawing.Point(11, 70);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 18);
            this.label11.TabIndex = 104;
            this.label11.Text = "开始";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(352, 19);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(80, 18);
            this.label32.TabIndex = 200;
            this.label32.Text = "运算时间";
            // 
            // bjqx_dt
            // 
            this.bjqx_dt.CustomFormat = "yyyy-MM-dd HH:mm";
            this.bjqx_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bjqx_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bjqx_dt.Location = new System.Drawing.Point(780, 13);
            this.bjqx_dt.Margin = new System.Windows.Forms.Padding(4);
            this.bjqx_dt.Name = "bjqx_dt";
            this.bjqx_dt.Size = new System.Drawing.Size(268, 31);
            this.bjqx_dt.TabIndex = 203;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.bjqx_dt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.xjsj_dt);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.wlbh_cmb);
            this.panel2.Location = new System.Drawing.Point(2, 88);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1164, 61);
            this.panel2.TabIndex = 207;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(440, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 18);
            this.label3.TabIndex = 204;
            this.label3.Text = "从";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(746, 19);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 18);
            this.label4.TabIndex = 202;
            this.label4.Text = "至";
            // 
            // xjsj_dt
            // 
            this.xjsj_dt.CustomFormat = "yyyy-MM-dd HH:mm";
            this.xjsj_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xjsj_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.xjsj_dt.Location = new System.Drawing.Point(470, 13);
            this.xjsj_dt.Margin = new System.Windows.Forms.Padding(4);
            this.xjsj_dt.Name = "xjsj_dt";
            this.xjsj_dt.Size = new System.Drawing.Size(268, 31);
            this.xjsj_dt.TabIndex = 201;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(7, 19);
            this.xjdh_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(71, 18);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "MRP编号";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.Location = new System.Drawing.Point(85, 13);
            this.wlbh_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(238, 29);
            this.wlbh_cmb.TabIndex = 163;
            this.wlbh_cmb.Text = "MRP130900001";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 103;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(197, 69);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 18);
            this.label7.TabIndex = 101;
            this.label7.Text = "创建";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(135, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 18);
            this.label6.TabIndex = 96;
            this.label6.Text = "删除";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(260, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 18);
            this.label5.TabIndex = 97;
            this.label5.Text = "保存";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(323, 70);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 100;
            this.label8.Text = "关闭";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.close_pb);
            this.panel1.Controls.Add(this.examine_pb);
            this.panel1.Controls.Add(this.edit_pb);
            this.panel1.Controls.Add(this.delete_pb);
            this.panel1.Controls.Add(this.detail_pb);
            this.panel1.Location = new System.Drawing.Point(2, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1152, 99);
            this.panel1.TabIndex = 210;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(76, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 95;
            this.label2.Text = "查询";
            // 
            // close_pb
            // 
            this.close_pb.Image = ((System.Drawing.Image)(resources.GetObject("close_pb.Image")));
            this.close_pb.Location = new System.Drawing.Point(314, 4);
            this.close_pb.Margin = new System.Windows.Forms.Padding(4);
            this.close_pb.Name = "close_pb";
            this.close_pb.Size = new System.Drawing.Size(60, 63);
            this.close_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.close_pb.TabIndex = 93;
            this.close_pb.TabStop = false;
            // 
            // examine_pb
            // 
            this.examine_pb.Image = ((System.Drawing.Image)(resources.GetObject("examine_pb.Image")));
            this.examine_pb.Location = new System.Drawing.Point(189, 3);
            this.examine_pb.Margin = new System.Windows.Forms.Padding(4);
            this.examine_pb.Name = "examine_pb";
            this.examine_pb.Size = new System.Drawing.Size(60, 63);
            this.examine_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.examine_pb.TabIndex = 91;
            this.examine_pb.TabStop = false;
            // 
            // edit_pb
            // 
            this.edit_pb.Image = ((System.Drawing.Image)(resources.GetObject("edit_pb.Image")));
            this.edit_pb.Location = new System.Drawing.Point(252, 4);
            this.edit_pb.Margin = new System.Windows.Forms.Padding(4);
            this.edit_pb.Name = "edit_pb";
            this.edit_pb.Size = new System.Drawing.Size(60, 63);
            this.edit_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.edit_pb.TabIndex = 87;
            this.edit_pb.TabStop = false;
            // 
            // delete_pb
            // 
            this.delete_pb.Image = ((System.Drawing.Image)(resources.GetObject("delete_pb.Image")));
            this.delete_pb.Location = new System.Drawing.Point(126, 3);
            this.delete_pb.Margin = new System.Windows.Forms.Padding(4);
            this.delete_pb.Name = "delete_pb";
            this.delete_pb.Size = new System.Drawing.Size(60, 63);
            this.delete_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.delete_pb.TabIndex = 86;
            this.delete_pb.TabStop = false;
            // 
            // detail_pb
            // 
            this.detail_pb.Image = ((System.Drawing.Image)(resources.GetObject("detail_pb.Image")));
            this.detail_pb.Location = new System.Drawing.Point(64, 3);
            this.detail_pb.Margin = new System.Windows.Forms.Padding(4);
            this.detail_pb.Name = "detail_pb";
            this.detail_pb.Size = new System.Drawing.Size(60, 63);
            this.detail_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.detail_pb.TabIndex = 85;
            this.detail_pb.TabStop = false;
            // 
            // dataGridViewComboBoxColumn7
            // 
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.Frozen = true;
            this.dataGridViewComboBoxColumn7.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn7.Width = 80;
            // 
            // dataGridViewComboBoxColumn8
            // 
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn8.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.Frozen = true;
            this.dataGridViewComboBoxColumn8.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn8.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn9
            // 
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn9.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.Frozen = true;
            this.dataGridViewComboBoxColumn9.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn9.Width = 140;
            // 
            // dataGridViewComboBoxColumn10
            // 
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn10.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.Frozen = true;
            this.dataGridViewComboBoxColumn10.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn10.Width = 140;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(13, 165);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 23);
            this.label23.TabIndex = 206;
            this.label23.Text = "MRP运算结果";
            // 
            // dataGridViewComboBoxColumn11
            // 
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn11.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.Frozen = true;
            this.dataGridViewComboBoxColumn11.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn11.Width = 80;
            // 
            // source_view
            // 
            this.source_view.BackgroundColor = System.Drawing.Color.White;
            this.source_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.source_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Inquiry_view1,
            this.Inquiry_view2,
            this.Inquiry_view3,
            this.Inquiry_view5,
            this.Inquiry_view4,
            this.Inquiry_view11,
            this.Column3,
            this.youxiao,
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column6});
            this.source_view.Location = new System.Drawing.Point(13, 198);
            this.source_view.Margin = new System.Windows.Forms.Padding(4);
            this.source_view.Name = "source_view";
            this.source_view.RowHeadersVisible = false;
            this.source_view.RowTemplate.Height = 23;
            this.source_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.source_view.Size = new System.Drawing.Size(1532, 800);
            this.source_view.TabIndex = 208;
            // 
            // dataGridViewComboBoxColumn12
            // 
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn12.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.Frozen = true;
            this.dataGridViewComboBoxColumn12.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn12.Width = 160;
            // 
            // Column4
            // 
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 30;
            // 
            // Inquiry_view1
            // 
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            this.Inquiry_view1.DefaultCellStyle = dataGridViewCellStyle26;
            this.Inquiry_view1.Frozen = true;
            this.Inquiry_view1.HeaderText = "原料编号";
            this.Inquiry_view1.Name = "Inquiry_view1";
            this.Inquiry_view1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view2
            // 
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            this.Inquiry_view2.DefaultCellStyle = dataGridViewCellStyle27;
            this.Inquiry_view2.Frozen = true;
            this.Inquiry_view2.HeaderText = "原料名称";
            this.Inquiry_view2.Name = "Inquiry_view2";
            this.Inquiry_view2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view3
            // 
            this.Inquiry_view3.Frozen = true;
            this.Inquiry_view3.HeaderText = "单位";
            this.Inquiry_view3.Name = "Inquiry_view3";
            this.Inquiry_view3.Width = 60;
            // 
            // Inquiry_view5
            // 
            this.Inquiry_view5.Frozen = true;
            this.Inquiry_view5.HeaderText = "来源";
            this.Inquiry_view5.Name = "Inquiry_view5";
            this.Inquiry_view5.Width = 60;
            // 
            // Inquiry_view4
            // 
            this.Inquiry_view4.Frozen = true;
            this.Inquiry_view4.HeaderText = "MRP供需日期";
            this.Inquiry_view4.Name = "Inquiry_view4";
            // 
            // Inquiry_view11
            // 
            this.Inquiry_view11.Frozen = true;
            this.Inquiry_view11.HeaderText = "MRP毛需求量";
            this.Inquiry_view11.Name = "Inquiry_view11";
            this.Inquiry_view11.Width = 80;
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "初始库存";
            this.Column3.Name = "Column3";
            // 
            // youxiao
            // 
            this.youxiao.HeaderText = "安全库存";
            this.youxiao.Name = "youxiao";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "出库数";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "入库数";
            this.Column2.Name = "Column2";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "MRP净需求量";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "单据编号";
            this.Column6.Name = "Column6";
            // 
            // MRPCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1578, 1094);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.source_view);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MRPCount";
            this.Text = "MRPCount";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examine_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DateTimePicker bjqx_dt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker xjsj_dt;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox close_pb;
        private System.Windows.Forms.PictureBox examine_pb;
        private System.Windows.Forms.PictureBox edit_pb;
        private System.Windows.Forms.PictureBox delete_pb;
        private System.Windows.Forms.PictureBox detail_pb;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridView source_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn youxiao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}