﻿namespace MMClient.SourcingManage.SourceingRecord
{
    partial class MRPCountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MRPCountForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.wlmc_lbl = new System.Windows.Forms.Label();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.search_pb = new System.Windows.Forms.PictureBox();
            this.edit_pb = new System.Windows.Forms.PictureBox();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.edit_lbl = new System.Windows.Forms.Label();
            this.close_lbl = new System.Windows.Forms.Label();
            this.close_pb = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.read_lbl = new System.Windows.Forms.Label();
            this.read_pb = new System.Windows.Forms.PictureBox();
            this.activate_tb = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.source_view = new System.Windows.Forms.DataGridView();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.youxiao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.search_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox3.Location = new System.Drawing.Point(965, 57);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(63, 29);
            this.comboBox3.TabIndex = 204;
            this.comboBox3.Text = "辆";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(913, 63);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 18);
            this.label4.TabIndex = 203;
            this.label4.Text = "单位";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.wlmc_lbl);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.wlbh_cmb);
            this.panel2.Location = new System.Drawing.Point(3, 92);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1164, 106);
            this.panel2.TabIndex = 198;
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox2.Location = new System.Drawing.Point(772, 58);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(130, 29);
            this.comboBox2.TabIndex = 202;
            this.comboBox2.Text = "26寸";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(720, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 18);
            this.label3.TabIndex = 201;
            this.label3.Text = "规格";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox1.Location = new System.Drawing.Point(469, 57);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(238, 29);
            this.comboBox1.TabIndex = 200;
            this.comboBox1.Text = "山地自行车";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(82, 23);
            this.label1.TabIndex = 199;
            this.label1.Text = "主产品";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button1.Location = new System.Drawing.Point(345, 58);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 30);
            this.button1.TabIndex = 198;
            this.button1.Text = "i";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // wlmc_lbl
            // 
            this.wlmc_lbl.AutoSize = true;
            this.wlmc_lbl.Location = new System.Drawing.Point(381, 64);
            this.wlmc_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wlmc_lbl.Name = "wlmc_lbl";
            this.wlmc_lbl.Size = new System.Drawing.Size(80, 18);
            this.wlmc_lbl.TabIndex = 171;
            this.wlmc_lbl.Text = "产品名称";
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(14, 63);
            this.xjdh_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(80, 18);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "产品编号";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.Location = new System.Drawing.Point(102, 57);
            this.wlbh_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(238, 29);
            this.wlbh_cmb.TabIndex = 163;
            this.wlbh_cmb.Text = "2937000001";
            // 
            // dataGridViewComboBoxColumn7
            // 
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.Frozen = true;
            this.dataGridViewComboBoxColumn7.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn7.Width = 80;
            // 
            // dataGridViewComboBoxColumn8
            // 
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn8.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.Frozen = true;
            this.dataGridViewComboBoxColumn8.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn8.Width = 80;
            // 
            // dataGridViewComboBoxColumn9
            // 
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn9.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.Frozen = true;
            this.dataGridViewComboBoxColumn9.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn9.Width = 140;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(142, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 111;
            this.label2.Text = "查询";
            // 
            // search_pb
            // 
            this.search_pb.Image = ((System.Drawing.Image)(resources.GetObject("search_pb.Image")));
            this.search_pb.Location = new System.Drawing.Point(134, 8);
            this.search_pb.Margin = new System.Windows.Forms.Padding(4);
            this.search_pb.Name = "search_pb";
            this.search_pb.Size = new System.Drawing.Size(60, 60);
            this.search_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.search_pb.TabIndex = 110;
            this.search_pb.TabStop = false;
            // 
            // edit_pb
            // 
            this.edit_pb.Image = ((System.Drawing.Image)(resources.GetObject("edit_pb.Image")));
            this.edit_pb.Location = new System.Drawing.Point(8, 8);
            this.edit_pb.Margin = new System.Windows.Forms.Padding(4);
            this.edit_pb.Name = "edit_pb";
            this.edit_pb.Size = new System.Drawing.Size(60, 60);
            this.edit_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.edit_pb.TabIndex = 108;
            this.edit_pb.TabStop = false;
            // 
            // dataGridViewComboBoxColumn10
            // 
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn10.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.Frozen = true;
            this.dataGridViewComboBoxColumn10.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn10.Width = 140;
            // 
            // edit_lbl
            // 
            this.edit_lbl.AutoSize = true;
            this.edit_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.edit_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.edit_lbl.Location = new System.Drawing.Point(14, 72);
            this.edit_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.edit_lbl.Name = "edit_lbl";
            this.edit_lbl.Size = new System.Drawing.Size(44, 18);
            this.edit_lbl.TabIndex = 109;
            this.edit_lbl.Text = "维护";
            // 
            // close_lbl
            // 
            this.close_lbl.AutoSize = true;
            this.close_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.close_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.close_lbl.Location = new System.Drawing.Point(206, 72);
            this.close_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.close_lbl.Name = "close_lbl";
            this.close_lbl.Size = new System.Drawing.Size(44, 18);
            this.close_lbl.TabIndex = 106;
            this.close_lbl.Text = "关闭";
            // 
            // close_pb
            // 
            this.close_pb.Image = ((System.Drawing.Image)(resources.GetObject("close_pb.Image")));
            this.close_pb.Location = new System.Drawing.Point(196, 8);
            this.close_pb.Margin = new System.Windows.Forms.Padding(4);
            this.close_pb.Name = "close_pb";
            this.close_pb.Size = new System.Drawing.Size(60, 60);
            this.close_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.close_pb.TabIndex = 105;
            this.close_pb.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.search_pb);
            this.panel1.Controls.Add(this.edit_lbl);
            this.panel1.Controls.Add(this.edit_pb);
            this.panel1.Controls.Add(this.close_lbl);
            this.panel1.Controls.Add(this.close_pb);
            this.panel1.Controls.Add(this.read_lbl);
            this.panel1.Controls.Add(this.read_pb);
            this.panel1.Controls.Add(this.activate_tb);
            this.panel1.Location = new System.Drawing.Point(3, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(933, 94);
            this.panel1.TabIndex = 196;
            // 
            // read_lbl
            // 
            this.read_lbl.AutoSize = true;
            this.read_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.read_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.read_lbl.Location = new System.Drawing.Point(80, 72);
            this.read_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.read_lbl.Name = "read_lbl";
            this.read_lbl.Size = new System.Drawing.Size(44, 18);
            this.read_lbl.TabIndex = 102;
            this.read_lbl.Text = "创建";
            // 
            // read_pb
            // 
            this.read_pb.Image = ((System.Drawing.Image)(resources.GetObject("read_pb.Image")));
            this.read_pb.Location = new System.Drawing.Point(70, 8);
            this.read_pb.Margin = new System.Windows.Forms.Padding(4);
            this.read_pb.Name = "read_pb";
            this.read_pb.Size = new System.Drawing.Size(60, 60);
            this.read_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.read_pb.TabIndex = 92;
            this.read_pb.TabStop = false;
            // 
            // activate_tb
            // 
            this.activate_tb.BackColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.activate_tb.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.Location = new System.Drawing.Point(10, 8);
            this.activate_tb.Margin = new System.Windows.Forms.Padding(4);
            this.activate_tb.Name = "activate_tb";
            this.activate_tb.Size = new System.Drawing.Size(150, 21);
            this.activate_tb.TabIndex = 107;
            // 
            // saveButton
            // 
            this.saveButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveButton.Location = new System.Drawing.Point(1220, 205);
            this.saveButton.Margin = new System.Windows.Forms.Padding(4);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(94, 34);
            this.saveButton.TabIndex = 200;
            this.saveButton.Text = "保存";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // dataGridViewComboBoxColumn11
            // 
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn11.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.Frozen = true;
            this.dataGridViewComboBoxColumn11.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn11.Width = 80;
            // 
            // source_view
            // 
            this.source_view.BackgroundColor = System.Drawing.Color.White;
            this.source_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.source_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Inquiry_view1,
            this.Inquiry_view2,
            this.Inquiry_view3,
            this.Inquiry_view4,
            this.Inquiry_view11,
            this.Inquiry_view5,
            this.Column3,
            this.youxiao,
            this.Column1,
            this.Column2});
            this.source_view.Location = new System.Drawing.Point(13, 249);
            this.source_view.Margin = new System.Windows.Forms.Padding(4);
            this.source_view.Name = "source_view";
            this.source_view.RowHeadersVisible = false;
            this.source_view.RowTemplate.Height = 23;
            this.source_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.source_view.Size = new System.Drawing.Size(1320, 382);
            this.source_view.TabIndex = 199;
            // 
            // dataGridViewComboBoxColumn12
            // 
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn12.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.Frozen = true;
            this.dataGridViewComboBoxColumn12.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn12.Width = 160;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(13, 216);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(178, 23);
            this.label23.TabIndex = 197;
            this.label23.Text = "子产品原料信息";
            // 
            // Column4
            // 
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 30;
            // 
            // Inquiry_view1
            // 
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            this.Inquiry_view1.DefaultCellStyle = dataGridViewCellStyle26;
            this.Inquiry_view1.Frozen = true;
            this.Inquiry_view1.HeaderText = "原料编号";
            this.Inquiry_view1.Name = "Inquiry_view1";
            this.Inquiry_view1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view2
            // 
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            this.Inquiry_view2.DefaultCellStyle = dataGridViewCellStyle27;
            this.Inquiry_view2.Frozen = true;
            this.Inquiry_view2.HeaderText = "原料名称";
            this.Inquiry_view2.Name = "Inquiry_view2";
            this.Inquiry_view2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view3
            // 
            this.Inquiry_view3.Frozen = true;
            this.Inquiry_view3.HeaderText = "单位";
            this.Inquiry_view3.Name = "Inquiry_view3";
            this.Inquiry_view3.Width = 60;
            // 
            // Inquiry_view4
            // 
            this.Inquiry_view4.Frozen = true;
            this.Inquiry_view4.HeaderText = "用量";
            this.Inquiry_view4.Name = "Inquiry_view4";
            this.Inquiry_view4.Width = 60;
            // 
            // Inquiry_view11
            // 
            this.Inquiry_view11.Frozen = true;
            this.Inquiry_view11.HeaderText = "损耗率";
            this.Inquiry_view11.Name = "Inquiry_view11";
            this.Inquiry_view11.Width = 80;
            // 
            // Inquiry_view5
            // 
            this.Inquiry_view5.Frozen = true;
            this.Inquiry_view5.HeaderText = "替代编号";
            this.Inquiry_view5.Name = "Inquiry_view5";
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "工序日期";
            this.Column3.Name = "Column3";
            // 
            // youxiao
            // 
            this.youxiao.HeaderText = "生效日期";
            this.youxiao.Name = "youxiao";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "失效日期";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "备注信息";
            this.Column2.Name = "Column2";
            // 
            // NewBomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 1094);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.source_view);
            this.Controls.Add(this.label23);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewBomForm";
            this.Text = "创建产品BOM";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.search_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.source_view)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label wlmc_lbl;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox search_pb;
        private System.Windows.Forms.PictureBox edit_pb;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.Label edit_lbl;
        private System.Windows.Forms.Label close_lbl;
        private System.Windows.Forms.PictureBox close_pb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label read_lbl;
        private System.Windows.Forms.PictureBox read_pb;
        private System.Windows.Forms.TextBox activate_tb;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridView source_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn youxiao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}