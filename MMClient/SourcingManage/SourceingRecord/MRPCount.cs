﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourceingRecord
{
    public partial class MRPCount : DockContent
    {
        public MRPCount()
        {
            InitializeComponent();
            load2();
        }

        public void load2()
        {
            for (int i = 0; i < 17; i++)
            {
                source_view.Rows.Add();
            }
            for (int i = 0; i < 17; i++)
            {
                source_view.Rows[i].Cells[0].Value = (i+1).ToString();
                source_view.Rows[i].Cells[3].Value = "个";
                source_view.Rows[i].Cells[4].Value = "采购";
                source_view.Rows[i].Cells[5].Value = "2016-04-25";
                source_view.Rows[i].Cells[6].Value = "100";
                source_view.Rows[i].Cells[7].Value = "150";
                source_view.Rows[i].Cells[8].Value = "50";
                source_view.Rows[i].Cells[9].Value = "0";
                source_view.Rows[i].Cells[10].Value = "0";
                source_view.Rows[i].Cells[11].Value = "0";
                source_view.Rows[i].Cells[12].Value = "201604211024535"+i.ToString();
            }
            source_view.Rows[0].Cells[1].Value = "21700001";
            source_view.Rows[1].Cells[1].Value = "21700002";
            source_view.Rows[2].Cells[1].Value = "21700003";
            source_view.Rows[3].Cells[1].Value = "21700004";

            source_view.Rows[4].Cells[1].Value = "12300001";
            source_view.Rows[5].Cells[1].Value = "12500012";
            source_view.Rows[6].Cells[1].Value = "12600015";

            source_view.Rows[7].Cells[1].Value = "14500001";
            source_view.Rows[8].Cells[1].Value = "15400052";
            source_view.Rows[9].Cells[1].Value = "15400073";
            source_view.Rows[10].Cells[1].Value = "15400121";

            source_view.Rows[11].Cells[1].Value = "15700001";
            source_view.Rows[12].Cells[1].Value = "16500001";
            source_view.Rows[13].Cells[1].Value = "16500003";
            source_view.Rows[14].Cells[1].Value = "16500004";
            source_view.Rows[15].Cells[1].Value = "16500006";
            source_view.Rows[16].Cells[1].Value = "16500007";

            source_view.Rows[0].Cells[2].Value = "车架";
            source_view.Rows[1].Cells[2].Value = "车锁";
            source_view.Rows[2].Cells[2].Value = "车座";
            source_view.Rows[3].Cells[2].Value = "支架";

            source_view.Rows[4].Cells[2].Value = "车把系统";
            source_view.Rows[5].Cells[2].Value = "车把";
            source_view.Rows[6].Cells[2].Value = "车刹";

            source_view.Rows[7].Cells[2].Value = "车轮系统";
            source_view.Rows[8].Cells[2].Value = "车轮";
            source_view.Rows[9].Cells[2].Value = "前轴";
            source_view.Rows[10].Cells[2].Value = "后轴";

            source_view.Rows[11].Cells[2].Value = "脚蹬系统";
            source_view.Rows[12].Cells[2].Value = "脚蹬";
            source_view.Rows[13].Cells[2].Value = "中轴";
            source_view.Rows[14].Cells[2].Value = "链条";
            source_view.Rows[15].Cells[2].Value = "档板";
            source_view.Rows[16].Cells[2].Value = "齿轮";

            source_view.Rows[1].Cells[8].Value = "21700016";
            source_view.Rows[4].Cells[8].Value = "12500123";

        }
    }
}
