﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourceingRecord
{
    public partial class MRPCountForm : DockContent
    {
        public MRPCountForm()
        {
            InitializeComponent();
            load();
        }

        public void load()
        {
            for (int i = 0; i < 7; i++)
            {
                source_view.Rows.Add();
            }
            for (int i = 0; i < 7; i++)
            {
                source_view.Rows[i].Cells[3].Value = "个";
                source_view.Rows[i].Cells[4].Value = "1";
                source_view.Rows[i].Cells[5].Value = "0";
                source_view.Rows[i].Cells[7].Value = "0";
                source_view.Rows[i].Cells[8].Value = "2016-01-01";
                source_view.Rows[i].Cells[9].Value = "2199-01-01";
            }
            source_view.Rows[0].Cells[1].Value = "21700001";
            source_view.Rows[1].Cells[1].Value = "21700002";
            source_view.Rows[2].Cells[1].Value = "21700003";
            source_view.Rows[3].Cells[1].Value = "21700004";
            source_view.Rows[4].Cells[1].Value = "12300001";
            source_view.Rows[5].Cells[1].Value = "14500001";
            source_view.Rows[6].Cells[1].Value = "15700001";

            source_view.Rows[0].Cells[2].Value = "车架";
            source_view.Rows[1].Cells[2].Value = "车锁";
            source_view.Rows[2].Cells[2].Value = "车座";
            source_view.Rows[3].Cells[2].Value = "支架";
            source_view.Rows[4].Cells[2].Value = "车把系统";
            source_view.Rows[5].Cells[2].Value = "车轮系统";
            source_view.Rows[6].Cells[2].Value = "脚蹬系统";

            source_view.Rows[1].Cells[6].Value = "21700016";
            source_view.Rows[4].Cells[6].Value = "12500123";

        }
    }
}
