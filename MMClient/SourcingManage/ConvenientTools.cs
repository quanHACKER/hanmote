﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
//using LumiSoft.Net.POP3.Client;
//using LumiSoft.Net.Mail;
using System.Net.Sockets;
using System.Windows.Forms;
using Lib.Bll.SourcingManage.ProcurementPlan;
using System.Text.RegularExpressions;
using Lib.Model.SourcingManage.SourcingManagement;
using Aspose.Cells;

namespace MMClient.SourcingManage
{
    class ConvenientTools
    {
       
        /// <summary>
        /// 从数据库加载数据至控件Items
        /// </summary>
        /// <param name="comboBox">comboBox名称</param>
        /// <param name="value">条件值</param>
        /// <param name="key">条件数据列名</param>
        /// <param name="itemName">查询数据列</param>
        /// <param name="tableName">查询数据表</param>
        public void addItemsToComboBox(ComboBox comboBox,string value,string key, string itemName,string tableName)
        {
            Summary_DemandBLL DemandTool = new Summary_DemandBLL();
            List<string> ItemList = new List<string>();
            try 
            {
                ItemList = DemandTool.FindAddItems(value, key, itemName, tableName);
                if (ItemList != null)
                {
                    comboBox.Items.Clear();
                    comboBox.Items.Add(ItemList.ElementAt(ItemList.Count-1));
                    string str = ItemList.ElementAt(ItemList.Count - 1);
                    for (int n = ItemList.Count - 2; n >= 0; n--)
                    {
                        if (comboBox.Text.ToString().Equals(ItemList.ElementAt(n)))
                        {
                            str = comboBox.Text.ToString();
                        }
                        comboBox.Items.Add(ItemList.ElementAt(n));
                    }
                    comboBox.Text = str;
                    comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    comboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
                }
            }
            catch(Exception e) 
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void addItemsToCMB(ComboBox comboBox, string value, string key, string itemName, string tableName)
        {
            Summary_DemandBLL DemandTool = new Summary_DemandBLL();
            List<string> ItemList = new List<string>();
            try
            {
                ItemList = DemandTool.FindAddItems(value, key, itemName, tableName);
                if (ItemList != null)
                {
                    comboBox.Items.Clear();
                    bool contains = false;
                    for (int n = ItemList.Count - 1; n >= 0; n--)
                    {
                        if (comboBox.Text.ToString().Equals(ItemList.ElementAt(n)))
                        {
                            contains = true;
                        }
                        comboBox.Items.Add(ItemList.ElementAt(n));
                    }
                    if (!contains && ItemList.Count==1)
                        comboBox.Text = ItemList.ElementAt(0);
                    else if (!contains)
                        comboBox.Text = "";
                    comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    comboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
                }
                else
                    comboBox.Text = "";
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        /// <summary>
        /// 从数据库加载数据至list
        /// </summary>
        /// <param></param>
        /// <returns>null</returns>
        public List<string> addItemsStringList(String value, String key, String itemName, String tableName)
        {
            Summary_DemandBLL DemandTool = new Summary_DemandBLL();
            List<string> ItemList = new List<string>();
            try
            {
                ItemList = DemandTool.FindAddItems(value, key, itemName, tableName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            return ItemList;
        }

        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

        /// <summary>
        /// 时间差
        /// </summary>
        /// <param></param>
        /// <returns>time</returns>
        public TimeSpan dateDiff(DateTime time1, DateTime time2)
        {
            TimeSpan ts1 = new TimeSpan(time1.Ticks);
            TimeSpan ts2 = new TimeSpan(time2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            return ts;
        }

        /// <summary>
        /// 读取系统时间并转换为数字字符
        /// </summary>
        /// <param></param>
        /// <returns>time</returns>
        public String systemTimeToStr()
        {
            DateTime time = DateTime.Now;
            string[] s = { time.Year.ToString(), 
                           timeTool(time.Month.ToString()), 
                           timeTool(time.Day.ToString()), 
                           timeTool(time.Hour.ToString()), 
                           timeTool(time.Minute.ToString()), 
                           timeTool(time.Second.ToString()),
                           timeTool(time.Millisecond)
                         };
            return string.Concat(s);
        }
        //若时间为个位数则加0
        public string timeTool(string str)
        {
            if (str.Length == 1)
            {
                return "0"+str;
            }
            return str;
        }
        //毫秒加0
        public string timeTool(int num)
        {
            if (num <10)
                return "00"+num.ToString();
            else if(num<100)
                return "0" + num.ToString();
            else
                return num.ToString();
        }

        /// <summary>
        /// 只能输入数字和小数点
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="dict_temp"></param>
        public void onlyNumber(DataGridViewTextBoxCell tb)
        {
            //string pattern = @"^[0-9]+[\.]?\d*$";
            string pattern = @"^([0-9]+[\.]?\d*)?$";     //可以支持一次全删除数字
            Match m = Regex.Match(tb.EditedFormattedValue.ToString(), pattern);
            if (!m.Success)
            {
                tb.Value = "";
            }
        }

        /// <summary>
        /// 返回控件的string值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public string boxToString(DataGridViewCell tb)
        {
            if (tb.Value==null)
                return "";
            return tb.EditedFormattedValue.ToString().Trim();
        }

        /// <summary>
        /// 返回控件的string值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public string cmbToString(ComboBox tb)
        {
            if (tb.Text == null)
                return "";
            return tb.Text.ToString().Trim();
        }

        /// <summary>
        /// 返回控件的int值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public int boxToInt(DataGridViewCell tb)
        {
            if (tb.Value == null||tb.Value.ToString().Equals(""))
                return 0;
            return Convert.ToInt32(tb.Value.ToString().Trim());
        }

        /// <summary>
        /// 返回控件的float值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public double boxToDouble(DataGridViewCell tb)
        {
            if (tb.Value == null)
                return 0.0;
            try
            {
                return Convert.ToDouble(tb.EditedFormattedValue.ToString().Trim());
            }
            catch (Exception e)
            {
                return 0.0;
            }
        }

        /// <summary>
        /// 返回控件的float值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public double strToDouble(string str)
        {
            try
            {
                return Convert.ToDouble(str);
            }
            catch (Exception e)
            {
                return 0.0;
            }
        }

        /// <summary>
        /// 返回控件的time值
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        public DateTime boxToTime(DataGridViewCell tb)
        {
            try
            {
                return DateTime.Parse(tb.EditedFormattedValue.ToString());
            }
            catch (Exception e)
            {
                return DateTime.Parse("1900-01-01 00:00:00.000");
            }
        }
        public DateTime boxToTime2(DataGridViewCell tb)
        {
            try
            {
                return DateTime.Parse(tb.EditedFormattedValue.ToString());
            }
            catch (Exception e)
            {
                return DateTime.Parse("9999-01-01 00:00:00.000");
            }
        }

        public Enquiry setEnquiry(Enquiry en)
        {
            Enquiry enquiry = new Enquiry();
            enquiry.Inquiry_ID = en.Inquiry_ID;
            enquiry.Material_ID = en.Material_ID;
            enquiry.Supplier_ID = en.Supplier_ID;
            enquiry.Number = en.Number;
            enquiry.Price = en.Price;
            enquiry.Clause_ID = en.Clause_ID;
            enquiry.Delivery_Time = en.Delivery_Time;
            enquiry.Factory_ID = en.Factory_ID;
            enquiry.Stock_ID = en.Stock_ID;
            enquiry.Condition_ID = en.Condition_ID;
            enquiry.State = en.State;
            enquiry.Update_Time = en.Update_Time;
            return enquiry;
        }

        /// <summary>
        /// 设置单元格的样式
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        public Style SettingCellStyle(Workbook workbook, Cells cells, Boolean isBold)
        {
            Style style = workbook.Styles[workbook.Styles.Add()];//新增样式
            style.HorizontalAlignment = TextAlignmentType.Center;//文字居中
            style.Font.Name = "宋体";//文字字体
            style.Font.Size = 10;//文字大小
            style.IsLocked = false;//单元格解锁
            if (isBold)
            {
                style.Font.IsBold = true;//粗体
            }
            else
            {
                style.Font.IsBold = false;//粗体
            }
            //style.ForegroundColor = Color.FromArgb(255, 255, 255);//设置背景色
            style.Pattern = BackgroundType.Solid; //设置背景样式
            style.IsTextWrapped = true;//单元格内容自动换行
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin; //应用边界线 左边界线
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin; //应用边界线 右边界线
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin; //应用边界线 上边界线
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin; //应用边界线 下边界线
            return style;
        }

        #region 发送邮件：此方法可行
        public bool isMailAddress(string address)
        {
            if (address == null || address.Equals(""))
                return false;
            if (address.Contains("@") && address.Contains(".com"))
                return true;
            else
                return false;
        }

        public bool SendEmail(MailMessage message)
        {
            message.From = new MailAddress("qiaoqianxiong@126.com", "qiao1377");//必须是提供smtp服务的邮件服务器 
            //message.To.Add(new MailAddress(""));
            message.Subject = "测试邮件";
            //message.CC.Add(new MailAddress("test@126.com"));
            //message.Bcc.Add(new MailAddress("test@126.com"));
            message.IsBodyHtml = true;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = "询报价邮件发送测试";
            //Attachment 附件
            //Attachment att = new Attachment(@"E:/迅雷下载/20151229214321.xls");
            //message.Attachments.Add(att);//添加附件

            message.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient("smtp.126.com", 25); // 587;//Gmail使用的端口 
            client.Credentials = new System.Net.NetworkCredential("qiaoqianxiong@126.com", "qiao1377"); //这里是申请的邮箱和密码 
            client.EnableSsl = true; //必须经过ssl加密 
            try
            {
                client.Send(message);
                MessageBox.Show("邮件已经成功发送到" + message.To.ToString());
                return true;
            }
            catch (Exception ee)
            {
                
                MessageBox.Show(ee.Message  /* + ee.InnerException.Message*/ +"ddddd");
                return false;
            }
        }
        #endregion

        #region 接收邮件：此方法可行
        public void getMessage()
        {

            //TcpClient mtcpClient = new TcpClient();
            //IList<Mail_Message> mailList = new List<Mail_Message>();
            //try 
            //{
            //    using (POP3_Client client = new POP3_Client())
            //    {
            //        client.Connect("pop.qq.com", 110, true);
            //        client.Authenticate("342706245@qq.com", "Wj825107",false);
            //        POP3_ClientMessageCollection coll = client.Messages;
            //        MessageBox.Show("dengluchenggong");
            //        if (coll.Count == 0)
            //            MessageBox.Show("邮件为空");
            //        for (int i = 0; i < coll.Count; i++)
            //        {
            //            POP3_ClientMessage message = coll[i];
            //            Mail_Message mm = Mail_Message.ParseFromByte(coll[i].MessageToByte());
            //            mailList.Add(mm);
            //        }
            //    }
            //    //LumiSoft.Net.Mail.Mail_t_Attachment att;
            //    foreach (Mail_Message mail in mailList)
            //    {
            //        StringBuilder sb = new StringBuilder();
            //        sb.Append(mail.From.ToString()).Append("  发送给  ");
            //        sb.Append(mail.To.ToString()).Append("<br/>");
            //        sb.Append(mail.Subject).Append("<br/>");
            //        sb.Append(mail.BodyHtmlText).Append("<hr/>");
            //        MessageBox.Show(sb.ToString());
            //    }
            //}
            //catch (Exception ee)
            //{
            //    MessageBox.Show(ee.Message + "<br>" /* + ee.InnerException.Message*/ );
            //}

        }
        //protected void getMessage()
        //{

        //    jmail.POP3Class popMail = new jmail.POP3Class();//建立收邮件对象
        //    jmail.Message mailMessage; //建立邮件信息接口
        //    jmail.Attachments atts;//建立附件集接口
        //    jmail.Attachment att;//建立附件接口
        //    try
        //    {
        //        popMail.Connect("342706245@qq.com.com", "Wj825107", "pop.qq.com", 110);
        //        if (0 < popMail.Count) //如果收到邮件
        //        {
        //            for (int i = 1; i <= popMail.Count; i++) //根据取到的邮件数量依次取得每封邮件
        //            {
        //                mailMessage = popMail.Messages[i]; //取得一条邮件信息
        //                atts = mailMessage.Attachments; //取得该邮件的附件集合
        //                mailMessage.Charset = "GB2312"; //设置邮件的编码方式
        //                mailMessage.Encoding = "Base64"; //设置邮件的附件编码方式
        //                mailMessage.ISOEncodeHeaders = false; //是否将信头编码成iso-8859-1字符集
        //                // priority.Text = mailMessage.Priority.ToString(); //邮件的优先级
        //                string sendAdress = mailMessage.From; //邮件的发送人的信箱地址
        //                string sendName = mailMessage.FromName; //邮件的发送人
        //                string sendTheme = mailMessage.Subject; //邮件主题
        //                string sendContent= mailMessage.Body; //邮件内容
        //                // txtSize.Text = mailMessage.Size.ToString(); //邮件大小
        //                MessageBox.Show(sendAdress + sendName + sendTheme + sendContent);
        //                for (int j = 0; j < atts.Count; j++)
        //                {
        //                    att = atts[j]; //取得附件
        //                    string attname = att.Name; //附件名称
        //                    att.SaveToFile("E:\\迅雷下载\\" + attname); //上传到服务器
        //                    MessageBox.Show("保存至" + "E:\\迅雷下载\\" + attname);
        //                }

        //            }
        //            //panMailInfo.Visible = true;
        //            att = null;
        //            atts = null;
        //        }
        //        else
        //        {
        //            MessageBox.Show("没有新邮件!");
        //        }
        //        popMail.DeleteMessages();
        //        popMail.Disconnect();
        //        popMail = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Warning!请检查邮件服务器的设置是否正确！" + ex.ToString());
        //    }
        //}
        #endregion 
    }
}
