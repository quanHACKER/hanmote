﻿namespace MMClient.progress
{
    partial class progressDownLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.文件上传 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 66);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "正在下载....";
            // 
            // 文件上传
            // 
            this.文件上传.Location = new System.Drawing.Point(78, 26);
            this.文件上传.Name = "文件上传";
            this.文件上传.Size = new System.Drawing.Size(269, 23);
            this.文件上传.TabIndex = 2;
            // 
            // progressDownLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 104);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.文件上传);
            this.Name = "progressDownLoad";
            this.Text = "下载进度";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar 文件上传;
    }
}