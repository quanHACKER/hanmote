﻿using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.progress
{
    public partial class progressDownLoad : Form
    {

        string Message;
        FTPHelper fTPHelper = new FTPHelper("");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fTPHelper">ftp公共类</param>
        /// <param name="fileName">上传文档名字，可多文档上传</param>
        /// <param name="filePath">上传服务器目录</param>
        public progressDownLoad(string pathName,string filePath,CheckedListBox checkedListBox)
        {
            InitializeComponent();

            文件上传.Maximum = 100;//设置最大长度值
            文件上传.Value = 0;//设置当前值
            文件上传.Step = 15;//设置没次增长多少
           
                
                
                for (int i = 0; i < checkedListBox.Items.Count; i++)
                {
                    if (checkedListBox.GetItemChecked(i))
                    {
                        string filename = checkedListBox.GetItemText(checkedListBox.Items[i]);
                        Message = fTPHelper.Download(pathName, filename, filePath + filename);
                        
                        文件上传.Value += 文件上传.Step; //让进度条增加一次
                    }

                }
                文件上传.Value = 100;
            label1.Text = Message;

            
 
        }

    }
}
