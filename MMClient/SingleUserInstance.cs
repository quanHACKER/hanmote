﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Lib.Model.SystemConfig;

namespace MMClient
{
    public class SingleUserInstance
    {
        /// <summary>
        /// 用户基本信息实例
        /// </summary>
        private static BaseUserModel userInfor = new BaseUserModel();

        /// <summary>
        /// 获取用户基本信息实例
        /// </summary>
        /// <returns></returns>
        public static BaseUserModel getCurrentUserInstance()
        {
            return userInfor;
        }

        /// <summary>
        /// 重置用户基本信息Model
        /// </summary>
        public static void clearBaseUserModel()
        {
            userInfor.User_ID = null;
            userInfor.Login_Name = null;
            userInfor.Login_Password = null;
            userInfor.Username = null;
            userInfor.Gender = 0;
            userInfor.Birthday = null;
            userInfor.Birth_Place = null;
            userInfor.ID_Number = null;
            userInfor.Mobile = null;
            userInfor.Email = null;
            userInfor.User_Description = null;
            userInfor.Question = null;
            userInfor.Answer_Question = null;
            userInfor.Enabled = 0;
            userInfor.Manager_Flag = 0;
            userInfor.Generate_Time = null;
            userInfor.Generate_User_ID = null;
            userInfor.Generate_Username = null;
            userInfor.Modify_Time = null;
            userInfor.Modify_User_ID = null;
            userInfor.Modify_Username = null;
        }
    }
}
