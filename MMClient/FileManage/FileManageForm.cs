﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.FileManageBLL;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;

namespace MMClient.FileManage
{
    public partial class FileManageForm : DockContent
    {
        # region 子窗体

        NewFileTypeForm newFileTypeForm = null;           //文件类型管理窗口
        NewFileForm newFileForm = null;                   //新建文件窗口
        NewFileForm displayFileForm = null;               //预览文件窗口
        NewFileForm editFileForm = null;    

        # endregion

        private FileBLL fileTool = new FileBLL();                   //文件业务逻辑处理工具
        private FileTypeBLL fileTypeTool = new FileTypeBLL();       //文件类型业务逻辑处理工具
        List<File_Info> displayFileList = new List<File_Info>();
        private FTPTool ftp = FTPTool.getInstance();                //ftp工具

        public FileManageForm()
        {
            InitializeComponent();

            initialForm();
        }

        /// <summary>
        /// 初始化Form
        /// </summary>
        private void initialForm() { 
            //初始化文件类型ComboBox
            List<string> fileTypeNameList = new List<string>();
            fileTypeNameList.Add("全部类型");
            fileTypeNameList.AddRange(fileTypeTool.getAllFileTypeName());
            this.cbxFileType.DataSource = fileTypeNameList;

        }

        /// <summary>
        /// 新建文档
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbNew_Click(object sender, EventArgs e)
        {
            if (this.newFileForm == null || this.newFileForm.IsDisposed) {
                this.newFileForm = new NewFileForm();
            }
            SingletonUserUI.addToUserUI(this.newFileForm);
        }

        /// <summary>
        /// 文档类型管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbFileTypeManage_Click(object sender, EventArgs e)
        {
            if (this.newFileTypeForm == null || this.newFileTypeForm.IsDisposed)
            {
                this.newFileTypeForm = new NewFileTypeForm();
            }
            SingletonUserUI.addToUserUI(this.newFileTypeForm);
        }

        /// <summary>
        /// 点击查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;
            if (beginTime > endTime) {
                MessageUtil.ShowError("请正确选择起止时间！");
                return;
            }
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            if (!this.tbFileID.Text.Trim().Equals("")) { 
                conditions.Add("File_ID", this.tbFileID.Text.Trim());
            }
            if (!this.tbCreatorID.Text.Trim().Equals("")) {
                conditions.Add("Creator_ID", this.tbCreatorID.Text.Trim());
            }
            if (!this.cbxFileType.Text.Equals("全部类型")) {
                conditions.Add("File_Type", this.cbxFileType.Text);
            }

            //查询
            displayFileList = fileTool.getFileInfoListByConditions(
                conditions, beginTime, endTime);
            displayList(displayFileList);
        }

        /// <summary>
        /// 展示列表
        /// </summary>
        /// <param name="fileList"></param>
        private void displayList(List<File_Info> fileList) {
            this.dgvFileInfo.Rows.Clear();
            foreach (File_Info file in fileList)
            {
                this.dgvFileInfo.Rows.Add();
                DataGridViewRow row = this.dgvFileInfo.Rows[this.dgvFileInfo.Rows.Count - 1];
                row.Cells[1].Value = file.File_ID;
                row.Cells[2].Value = file.File_Type;
                row.Cells[3].Value = file.Creator_ID;
                row.Cells[4].Value = file.Creator_Type;
                row.Cells[5].Value = file.Create_Time;
            }
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvFileInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    fileIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 1)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];
            File_Info previewFile = null;
            foreach (File_Info file in displayFileList) {
                if (file.File_ID.Equals(fileName)) {
                    previewFile = file;
                    break;
                }
            }
            if (previewFile != null) {
                if (this.displayFileForm == null || this.displayFileForm.IsDisposed)
                {
                    this.displayFileForm = new NewFileForm(previewFile, "display");
                }
                SingletonUserUI.addToUserUI(this.displayFileForm);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbEdit_Click(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvFileInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    fileIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择修改文件");
                return;
            }
            if (fileIDList.Count > 1)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];
            File_Info editFile = null;
            foreach (File_Info file in displayFileList)
            {
                if (file.File_ID.Equals(fileName))
                {
                    editFile = file;
                    break;
                }
            }
            if (editFile != null)
            {
                if (this.editFileForm == null || this.editFileForm.IsDisposed)
                {
                    this.editFileForm = new NewFileForm(editFile, "edit");
                }
                SingletonUserUI.addToUserUI(this.editFileForm);
            }
        }

        /// <summary>
        /// 点击删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            bool flag1 = true, flag2 = true, flag3 = true;
            List<string> fileList = new List<string>();
            List<string> dirList = new List<string>();
            foreach (DataGridViewRow row in this.dgvFileInfo.Rows)
            {
                if (row.Cells[0].EditedFormattedValue.ToString().Equals("True"))
                {
                    fileList.Add(row.Cells[1].Value.ToString());
                    dirList.Add(row.Cells[2].Value.ToString() + @"/" + row.Cells[1].Value.ToString());
                }
            }
            if (fileList.Count == 0)
            {
                MessageBox.Show("请至少选择一项要删除的文件！");
                return;
            }
            DialogResult re = MessageUtil.ShowYesNoCancelAndTips("删除后将无法恢复文件，确认删除吗？");
            if (re == DialogResult.Yes)
            {
                #region 删除文件
                List<string> fileAttachmentList = new List<string>();
                foreach(string file in fileList)
                {
                    List<string> temp = getFileAttachmentByFileID(file);
                    if (temp != null && temp.Count != 0)
                    {
                        fileAttachmentList.AddRange(temp);
                    }
                }
                List<string> failList = ftp.deleteFileList(fileAttachmentList);
                if (failList.Count == 0)
                {
                    //MessageBox.Show("文件删除成功！");
                    flag1 = true;
                }
                else
                {
                    MessageBox.Show(failList.Count + "个附件删除失败！请检查后重试！");
                    flag1 = false;
                }
                #endregion
                #region 删除文件夹
                if (flag1)
                {
                    foreach (string dir in dirList)
                    {
                        MessageBox.Show(dir);
                        flag2 = flag2 && ftp.removeDirectory(dir);
                    }
                }
                #endregion
                #region 删除数据库记录
                if (flag2)
                {
                    foreach (string file in fileList)
                    {
                        int rowCount = fileTool.deleteInfo(file);
                        bool f = rowCount >= 0 ? true : false;
                        flag3 = flag3 && f;
                    }
                }
                #endregion
                if (flag1&&flag2&&flag3)
                {
                    MessageBox.Show("文件删除成功！");
                }
                else if (flag1 && flag2)
                {
                    MessageBox.Show("文件删除成功，数据库记录删除失败，请联系DBA手动删除！");
                }
                this.btnSearch.PerformClick();
            }
            else
                return;
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            this.btnSearch.PerformClick();
        }

        private List<string> getFileAttachmentByFileID(string fileID)
        {
            List<string> result = new List<string>();
            List<File_Attachment_Info> aList = fileTool.getFileAttachmentList(fileID);
            if (aList != null && aList.Count != 0)
            {
                foreach (File_Attachment_Info a in aList)
                    result.Add(a.Attachment_Location);
            }
            return result;
        }
    }
}
