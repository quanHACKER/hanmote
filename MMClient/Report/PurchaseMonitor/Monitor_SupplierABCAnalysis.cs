﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using ChartDirector;
using MMClient.RA;
using MMClient.RA.ChartClass;
using Lib.Common.CommonUtils;

namespace MMClient.RA.PurchaseMonitor
{
    public partial class Monitor_SupplierABCAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 与打印相关类声明
        ChartPrinter MyChartPrinter;
        DataGridViewPrinter MyDataGridViewPrinter;
        #endregion

        PM_SupplierABC pmsabc = new PM_SupplierABC();

        #region 窗体初始化程序
        public Monitor_SupplierABCAnalysis()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";

            this.cbb_Type.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表按钮事件
        /// <summary>
        /// 生成报表按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            string sqlText = null;
            sqlText = @"WITH T AS(SELECT Supplier_Base.Supplier_Name AS Name, SUM([Order].Price*[Order].Material_Count) AS TotalPrice, ROW_NUMBER() OVER(Order BY SUM([Order].Price*[Order].Material_Count) DESC) AS RowNum FROM [Order] JOIN Supplier_Base ON [Order].Supplier_ID = Supplier_Base.Supplier_ID GROUP BY Supplier_Base.Supplier_Name) SELECT Name,  TotalPrice, TotalPrice / SUM(TotalPrice) OVER() AS Share, (SELECT SUM(TotalPrice)/(SELECT SUM(TotalPrice)FROM T) FROM T b WHERE b.RowNum<=a.RowNum) AS CumShare, (SELECT COUNT(RowNum)/ CAST((SELECT COUNT(RowNum) FROM T) as decimal(4,3) )FROM T c WHERE c.RowNum<=a.RowNum) AS QCumShare FROM T a";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.DataGridShow(dt);
                WinChartViewer viewer = this.chartViewer1;
                this.SupplierABCAnalysisChart(dt, viewer);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        #endregion

        #region 与DataGridView操作有关的函数
        /// <summary>
        /// 将数据绑定到datagridview并显示
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        { 
            if(dt != null)
            {
                if(dt.Rows.Count >= 0)
                {
                    this.dataGridView1.DataSource = dt;   //将数据绑定到datagridview
                    this.DataGridColor();
                }
            }
        }
        /// <summary>
        /// 根据不同条件，该列显示不同颜色
        /// </summary>
        private void DataGridColor()
        {
            double temp = 0;
            for(int i=0; i < this.dataGridView1.Rows.Count; i++)
            {
                if(this.dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                    if(temp <= 0.75)
                    {
                        this.dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.GreenYellow;
                    }
                    else if (temp > 0.75 && temp <= 0.92)
                    {
                        this.dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Yellow;
                    }
                    else
                    {
                        this.dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Pink;
                    }
                }
            }
        }
        #endregion

        #region 绘制ABC类分析图表函数
        /// <summary>
        /// 画出供应商ABC类分析图
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="viewer"></param>
        private void SupplierABCAnalysisChart(DataTable dt,WinChartViewer viewer)
        {
            this.pmsabc.createChart(dt, viewer);
        }
        #endregion

        #region 图表钻取事件
        /// <summary>
        /// 显示钻取窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chartViewer1_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender,e);
        }
        #endregion

        #region 绘制DataGridView行号
        /// <summary>
        /// 绘制datagridview左边的行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        #endregion

        /*

        #region 分别显示A、B、C类物料按钮事件
        /// <summary>
        /// 点击A供应商按钮，显示不用颜色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_AMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
            cm.SuspendBinding();//挂起数据绑定
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                    if (temp <= 0.75)
                    {
                        this.dataGridView1.Rows[i].Visible = true;
                    }
                    else if (temp > 0.75 && temp <= 0.92)
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                    else
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                 }
             }
            cm.ResumeBinding();//恢复数据绑定
        }

        /// <summary>
        /// 点击B供应商按钮，显示不用颜色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_BMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
            cm.SuspendBinding();
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                    if (temp <= 0.75)
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                    else if (temp > 0.75 && temp <= 0.92)
                    {
                        this.dataGridView1.Rows[i].Visible = true;
                    }
                    else
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                }
            }
            cm.ResumeBinding();
        }

        /// <summary>
        /// 点击C供应商按钮，显示不用颜色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_CMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
            cm.SuspendBinding();
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                    if (temp <= 0.75)
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                    else if (temp > 0.75 && temp <= 0.92)
                    {
                        this.dataGridView1.Rows[i].Visible = false;
                    }
                    else
                    {
                        this.dataGridView1.Rows[i].Visible = true;
                    }
                }
            }
            cm.ResumeBinding();
        }
        #endregion
         * */

        #region 供应商ABC类分析明细表打印程序
        /// <summary>
        /// 供应商ABC类分析明细表打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_PrintDetailChart_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                DetailChartPrintDocument.Print();
            }
        }

        /// <summary>
        /// 供应商ABC类分析明细表打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_PreviewDetailChart_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = DetailChartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        /// <summary>
        /// 供应商ABC类分析明细表打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetailChartPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        private bool SetupThePrinting()
        {
            DetailChartPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.label_DetaiChart.Text.Trim();
            DetailChartPrintDocument.DocumentName = titleName;
            DetailChartPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            DetailChartPrintDocument.DefaultPageSettings = DetailChartPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dataGridView1, DetailChartPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
        #endregion

        #region 供应商ABC类分析图打印程序
        /// <summary>
        /// 供应商ABC类分析图打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_PrintChart_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                this.ChartPrintDocument.Print();
            }
        }

        /// <summary>
        /// 供应商ABC类分析图打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_PreviewChart_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = ChartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        /// <summary>
        /// 供应商ABC类分析图打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }

        /// <summary>
        /// 将图表转换成一张图片
        /// </summary>
        /// <returns></returns>
        private bool TransforToImage()
        {
            ChartPageSetupDialog.ShowDialog();
            //允许用户选择打印机
            PrintDialog chartPrintDialog = new PrintDialog();
            chartPrintDialog.AllowCurrentPage = true;
            chartPrintDialog.AllowPrintToFile = true;
            chartPrintDialog.AllowSelection = true;
            chartPrintDialog.AllowSomePages = true;
            chartPrintDialog.PrintToFile = false;
            chartPrintDialog.ShowHelp = true;
            chartPrintDialog.ShowHelp = true;
            if (chartPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //获取打印文档名称
            string titleName = this.lb_ChartTitle.Text.Trim();
            ChartPrintDocument.DocumentName = titleName;
            ChartPrintDocument.PrinterSettings = chartPrintDialog.PrinterSettings;
            ChartPrintDocument.DefaultPageSettings = ChartPageSetupDialog.PageSettings;

            //this.panel_Chart.BackColor = Color.White;
            Bitmap bitmap = new Bitmap(this.panel_Chart.Width, this.panel_Chart.Height);
            this.panel_Chart.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height));
            MyChartPrinter = new ChartPrinter(bitmap, ChartPrintDocument, true, true, titleName, new Font("宋体", 14, FontStyle.Bold, GraphicsUnit.Point), Color.Black);
            return true;
        }
        #endregion

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dataGridView1.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dataGridView1.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void btn_ASupplier_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
                cm.SuspendBinding();//挂起数据绑定
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                        if (temp <= 0.75)
                        {
                            this.dataGridView1.Rows[i].Visible = true;
                        }
                        else if (temp > 0.75 && temp <= 0.92)
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                        else
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();//恢复数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btnBASupplier_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
                cm.SuspendBinding();
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                        if (temp <= 0.75)
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                        else if (temp > 0.75 && temp <= 0.92)
                        {
                            this.dataGridView1.Rows[i].Visible = true;
                        }
                        else
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btn_CSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[this.dataGridView1.DataSource];
                cm.SuspendBinding();
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    if (this.dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(this.dataGridView1.Rows[i].Cells[3].Value);
                        if (temp <= 0.75)
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                        else if (temp > 0.75 && temp <= 0.92)
                        {
                            this.dataGridView1.Rows[i].Visible = false;
                        }
                        else
                        {
                            this.dataGridView1.Rows[i].Visible = true;
                        }
                    }
                }
                cm.ResumeBinding();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
    }
}
