﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using ChartDirector;
using MMClient.RA;
using MMClient.RA.ChartClass;
using Lib.Common.CommonUtils;

namespace MMClient.RA.PurchaseMonitor
{
    public partial class Monitor_MaterialABCAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 与打印相关类声明
        ChartPrinter MyChartPrinter;
        DataGridViewPrinter MyDataGridViewPrinter;
        #endregion

        PM_MaterialABC pmmabc = new PM_MaterialABC();

        #region 窗体初试化程序
        public Monitor_MaterialABCAnalysis()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";

            //窗口初始化时，复选框选中第0个
            this.cbb_Type.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表按钮事件
        /// <summary>
        /// 点击查询按钮，显示数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            string sqlText = null;
            if(this.cbb_Type.SelectedIndex == 0)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_Name AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  ";
   //             dt1 = DBHelper.ExecuteQueryDT(sqlText);
            }
            else if (this.cbb_Type.SelectedIndex == 1)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_Name AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) < 0.75";
                
            }
            else if (this.cbb_Type.SelectedIndex == 2)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_Name AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) >= 0.75 and (select SUM(Share) from T b where b.ROW<=a.ROW) < 0.92";
            }
            else
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_Name AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) >= 0.92 and (select SUM(Share) from T b where b.ROW<=a.ROW) < 1.0";
            }
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.DataGridShow(dt);
                WinChartViewer viewer = this.chartViewer1;
                this.MaterialABCAnalysisChart(dt, viewer);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        #endregion

        #region 与DataGridView操作有关的函数
        /// <summary>
        /// 根据不同条件，datagridview某列显示不同颜色
        /// </summary>
        private void DataGridColor()
        {
            double temp = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.GreenYellow;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Yellow;
                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Pink;
                    }
                }
            }
        }
        /// <summary>
        /// 将数据绑定到datagridview上
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
               if(dt != null)
               {
                   if(dt.Rows.Count > 0)
                   {
                       this.dataGridView1.DataSource = dt;
                       this.DataGridColor();
                   }
               }
        }
        #endregion

        #region 绘制ABC类分析图表函数
        /// <summary>
        /// 绘制ABC类分析图
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="viewer"></param>
        private void MaterialABCAnalysisChart(DataTable dt,WinChartViewer viewer)
        {
            this.pmmabc.createChart(dt, viewer);
        }

        #endregion

        #region 图表钻取事件
        /// <summary>
        /// 显示钻取窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chartViewer1_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender,e);
        }
        #endregion

        #region 绘制DataGridView行号
        /// <summary>
        /// 绘制datagridview左边固定的行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        #endregion

        /*
        #region 分别显示A、B、C类物料按钮事件
        /// <summary>
        /// 只显示A类物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_AMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dataGridView1.Rows[i].Visible = true;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                    else
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                }
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }

        /// <summary>
        /// 只显示B类物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_BMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dataGridView1.Rows[i].Visible = true;
                    }
                    else
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                } 
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }

        /// <summary>
        /// 只显示C类物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_CMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                    else
                    {
                        dataGridView1.Rows[i].Visible =true;
                    }
                }
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }
        #endregion
         * */

        #region 物料ABC类分析图打印程序
        /// <summary>
        /// 物料ABC类分析图打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Print_Click_1(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                this.chartPrintDocument.Print();
            }
            //this.panel_Chart.BackColor = Color.White;
        }
        /// <summary>
        /// 物料ABC类分析图打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }
        /// <summary>
        /// 物料ABC类分析图预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = chartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        /// <summary>
        /// 将图表转换成一张图片
        /// </summary>
        /// <returns></returns>
        private bool TransforToImage()
        {
            chartPageSetupDialog.ShowDialog();
            //允许用户选择打印机
            PrintDialog chartPrintDialog = new PrintDialog();
            chartPrintDialog.AllowCurrentPage = true;
            chartPrintDialog.AllowPrintToFile = true;
            chartPrintDialog.AllowSelection = true;
            chartPrintDialog.AllowSomePages = true;
            chartPrintDialog.PrintToFile = false;
            chartPrintDialog.ShowHelp = true;
            chartPrintDialog.ShowHelp = true;
            if (chartPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //获取打印文档名称
            string titleName = this.lb_ChartTitle.Text.Trim();
            chartPrintDocument.DocumentName = titleName;
            chartPrintDocument.PrinterSettings = chartPrintDialog.PrinterSettings;
            chartPrintDocument.DefaultPageSettings = chartPageSetupDialog.PageSettings;

            //this.panel_Chart.BackColor = Color.White;
            Bitmap bitmap = new Bitmap(this.panel_Chart.Width, this.panel_Chart.Height);
            this.panel_Chart.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height));
            MyChartPrinter = new ChartPrinter(bitmap, chartPrintDocument, true, true, titleName, new Font("宋体", 14, FontStyle.Bold, GraphicsUnit.Point), Color.Black);
            return true;
        }
        #endregion

        #region 物料ABC类分析明细表打印程序
        /// <summary>
        /// 物料ABC类分析明细表打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DetailChartPrint_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                detailPrintDocument.Print();
            }
        }

        /// <summary>
        /// 物料ABC类分析明细表打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void detailPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        /// <summary>
        /// 物料ABC类分析明细表打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DetailChartPreview_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = detailPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        private bool SetupThePrinting()
        {
            detailPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.label_DetaiChart.Text.Trim();
            detailPrintDocument.DocumentName = titleName;
            detailPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            detailPrintDocument.DefaultPageSettings = detailPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dataGridView1, detailPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
        #endregion

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dataGridView1.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dataGridView1.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void btn_AMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dataGridView1.Rows[i].Visible = true;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                        else
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btn_BMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {

                    if (dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dataGridView1.Rows[i].Visible = true;
                        }
                        else
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btn_CMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dataGridView1.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                        else
                        {
                            dataGridView1.Rows[i].Visible = true;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
    }
}
