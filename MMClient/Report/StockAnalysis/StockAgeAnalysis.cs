﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using MMClient.RA;
using Lib.Common.CommonUtils;

namespace MMClient.RA.StockAnalysis
{
    public partial class StockAgeAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 与打印相关类声明
        //打印类
        DataGridViewPrinter MyDataGridViewPrinter;
        #endregion

        #region 窗体初试化
        public StockAgeAnalysis()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_StockAgeAnalysis.TopLeftHeaderCell.Value = "行号";

            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表按钮事件
        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_CreateReport_Click(object sender, EventArgs e)
        {
            string dateTime = this.dtp_BaseDate.Value.ToString("yyyy-MM-dd");
            string sqlText = "SELECT Stock.Stock_Name,Material.Material_ID,Material.Material_Name,GoodsReceiveNote.Receive_count,GoodsReceiveNote.Receive_count*[Order].Price AS TotalAmount,GoodsReceiveNote.StockIn_Date,CAST(DATEDIFF(day,GoodsReceiveNote.StockIn_Date,CONVERT(datetime,'" + dateTime + "')) AS nvarchar(50)) AS DaysL FROM GoodsReceiveNote JOIN Stock ON GoodsReceiveNote.Stock_ID=Stock.Stock_ID JOIN Material ON GoodsReceiveNote.Material_ID=Material.Material_ID JOIN [Order] ON GoodsReceiveNote.Order_ID=[Order].Order_ID ORDER BY DaysL DESC";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.DataGridShow(dt);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        #endregion

        #region 与DataGridView操作相关的方法
        /// <summary>
        /// 将数据绑定在DataGridView上
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dgv_StockAgeAnalysis.DataSource = dt;
                    this.DataGridColor();
                }
            }
        }
        /// <summary>
        /// 根据不同条件，datagridview某列显示不同颜色
        /// </summary>
        private void DataGridColor()
        {
            double temp = 0;
            string str1 = "超期";
            string str2 = "天";
            for (int i = 0; i < dgv_StockAgeAnalysis.Rows.Count; i++)
            {
                if (dgv_StockAgeAnalysis.Rows[i].Cells[6].Value != null)
                {
                    temp = Convert.ToDouble(dgv_StockAgeAnalysis.Rows[i].Cells[6].Value);
                    if (temp <= 0)
                    {
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Value = "未超期";
                        //dgv_StockAgeAnalysis.Rows[i].Cells[6].Style.BackColor = System.Drawing.Color.GreenYellow;
                    }
                    else if (temp > 0 && temp <= 30)
                    {
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Value = str1 + temp + str2;
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Style.BackColor = System.Drawing.Color.Yellow;
                    }
                    else if (temp > 30 && temp <= 180)
                    {
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Value = str1 + temp + str2;
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Style.BackColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Value = str1 + temp + str2;
                        dgv_StockAgeAnalysis.Rows[i].Cells[6].Style.BackColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
        #endregion

        #region 库龄分析明细表打印程序
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Print_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                MyPrintDocument.Print();
            }
        }
        /// <summary>
        /// 预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = MyPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }
        /// <summary>
        /// PrintDocument的打印程序
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        private bool SetupThePrinting()
        {
            MyPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.lb_Title.Text.Trim();
            MyPrintDocument.DocumentName = titleName;
            MyPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            MyPrintDocument.DefaultPageSettings = MyPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dgv_StockAgeAnalysis, MyPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
        #endregion

        #region 绘制DataGridView行号事件
        /// <summary>
        /// 绘制行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockAgeAnalysis_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        #endregion
    }
}
