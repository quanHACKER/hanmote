﻿namespace MMClient.RA.StockAnalysis
{
    partial class StockABCAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.cbb_Type = new System.Windows.Forms.ComboBox();
            this.lb_Type = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.dgv_DetailChart = new System.Windows.Forms.DataGridView();
            this.Column_ProductCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_InvoiceValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Share = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumQShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.detailChartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.detailChartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.chartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.chartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_DetailChartPreview = new System.Windows.Forms.Button();
            this.label_DetaiChart = new System.Windows.Forms.Label();
            this.btn_DetailChartPrint = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_CMaterial = new System.Windows.Forms.Button();
            this.btn_BMaterial = new System.Windows.Forms.Button();
            this.btn_AMaterial = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.lb_ChartTitle = new System.Windows.Forms.Label();
            this.btn_Print = new System.Windows.Forms.Button();
            this.gb_Query.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DetailChart)).BeginInit();
            this.panel_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Query.Controls.Add(this.cbb_Type);
            this.gb_Query.Controls.Add(this.lb_Type);
            this.gb_Query.Controls.Add(this.btn_Query);
            this.gb_Query.Controls.Add(this.dtp_EndTime);
            this.gb_Query.Controls.Add(this.lb_To);
            this.gb_Query.Controls.Add(this.dtp_StartTime);
            this.gb_Query.Controls.Add(this.lb_Time);
            this.gb_Query.Location = new System.Drawing.Point(6, 7);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(1160, 84);
            this.gb_Query.TabIndex = 1;
            this.gb_Query.TabStop = false;
            this.gb_Query.Text = "查询条件";
            // 
            // cbb_Type
            // 
            this.cbb_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Type.FormattingEnabled = true;
            this.cbb_Type.Items.AddRange(new object[] {
            "所有",
            "A物料",
            "B物料",
            "C物料"});
            this.cbb_Type.Location = new System.Drawing.Point(387, 33);
            this.cbb_Type.Name = "cbb_Type";
            this.cbb_Type.Size = new System.Drawing.Size(125, 20);
            this.cbb_Type.TabIndex = 6;
            // 
            // lb_Type
            // 
            this.lb_Type.AutoSize = true;
            this.lb_Type.Location = new System.Drawing.Point(350, 37);
            this.lb_Type.Name = "lb_Type";
            this.lb_Type.Size = new System.Drawing.Size(41, 12);
            this.lb_Type.TabIndex = 5;
            this.lb_Type.Text = "类别：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(577, 28);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(89, 28);
            this.btn_Query.TabIndex = 4;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(204, 33);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(181, 37);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(53, 33);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 1;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(15, 37);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 0;
            this.lb_Time.Text = "时间：";
            // 
            // dgv_DetailChart
            // 
            this.dgv_DetailChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_DetailChart.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_DetailChart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_DetailChart.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_DetailChart.ColumnHeadersHeight = 30;
            this.dgv_DetailChart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_ProductCategory,
            this.Column_InvoiceValue,
            this.Column_Share,
            this.Column_CumShare,
            this.Column_CumQShare});
            this.dgv_DetailChart.EnableHeadersVisualStyles = false;
            this.dgv_DetailChart.Location = new System.Drawing.Point(6, 196);
            this.dgv_DetailChart.Name = "dgv_DetailChart";
            this.dgv_DetailChart.RowTemplate.Height = 23;
            this.dgv_DetailChart.Size = new System.Drawing.Size(486, 460);
            this.dgv_DetailChart.TabIndex = 6;
            this.dgv_DetailChart.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_DetailChart_RowPostPaint);
            // 
            // Column_ProductCategory
            // 
            this.Column_ProductCategory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_ProductCategory.DataPropertyName = "Name";
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Column_ProductCategory.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_ProductCategory.HeaderText = "产品类别";
            this.Column_ProductCategory.Name = "Column_ProductCategory";
            this.Column_ProductCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_InvoiceValue
            // 
            this.Column_InvoiceValue.DataPropertyName = "TotalPrice";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Column_InvoiceValue.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_InvoiceValue.HeaderText = "库存金额";
            this.Column_InvoiceValue.Name = "Column_InvoiceValue";
            this.Column_InvoiceValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_Share
            // 
            this.Column_Share.DataPropertyName = "Share";
            dataGridViewCellStyle4.Format = "0.00%";
            dataGridViewCellStyle4.NullValue = null;
            this.Column_Share.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column_Share.HeaderText = "占比";
            this.Column_Share.Name = "Column_Share";
            this.Column_Share.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumShare
            // 
            this.Column_CumShare.DataPropertyName = "CumShare";
            dataGridViewCellStyle5.Format = "0.00%";
            dataGridViewCellStyle5.NullValue = null;
            this.Column_CumShare.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column_CumShare.HeaderText = "累计占比";
            this.Column_CumShare.Name = "Column_CumShare";
            this.Column_CumShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumQShare
            // 
            this.Column_CumQShare.DataPropertyName = "CumQShare";
            this.Column_CumQShare.HeaderText = "品种累计占比";
            this.Column_CumQShare.Name = "Column_CumQShare";
            this.Column_CumQShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_CumQShare.Visible = false;
            // 
            // panel_Chart
            // 
            this.panel_Chart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Chart.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Chart.Controls.Add(this.chartViewer1);
            this.panel_Chart.Location = new System.Drawing.Point(499, 139);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(667, 517);
            this.panel_Chart.TabIndex = 7;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(14, 15);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 2;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot_1);
            // 
            // detailChartPageSetupDialog
            // 
            this.detailChartPageSetupDialog.Document = this.detailChartPrintDocument;
            // 
            // detailChartPrintDocument
            // 
            this.detailChartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.detailChartPrintDocument_PrintPage);
            // 
            // chartPageSetupDialog
            // 
            this.chartPageSetupDialog.Document = this.chartPrintDocument;
            // 
            // chartPrintDocument
            // 
            this.chartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.chartPrintDocument_PrintPage);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_DetailChartPreview);
            this.groupBox1.Controls.Add(this.label_DetaiChart);
            this.groupBox1.Controls.Add(this.btn_DetailChartPrint);
            this.groupBox1.Location = new System.Drawing.Point(6, 150);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(487, 41);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // btn_DetailChartPreview
            // 
            this.btn_DetailChartPreview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPreview.Location = new System.Drawing.Point(426, 13);
            this.btn_DetailChartPreview.Name = "btn_DetailChartPreview";
            this.btn_DetailChartPreview.Size = new System.Drawing.Size(53, 23);
            this.btn_DetailChartPreview.TabIndex = 2;
            this.btn_DetailChartPreview.Text = "预览";
            this.btn_DetailChartPreview.UseVisualStyleBackColor = true;
            // 
            // label_DetaiChart
            // 
            this.label_DetaiChart.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_DetaiChart.Location = new System.Drawing.Point(4, 15);
            this.label_DetaiChart.Name = "label_DetaiChart";
            this.label_DetaiChart.Size = new System.Drawing.Size(220, 19);
            this.label_DetaiChart.TabIndex = 0;
            this.label_DetaiChart.Text = "库存物料ABC类分析明细表";
            // 
            // btn_DetailChartPrint
            // 
            this.btn_DetailChartPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPrint.Location = new System.Drawing.Point(370, 13);
            this.btn_DetailChartPrint.Name = "btn_DetailChartPrint";
            this.btn_DetailChartPrint.Size = new System.Drawing.Size(52, 23);
            this.btn_DetailChartPrint.TabIndex = 1;
            this.btn_DetailChartPrint.Text = "打印";
            this.btn_DetailChartPrint.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_CMaterial);
            this.groupBox3.Controls.Add(this.btn_BMaterial);
            this.groupBox3.Controls.Add(this.btn_AMaterial);
            this.groupBox3.Location = new System.Drawing.Point(6, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(486, 62);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // btn_CMaterial
            // 
            this.btn_CMaterial.Location = new System.Drawing.Point(319, 17);
            this.btn_CMaterial.Name = "btn_CMaterial";
            this.btn_CMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_CMaterial.TabIndex = 2;
            this.btn_CMaterial.Text = "C类物料";
            this.btn_CMaterial.UseVisualStyleBackColor = true;
            this.btn_CMaterial.Click += new System.EventHandler(this.btn_CMaterial_Click);
            // 
            // btn_BMaterial
            // 
            this.btn_BMaterial.Location = new System.Drawing.Point(176, 17);
            this.btn_BMaterial.Name = "btn_BMaterial";
            this.btn_BMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_BMaterial.TabIndex = 1;
            this.btn_BMaterial.Text = "B类物料";
            this.btn_BMaterial.UseVisualStyleBackColor = true;
            this.btn_BMaterial.Click += new System.EventHandler(this.btn_BMaterial_Click);
            // 
            // btn_AMaterial
            // 
            this.btn_AMaterial.Location = new System.Drawing.Point(33, 17);
            this.btn_AMaterial.Name = "btn_AMaterial";
            this.btn_AMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_AMaterial.TabIndex = 0;
            this.btn_AMaterial.Text = "A类物料";
            this.btn_AMaterial.UseVisualStyleBackColor = true;
            this.btn_AMaterial.Click += new System.EventHandler(this.btn_AMaterial_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btn_Preview);
            this.groupBox2.Controls.Add(this.lb_ChartTitle);
            this.groupBox2.Controls.Add(this.btn_Print);
            this.groupBox2.Location = new System.Drawing.Point(499, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(667, 45);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // btn_Preview
            // 
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(599, 15);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(53, 23);
            this.btn_Preview.TabIndex = 2;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            // 
            // lb_ChartTitle
            // 
            this.lb_ChartTitle.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ChartTitle.Location = new System.Drawing.Point(3, 17);
            this.lb_ChartTitle.Name = "lb_ChartTitle";
            this.lb_ChartTitle.Size = new System.Drawing.Size(156, 19);
            this.lb_ChartTitle.TabIndex = 0;
            this.lb_ChartTitle.Text = "库存物料ABC类分析图";
            // 
            // btn_Print
            // 
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(541, 15);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(52, 23);
            this.btn_Print.TabIndex = 1;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            // 
            // StockABCAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 668);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel_Chart);
            this.Controls.Add(this.dgv_DetailChart);
            this.Controls.Add(this.gb_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "StockABCAnalysis";
            this.Text = "库存物料ABC类分析";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DetailChart)).EndInit();
            this.panel_Chart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.ComboBox cbb_Type;
        private System.Windows.Forms.Label lb_Type;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.DataGridView dgv_DetailChart;
        private System.Windows.Forms.Panel panel_Chart;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.PageSetupDialog detailChartPageSetupDialog;
        private System.Windows.Forms.PageSetupDialog chartPageSetupDialog;
        private System.Drawing.Printing.PrintDocument detailChartPrintDocument;
        private System.Drawing.Printing.PrintDocument chartPrintDocument;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ProductCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_InvoiceValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Share;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumShare;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumQShare;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_DetailChartPreview;
        private System.Windows.Forms.Label label_DetaiChart;
        private System.Windows.Forms.Button btn_DetailChartPrint;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_CMaterial;
        private System.Windows.Forms.Button btn_BMaterial;
        private System.Windows.Forms.Button btn_AMaterial;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.Label lb_ChartTitle;
        private System.Windows.Forms.Button btn_Print;
    }
}