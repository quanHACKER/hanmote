﻿namespace MMClient.RA.StockAnalysis
{
    partial class StockAgeAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel_Condition = new System.Windows.Forms.Panel();
            this.dtp_BaseDate = new System.Windows.Forms.DateTimePicker();
            this.lb_BaseDate = new System.Windows.Forms.Label();
            this.btn_CreateReport = new System.Windows.Forms.Button();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.panel_TitleColumn = new System.Windows.Forms.Panel();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.lb_Title = new System.Windows.Forms.Label();
            this.panel_DataGridView = new System.Windows.Forms.Panel();
            this.dgv_StockAgeAnalysis = new System.Windows.Forms.DataGridView();
            this.Column_StockAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockMaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockMaterialQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockMaterialTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockInDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.MyPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.panel_Condition.SuspendLayout();
            this.panel_TitleColumn.SuspendLayout();
            this.panel_DataGridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockAgeAnalysis)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_Condition
            // 
            this.panel_Condition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Condition.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Condition.Controls.Add(this.dtp_BaseDate);
            this.panel_Condition.Controls.Add(this.lb_BaseDate);
            this.panel_Condition.Controls.Add(this.btn_CreateReport);
            this.panel_Condition.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_Condition.Controls.Add(this.lb_PurchaseGroup);
            this.panel_Condition.Controls.Add(this.cbb_MaterialGroup);
            this.panel_Condition.Controls.Add(this.lb_MaterialGroup);
            this.panel_Condition.Location = new System.Drawing.Point(1, 3);
            this.panel_Condition.Name = "panel_Condition";
            this.panel_Condition.Size = new System.Drawing.Size(1072, 87);
            this.panel_Condition.TabIndex = 0;
            // 
            // dtp_BaseDate
            // 
            this.dtp_BaseDate.Location = new System.Drawing.Point(476, 32);
            this.dtp_BaseDate.Name = "dtp_BaseDate";
            this.dtp_BaseDate.Size = new System.Drawing.Size(126, 21);
            this.dtp_BaseDate.TabIndex = 6;
            // 
            // lb_BaseDate
            // 
            this.lb_BaseDate.AutoSize = true;
            this.lb_BaseDate.Location = new System.Drawing.Point(410, 37);
            this.lb_BaseDate.Name = "lb_BaseDate";
            this.lb_BaseDate.Size = new System.Drawing.Size(65, 12);
            this.lb_BaseDate.TabIndex = 5;
            this.lb_BaseDate.Text = "基准日期：";
            // 
            // btn_CreateReport
            // 
            this.btn_CreateReport.Location = new System.Drawing.Point(694, 27);
            this.btn_CreateReport.Name = "btn_CreateReport";
            this.btn_CreateReport.Size = new System.Drawing.Size(107, 30);
            this.btn_CreateReport.TabIndex = 4;
            this.btn_CreateReport.Text = "生成报表";
            this.btn_CreateReport.UseVisualStyleBackColor = true;
            this.btn_CreateReport.Click += new System.EventHandler(this.btn_CreateReport_Click);
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(73, 33);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbb_PurchaseGroup.TabIndex = 3;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(6, 37);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_PurchaseGroup.TabIndex = 2;
            this.lb_PurchaseGroup.Text = "采购组织：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(269, 33);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(123, 20);
            this.cbb_MaterialGroup.TabIndex = 1;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(210, 37);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 0;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // panel_TitleColumn
            // 
            this.panel_TitleColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_TitleColumn.BackColor = System.Drawing.SystemColors.Control;
            this.panel_TitleColumn.Controls.Add(this.btn_Preview);
            this.panel_TitleColumn.Controls.Add(this.btn_Print);
            this.panel_TitleColumn.Controls.Add(this.lb_Title);
            this.panel_TitleColumn.Location = new System.Drawing.Point(1, 92);
            this.panel_TitleColumn.Name = "panel_TitleColumn";
            this.panel_TitleColumn.Size = new System.Drawing.Size(1072, 40);
            this.panel_TitleColumn.TabIndex = 1;
            // 
            // btn_Preview
            // 
            this.btn_Preview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(1012, 9);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(54, 23);
            this.btn_Preview.TabIndex = 2;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(950, 9);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(56, 23);
            this.btn_Print.TabIndex = 1;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click);
            // 
            // lb_Title
            // 
            this.lb_Title.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Title.Location = new System.Drawing.Point(9, 9);
            this.lb_Title.Name = "lb_Title";
            this.lb_Title.Size = new System.Drawing.Size(164, 25);
            this.lb_Title.TabIndex = 0;
            this.lb_Title.Text = "库龄分析明细表";
            // 
            // panel_DataGridView
            // 
            this.panel_DataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_DataGridView.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_DataGridView.Controls.Add(this.dgv_StockAgeAnalysis);
            this.panel_DataGridView.Location = new System.Drawing.Point(1, 135);
            this.panel_DataGridView.Name = "panel_DataGridView";
            this.panel_DataGridView.Size = new System.Drawing.Size(1072, 294);
            this.panel_DataGridView.TabIndex = 2;
            // 
            // dgv_StockAgeAnalysis
            // 
            this.dgv_StockAgeAnalysis.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_StockAgeAnalysis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_StockAgeAnalysis.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_StockAgeAnalysis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_StockAddress,
            this.Column_StockMaterialID,
            this.Column_StockMaterialName,
            this.Column_StockMaterialQuantity,
            this.Column_StockMaterialTotalAmount,
            this.Column_StockInDate,
            this.Column_StockAge});
            this.dgv_StockAgeAnalysis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_StockAgeAnalysis.EnableHeadersVisualStyles = false;
            this.dgv_StockAgeAnalysis.Location = new System.Drawing.Point(0, 0);
            this.dgv_StockAgeAnalysis.Name = "dgv_StockAgeAnalysis";
            this.dgv_StockAgeAnalysis.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgv_StockAgeAnalysis.RowTemplate.Height = 23;
            this.dgv_StockAgeAnalysis.Size = new System.Drawing.Size(1072, 294);
            this.dgv_StockAgeAnalysis.TabIndex = 0;
            this.dgv_StockAgeAnalysis.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_StockAgeAnalysis_RowPostPaint);
            // 
            // Column_StockAddress
            // 
            this.Column_StockAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_StockAddress.DataPropertyName = "Stock_Name";
            this.Column_StockAddress.HeaderText = "库存地点";
            this.Column_StockAddress.Name = "Column_StockAddress";
            // 
            // Column_StockMaterialID
            // 
            this.Column_StockMaterialID.DataPropertyName = "Material_ID";
            this.Column_StockMaterialID.HeaderText = "存货编码";
            this.Column_StockMaterialID.Name = "Column_StockMaterialID";
            this.Column_StockMaterialID.Width = 150;
            // 
            // Column_StockMaterialName
            // 
            this.Column_StockMaterialName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_StockMaterialName.DataPropertyName = "Material_Name";
            this.Column_StockMaterialName.HeaderText = "存货名称";
            this.Column_StockMaterialName.Name = "Column_StockMaterialName";
            // 
            // Column_StockMaterialQuantity
            // 
            this.Column_StockMaterialQuantity.DataPropertyName = "Receive_count";
            this.Column_StockMaterialQuantity.HeaderText = "数量";
            this.Column_StockMaterialQuantity.Name = "Column_StockMaterialQuantity";
            // 
            // Column_StockMaterialTotalAmount
            // 
            this.Column_StockMaterialTotalAmount.DataPropertyName = "TotalAmount";
            this.Column_StockMaterialTotalAmount.HeaderText = "总金额";
            this.Column_StockMaterialTotalAmount.Name = "Column_StockMaterialTotalAmount";
            // 
            // Column_StockInDate
            // 
            this.Column_StockInDate.DataPropertyName = "StockIn_Date";
            this.Column_StockInDate.HeaderText = "入库日期";
            this.Column_StockInDate.Name = "Column_StockInDate";
            // 
            // Column_StockAge
            // 
            this.Column_StockAge.DataPropertyName = "DaysL";
            this.Column_StockAge.HeaderText = "库龄";
            this.Column_StockAge.Name = "Column_StockAge";
            // 
            // MyPrintDocument
            // 
            this.MyPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.MyPrintDocument_PrintPage);
            // 
            // MyPageSetupDialog
            // 
            this.MyPageSetupDialog.Document = this.MyPrintDocument;
            // 
            // StockAgeAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 432);
            this.Controls.Add(this.panel_DataGridView);
            this.Controls.Add(this.panel_TitleColumn);
            this.Controls.Add(this.panel_Condition);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "StockAgeAnalysis";
            this.Text = "库龄分析";
            this.panel_Condition.ResumeLayout(false);
            this.panel_Condition.PerformLayout();
            this.panel_TitleColumn.ResumeLayout(false);
            this.panel_DataGridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockAgeAnalysis)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Condition;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.Panel panel_TitleColumn;
        private System.Windows.Forms.Label lb_Title;
        private System.Windows.Forms.Panel panel_DataGridView;
        private System.Windows.Forms.DateTimePicker dtp_BaseDate;
        private System.Windows.Forms.Label lb_BaseDate;
        private System.Windows.Forms.Button btn_CreateReport;
        private System.Windows.Forms.DataGridView dgv_StockAgeAnalysis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockMaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockMaterialQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockMaterialTotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockInDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockAge;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.Button btn_Print;
        private System.Drawing.Printing.PrintDocument MyPrintDocument;
        private System.Windows.Forms.PageSetupDialog MyPageSetupDialog;
    }
}