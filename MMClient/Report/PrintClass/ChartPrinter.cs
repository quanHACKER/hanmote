﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;

namespace MMClient.RA
{
    /// <summary>
    /// 图表打印类
    /// </summary>
    public class ChartPrinter
    {
        #region 字段
        private Bitmap TheBitmap;   //要打印的图片
        private PrintDocument ThePrintDocument;
        private bool IsCenterOnPage; //是否居中打印
        private bool IsWithTile;   //是否页面需要标题
        private string TheTitleText; //要打印的标题名
        private Font TheTitleFont; //要打印标题名的字体
        private Color TheTitleColor;//要打印标题名的颜色

        private int PageWidth;   //打印页面宽度
        private int PageHeight;  //打印页面高度

        //四周边距
        private int LeftMargin;
        private int RightMargin;
        private int TopMargin;
        private int BottomMargin;

        //用来跟踪页面的y坐标
        private float CurrentY;
        private float CurrentX;
        #endregion

        #region 构造函数，初始化特定值
        /// <summary>
        /// 构造函数，初始化一些特定值
        /// </summary>
        /// <param name="aBitmap"></param>
        /// <param name="aPrintDocument"></param>
        /// <param name="CenterOnPage"></param>
        /// <param name="WithTitle"></param>
        /// <param name="aTitleText"></param>
        /// <param name="aTitleFont"></param>
        /// <param name="aTitleColor"></param>
        public ChartPrinter(Bitmap aBitmap, PrintDocument aPrintDocument, bool CenterOnPage, bool WithTitle, string aTitleText, Font aTitleFont, Color aTitleColor )
        {
            TheBitmap = aBitmap;
            ThePrintDocument = aPrintDocument;
            IsCenterOnPage = CenterOnPage;
            IsWithTile = WithTitle;
            TheTitleText = aTitleText;
            TheTitleFont = aTitleFont;
            TheTitleColor = aTitleColor;

            // Calculating the PageWidth and the PageHeight
            if (!ThePrintDocument.DefaultPageSettings.Landscape)
            {
                PageWidth = ThePrintDocument.DefaultPageSettings.PaperSize.Width;
                PageHeight = ThePrintDocument.DefaultPageSettings.PaperSize.Height;
            }
            else
            {
                PageHeight = ThePrintDocument.DefaultPageSettings.PaperSize.Width;
                PageWidth = ThePrintDocument.DefaultPageSettings.PaperSize.Height;
            }
            //设置四周的边距
            LeftMargin = ThePrintDocument.DefaultPageSettings.Margins.Left;
            RightMargin = ThePrintDocument.DefaultPageSettings.Margins.Right;
            TopMargin = ThePrintDocument.DefaultPageSettings.Margins.Top;
            BottomMargin = ThePrintDocument.DefaultPageSettings.Margins.Bottom;
        }
        #endregion

        #region 画标题
        private void DrawTitle(Graphics g)
        {
            CurrentY = (float)TopMargin;

            if (IsWithTile)
            {
                StringFormat TitleFormat = new StringFormat();
                TitleFormat.Trimming = StringTrimming.Word;
                TitleFormat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
                if (IsCenterOnPage)
                {
                    TitleFormat.Alignment = StringAlignment.Center;
                }
                else
                {
                    TitleFormat.Alignment = StringAlignment.Near;
                }
                RectangleF TitleRectangle = new RectangleF((float)LeftMargin, CurrentY, (float)PageWidth - (float)RightMargin - (float)LeftMargin, g.MeasureString(TheTitleText, TheTitleFont).Height);

                g.DrawString(TheTitleText, TheTitleFont, new SolidBrush(TheTitleColor), TitleRectangle, TitleFormat);

                CurrentY += g.MeasureString(TheTitleText, TheTitleFont).Height;
            }
        }
        #endregion

        #region 画图表
        private void DrawImage(Graphics g)
        {

            // Calculating the starting x coordinate that the printing process will start from
            CurrentX = (float)LeftMargin;
            if (IsCenterOnPage)
            {
                CurrentX += (((float)PageWidth - (float)RightMargin - (float)LeftMargin) - (float)TheBitmap.Width) / 2.0F;
            }
            g.DrawImage(TheBitmap, CurrentX, CurrentY + 5.0F, TheBitmap.Width, TheBitmap.Height);
        }
        #endregion

        #region 打印图表
        public void DrawChart(Graphics g)
        {
            try
            {
                DrawTitle(g);
                DrawImage(g);
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作失败: " + ex.Message.ToString(), Application.ProductName + " - 错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
