﻿namespace MMClient.RA.ConstractExecution
{
    partial class Contract_ExecutionAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.btn_query = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_endTime = new System.Windows.Forms.Label();
            this.lb_stratTime = new System.Windows.Forms.Label();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.lb_SupplierName = new System.Windows.Forms.Label();
            this.lb_SupplierID = new System.Windows.Forms.Label();
            this.lb_MaterialId = new System.Windows.Forms.Label();
            this.txt_SupplierName = new System.Windows.Forms.TextBox();
            this.txt_SupplierID = new System.Windows.Forms.TextBox();
            this.txt_MaterialID = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.gb_Query.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Query.Controls.Add(this.btn_query);
            this.gb_Query.Controls.Add(this.groupBox1);
            this.gb_Query.Controls.Add(this.lb_SupplierName);
            this.gb_Query.Controls.Add(this.lb_SupplierID);
            this.gb_Query.Controls.Add(this.lb_MaterialId);
            this.gb_Query.Controls.Add(this.txt_SupplierName);
            this.gb_Query.Controls.Add(this.txt_SupplierID);
            this.gb_Query.Controls.Add(this.txt_MaterialID);
            this.gb_Query.Location = new System.Drawing.Point(12, 12);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(979, 113);
            this.gb_Query.TabIndex = 1;
            this.gb_Query.TabStop = false;
            this.gb_Query.Text = "查询条件";
            // 
            // btn_query
            // 
            this.btn_query.Location = new System.Drawing.Point(784, 53);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(75, 23);
            this.btn_query.TabIndex = 7;
            this.btn_query.Text = "查询";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.lb_endTime);
            this.groupBox1.Controls.Add(this.lb_stratTime);
            this.groupBox1.Controls.Add(this.dtp_endTime);
            this.groupBox1.Controls.Add(this.dtp_startTime);
            this.groupBox1.Location = new System.Drawing.Point(467, 17);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(263, 88);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "时间范围";
            // 
            // lb_endTime
            // 
            this.lb_endTime.AutoSize = true;
            this.lb_endTime.Location = new System.Drawing.Point(34, 58);
            this.lb_endTime.Name = "lb_endTime";
            this.lb_endTime.Size = new System.Drawing.Size(65, 12);
            this.lb_endTime.TabIndex = 3;
            this.lb_endTime.Text = "截止日期：";
            // 
            // lb_stratTime
            // 
            this.lb_stratTime.AutoSize = true;
            this.lb_stratTime.Location = new System.Drawing.Point(34, 24);
            this.lb_stratTime.Name = "lb_stratTime";
            this.lb_stratTime.Size = new System.Drawing.Size(65, 12);
            this.lb_stratTime.TabIndex = 2;
            this.lb_stratTime.Text = "起始时间：";
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.Checked = false;
            this.dtp_endTime.Location = new System.Drawing.Point(105, 52);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.ShowCheckBox = true;
            this.dtp_endTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_endTime.TabIndex = 1;
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.Checked = false;
            this.dtp_startTime.Location = new System.Drawing.Point(105, 20);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.ShowCheckBox = true;
            this.dtp_startTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_startTime.TabIndex = 0;
            // 
            // lb_SupplierName
            // 
            this.lb_SupplierName.AutoSize = true;
            this.lb_SupplierName.Location = new System.Drawing.Point(221, 75);
            this.lb_SupplierName.Name = "lb_SupplierName";
            this.lb_SupplierName.Size = new System.Drawing.Size(77, 12);
            this.lb_SupplierName.TabIndex = 5;
            this.lb_SupplierName.Text = "供应商名称：";
            // 
            // lb_SupplierID
            // 
            this.lb_SupplierID.AutoSize = true;
            this.lb_SupplierID.Location = new System.Drawing.Point(221, 20);
            this.lb_SupplierID.Name = "lb_SupplierID";
            this.lb_SupplierID.Size = new System.Drawing.Size(77, 12);
            this.lb_SupplierID.TabIndex = 4;
            this.lb_SupplierID.Text = "供应商编码：";
            // 
            // lb_MaterialId
            // 
            this.lb_MaterialId.AutoSize = true;
            this.lb_MaterialId.Location = new System.Drawing.Point(6, 44);
            this.lb_MaterialId.Name = "lb_MaterialId";
            this.lb_MaterialId.Size = new System.Drawing.Size(65, 12);
            this.lb_MaterialId.TabIndex = 3;
            this.lb_MaterialId.Text = "物料编码：";
            // 
            // txt_SupplierName
            // 
            this.txt_SupplierName.Location = new System.Drawing.Point(304, 72);
            this.txt_SupplierName.Name = "txt_SupplierName";
            this.txt_SupplierName.Size = new System.Drawing.Size(130, 21);
            this.txt_SupplierName.TabIndex = 2;
            // 
            // txt_SupplierID
            // 
            this.txt_SupplierID.Location = new System.Drawing.Point(304, 17);
            this.txt_SupplierID.Name = "txt_SupplierID";
            this.txt_SupplierID.Size = new System.Drawing.Size(130, 21);
            this.txt_SupplierID.TabIndex = 1;
            // 
            // txt_MaterialID
            // 
            this.txt_MaterialID.Location = new System.Drawing.Point(72, 41);
            this.txt_MaterialID.Name = "txt_MaterialID";
            this.txt_MaterialID.Size = new System.Drawing.Size(100, 21);
            this.txt_MaterialID.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.chartViewer1);
            this.panel1.Location = new System.Drawing.Point(13, 126);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 328);
            this.panel1.TabIndex = 2;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 3;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // Contract_ExecutionAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 466);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gb_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Contract_ExecutionAnalysis";
            this.Text = "合同执行进度";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lb_endTime;
        private System.Windows.Forms.Label lb_stratTime;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.Label lb_SupplierName;
        private System.Windows.Forms.Label lb_SupplierID;
        private System.Windows.Forms.Label lb_MaterialId;
        private System.Windows.Forms.TextBox txt_SupplierName;
        private System.Windows.Forms.TextBox txt_SupplierID;
        private System.Windows.Forms.TextBox txt_MaterialID;
        private System.Windows.Forms.Panel panel1;
        private ChartDirector.WinChartViewer chartViewer1;
    }
}