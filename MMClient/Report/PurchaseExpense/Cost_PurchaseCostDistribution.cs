﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.RA.PurchaseExpense
{
    public partial class Cost_PurchaseCostDistribution : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Cost_PurchaseCostDistribution()
        {
            InitializeComponent();
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = this.chartViewer1;
            this.createChart(viewer,"1");
        }

        //Main code for creating chart.
        //Note: the argument img is unused because this demo only has 1 chart.
        public void createChart(WinChartViewer viewer, string img)
        {
            //条件检查
            if(this.dtp_startTime.Checked==false || this.dtp_endTime.Checked==false)
            {
                MessageBox.Show("请选择查询时间范围！","软件提示");
                return;
            }
            //查询数据库
            string sql_Text = @"select t.[Type_Name] as typeName,SUM(t.total) as total  from (select Material_Type.[Type_Name],Purchase_Cost.Number*Purchase_Cost.Price as total  from Purchase_Cost inner join Material_Type on Purchase_Cost.Material_ID = Material_Type.Material_ID where";
            sql_Text += @" Purchase_Cost.BeginTime >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
            sql_Text += @" and Purchase_Cost.BeginTime <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
            sql_Text += @" ) t group by t.[Type_Name]  order by t.[Type_Name] DESC";
            DataTable data_Table = null;
            try
            {
                data_Table = DBHelper.ExecuteQueryDT(sql_Text);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
            int n = data_Table.Rows.Count;
            double[] data = new double[n];
            string[] labels = new string[n];
            //double[] data={0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
            //string[] labels = {"原材料","未估价物料", "经营供应","贸易货物", "服务", "非存储物料", "成品", "包装物料","半成品"};
            //string str = null;
            for (int i = 0; i < data_Table.Rows.Count; i++)
            {
                data[i] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                labels[i] = Convert.ToString(data_Table.Rows[i]["typeName"]);

                /*
                str = Convert.ToString(data_Table.Rows[i]["typeName"]);
                if (str.Equals("原材料"))
                {
                    data[0] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("未估价物料"))
                {
                    data[1] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("经营供应"))
                {
                    data[2] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("贸易货物"))
                {
                    data[3] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("服务"))
                {
                    data[4] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("非存储物料"))
                {
                    data[5] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("成品"))
                {
                    data[6] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("包装物料"))
                {
                    data[7] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                if (str.Equals("半成品"))
                {
                    data[8] = Convert.ToDouble(data_Table.Rows[i]["total"]);
                }
                 * */
            }

            // The data for the pie chart
            //double[] data = { 35, 30, 0, 7, 6, 5, 4, 3, 0};

            // The labels for the pie chart
            

            // Create a PieChart object of size 560 x 270 pixels, with a golden
            // background and a 1 pixel 3D border
            PieChart c = new PieChart(1150, 580, Chart.goldColor(), -1, 1);

            // Add a title box using 15 pts Times Bold Italic font and metallic pink
            // background color
            string str1 = this.dtp_startTime.Value.ToString("yyyy-MM-dd");
            string str2 = this.dtp_endTime.Value.ToString("yyyy-MM-dd");
            string str3 = str1 + "至" + str2 + "  物料采购花费分布";
            c.addTitle(str3, "Times New Roman Bold ", 15
                ).setBackground(Chart.metalColor(0xff9999));

            // Set the center of the pie at (280, 135) and the radius to 110 pixels
            c.setPieSize(575, 235, 210);

            // Draw the pie in 3D with 20 pixels 3D depth
            c.set3D(20);

            // Use the side label layout method
            c.setLabelLayout(Chart.SideLayout);

            // Set the label box background color the same as the sector color, with
            // glass effect, and with 5 pixels rounded corners
            ChartDirector.TextBox t = c.setLabelStyle();
            t.setBackground(Chart.SameAsMainColor, Chart.Transparent,
                Chart.glassEffect());
            t.setRoundedCorners(5);

            // Set the border color of the sector the same color as the fill color.
            // Set the line color of the join line to black (0x0)
            c.setLineColor(Chart.SameAsMainColor, 0x000000);

            // Set the start angle to 135 degrees may improve layout when there are
            // many small sectors at the end of the data array (that is, data sorted
            // in descending order). It is because this makes the small sectors
            // position near the horizontal axis, where the text label has the least
            // tendency to overlap. For data sorted in ascending order, a start angle
            // of 45 degrees can be used instead.
            c.setStartAngle(135);

            // Set the pie data and the pie labels
            c.setData(data, labels);

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='{label}: US${value}K ({percent}%)'");
        }

        //触发热点事件
        private void chartViewer1_ClickHotSpot(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender,e);
        }
    }
}
