﻿namespace MMClient.RA.SupplierPerformance
{
    partial class SupplierTrendAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_query = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.panel_Quality = new System.Windows.Forms.Panel();
            this.QualityChartViewer = new ChartDirector.WinChartViewer();
            this.panel_All = new System.Windows.Forms.Panel();
            this.ALLChartViewer = new ChartDirector.WinChartViewer();
            this.AllPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.AllPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.QualityPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.QualityPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.DeliveryPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.DeliveryPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.CostPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.CostPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.panel_ScoreTrend = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DeliveryChartViewer = new ChartDirector.WinChartViewer();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CostChartViewer = new ChartDirector.WinChartViewer();
            this.panel_query.SuspendLayout();
            this.panel_Quality.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QualityChartViewer)).BeginInit();
            this.panel_All.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ALLChartViewer)).BeginInit();
            this.panel_ScoreTrend.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryChartViewer)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostChartViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_query
            // 
            this.panel_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_query.BackColor = System.Drawing.SystemColors.Control;
            this.panel_query.Controls.Add(this.label5);
            this.panel_query.Controls.Add(this.dtp_EndTime);
            this.panel_query.Controls.Add(this.lb_To);
            this.panel_query.Controls.Add(this.dtp_StartTime);
            this.panel_query.Controls.Add(this.lb_Time);
            this.panel_query.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_query.Controls.Add(this.lb_PurchaseGroup);
            this.panel_query.Controls.Add(this.cbb_MaterialGroup);
            this.panel_query.Controls.Add(this.lb_MaterialGroup);
            this.panel_query.Controls.Add(this.btn_Query);
            this.panel_query.Location = new System.Drawing.Point(5, 4);
            this.panel_query.Name = "panel_query";
            this.panel_query.Size = new System.Drawing.Size(1176, 77);
            this.panel_query.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(-1, 345);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "成本得分趋势";
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(229, 28);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 11;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(206, 32);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 10;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(78, 28);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 9;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(38, 32);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 8;
            this.lb_Time.Text = "时间：";
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(488, 29);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbb_PurchaseGroup.TabIndex = 7;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(417, 33);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_PurchaseGroup.TabIndex = 6;
            this.lb_PurchaseGroup.Text = "采购组织：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(684, 29);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(123, 20);
            this.cbb_MaterialGroup.TabIndex = 5;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(625, 33);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 4;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(880, 24);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(91, 32);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // panel_Quality
            // 
            this.panel_Quality.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Quality.Controls.Add(this.QualityChartViewer);
            this.panel_Quality.Location = new System.Drawing.Point(3, 6);
            this.panel_Quality.Name = "panel_Quality";
            this.panel_Quality.Size = new System.Drawing.Size(681, 315);
            this.panel_Quality.TabIndex = 1;
            // 
            // QualityChartViewer
            // 
            this.QualityChartViewer.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.QualityChartViewer.Location = new System.Drawing.Point(3, 4);
            this.QualityChartViewer.Name = "QualityChartViewer";
            this.QualityChartViewer.Size = new System.Drawing.Size(168, 126);
            this.QualityChartViewer.TabIndex = 3;
            this.QualityChartViewer.TabStop = false;
            this.QualityChartViewer.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.QualityChartViewer_ClickHotSpot);
            // 
            // panel_All
            // 
            this.panel_All.BackColor = System.Drawing.SystemColors.Control;
            this.panel_All.Controls.Add(this.ALLChartViewer);
            this.panel_All.Location = new System.Drawing.Point(7, 6);
            this.panel_All.Name = "panel_All";
            this.panel_All.Size = new System.Drawing.Size(576, 257);
            this.panel_All.TabIndex = 1;
            // 
            // ALLChartViewer
            // 
            this.ALLChartViewer.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.ALLChartViewer.Location = new System.Drawing.Point(3, 5);
            this.ALLChartViewer.Name = "ALLChartViewer";
            this.ALLChartViewer.Size = new System.Drawing.Size(168, 126);
            this.ALLChartViewer.TabIndex = 3;
            this.ALLChartViewer.TabStop = false;
            this.ALLChartViewer.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.ALLChartViewer_ClickHotSpot);
            // 
            // AllPageSetupDialog
            // 
            this.AllPageSetupDialog.Document = this.AllPrintDocument;
            // 
            // QualityPageSetupDialog
            // 
            this.QualityPageSetupDialog.Document = this.QualityPrintDocument;
            // 
            // DeliveryPageSetupDialog
            // 
            this.DeliveryPageSetupDialog.Document = this.DeliveryPrintDocument;
            // 
            // CostPageSetupDialog
            // 
            this.CostPageSetupDialog.Document = this.CostPrintDocument;
            // 
            // panel_ScoreTrend
            // 
            this.panel_ScoreTrend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_ScoreTrend.BackColor = System.Drawing.SystemColors.Control;
            this.panel_ScoreTrend.Controls.Add(this.tabControl1);
            this.panel_ScoreTrend.Location = new System.Drawing.Point(5, 84);
            this.panel_ScoreTrend.Name = "panel_ScoreTrend";
            this.panel_ScoreTrend.Size = new System.Drawing.Size(1177, 596);
            this.panel_ScoreTrend.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.ItemSize = new System.Drawing.Size(84, 28);
            this.tabControl1.Location = new System.Drawing.Point(4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1170, 591);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.panel_All);
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1162, 555);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "综合得分趋势";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel_Quality);
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1162, 555);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "质量得分趋势";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1162, 555);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "交付得分趋势";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DeliveryChartViewer);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(577, 245);
            this.panel2.TabIndex = 2;
            // 
            // DeliveryChartViewer
            // 
            this.DeliveryChartViewer.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.DeliveryChartViewer.Location = new System.Drawing.Point(4, 4);
            this.DeliveryChartViewer.Name = "DeliveryChartViewer";
            this.DeliveryChartViewer.Size = new System.Drawing.Size(168, 126);
            this.DeliveryChartViewer.TabIndex = 3;
            this.DeliveryChartViewer.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.panel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 32);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1162, 555);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "成本得分趋势";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.CostChartViewer);
            this.panel3.Location = new System.Drawing.Point(6, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(577, 244);
            this.panel3.TabIndex = 2;
            // 
            // CostChartViewer
            // 
            this.CostChartViewer.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.CostChartViewer.Location = new System.Drawing.Point(3, 3);
            this.CostChartViewer.Name = "CostChartViewer";
            this.CostChartViewer.Size = new System.Drawing.Size(168, 126);
            this.CostChartViewer.TabIndex = 3;
            this.CostChartViewer.TabStop = false;
            // 
            // SupplierTrendAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 683);
            this.Controls.Add(this.panel_ScoreTrend);
            this.Controls.Add(this.panel_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierTrendAnalysis";
            this.Text = "供应商趋势分析";
            this.panel_query.ResumeLayout(false);
            this.panel_query.PerformLayout();
            this.panel_Quality.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.QualityChartViewer)).EndInit();
            this.panel_All.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ALLChartViewer)).EndInit();
            this.panel_ScoreTrend.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryChartViewer)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CostChartViewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_query;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel_Quality;
        private System.Windows.Forms.Panel panel_All;
        private ChartDirector.WinChartViewer QualityChartViewer;
        private ChartDirector.WinChartViewer ALLChartViewer;
        private System.Windows.Forms.PageSetupDialog AllPageSetupDialog;
        private System.Drawing.Printing.PrintDocument AllPrintDocument;
        private System.Windows.Forms.PageSetupDialog QualityPageSetupDialog;
        private System.Drawing.Printing.PrintDocument QualityPrintDocument;
        private System.Windows.Forms.PageSetupDialog DeliveryPageSetupDialog;
        private System.Drawing.Printing.PrintDocument DeliveryPrintDocument;
        private System.Windows.Forms.PageSetupDialog CostPageSetupDialog;
        private System.Drawing.Printing.PrintDocument CostPrintDocument;
        private System.Windows.Forms.Panel panel_ScoreTrend;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel2;
        private ChartDirector.WinChartViewer DeliveryChartViewer;
        private System.Windows.Forms.Panel panel3;
        private ChartDirector.WinChartViewer CostChartViewer;
    }
}