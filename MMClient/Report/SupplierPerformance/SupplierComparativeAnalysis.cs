﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using Lib.SqlServerDAL;

namespace MMClient.RA.SupplierPerformance
{
    public partial class SupplierComparativeAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private readonly string sqlText_combox = @"SELECT Buyer_Org_Name FROM Buyer_Org";
        DataTable dt = new DataTable();
        //此处可以实现后续追加
        //StringBuilder strB = new StringBuilder();
        //定义一个string类型的集合属性
        public List<string> listSupplierName { get; set; }

        public SupplierComparativeAnalysis()
        {
            InitializeComponent();
            this.dt = DBHelper.ExecuteQueryDT(sqlText_combox);
            //当窗体加载时combox就绑定数据
            this.cbb_PurchaseOrganization.DataSource = dt;
            //要显示字段名称
            this.cbb_PurchaseOrganization.DisplayMember = "Buyer_Org_Name";
            //combox的下拉值
            this.cbb_PurchaseOrganization.ValueMember = "Buyer_Org_Name";
        }

        /// <summary>
        /// 更新txt_Supplier内容
        /// </summary>
        /// <param name="str"></param>
        public void update_TxtSupplier(List<string> listStr)
        {
            StringBuilder strB = new StringBuilder();
            this.listSupplierName = listStr;
            if (listStr != null && listStr.Count > 0)
            {
                foreach (string str in listStr)
                {
                    strB.Append(str.ToString());
                    strB.Append("\r\n");
                }
            }
            this.txt_Supplier.Text = strB.ToString();
        }
        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = this.chartViewer1;
            this.createChart(viewer);
        }

        /// <summary>
        /// 画图表
        /// </summary>
        /// <param name="viewer"></param>
        private void createChart(WinChartViewer viewer)
        {
            
            string[] label = { "价格", "质量", "交货", "服务" };
            //声明一个二维数组
            List<List<double>> towList = new List<List<double>>();
            string sqlText = null;
            DataTable dt = null;
            if (listSupplierName != null && listSupplierName.Count > 0)
            {
                foreach (string strSupplierName in listSupplierName)
                {
                    //声明一个一维数组,用于存储数据
                    List<double> oneList = new List<double>();
                    sqlText = "SELECT Standards.Name,Score FROM [Supplier_Performance Analysis] JOIN Standards ON [Supplier_Performance Analysis].Standard_ID=Standards.Standard_ID WHERE Supplier_Name='" + strSupplierName + "'";
                    try
                    {
                        dt = DBHelper.ExecuteQueryDT(sqlText);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("程序出错，错误信息为：" + ex.Message);
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if ("价格".Equals(Convert.ToString(dt.Rows[i]["Name"]).Trim()))
                        {
                            oneList.Add(Convert.ToDouble(dt.Rows[i]["Score"]));
                        }
                        if ("质量".Equals(Convert.ToString(dt.Rows[i]["Name"]).Trim()))
                        {
                            oneList.Add(Convert.ToDouble(dt.Rows[i]["Score"]));
                        }
                        if ("交货".Equals(Convert.ToString(dt.Rows[i]["Name"]).Trim()))
                        {
                            oneList.Add(Convert.ToDouble(dt.Rows[i]["Score"]));
                        }
                        if ("服务".Equals(Convert.ToString(dt.Rows[i]["Name"]).Trim()))
                        {
                            oneList.Add(Convert.ToDouble(dt.Rows[i]["Score"]));
                        }
                    }
                    towList.Add(oneList);
                }
                //画一个大小为480*380的极图，背景色为金色，边界为1px的3D效果
                PolarChart c = new PolarChart(890, 540, 0xffffff, 0x000000, 1);
                c.setPlotArea(320, 250, 200, 0xffffff);
                LegendBox b = c.addLegend(830, 75, true, "Arial Bold", 10);
                b.setAlignment(Chart.TopRight);
                b.setBackground(Chart.silverColor(), Chart.Transparent, 1);
                //c.addAreaLayer(data, 0x806666cc, "Model Saturn");
                for (int j = 0; j < towList.Count; j++)
                {
                    double[] data = towList[j].ToArray();
                    c.addAreaLayer(data, unchecked((int)0x806666cc + 400 * j), listSupplierName[j]);
                    c.addLineLayer(data, 0x6666cc).setLineWidth(3);
                }
                c.angularAxis().setLabels(label);
                viewer.Chart = c;
                viewer.ImageMap = c.getHTMLImageMap("clickable", "", "title='[{dataSetName}] {label}:分数 = {value}'");
            }
            else
            {
                MessageBox.Show("您还没有没有选择任何供应商","温馨提示");
            }
        }

        private void chartViewer1_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender,e); 
        }

        /// <summary>
        /// 选择供应商按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //将采购组织传递过去，再将父窗体当前对象传递过去，不能再子窗体中另外新建一个，这样就不是当前窗体了
            SupplierView sv = new SupplierView(this.cbb_PurchaseOrganization.Text,this);
            sv.ShowDialog();
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
