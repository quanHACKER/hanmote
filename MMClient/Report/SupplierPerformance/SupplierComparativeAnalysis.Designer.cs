﻿namespace MMClient.RA.SupplierPerformance
{
    partial class SupplierComparativeAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_query = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_Supplier = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_PurchaseOrganization = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_query = new System.Windows.Forms.Button();
            this.pl_drawing = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ExternalService_Score = new System.Windows.Forms.CheckBox();
            this.GeneralServiceAndSupport_Score = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PriceLevel_Score = new System.Windows.Forms.CheckBox();
            this.Price_Score = new System.Windows.Forms.CheckBox();
            this.PriceHistory_Score = new System.Windows.Forms.CheckBox();
            this.GoodReceipt_Score = new System.Windows.Forms.CheckBox();
            this.Quality_Score = new System.Windows.Forms.CheckBox();
            this.QualityAudit_Score = new System.Windows.Forms.CheckBox();
            this.ComplaintAndReject_Score = new System.Windows.Forms.CheckBox();
            this.QuantityReliability_Score = new System.Windows.Forms.CheckBox();
            this.ConfirmDate_Score = new System.Windows.Forms.CheckBox();
            this.Shipment_Score = new System.Windows.Forms.CheckBox();
            this.OnTimeDelivery_Score = new System.Windows.Forms.CheckBox();
            this.Delivery_Score = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gb_query.SuspendLayout();
            this.pl_drawing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_query
            // 
            this.gb_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_query.Controls.Add(this.button1);
            this.gb_query.Controls.Add(this.txt_Supplier);
            this.gb_query.Controls.Add(this.label2);
            this.gb_query.Controls.Add(this.cbb_PurchaseOrganization);
            this.gb_query.Controls.Add(this.label1);
            this.gb_query.Controls.Add(this.btn_query);
            this.gb_query.Location = new System.Drawing.Point(2, 12);
            this.gb_query.Name = "gb_query";
            this.gb_query.Size = new System.Drawing.Size(1163, 79);
            this.gb_query.TabIndex = 0;
            this.gb_query.TabStop = false;
            this.gb_query.Text = "查询条件";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Honeydew;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(512, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 33);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_Supplier
            // 
            this.txt_Supplier.Location = new System.Drawing.Point(319, 29);
            this.txt_Supplier.Multiline = true;
            this.txt_Supplier.Name = "txt_Supplier";
            this.txt_Supplier.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Supplier.Size = new System.Drawing.Size(187, 31);
            this.txt_Supplier.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(260, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "供应商：";
            // 
            // cbb_PurchaseOrganization
            // 
            this.cbb_PurchaseOrganization.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_PurchaseOrganization.FormattingEnabled = true;
            this.cbb_PurchaseOrganization.Location = new System.Drawing.Point(93, 32);
            this.cbb_PurchaseOrganization.Name = "cbb_PurchaseOrganization";
            this.cbb_PurchaseOrganization.Size = new System.Drawing.Size(137, 20);
            this.cbb_PurchaseOrganization.TabIndex = 2;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 35);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "采购组织：";
            // 
            // btn_query
            // 
            this.btn_query.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_query.FlatAppearance.BorderSize = 2;
            this.btn_query.Location = new System.Drawing.Point(671, 27);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(102, 36);
            this.btn_query.TabIndex = 0;
            this.btn_query.Text = "生成报表";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // pl_drawing
            // 
            this.pl_drawing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pl_drawing.BackColor = System.Drawing.SystemColors.Control;
            this.pl_drawing.Controls.Add(this.chartViewer1);
            this.pl_drawing.Location = new System.Drawing.Point(263, 94);
            this.pl_drawing.Name = "pl_drawing";
            this.pl_drawing.Size = new System.Drawing.Size(902, 544);
            this.pl_drawing.TabIndex = 1;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 2;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.ExternalService_Score);
            this.groupBox1.Controls.Add(this.GeneralServiceAndSupport_Score);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.PriceLevel_Score);
            this.groupBox1.Controls.Add(this.Price_Score);
            this.groupBox1.Controls.Add(this.PriceHistory_Score);
            this.groupBox1.Controls.Add(this.GoodReceipt_Score);
            this.groupBox1.Controls.Add(this.Quality_Score);
            this.groupBox1.Controls.Add(this.QualityAudit_Score);
            this.groupBox1.Controls.Add(this.ComplaintAndReject_Score);
            this.groupBox1.Controls.Add(this.QuantityReliability_Score);
            this.groupBox1.Controls.Add(this.ConfirmDate_Score);
            this.groupBox1.Controls.Add(this.Shipment_Score);
            this.groupBox1.Controls.Add(this.OnTimeDelivery_Score);
            this.groupBox1.Controls.Add(this.Delivery_Score);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(2, 92);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(258, 546);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "标准选择";
            // 
            // ExternalService_Score
            // 
            this.ExternalService_Score.AutoSize = true;
            this.ExternalService_Score.Location = new System.Drawing.Point(19, 417);
            this.ExternalService_Score.Name = "ExternalService_Score";
            this.ExternalService_Score.Size = new System.Drawing.Size(72, 16);
            this.ExternalService_Score.TabIndex = 16;
            this.ExternalService_Score.Text = "外部服务";
            this.ExternalService_Score.UseVisualStyleBackColor = true;
            // 
            // GeneralServiceAndSupport_Score
            // 
            this.GeneralServiceAndSupport_Score.AutoSize = true;
            this.GeneralServiceAndSupport_Score.Checked = true;
            this.GeneralServiceAndSupport_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GeneralServiceAndSupport_Score.Location = new System.Drawing.Point(19, 358);
            this.GeneralServiceAndSupport_Score.Name = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.Size = new System.Drawing.Size(102, 16);
            this.GeneralServiceAndSupport_Score.TabIndex = 15;
            this.GeneralServiceAndSupport_Score.Text = "一般服务/支持";
            this.GeneralServiceAndSupport_Score.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(8, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 1);
            this.label6.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(8, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 1);
            this.label7.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(8, 390);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 1);
            this.label9.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(8, 336);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 1);
            this.label8.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(8, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 1);
            this.label5.TabIndex = 14;
            // 
            // PriceLevel_Score
            // 
            this.PriceLevel_Score.AutoSize = true;
            this.PriceLevel_Score.Location = new System.Drawing.Point(120, 87);
            this.PriceLevel_Score.Name = "PriceLevel_Score";
            this.PriceLevel_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceLevel_Score.TabIndex = 3;
            this.PriceLevel_Score.Text = "价格水平";
            this.PriceLevel_Score.UseVisualStyleBackColor = true;
            // 
            // Price_Score
            // 
            this.Price_Score.AutoSize = true;
            this.Price_Score.Checked = true;
            this.Price_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Price_Score.Location = new System.Drawing.Point(19, 100);
            this.Price_Score.Name = "Price_Score";
            this.Price_Score.Size = new System.Drawing.Size(48, 16);
            this.Price_Score.TabIndex = 2;
            this.Price_Score.Text = "价格";
            this.Price_Score.UseVisualStyleBackColor = true;
            // 
            // PriceHistory_Score
            // 
            this.PriceHistory_Score.AutoSize = true;
            this.PriceHistory_Score.Location = new System.Drawing.Point(120, 109);
            this.PriceHistory_Score.Name = "PriceHistory_Score";
            this.PriceHistory_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceHistory_Score.TabIndex = 4;
            this.PriceHistory_Score.Text = "价格历史";
            this.PriceHistory_Score.UseVisualStyleBackColor = true;
            // 
            // GoodReceipt_Score
            // 
            this.GoodReceipt_Score.AutoSize = true;
            this.GoodReceipt_Score.Location = new System.Drawing.Point(120, 156);
            this.GoodReceipt_Score.Name = "GoodReceipt_Score";
            this.GoodReceipt_Score.Size = new System.Drawing.Size(48, 16);
            this.GoodReceipt_Score.TabIndex = 6;
            this.GoodReceipt_Score.Text = "收货";
            this.GoodReceipt_Score.UseVisualStyleBackColor = true;
            // 
            // Quality_Score
            // 
            this.Quality_Score.AutoSize = true;
            this.Quality_Score.Checked = true;
            this.Quality_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Quality_Score.Location = new System.Drawing.Point(19, 178);
            this.Quality_Score.Name = "Quality_Score";
            this.Quality_Score.Size = new System.Drawing.Size(48, 16);
            this.Quality_Score.TabIndex = 5;
            this.Quality_Score.Text = "质量";
            this.Quality_Score.UseVisualStyleBackColor = true;
            // 
            // QualityAudit_Score
            // 
            this.QualityAudit_Score.AutoSize = true;
            this.QualityAudit_Score.Location = new System.Drawing.Point(120, 178);
            this.QualityAudit_Score.Name = "QualityAudit_Score";
            this.QualityAudit_Score.Size = new System.Drawing.Size(72, 16);
            this.QualityAudit_Score.TabIndex = 7;
            this.QualityAudit_Score.Text = "质量审计";
            this.QualityAudit_Score.UseVisualStyleBackColor = true;
            // 
            // ComplaintAndReject_Score
            // 
            this.ComplaintAndReject_Score.AutoSize = true;
            this.ComplaintAndReject_Score.Location = new System.Drawing.Point(120, 200);
            this.ComplaintAndReject_Score.Name = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.Size = new System.Drawing.Size(102, 16);
            this.ComplaintAndReject_Score.TabIndex = 9;
            this.ComplaintAndReject_Score.Text = "抱怨/拒绝水平";
            this.ComplaintAndReject_Score.UseVisualStyleBackColor = true;
            // 
            // QuantityReliability_Score
            // 
            this.QuantityReliability_Score.AutoSize = true;
            this.QuantityReliability_Score.Location = new System.Drawing.Point(120, 286);
            this.QuantityReliability_Score.Name = "QuantityReliability_Score";
            this.QuantityReliability_Score.Size = new System.Drawing.Size(84, 16);
            this.QuantityReliability_Score.TabIndex = 13;
            this.QuantityReliability_Score.Text = "数量可靠性";
            this.QuantityReliability_Score.UseVisualStyleBackColor = true;
            // 
            // ConfirmDate_Score
            // 
            this.ConfirmDate_Score.AutoSize = true;
            this.ConfirmDate_Score.Location = new System.Drawing.Point(120, 264);
            this.ConfirmDate_Score.Name = "ConfirmDate_Score";
            this.ConfirmDate_Score.Size = new System.Drawing.Size(72, 16);
            this.ConfirmDate_Score.TabIndex = 12;
            this.ConfirmDate_Score.Text = "确认日期";
            this.ConfirmDate_Score.UseVisualStyleBackColor = true;
            // 
            // Shipment_Score
            // 
            this.Shipment_Score.AutoSize = true;
            this.Shipment_Score.Location = new System.Drawing.Point(120, 308);
            this.Shipment_Score.Name = "Shipment_Score";
            this.Shipment_Score.Size = new System.Drawing.Size(120, 16);
            this.Shipment_Score.TabIndex = 11;
            this.Shipment_Score.Text = "对装运须知的遵守";
            this.Shipment_Score.UseVisualStyleBackColor = true;
            this.Shipment_Score.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // OnTimeDelivery_Score
            // 
            this.OnTimeDelivery_Score.AutoSize = true;
            this.OnTimeDelivery_Score.Location = new System.Drawing.Point(120, 242);
            this.OnTimeDelivery_Score.Name = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.Size = new System.Drawing.Size(108, 16);
            this.OnTimeDelivery_Score.TabIndex = 10;
            this.OnTimeDelivery_Score.Text = "按时交货的表现";
            this.OnTimeDelivery_Score.UseVisualStyleBackColor = true;
            // 
            // Delivery_Score
            // 
            this.Delivery_Score.AutoSize = true;
            this.Delivery_Score.Checked = true;
            this.Delivery_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Delivery_Score.Location = new System.Drawing.Point(19, 264);
            this.Delivery_Score.Name = "Delivery_Score";
            this.Delivery_Score.Size = new System.Drawing.Size(48, 16);
            this.Delivery_Score.TabIndex = 8;
            this.Delivery_Score.Text = "交货";
            this.Delivery_Score.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "次标准";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "主标准";
            // 
            // SupplierComparativeAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 639);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pl_drawing);
            this.Controls.Add(this.gb_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierComparativeAnalysis";
            this.Text = "供应商对比分析";
            this.gb_query.ResumeLayout(false);
            this.gb_query.PerformLayout();
            this.pl_drawing.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_query;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.Panel pl_drawing;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.ComboBox cbb_PurchaseOrganization;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Supplier;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox QuantityReliability_Score;
        private System.Windows.Forms.CheckBox ConfirmDate_Score;
        private System.Windows.Forms.CheckBox Shipment_Score;
        private System.Windows.Forms.CheckBox OnTimeDelivery_Score;
        private System.Windows.Forms.CheckBox ComplaintAndReject_Score;
        private System.Windows.Forms.CheckBox Delivery_Score;
        private System.Windows.Forms.CheckBox QualityAudit_Score;
        private System.Windows.Forms.CheckBox GoodReceipt_Score;
        private System.Windows.Forms.CheckBox Quality_Score;
        private System.Windows.Forms.CheckBox PriceHistory_Score;
        private System.Windows.Forms.CheckBox PriceLevel_Score;
        private System.Windows.Forms.CheckBox Price_Score;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox ExternalService_Score;
        private System.Windows.Forms.CheckBox GeneralServiceAndSupport_Score;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;

    }
}