﻿namespace MMClient.RA.SupplierPerformance
{
    partial class SupplierScoreRanking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel_query = new System.Windows.Forms.Panel();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgv_ComparativeData = new System.Windows.Forms.DataGridView();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Column_TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Quality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Delivery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Service = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgv_SupplierTotalStandard = new System.Windows.Forms.DataGridView();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceLevel_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceHistory_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quality_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodReceipt_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualityAudit_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComplaintAndReject_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delivery_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OnTimeDelivery_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmDate_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuantityReliability_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Shipment_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExternalService_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GeneralServiceAndSupport_Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_Performance = new System.Windows.Forms.Panel();
            this.panel_query.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ComparativeData)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierTotalStandard)).BeginInit();
            this.panel_Performance.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_query
            // 
            this.panel_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_query.BackColor = System.Drawing.SystemColors.Control;
            this.panel_query.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_query.Controls.Add(this.lb_PurchaseGroup);
            this.panel_query.Controls.Add(this.cbb_MaterialGroup);
            this.panel_query.Controls.Add(this.lb_MaterialGroup);
            this.panel_query.Controls.Add(this.btn_Query);
            this.panel_query.Location = new System.Drawing.Point(12, 12);
            this.panel_query.Name = "panel_query";
            this.panel_query.Size = new System.Drawing.Size(1138, 90);
            this.panel_query.TabIndex = 1;
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(115, 37);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbb_PurchaseGroup.TabIndex = 7;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(44, 40);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_PurchaseGroup.TabIndex = 6;
            this.lb_PurchaseGroup.Text = "采购组织：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(357, 37);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(123, 20);
            this.cbb_MaterialGroup.TabIndex = 5;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(283, 41);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 4;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(575, 30);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(91, 32);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ItemSize = new System.Drawing.Size(132, 28);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1131, 405);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.dgv_ComparativeData);
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1123, 369);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "供应商主标准得分排名";
            // 
            // dgv_ComparativeData
            // 
            this.dgv_ComparativeData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_ComparativeData.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_ComparativeData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ComparativeData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ComparativeData.ColumnHeadersHeight = 30;
            this.dgv_ComparativeData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_ComparativeData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierName,
            this.Column_TotalScore,
            this.Column_Quality,
            this.Column_Cost,
            this.Column_Delivery,
            this.Column_Service});
            this.dgv_ComparativeData.EnableHeadersVisualStyles = false;
            this.dgv_ComparativeData.Location = new System.Drawing.Point(3, 6);
            this.dgv_ComparativeData.Name = "dgv_ComparativeData";
            this.dgv_ComparativeData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_ComparativeData.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_ComparativeData.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_ComparativeData.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_ComparativeData.RowTemplate.Height = 26;
            this.dgv_ComparativeData.RowTemplate.ReadOnly = true;
            this.dgv_ComparativeData.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ComparativeData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ComparativeData.Size = new System.Drawing.Size(743, 341);
            this.dgv_ComparativeData.TabIndex = 0;
            this.dgv_ComparativeData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ComparativeData_CellContentClick);
            this.dgv_ComparativeData.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ComparativeData_CellMouseEnter);
            this.dgv_ComparativeData.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ComparativeData_CellMouseLeave);
            this.dgv_ComparativeData.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_ComparativeData_RowPostPaint);
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.DataPropertyName = "Supplier_Name";
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column_SupplierName.Width = 200;
            // 
            // Column_TotalScore
            // 
            this.Column_TotalScore.DataPropertyName = "Total_Score";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.Column_TotalScore.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_TotalScore.HeaderText = "总分";
            this.Column_TotalScore.Name = "Column_TotalScore";
            // 
            // Column_Quality
            // 
            this.Column_Quality.DataPropertyName = "Quality_Score";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Column_Quality.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_Quality.HeaderText = "质量";
            this.Column_Quality.Name = "Column_Quality";
            // 
            // Column_Cost
            // 
            this.Column_Cost.DataPropertyName = "Price_Score";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.Column_Cost.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column_Cost.HeaderText = "成本";
            this.Column_Cost.Name = "Column_Cost";
            // 
            // Column_Delivery
            // 
            this.Column_Delivery.DataPropertyName = "Delivery_Score";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.Column_Delivery.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column_Delivery.HeaderText = "交付";
            this.Column_Delivery.Name = "Column_Delivery";
            // 
            // Column_Service
            // 
            this.Column_Service.DataPropertyName = "GeneralServiceAndSupport_Score";
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.Column_Service.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column_Service.HeaderText = "服务";
            this.Column_Service.Name = "Column_Service";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.dgv_SupplierTotalStandard);
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1123, 369);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "供应商主次标准得分排名";
            // 
            // dgv_SupplierTotalStandard
            // 
            this.dgv_SupplierTotalStandard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_SupplierTotalStandard.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_SupplierTotalStandard.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierTotalStandard.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_SupplierTotalStandard.ColumnHeadersHeight = 30;
            this.dgv_SupplierTotalStandard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supplier_ID,
            this.Column1,
            this.Price_Score,
            this.PriceLevel_Score,
            this.PriceHistory_Score,
            this.Quality_Score,
            this.GoodReceipt_Score,
            this.QualityAudit_Score,
            this.ComplaintAndReject_Score,
            this.Delivery_Score,
            this.OnTimeDelivery_Score,
            this.ConfirmDate_Score,
            this.QuantityReliability_Score,
            this.Shipment_Score,
            this.ExternalService_Score,
            this.GeneralServiceAndSupport_Score});
            this.dgv_SupplierTotalStandard.EnableHeadersVisualStyles = false;
            this.dgv_SupplierTotalStandard.Location = new System.Drawing.Point(4, 6);
            this.dgv_SupplierTotalStandard.Name = "dgv_SupplierTotalStandard";
            this.dgv_SupplierTotalStandard.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierTotalStandard.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_SupplierTotalStandard.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_SupplierTotalStandard.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_SupplierTotalStandard.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierTotalStandard.RowTemplate.Height = 26;
            this.dgv_SupplierTotalStandard.RowTemplate.ReadOnly = true;
            this.dgv_SupplierTotalStandard.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_SupplierTotalStandard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierTotalStandard.Size = new System.Drawing.Size(1113, 357);
            this.dgv_SupplierTotalStandard.TabIndex = 0;
            this.dgv_SupplierTotalStandard.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierTotalStandard_CellContentClick);
            this.dgv_SupplierTotalStandard.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierTotalStandard_CellMouseEnter);
            this.dgv_SupplierTotalStandard.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierTotalStandard_CellMouseLeave);
            this.dgv_SupplierTotalStandard.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SupplierTotalStandard_RowPostPaint);
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "Supplier_Name";
            this.Supplier_ID.HeaderText = "供应商名称";
            this.Supplier_ID.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Supplier_ID.Name = "Supplier_ID";
            this.Supplier_ID.ReadOnly = true;
            this.Supplier_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Supplier_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Total_Score";
            this.Column1.HeaderText = "总分";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Price_Score
            // 
            this.Price_Score.DataPropertyName = "Price_Score";
            this.Price_Score.HeaderText = "价格";
            this.Price_Score.Name = "Price_Score";
            this.Price_Score.ReadOnly = true;
            // 
            // PriceLevel_Score
            // 
            this.PriceLevel_Score.DataPropertyName = "PriceLevel_Score";
            this.PriceLevel_Score.HeaderText = "价格水平";
            this.PriceLevel_Score.Name = "PriceLevel_Score";
            this.PriceLevel_Score.ReadOnly = true;
            // 
            // PriceHistory_Score
            // 
            this.PriceHistory_Score.DataPropertyName = "PriceHistory_Score";
            this.PriceHistory_Score.HeaderText = "价格历史";
            this.PriceHistory_Score.Name = "PriceHistory_Score";
            this.PriceHistory_Score.ReadOnly = true;
            // 
            // Quality_Score
            // 
            this.Quality_Score.DataPropertyName = "Quality_Score";
            this.Quality_Score.HeaderText = "质量";
            this.Quality_Score.Name = "Quality_Score";
            this.Quality_Score.ReadOnly = true;
            // 
            // GoodReceipt_Score
            // 
            this.GoodReceipt_Score.DataPropertyName = "GoodReceipt_Score";
            this.GoodReceipt_Score.HeaderText = "物料接收";
            this.GoodReceipt_Score.Name = "GoodReceipt_Score";
            this.GoodReceipt_Score.ReadOnly = true;
            // 
            // QualityAudit_Score
            // 
            this.QualityAudit_Score.DataPropertyName = "QualityAudit_Score";
            this.QualityAudit_Score.HeaderText = "质量审计";
            this.QualityAudit_Score.Name = "QualityAudit_Score";
            this.QualityAudit_Score.ReadOnly = true;
            // 
            // ComplaintAndReject_Score
            // 
            this.ComplaintAndReject_Score.DataPropertyName = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.HeaderText = "抱怨拒绝水平";
            this.ComplaintAndReject_Score.Name = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.ReadOnly = true;
            this.ComplaintAndReject_Score.Width = 120;
            // 
            // Delivery_Score
            // 
            this.Delivery_Score.DataPropertyName = "Delivery_Score";
            this.Delivery_Score.HeaderText = "交货";
            this.Delivery_Score.Name = "Delivery_Score";
            this.Delivery_Score.ReadOnly = true;
            // 
            // OnTimeDelivery_Score
            // 
            this.OnTimeDelivery_Score.DataPropertyName = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.HeaderText = "按时交货";
            this.OnTimeDelivery_Score.Name = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.ReadOnly = true;
            // 
            // ConfirmDate_Score
            // 
            this.ConfirmDate_Score.DataPropertyName = "ConfirmDate_Score";
            this.ConfirmDate_Score.HeaderText = "确认日期";
            this.ConfirmDate_Score.Name = "ConfirmDate_Score";
            this.ConfirmDate_Score.ReadOnly = true;
            // 
            // QuantityReliability_Score
            // 
            this.QuantityReliability_Score.DataPropertyName = "QuantityReliability_Score";
            this.QuantityReliability_Score.HeaderText = "质量可靠性";
            this.QuantityReliability_Score.Name = "QuantityReliability_Score";
            this.QuantityReliability_Score.ReadOnly = true;
            // 
            // Shipment_Score
            // 
            this.Shipment_Score.DataPropertyName = "Shipment_Score";
            this.Shipment_Score.HeaderText = "装运须知";
            this.Shipment_Score.Name = "Shipment_Score";
            this.Shipment_Score.ReadOnly = true;
            // 
            // ExternalService_Score
            // 
            this.ExternalService_Score.DataPropertyName = "ExternalService_Score";
            this.ExternalService_Score.HeaderText = "外部服务";
            this.ExternalService_Score.Name = "ExternalService_Score";
            this.ExternalService_Score.ReadOnly = true;
            // 
            // GeneralServiceAndSupport_Score
            // 
            this.GeneralServiceAndSupport_Score.DataPropertyName = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.HeaderText = "一般服务/支持";
            this.GeneralServiceAndSupport_Score.Name = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.ReadOnly = true;
            this.GeneralServiceAndSupport_Score.Width = 120;
            // 
            // panel_Performance
            // 
            this.panel_Performance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Performance.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Performance.Controls.Add(this.tabControl1);
            this.panel_Performance.Location = new System.Drawing.Point(13, 105);
            this.panel_Performance.Name = "panel_Performance";
            this.panel_Performance.Size = new System.Drawing.Size(1137, 411);
            this.panel_Performance.TabIndex = 4;
            // 
            // SupplierScoreRanking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 520);
            this.Controls.Add(this.panel_Performance);
            this.Controls.Add(this.panel_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierScoreRanking";
            this.Text = "供应商得分排名";
            this.panel_query.ResumeLayout(false);
            this.panel_query.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ComparativeData)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierTotalStandard)).EndInit();
            this.panel_Performance.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_query;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgv_ComparativeData;
        private System.Windows.Forms.DataGridViewLinkColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Quality;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Delivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Service;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel_Performance;
        private System.Windows.Forms.DataGridView dgv_SupplierTotalStandard;
        private System.Windows.Forms.DataGridViewLinkColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceLevel_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceHistory_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quality_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoodReceipt_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualityAudit_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComplaintAndReject_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Delivery_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn OnTimeDelivery_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmDate_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityReliability_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shipment_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExternalService_Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn GeneralServiceAndSupport_Score;
    }
}