﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.RA.SupplierPerformance
{
    public partial class SupplierView : Form
    {
        //只读型常量
        private readonly string str_txt_ChooseSupplier = "供应商编码/供应商名称";
        //获取父窗体值的属性
        public string ComboBoxValue { get; set; }
        SupplierComparativeAnalysis ssr = null;
        //定义一个string类型的集合
        List<string> list = new List<string>();

        public SupplierView():this(null,null)
        {
            //调用自身构造函数
        }

        public SupplierView(string comboBoxValue,SupplierComparativeAnalysis ssr)
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_SupplierDetails.TopLeftHeaderCell.Value = "行号";
            //获取从父窗体传递过来的comboBoxValue
            this.ComboBoxValue = comboBoxValue;
            this.ssr = ssr;
            //当窗体加载时，让其焦点在标签上
            this.lb_ChooseSupplier.Select();
            this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;
            //this.txt_ChooseSupplier.SelectionStart = 0;   //初试选中位置为0
            //this.txt_ChooseSupplier.SelectionLength = 0;//初试选中内容长度
            //窗体加载时选中第一项
            this.cbb_Country.SelectedIndex = 0;
        }

        /// <summary>
        /// 点击取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 点击搜索按钮时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            string sqlTexDetail = @"SELECT Supplier_Base.Supplier_ID,Supplier_Base.Supplier_Name,Supplier_Base.[Address] FROM Buyer JOIN Material_Supplier ON Buyer.Material_ID=Material_Supplier.Material_ID JOIN Supplier_Base ON Material_Supplier.Supplier_ID=Supplier_Base.Supplier_ID WHERE Buyer.Buyer_Org_Name='" + this.ComboBoxValue+"'";
            //供应商选择条件
            if (!this.txt_ChooseSupplier.Text.Equals(str_txt_ChooseSupplier) && this.txt_ChooseSupplier.Text.Trim() != null)
            {
                sqlTexDetail += " AND Supplier_Base.Supplier_ID='" + this.txt_ChooseSupplier.Text.Trim() + "' OR Supplier_Base.Supplier_Name='" + this.txt_ChooseSupplier.Text.Trim() + "'"; 
            }
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlTexDetail);
                this.DataGridViewShow(dt);
                this.lb_Number.Text = Convert.ToString(dt.Rows.Count);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void DataGridViewShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dgv_SupplierDetails.DataSource = dt;
                }
            }
        }

        /// <summary>
        /// 当焦点在textbox上触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Enter(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim().Equals(str_txt_ChooseSupplier))
            {
                //将textbox内容置空
                this.txt_ChooseSupplier.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.Black;
            }
        }

        /// <summary>
        /// 当焦点离开textbox时触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Leave(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim() == "")
            {
                this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        /// <summary>
        /// 给datagridview画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 点击确认按钮时将至返回给父窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            /*
            List<double> listd = new List<double>();
            listd.Add(1);
            listd.Add(2.1);
            double[] d = listd.ToArray();
            foreach (double i in d)
            {
                MessageBox.Show(i.ToString());
            }
            */

            this.getSelectedSupplierName();
            if (list != null && list.Count > 0)
            {
                ssr.update_TxtSupplier(list);
                this.Close(); 
            }
            else
            {
                MessageBox.Show("至少选择一个供应商","温馨提示");
            }  
        }

        private void getSelectedSupplierName()
        {
            //遍历datagridview行
            foreach (DataGridViewRow dgvr in this.dgv_SupplierDetails.Rows)
            {
                //判断勾选 是否被选中，若被选中则将供应商名称加入list中
                if (dgvr.Cells["Column_Choose"].Value != null)
                {
                    if (Convert.ToBoolean(dgvr.Cells["Column_Choose"].Value))
                    {
                        list.Add(Convert.ToString(dgvr.Cells["Column_SupplierName"].Value));
                    } 
                }
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDetails_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_SupplierDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDetails_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_SupplierDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }
    }
}
