﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using Lib.SqlServerDAL;
using MMClient.RA.ChartClass;
using Lib.Common.CommonUtils;

namespace MMClient.RA.SupplierAnalysis
{
    public partial class SupplierAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        //新建一个气泡图对象
        SA_BubbleChart sabc = new SA_BubbleChart();

        public SupplierAnalysis()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 点击按钮，生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = this.chartViewer1;
            string sqlText = @"WITH T AS(SELECT Supplier_ID,sum(Material_Count*Price) AS TotalMoney FROM [Order] GROUP BY [Order].Supplier_ID)SELECT Supplier_Base.Supplier_Name,TotalMoney,[Supplier_Performance Analysis].Score FROM T JOIN Supplier_Base ON T.Supplier_ID=Supplier_Base.Supplier_ID JOIN [Supplier_Performance Analysis] ON T.Supplier_ID=[Supplier_Performance Analysis].Supplier_ID WHERE [Supplier_Performance Analysis].Standard_ID=0";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.BubbleChart(viewer, dt);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        #region 绘制分析图表函数
        /// <summary>
        /// 绘制气泡分析图
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="viewer"></param>
        private void BubbleChart(WinChartViewer viewer,DataTable dt)
        {
            this.sabc.createChart(viewer, dt);
        }

        #endregion

        //Hot Spot
        private void chartViewer1_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }
    }
}
