﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.RA;
using System.Drawing.Printing;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.RA.SupplierDependentFactors
{
    public partial class SupplierDependentFactors : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 与打印相关类声明
        //打印DataGridView类
        DataGridViewPrinter MyDataGridViewPrinter;
        #endregion

        #region 窗体初始化
        public SupplierDependentFactors()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_SupplierDependentFactor.TopLeftHeaderCell.Value = "行号";

            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表事件
        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            /*
            //获取当前dataGridView列之间的宽度
            int width = this.dgv_SupplierDependentFactor.Columns["Column_SupplierName"].Width;
            //获取当前系统时间,该时间为SQL Server数据库中的DataTime类型
            DateTime dt = DateTime.Now;
            string currentTime = string.Format("{0}年{1}月{2}日",dt.Year,dt.Month,dt.Day);
            MessageBox.Show(currentTime);
             * */
            try
            {
                string sqlText = "WITH T AS (SELECT Supplier_ID,SUM(Material_Count*Price) AS TotalAmount FROM [Order] WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "'  GROUP BY Supplier_ID) SELECT Supplier_Base.Supplier_Name,Supplier_Base.Supplier_ID,Supplier_Base.Annual_Sales,T.TotalAmount,T.TotalAmount/Supplier_Base.Annual_Sales AS Rate, Supplier_Base.Enterprise FROM T JOIN Supplier_Base ON T.Supplier_ID=Supplier_Base.Supplier_ID;";
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.DataGridShow(dt);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        #endregion

        #region 与DataGridView操作相关的方法
        /// <summary>
        /// 将数据绑定在DataGridView上
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dgv_SupplierDependentFactor.DataSource = dt;
                    this.ShareCategory();
                }
            }
        }

        private void ShareCategory()
        {
            double temp = 0;
            for (int i = 0; i < dgv_SupplierDependentFactor.Rows.Count; i++)
            {
                if (dgv_SupplierDependentFactor.Rows[i].Cells[4].Value != null)
                {
                    temp = Convert.ToDouble(dgv_SupplierDependentFactor.Rows[i].Cells[4].Value) * 100;
                    if (temp < 0.8)
                    {
                        dgv_SupplierDependentFactor.Rows[i].Cells[5].Value = "N";
                        //dgv_StockAgeAnalysis.Rows[i].Cells[6].Style.BackColor = System.Drawing.Color.GreenYellow;
                    }
                    else if (temp >= 0.8 && temp < 5)
                    {
                        dgv_SupplierDependentFactor.Rows[i].Cells[5].Value = "L";
                        //dgv_SupplierDependentFactor.Rows[i].Cells[5].Style.BackColor = System.Drawing.Color.Yellow;
                    }
                    else if (temp >= 5 && temp < 15)
                    {
                        dgv_SupplierDependentFactor.Rows[i].Cells[5].Value = "M";
                        //dgv_SupplierDependentFactor.Rows[i].Cells[5].Style.BackColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        dgv_SupplierDependentFactor.Rows[i].Cells[5].Value = "H";
                        //dgv_SupplierDependentFactor.Rows[i].Cells[5].Style.BackColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
        #endregion

        #region 供应商依赖因素分析明细表打印程序
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Print_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                detailChartPrintDocument.Print();
            }
        }
        /// <summary>
        /// 打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void detailChartPrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        /// <summary>
        /// 打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = detailChartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }
        private bool SetupThePrinting()
        {
            detailChartPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.lb_Title.Text.Trim();
            detailChartPrintDocument.DocumentName = titleName;
            detailChartPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            detailChartPrintDocument.DefaultPageSettings = detailChartPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dgv_SupplierDependentFactor, detailChartPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
        #endregion

        #region 绘制DataGridView行号
        /// <summary>
        /// 给dgv画出行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyDataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        #endregion

        /// <summary>
        /// 当鼠标移出单元时触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDependentFactor_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierDependentFactor.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_SupplierDependentFactor.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDependentFactor_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierDependentFactor.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_SupplierDependentFactor.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 点击单元格内容时触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SupplierDependentFactor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //当点击第一列单元格（公司名称）中内容时触发事件
            if (e.ColumnIndex == 0)
            {
                //MessageBox.Show(this.dgv_SupplierDependentFactor.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                new SupplierDetails(this.dgv_SupplierDependentFactor.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()).ShowDialog();
            }
        }

        /// <summary>
        /// 导出到Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Export_Click(object sender, EventArgs e)
        {
            //ExportToExcel.DataToExcel(dgv_SupplierDependentFactor, tempProgressBar, toolStrip);
        }
    }
}
