﻿namespace MMClient.RA.SupplierDependentFactors
{
    partial class SupplierDependentFactors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel_query = new System.Windows.Forms.Panel();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tempProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel_Navigation = new System.Windows.Forms.Panel();
            this.btn_Export = new System.Windows.Forms.Button();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.lb_Title = new System.Windows.Forms.Label();
            this.dgv_SupplierDependentFactor = new System.Windows.Forms.DataGridView();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Column_SupplierNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_AnnualSales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_InvoicValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_DependencyRatio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_ShareCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detailChartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.detailChartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.panel_query.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.panel_Navigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDependentFactor)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_query
            // 
            this.panel_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_query.BackColor = System.Drawing.SystemColors.Control;
            this.panel_query.Controls.Add(this.dtp_EndTime);
            this.panel_query.Controls.Add(this.lb_To);
            this.panel_query.Controls.Add(this.dtp_StartTime);
            this.panel_query.Controls.Add(this.lb_Time);
            this.panel_query.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_query.Controls.Add(this.lb_PurchaseGroup);
            this.panel_query.Controls.Add(this.cbb_MaterialGroup);
            this.panel_query.Controls.Add(this.lb_MaterialGroup);
            this.panel_query.Controls.Add(this.btn_Query);
            this.panel_query.Location = new System.Drawing.Point(6, 2);
            this.panel_query.Name = "panel_query";
            this.panel_query.Size = new System.Drawing.Size(1138, 90);
            this.panel_query.TabIndex = 0;
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(229, 35);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 11;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(206, 39);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 10;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(78, 35);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 9;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(40, 39);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 8;
            this.lb_Time.Text = "时间：";
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(488, 36);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbb_PurchaseGroup.TabIndex = 7;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(417, 40);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_PurchaseGroup.TabIndex = 6;
            this.lb_PurchaseGroup.Text = "采购组织：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(684, 36);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(123, 20);
            this.cbb_MaterialGroup.TabIndex = 5;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(625, 40);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 4;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(880, 31);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(91, 32);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.statusStrip);
            this.panel2.Controls.Add(this.panel_Navigation);
            this.panel2.Controls.Add(this.dgv_SupplierDependentFactor);
            this.panel2.Location = new System.Drawing.Point(3, 93);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1144, 423);
            this.panel2.TabIndex = 1;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tempProgressBar,
            this.toolStrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 401);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1144, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // tempProgressBar
            // 
            this.tempProgressBar.Name = "tempProgressBar";
            this.tempProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // toolStrip
            // 
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(32, 17);
            this.toolStrip.Text = "状态";
            // 
            // panel_Navigation
            // 
            this.panel_Navigation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Navigation.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Navigation.Controls.Add(this.btn_Export);
            this.panel_Navigation.Controls.Add(this.btn_Preview);
            this.panel_Navigation.Controls.Add(this.btn_Print);
            this.panel_Navigation.Controls.Add(this.lb_Title);
            this.panel_Navigation.Location = new System.Drawing.Point(3, 3);
            this.panel_Navigation.Name = "panel_Navigation";
            this.panel_Navigation.Size = new System.Drawing.Size(1138, 32);
            this.panel_Navigation.TabIndex = 1;
            // 
            // btn_Export
            // 
            this.btn_Export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Export.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Export.Location = new System.Drawing.Point(1079, 5);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(53, 23);
            this.btn_Export.TabIndex = 3;
            this.btn_Export.Text = "导出";
            this.btn_Export.UseVisualStyleBackColor = true;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // btn_Preview
            // 
            this.btn_Preview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(1020, 5);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(53, 23);
            this.btn_Preview.TabIndex = 3;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(958, 5);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(56, 23);
            this.btn_Print.TabIndex = 2;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click);
            // 
            // lb_Title
            // 
            this.lb_Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_Title.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Title.Location = new System.Drawing.Point(6, 7);
            this.lb_Title.Name = "lb_Title";
            this.lb_Title.Size = new System.Drawing.Size(250, 22);
            this.lb_Title.TabIndex = 0;
            this.lb_Title.Text = "供应商依赖因素分析";
            // 
            // dgv_SupplierDependentFactor
            // 
            this.dgv_SupplierDependentFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_SupplierDependentFactor.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_SupplierDependentFactor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDependentFactor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_SupplierDependentFactor.ColumnHeadersHeight = 32;
            this.dgv_SupplierDependentFactor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierDependentFactor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierName,
            this.Column_SupplierNo,
            this.Column_AnnualSales,
            this.Column_InvoicValue,
            this.Column_DependencyRatio,
            this.Column_ShareCategory});
            this.dgv_SupplierDependentFactor.EnableHeadersVisualStyles = false;
            this.dgv_SupplierDependentFactor.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.dgv_SupplierDependentFactor.Location = new System.Drawing.Point(4, 37);
            this.dgv_SupplierDependentFactor.MultiSelect = false;
            this.dgv_SupplierDependentFactor.Name = "dgv_SupplierDependentFactor";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDependentFactor.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierDependentFactor.RowTemplate.Height = 26;
            this.dgv_SupplierDependentFactor.RowTemplate.ReadOnly = true;
            this.dgv_SupplierDependentFactor.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_SupplierDependentFactor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierDependentFactor.Size = new System.Drawing.Size(1137, 382);
            this.dgv_SupplierDependentFactor.TabIndex = 0;
            this.dgv_SupplierDependentFactor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierDependentFactor_CellContentClick);
            this.dgv_SupplierDependentFactor.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierDependentFactor_CellMouseEnter);
            this.dgv_SupplierDependentFactor.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierDependentFactor_CellMouseLeave);
            this.dgv_SupplierDependentFactor.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.MyDataGridView_RowPostPaint);
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_SupplierName.DataPropertyName = "Supplier_Name";
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Column_SupplierName.LinkColor = System.Drawing.Color.Blue;
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column_SupplierName.ToolTipText = "ddwd";
            this.Column_SupplierName.VisitedLinkColor = System.Drawing.Color.Purple;
            // 
            // Column_SupplierNo
            // 
            this.Column_SupplierNo.DataPropertyName = "Supplier_ID";
            this.Column_SupplierNo.HeaderText = "供应商编号";
            this.Column_SupplierNo.Name = "Column_SupplierNo";
            this.Column_SupplierNo.ReadOnly = true;
            this.Column_SupplierNo.Width = 200;
            // 
            // Column_AnnualSales
            // 
            this.Column_AnnualSales.DataPropertyName = "Annual_Sales";
            this.Column_AnnualSales.HeaderText = "年营销额";
            this.Column_AnnualSales.Name = "Column_AnnualSales";
            this.Column_AnnualSales.ReadOnly = true;
            this.Column_AnnualSales.Width = 200;
            // 
            // Column_InvoicValue
            // 
            this.Column_InvoicValue.DataPropertyName = "TotalAmount";
            this.Column_InvoicValue.HeaderText = "发票余额";
            this.Column_InvoicValue.Name = "Column_InvoicValue";
            this.Column_InvoicValue.ReadOnly = true;
            this.Column_InvoicValue.Width = 200;
            // 
            // Column_DependencyRatio
            // 
            this.Column_DependencyRatio.DataPropertyName = "Rate";
            dataGridViewCellStyle5.Format = "0.00%";
            dataGridViewCellStyle5.NullValue = null;
            this.Column_DependencyRatio.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column_DependencyRatio.HeaderText = "依赖率";
            this.Column_DependencyRatio.Name = "Column_DependencyRatio";
            this.Column_DependencyRatio.ReadOnly = true;
            this.Column_DependencyRatio.Width = 190;
            // 
            // Column_ShareCategory
            // 
            this.Column_ShareCategory.DataPropertyName = "Enterprise";
            this.Column_ShareCategory.HeaderText = "份额类别";
            this.Column_ShareCategory.Name = "Column_ShareCategory";
            this.Column_ShareCategory.ReadOnly = true;
            // 
            // detailChartPageSetupDialog
            // 
            this.detailChartPageSetupDialog.Document = this.detailChartPrintDocument;
            // 
            // detailChartPrintDocument
            // 
            this.detailChartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.detailChartPrintDocument_PrintPage);
            // 
            // SupplierDependentFactors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 520);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierDependentFactors";
            this.Text = "供应商依赖因素分析";
            this.panel_query.ResumeLayout(false);
            this.panel_query.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panel_Navigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDependentFactor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_query;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.DataGridView dgv_SupplierDependentFactor;
        private System.Windows.Forms.Panel panel_Navigation;
        private System.Windows.Forms.Label lb_Title;
        private System.Windows.Forms.Button btn_Print;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.PageSetupDialog detailChartPageSetupDialog;
        private System.Drawing.Printing.PrintDocument detailChartPrintDocument;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.DataGridViewLinkColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_AnnualSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_InvoicValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_DependencyRatio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ShareCategory;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripProgressBar tempProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStrip;
        private System.Windows.Forms.Button btn_Export;
    }
}