﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Model;

namespace MMClient.RA
{
    public partial class SupplierDetails : Form
    {
        public SupplierBaseInformationBLL sBaseInfoBLL = new SupplierBaseInformationBLL();

        private SupplierBaseInformation sBaseInfo = null;

        private Point mouseOffset; // 记录鼠标指针的坐标

        private bool isMouseDown = false; //记录鼠标按键是否按下

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public SupplierDetails()
        {
            InitializeComponent();
            //当该窗体加载时，让焦点脱离datagridview默认行
            this.lb_SupplierInformation.Select();
        }

        /// <summary>
        /// 有参构造函数：供应商名称
        /// </summary>
        /// <param name="SupplierName"></param>
        public SupplierDetails(string SupplierName)
        {
            InitializeComponent();

            sBaseInfo = sBaseInfoBLL.FindBySupplierName(SupplierName);
            this.SupplierDetailInformation(sBaseInfo);
            //当该窗体加载时，让焦点脱离datagridview默认行
            this.lb_SupplierInformation.Select();
        }

        /// <summary>
        /// 有参构造函数:供应商名称、供应商ID
        /// </summary>
        /// <param name="SupplierName"></param>
        public SupplierDetails(string SupplierName, string SupplierID)
        {
            InitializeComponent();
            if (SupplierID == "")
            {
                sBaseInfo = sBaseInfoBLL.FindBySupplierName(SupplierName);
                this.SupplierDetailInformation(sBaseInfo);
            }

            if (SupplierName == "")
            {
                sBaseInfo = sBaseInfoBLL.FindBySupplierID(SupplierID);
                this.SupplierDetailInformation(sBaseInfo);
            }
            //当该窗体加载时，让焦点脱离datagridview默认行
            this.lb_SupplierInformation.Select();
        }

        /// <summary>
        /// 给窗口中公司详细信息赋值
        /// </summary>
        /// <param name="SupplierName"></param>
        private void SupplierDetailInformation(SupplierBaseInformation sBaseInfo)
        {
            this.lb_BankAccountD.Text = sBaseInfo.Bank_Account;
            this.lb_BankNameD.Text = sBaseInfo.Bank_Name;
            this.lb_NationD.Text = sBaseInfo.Nation;
            this.lb_DetailAddressD.Text = sBaseInfo.Address;
            this.lb_EnterpriseD.Text = sBaseInfo.Enterprise;
            this.lb_FaxD.Text = sBaseInfo.Fax;
            this.lb_FixedPhoneD.Text = sBaseInfo.Payer_No;
            /*
            if (sBaseInfo.IsListedCompany)
            {
                this.lb_IsListedCompanyD.Text = "是";
            }
            else
            {
                this.lb_IsListedCompanyD.Text = "否";
            }
             * */
            this.lb_IsListedCompanyD.Text = sBaseInfo.IsListedCompany;
            this.lb_PostD.Text = sBaseInfo.Zip_Code;
            //this.lb_RegisterTimeD.Text = String.Format("{0}年{1}月{2}日",sBaseInfo.Register_Time.Year,sBaseInfo.Register_Time.Month,sBaseInfo.Register_Time.Day);
            this.lb_RegisterTimeD.Text = sBaseInfo.Register_Time;
            this.lb_RegisterTimeD.Text = sBaseInfo.Register_Time.ToString();
            this.lb_RepresentativeD.Text = sBaseInfo.Representative;
            this.lb_StockCodeD.Text = sBaseInfo.Stock_code;
            this.lb_SupplierLoginNameD.Text = sBaseInfo.Supplier_LoginName;
            this.lb_SupplierNameD.Text = sBaseInfo.Supplier_Name;
            this.lb_URLD.Text = sBaseInfo.URL;
            this.lb_SupplierIDD.Text = sBaseInfo.Supplier_ID;
        }


        /// <summary>
        /// 当鼠标进入单元行时触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_ContactsInformation_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dvg_ContactsInformation.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dvg_ContactsInformation.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标离开单元行时触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_ContactsInformation_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dvg_ContactsInformation.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dvg_ContactsInformation.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        #region 控制无边框窗体移动的几个事件

        /// <summary>
        /// 当鼠标经过Close图片时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Close_MouseEnter(object sender, EventArgs e)
        {
            this.picBox_Close.BackColor = Color.SkyBlue;
           // MessageBox.Show("123");
        }

        /// <summary>
        /// 当鼠标移开Close图片时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Close_MouseLeave(object sender, EventArgs e)
        {
            this.picBox_Close.BackColor = SystemColors.ActiveCaption;
        }

        /// <summary>
        /// 当鼠标点击Close图片时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 当鼠标按下时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Close_MouseDown(object sender, MouseEventArgs e)
        {
            this.picBox_Close.BackColor = SystemColors.ActiveCaption;
        }

        /// <summary>
        /// 双击导航条时发生,使窗口变大或者变为正常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel_Nav_DoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }
        
        /// <summary>
        /// 双击导航标题时发生，是窗口并大或者变为正常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_Nav_DoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }


        private void panel_Nav_MouseDown(object sender, MouseEventArgs e)
        {
            int xOffset;
            int yOffset;
            if(e.Button == MouseButtons.Left)
            {
                xOffset = -e.X - SystemInformation.FrameBorderSize.Width;
                yOffset = -e.Y - SystemInformation.CaptionHeight - SystemInformation.FrameBorderSize.Height;
                mouseOffset = new Point(xOffset, yOffset);
                isMouseDown = true;
            }
        }

        private void panel_Nav_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X + 5, mouseOffset.Y + 30);
                Location = mousePos;
            }
        }

        private void panel_Nav_MouseUp(object sender, MouseEventArgs e)
        {
            //修改鼠标状态isMouseDown的值
            //确保只有鼠标左键按下并移动时，才移动窗体
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = false;
            }
        }

        #endregion

        /// <summary>
        /// 绘制边框颜色(有问题？)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplierDetails_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
            Pen p = new Pen(SystemColors.ActiveCaption);
            g.DrawRectangle(p, rect);
        }

        
    }
}
