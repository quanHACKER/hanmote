﻿namespace MMClient.RA
{
    partial class SupplierDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lb_CheckSupplier = new System.Windows.Forms.Label();
            this.panel_SupplierInformation = new System.Windows.Forms.Panel();
            this.lb_SupplierInformation = new System.Windows.Forms.Label();
            this.panel_ContactInformation = new System.Windows.Forms.Panel();
            this.lb_ContactsInformation = new System.Windows.Forms.Label();
            this.panel_ContactsInformationDetail = new System.Windows.Forms.Panel();
            this.dvg_ContactsInformation = new System.Windows.Forms.DataGridView();
            this.Column_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Duty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Fax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_MobilePhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_QQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lb_SupplierName = new System.Windows.Forms.Label();
            this.lb_SupplierID = new System.Windows.Forms.Label();
            this.lb_SupplierLoginName = new System.Windows.Forms.Label();
            this.lb_Enterprise = new System.Windows.Forms.Label();
            this.lb_Nation = new System.Windows.Forms.Label();
            this.lb_DetailAddress = new System.Windows.Forms.Label();
            this.lb_RegisterTime = new System.Windows.Forms.Label();
            this.lb_Representative = new System.Windows.Forms.Label();
            this.lb_BankAccount = new System.Windows.Forms.Label();
            this.lb_IsListedCompany = new System.Windows.Forms.Label();
            this.lb_StockCode = new System.Windows.Forms.Label();
            this.lb_FixedPhone = new System.Windows.Forms.Label();
            this.lb_Fax = new System.Windows.Forms.Label();
            this.lb_SupplierNameD = new System.Windows.Forms.Label();
            this.lb_SupplierLoginNameD = new System.Windows.Forms.Label();
            this.panel_SupplierInformationDetails = new System.Windows.Forms.Panel();
            this.lb_URLD = new System.Windows.Forms.Label();
            this.lb_FixedPhoneD = new System.Windows.Forms.Label();
            this.lb_IsListedCompanyD = new System.Windows.Forms.Label();
            this.lb_NationD = new System.Windows.Forms.Label();
            this.lb_BankNameD = new System.Windows.Forms.Label();
            this.lb_RegisterTimeD = new System.Windows.Forms.Label();
            this.lb_PostD = new System.Windows.Forms.Label();
            this.lb_FaxD = new System.Windows.Forms.Label();
            this.lb_StockCodeD = new System.Windows.Forms.Label();
            this.lb_BankAccountD = new System.Windows.Forms.Label();
            this.lb_RepresentativeD = new System.Windows.Forms.Label();
            this.lb_DetailAddressD = new System.Windows.Forms.Label();
            this.lb_EnterpriseD = new System.Windows.Forms.Label();
            this.lb_SupplierIDD = new System.Windows.Forms.Label();
            this.lb_Post = new System.Windows.Forms.Label();
            this.lb_BankName = new System.Windows.Forms.Label();
            this.lb_URL = new System.Windows.Forms.Label();
            this.panel_Nav = new System.Windows.Forms.Panel();
            this.lb_Nav = new System.Windows.Forms.Label();
            this.picBox_Close = new System.Windows.Forms.PictureBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panel_SupplierInformation.SuspendLayout();
            this.panel_ContactInformation.SuspendLayout();
            this.panel_ContactsInformationDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg_ContactsInformation)).BeginInit();
            this.panel_SupplierInformationDetails.SuspendLayout();
            this.panel_Nav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // lb_CheckSupplier
            // 
            this.lb_CheckSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_CheckSupplier.BackColor = System.Drawing.Color.White;
            this.lb_CheckSupplier.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_CheckSupplier.Location = new System.Drawing.Point(375, 68);
            this.lb_CheckSupplier.Name = "lb_CheckSupplier";
            this.lb_CheckSupplier.Size = new System.Drawing.Size(213, 40);
            this.lb_CheckSupplier.TabIndex = 0;
            this.lb_CheckSupplier.Text = "查看供应商";
            this.lb_CheckSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_SupplierInformation
            // 
            this.panel_SupplierInformation.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_SupplierInformation.Controls.Add(this.lb_SupplierInformation);
            this.panel_SupplierInformation.Location = new System.Drawing.Point(36, 120);
            this.panel_SupplierInformation.Name = "panel_SupplierInformation";
            this.panel_SupplierInformation.Size = new System.Drawing.Size(889, 28);
            this.panel_SupplierInformation.TabIndex = 1;
            // 
            // lb_SupplierInformation
            // 
            this.lb_SupplierInformation.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_SupplierInformation.Location = new System.Drawing.Point(3, 6);
            this.lb_SupplierInformation.Name = "lb_SupplierInformation";
            this.lb_SupplierInformation.Size = new System.Drawing.Size(100, 19);
            this.lb_SupplierInformation.TabIndex = 0;
            this.lb_SupplierInformation.Text = "公司信息";
            // 
            // panel_ContactInformation
            // 
            this.panel_ContactInformation.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_ContactInformation.Controls.Add(this.lb_ContactsInformation);
            this.panel_ContactInformation.Location = new System.Drawing.Point(36, 498);
            this.panel_ContactInformation.Name = "panel_ContactInformation";
            this.panel_ContactInformation.Size = new System.Drawing.Size(889, 28);
            this.panel_ContactInformation.TabIndex = 3;
            // 
            // lb_ContactsInformation
            // 
            this.lb_ContactsInformation.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ContactsInformation.Location = new System.Drawing.Point(3, 6);
            this.lb_ContactsInformation.Name = "lb_ContactsInformation";
            this.lb_ContactsInformation.Size = new System.Drawing.Size(114, 19);
            this.lb_ContactsInformation.TabIndex = 0;
            this.lb_ContactsInformation.Text = "联系人信息";
            // 
            // panel_ContactsInformationDetail
            // 
            this.panel_ContactsInformationDetail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel_ContactsInformationDetail.Controls.Add(this.dvg_ContactsInformation);
            this.panel_ContactsInformationDetail.Location = new System.Drawing.Point(36, 527);
            this.panel_ContactsInformationDetail.Name = "panel_ContactsInformationDetail";
            this.panel_ContactsInformationDetail.Size = new System.Drawing.Size(874, 106);
            this.panel_ContactsInformationDetail.TabIndex = 4;
            // 
            // dvg_ContactsInformation
            // 
            this.dvg_ContactsInformation.BackgroundColor = System.Drawing.Color.White;
            this.dvg_ContactsInformation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.MenuText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvg_ContactsInformation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dvg_ContactsInformation.ColumnHeadersHeight = 32;
            this.dvg_ContactsInformation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_Name,
            this.Column_Duty,
            this.Column_Tel,
            this.Column_Fax,
            this.Column_MobilePhone,
            this.Column_QQ,
            this.Column_Email,
            this.Column_Remark});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvg_ContactsInformation.DefaultCellStyle = dataGridViewCellStyle11;
            this.dvg_ContactsInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg_ContactsInformation.EnableHeadersVisualStyles = false;
            this.dvg_ContactsInformation.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.dvg_ContactsInformation.Location = new System.Drawing.Point(0, 0);
            this.dvg_ContactsInformation.MultiSelect = false;
            this.dvg_ContactsInformation.Name = "dvg_ContactsInformation";
            this.dvg_ContactsInformation.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvg_ContactsInformation.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dvg_ContactsInformation.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dvg_ContactsInformation.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dvg_ContactsInformation.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dvg_ContactsInformation.RowTemplate.Height = 26;
            this.dvg_ContactsInformation.RowTemplate.ReadOnly = true;
            this.dvg_ContactsInformation.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dvg_ContactsInformation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvg_ContactsInformation.Size = new System.Drawing.Size(874, 106);
            this.dvg_ContactsInformation.TabIndex = 0;
            this.dvg_ContactsInformation.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_ContactsInformation_CellMouseEnter);
            this.dvg_ContactsInformation.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_ContactsInformation_CellMouseLeave);
            // 
            // Column_Name
            // 
            this.Column_Name.HeaderText = "姓名";
            this.Column_Name.Name = "Column_Name";
            this.Column_Name.ReadOnly = true;
            this.Column_Name.Width = 110;
            // 
            // Column_Duty
            // 
            this.Column_Duty.HeaderText = "职务";
            this.Column_Duty.Name = "Column_Duty";
            this.Column_Duty.ReadOnly = true;
            // 
            // Column_Tel
            // 
            this.Column_Tel.HeaderText = "电话";
            this.Column_Tel.Name = "Column_Tel";
            this.Column_Tel.ReadOnly = true;
            // 
            // Column_Fax
            // 
            this.Column_Fax.HeaderText = "传真";
            this.Column_Fax.Name = "Column_Fax";
            this.Column_Fax.ReadOnly = true;
            // 
            // Column_MobilePhone
            // 
            this.Column_MobilePhone.HeaderText = "手机";
            this.Column_MobilePhone.Name = "Column_MobilePhone";
            this.Column_MobilePhone.ReadOnly = true;
            this.Column_MobilePhone.Width = 113;
            // 
            // Column_QQ
            // 
            this.Column_QQ.HeaderText = "QQ";
            this.Column_QQ.Name = "Column_QQ";
            this.Column_QQ.ReadOnly = true;
            // 
            // Column_Email
            // 
            this.Column_Email.HeaderText = "电子邮箱";
            this.Column_Email.Name = "Column_Email";
            this.Column_Email.ReadOnly = true;
            this.Column_Email.Width = 110;
            // 
            // Column_Remark
            // 
            this.Column_Remark.HeaderText = "备注";
            this.Column_Remark.Name = "Column_Remark";
            this.Column_Remark.ReadOnly = true;
            // 
            // lb_SupplierName
            // 
            this.lb_SupplierName.AutoSize = true;
            this.lb_SupplierName.Location = new System.Drawing.Point(49, 22);
            this.lb_SupplierName.Name = "lb_SupplierName";
            this.lb_SupplierName.Size = new System.Drawing.Size(77, 12);
            this.lb_SupplierName.TabIndex = 0;
            this.lb_SupplierName.Text = "供应商名称：";
            // 
            // lb_SupplierID
            // 
            this.lb_SupplierID.AutoSize = true;
            this.lb_SupplierID.Location = new System.Drawing.Point(434, 22);
            this.lb_SupplierID.Name = "lb_SupplierID";
            this.lb_SupplierID.Size = new System.Drawing.Size(77, 12);
            this.lb_SupplierID.TabIndex = 1;
            this.lb_SupplierID.Text = "供应商编码：";
            // 
            // lb_SupplierLoginName
            // 
            this.lb_SupplierLoginName.AutoSize = true;
            this.lb_SupplierLoginName.Location = new System.Drawing.Point(25, 56);
            this.lb_SupplierLoginName.Name = "lb_SupplierLoginName";
            this.lb_SupplierLoginName.Size = new System.Drawing.Size(101, 12);
            this.lb_SupplierLoginName.TabIndex = 2;
            this.lb_SupplierLoginName.Text = "供应商登记名称：";
            // 
            // lb_Enterprise
            // 
            this.lb_Enterprise.AutoSize = true;
            this.lb_Enterprise.Location = new System.Drawing.Point(445, 56);
            this.lb_Enterprise.Name = "lb_Enterprise";
            this.lb_Enterprise.Size = new System.Drawing.Size(65, 12);
            this.lb_Enterprise.TabIndex = 3;
            this.lb_Enterprise.Text = "企业性质：";
            // 
            // lb_Nation
            // 
            this.lb_Nation.AutoSize = true;
            this.lb_Nation.Location = new System.Drawing.Point(61, 92);
            this.lb_Nation.Name = "lb_Nation";
            this.lb_Nation.Size = new System.Drawing.Size(65, 12);
            this.lb_Nation.TabIndex = 4;
            this.lb_Nation.Text = "所在国家：";
            // 
            // lb_DetailAddress
            // 
            this.lb_DetailAddress.AutoSize = true;
            this.lb_DetailAddress.Location = new System.Drawing.Point(446, 92);
            this.lb_DetailAddress.Name = "lb_DetailAddress";
            this.lb_DetailAddress.Size = new System.Drawing.Size(65, 12);
            this.lb_DetailAddress.TabIndex = 5;
            this.lb_DetailAddress.Text = "详细地址：";
            // 
            // lb_RegisterTime
            // 
            this.lb_RegisterTime.AutoSize = true;
            this.lb_RegisterTime.Location = new System.Drawing.Point(61, 128);
            this.lb_RegisterTime.Name = "lb_RegisterTime";
            this.lb_RegisterTime.Size = new System.Drawing.Size(65, 12);
            this.lb_RegisterTime.TabIndex = 7;
            this.lb_RegisterTime.Text = "注册时间：";
            // 
            // lb_Representative
            // 
            this.lb_Representative.AutoSize = true;
            this.lb_Representative.Location = new System.Drawing.Point(446, 128);
            this.lb_Representative.Name = "lb_Representative";
            this.lb_Representative.Size = new System.Drawing.Size(65, 12);
            this.lb_Representative.TabIndex = 8;
            this.lb_Representative.Text = "法人代表：";
            // 
            // lb_BankAccount
            // 
            this.lb_BankAccount.AutoSize = true;
            this.lb_BankAccount.Location = new System.Drawing.Point(446, 162);
            this.lb_BankAccount.Name = "lb_BankAccount";
            this.lb_BankAccount.Size = new System.Drawing.Size(65, 12);
            this.lb_BankAccount.TabIndex = 9;
            this.lb_BankAccount.Text = "银行账号：";
            // 
            // lb_IsListedCompany
            // 
            this.lb_IsListedCompany.AutoSize = true;
            this.lb_IsListedCompany.Location = new System.Drawing.Point(61, 198);
            this.lb_IsListedCompany.Name = "lb_IsListedCompany";
            this.lb_IsListedCompany.Size = new System.Drawing.Size(65, 12);
            this.lb_IsListedCompany.TabIndex = 10;
            this.lb_IsListedCompany.Text = "上市公司：";
            // 
            // lb_StockCode
            // 
            this.lb_StockCode.AutoSize = true;
            this.lb_StockCode.Location = new System.Drawing.Point(445, 198);
            this.lb_StockCode.Name = "lb_StockCode";
            this.lb_StockCode.Size = new System.Drawing.Size(65, 12);
            this.lb_StockCode.TabIndex = 11;
            this.lb_StockCode.Text = "股票代码：";
            // 
            // lb_FixedPhone
            // 
            this.lb_FixedPhone.AutoSize = true;
            this.lb_FixedPhone.Location = new System.Drawing.Point(61, 233);
            this.lb_FixedPhone.Name = "lb_FixedPhone";
            this.lb_FixedPhone.Size = new System.Drawing.Size(65, 12);
            this.lb_FixedPhone.TabIndex = 12;
            this.lb_FixedPhone.Text = "公司固话：";
            // 
            // lb_Fax
            // 
            this.lb_Fax.AutoSize = true;
            this.lb_Fax.Location = new System.Drawing.Point(446, 233);
            this.lb_Fax.Name = "lb_Fax";
            this.lb_Fax.Size = new System.Drawing.Size(65, 12);
            this.lb_Fax.TabIndex = 13;
            this.lb_Fax.Text = "公司传真：";
            // 
            // lb_SupplierNameD
            // 
            this.lb_SupplierNameD.Location = new System.Drawing.Point(133, 22);
            this.lb_SupplierNameD.Name = "lb_SupplierNameD";
            this.lb_SupplierNameD.Size = new System.Drawing.Size(255, 12);
            this.lb_SupplierNameD.TabIndex = 14;
            this.lb_SupplierNameD.Text = "--";
            // 
            // lb_SupplierLoginNameD
            // 
            this.lb_SupplierLoginNameD.Location = new System.Drawing.Point(133, 56);
            this.lb_SupplierLoginNameD.Name = "lb_SupplierLoginNameD";
            this.lb_SupplierLoginNameD.Size = new System.Drawing.Size(255, 12);
            this.lb_SupplierLoginNameD.TabIndex = 28;
            this.lb_SupplierLoginNameD.Text = "--";
            // 
            // panel_SupplierInformationDetails
            // 
            this.panel_SupplierInformationDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_URLD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_FixedPhoneD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_IsListedCompanyD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_NationD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_BankNameD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_RegisterTimeD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierLoginNameD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_PostD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_FaxD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_StockCodeD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_BankAccountD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_RepresentativeD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_DetailAddressD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_EnterpriseD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierIDD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierNameD);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_Post);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_Fax);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_BankName);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_URL);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_FixedPhone);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_StockCode);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_IsListedCompany);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_BankAccount);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_Representative);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_RegisterTime);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_DetailAddress);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_Nation);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_Enterprise);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierLoginName);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierID);
            this.panel_SupplierInformationDetails.Controls.Add(this.lb_SupplierName);
            this.panel_SupplierInformationDetails.Location = new System.Drawing.Point(36, 149);
            this.panel_SupplierInformationDetails.Name = "panel_SupplierInformationDetails";
            this.panel_SupplierInformationDetails.Size = new System.Drawing.Size(889, 325);
            this.panel_SupplierInformationDetails.TabIndex = 2;
            // 
            // lb_URLD
            // 
            this.lb_URLD.Location = new System.Drawing.Point(132, 267);
            this.lb_URLD.Name = "lb_URLD";
            this.lb_URLD.Size = new System.Drawing.Size(255, 12);
            this.lb_URLD.TabIndex = 28;
            this.lb_URLD.Text = "--";
            // 
            // lb_FixedPhoneD
            // 
            this.lb_FixedPhoneD.Location = new System.Drawing.Point(132, 233);
            this.lb_FixedPhoneD.Name = "lb_FixedPhoneD";
            this.lb_FixedPhoneD.Size = new System.Drawing.Size(255, 12);
            this.lb_FixedPhoneD.TabIndex = 28;
            this.lb_FixedPhoneD.Text = "--";
            // 
            // lb_IsListedCompanyD
            // 
            this.lb_IsListedCompanyD.Location = new System.Drawing.Point(133, 198);
            this.lb_IsListedCompanyD.Name = "lb_IsListedCompanyD";
            this.lb_IsListedCompanyD.Size = new System.Drawing.Size(255, 12);
            this.lb_IsListedCompanyD.TabIndex = 28;
            this.lb_IsListedCompanyD.Text = "--";
            // 
            // lb_NationD
            // 
            this.lb_NationD.Location = new System.Drawing.Point(133, 92);
            this.lb_NationD.Name = "lb_NationD";
            this.lb_NationD.Size = new System.Drawing.Size(255, 12);
            this.lb_NationD.TabIndex = 28;
            this.lb_NationD.Text = "--";
            // 
            // lb_BankNameD
            // 
            this.lb_BankNameD.Location = new System.Drawing.Point(132, 162);
            this.lb_BankNameD.Name = "lb_BankNameD";
            this.lb_BankNameD.Size = new System.Drawing.Size(255, 12);
            this.lb_BankNameD.TabIndex = 28;
            this.lb_BankNameD.Text = "--";
            // 
            // lb_RegisterTimeD
            // 
            this.lb_RegisterTimeD.Location = new System.Drawing.Point(133, 128);
            this.lb_RegisterTimeD.Name = "lb_RegisterTimeD";
            this.lb_RegisterTimeD.Size = new System.Drawing.Size(255, 12);
            this.lb_RegisterTimeD.TabIndex = 28;
            this.lb_RegisterTimeD.Text = "--";
            // 
            // lb_PostD
            // 
            this.lb_PostD.Location = new System.Drawing.Point(516, 267);
            this.lb_PostD.Name = "lb_PostD";
            this.lb_PostD.Size = new System.Drawing.Size(255, 12);
            this.lb_PostD.TabIndex = 14;
            this.lb_PostD.Text = "--";
            // 
            // lb_FaxD
            // 
            this.lb_FaxD.Location = new System.Drawing.Point(516, 233);
            this.lb_FaxD.Name = "lb_FaxD";
            this.lb_FaxD.Size = new System.Drawing.Size(255, 12);
            this.lb_FaxD.TabIndex = 14;
            this.lb_FaxD.Text = "--";
            // 
            // lb_StockCodeD
            // 
            this.lb_StockCodeD.Location = new System.Drawing.Point(517, 198);
            this.lb_StockCodeD.Name = "lb_StockCodeD";
            this.lb_StockCodeD.Size = new System.Drawing.Size(255, 12);
            this.lb_StockCodeD.TabIndex = 14;
            this.lb_StockCodeD.Text = "--";
            // 
            // lb_BankAccountD
            // 
            this.lb_BankAccountD.Location = new System.Drawing.Point(516, 162);
            this.lb_BankAccountD.Name = "lb_BankAccountD";
            this.lb_BankAccountD.Size = new System.Drawing.Size(255, 12);
            this.lb_BankAccountD.TabIndex = 14;
            this.lb_BankAccountD.Text = "--";
            // 
            // lb_RepresentativeD
            // 
            this.lb_RepresentativeD.Location = new System.Drawing.Point(516, 128);
            this.lb_RepresentativeD.Name = "lb_RepresentativeD";
            this.lb_RepresentativeD.Size = new System.Drawing.Size(255, 12);
            this.lb_RepresentativeD.TabIndex = 14;
            this.lb_RepresentativeD.Text = "--";
            // 
            // lb_DetailAddressD
            // 
            this.lb_DetailAddressD.Location = new System.Drawing.Point(517, 92);
            this.lb_DetailAddressD.Name = "lb_DetailAddressD";
            this.lb_DetailAddressD.Size = new System.Drawing.Size(255, 12);
            this.lb_DetailAddressD.TabIndex = 14;
            this.lb_DetailAddressD.Text = "--";
            // 
            // lb_EnterpriseD
            // 
            this.lb_EnterpriseD.Location = new System.Drawing.Point(517, 56);
            this.lb_EnterpriseD.Name = "lb_EnterpriseD";
            this.lb_EnterpriseD.Size = new System.Drawing.Size(255, 12);
            this.lb_EnterpriseD.TabIndex = 14;
            this.lb_EnterpriseD.Text = "--";
            // 
            // lb_SupplierIDD
            // 
            this.lb_SupplierIDD.Location = new System.Drawing.Point(517, 22);
            this.lb_SupplierIDD.Name = "lb_SupplierIDD";
            this.lb_SupplierIDD.Size = new System.Drawing.Size(255, 12);
            this.lb_SupplierIDD.TabIndex = 14;
            this.lb_SupplierIDD.Text = "--";
            // 
            // lb_Post
            // 
            this.lb_Post.AutoSize = true;
            this.lb_Post.Location = new System.Drawing.Point(446, 267);
            this.lb_Post.Name = "lb_Post";
            this.lb_Post.Size = new System.Drawing.Size(65, 12);
            this.lb_Post.TabIndex = 13;
            this.lb_Post.Text = "公司邮编：";
            // 
            // lb_BankName
            // 
            this.lb_BankName.AutoSize = true;
            this.lb_BankName.Location = new System.Drawing.Point(62, 162);
            this.lb_BankName.Name = "lb_BankName";
            this.lb_BankName.Size = new System.Drawing.Size(65, 12);
            this.lb_BankName.TabIndex = 12;
            this.lb_BankName.Text = "开户银行：";
            // 
            // lb_URL
            // 
            this.lb_URL.AutoSize = true;
            this.lb_URL.Location = new System.Drawing.Point(62, 267);
            this.lb_URL.Name = "lb_URL";
            this.lb_URL.Size = new System.Drawing.Size(65, 12);
            this.lb_URL.TabIndex = 12;
            this.lb_URL.Text = "公司网址：";
            // 
            // panel_Nav
            // 
            this.panel_Nav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Nav.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel_Nav.Controls.Add(this.lb_Nav);
            this.panel_Nav.Controls.Add(this.picBox_Close);
            this.panel_Nav.Location = new System.Drawing.Point(0, 0);
            this.panel_Nav.Name = "panel_Nav";
            this.panel_Nav.Size = new System.Drawing.Size(932, 35);
            this.panel_Nav.TabIndex = 5;
            this.panel_Nav.DoubleClick += new System.EventHandler(this.panel_Nav_DoubleClick);
            this.panel_Nav.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_Nav_MouseDown);
            this.panel_Nav.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_Nav_MouseMove);
            this.panel_Nav.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_Nav_MouseUp);
            // 
            // lb_Nav
            // 
            this.lb_Nav.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Nav.ForeColor = System.Drawing.Color.Black;
            this.lb_Nav.Location = new System.Drawing.Point(2, 9);
            this.lb_Nav.Name = "lb_Nav";
            this.lb_Nav.Size = new System.Drawing.Size(100, 19);
            this.lb_Nav.TabIndex = 7;
            this.lb_Nav.Text = "查看供应商";
            this.lb_Nav.DoubleClick += new System.EventHandler(this.lb_Nav_DoubleClick);
            // 
            // picBox_Close
            // 
            this.picBox_Close.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.picBox_Close.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox_Close.Location = new System.Drawing.Point(889, -7);
            this.picBox_Close.Name = "picBox_Close";
            this.picBox_Close.Size = new System.Drawing.Size(41, 42);
            this.picBox_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox_Close.TabIndex = 6;
            this.picBox_Close.TabStop = false;
            this.toolTip.SetToolTip(this.picBox_Close, "关闭");
            this.picBox_Close.Click += new System.EventHandler(this.picBox_Close_Click);
            this.picBox_Close.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBox_Close_MouseDown);
            this.picBox_Close.MouseEnter += new System.EventHandler(this.picBox_Close_MouseEnter);
            this.picBox_Close.MouseLeave += new System.EventHandler(this.picBox_Close_MouseLeave);
            // 
            // toolTip
            // 
            this.toolTip.Tag = "";
            // 
            // SupplierDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(931, 650);
            this.Controls.Add(this.panel_Nav);
            this.Controls.Add(this.panel_ContactsInformationDetail);
            this.Controls.Add(this.panel_ContactInformation);
            this.Controls.Add(this.panel_SupplierInformationDetails);
            this.Controls.Add(this.panel_SupplierInformation);
            this.Controls.Add(this.lb_CheckSupplier);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "SupplierDetails";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "查看供应商";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SupplierDetails_Paint);
            this.panel_SupplierInformation.ResumeLayout(false);
            this.panel_ContactInformation.ResumeLayout(false);
            this.panel_ContactsInformationDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg_ContactsInformation)).EndInit();
            this.panel_SupplierInformationDetails.ResumeLayout(false);
            this.panel_SupplierInformationDetails.PerformLayout();
            this.panel_Nav.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lb_CheckSupplier;
        private System.Windows.Forms.Panel panel_SupplierInformation;
        private System.Windows.Forms.Label lb_SupplierInformation;
        private System.Windows.Forms.Panel panel_ContactInformation;
        private System.Windows.Forms.Label lb_ContactsInformation;
        private System.Windows.Forms.Panel panel_ContactsInformationDetail;
        private System.Windows.Forms.DataGridView dvg_ContactsInformation;
        private System.Windows.Forms.Label lb_SupplierName;
        private System.Windows.Forms.Label lb_SupplierID;
        private System.Windows.Forms.Label lb_SupplierLoginName;
        private System.Windows.Forms.Label lb_Enterprise;
        private System.Windows.Forms.Label lb_Nation;
        private System.Windows.Forms.Label lb_DetailAddress;
        private System.Windows.Forms.Label lb_RegisterTime;
        private System.Windows.Forms.Label lb_Representative;
        private System.Windows.Forms.Label lb_BankAccount;
        private System.Windows.Forms.Label lb_IsListedCompany;
        private System.Windows.Forms.Label lb_StockCode;
        private System.Windows.Forms.Label lb_FixedPhone;
        private System.Windows.Forms.Label lb_Fax;
        private System.Windows.Forms.Label lb_SupplierNameD;
        private System.Windows.Forms.Label lb_SupplierLoginNameD;
        private System.Windows.Forms.Panel panel_SupplierInformationDetails;
        private System.Windows.Forms.Label lb_URLD;
        private System.Windows.Forms.Label lb_FixedPhoneD;
        private System.Windows.Forms.Label lb_IsListedCompanyD;
        private System.Windows.Forms.Label lb_NationD;
        private System.Windows.Forms.Label lb_BankNameD;
        private System.Windows.Forms.Label lb_RegisterTimeD;
        private System.Windows.Forms.Label lb_StockCodeD;
        private System.Windows.Forms.Label lb_BankAccountD;
        private System.Windows.Forms.Label lb_RepresentativeD;
        private System.Windows.Forms.Label lb_DetailAddressD;
        private System.Windows.Forms.Label lb_EnterpriseD;
        private System.Windows.Forms.Label lb_SupplierIDD;
        private System.Windows.Forms.Label lb_BankName;
        private System.Windows.Forms.Label lb_URL;
        private System.Windows.Forms.Label lb_FaxD;
        private System.Windows.Forms.Label lb_Post;
        private System.Windows.Forms.Label lb_PostD;
        private System.Windows.Forms.Panel panel_Nav;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Duty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Fax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_MobilePhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_QQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Remark;
        private System.Windows.Forms.PictureBox picBox_Close;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label lb_Nav;
    }
}