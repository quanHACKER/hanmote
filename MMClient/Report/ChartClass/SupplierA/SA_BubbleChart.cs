﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartDirector;
using System.Data;

namespace MMClient.RA.ChartClass
{
    public class SA_BubbleChart
    {
        public void createChart(WinChartViewer viewer, DataTable dt)
        {
            int n = dt.Rows.Count;
            double[] xlables = new double[n];
            double[] ylables = new double[n];
            string[] labels = new string[n];

            double[] z = { 30, 35, 40 };
            string[] label = { "1", "2", "3" };

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        //从数据库中取得横纵轴坐标值
                        xlables[i] = Convert.ToDouble(dt.Rows[i]["Score"]);
                        ylables[i] = Convert.ToDouble(dt.Rows[i]["TotalMoney"]) / 10000;
                        labels[i] = Convert.ToString(dt.Rows[i]["Supplier_Name"]);
                    }
                }
            }

            // Create a XYChart object of size 450 x 400 pixels
            XYChart c = new XYChart(1300, 580);

            // Set the plotarea at (55, 40) and of size 350 x 300 pixels, with a
            // light grey border (0xc0c0c0). Turn on both horizontal and vertical
            // grid lines with light grey color (0xc0c0c0)
            c.setPlotArea(55, 40,1050, 400, 0xffffff, -1, 0xc0c0c0, 0xc0c0c0, -1);

            // Add a title to the chart using 18 pts Times Bold Itatic font.
            c.addTitle("供应商分析", "Times New Roman Bold ", 18);

            // Add a title to the y axis using 12 pts Arial Bold Italic font
            c.yAxis().setTitle("采购金额", "Arial Bold ", 12);

            // Add a title to the x axis using 12 pts Arial Bold Italic font
            c.xAxis().setTitle("供应商评分", "Arial Bold ", 12);

            // Set the axes line width to 3 pixels
            c.xAxis().setWidth(3);
            c.yAxis().setWidth(3);

            // Add the data as a scatter chart layer, using a 15 pixel circle as the
            // symbol
            ScatterLayer layer = c.addScatterLayer(xlables, ylables, "", Chart.CircleSymbol, 35, 0xF0A001, 0xF0A001);

            //动态设置symbol的scale
            layer.setSymbolScale(z);

            // Add labels to the chart as an extra field
            layer.addExtraField(label);

            // Set the data label format to display the extra field
            layer.setDataLabelFormat("{field0}");

            LegendBox legendBox = c.addLegend(600,300,true);
            legendBox.setBackground(0xffffff,0xeeeeee,2);


            // Use 8pts Arial Bold to display the labels
            ChartDirector.TextBox textbox = layer.setDataLabelStyle("Arial Bold", 8);

            // Set the background to purple with a 1 pixel 3D border
            textbox.setBackground(Chart.Transparent, Chart.Transparent, 0);

            // Put the text box 4 pixels to the right of the data point
            textbox.setAlignment(Chart.Center);
            //setPos(means " set position")
            textbox.setPos(4, 0);

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='供应商评分 = {x} 分, 采购金额 = {value} 万元'");

        }
    }
}
