﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ChartDirector;

namespace MMClient.RA.ChartClass
{
    public class PM_MaterialABC
    {
        public void createChart(DataTable dt, WinChartViewer viewer)
        {
            int n = dt.Rows.Count;
            double[] xlable = new double[n];
            double[] ylable = new double[n];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        //从数据库中取得横纵轴坐标值
                        xlable[i] = Convert.ToDouble(dt.Rows[i]["CumQShare"]) * 100;
                        ylable[i] = Convert.ToDouble(dt.Rows[i]["CumShare"]) * 100;
                    }
                }
            }
            //画图表
            XYChart c = new XYChart(630, 400);
            c.setPlotArea(55, 40, 550, 300, 0xADD8E6);
            //设置X、Y标题
            c.xAxis().setTitle("品种数量比例%");
            c.yAxis().setTitle("金额比例%");
            //添加折线
            LineLayer line = c.addLineLayer(ylable);
            line.setXData(xlable);
            //每个数据点用红色小正方块标明
            line.getDataSet(0).setDataSymbol(Chart.SquareSymbol, 6);
            //输出图表
            viewer.Chart = c;
            //每个数据点的提示信息
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='品种数量比例 = {x} %, 金额比例 = {value} %");
        }
    }
}
