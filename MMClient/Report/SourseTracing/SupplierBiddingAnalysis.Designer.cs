﻿namespace MMClient.RA.SourseTracing
{
    partial class SupplierBiddingAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel_query = new System.Windows.Forms.Panel();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tab_Chart = new System.Windows.Forms.TabPage();
            this.panel_BestBid = new System.Windows.Forms.Panel();
            this.chartViewer3 = new ChartDirector.WinChartViewer();
            this.panel_BidAmount = new System.Windows.Forms.Panel();
            this.chartViewer2 = new ChartDirector.WinChartViewer();
            this.panel_Bid = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.tab_Detail = new System.Windows.Forms.TabPage();
            this.dgv_SupplierDependentFactor = new System.Windows.Forms.DataGridView();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Column_SupplierNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_AnnualSales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_InvoicValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_DependencyRatio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_query.SuspendLayout();
            this.panel_Chart.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tab_Chart.SuspendLayout();
            this.panel_BestBid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer3)).BeginInit();
            this.panel_BidAmount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer2)).BeginInit();
            this.panel_Bid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.tab_Detail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDependentFactor)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_query
            // 
            this.panel_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_query.BackColor = System.Drawing.SystemColors.Control;
            this.panel_query.Controls.Add(this.dtp_EndTime);
            this.panel_query.Controls.Add(this.lb_To);
            this.panel_query.Controls.Add(this.dtp_StartTime);
            this.panel_query.Controls.Add(this.lb_Time);
            this.panel_query.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_query.Controls.Add(this.lb_PurchaseGroup);
            this.panel_query.Controls.Add(this.cbb_MaterialGroup);
            this.panel_query.Controls.Add(this.lb_MaterialGroup);
            this.panel_query.Controls.Add(this.btn_Query);
            this.panel_query.Location = new System.Drawing.Point(2, 2);
            this.panel_query.Name = "panel_query";
            this.panel_query.Size = new System.Drawing.Size(1146, 90);
            this.panel_query.TabIndex = 1;
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(229, 35);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 11;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(206, 39);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 10;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(78, 35);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 9;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(40, 39);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 8;
            this.lb_Time.Text = "时间：";
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(488, 36);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbb_PurchaseGroup.TabIndex = 7;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(417, 40);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_PurchaseGroup.TabIndex = 6;
            this.lb_PurchaseGroup.Text = "采购组织：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(684, 36);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(123, 20);
            this.cbb_MaterialGroup.TabIndex = 5;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(625, 40);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 4;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(880, 31);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(91, 32);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // panel_Chart
            // 
            this.panel_Chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Chart.Controls.Add(this.tabControl);
            this.panel_Chart.Location = new System.Drawing.Point(2, 94);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(1146, 424);
            this.panel_Chart.TabIndex = 2;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tab_Chart);
            this.tabControl.Controls.Add(this.tab_Detail);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1146, 424);
            this.tabControl.TabIndex = 0;
            // 
            // tab_Chart
            // 
            this.tab_Chart.Controls.Add(this.panel_BestBid);
            this.tab_Chart.Controls.Add(this.panel_BidAmount);
            this.tab_Chart.Controls.Add(this.panel_Bid);
            this.tab_Chart.Location = new System.Drawing.Point(4, 22);
            this.tab_Chart.Name = "tab_Chart";
            this.tab_Chart.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Chart.Size = new System.Drawing.Size(1138, 398);
            this.tab_Chart.TabIndex = 0;
            this.tab_Chart.Text = "图表";
            this.tab_Chart.UseVisualStyleBackColor = true;
            // 
            // panel_BestBid
            // 
            this.panel_BestBid.BackColor = System.Drawing.SystemColors.Control;
            this.panel_BestBid.Controls.Add(this.chartViewer3);
            this.panel_BestBid.Location = new System.Drawing.Point(773, 6);
            this.panel_BestBid.Name = "panel_BestBid";
            this.panel_BestBid.Size = new System.Drawing.Size(359, 351);
            this.panel_BestBid.TabIndex = 0;
            // 
            // chartViewer3
            // 
            this.chartViewer3.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer3.Location = new System.Drawing.Point(3, 3);
            this.chartViewer3.Name = "chartViewer3";
            this.chartViewer3.Size = new System.Drawing.Size(168, 126);
            this.chartViewer3.TabIndex = 4;
            this.chartViewer3.TabStop = false;
            // 
            // panel_BidAmount
            // 
            this.panel_BidAmount.BackColor = System.Drawing.SystemColors.Control;
            this.panel_BidAmount.Controls.Add(this.chartViewer2);
            this.panel_BidAmount.Location = new System.Drawing.Point(371, 6);
            this.panel_BidAmount.Name = "panel_BidAmount";
            this.panel_BidAmount.Size = new System.Drawing.Size(396, 351);
            this.panel_BidAmount.TabIndex = 0;
            // 
            // chartViewer2
            // 
            this.chartViewer2.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer2.Location = new System.Drawing.Point(3, 3);
            this.chartViewer2.Name = "chartViewer2";
            this.chartViewer2.Size = new System.Drawing.Size(168, 126);
            this.chartViewer2.TabIndex = 4;
            this.chartViewer2.TabStop = false;
            // 
            // panel_Bid
            // 
            this.panel_Bid.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Bid.Controls.Add(this.chartViewer1);
            this.panel_Bid.Location = new System.Drawing.Point(6, 6);
            this.panel_Bid.Name = "panel_Bid";
            this.panel_Bid.Size = new System.Drawing.Size(359, 351);
            this.panel_Bid.TabIndex = 0;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 4;
            this.chartViewer1.TabStop = false;
            // 
            // tab_Detail
            // 
            this.tab_Detail.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Detail.Controls.Add(this.dgv_SupplierDependentFactor);
            this.tab_Detail.Location = new System.Drawing.Point(4, 22);
            this.tab_Detail.Name = "tab_Detail";
            this.tab_Detail.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Detail.Size = new System.Drawing.Size(1138, 398);
            this.tab_Detail.TabIndex = 1;
            this.tab_Detail.Text = "明细";
            // 
            // dgv_SupplierDependentFactor
            // 
            this.dgv_SupplierDependentFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_SupplierDependentFactor.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_SupplierDependentFactor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDependentFactor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierDependentFactor.ColumnHeadersHeight = 30;
            this.dgv_SupplierDependentFactor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierDependentFactor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierName,
            this.Column_SupplierNo,
            this.Column_AnnualSales,
            this.Column_InvoicValue,
            this.Column_DependencyRatio});
            this.dgv_SupplierDependentFactor.EnableHeadersVisualStyles = false;
            this.dgv_SupplierDependentFactor.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.dgv_SupplierDependentFactor.Location = new System.Drawing.Point(1, 3);
            this.dgv_SupplierDependentFactor.MultiSelect = false;
            this.dgv_SupplierDependentFactor.Name = "dgv_SupplierDependentFactor";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDependentFactor.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_SupplierDependentFactor.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierDependentFactor.RowTemplate.Height = 26;
            this.dgv_SupplierDependentFactor.RowTemplate.ReadOnly = true;
            this.dgv_SupplierDependentFactor.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_SupplierDependentFactor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierDependentFactor.Size = new System.Drawing.Size(1137, 378);
            this.dgv_SupplierDependentFactor.TabIndex = 1;
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_SupplierName.DataPropertyName = "Supplier_Name";
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Column_SupplierName.LinkColor = System.Drawing.Color.Blue;
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column_SupplierName.ToolTipText = "ddwd";
            this.Column_SupplierName.VisitedLinkColor = System.Drawing.Color.Purple;
            // 
            // Column_SupplierNo
            // 
            this.Column_SupplierNo.DataPropertyName = "Supplier_ID";
            this.Column_SupplierNo.HeaderText = "供应商编号";
            this.Column_SupplierNo.Name = "Column_SupplierNo";
            this.Column_SupplierNo.ReadOnly = true;
            this.Column_SupplierNo.Width = 200;
            // 
            // Column_AnnualSales
            // 
            this.Column_AnnualSales.DataPropertyName = "Annual_Sales";
            this.Column_AnnualSales.HeaderText = "物料名称";
            this.Column_AnnualSales.Name = "Column_AnnualSales";
            this.Column_AnnualSales.ReadOnly = true;
            this.Column_AnnualSales.Width = 200;
            // 
            // Column_InvoicValue
            // 
            this.Column_InvoicValue.DataPropertyName = "TotalAmount";
            this.Column_InvoicValue.HeaderText = "物料编码";
            this.Column_InvoicValue.Name = "Column_InvoicValue";
            this.Column_InvoicValue.ReadOnly = true;
            this.Column_InvoicValue.Width = 200;
            // 
            // Column_DependencyRatio
            // 
            this.Column_DependencyRatio.DataPropertyName = "Rate";
            dataGridViewCellStyle2.Format = "0.00%";
            dataGridViewCellStyle2.NullValue = null;
            this.Column_DependencyRatio.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_DependencyRatio.HeaderText = "出价";
            this.Column_DependencyRatio.Name = "Column_DependencyRatio";
            this.Column_DependencyRatio.ReadOnly = true;
            this.Column_DependencyRatio.Width = 190;
            // 
            // SupplierBiddingAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 520);
            this.Controls.Add(this.panel_Chart);
            this.Controls.Add(this.panel_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierBiddingAnalysis";
            this.Text = "供应商竞价分析";
            this.panel_query.ResumeLayout(false);
            this.panel_query.PerformLayout();
            this.panel_Chart.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tab_Chart.ResumeLayout(false);
            this.panel_BestBid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer3)).EndInit();
            this.panel_BidAmount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer2)).EndInit();
            this.panel_Bid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.tab_Detail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDependentFactor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_query;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.Panel panel_Chart;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tab_Chart;
        private System.Windows.Forms.Panel panel_BestBid;
        private System.Windows.Forms.Panel panel_BidAmount;
        private System.Windows.Forms.Panel panel_Bid;
        private System.Windows.Forms.TabPage tab_Detail;
        private System.Windows.Forms.DataGridView dgv_SupplierDependentFactor;
        private System.Windows.Forms.DataGridViewLinkColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_AnnualSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_InvoicValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_DependencyRatio;
        private ChartDirector.WinChartViewer chartViewer3;
        private ChartDirector.WinChartViewer chartViewer2;
        private ChartDirector.WinChartViewer chartViewer1;

    }
}