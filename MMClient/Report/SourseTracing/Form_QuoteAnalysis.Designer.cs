﻿namespace MMClient.RA.SourseTracing
{
    partial class Form_QuoteAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gb_query = new System.Windows.Forms.GroupBox();
            this.lb_Type = new System.Windows.Forms.Label();
            this.cb_Type = new System.Windows.Forms.ComboBox();
            this.btn_query = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_endTime = new System.Windows.Forms.Label();
            this.lb_stratTime = new System.Windows.Forms.Label();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.lb_MaterialId = new System.Windows.Forms.Label();
            this.txt_MaterialID = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.gb_query.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_query
            // 
            this.gb_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_query.Controls.Add(this.lb_Type);
            this.gb_query.Controls.Add(this.cb_Type);
            this.gb_query.Controls.Add(this.btn_query);
            this.gb_query.Controls.Add(this.groupBox1);
            this.gb_query.Controls.Add(this.lb_MaterialId);
            this.gb_query.Controls.Add(this.txt_MaterialID);
            this.gb_query.Location = new System.Drawing.Point(12, 12);
            this.gb_query.Name = "gb_query";
            this.gb_query.Size = new System.Drawing.Size(1111, 105);
            this.gb_query.TabIndex = 0;
            this.gb_query.TabStop = false;
            this.gb_query.Text = "查询条件";
            // 
            // lb_Type
            // 
            this.lb_Type.AutoSize = true;
            this.lb_Type.Location = new System.Drawing.Point(517, 51);
            this.lb_Type.Name = "lb_Type";
            this.lb_Type.Size = new System.Drawing.Size(65, 12);
            this.lb_Type.TabIndex = 8;
            this.lb_Type.Text = "查看类型：";
            // 
            // cb_Type
            // 
            this.cb_Type.FormattingEnabled = true;
            this.cb_Type.Items.AddRange(new object[] {
            "供应商报价分布",
            "按最低报价",
            "按最高报价",
            "按信誉度（得分最高）"});
            this.cb_Type.Location = new System.Drawing.Point(588, 47);
            this.cb_Type.Name = "cb_Type";
            this.cb_Type.Size = new System.Drawing.Size(167, 20);
            this.cb_Type.TabIndex = 7;
            // 
            // btn_query
            // 
            this.btn_query.Location = new System.Drawing.Point(828, 35);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(114, 38);
            this.btn_query.TabIndex = 6;
            this.btn_query.Text = "生成报表";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.lb_endTime);
            this.groupBox1.Controls.Add(this.lb_stratTime);
            this.groupBox1.Controls.Add(this.dtp_endTime);
            this.groupBox1.Controls.Add(this.dtp_startTime);
            this.groupBox1.Location = new System.Drawing.Point(238, 11);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(263, 88);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "时间范围";
            // 
            // lb_endTime
            // 
            this.lb_endTime.AutoSize = true;
            this.lb_endTime.Location = new System.Drawing.Point(34, 58);
            this.lb_endTime.Name = "lb_endTime";
            this.lb_endTime.Size = new System.Drawing.Size(65, 12);
            this.lb_endTime.TabIndex = 3;
            this.lb_endTime.Text = "截止日期：";
            // 
            // lb_stratTime
            // 
            this.lb_stratTime.AutoSize = true;
            this.lb_stratTime.Location = new System.Drawing.Point(34, 24);
            this.lb_stratTime.Name = "lb_stratTime";
            this.lb_stratTime.Size = new System.Drawing.Size(65, 12);
            this.lb_stratTime.TabIndex = 2;
            this.lb_stratTime.Text = "起始时间：";
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.Checked = false;
            this.dtp_endTime.Location = new System.Drawing.Point(105, 52);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.ShowCheckBox = true;
            this.dtp_endTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_endTime.TabIndex = 1;
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.Checked = false;
            this.dtp_startTime.Location = new System.Drawing.Point(105, 20);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.ShowCheckBox = true;
            this.dtp_startTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_startTime.TabIndex = 0;
            // 
            // lb_MaterialId
            // 
            this.lb_MaterialId.AutoSize = true;
            this.lb_MaterialId.Location = new System.Drawing.Point(44, 51);
            this.lb_MaterialId.Name = "lb_MaterialId";
            this.lb_MaterialId.Size = new System.Drawing.Size(65, 12);
            this.lb_MaterialId.TabIndex = 1;
            this.lb_MaterialId.Text = "物料编码：";
            // 
            // txt_MaterialID
            // 
            this.txt_MaterialID.Location = new System.Drawing.Point(115, 47);
            this.txt_MaterialID.Name = "txt_MaterialID";
            this.txt_MaterialID.Size = new System.Drawing.Size(100, 21);
            this.txt_MaterialID.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.chartViewer1);
            this.panel1.Location = new System.Drawing.Point(12, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1111, 331);
            this.panel1.TabIndex = 1;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 2;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // Form_QuoteAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 466);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gb_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Form_QuoteAnalysis";
            this.Text = "物料报价分析";
            this.Load += new System.EventHandler(this.Form_QuoteAnalysis_Load);
            this.gb_query.ResumeLayout(false);
            this.gb_query.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_query;
        private System.Windows.Forms.Label lb_MaterialId;
        private System.Windows.Forms.TextBox txt_MaterialID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lb_endTime;
        private System.Windows.Forms.Label lb_stratTime;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.Panel panel1;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.Label lb_Type;
        private System.Windows.Forms.ComboBox cb_Type;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}