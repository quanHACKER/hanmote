﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using Lib.SqlServerDAL;

namespace MMClient.RA.SourseTracing
{
    public partial class Form_QuoteAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Form_QuoteAnalysis()
        {
            InitializeComponent();
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = this.chartViewer1;
            this.createChart(viewer, "1");
        }

        //开始画图表
        //Note: the argument img is unused because this demo only has 1 chart.（注意:参数img是未使用的,因为这个演示只有1图）
        public void createChart(WinChartViewer viewer, string img)
        {
            //如果没有输入物料编码则弹出提示信息
            if (this.txt_MaterialID.Text.Trim() == "")
            {
                MessageBox.Show("请输入物料编码!", "软件提示");
                return;//弹出提示信息后则不在执行该方法的后续代码
            }
            #region 最低报价分析
            if (this.cb_Type.SelectedIndex == 1)   //最低报价分析
            {
                //查找数据库
                string sql_Text = @"select top 8 Supplier_ID,Price from [Quote] where Material_ID = '" + this.txt_MaterialID.Text + "'";
                if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
                {
                    MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                    return;
                }
                if (this.dtp_startTime.ShowCheckBox == true)
                {
                    if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                    {
                        sql_Text += @" and Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                if (this.dtp_endTime.ShowCheckBox == true)
                {
                    if (this.dtp_endTime.Checked == true)
                    {
                        sql_Text += @" and Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                sql_Text += @" order by Price";   //将查询结果按时间排序
                DataTable data_Table = DBHelper.ExecuteQueryDT(sql_Text);
                int n = data_Table.Rows.Count;
                double[] data = new double[n];
                string[] labels = new string[n];
                for (int i = 0; i < data_Table.Rows.Count; i++)
                {
                    data[i] = Convert.ToDouble(data_Table.Rows[i]["Price"]);
                    labels[i] = Convert.ToString(data_Table.Rows[i]["Supplier_ID"]);
                }
                double[] data2 = { 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00 };
                // Create a XYChart object of size 500 x 320 pixels, with a pale purpule
                // (0xffccff) background, a black border, and 1 pixel 3D border effect(一个3D的边界效果).
                //XYChart c = new XYChart(500, 320, 0xffccff, 0x000000, 1);
                XYChart c = new XYChart(1150, 580);

                // Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white
                // background. Turn on both horizontal and vertical grid lines with light
                // grey color (0xc0c0c0)
                c.setPlotArea(55, 45, 1020, 450, 0xffffff, -1, -1, 0xc0c0c0, -1);

                // Add a legend box at (55, 25) (top of the chart) with horizontal
                // layout. Use 8 pts Arial font. Set the background and border color to
                // Transparent.(增加一个水平说明区域)
                c.addLegend(55, 22, false, "", 8).setBackground(Chart.Transparent);

                // Add a title box to the chart using 13 pts Times Bold Italic font. The
                // text is white (0xffffff) on a purple (0x800080) background, with a 1
                // pixel 3D border.
                c.addTitle("物料报价分析", "Times New Roman Bold Italic", 13,
                    0xffffff).setBackground(0x800080, -1, 1);

                // Add a title to the y axis
                c.yAxis().setTitle("单价（元）");
                c.xAxis().setTitle("供应商编码");
                // Set the labels on the x axis. Rotate the font by 90 degrees.
                c.xAxis().setLabels(labels);

                // Add a line layer to the chart
                LineLayer lineLayer1 = c.addLineLayer();

                // Add the data to the line layer using light brown color (0xcc9966) with
                // a 7 pixel square symbol
                lineLayer1.addDataSet(data, 0xcc9966, "供应商报价").setDataSymbol(
                    Chart.SquareSymbol, 10);

                // Set the line width to 2 pixels
                lineLayer1.setLineWidth(2);

                // tool tip for the line layer
                lineLayer1.setHTMLImageMap("", "", "title='{xLabel}: {value} 元'");

                LineLayer lineLayer2 = c.addLineLayer();
                lineLayer2.addDataSet(data2, 0x008000, "平均报价").setDataSymbol(Chart.GlassSphere2Shape, 6);


                /*
                // Add a trend line layer using the same data with a dark green
                // (0x008000) color. Set the line width to 2 pixels
                TrendLayer trendLayer = c.addTrendLayer(data, 0x008000, "平均报价");
                trendLayer.setLineWidth(2);

                // tool tip for the trend layer
                trendLayer.setHTMLImageMap("", "",
                    "title='Change rate: {slope|2} 元'");
                */
                // Output the chart
                viewer.Chart = c;

                // include tool tip for the chart
                viewer.ImageMap = c.getHTMLImageMap("可点击");
            }
            #endregion
            #region 供应商报价分布
            else if (this.cb_Type.SelectedIndex == 0)   //供应商报价分布
            {
                //查找数据库
                string sql_Text = @"select  Supplier_ID,Price from [Quote] where Material_ID = '" + this.txt_MaterialID.Text + "'";
                if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
                {
                    MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                    return;
                }
                if (this.dtp_startTime.ShowCheckBox == true)
                {
                    if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                    {
                        sql_Text += @" and Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                if (this.dtp_endTime.ShowCheckBox == true)
                {
                    if (this.dtp_endTime.Checked == true)
                    {
                        sql_Text += @" and Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                //sql_Text += @" order by Price";   //将查询结果按时间排序
                DataTable data_Table = DBHelper.ExecuteQueryDT(sql_Text);
                int n = data_Table.Rows.Count;
                double[] data = new double[n];
                string[] labels = new string[n];
                double[] data2=new double[n];
                for (int i = 0; i < data_Table.Rows.Count; i++)
                {
                    data[i] = Convert.ToDouble(data_Table.Rows[i]["Price"]);
                    labels[i] = Convert.ToString(data_Table.Rows[i]["Supplier_ID"]);
                    data2[i] = 13.00;
                }
                 
                // Create a XYChart object of size 500 x 320 pixels, with a pale purpule
                // (0xffccff) background, a black border, and 1 pixel 3D border effect(一个3D的边界效果).
                //XYChart c = new XYChart(500, 320, 0xffccff, 0x000000, 1);
                XYChart c = new XYChart(1150, 580);

                // Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white
                // background. Turn on both horizontal and vertical grid lines with light
                // grey color (0xc0c0c0)
                c.setPlotArea(55, 45, 1020, 450, 0xffffff, -1, -1, 0xc0c0c0, -1);

                // Add a legend box at (55, 25) (top of the chart) with horizontal
                // layout. Use 8 pts Arial font. Set the background and border color to
                // Transparent.(增加一个水平说明区域)
                c.addLegend(55, 22, false, "", 8).setBackground(Chart.Transparent);

                // Add a title box to the chart using 13 pts Times Bold Italic font. The
                // text is white (0xffffff) on a purple (0x800080) background, with a 1
                // pixel 3D border.
                c.addTitle("物料报价分析", "Times New Roman Bold Italic", 13,
                    0xffffff).setBackground(0x800080, -1, 1);

                // Add a title to the y axis
                c.yAxis().setTitle("单价（元）");
                c.xAxis().setTitle("供应商编码");
                // Set the labels on the x axis. Rotate the font by 90 degrees.
                c.xAxis().setLabels(labels);

                // Add a line layer to the chart
                LineLayer lineLayer1 = c.addLineLayer();

                // Add the data to the line layer using light brown color (0xcc9966) with
                // a 7 pixel square symbol
                lineLayer1.addDataSet(data, 0x33FF33, "供应商报价").setDataSymbol(
                    Chart.SquareSymbol, 10);

                // Set the line width to 2 pixels
                lineLayer1.setLineWidth(2);

                // tool tip for the line layer
                lineLayer1.setHTMLImageMap("", "", "title='{xLabel}: {value} 元'");

                LineLayer lineLayer2 = c.addLineLayer();
                lineLayer2.addDataSet(data2, 0xFF0033, "平均报价").setDataSymbol(Chart.GlassSphere2Shape, 6);


                /*
                // Add a trend line layer using the same data with a dark green
                // (0x008000) color. Set the line width to 2 pixels
                TrendLayer trendLayer = c.addTrendLayer(data, 0x008000, "平均报价");
                trendLayer.setLineWidth(2);

                // tool tip for the trend layer
                trendLayer.setHTMLImageMap("", "",
                    "title='Change rate: {slope|2} 元'");
                */
                // Output the chart
                viewer.Chart = c;

                // include tool tip for the chart
                viewer.ImageMap = c.getHTMLImageMap("可点击");
            }
            #endregion
            #region 最高报价分析
            else if (this.cb_Type.SelectedIndex == 2)  //最高报价分析
            {
                //查找数据库
                string sql_Text = @"select top 8 Supplier_ID,Price from [Quote] where Material_ID = '" + this.txt_MaterialID.Text + "'";
                if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
                {
                    MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                    return;
                }
                if (this.dtp_startTime.ShowCheckBox == true)
                {
                    if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                    {
                        sql_Text += @" and Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                if (this.dtp_endTime.ShowCheckBox == true)
                {
                    if (this.dtp_endTime.Checked == true)
                    {
                        sql_Text += @" and Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                sql_Text += @" order by Price DESC";   //将查询结果按时间排序
                DataTable data_Table = DBHelper.ExecuteQueryDT(sql_Text);
                int n = data_Table.Rows.Count;
                double[] data = new double[n];
                string[] labels = new string[n];
                for (int i = 0; i < data_Table.Rows.Count; i++)
                {
                    data[i] = Convert.ToDouble(data_Table.Rows[i]["Price"]);
                    labels[i] = Convert.ToString(data_Table.Rows[i]["Supplier_ID"]);
                }
                double[] data2 = { 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00 };
                // Create a XYChart object of size 500 x 320 pixels, with a pale purpule
                // (0xffccff) background, a black border, and 1 pixel 3D border effect(一个3D的边界效果).
                //XYChart c = new XYChart(500, 320, 0xffccff, 0x000000, 1);
                XYChart c = new XYChart(1150, 580);

                // Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white
                // background. Turn on both horizontal and vertical grid lines with light
                // grey color (0xc0c0c0)
                c.setPlotArea(55, 45, 1020, 450, 0xffffff, -1, -1, 0xc0c0c0, -1);

                // Add a legend box at (55, 25) (top of the chart) with horizontal
                // layout. Use 8 pts Arial font. Set the background and border color to
                // Transparent.(增加一个水平说明区域)
                c.addLegend(55, 22, false, "", 8).setBackground(Chart.Transparent);

                // Add a title box to the chart using 13 pts Times Bold Italic font. The
                // text is white (0xffffff) on a purple (0x800080) background, with a 1
                // pixel 3D border.
                c.addTitle("物料报价分析", "Times New Roman Bold Italic", 13,
                    0xffffff).setBackground(0x800080, -1, 1);

                // Add a title to the y axis
                c.yAxis().setTitle("单价（元）");
                c.xAxis().setTitle("供应商编码");
                // Set the labels on the x axis. Rotate the font by 90 degrees.
                c.xAxis().setLabels(labels);

                // Add a line layer to the chart
                LineLayer lineLayer1 = c.addLineLayer();

                // Add the data to the line layer using light brown color (0xcc9966) with
                // a 7 pixel square symbol
                lineLayer1.addDataSet(data, 0xcc9966, "供应商报价").setDataSymbol(
                    Chart.SquareSymbol, 10);

                // Set the line width to 2 pixels
                lineLayer1.setLineWidth(2);

                // tool tip for the line layer
                lineLayer1.setHTMLImageMap("", "", "title='{xLabel}: {value} 元'");

                LineLayer lineLayer2 = c.addLineLayer();
                lineLayer2.addDataSet(data2, 0x008000, "平均报价").setDataSymbol(Chart.GlassSphere2Shape, 6);


                /*
                // Add a trend line layer using the same data with a dark green
                // (0x008000) color. Set the line width to 2 pixels
                TrendLayer trendLayer = c.addTrendLayer(data, 0x008000, "平均报价");
                trendLayer.setLineWidth(2);

                // tool tip for the trend layer
                trendLayer.setHTMLImageMap("", "",
                    "title='Change rate: {slope|2} 元'");
                */
                // Output the chart
                viewer.Chart = c;

                // include tool tip for the chart
                viewer.ImageMap = c.getHTMLImageMap("可点击");
            }
            #endregion
            #region 信誉度排名
            else
            {
                //查找数据库
                string sql_Text = @"select top 8 [Quote].Supplier_ID,[Quote].Price from [Quote],[Supplier_Performance Analysis] where  [Quote].Material_ID=[Supplier_Performance Analysis].Material_ID and [Quote].Supplier_ID=[Supplier_Performance Analysis].Supplier_ID and [Quote].Material_ID = '" + this.txt_MaterialID.Text + "'";
                if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
                {
                    MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                    return;
                }
                if (this.dtp_startTime.ShowCheckBox == true)
                {
                    if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                    {
                        sql_Text += @" and [Quote].Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                if (this.dtp_endTime.ShowCheckBox == true)
                {
                    if (this.dtp_endTime.Checked == true)
                    {
                        sql_Text += @" and [Quote].Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                    }
                }
                sql_Text += @" order by [Supplier_Performance Analysis].Score DESC";   //将查询结果按得分降序
                DataTable data_Table = DBHelper.ExecuteQueryDT(sql_Text);
                int n = data_Table.Rows.Count;
                double[] data = new double[n];
                string[] labels = new string[n];
                for (int i = 0; i < data_Table.Rows.Count; i++)
                {
                    data[i] = Convert.ToDouble(data_Table.Rows[i]["Price"]);
                    labels[i] = Convert.ToString(data_Table.Rows[i]["Supplier_ID"]);
                }
                double[] data2 = { 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00, 13.00 };
                // Create a XYChart object of size 500 x 320 pixels, with a pale purpule
                // (0xffccff) background, a black border, and 1 pixel 3D border effect(一个3D的边界效果).
                //XYChart c = new XYChart(500, 320, 0xffccff, 0x000000, 1);
                XYChart c = new XYChart(1150, 580);

                // Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white
                // background. Turn on both horizontal and vertical grid lines with light
                // grey color (0xc0c0c0)
                c.setPlotArea(55, 45, 1020, 450, 0xffffff, -1, -1, 0xc0c0c0, -1);

                // Add a legend box at (55, 25) (top of the chart) with horizontal
                // layout. Use 8 pts Arial font. Set the background and border color to
                // Transparent.(增加一个水平说明区域)
                c.addLegend(55, 22, false, "", 8).setBackground(Chart.Transparent);

                // Add a title box to the chart using 13 pts Times Bold Italic font. The
                // text is white (0xffffff) on a purple (0x800080) background, with a 1
                // pixel 3D border.
                c.addTitle("物料报价分析", "Times New Roman Bold Italic", 13,
                    0xffffff).setBackground(0x800080, -1, 1);

                // Add a title to the y axis
                c.yAxis().setTitle("单价（元）");
                c.xAxis().setTitle("供应商编码");
                // Set the labels on the x axis. Rotate the font by 90 degrees.
                c.xAxis().setLabels(labels);

                // Add a line layer to the chart
                LineLayer lineLayer1 = c.addLineLayer();

                // Add the data to the line layer using light brown color (0xcc9966) with
                // a 7 pixel square symbol
                lineLayer1.addDataSet(data, 0xcc9966, "供应商报价").setDataSymbol(
                    Chart.SquareSymbol, 10);

                // Set the line width to 2 pixels
                lineLayer1.setLineWidth(2);

                // tool tip for the line layer
                lineLayer1.setHTMLImageMap("", "", "title='{xLabel}: {value} 元'");

                LineLayer lineLayer2 = c.addLineLayer();
                lineLayer2.addDataSet(data2, 0x008000, "平均报价").setDataSymbol(Chart.GlassSphere2Shape, 6);

                // Output the chart
                viewer.Chart = c;

                // include tool tip for the chart
                viewer.ImageMap = c.getHTMLImageMap("可点击");
            }
            #endregion
        }

        //热点事件
        private void chartViewer1_ClickHotSpot(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender,e);
        }

        //该窗体加载时触发
        private void Form_QuoteAnalysis_Load(object sender, EventArgs e)
        {
            //默认选中下拉列表的第一个
            this.cb_Type.SelectedIndex = 0;  
        }
    }
}
