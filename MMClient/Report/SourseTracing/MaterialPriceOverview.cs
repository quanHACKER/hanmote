﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Common.CommonUtils;

namespace MMClient.RA.SourseTracing
{
    public partial class MaterialPriceOverview : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        //只读型常量
        private readonly string str_txt_ChooseSupplier = "供应商编码/供应商名称";
        MaterialPriceBLL mpBLL = new MaterialPriceBLL();

        public MaterialPriceOverview()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dvg_MaterialPriceOverview.TopLeftHeaderCell.Value = "行号";

            //当窗体加载时，让其焦点在标签上
            this.lb_ChooseSupplier.Select();
            this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;

            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;
        }

        /// <summary>
        /// 点击生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_genarateReport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = mpBLL.GetMaterialDetailPrice("123", this.txt_ChooseSupplier.Text.Trim());
                this.DataGridShow(dt);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 将数据显示在datagridview中
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dvg_MaterialPriceOverview.DataSource = dt;
                }
            }
        }

        /// <summary>
        /// 双击单元格任意位置时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_MaterialPrice_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            new MaterialPriceAnalysis().ShowDialog();
        }

        /// <summary>
        /// 给dgv添加行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_MaterialPrice_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单位格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_MaterialPrice_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dvg_MaterialPriceOverview.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dvg_MaterialPriceOverview.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单位格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_MaterialPrice_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dvg_MaterialPriceOverview.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dvg_MaterialPriceOverview.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标进入textbox触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Enter(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim().Equals(str_txt_ChooseSupplier))
            {
                //将textbox内容置空
                this.txt_ChooseSupplier.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.Black;
            }
        }

        /// <summary>
        /// 当鼠标离开textbox时触发该时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Leave(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim() == "")
            {
                this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        /// <summary>
        /// 点击单元格内容时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dvg_MaterialPrice_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //点击供应商名称时触发
            if (e.ColumnIndex == 7)
            {
                new SupplierDetails(this.dvg_MaterialPriceOverview.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),"").ShowDialog();
            }

            //点击供应商ID时触发
            if (e.ColumnIndex == 6)
            {
                new SupplierDetails("",this.dvg_MaterialPriceOverview.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()).ShowDialog();
            }
        }
    }
}
