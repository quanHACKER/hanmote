﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.RA.ChartClass;

namespace MMClient.RA.SourseTracing
{
    public partial class MaterialPriceAnalysis : Form
    {
        ST_CurrentYearPriceBarChart st_cyp = new ST_CurrentYearPriceBarChart();
        ST_PastFourYearsPriceBarChart st_pfyp = new ST_PastFourYearsPriceBarChart();
        //SP_SupplierComparativePolarAreaChart sp_scpac = new SP_SupplierComparativePolarAreaChart();

        public MaterialPriceAnalysis()
        {
            InitializeComponent();

            st_cyp.createChart(this.wcv_CurrentYearPrice);
            //sp_scpac.createChart(this.wcv_CurrentYearPrice);
            st_pfyp.createChart(this.wcv_PastFourYearsPrice);
        }
    }
}
