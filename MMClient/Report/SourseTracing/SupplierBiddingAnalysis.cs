﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.RA.ChartClass;

namespace MMClient.RA.SourseTracing
{
    public partial class SupplierBiddingAnalysis :WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SupplierBiddingAnalysis()
        {
            InitializeComponent();
            this.dgv_SupplierDependentFactor.TopLeftHeaderCell.Value = "行号";
        }

        private void btn_Query_Click(object sender, EventArgs e)
        {
            ST_PastFourYearsPriceBarChart1 st_pfyp1 = new ST_PastFourYearsPriceBarChart1();
            st_pfyp1.createChart(this.chartViewer1);
            st_pfyp1.createChart(this.chartViewer2);
            st_pfyp1.createChart(this.chartViewer3);
        }
    }
}
