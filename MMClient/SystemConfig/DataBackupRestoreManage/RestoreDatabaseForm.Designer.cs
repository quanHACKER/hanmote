﻿namespace MMClient.SystemConfig.DataBackupRestoreManage
{
    partial class RestoreDatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreDatabaseForm));
            this.label5 = new System.Windows.Forms.Label();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.restoreBtn = new System.Windows.Forms.Button();
            this.backupHistory = new System.Windows.Forms.DataGridView();
            this.iD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.databaseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backupTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Backup_Operator_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Backup_Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serverName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.databaseList = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.createLinkBtn = new System.Windows.Forms.Button();
            this.loginPassword = new System.Windows.Forms.TextBox();
            this.loginId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sourceBackupPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.backupHistory)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(28, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "备份记录：";
            // 
            // refreshBtn
            // 
            this.refreshBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.refreshBtn.Location = new System.Drawing.Point(348, 414);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(75, 25);
            this.refreshBtn.TabIndex = 7;
            this.refreshBtn.Text = "刷 新";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.exitBtn.Location = new System.Drawing.Point(310, 188);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 6;
            this.exitBtn.Text = "退出";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // restoreBtn
            // 
            this.restoreBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.restoreBtn.Location = new System.Drawing.Point(220, 188);
            this.restoreBtn.Name = "restoreBtn";
            this.restoreBtn.Size = new System.Drawing.Size(75, 25);
            this.restoreBtn.TabIndex = 5;
            this.restoreBtn.Text = "恢 复";
            this.restoreBtn.UseVisualStyleBackColor = true;
            this.restoreBtn.Click += new System.EventHandler(this.restoreBtn_Click);
            // 
            // backupHistory
            // 
            this.backupHistory.AllowUserToAddRows = false;
            this.backupHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.backupHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.backupHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.backupHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.backupHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iD,
            this.databaseName,
            this.backupTime,
            this.username,
            this.Backup_Operator_Name,
            this.Backup_Path});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.backupHistory.DefaultCellStyle = dataGridViewCellStyle2;
            this.backupHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.backupHistory.Location = new System.Drawing.Point(25, 267);
            this.backupHistory.MultiSelect = false;
            this.backupHistory.Name = "backupHistory";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.backupHistory.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.backupHistory.RowHeadersVisible = false;
            this.backupHistory.RowTemplate.Height = 23;
            this.backupHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.backupHistory.Size = new System.Drawing.Size(399, 140);
            this.backupHistory.TabIndex = 32;
            this.backupHistory.SelectionChanged += new System.EventHandler(this.backupHistory_SelectionChanged);
            // 
            // iD
            // 
            this.iD.DataPropertyName = "ID";
            this.iD.HeaderText = "备份记录ID";
            this.iD.Name = "iD";
            this.iD.Visible = false;
            this.iD.Width = 75;
            // 
            // databaseName
            // 
            this.databaseName.DataPropertyName = "DB_Name";
            this.databaseName.HeaderText = "数据库名";
            this.databaseName.Name = "databaseName";
            this.databaseName.Width = 81;
            // 
            // backupTime
            // 
            this.backupTime.DataPropertyName = "Backup_Time";
            this.backupTime.HeaderText = "备份时间";
            this.backupTime.Name = "backupTime";
            this.backupTime.Width = 81;
            // 
            // username
            // 
            this.username.DataPropertyName = "Backup_Operator";
            this.username.HeaderText = "操作人编号";
            this.username.Name = "username";
            this.username.Width = 93;
            // 
            // Backup_Operator_Name
            // 
            this.Backup_Operator_Name.DataPropertyName = "Backup_Operator_Name";
            this.Backup_Operator_Name.HeaderText = "操作人姓名";
            this.Backup_Operator_Name.Name = "Backup_Operator_Name";
            this.Backup_Operator_Name.Width = 93;
            // 
            // Backup_Path
            // 
            this.Backup_Path.DataPropertyName = "Backup_Path";
            this.Backup_Path.HeaderText = "备份路径";
            this.Backup_Path.Name = "Backup_Path";
            this.Backup_Path.Visible = false;
            this.Backup_Path.Width = 81;
            // 
            // serverName
            // 
            this.serverName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.serverName.Location = new System.Drawing.Point(114, 28);
            this.serverName.Name = "serverName";
            this.serverName.Size = new System.Drawing.Size(189, 23);
            this.serverName.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(16, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 29;
            this.label4.Text = "服务器名称(S)：";
            // 
            // databaseList
            // 
            this.databaseList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.databaseList.FormattingEnabled = true;
            this.databaseList.Location = new System.Drawing.Point(114, 121);
            this.databaseList.Name = "databaseList";
            this.databaseList.Size = new System.Drawing.Size(271, 25);
            this.databaseList.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(17, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 28;
            this.label3.Text = "数据库(D)：";
            // 
            // createLinkBtn
            // 
            this.createLinkBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.createLinkBtn.Location = new System.Drawing.Point(310, 89);
            this.createLinkBtn.Name = "createLinkBtn";
            this.createLinkBtn.Size = new System.Drawing.Size(75, 25);
            this.createLinkBtn.TabIndex = 3;
            this.createLinkBtn.Text = "建立连接";
            this.createLinkBtn.UseVisualStyleBackColor = true;
            this.createLinkBtn.Click += new System.EventHandler(this.createLinkBtn_Click);
            // 
            // loginPassword
            // 
            this.loginPassword.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.loginPassword.Location = new System.Drawing.Point(114, 90);
            this.loginPassword.Name = "loginPassword";
            this.loginPassword.Size = new System.Drawing.Size(189, 23);
            this.loginPassword.TabIndex = 2;
            this.loginPassword.UseSystemPasswordChar = true;
            // 
            // loginId
            // 
            this.loginId.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.loginId.Location = new System.Drawing.Point(114, 59);
            this.loginId.Name = "loginId";
            this.loginId.Size = new System.Drawing.Size(189, 23);
            this.loginId.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(17, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "密   码(P)：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(17, 62);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 23;
            this.label1.Text = "登录名(L)：";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "数据库名";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 81;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "备份时间";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 81;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "操作人";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 69;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.sourceBackupPath);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.loginPassword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.exitBtn);
            this.groupBox1.Controls.Add(this.loginId);
            this.groupBox1.Controls.Add(this.restoreBtn);
            this.groupBox1.Controls.Add(this.createLinkBtn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.serverName);
            this.groupBox1.Controls.Add(this.databaseList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(25, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(400, 223);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "恢复";
            // 
            // sourceBackupPath
            // 
            this.sourceBackupPath.Location = new System.Drawing.Point(114, 154);
            this.sourceBackupPath.Name = "sourceBackupPath";
            this.sourceBackupPath.ReadOnly = true;
            this.sourceBackupPath.Size = new System.Drawing.Size(271, 23);
            this.sourceBackupPath.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(17, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 31;
            this.label6.Text = "源备份(D)：";
            // 
            // RestoreDatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 454);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.backupHistory);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(467, 492);
            this.MinimumSize = new System.Drawing.Size(467, 492);
            this.Name = "RestoreDatabaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "数据库恢复";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RestoreDatabaseForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.backupHistory)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button restoreBtn;
        private System.Windows.Forms.DataGridView backupHistory;
        private System.Windows.Forms.TextBox serverName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox databaseList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button createLinkBtn;
        private System.Windows.Forms.TextBox loginPassword;
        private System.Windows.Forms.TextBox loginId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox sourceBackupPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn iD;
        private System.Windows.Forms.DataGridViewTextBoxColumn databaseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn backupTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Backup_Operator_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Backup_Path;
    }
}