﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.DataBackupRestoreManage;
using Lib.Model.SystemConfig;

using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace MMClient.SystemConfig.DataBackupRestoreManage
{
    public partial class RestoreDatabaseForm : Form
    {
        private Server server;                                  //数据库服务器对象
        private String selectedDatabase;                        //选择的要恢复的数据库
        private String sourceFilePath;                          //源备份在服务器上绝对路径

        private ProgressForm progressForm = null;               //进度条窗口
        private delegate void closeProressBarFormDelegate();    //关闭进度条窗口委托

        public RestoreDatabaseForm()
        {
            InitializeComponent();
            initServerName();
        }

        /// <summary>
        /// 初始化服务器名
        /// </summary>
        public void initServerName()
        {
            String serverAddress = ConfigurationManager.AppSettings["serverAddress"];
            String[] childItem = serverAddress.Split(new char[] { ';' });
            foreach (String item in childItem)
            {
                if (item.StartsWith("Data Source"))
                {
                    String[] ipAddrss = item.Split(new char[] { '=' });
                    if (ipAddrss.Length == 2)
                    {
                        this.serverName.Text = ipAddrss[1];
                    }
                }
            }
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        /// <param name="serverAndInstanceName">服务器名</param>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="useWindowAuthentication">是否使用windows身份验证</param>
        private void connect(String serverAndInstanceName, String userName, String password, bool useWindowAuthentication)
        {
            if (server == null)
            {
                server = new Server();
            }

            server.ConnectionContext.ServerInstance = serverAndInstanceName;
            server.ConnectionContext.LoginSecure = useWindowAuthentication; //是否启用windows身份验证

            if (!useWindowAuthentication)
            {
                server.ConnectionContext.Login = userName;
                server.ConnectionContext.Password = password;
            }

            server.ConnectionContext.Connect();
        }

        /// <summary>
        /// 获取服务器数据库名列表
        /// </summary>
        /// <returns></returns>
        private List<String> getDatabaseNameList()
        {
            List<String> dbList = new List<String>();
            foreach (Database db in server.Databases)
            {
                dbList.Add(db.Name);
            }
            return dbList;
        }

        /// <summary>
        /// 查询备份历史记录
        /// </summary>
        private void buildBackupHistory()
        {
            BackupDatabaseBLL backupDatabase = new BackupDatabaseBLL();
            this.backupHistory.Columns["backupTime"].DefaultCellStyle.Format = "yyyy-MM-dd hh:MM:ss";
            this.backupHistory.AutoGenerateColumns = false;
            this.backupHistory.DataSource = backupDatabase.queryAllBackupRecords();
        }

        /// <summary>
        /// 检查备份文件是否完好
        /// </summary>
        /// <param name="backupPath"></param>
        /// <returns></returns>
        private bool verifyBackupFile(String backupPath)
        {
            Restore restore = new Restore();
            try
            {
                restore.Devices.AddDevice(backupPath,DeviceType.File);
                bool verifyFlag = restore.SqlVerify(server);
                if (!verifyFlag)
                {
                    MessageUtil.ShowError("备份损坏或者丢失，无法进行恢复！");
                    return false;
                }
            }
            catch (Exception)
            {
                MessageUtil.ShowError("备份损坏或者丢失，无法进行恢复！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="databaseName"></param>
        private void restoreDatabase(String srcDatabasePath,String desDatabaseName)
        {
            //检查源备份
            if (!verifyBackupFile(srcDatabasePath))
            {
                return;
            }

            Restore restore = new Restore();
            restore.Action = RestoreActionType.Database;
            restore.Database = desDatabaseName;
            restore.Devices.AddDevice(srcDatabasePath, DeviceType.File);

            restore.ReplaceDatabase = true;
            restore.Complete += new ServerMessageEventHandler(restore_Complete);
            try
            {
                restore.SqlRestoreAsync(server);
            }
            catch (Exception)
            {
                MessageUtil.ShowError("恢复数据库失败！");
            }
        }

        /// <summary>
        /// 恢复完成事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void restore_Complete(object sender, ServerMessageEventArgs e)
        {
            this.Invoke(new closeProressBarFormDelegate(closeProgressForm));
        }

        /// <summary>
        /// 关闭进度条窗口
        /// </summary>
        private void closeProgressForm()
        {
            if (this.progressForm != null)
            {
                progressForm.Close();
                progressForm = null;
            }

            MessageUtil.ShowTips("恢复完毕！");
        }

        #region 按钮点击事件
        
        /// <summary>
        /// 检查连接必要的信息是否填写完整
        /// </summary>
        /// <returns></returns>
        private bool checkLinkInforIntegrity()
        {
            if (String.IsNullOrEmpty(this.serverName.Text))
            {
                MessageUtil.ShowWarning("服务器名称为空，请填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.loginId.Text))
            {
                MessageUtil.ShowWarning("登录名为空，请填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.loginPassword.Text))
            {
                MessageUtil.ShowWarning("登录密码为空，请填写！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检查恢复必要的信息是否完全
        /// </summary>
        /// <returns></returns>
        private bool checkRestoreInforIntegrity()
        {
            if (String.IsNullOrEmpty(this.databaseList.Text))
            {
                MessageUtil.ShowWarning("目标数据库名为空，请选择或填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.sourceBackupPath.Text))
            {
                MessageUtil.ShowWarning("源备份文件为空，请在备份记录中选择！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// //初始化数据库列表
        /// </summary>
        private void initDatabaseList()
        {
            List<String> databaseNameList = getDatabaseNameList();
            foreach (String databaseName in databaseNameList)
            {
                this.databaseList.Items.Add(databaseName);
            }
        }

        /// <summary>
        /// 建立数据库连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createLinkBtn_Click(object sender, EventArgs e)
        {
            if (!checkLinkInforIntegrity())
            {
                return;
            }

            if (this.server != null && this.server.ConnectionContext.IsOpen)
            {
                MessageUtil.ShowWarning("连接已建立并打开！");
                return;
            }

            try
            {
                //和数据库服务器建立连接
                connect(this.serverName.Text, this.loginId.Text, this.loginPassword.Text, false);

                //初始化数据库列表
                initDatabaseList();

                //初始化备份历史表
                buildBackupHistory();
            }
            catch (Exception exp)
            {
                MessageUtil.ShowError(exp.Message);
            }
        }

        /// <summary>
        /// 恢复数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restoreBtn_Click(object sender, EventArgs e)
        {
            if (server == null)
            {
                MessageUtil.ShowWarning("数据库未连接，无法恢复！");
                return;
            }
            if (!server.ConnectionContext.IsOpen)
            {
                MessageUtil.ShowWarning("数据库连接断开，无法恢复！");
                return;
            }

            if (!checkRestoreInforIntegrity())
            {
                return;
            }

            this.selectedDatabase = this.databaseList.Text;
            this.sourceFilePath = this.sourceBackupPath.Text;

            this.backgroundWorker.RunWorkerAsync(this);
        }

        /// <summary>
        /// 退出窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 刷新备份历史
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            buildBackupHistory();
        }
        #endregion

        /// <summary>
        /// 备份历史表选中行变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backupHistory_SelectionChanged(object sender, EventArgs e)
        {
            int currentIndex = this.backupHistory.CurrentRow.Index;
            if (currentIndex < 0)
            {
                return;
            }

            this.sourceBackupPath.Text = Convert.ToString(this.backupHistory.Rows[currentIndex].Cells["Backup_Path"].Value);
        }

        /// <summary>
        /// 窗口即将关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RestoreDatabaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            releaseConnection();
        }

        /// <summary>
        /// 释放连接资源
        /// </summary>
        private void releaseConnection()
        {
            if (server != null && server.ConnectionContext.IsOpen)
            {
                server.ConnectionContext.Disconnect();
            }
        }

        /// <summary>
        /// 后台程序执行函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MethodInvoker methodInvoker = new MethodInvoker(showProgressBar);
            this.BeginInvoke(methodInvoker);

            restoreDatabase(this.sourceFilePath, this.selectedDatabase);
        }

        /// <summary>
        /// 显示进度条窗体
        /// </summary>
        private void showProgressBar()
        {
            progressForm = new ProgressForm();
            progressForm.ShowDialog();
        }

    }
}
