﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SystemConfig
{
    public partial class TasksForm : Form
    {
        public TasksForm()
        {
            InitializeComponent();
            this.label1.Text = getTask();
            int x = (System.Windows.Forms.SystemInformation.WorkingArea.Width - this.Size.Width) / 2;
            int y = (System.Windows.Forms.SystemInformation.WorkingArea.Height - this.Size.Height) / 2+ 40;
            this.StartPosition = FormStartPosition.Manual; //窗体的位置由Location属性决定
            this.Location = (Point)new Size(x, y);         //窗体的起始位置为(x,y)
        }


        private string getTask()
        {
            LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
            DataTable dt = locationEvalutionPersonBLL.findAllTaskByUserID(SingleUserInstance.getCurrentUserInstance().User_ID);
            string taskTxt = "";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    taskTxt += dt.Rows[i][0].ToString() + "\n" + "\n";
                }
            }
            return taskTxt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
