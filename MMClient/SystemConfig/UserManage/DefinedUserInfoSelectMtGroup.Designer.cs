﻿using System;

namespace MMClient.SystemConfig.UserManage
{
    partial class DefinedUserInfoSelectMtGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PurMtInfo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PurGroupName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MtGroupTable = new System.Windows.Forms.DataGridView();
            this.sysUserName = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurGroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MtPorGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MtGroupTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.PurMtInfo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.PurGroupName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Location = new System.Drawing.Point(-1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(695, 166);
            this.panel1.TabIndex = 0;
            // 
            // PurMtInfo
            // 
            this.PurMtInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.PurMtInfo.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PurMtInfo.Location = new System.Drawing.Point(117, 66);
            this.PurMtInfo.Multiline = true;
            this.PurMtInfo.Name = "PurMtInfo";
            this.PurMtInfo.ReadOnly = true;
            this.PurMtInfo.Size = new System.Drawing.Size(529, 84);
            this.PurMtInfo.TabIndex = 66;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 65;
            this.label2.Text = "修改前：";
           
            // 
            // PurGroupName
            // 
            this.PurGroupName.Cursor = System.Windows.Forms.Cursors.Default;
            this.PurGroupName.FormattingEnabled = true;
            this.PurGroupName.Location = new System.Drawing.Point(117, 21);
            this.PurGroupName.Name = "PurGroupName";
            this.PurGroupName.Size = new System.Drawing.Size(183, 20);
            this.PurGroupName.TabIndex = 64;
          
            this.PurGroupName.SelectedIndexChanged += new System.EventHandler(this.PurGroupName_SelectedIndexChanged);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 24);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择采购组织：";
           
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.MtGroupTable);
            this.panel2.Location = new System.Drawing.Point(-1, 173);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(695, 301);
            this.panel2.TabIndex = 1;
            // 
            // MtGroupTable
            // 
            this.MtGroupTable.AllowUserToAddRows = false;
            this.MtGroupTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MtGroupTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MtGroupTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.MtGroupTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MtGroupTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MtGroupTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MtGroupTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sysUserName,
            this.ID,
            this.name,
            this.PurGroupID,
            this.MtPorGroup});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MtGroupTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.MtGroupTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.MtGroupTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.MtGroupTable.Location = new System.Drawing.Point(0, 3);
            this.MtGroupTable.Name = "MtGroupTable";
            this.MtGroupTable.RowTemplate.Height = 23;
            this.MtGroupTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MtGroupTable.Size = new System.Drawing.Size(695, 295);
            this.MtGroupTable.TabIndex = 6;
            this.MtGroupTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MtGroupTable_CellContentClick);
            // 
            // sysUserName
            // 
            this.sysUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.sysUserName.FillWeight = 91.37056F;
            this.sysUserName.Frozen = true;
            this.sysUserName.HeaderText = "选择";
            this.sysUserName.Name = "sysUserName";
            this.sysUserName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sysUserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sysUserName.Width = 60;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID.DataPropertyName = "Material_Group";
            this.ID.FillWeight = 104.3147F;
            this.ID.Frozen = true;
            this.ID.HeaderText = "物料组编号";
            this.ID.Name = "ID";
            this.ID.Width = 120;
            // 
            // name
            // 
            this.name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.name.DataPropertyName = "Description";
            this.name.FillWeight = 104.3147F;
            this.name.Frozen = true;
            this.name.HeaderText = "物料组名称";
            this.name.Name = "name";
            this.name.Width = 120;
            // 
            // PurGroupID
            // 
            this.PurGroupID.DataPropertyName = "PurGroupID";
            this.PurGroupID.HeaderText = "采购组编号";
            this.PurGroupID.Name = "PurGroupID";
            // 
            // MtPorGroup
            // 
            this.MtPorGroup.DataPropertyName = "MtPorGroup";
            this.MtPorGroup.HeaderText = "采购组名称";
            this.MtPorGroup.Name = "MtPorGroup";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 493);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "确  认";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(425, 494);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "返  回";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DefinedUserInfoSelectMtGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 526);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "DefinedUserInfoSelectMtGroup";
            this.Text = "物料组列表";
            this.Load += new System.EventHandler(this.LoadPurGroupName);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MtGroupTable)).EndInit();
            this.ResumeLayout(false);

        }
        

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox PurGroupName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView MtGroupTable;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sysUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurGroupID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MtPorGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PurMtInfo;
        private System.Windows.Forms.Button button2;
    }
}