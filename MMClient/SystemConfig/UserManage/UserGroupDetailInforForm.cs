﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserGroupDetailInforForm : Form
    {
        private int operateFlag;    //操作标志，1表示新建，2表示编辑修改
        private BaseGroupModel groupModel;

        public UserGroupDetailInforForm(int operateFlag)
        {
            InitializeComponent();
            this.operateFlag = operateFlag;
            if (this.operateFlag == 1)
            {
                setUserGroupID();
            }
        }

        /// <summary>
        /// 根据用户组编号初始化用户信息
        /// </summary>
        /// <param name="Group_ID"></param>
        public void initGroupInfor(String Group_ID)
        {
            if (groupModel == null)
            {
                groupModel = new BaseGroupModel();
            }
            groupModel.Group_ID = Group_ID;

            UserGroupDetailInforBLL userGroupDetail = new UserGroupDetailInforBLL();
            try
            {
                userGroupDetail.queryGroupInforById(groupModel);

                this.userGroupId.Text = groupModel.Group_ID;
                this.userGroupName.Text = groupModel.Group_Name;
                this.userGroupDescription.Text = groupModel.Description;
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        /// <summary>
        /// 设置角色编号
        /// </summary>
        private void setUserGroupID()
        {
            UserGroupDetailInforBLL userGroup = new UserGroupDetailInforBLL();
            try
            {
                this.userGroupId.Text = userGroup.getBiggestGroupId();
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        /// <summary>
        /// 检查信息完整性
        /// </summary>
        /// <returns></returns>
        private bool checkIntegrity()
        {
            if (String.IsNullOrEmpty(this.userGroupName.Text))
            {
                MessageUtil.ShowWarning("用户组名称不能为空，请填写！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 构建角色Model
        /// </summary>
        private void buildGroupModel()
        {
            if (groupModel == null)
            {
                groupModel = new BaseGroupModel();
            }

            groupModel.Group_ID = this.userGroupId.Text;
            groupModel.Group_Name = this.userGroupName.Text;
            groupModel.Description = this.userGroupDescription.Text;

            if (this.operateFlag == 1)
            {
                groupModel.Generate_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                groupModel.Generate_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                groupModel.Generate_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
            else if (this.operateFlag == 2)
            {
                groupModel.Modify_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                groupModel.Modify_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                groupModel.Modify_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
        }

        /// <summary>
        /// 确认提交修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            //检查信息是否填写完整
            if (!checkIntegrity())
            {
                return;
            }

            //提示是否确认保存
            if (DialogResult.OK != MessageUtil.ShowOKCancelAndQuestion("确定保存用户组信息？"))
            {
                return;
            }

            //构建用户组信息Model
            buildGroupModel();

            UserGroupDetailInforBLL userGroup = new UserGroupDetailInforBLL();
            if (this.operateFlag == 1)
            {
                userGroup.insertNewGroup(groupModel);
            }
            else if (this.operateFlag == 2)
            {
                userGroup.updateGroupInforById(groupModel);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 取消操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// 获取组信息Model
        /// </summary>
        /// <returns></returns>
        public BaseGroupModel getGroupModel()
        {
            return this.groupModel;
        }

    }
}
