﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SystemConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMClient.progress;
using System.Windows.Forms;
using MMClient.CommFileShow;

namespace MMClient.SystemConfig.UserManage
{
    public partial class AddSysUserForm : Form
    {
        private int operateFlag;
        private DefinedUserInfoSelectMtGroup definedUserInfoSelectMtGroup;
        private UserInfoListForm userInfoListForm;
        private string userid;
        private SystemUserOrganicRelationshipModel model;
        SystemUserOrganicRelationshipBLL systemUserOrganicRelationshipBLL = new SystemUserOrganicRelationshipBLL();
        private bool EvalTag = false;
        private int type;
        FileShowForm fileShowForm = null;
        FTPHelper fTPHelper = new FTPHelper("");
        public AddSysUserForm(string userid, int operateFlag)
        {
            InitializeComponent();
            this.operateFlag = operateFlag;
            this.userid = userid;
            if (this.operateFlag == 1)
            {
                this.Text = "新建员工";
            }
            else if (this.operateFlag == 2)
            {
                this.Text = "员工信息查看/修改";
            }
        }

        private void concelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// save new user info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (!checkUserInformationIntegrity())
            {
                MessageUtil.ShowWarning("信息不完整");
                return;
            }
            else
            {
                //将用户信息插入数据库中 
                setSysMAnagerUserModel();
                try
                {
                    if (operateFlag == 1)//保存信息
                    {
                        model.sysUser_ID = this.userID.Text.ToString();
                        model.pid = this.upUserID.Text;
                        //保存用户信息
                        systemUserOrganicRelationshipBLL.insertNewUser(model);
                        //保存主审关系
                        if (EvalTag == false)
                        {
                            string purName = this.PurName.Text.ToString();
                            if (this.labelMt.Text.ToString().Equals("物料组"))
                            {
                                //1 删除前有的评估员身份 2 删除前有的主审身份
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID,1);
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID,2);
                                String[] names = this.buttonMtGroup.Text.ToString().Split('、');
                                String[] nameMtGroup = this.PurName.Text.ToString().Split('、');
                                for (int i = 0; i < names.Length; i++)
                                {
                                    if (!names[i].ToString().Trim().Equals(""))
                                    {
                                        type = 1;
                                        systemUserOrganicRelationshipBLL.insertHanmote_User_MtGroupName(this.userID.Text.ToString(), names[i], nameMtGroup[i],type);
                                    }
                                }
                            }
                        }
                        //保存评审员与评估方面的关系
                        if (EvalTag == true)
                        {
                            if (this.labelMt.Text.ToString().Equals("评估方面"))
                            {
                                string evalcode = this.EvalCode.Text.ToString();
                                string code = "";
                                if (evalcode.Equals("技术")) code = "20000";
                                if (evalcode.Equals("财务")) code = "50000";
                                if (evalcode.Equals("质量")) code = "40000";
                                if (evalcode.Equals("交付")) code = "30000";
                                if (evalcode.Equals("成本")) code = "10000";
                                //1 删除前有的评估员身份 2 删除前有的主审身份
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID, 2);
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID, 1);
                                systemUserOrganicRelationshipBLL.insertHanmoteBaseUser_EvalCode(this.userID.Text.ToString(), code,evalcode);
                                //保存物料组评估员关系
                                String[] names = this.buttonMtGroup.Text.ToString().Split('、');
                                String[] nameMtGroup = this.PurName.Text.ToString().Split('、');
                                for (int i = 0; i < names.Length; i++)
                                {
                                    if (!names[i].ToString().Trim().Equals(""))
                                    {
                                        type = 2;//建立评估员与主审关系
                                        systemUserOrganicRelationshipBLL.insertHanmote_User_MtGroupName(this.userID.Text.ToString(), names[i], nameMtGroup[i],type);
                                    }
                                }
                            }
                        }
                        MessageBox.Show("保存成功");
                        this.Close();
                    }
                    else if (operateFlag == 2)
                    {
                        //将编辑后的用户信息插入数据库
                        model.sysUser_ID = this.userid;
                        model.pid = this.upUserID.Text;
                        systemUserOrganicRelationshipBLL.updateUserInfor(model);
                        //保存主审关系并删除评估员身份
                        string purName = this.PurName.Text.ToString();//采购组织
                        if (EvalTag == false)
                        {
                            if (this.labelMt.Text.ToString().Equals("物料组"))
                            {
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID,1);
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID,2);
                                String[] names = this.buttonMtGroup.Text.ToString().Split('、');
                                String[] nameMtGroup = this.PurName.Text.ToString().Split('、');
                                type = 1;//主审
                                for (int i = 0; i < names.Length; i++)
                                {
                                    if (!names[i].ToString().Trim().Equals(""))
                                    {
                                        systemUserOrganicRelationshipBLL.updateHanmote_User_MtGroupName(model.sysUser_ID, names[i], nameMtGroup[i],type);
                                    }
                                }
                            }
                        }
                        if (EvalTag == true)
                        {
                            //保存评审员与评估方面的关系并删除主审身份
                            if (this.labelMt.Text.ToString().Equals("评估方面"))
                            {
                                string evalcode = this.EvalCode.Text.ToString();
                                string code = "";
                                if (evalcode.Equals("技术")) code = "20000";
                                if (evalcode.Equals("财务")) code = "50000";
                                if (evalcode.Equals("质量")) code = "40000";
                                if (evalcode.Equals("交付")) code = "30000";
                                if (evalcode.Equals("成本")) code = "10000";
                                //1 删除前有的评估员身份 2 删除前有的主审身份
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID, 1);
                                systemUserOrganicRelationshipBLL.deleteHamote_User_MtGroup(model.sysUser_ID, 2);
                                systemUserOrganicRelationshipBLL.insertHanmoteBaseUser_EvalCode(this.userID.Text.ToString(), code, evalcode);
                                //读取信息并保存评估员与物料组关系
                                String[] names = this.buttonMtGroup.Text.ToString().Split('、');
                                String[] nameMtGroup = this.PurName.Text.ToString().Split('、');
                                type = 2;//评估员
                                for (int i = 0; i < names.Length; i++)
                                {
                                    if (!names[i].ToString().Trim().Equals(""))
                                    {
                                        systemUserOrganicRelationshipBLL.updateHanmote_User_MtGroupName(model.sysUser_ID, names[i], nameMtGroup[i],type);
                                    }
                                }
                            }

                        }
                        MessageBox.Show("更新成功");
                        this.Close();
                    }
                }
                catch (Exception)
                {

                    MessageUtil.ShowError("插入或更新用户信息失败！");
                    this.DialogResult = DialogResult.Cancel;
                    return;
                }
            }

        }

        private void setSysMAnagerUserModel()
        {
            //界面信息shilihua
            if (model == null)
            {
                model = new SystemUserOrganicRelationshipModel();
            }
            model.sysUserName = this.username.Text.ToString();
            model.Login_Name = this.loginName.Text.ToString();
            model.Birthday = this.birthday.Text.ToString();
            model.Birth_Place = this.birthPlace.Text.ToString();
            model.departName = this.departNametxt.Text.ToString();
            model.User_Description = this.description.Text.ToString();
            model.Login_Password = this.loginPassword.Text.ToString();
            model.compangClass = this.companyIDtxt.SelectedValue.ToString();
            model.jobPosition = this.jobPositionIDtxt.SelectedValue.ToString();
            model.Email = this.email.Text.ToString();
            model.Gender = this.gender.SelectedIndex;
            model.Mobile = this.telephone.Text.ToString();
            model.status = this.status.SelectedIndex;
            model.pid = this.upUserID.Text.ToString();


        }

        /// <summary>
        /// 检查信息是否填写完整
        /// </summary>
        /// <returns></returns>
        private bool checkUserInformationIntegrity()
        {
            if (String.IsNullOrEmpty(this.userID.Text))
            {
                MessageUtil.ShowError("系统生成用户编号失败，请关闭窗口重试！");
                return false;
            }

            if (String.IsNullOrEmpty(this.username.Text))
            {
                MessageUtil.ShowWarning("用户姓名不能为空，请填写");
                this.username.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.gender.Text) || this.gender.SelectedIndex < 0)
            {
                MessageUtil.ShowWarning("用户性别不能为空，请选择！");
                this.gender.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.birthPlace.Text))
            {
                MessageUtil.ShowWarning("用户籍贯不能为空，请填写！");
                this.birthPlace.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(this.loginName.Text))
            {
                MessageUtil.ShowWarning("用户登录名不能为空，请填写！");
                this.loginName.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.loginPassword.Text))
            {
                MessageUtil.ShowWarning("用户登录密码不能为空，请填写！");
                this.loginPassword.Focus();
                return false;
            }

            if (this.loginPassword.Text.Length > 0 && this.loginPassword.Text.Length < 6)
            {
                MessageUtil.ShowWarning("用户登录密码必须为6~16个字符，请修改！");
                this.loginPassword.Focus();
                return false;
            }

            if (this.birthday.Value.CompareTo(DateTime.Now) >= 0)
            {
                MessageUtil.ShowWarning("用户出生日期不合法，请修改！");
                this.birthday.Focus();
                return false;
            }
            if (this.companyIDtxt.SelectedValue == null)
            {
                MessageUtil.ShowWarning("信息不完整");
                return false;
            }
            if (this.jobPositionIDtxt.SelectedValue == null)
            {
                MessageUtil.ShowWarning("信息不完整");
                return false;
            }
            if (this.departNametxt.SelectedValue == null)
            {
                MessageUtil.ShowWarning("信息不完整");
                return false;
            }

            return true;
        }



        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SysManager_Load(object sender, EventArgs e)
        {
            DataTable companyNameSQL = systemUserOrganicRelationshipBLL.getCompanyNameSQL();
            this.companyIDtxt.DataSource = companyNameSQL;
            this.companyIDtxt.DisplayMember = "name";
            this.companyIDtxt.ValueMember = "id";
            DataTable dt = DBHelper.ExecuteQueryDT("SELECT Description FROM [TabEvalItem]");
            List<string> certiItem = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++) {
                certiItem.Add(dt.Rows[i][0].ToString());
            }
            cbx_qualityCerti.DataSource = certiItem;

            if (operateFlag == 1)
            {
                Random r = new Random();
                this.userID.Text = DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss");

            }
            if (operateFlag == 2)
            {
                initEditingUserinfor(this.userid);
            }
        }

        private void companyIDtxt_SelectedIndexChanged(object sender, EventArgs e)
        {
            string comanyID = this.companyIDtxt.SelectedValue.ToString();
            DataTable RoleNameSQL = systemUserOrganicRelationshipBLL.getRoleNameSQL(comanyID);
            if (RoleNameSQL != null)
            {
                this.jobPositionIDtxt.DataSource = RoleNameSQL;
                this.jobPositionIDtxt.DisplayMember = "name";
                this.jobPositionIDtxt.ValueMember = "id";
            }
            DataTable OrganicNameSQL = systemUserOrganicRelationshipBLL.getOrganicNameSQL(comanyID);
            if (OrganicNameSQL != null)
            {
                this.departNametxt.DataSource = OrganicNameSQL;
                this.departNametxt.DisplayMember = "name";
                this.departNametxt.ValueMember = "id";
            }
        }
        /// <summary>
        /// 编辑用户时初始化基本信息
        /// </summary>
        /// <param name="userID">用户编号</param>
        public void initEditingUserinfor(string userID)
        {
            //界面信息
            if (model == null)
            {
                model = new SystemUserOrganicRelationshipModel();
            }
            model.sysUser_ID = userID;
            //查询用户基本信息
            systemUserOrganicRelationshipBLL.queryUserInforById(model);
            string codeID = systemUserOrganicRelationshipBLL.getEvalCodeByUserId(model.sysUser_ID);
            if (codeID.Equals("20000")) model.Evalcode = "技术";
            if (codeID.Equals("50000")) model.Evalcode = "财务";
            if (codeID.Equals("40000")) model.Evalcode = "质量";
            if (codeID.Equals("30000")) model.Evalcode = "交付";
            if (codeID.Equals("10000")) model.Evalcode = "成本";

            getFile(userID + "//资格", this.checkedListBox1);


            //设置界面上的用户信息
            setUserInforOnPanel();
        }
        //获取文件信息filePath格式：string+//+string
        private void getFile(string filePath,CheckedListBox checkedListBox)
        {
            string[] filenames = fTPHelper.GetFileList(filePath);
            if (filenames == null) return;
            for (int i = 0; i < filenames.Length; i++)
                checkedListBox.Items.Add(filenames[i]);

        }


        private void setUserInforOnPanel()
        {
            if (model == null)
            {
                return;
            }
            //界面填写的信息
            this.userID.Text = model.sysUser_ID;
            this.username.Text = model.Username;
            this.gender.SelectedIndex = model.Gender;
            this.birthday.Value = Convert.ToDateTime(model.Birthday);
            this.birthPlace.Text = model.Birth_Place;
            this.telephone.Text = model.Mobile;
            this.email.Text = model.Email;
            this.description.Text = model.User_Description;
            this.loginName.Text = model.Login_Name;
            this.loginPassword.Text = model.Login_Password;
            this.status.SelectedIndex = model.status;
            this.companyIDtxt.SelectedValue = model.compangClass;
            this.jobPositionIDtxt.SelectedValue = model.jobPosition;
            this.departNametxt.Text = model.departName;
            //根据用户ID得到直接上级和物料组
            this.buttonUser.Text = systemUserOrganicRelationshipBLL.getUpUserNameById(model.sysUser_ID);
            this.upUserID.Text = systemUserOrganicRelationshipBLL.getPIDByUserId(model.sysUser_ID);
            this.EvalCode.Text = model.Evalcode;
            this.buttonMtGroup.Text = systemUserOrganicRelationshipBLL.getMtGroupName(model.sysUser_ID); 
            this.MethodMtGroup.Text = systemUserOrganicRelationshipBLL.getMtMethodGroupName(model.sysUser_ID);
        }

        private void jobPositionIDtxt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.jobPositionIDtxt.Text.Equals("主审员") || this.jobPositionIDtxt.Text.Equals("主审") || this.jobPositionIDtxt.Text.Equals("主评审员") || this.jobPositionIDtxt.Text.Equals("主评"))
            {
                DataTable table = systemUserOrganicRelationshipBLL.getMtGroupName();
                this.labelMt.Text = "物料组";
                this.labelMt.Show();
                this.buttonMtGroup.Show();
                this.buttonMtGroup.Text = "选择物料组";
                this.EvalCode.Hide();
                this.EvalTag = false;
                this.labelMethodMtGroup.Hide();
                this.MethodMtGroup.Hide();
            }
            else if (this.jobPositionIDtxt.Text.Equals("普通评审员") || this.jobPositionIDtxt.Text.Equals("评审") || this.jobPositionIDtxt.Text.Equals("评审员") || this.jobPositionIDtxt.Text.Equals("评审人"))
            {
                this.labelMt.Text = "评估方面";
                this.labelMt.Show();
                this.EvalCode.Show();
                this.buttonMtGroup.Hide();
                this.EvalCode.Items.Clear();
                this.EvalCode.Items.Add("技术");//20000
                this.EvalCode.Items.Add("财务");//50000
                this.EvalCode.Items.Add("质量");//40000
                this.EvalCode.Items.Add("成本");//10000
                this.EvalCode.Items.Add("交付");//30000
                this.EvalCode.SelectedIndex = 0;
                this.EvalTag = true;
                this.labelMethodMtGroup.Show();
                this.MethodMtGroup.Show();
            }
            else
            {
                this.labelMt.Hide();
                this.buttonMtGroup.Hide();
                this.EvalCode.Hide();
                this.buttonMtGroup.Text = "查看详情";
                this.labelMt.Text = "查看详情";
                this.labelMethodMtGroup.Text = "查看详情";
                this.MethodMtGroup.Text = "查看详情";
                this.labelMethodMtGroup.Hide();
                this.MethodMtGroup.Hide();

            }
        }
        /// <summary>
        /// 选择物料组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMtGroup_Click(object sender, EventArgs e)
        {
            if (EvalTag == false)//如果EvalTag=false 表示主审
            {
                //this.buttonMtGroup.Text = "";
                if (definedUserInfoSelectMtGroup == null || definedUserInfoSelectMtGroup.IsDisposed)
                {
                    definedUserInfoSelectMtGroup = new DefinedUserInfoSelectMtGroup(this, this.userID.Text.ToString(),1);
                }
                definedUserInfoSelectMtGroup.Show();
                if (definedUserInfoSelectMtGroup.MTname != null && !definedUserInfoSelectMtGroup.MTname.Equals(""))
                {
                    this.buttonMtGroup.Text = definedUserInfoSelectMtGroup.MTname;
                }
            }
            if (EvalTag == true)//EvalTag = true 评估方面
            {
                //直接保存当前信息
            }
        }
        /// <summary>
        /// 选择直接上级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUser_Click(object sender, EventArgs e)
        {
            if (userInfoListForm == null || userInfoListForm.IsDisposed)
            {
                userInfoListForm = new UserInfoListForm(this);
            }
            userInfoListForm.Show();
        }

        private void MethodMtGroup_Click(object sender, EventArgs e)
        {
            //选择物料组
            if (definedUserInfoSelectMtGroup == null || definedUserInfoSelectMtGroup.IsDisposed)
            {
                definedUserInfoSelectMtGroup = new DefinedUserInfoSelectMtGroup(this, this.userID.Text.ToString(),2);
            }
            definedUserInfoSelectMtGroup.Show();
            if (definedUserInfoSelectMtGroup.MTname != null && !definedUserInfoSelectMtGroup.MTname.Equals(""))
            {
                this.MethodMtGroup.Text = definedUserInfoSelectMtGroup.MTname;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_Upload_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                for (int i = 0; i < openFileDialog1.SafeFileNames.Length; i++)
                {
                    checkedListBox1.Items.Add(openFileDialog1.SafeFileNames[i]);

                }


            }
            else {
                return;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            progreBar progreBar = new progreBar(fTPHelper, openFileDialog1.FileNames, "/" +this.userID.Text + "/资格");


            progreBar.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            List<string> fileIDList = new List<string>();
            string filePath = this.userID.Text + "//资格//";

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    string filename = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    fileIDList.Add(filename);

                }

            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];

            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try
            {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }



            }
            catch (Exception)
            {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);
        }
    }
}
