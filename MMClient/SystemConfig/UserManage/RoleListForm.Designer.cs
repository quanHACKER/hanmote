﻿namespace MMClient.SystemConfig.UserManage
{
    partial class RoleListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoleListForm));
            this.topPanel = new System.Windows.Forms.Panel();
            this.roleTable = new System.Windows.Forms.DataGridView();
            this.Role_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Role_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roleTable)).BeginInit();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.roleTable);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(753, 304);
            this.topPanel.TabIndex = 1;
            // 
            // roleTable
            // 
            this.roleTable.AllowUserToAddRows = false;
            this.roleTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.roleTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.roleTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.roleTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.roleTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.roleTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Role_ID,
            this.Role_Name,
            this.Description});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.roleTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.roleTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roleTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.roleTable.Location = new System.Drawing.Point(0, 0);
            this.roleTable.Name = "roleTable";
            this.roleTable.RowTemplate.Height = 23;
            this.roleTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.roleTable.Size = new System.Drawing.Size(753, 304);
            this.roleTable.TabIndex = 0;
            // 
            // Role_ID
            // 
            this.Role_ID.DataPropertyName = "Role_ID";
            this.Role_ID.FillWeight = 8.583295F;
            this.Role_ID.HeaderText = "角色编号";
            this.Role_ID.MinimumWidth = 150;
            this.Role_ID.Name = "Role_ID";
            // 
            // Role_Name
            // 
            this.Role_Name.DataPropertyName = "Role_Name";
            this.Role_Name.FillWeight = 15.58376F;
            this.Role_Name.HeaderText = "角色名称";
            this.Role_Name.MinimumWidth = 240;
            this.Role_Name.Name = "Role_Name";
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.FillWeight = 85.83295F;
            this.Description.HeaderText = "备注";
            this.Description.MinimumWidth = 300;
            this.Description.Name = "Description";
            // 
            // RoleListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 304);
            this.Controls.Add(this.topPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RoleListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "角色列表";
            this.topPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roleTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.DataGridView roleTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Role_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Role_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
    }
}