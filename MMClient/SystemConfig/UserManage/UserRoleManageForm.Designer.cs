﻿namespace MMClient.SystemConfig.UserManage
{
    partial class UserRoleManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserRoleManageForm));
            this.topPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.userDataGridView = new System.Windows.Forms.DataGridView();
            this.userId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userRoleTreeView = new System.Windows.Forms.TreeView();
            this.roleTreeViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addRoleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editRoleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeRoleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStaffMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorizeRoleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userDataGridViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeUserMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).BeginInit();
            this.roleTreeViewMenu.SuspendLayout();
            this.userDataGridViewMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label3);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.userDataGridView);
            this.topPanel.Controls.Add(this.userRoleTreeView);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(927, 510);
            this.topPanel.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(274, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = ">>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(298, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "员工信息";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(28, 17);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "角色";
            // 
            // userDataGridView
            // 
            this.userDataGridView.AllowUserToAddRows = false;
            this.userDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.userDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.userDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userId,
            this.userName,
            this.gender,
            this.birthday,
            this.mobile});
            this.userDataGridView.Location = new System.Drawing.Point(302, 40);
            this.userDataGridView.Name = "userDataGridView";
            this.userDataGridView.RowHeadersVisible = false;
            this.userDataGridView.RowTemplate.Height = 23;
            this.userDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userDataGridView.Size = new System.Drawing.Size(604, 457);
            this.userDataGridView.TabIndex = 1;
            this.userDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.userDataGridView_CellMouseClick);
            // 
            // userId
            // 
            this.userId.DataPropertyName = "User_ID";
            this.userId.HeaderText = "员工编号";
            this.userId.Name = "userId";
            // 
            // userName
            // 
            this.userName.DataPropertyName = "Username";
            this.userName.HeaderText = "姓名";
            this.userName.Name = "userName";
            // 
            // gender
            // 
            this.gender.DataPropertyName = "Gender";
            this.gender.HeaderText = "性别";
            this.gender.Name = "gender";
            // 
            // birthday
            // 
            this.birthday.DataPropertyName = "Birthday";
            this.birthday.HeaderText = "出生日期";
            this.birthday.Name = "birthday";
            // 
            // mobile
            // 
            this.mobile.DataPropertyName = "Mobile";
            this.mobile.HeaderText = "联系方式";
            this.mobile.Name = "mobile";
            // 
            // userRoleTreeView
            // 
            this.userRoleTreeView.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userRoleTreeView.Location = new System.Drawing.Point(30, 40);
            this.userRoleTreeView.Name = "userRoleTreeView";
            this.userRoleTreeView.Size = new System.Drawing.Size(234, 457);
            this.userRoleTreeView.TabIndex = 0;
            this.userRoleTreeView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.userRoleTreeView_MouseUp);
            // 
            // roleTreeViewMenu
            // 
            this.roleTreeViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRoleMenuItem,
            this.editRoleMenuItem,
            this.removeRoleMenuItem,
            this.addStaffMenuItem,
            this.authorizeRoleMenuItem});
            this.roleTreeViewMenu.Name = "roleTreeViewMenu";
            this.roleTreeViewMenu.Size = new System.Drawing.Size(125, 114);
            // 
            // addRoleMenuItem
            // 
            this.addRoleMenuItem.Name = "addRoleMenuItem";
            this.addRoleMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addRoleMenuItem.Text = "新建";
            this.addRoleMenuItem.Click += new System.EventHandler(this.addRoleMenuItem_Click);
            // 
            // editRoleMenuItem
            // 
            this.editRoleMenuItem.Name = "editRoleMenuItem";
            this.editRoleMenuItem.Size = new System.Drawing.Size(124, 22);
            this.editRoleMenuItem.Text = "编辑";
            this.editRoleMenuItem.Click += new System.EventHandler(this.editRoleMenuItem_Click);
            // 
            // removeRoleMenuItem
            // 
            this.removeRoleMenuItem.Name = "removeRoleMenuItem";
            this.removeRoleMenuItem.Size = new System.Drawing.Size(124, 22);
            this.removeRoleMenuItem.Text = "删除";
            this.removeRoleMenuItem.Click += new System.EventHandler(this.removeRoleMenuItem_Click);
            // 
            // addStaffMenuItem
            // 
            this.addStaffMenuItem.Name = "addStaffMenuItem";
            this.addStaffMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addStaffMenuItem.Text = "添加员工";
            this.addStaffMenuItem.Click += new System.EventHandler(this.addStaffMenuItem_Click);
            // 
            // authorizeRoleMenuItem
            // 
            this.authorizeRoleMenuItem.Name = "authorizeRoleMenuItem";
            this.authorizeRoleMenuItem.Size = new System.Drawing.Size(124, 22);
            this.authorizeRoleMenuItem.Text = "功能授权";
            this.authorizeRoleMenuItem.Click += new System.EventHandler(this.authorizeRoleMenuItem_Click);
            // 
            // userDataGridViewMenu
            // 
            this.userDataGridViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeUserMenuItem});
            this.userDataGridViewMenu.Name = "userDataGridViewMenu";
            this.userDataGridViewMenu.Size = new System.Drawing.Size(101, 26);
            // 
            // removeUserMenuItem
            // 
            this.removeUserMenuItem.Name = "removeUserMenuItem";
            this.removeUserMenuItem.Size = new System.Drawing.Size(100, 22);
            this.removeUserMenuItem.Text = "移除";
            this.removeUserMenuItem.Click += new System.EventHandler(this.removeUserMenuItem_Click);
            // 
            // UserRoleManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 510);
            this.Controls.Add(this.topPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(943, 549);
            this.MinimumSize = new System.Drawing.Size(943, 549);
            this.Name = "UserRoleManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "角色管理";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).EndInit();
            this.roleTreeViewMenu.ResumeLayout(false);
            this.userDataGridViewMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TreeView userRoleTreeView;
        private System.Windows.Forms.DataGridView userDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn userId;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip roleTreeViewMenu;
        private System.Windows.Forms.ContextMenuStrip userDataGridViewMenu;
        private System.Windows.Forms.ToolStripMenuItem addRoleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editRoleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeRoleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStaffMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorizeRoleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeUserMenuItem;
        private System.Windows.Forms.Label label3;
    }
}