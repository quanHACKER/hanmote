﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;

using MMClient.SystemConfig.PermissionManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserGroupManageForm : Form
    {
        private DataTable userGroupTable;

        public UserGroupManageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗口初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserGroupManageForm_Load(object sender, EventArgs e)
        {
            reBuildUserGroupTree();
            setOnTreeViewEmpty();
        }

        /// <summary>
        /// 创建树形节点
        /// </summary>
        /// <param name="name">节点name</param>
        /// <param name="text">节点text</param>
        /// <returns></returns>
        private TreeNode createTreeNode(String name,String text)
        {
            TreeNode treeNode = new TreeNode();
            treeNode.Name = name;
            treeNode.Text = text;
            return treeNode;
        }

        /// <summary>
        /// 向树中增加根节点
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="node"></param>
        private void addRootTreeNode(TreeNodeCollection parentNode,TreeNode node)
        {
            if (parentNode != null && node != null)
            {
                node.ImageIndex = 0;            //设置结点图标
                node.SelectedImageIndex = 0;
                parentNode.Add(node);
            }
        }

        /// <summary>
        /// 向树中增加子节点
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="childNode"></param>
        private void addChildTreeNode(TreeNodeCollection parentNode, DataRow row)
        {
            if (parentNode != null && 
                !row["User_ID"].Equals(DBNull.Value) && 
                !row["Username"].Equals(DBNull.Value))
            {
                TreeNode node = createTreeNode(row["User_ID"].ToString(), row["Username"].ToString());
                node.ImageIndex = 1;            //设置结点图标
                node.SelectedImageIndex = 1;
                parentNode.Add(node);
            }
        }
         
        /// <summary>
        /// 重建用户组树形结构
        /// </summary>
        private void reBuildUserGroupTree()
        {
            //清空当前树的所有节点
            this.userGroupTreeView.Nodes.Clear();

            //查询数据库，重新建立树形
            UserGroupManageBLL userGroupManage = new UserGroupManageBLL();
            try
            {
                userGroupTable = userGroupManage.queryAllGroupAndUser();
                int rowCount = userGroupTable.Rows.Count;
                if (rowCount <= 0)
                {
                    return;
                }
                DataRow preRow = userGroupTable.Rows[0];
                String preGroupId = preRow["Group_ID"].ToString();
                String preGroupName = preRow["Group_Name"].ToString();

                TreeNodeCollection root = this.userGroupTreeView.Nodes;
                TreeNode node = createTreeNode(preGroupId,preGroupName);
                //添加根节点
                addRootTreeNode(root, node);
                //添加子节点
                addChildTreeNode(node.Nodes, preRow);

                for (int index = 1; index < rowCount; index++)
                {
                    preGroupId = preRow["Group_ID"].ToString();
                    preGroupName = preRow["Group_Name"].ToString();

                    DataRow curRow = userGroupTable.Rows[index];
                    String curGroupId = curRow["Group_ID"].ToString();
                    String curGroupName = curRow["Group_Name"].ToString();
                    if (preGroupId == curGroupId)   //同一个组中
                    {
                        addChildTreeNode(node.Nodes, curRow);
                    }
                    else     //不同组中
                    {
                        node = createTreeNode(curGroupId,curGroupName);
                        addRootTreeNode(root, node);
                        addChildTreeNode(node.Nodes,curRow);
                        preRow = curRow;
                    }
                }
            }
            catch (Exception)
            {
                MessageUtil.ShowError("初始化用户(组)信息失败！");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        #region 按钮点击事件
        #region 用户区按钮事件
        
        /// <summary>
        /// 恢复TreeView状态
        /// </summary>
        private void recoverTreeViewState()
        {
            //选中TreeView，使被选中的Node高亮
            this.userGroupTreeView.Select();
        }

        /// <summary>
        /// 获取选中子树的根节点
        /// </summary>
        /// <returns></returns>
        private TreeNode getParentRoleNode()
        {
            TreeNode node = this.userGroupTreeView.SelectedNode;
            if (node != null)
            {
                if (node.Level == 0)
                    return node;
                else
                    return node.Parent;
            }
            return null;
        }

        /// <summary>
        /// 添加用户到组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addUserToGroupBtn_Click(object sender, EventArgs e)
        {
            UserListForSelectionForm userListForSelection = new UserListForSelectionForm();
            if (userListForSelection.ShowDialog(this) == DialogResult.OK)
            {
                TreeNode node = getParentRoleNode();
                if (node == null)
                {
                    return;
                }
                String groupId = node.Name;
                String groupName = node.Text;

                UserGroupManageBLL userGroupManage = new UserGroupManageBLL();
                JoinUserGroupModel joinUserGroupModel = new JoinUserGroupModel();
                Dictionary<String, String> selectedUserDict = userListForSelection.getSelectedUserDict();
                foreach (var item in selectedUserDict)
                {
                    try
                    {
                        joinUserGroupModel.Group_ID = groupId;
                        joinUserGroupModel.Group_Name = groupName;
                        joinUserGroupModel.User_ID = item.Key;
                        joinUserGroupModel.Username = item.Value;
                        userGroupManage.addUserToGroup(joinUserGroupModel);
                    }
                    catch (Exception)
                    {
                        //可以打log，不适合弹出对话框提示
                    }
                }

                reBuildUserGroupTree();
            }

            recoverTreeViewState();
        }

        /// <summary>
        /// 从组中移除用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeUserFromGroupBtn_Click(object sender, EventArgs e)
        {
            DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定将该用户移出当前组？");
            if (rest != DialogResult.OK)
            {
                recoverTreeViewState();
                return;
            }

            TreeNode node = this.userGroupTreeView.SelectedNode;
            if (node == null)
            {
                return;
            }
            TreeNode parent = node.Parent;
            JoinUserGroupModel joinUserGroupModel = new JoinUserGroupModel();
            joinUserGroupModel.Group_ID = parent.Name;
            joinUserGroupModel.Group_Name = parent.Text;
            joinUserGroupModel.User_ID = node.Name;
            joinUserGroupModel.Username = node.Text;

            UserGroupManageBLL userGroupManage = new UserGroupManageBLL();
            try
            {
                userGroupManage.removeUserFromGroup(joinUserGroupModel);

                reBuildUserGroupTree();
            }
            catch (Exception exp)
            {
                MessageUtil.ShowError(exp.Message);
            }

            recoverTreeViewState();
        }
        #endregion

        #region 用户组区按钮事件
        /// <summary>
        /// 新增用户组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addUserGroup_Click(object sender, EventArgs e)
        {
            UserGroupDetailInforForm userGroupDetail = new UserGroupDetailInforForm(1);
            if (userGroupDetail.ShowDialog(this) == DialogResult.OK)
            {
                reBuildUserGroupTree();
            }

            recoverTreeViewState();
        }

        /// <summary>
        /// 用户组授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorizeUserGroupBtn_Click(object sender, EventArgs e)
        {
            TreeNode node = this.userGroupTreeView.SelectedNode;
            if (node == null || node.Level != 0)
            {
                return;
            }

            BaseGroupModel baseGroupModel = new BaseGroupModel();
            baseGroupModel.Group_ID = node.Name;
            baseGroupModel.Group_Name = node.Text;

            FunctionLevelPermissionManageForm functionLevelPermission = new FunctionLevelPermissionManageForm(baseGroupModel,2);
            functionLevelPermission.ShowDialog();

            recoverTreeViewState();
        }

        /// <summary>
        /// 编辑用户组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editUserGroupBtn_Click(object sender, EventArgs e)
        {
            UserGroupDetailInforForm userGroupDetail = new UserGroupDetailInforForm(2);
            userGroupDetail.initGroupInfor(this.userGroupTreeView.SelectedNode.Name);
            if (userGroupDetail.ShowDialog(this) == DialogResult.OK)
            {
                reBuildUserGroupTree();
            }

            recoverTreeViewState();
        }

        /// <summary>
        /// 删除用户组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeUserGroupBtn_Click(object sender, EventArgs e)
        {
            DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定删除当前组？");
            if (rest == DialogResult.OK)
            {
                try
                {
                    TreeNode groupNode = this.userGroupTreeView.SelectedNode;

                    JoinUserGroupModel joinUserGroupModel = new JoinUserGroupModel();
                    joinUserGroupModel.Group_ID = groupNode.Name;
                    joinUserGroupModel.Group_Name = groupNode.Text;

                    //移除用户组中所有用户
                    UserGroupManageBLL userGroupManage = new UserGroupManageBLL();
                    userGroupManage.deleteAllUserOfGroup(joinUserGroupModel);

                    //删除用户组的权限信息
                    JoinGroupFunctionPermissionObjectModel groupFunctionPermission = new JoinGroupFunctionPermissionObjectModel();
                    groupFunctionPermission.Group_ID = groupNode.Name;
                    userGroupManage.revokeGroupAllPermission(groupFunctionPermission);

                    //删除用户组信息
                    BaseGroupModel baseGroupModel = new BaseGroupModel();
                    baseGroupModel.Group_ID = groupNode.Name;
                    userGroupManage.deleteGroupById(baseGroupModel);

                    //刷新TreeView
                    groupNode.Nodes.Clear();
                    groupNode.Remove();
                }
                catch (Exception exp)
                {
                    MessageUtil.ShowError(exp.Message);
                }
            }

            
        }
        #endregion

        /// <summary>
        /// 退出窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 按钮启用与否区块
        
        /// <summary>
        /// 树节点为空
        /// </summary>
        private void setOnTreeViewEmpty()
        {
            this.addUserGroup.Enabled = true;

            this.addUserToGroupBtn.Enabled = false;
            this.removeUserFromGroupBtn.Enabled = false;
            this.authorizeUserGroupBtn.Enabled = false;
            this.editUserGroupBtn.Enabled = false;
            this.removeUserGroupBtn.Enabled = false;
        }

        /// <summary>
        /// 组节点选中 按钮设置
        /// </summary>
        private void setButtonClickableRootSelected()
        {
            this.addUserToGroupBtn.Enabled = true;
            this.addUserGroup.Enabled = true;
            this.authorizeUserGroupBtn.Enabled = true;
            this.editUserGroupBtn.Enabled = true;
            this.removeUserGroupBtn.Enabled = true;

            this.removeUserFromGroupBtn.Enabled = false;
        }

        /// <summary>
        /// 用户节点选中 按钮设置
        /// </summary>
        private void setButtonClickableLeafSelected()
        {
            this.addUserToGroupBtn.Enabled = true;
            this.removeUserFromGroupBtn.Enabled = true;
            this.addUserGroup.Enabled = true;
            this.authorizeUserGroupBtn.Enabled = false;
            this.editUserGroupBtn.Enabled = false;
            this.removeUserGroupBtn.Enabled = false;
        }

        /// <summary>
        /// 用户组树形结构节点选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userGroupTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                setButtonClickableRootSelected();
            }
            else
            {
                setButtonClickableLeafSelected();
            }

        }
        #endregion

    }
}
