﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class RoleListForm : Form
    {
        public RoleListForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 获取一个用户的所有角色信息
        /// </summary>
        /// <param name="userId"></param>
        public void setRoleByUserId(String userId)
        {
            RoleListBLL roleListBll = new RoleListBLL();
            JoinUserRoleModel joinUserRoleModel = new JoinUserRoleModel();
            joinUserRoleModel.User_ID = userId;
            try
            {
                this.roleTable.AutoGenerateColumns = false;
                this.roleTable.DataSource = roleListBll.queryRoleByUserId(joinUserRoleModel);
            }
            catch (Exception)
            {
                MessageUtil.ShowError("获取用户角色信息失败！");
                this.Close();
            }
        }

    }
}
