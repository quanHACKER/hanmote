﻿namespace MMClient.SystemConfig.UserManage
{
    partial class AddSysUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.companyIDtxt = new System.Windows.Forms.ComboBox();
            this.jobPositionIDtxt = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.departNametxt = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.concelBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Upload = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.cbx_qualityCerti = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.loginName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.loginPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.birthPlace = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.birthday = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gender = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.telephone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.userID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelMt = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.buttonMtGroup = new System.Windows.Forms.Button();
            this.buttonUser = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.upUserID = new System.Windows.Forms.TextBox();
            this.PurName = new System.Windows.Forms.TextBox();
            this.EvalCode = new System.Windows.Forms.ComboBox();
            this.labelMethodMtGroup = new System.Windows.Forms.Label();
            this.MethodMtGroup = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "所属采购部：";
            // 
            // companyIDtxt
            // 
            this.companyIDtxt.FormattingEnabled = true;
            this.companyIDtxt.Location = new System.Drawing.Point(92, 35);
            this.companyIDtxt.Name = "companyIDtxt";
            this.companyIDtxt.Size = new System.Drawing.Size(158, 20);
            this.companyIDtxt.TabIndex = 63;
            this.companyIDtxt.SelectedIndexChanged += new System.EventHandler(this.companyIDtxt_SelectedIndexChanged);
            // 
            // jobPositionIDtxt
            // 
            this.jobPositionIDtxt.FormattingEnabled = true;
            this.jobPositionIDtxt.Location = new System.Drawing.Point(409, 32);
            this.jobPositionIDtxt.Name = "jobPositionIDtxt";
            this.jobPositionIDtxt.Size = new System.Drawing.Size(158, 20);
            this.jobPositionIDtxt.TabIndex = 66;
            this.jobPositionIDtxt.SelectedIndexChanged += new System.EventHandler(this.jobPositionIDtxt_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(346, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 65;
            this.label8.Text = "职    务：";
            // 
            // departNametxt
            // 
            this.departNametxt.FormattingEnabled = true;
            this.departNametxt.Location = new System.Drawing.Point(92, 78);
            this.departNametxt.Name = "departNametxt";
            this.departNametxt.Size = new System.Drawing.Size(158, 20);
            this.departNametxt.TabIndex = 68;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 67;
            this.label9.Text = "所属部门：";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(165, 183);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(115, 23);
            this.saveBtn.TabIndex = 71;
            this.saveBtn.Text = "保  存";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // concelBtn
            // 
            this.concelBtn.Location = new System.Drawing.Point(341, 183);
            this.concelBtn.Name = "concelBtn";
            this.concelBtn.Size = new System.Drawing.Size(115, 23);
            this.concelBtn.TabIndex = 72;
            this.concelBtn.Text = "取  消";
            this.concelBtn.UseVisualStyleBackColor = true;
            this.concelBtn.Click += new System.EventHandler(this.concelBtn_Click);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_Upload);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.cbx_qualityCerti);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.loginName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.loginPassword);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.status);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.birthPlace);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.birthday);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.description);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.gender);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.telephone);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.userID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.username);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(36, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(651, 489);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本信息";
            // 
            // btn_Upload
            // 
            this.btn_Upload.Location = new System.Drawing.Point(165, 221);
            this.btn_Upload.Name = "btn_Upload";
            this.btn_Upload.Size = new System.Drawing.Size(75, 25);
            this.btn_Upload.TabIndex = 86;
            this.btn_Upload.Text = "上传文件";
            this.btn_Upload.UseVisualStyleBackColor = true;
            this.btn_Upload.Click += new System.EventHandler(this.btn_Upload_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.checkedListBox1);
            this.groupBox3.Location = new System.Drawing.Point(28, 262);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(252, 221);
            this.groupBox3.TabIndex = 85;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "证书信息";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(137, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 23);
            this.button1.TabIndex = 88;
            this.button1.Text = "上传";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(6, 22);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(240, 148);
            this.checkedListBox1.TabIndex = 87;
            // 
            // cbx_qualityCerti
            // 
            this.cbx_qualityCerti.FormattingEnabled = true;
            this.cbx_qualityCerti.Location = new System.Drawing.Point(91, 221);
            this.cbx_qualityCerti.Name = "cbx_qualityCerti";
            this.cbx_qualityCerti.Size = new System.Drawing.Size(60, 25);
            this.cbx_qualityCerti.TabIndex = 84;
            this.cbx_qualityCerti.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label33.Location = new System.Drawing.Point(25, 224);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 17);
            this.label33.TabIndex = 83;
            this.label33.Text = "资格类别：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label32.Location = new System.Drawing.Point(25, 224);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(0, 17);
            this.label32.TabIndex = 80;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(577, 74);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 17);
            this.label27.TabIndex = 79;
            this.label27.Text = "*";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(577, 182);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 17);
            this.label26.TabIndex = 78;
            this.label26.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(256, 185);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 17);
            this.label25.TabIndex = 77;
            this.label25.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(257, 110);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 17);
            this.label22.TabIndex = 76;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(257, 74);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 17);
            this.label21.TabIndex = 75;
            this.label21.Text = "*";
            // 
            // loginName
            // 
            this.loginName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.loginName.Location = new System.Drawing.Point(92, 70);
            this.loginName.Name = "loginName";
            this.loginName.Size = new System.Drawing.Size(164, 23);
            this.loginName.TabIndex = 73;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(25, 73);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 71;
            this.label1.Text = "登录名：";
            // 
            // loginPassword
            // 
            this.loginPassword.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.loginPassword.Location = new System.Drawing.Point(412, 71);
            this.loginPassword.Name = "loginPassword";
            this.loginPassword.Size = new System.Drawing.Size(164, 23);
            this.loginPassword.TabIndex = 74;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label14.Location = new System.Drawing.Point(345, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 17);
            this.label14.TabIndex = 72;
            this.label14.Text = "登录密码：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(257, 147);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 17);
            this.label20.TabIndex = 24;
            this.label20.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(577, 111);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 17);
            this.label18.TabIndex = 22;
            this.label18.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(577, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 17);
            this.label17.TabIndex = 21;
            this.label17.Text = "*";
            // 
            // status
            // 
            this.status.FormattingEnabled = true;
            this.status.Items.AddRange(new object[] {
            "在籍",
            "注销"});
            this.status.Location = new System.Drawing.Point(409, 138);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(167, 25);
            this.status.TabIndex = 70;
            this.status.Text = "在籍";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(257, 110);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 17);
            this.label16.TabIndex = 20;
            this.label16.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label10.Location = new System.Drawing.Point(346, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 17);
            this.label10.TabIndex = 69;
            this.label10.Text = "状  态：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(257, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 17);
            this.label15.TabIndex = 19;
            this.label15.Text = "*";
            // 
            // birthPlace
            // 
            this.birthPlace.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.birthPlace.Location = new System.Drawing.Point(92, 143);
            this.birthPlace.Name = "birthPlace";
            this.birthPlace.Size = new System.Drawing.Size(164, 23);
            this.birthPlace.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label13.Location = new System.Drawing.Point(25, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "籍贯：";
            // 
            // birthday
            // 
            this.birthday.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.birthday.Location = new System.Drawing.Point(412, 107);
            this.birthday.MaxDate = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
            this.birthday.Name = "birthday";
            this.birthday.Size = new System.Drawing.Size(164, 23);
            this.birthday.TabIndex = 4;
            this.birthday.Value = new System.DateTime(2018, 1, 20, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label12.Location = new System.Drawing.Point(345, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 15;
            this.label12.Text = "出生日期：";
            // 
            // description
            // 
            this.description.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.description.Location = new System.Drawing.Point(412, 221);
            this.description.Multiline = true;
            this.description.Name = "description";
            this.description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.description.Size = new System.Drawing.Size(164, 171);
            this.description.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label11.Location = new System.Drawing.Point(346, 221);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "备注：";
            // 
            // gender
            // 
            this.gender.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gender.FormattingEnabled = true;
            this.gender.Items.AddRange(new object[] {
            "男",
            "女"});
            this.gender.Location = new System.Drawing.Point(92, 106);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(164, 25);
            this.gender.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(25, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "性别：";
            // 
            // email
            // 
            this.email.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.email.Location = new System.Drawing.Point(411, 179);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(164, 23);
            this.email.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(345, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "邮    箱：";
            // 
            // telephone
            // 
            this.telephone.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.telephone.Location = new System.Drawing.Point(92, 179);
            this.telephone.Name = "telephone";
            this.telephone.Size = new System.Drawing.Size(164, 23);
            this.telephone.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.Location = new System.Drawing.Point(25, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "联系方式：";
            // 
            // userID
            // 
            this.userID.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userID.Location = new System.Drawing.Point(92, 34);
            this.userID.Name = "userID";
            this.userID.ReadOnly = true;
            this.userID.Size = new System.Drawing.Size(164, 23);
            this.userID.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(25, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "用户编号：";
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.username.Location = new System.Drawing.Point(412, 33);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(164, 23);
            this.username.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(345, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "用户姓名：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(256, 35);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 17);
            this.label19.TabIndex = 74;
            this.label19.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(257, 84);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 17);
            this.label23.TabIndex = 75;
            this.label23.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(577, 35);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 17);
            this.label24.TabIndex = 76;
            this.label24.Text = "*";
            // 
            // labelMt
            // 
            this.labelMt.AutoSize = true;
            this.labelMt.Location = new System.Drawing.Point(342, 79);
            this.labelMt.Name = "labelMt";
            this.labelMt.Size = new System.Drawing.Size(59, 12);
            this.labelMt.TabIndex = 77;
            this.labelMt.Text = "物 料 组:";
            this.labelMt.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(618, 373);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 17);
            this.label28.TabIndex = 79;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(8, 117);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 12);
            this.label29.TabIndex = 81;
            this.label29.Text = "直接上级：";
            // 
            // buttonMtGroup
            // 
            this.buttonMtGroup.Location = new System.Drawing.Point(407, 74);
            this.buttonMtGroup.Name = "buttonMtGroup";
            this.buttonMtGroup.Size = new System.Drawing.Size(160, 24);
            this.buttonMtGroup.TabIndex = 82;
            this.buttonMtGroup.Text = "选择物料组";
            this.buttonMtGroup.UseVisualStyleBackColor = true;
            this.buttonMtGroup.Visible = false;
            this.buttonMtGroup.Click += new System.EventHandler(this.buttonMtGroup_Click);
            // 
            // buttonUser
            // 
            this.buttonUser.Location = new System.Drawing.Point(91, 111);
            this.buttonUser.Name = "buttonUser";
            this.buttonUser.Size = new System.Drawing.Size(160, 24);
            this.buttonUser.TabIndex = 83;
            this.buttonUser.Text = "选择直接上级";
            this.buttonUser.UseVisualStyleBackColor = true;
            this.buttonUser.Click += new System.EventHandler(this.buttonUser_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(257, 120);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 17);
            this.label30.TabIndex = 84;
            this.label30.Text = "*";
            // 
            // upUserID
            // 
            this.upUserID.Location = new System.Drawing.Point(194, 141);
            this.upUserID.Name = "upUserID";
            this.upUserID.Size = new System.Drawing.Size(41, 21);
            this.upUserID.TabIndex = 85;
            this.upUserID.Visible = false;
            // 
            // PurName
            // 
            this.PurName.Location = new System.Drawing.Point(576, 74);
            this.PurName.Name = "PurName";
            this.PurName.Size = new System.Drawing.Size(33, 21);
            this.PurName.TabIndex = 86;
            this.PurName.Visible = false;
            // 
            // EvalCode
            // 
            this.EvalCode.FormattingEnabled = true;
            this.EvalCode.Location = new System.Drawing.Point(408, 76);
            this.EvalCode.Name = "EvalCode";
            this.EvalCode.Size = new System.Drawing.Size(158, 20);
            this.EvalCode.TabIndex = 87;
            this.EvalCode.Visible = false;
            // 
            // labelMethodMtGroup
            // 
            this.labelMethodMtGroup.AutoSize = true;
            this.labelMethodMtGroup.Location = new System.Drawing.Point(339, 120);
            this.labelMethodMtGroup.Name = "labelMethodMtGroup";
            this.labelMethodMtGroup.Size = new System.Drawing.Size(59, 12);
            this.labelMethodMtGroup.TabIndex = 88;
            this.labelMethodMtGroup.Text = "物 料 组:";
            this.labelMethodMtGroup.Visible = false;
            // 
            // MethodMtGroup
            // 
            this.MethodMtGroup.Location = new System.Drawing.Point(412, 112);
            this.MethodMtGroup.Name = "MethodMtGroup";
            this.MethodMtGroup.Size = new System.Drawing.Size(159, 23);
            this.MethodMtGroup.TabIndex = 89;
            this.MethodMtGroup.Text = "匹配物料组";
            this.MethodMtGroup.UseVisualStyleBackColor = true;
            this.MethodMtGroup.Visible = false;
            this.MethodMtGroup.Click += new System.EventHandler(this.MethodMtGroup_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.companyIDtxt);
            this.groupBox2.Controls.Add(this.MethodMtGroup);
            this.groupBox2.Controls.Add(this.labelMethodMtGroup);
            this.groupBox2.Controls.Add(this.concelBtn);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.saveBtn);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.upUserID);
            this.groupBox2.Controls.Add(this.EvalCode);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.departNametxt);
            this.groupBox2.Controls.Add(this.buttonUser);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.PurName);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.jobPositionIDtxt);
            this.groupBox2.Controls.Add(this.labelMt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.buttonMtGroup);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Location = new System.Drawing.Point(36, 507);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(651, 207);
            this.groupBox2.TabIndex = 90;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "部门信息";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialog1.InitialDirectory = "D:\\\\Patch";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(198, 192);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 23);
            this.button2.TabIndex = 89;
            this.button2.Text = "查看";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddSysUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(711, 726);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.groupBox1);
            this.Name = "AddSysUserForm";
            this.Text = "+";
            this.Load += new System.EventHandler(this.SysManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox companyIDtxt;
        private System.Windows.Forms.ComboBox jobPositionIDtxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox departNametxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button concelBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox birthPlace;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker birthday;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox gender;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox telephone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox userID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox loginName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox loginPassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelMt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        public System.Windows.Forms.Button buttonMtGroup;
        public System.Windows.Forms.Button buttonUser;
        private System.Windows.Forms.Label label30;
        public System.Windows.Forms.TextBox upUserID;
        public  System.Windows.Forms.TextBox PurName;
        private System.Windows.Forms.ComboBox EvalCode;
        private System.Windows.Forms.Label labelMethodMtGroup;
        public System.Windows.Forms.Button MethodMtGroup;
        private System.Windows.Forms.ComboBox cbx_qualityCerti;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_Upload;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}