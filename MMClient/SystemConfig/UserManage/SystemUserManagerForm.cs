﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using MMClient.SystemConfig.PermissionManage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SystemConfig.UserManage
{
    public partial class SystemUserManagerForm : DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        private AddSysUserForm addSysUserForm;
        SystemUserOrganicRelationshipBLL systemUserOrganicRelationshipBLL = new SystemUserOrganicRelationshipBLL();
        public SystemUserManagerForm()
        {
            init();
            InitializeComponent();
            fillSystemUserManagerTable();
        }
        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }
        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.SystemUserManager.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.SystemUserManager.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.SystemUserManager.CurrentRow.Index;
        }

        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillSystemUserManagerTable()
        {
            //查询当前页大小的记录
            userInforTable = systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfo(this.pageSize, 0,0);
            this.SystemUserManager.AutoGenerateColumns = false;
            this.SystemUserManager.DataSource = userInforTable;
            //记录总数
            this.sumSize = systemUserOrganicRelationshipBLL.calculateUserNumber();
        }

        private void ReseachByCondition_Click(object sender, EventArgs e)
        {
            SystemUserOrganicRelationshipModel researchModel = new SystemUserOrganicRelationshipModel();
            researchModel.compangClass = (this.companyClassQe.SelectedValue.ToString() == null)? "":this.companyClassQe.SelectedValue.ToString();
            researchModel.jobPosition = (this.RoleNameQe.SelectedValue == null) ? null: this.RoleNameQe.SelectedValue.ToString();
            researchModel.departName = (this.OrganicNameQe.SelectedValue == null) ? null : this.OrganicNameQe.Text;
            researchModel.sysUserName = this.UserNAmeQe.Text;
            userInforTable = systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfoByCondition(researchModel, this.pageSize, 0);
            this.SystemUserManager.AutoGenerateColumns = false;
            this.SystemUserManager.DataSource = userInforTable;
            //记录总数
            this.sumSize = systemUserOrganicRelationshipBLL.calculateUserNumber();
        }

        public void SysManager_Load(object sender, EventArgs e)
        {
            DataTable companyNameSQL = systemUserOrganicRelationshipBLL.getCompanyNameSQL();
            this.companyClassQe.DataSource = companyNameSQL;
            this.companyClassQe.DisplayMember = "name";
            this.companyClassQe.ValueMember = "id";
        }
        //重置
        private void ResetInfoList_Click(object sender, EventArgs e)
        {
            this.UserNAmeQe.Text = "";
            this.RoleNameQe.Text = "";
            this.OrganicNameQe.Text = "";
            fillSystemUserManagerTable();
        }
        /// <summary>
        ///  单个授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AuthoritedBtn_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if (currentIndex == -1)
            {
                return;
            }
            //被授权用户Model
            DataGridViewRow row = this.SystemUserManager.Rows[currentIndex];
            BaseUserModel baseUserModel = new BaseUserModel();
            baseUserModel.User_ID = Convert.ToString(row.Cells["sysUserID"].Value);
            FunctionLevelPermissionManageForm functionLevelPermissionManage = new FunctionLevelPermissionManageForm(baseUserModel, 1);
            functionLevelPermissionManage.ShowDialog(this);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            authorExplain author = null;
            if (author == null || author.IsDisposed)
            {
                author = new authorExplain(2);
            }
            author.Show();
        }
        /// <summary>
        /// add new user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addUser_Click(object sender, EventArgs e)
        {
            //弹出页面
            if (addSysUserForm == null || addSysUserForm.IsDisposed)
            {
                addSysUserForm = new AddSysUserForm("null", 1);
            }
            addSysUserForm.Show();
            fillSystemUserManagerTable();
        }
        /// <summary>
        /// edit user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editUser_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.SystemUserManager.Rows[currentIndex];
            String userId = Convert.ToString(row.Cells["sysUserID"].Value);
            //弹出页面
            if (addSysUserForm == null || addSysUserForm.IsDisposed)
            {
                addSysUserForm = new AddSysUserForm(userId, 2);
            }
            //addSysUserForm.initEditingUserinfor(userId);
            addSysUserForm.Show();
            fillSystemUserManagerTable();
        }
        /// <summary>
        /// del user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void delbtn_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.SystemUserManager.Rows[currentIndex];
            String userId = Convert.ToString(row.Cells["sysUserID"].Value);
            systemUserOrganicRelationshipBLL.deleteSysUserInfoByUserId(userId);
            fillSystemUserManagerTable();
        }
        /// <summary>
        /// 公司选择发生改变时职务（角色）、部门 发生改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyClassQe_SelectedIndexChanged(object sender, EventArgs e)
        {
            string comanyID = this.companyClassQe.SelectedValue.ToString();
            DataTable RoleNameSQL = systemUserOrganicRelationshipBLL.getRoleNameSQL(comanyID);
            if (RoleNameSQL != null)
            {
                this.RoleNameQe.DataSource = RoleNameSQL;
                this.RoleNameQe.DisplayMember = "name";
                this.RoleNameQe.ValueMember = "id";
            }
            DataTable OrganicNameSQL = systemUserOrganicRelationshipBLL.getOrganicNameSQL(comanyID);
            if(OrganicNameQe != null)
            {
                this.OrganicNameQe.DataSource = OrganicNameSQL;
                this.OrganicNameQe.DisplayMember = "name";
                this.OrganicNameQe.ValueMember = "id";
            }
        }

        private void SystemUserManager_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.SystemUserManager.Rows[currentIndex];
                String userID = Convert.ToString(row.Cells["sysUserID"].Value);
                String status = Convert.ToString(row.Cells["sysUserStatus"].Value);
                //处理
                if (this.SystemUserManager.Columns[e.ColumnIndex].Name == "操作")
                {
                    //改变状态并刷新值 1：禁用 0：在籍
                    int a = (status.Equals("在籍")) ? 1 : 0;
                    systemUserOrganicRelationshipBLL.delOrActivitiUser(userID, a);
                    MessageBox.Show((a == 1) ? "已注销" : "已激活");
                    fillSystemUserManagerTable();
                }
            }
        }

    }
}
