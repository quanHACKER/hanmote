﻿namespace MMClient.SystemConfig.UserManage
{
    partial class CompanyUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompanyUserForm));
            this.editUserInforBtnCompanyUser = new System.Windows.Forms.Button();
            this.jumpToPageNum = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.userTableCompanyUser = new System.Windows.Forms.DataGridView();
            this.User_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Login_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birth_Place = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Enabled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.authorizeBtnCompanyUser = new System.Windows.Forms.Button();
            this.createUserBtnCompanyUser = new System.Windows.Forms.Button();
            this.removeUserInforBtnCompanyUser = new System.Windows.Forms.Button();
            this.searchOnConditionBtnCompanyUser = new System.Windows.Forms.Button();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.Reset = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.footerPanel = new System.Windows.Forms.Panel();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMovePreviousPage = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextPage = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sumRecord = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.eachPageNum = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userTableCompanyUser)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.footerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // editUserInforBtnCompanyUser
            // 
            this.editUserInforBtnCompanyUser.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.editUserInforBtnCompanyUser.Location = new System.Drawing.Point(357, 13);
            this.editUserInforBtnCompanyUser.Name = "editUserInforBtnCompanyUser";
            this.editUserInforBtnCompanyUser.Size = new System.Drawing.Size(75, 25);
            this.editUserInforBtnCompanyUser.TabIndex = 1;
            this.editUserInforBtnCompanyUser.Text = "编 辑";
            this.editUserInforBtnCompanyUser.UseVisualStyleBackColor = true;
            this.editUserInforBtnCompanyUser.Click += new System.EventHandler(this.editUserInforBtnCompanyUser_Click);
            // 
            // jumpToPageNum
            // 
            this.jumpToPageNum.Name = "jumpToPageNum";
            this.jumpToPageNum.Size = new System.Drawing.Size(50, 25);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel4.Text = "页";
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.userTableCompanyUser);
            this.contentPanel.Location = new System.Drawing.Point(0, 67);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(785, 314);
            this.contentPanel.TabIndex = 1;
            // 
            // userTableCompanyUser
            // 
            this.userTableCompanyUser.AllowUserToAddRows = false;
            this.userTableCompanyUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userTableCompanyUser.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.userTableCompanyUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userTableCompanyUser.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.userTableCompanyUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userTableCompanyUser.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.User_ID,
            this.Login_Name,
            this.Username,
            this.Birth_Place,
            this.Gender,
            this.Mobile,
            this.Email,
            this.Enabled});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.userTableCompanyUser.DefaultCellStyle = dataGridViewCellStyle2;
            this.userTableCompanyUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userTableCompanyUser.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.userTableCompanyUser.GridColor = System.Drawing.SystemColors.ControlLight;
            this.userTableCompanyUser.Location = new System.Drawing.Point(0, 0);
            this.userTableCompanyUser.Name = "userTableCompanyUser";
            this.userTableCompanyUser.RowTemplate.Height = 23;
            this.userTableCompanyUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userTableCompanyUser.Size = new System.Drawing.Size(785, 314);
            this.userTableCompanyUser.TabIndex = 0;
            // 
            // User_ID
            // 
            this.User_ID.DataPropertyName = "User_ID";
            this.User_ID.HeaderText = "编号";
            this.User_ID.Name = "User_ID";
            // 
            // Login_Name
            // 
            this.Login_Name.DataPropertyName = "Login_Name";
            this.Login_Name.HeaderText = "登录名";
            this.Login_Name.Name = "Login_Name";
            // 
            // Username
            // 
            this.Username.DataPropertyName = "Username";
            this.Username.HeaderText = "姓名";
            this.Username.Name = "Username";
            // 
            // Birth_Place
            // 
            this.Birth_Place.DataPropertyName = "Birth_Place";
            this.Birth_Place.HeaderText = "籍贯";
            this.Birth_Place.Name = "Birth_Place";
            // 
            // Gender
            // 
            this.Gender.DataPropertyName = "Gender";
            this.Gender.HeaderText = "性别";
            this.Gender.Name = "Gender";
            // 
            // Mobile
            // 
            this.Mobile.DataPropertyName = "Mobile";
            this.Mobile.HeaderText = "联系方式";
            this.Mobile.Name = "Mobile";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "邮箱";
            this.Email.Name = "Email";
            // 
            // Enabled
            // 
            this.Enabled.DataPropertyName = "Enabled";
            this.Enabled.HeaderText = "是否有效";
            this.Enabled.Name = "Enabled";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(56, 22);
            this.toolStripLabel3.Text = "跳转到：";
            // 
            // authorizeBtnCompanyUser
            // 
            this.authorizeBtnCompanyUser.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.authorizeBtnCompanyUser.Location = new System.Drawing.Point(630, 32);
            this.authorizeBtnCompanyUser.Name = "authorizeBtnCompanyUser";
            this.authorizeBtnCompanyUser.Size = new System.Drawing.Size(75, 25);
            this.authorizeBtnCompanyUser.TabIndex = 4;
            this.authorizeBtnCompanyUser.Text = "授 权";
            this.authorizeBtnCompanyUser.UseVisualStyleBackColor = true;
            this.authorizeBtnCompanyUser.Click += new System.EventHandler(this.authorizeBtnCompanyUser_Click);
            // 
            // createUserBtnCompanyUser
            // 
            this.createUserBtnCompanyUser.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.createUserBtnCompanyUser.Location = new System.Drawing.Point(244, 13);
            this.createUserBtnCompanyUser.Name = "createUserBtnCompanyUser";
            this.createUserBtnCompanyUser.Size = new System.Drawing.Size(75, 25);
            this.createUserBtnCompanyUser.TabIndex = 3;
            this.createUserBtnCompanyUser.Text = "新 建";
            this.createUserBtnCompanyUser.UseVisualStyleBackColor = true;
            this.createUserBtnCompanyUser.Click += new System.EventHandler(this.createUserBtnCompanyUser_Click);
            // 
            // removeUserInforBtnCompanyUser
            // 
            this.removeUserInforBtnCompanyUser.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.removeUserInforBtnCompanyUser.Location = new System.Drawing.Point(472, 13);
            this.removeUserInforBtnCompanyUser.Name = "removeUserInforBtnCompanyUser";
            this.removeUserInforBtnCompanyUser.Size = new System.Drawing.Size(75, 25);
            this.removeUserInforBtnCompanyUser.TabIndex = 2;
            this.removeUserInforBtnCompanyUser.Text = "删 除";
            this.removeUserInforBtnCompanyUser.UseVisualStyleBackColor = true;
            this.removeUserInforBtnCompanyUser.Click += new System.EventHandler(this.removeUserInforBtnCompanyUser_Click);
            // 
            // searchOnConditionBtnCompanyUser
            // 
            this.searchOnConditionBtnCompanyUser.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.searchOnConditionBtnCompanyUser.Location = new System.Drawing.Point(33, 13);
            this.searchOnConditionBtnCompanyUser.Name = "searchOnConditionBtnCompanyUser";
            this.searchOnConditionBtnCompanyUser.Size = new System.Drawing.Size(75, 25);
            this.searchOnConditionBtnCompanyUser.TabIndex = 0;
            this.searchOnConditionBtnCompanyUser.Text = "信息筛选";
            this.searchOnConditionBtnCompanyUser.UseVisualStyleBackColor = true;
            this.searchOnConditionBtnCompanyUser.Click += new System.EventHandler(this.searchOnConditionBtnCompanyUser_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.Reset);
            this.headerPanel.Controls.Add(this.authorizeBtnCompanyUser);
            this.headerPanel.Controls.Add(this.createUserBtnCompanyUser);
            this.headerPanel.Controls.Add(this.removeUserInforBtnCompanyUser);
            this.headerPanel.Controls.Add(this.editUserInforBtnCompanyUser);
            this.headerPanel.Controls.Add(this.searchOnConditionBtnCompanyUser);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(785, 61);
            this.headerPanel.TabIndex = 0;
            // 
            // Reset
            // 
            this.Reset.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Reset.Location = new System.Drawing.Point(136, 13);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 25);
            this.Reset.TabIndex = 5;
            this.Reset.Text = "重 置";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.footerPanel);
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Controls.Add(this.headerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(785, 412);
            this.topPanel.TabIndex = 1;
            // 
            // footerPanel
            // 
            this.footerPanel.Controls.Add(this.bindingNavigator);
            this.footerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerPanel.Location = new System.Drawing.Point(0, 384);
            this.footerPanel.Name = "footerPanel";
            this.footerPanel.Size = new System.Drawing.Size(785, 28);
            this.footerPanel.TabIndex = 2;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMovePreviousPage,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveNextPage,
            this.bindingNavigatorSeparator2,
            this.sumRecord,
            this.toolStripLabel1,
            this.eachPageNum,
            this.toolStripLabel2,
            this.toolStripSeparator1,
            this.toolStripLabel3,
            this.jumpToPageNum,
            this.toolStripLabel4});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.bindingNavigatorMovePreviousPage;
            this.bindingNavigator.MoveLastItem = this.bindingNavigatorMoveNextPage;
            this.bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator.Size = new System.Drawing.Size(785, 25);
            this.bindingNavigator.TabIndex = 2;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 22);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorMovePreviousPage
            // 
            this.bindingNavigatorMovePreviousPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousPage.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousPage.Image")));
            this.bindingNavigatorMovePreviousPage.Name = "bindingNavigatorMovePreviousPage";
            this.bindingNavigatorMovePreviousPage.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousPage.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousPage.Text = "移到第一条记录";
            this.bindingNavigatorMovePreviousPage.ToolTipText = "移到前一页";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "移到上一条记录";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "移到下一条记录";
            // 
            // bindingNavigatorMoveNextPage
            // 
            this.bindingNavigatorMoveNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextPage.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextPage.Image")));
            this.bindingNavigatorMoveNextPage.Name = "bindingNavigatorMoveNextPage";
            this.bindingNavigatorMoveNextPage.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextPage.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextPage.Text = "移到最后一条记录";
            this.bindingNavigatorMoveNextPage.ToolTipText = "移到下一页";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sumRecord
            // 
            this.sumRecord.Name = "sumRecord";
            this.sumRecord.Size = new System.Drawing.Size(71, 22);
            this.sumRecord.Text = "共 0 条记录";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel1.Text = "/每页";
            // 
            // eachPageNum
            // 
            this.eachPageNum.Name = "eachPageNum";
            this.eachPageNum.Size = new System.Drawing.Size(40, 25);
            this.eachPageNum.Text = "20";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel2.Text = "条记录";
            // 
            // CompanyUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 412);
            this.Controls.Add(this.topPanel);
            this.Name = "CompanyUserForm";
            this.Text = "公司员工信息列表";
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userTableCompanyUser)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.footerPanel.ResumeLayout(false);
            this.footerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button editUserInforBtnCompanyUser;
        private System.Windows.Forms.ToolStripTextBox jumpToPageNum;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.DataGridView userTableCompanyUser;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.Button authorizeBtnCompanyUser;
        private System.Windows.Forms.Button createUserBtnCompanyUser;
        private System.Windows.Forms.Button removeUserInforBtnCompanyUser;
        private System.Windows.Forms.Button searchOnConditionBtnCompanyUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel footerPanel;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousPage;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextPage;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripLabel sumRecord;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox eachPageNum;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn User_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Login_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birth_Place;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Enabled;
        private System.Windows.Forms.Button Reset;
    }
}