﻿namespace MMClient.SystemConfig.UserManage
{
    partial class UserGroupManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("缺省组");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserGroupManageForm));
            this.topPanel = new System.Windows.Forms.Panel();
            this.userGroupBox = new System.Windows.Forms.GroupBox();
            this.removeUserFromGroupBtn = new System.Windows.Forms.Button();
            this.addUserToGroupBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.removeUserGroupBtn = new System.Windows.Forms.Button();
            this.editUserGroupBtn = new System.Windows.Forms.Button();
            this.authorizeUserGroupBtn = new System.Windows.Forms.Button();
            this.addUserGroup = new System.Windows.Forms.Button();
            this.userGroupTreeView = new System.Windows.Forms.TreeView();
            this.userGroupBoxIconList = new System.Windows.Forms.ImageList(this.components);
            this.topPanel.SuspendLayout();
            this.userGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.userGroupBox);
            this.topPanel.Controls.Add(this.exitBtn);
            this.topPanel.Controls.Add(this.groupBox2);
            this.topPanel.Controls.Add(this.userGroupTreeView);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(459, 360);
            this.topPanel.TabIndex = 0;
            // 
            // userGroupBox
            // 
            this.userGroupBox.Controls.Add(this.removeUserFromGroupBtn);
            this.userGroupBox.Controls.Add(this.addUserToGroupBtn);
            this.userGroupBox.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userGroupBox.Location = new System.Drawing.Point(324, 21);
            this.userGroupBox.Name = "userGroupBox";
            this.userGroupBox.Size = new System.Drawing.Size(119, 105);
            this.userGroupBox.TabIndex = 4;
            this.userGroupBox.TabStop = false;
            this.userGroupBox.Text = "用户设置";
            // 
            // removeUserFromGroupBtn
            // 
            this.removeUserFromGroupBtn.Location = new System.Drawing.Point(14, 66);
            this.removeUserFromGroupBtn.Name = "removeUserFromGroupBtn";
            this.removeUserFromGroupBtn.Size = new System.Drawing.Size(91, 23);
            this.removeUserFromGroupBtn.TabIndex = 3;
            this.removeUserFromGroupBtn.Text = "从组移除用户";
            this.removeUserFromGroupBtn.UseVisualStyleBackColor = true;
            this.removeUserFromGroupBtn.Click += new System.EventHandler(this.removeUserFromGroupBtn_Click);
            // 
            // addUserToGroupBtn
            // 
            this.addUserToGroupBtn.Location = new System.Drawing.Point(14, 31);
            this.addUserToGroupBtn.Name = "addUserToGroupBtn";
            this.addUserToGroupBtn.Size = new System.Drawing.Size(91, 23);
            this.addUserToGroupBtn.TabIndex = 2;
            this.addUserToGroupBtn.Text = "添加用户到组";
            this.addUserToGroupBtn.UseVisualStyleBackColor = true;
            this.addUserToGroupBtn.Click += new System.EventHandler(this.addUserToGroupBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.exitBtn.Location = new System.Drawing.Point(324, 326);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(119, 23);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "退 出";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.removeUserGroupBtn);
            this.groupBox2.Controls.Add(this.editUserGroupBtn);
            this.groupBox2.Controls.Add(this.authorizeUserGroupBtn);
            this.groupBox2.Controls.Add(this.addUserGroup);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(324, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(119, 156);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "用户组设置";
            // 
            // removeUserGroupBtn
            // 
            this.removeUserGroupBtn.Location = new System.Drawing.Point(23, 122);
            this.removeUserGroupBtn.Name = "removeUserGroupBtn";
            this.removeUserGroupBtn.Size = new System.Drawing.Size(75, 23);
            this.removeUserGroupBtn.TabIndex = 7;
            this.removeUserGroupBtn.Text = "删 除";
            this.removeUserGroupBtn.UseVisualStyleBackColor = true;
            this.removeUserGroupBtn.Click += new System.EventHandler(this.removeUserGroupBtn_Click);
            // 
            // editUserGroupBtn
            // 
            this.editUserGroupBtn.Location = new System.Drawing.Point(23, 88);
            this.editUserGroupBtn.Name = "editUserGroupBtn";
            this.editUserGroupBtn.Size = new System.Drawing.Size(75, 23);
            this.editUserGroupBtn.TabIndex = 6;
            this.editUserGroupBtn.Text = "修 改";
            this.editUserGroupBtn.UseVisualStyleBackColor = true;
            this.editUserGroupBtn.Click += new System.EventHandler(this.editUserGroupBtn_Click);
            // 
            // authorizeUserGroupBtn
            // 
            this.authorizeUserGroupBtn.Location = new System.Drawing.Point(23, 56);
            this.authorizeUserGroupBtn.Name = "authorizeUserGroupBtn";
            this.authorizeUserGroupBtn.Size = new System.Drawing.Size(75, 23);
            this.authorizeUserGroupBtn.TabIndex = 5;
            this.authorizeUserGroupBtn.Text = "授 权";
            this.authorizeUserGroupBtn.UseVisualStyleBackColor = true;
            this.authorizeUserGroupBtn.Click += new System.EventHandler(this.authorizeUserGroupBtn_Click);
            // 
            // addUserGroup
            // 
            this.addUserGroup.Location = new System.Drawing.Point(23, 25);
            this.addUserGroup.Name = "addUserGroup";
            this.addUserGroup.Size = new System.Drawing.Size(75, 23);
            this.addUserGroup.TabIndex = 4;
            this.addUserGroup.Text = "新 增";
            this.addUserGroup.UseVisualStyleBackColor = true;
            this.addUserGroup.Click += new System.EventHandler(this.addUserGroup_Click);
            // 
            // userGroupTreeView
            // 
            this.userGroupTreeView.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userGroupTreeView.ImageIndex = 0;
            this.userGroupTreeView.ImageList = this.userGroupBoxIconList;
            this.userGroupTreeView.Location = new System.Drawing.Point(8, 8);
            this.userGroupTreeView.Name = "userGroupTreeView";
            treeNode2.ImageIndex = 0;
            treeNode2.Name = "defaultGroup";
            treeNode2.Text = "缺省组";
            this.userGroupTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.userGroupTreeView.SelectedImageIndex = 0;
            this.userGroupTreeView.Size = new System.Drawing.Size(304, 344);
            this.userGroupTreeView.TabIndex = 0;
            this.userGroupTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.userGroupTreeView_AfterSelect);
            // 
            // userGroupBoxIconList
            // 
            this.userGroupBoxIconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("userGroupBoxIconList.ImageStream")));
            this.userGroupBoxIconList.TransparentColor = System.Drawing.Color.Transparent;
            this.userGroupBoxIconList.Images.SetKeyName(0, "groupIcon.png");
            this.userGroupBoxIconList.Images.SetKeyName(1, "userIcon.png");
            // 
            // UserGroupManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 360);
            this.Controls.Add(this.topPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(475, 398);
            this.MinimumSize = new System.Drawing.Size(475, 398);
            this.Name = "UserGroupManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "用户（组）管理";
            this.Load += new System.EventHandler(this.UserGroupManageForm_Load);
            this.topPanel.ResumeLayout(false);
            this.userGroupBox.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TreeView userGroupTreeView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button removeUserGroupBtn;
        private System.Windows.Forms.Button editUserGroupBtn;
        private System.Windows.Forms.Button authorizeUserGroupBtn;
        private System.Windows.Forms.Button addUserGroup;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.ImageList userGroupBoxIconList;
        private System.Windows.Forms.GroupBox userGroupBox;
        private System.Windows.Forms.Button removeUserFromGroupBtn;
        private System.Windows.Forms.Button addUserToGroupBtn;
    }
}