﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.SystemConfig.UserManage;
using Lib.Model.SystemConfig;
using Lib.Common.CommonUtils;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserListForSelectionForm : Form
    {
        DataTable userInforTable;
        Dictionary<String, String> selectedUserDict;

        public UserListForSelectionForm()
        {
            InitializeComponent();

            userInforTable = new DataTable();
            selectedUserDict = new Dictionary<string, string>();

            loadData();
        }

        /// <summary>
        /// 加载用户列表信息
        /// </summary>
        private void loadData()
        {
            UserListBLL userListBll = new UserListBLL();
            userInforTable = userListBll.getSysUserList(20, 0);
            this.userTable.AutoGenerateColumns = false;
            this.userTable.DataSource = userInforTable;
        }

        /// <summary>
        /// 设置选中情况
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        private void setSelectionDict(int rowIndex)
        {
            DataGridViewRow row = this.userTable.Rows[rowIndex];
            String userId = Convert.ToString(row.Cells["User_ID"].Value);
            String username = Convert.ToString(row.Cells["Username"].Value);
            bool selectState = Convert.ToBoolean(row.Cells["selection"].Value);
            if (selectedUserDict.ContainsKey(userId))
            {
                if (!selectState)   //取消选中的项，从Dictinoary中移除键值对
                {
                    this.selectedUserDict.Remove(userId);
                }
            }
            else
            {
                if (selectState)    //选中，将键值对添加到Dictionary中
                {
                    this.selectedUserDict.Add(userId, username);
                }
            }
        }

        /// <summary>
        /// 获取存放选中情况的Map
        /// </summary>
        /// <returns></returns>
        public Dictionary<String, String> getSelectedUserDict()
        {
            return this.selectedUserDict;
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// “选择”点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowsIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;
            if (rowsIndex < 0 || columnIndex != 0)
            {
                return;
            }

            DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)this.userTable.Rows[rowsIndex].Cells[columnIndex];
            if (cell != null)
            {
                //“选择”列没有进行初始化，所示第一次点击要先设置默认值
                if (cell.Value == null)
                {
                    cell.Value = false;
                }
                cell.Value = !(bool)cell.Value;

                setSelectionDict(rowsIndex);
            }
        }

    }
}
