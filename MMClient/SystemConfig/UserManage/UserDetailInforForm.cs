﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Model.SystemConfig;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserDetailInforForm : Form
    {
        private int operateFlag;                //操作标志：1-新建；2-编辑修改
        private int managerFlag;              //是否建立的是管理员
        private String Supplier_Id;
        private String Supplier_Name;
        private BaseUserModel editUserModel;    //当前被编辑用户信息的Model
        private CompanyUserModel companyUserModel;    //当前被编辑用户信息的Model
        
        public UserDetailInforForm(string supplier_Id, string supplier_Name, int operateFlag,int managerFlag=0)
        {
            InitializeComponent();
            this.Supplier_Id = supplier_Id;
            this.Supplier_Name = supplier_Name;
            this.operateFlag = operateFlag;
            if(operateFlag==1) this.Text = "新建员工信息";
            else this.Text = "编辑员工信息";
            this.managerFlag = managerFlag;
        }

        private void UserDetailInforForm_Load(object sender, EventArgs e)
        {
            //设置鼠标悬浮提示
            setToolTips();

            if (this.operateFlag == 1)
            {
                getNewUserId();
            }
        }

        /// <summary>
        /// 设置鼠标悬浮提示信息
        /// </summary>
        private void setToolTips()
        {
            this.userDetailInforToolTip.SetToolTip(this.userID,"系统自动生成ID，不可修改");
            this.userDetailInforToolTip.SetToolTip(this.username,"用户真实姓名");
            this.userDetailInforToolTip.SetToolTip(this.birthday, "用户出生日期");
            this.userDetailInforToolTip.SetToolTip(this.loginPassword,"密码长度限制为6~16位");
            this.userDetailInforToolTip.SetToolTip(this.question, "密保问题，用户密码找回");
            this.userDetailInforToolTip.SetToolTip(this.answer, "密保答案，用户密码找回");
        }

        /// <summary>
        /// 生成一个新的用户ID
        /// </summary>
        private void getNewUserId()
        {
            UserDetailInforBLL userDetailInfor = new UserDetailInforBLL();
            try
            {
                this.userID.Text = userDetailInfor.getValidUserID();
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        /// <summary>
        /// 编辑用户时初始化基本信息
        /// </summary>
        /// <param name="userID">用户编号</param>
        public void initEditingUserinfor(string userID,string username)
        {
            if (editUserModel == null)
            {
                editUserModel = new BaseUserModel();
            }
            editUserModel.User_ID = userID;
            editUserModel.Username = username;

            //查询用户基本信息
            UserDetailInforBLL userDetailInforBLL = new UserDetailInforBLL();
            userDetailInforBLL.queryUserInforById(editUserModel);

            //设置界面上的用户信息
            setUserInforOnPanel();
        }

        /// <summary>
        /// 初始化界面上的用户信息
        /// </summary>
        private void setUserInforOnPanel()
        {
            if (editUserModel == null)
            {
                return;
            }

            //界面填写的信息
            this.userID.Text = editUserModel.User_ID;
            this.username.Text = editUserModel.Username;
            this.gender.SelectedIndex = editUserModel.Gender;
            this.birthday.Value = Convert.ToDateTime(editUserModel.Birthday);
            this.birthPlace.Text = editUserModel.Birth_Place;
            this.idNumber.Text = editUserModel.ID_Number;
            this.telephone.Text = editUserModel.Mobile;
            this.email.Text = editUserModel.Email;
            this.description.Text = editUserModel.User_Description;
            this.loginName.Text = editUserModel.Login_Name;
            this.loginPassword.Text = editUserModel.Login_Password;
            this.question.Text = editUserModel.Question;
            this.answer.Text = editUserModel.Answer_Question;
            //类成员
            this.managerFlag = editUserModel.Manager_Flag;
            
        }

        /// <summary>
        /// 检查信息是否填写完整
        /// </summary>
        /// <returns></returns>
        private bool checkUserInformationIntegrity()
        {
            if (String.IsNullOrEmpty(this.userID.Text))
            {
                MessageUtil.ShowError("系统生成用户编号失败，请关闭窗口重试！");
                return false;
            }

            if (String.IsNullOrEmpty(this.username.Text))
            {
                MessageUtil.ShowWarning("用户姓名不能为空，请填写");
                this.username.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.gender.Text) || this.gender.SelectedIndex < 0)
            {
                MessageUtil.ShowWarning("用户性别不能为空，请选择！");
                this.gender.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.birthPlace.Text))
            {
                MessageUtil.ShowWarning("用户籍贯不能为空，请填写！");
                this.birthPlace.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.idNumber.Text))
            {
                MessageUtil.ShowWarning("用户身份证号不能为空，请填写！");
                this.idNumber.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.loginName.Text))
            {
                MessageUtil.ShowWarning("用户登录名不能为空，请填写！");
                this.loginName.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(this.loginPassword.Text))
            {
                MessageUtil.ShowWarning("用户登录密码不能为空，请填写！");
                this.loginPassword.Focus();
                return false;
            }

            if (this.loginPassword.Text.Length > 0 && this.loginPassword.Text.Length < 6)
            {
                MessageUtil.ShowWarning("用户登录密码必须为6~16个字符，请修改！");
                this.loginPassword.Focus();
                return false;
            }

            if (this.birthday.Value.CompareTo(DateTime.Now) >= 0)
            {
                MessageUtil.ShowWarning("用户出生日期不合法，请修改！");
                this.birthday.Focus();
                return false;
            }

            return true;
        }

        private void setBaseUserModel()
        {
            if (editUserModel == null)
            {
                editUserModel = new BaseUserModel();
            }

            //界面填写的信息
            BaseUserModel.Supplier_Name = this.Supplier_Name;
            BaseUserModel.Supplier_Id = this.Supplier_Id;
            editUserModel.User_ID = this.userID.Text;
            editUserModel.Username = this.username.Text;
            editUserModel.Gender = this.gender.SelectedIndex;
            editUserModel.Birthday = this.birthday.Value.ToString("yyyy-MM-dd");
            editUserModel.Birth_Place = this.birthPlace.Text;
            editUserModel.ID_Number = this.idNumber.Text;
            editUserModel.Mobile = this.telephone.Text;
            editUserModel.Email = this.email.Text;
            editUserModel.User_Description = this.description.Text;
            editUserModel.Login_Name = this.loginName.Text;
            editUserModel.Login_Password = this.loginPassword.Text;
            editUserModel.Question = this.question.Text;
            editUserModel.Answer_Question = this.answer.Text;
            //默认的数据
            if (this.operateFlag == 1)
            {
                editUserModel.Enabled = 1;
                editUserModel.Manager_Flag = this.managerFlag;
                editUserModel.Generate_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                editUserModel.Generate_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                editUserModel.Generate_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
            else
            {
                editUserModel.Modify_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                editUserModel.Modify_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                editUserModel.Modify_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
        }

        /// <summary>
        /// 获取填写的用户信息Model
        /// </summary>
        /// <returns></returns>
        public BaseUserModel getBaseUserModel()
        {
            return this.editUserModel;
        }

        #region 按钮点击
        /// <summary>
        /// 保存当前信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            //监测用户信息是否填写完整，不完整则退出
            if (!checkUserInformationIntegrity())
            {
                return;
            }

            
            //检验身份证号是否合法
            if (!validateIDNumber())
            {
                return;
            }

            //检验手机号是否合法
            if (!validateMobileNumber())
            {
                return;
            }

            //检验邮件地址是否合法
            if (!validateEmail())
            {
                return;
            }
             

            //提示是否保存信息
            if (MessageUtil.ShowOKCancelAndQuestion("确定保存用户信息？") != DialogResult.OK)
            {
                return;
            }

            UserDetailInforBLL userDetailInforBll = new UserDetailInforBLL();
            
            //将编辑的用户信息打包成Model
          //  setBaseUserModel();
            setCompanyUserModel();
            try
            {
                if (operateFlag == 1)
                {
                    //将用户信息插入数据库中
                    userDetailInforBll.insertNewUser(companyUserModel);
                }
                else if (operateFlag == 2)
                {
                    //将编辑后的用户信息插入数据库
                    userDetailInforBll.updateUserInfor(companyUserModel);
                }
            }
            catch (Exception)
            {
                MessageUtil.ShowError("插入或更新用户信息失败！");
                this.DialogResult = DialogResult.Cancel;
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void setCompanyUserModel()
        {
            if (companyUserModel == null)
            {
                companyUserModel = new CompanyUserModel();
            }

            //界面填写的信息
            CompanyUserModel.Supplier_Id = this.Supplier_Id;
            CompanyUserModel.Supplier_Name = this.Supplier_Name;
            companyUserModel.Username = this.username.Text;
            companyUserModel.User_ID = this.userID.Text;
            companyUserModel.Gender = this.gender.SelectedIndex;
            companyUserModel.Birthday = this.birthday.Value.ToString("yyyy-MM-dd");
            companyUserModel.Birth_Place = this.birthPlace.Text;
            companyUserModel.ID_Number = this.idNumber.Text;
            companyUserModel.Mobile = this.telephone.Text;
            companyUserModel.Email = this.email.Text;
            companyUserModel.User_Description = this.description.Text;
            companyUserModel.Login_Name = this.loginName.Text;
            companyUserModel.Login_Password = this.loginPassword.Text;
            companyUserModel.Question = this.question.Text;
            companyUserModel.Answer_Question = this.answer.Text;
            //默认的数据
            if (this.operateFlag == 1)
            {
                companyUserModel.Enabled = 1;
                CompanyUserModel.Manager_Flag = this.managerFlag;
                companyUserModel.Generate_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                companyUserModel.Generate_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                companyUserModel.Generate_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
            else
            {
                companyUserModel.Modify_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
                companyUserModel.Modify_User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;
                companyUserModel.Modify_Username = SingleUserInstance.getCurrentUserInstance().Username;
            }
        }

        /// <summary>
        /// 取消，退出保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        /// <summary>
        /// 登录密码切换显示明文、密文
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plainCipherSwitch_CheckedChanged(object sender, EventArgs e)
        {
            this.loginPassword.UseSystemPasswordChar = this.plainCipherSwitch.Checked;
        }

        /// <summary>
        /// 登录密码输入事件，限制密码长度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginPassword_TextChanged(object sender, EventArgs e)
        {
            if (this.loginPassword.Text.Length > 16)
            {
                MessageUtil.ShowWarning("密码长度超出限制范围，系统将自动删除尾部多余部分！");
                this.loginPassword.Text = this.loginPassword.Text.Substring(0, 16);
            }
        }

        /// <summary>
        /// 检验身份证号是否合法，只针对18位二代身份证
        /// </summary>
        /// <returns></returns>
        public bool validateIDNumber()
        {
            char[] checkCode = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };
            int[] weight = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
            String idNumber = this.idNumber.Text;
            
            //检验身份证号位数
            if (idNumber.Length != 18)
            {
                MessageUtil.ShowWarning("身份证位数不正确，请修改！");
                return false;
            }

            //检验出生年月日是否合法、和所设置年龄是否一致
            String birthdayInID = idNumber.Substring(6, 8);
            String birthdayBySet = this.birthday.Value.ToString("yyyyMMdd");
            if (!birthdayInID.Equals(birthdayBySet))
            {
                MessageUtil.ShowWarning("身份证号中年龄和出生日期栏年龄不一致，请修改！");
                return false;
            }
            

            //检验身份证号中性别和所选的性别是否一致(3位，偶数为女性，奇数为男性)
            int genderFlag = Convert.ToInt32(idNumber.Substring(14, 3))%2;
            int genderSelectIndex = this.gender.SelectedIndex;
            if(genderSelectIndex < 0)
            {
                MessageUtil.ShowWarning("请选择用户性别！");
                return false;
            }
            if(genderFlag == genderSelectIndex)
            {
                MessageUtil.ShowWarning("身份证号中性别标志和所选择性别不一致，请修改！");
                return false;
            }

            //加权求模，从checkCode中取出对应校验码和身份证最后一位比较
            char []inArray = idNumber.ToCharArray();
            long sum = 0;
            for (int i = 0; i <= 16; i++)
            {
                sum += (inArray[i] - '0') * weight[i];
            }
            if (checkCode[sum % 11] != inArray[17])
            {
                MessageUtil.ShowWarning("身份证号不正确，请修改！");
                return false ;
            }

            return true;
        }

        /// <summary>
        /// //校验手机号是否合法（手机号非必填，一旦填写就必须正确）
        /// </summary>
        /// <returns></returns>
        public bool validateMobileNumber()
        {
            String telecom = @"^1[3578][01379]\d{8}$";  //电信手机号正则
            String unicom = @"^1[34578][01256]\d{8}$";  //联通手机号正则
            String mobile = @"^(134[012345678]\d{7}|1[34578][012356789]\d{8})$";    //移动手机号正则

            Regex telecomRegex = new Regex(telecom);
            Regex unicomRegex = new Regex(unicom);
            Regex mobileRegex = new Regex(mobile);

            String phoneNumber = this.telephone.Text;
            if(!String.IsNullOrEmpty(phoneNumber))
            {
                if (telecomRegex.IsMatch(phoneNumber) ||
                    unicomRegex.IsMatch(phoneNumber) ||
                    mobileRegex.IsMatch(phoneNumber))
                {
                    return true;
                }
                else
                {
                    MessageUtil.ShowWarning("手机号码不合法，请修改！");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 校验邮箱是否合法
        /// </summary>
        /// <returns></returns>
        public bool validateEmail()
        {
            String emailText = this.email.Text;
            if(!String.IsNullOrEmpty(emailText))
            {
                String email = @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$";
                Regex emailRegex = new Regex(email);
                if(!emailRegex.IsMatch(emailText))
                {
                    MessageUtil.ShowWarning("邮箱地址不合法，请修改！");
                    return false;
                }
            }
           
            return true;
        }

    }
}
