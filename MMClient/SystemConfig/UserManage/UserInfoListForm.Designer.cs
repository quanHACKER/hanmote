﻿namespace MMClient.SystemConfig.UserManage
{
    partial class UserInfoListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.qwe = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDepart = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.UserSelectedTable = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JobPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserSelectedTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.qwe);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxDepart);
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(713, 56);
            this.panel1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(596, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "重置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(466, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // qwe
            // 
            this.qwe.AutoSize = true;
            this.qwe.Location = new System.Drawing.Point(26, 26);
            this.qwe.Name = "qwe";
            this.qwe.Size = new System.Drawing.Size(41, 12);
            this.qwe.TabIndex = 3;
            this.qwe.Text = "姓名：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 26);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "部门";
            // 
            // textBoxDepart
            // 
            this.textBoxDepart.Location = new System.Drawing.Point(280, 23);
            this.textBoxDepart.Name = "textBoxDepart";
            this.textBoxDepart.Size = new System.Drawing.Size(114, 21);
            this.textBoxDepart.TabIndex = 1;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(67, 23);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(111, 21);
            this.textBoxName.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.UserSelectedTable);
            this.panel2.Location = new System.Drawing.Point(-1, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(713, 186);
            this.panel2.TabIndex = 1;
            // 
            // UserSelectedTable
            // 
            this.UserSelectedTable.AllowUserToAddRows = false;
            this.UserSelectedTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.UserSelectedTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.UserSelectedTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.UserSelectedTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.UserSelectedTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UserSelectedTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.companyClass,
            this.sysUserID,
            this.sysUserName,
            this.JobPosition,
            this.PhoneNumber});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.UserSelectedTable.DefaultCellStyle = dataGridViewCellStyle4;
            this.UserSelectedTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.UserSelectedTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.UserSelectedTable.Location = new System.Drawing.Point(0, 0);
            this.UserSelectedTable.Name = "UserSelectedTable";
            this.UserSelectedTable.RowTemplate.Height = 23;
            this.UserSelectedTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.UserSelectedTable.Size = new System.Drawing.Size(710, 186);
            this.UserSelectedTable.TabIndex = 4;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(465, 274);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "确定";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(595, 274);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "退出";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "联系方式";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            // 
            // JobPosition
            // 
            this.JobPosition.DataPropertyName = "职务";
            this.JobPosition.HeaderText = "职      务";
            this.JobPosition.Name = "JobPosition";
            // 
            // sysUserName
            // 
            this.sysUserName.DataPropertyName = "姓名";
            this.sysUserName.HeaderText = "姓      名";
            this.sysUserName.Name = "sysUserName";
            // 
            // sysUserID
            // 
            this.sysUserID.DataPropertyName = "员工编号";
            this.sysUserID.HeaderText = "员工编号";
            this.sysUserID.Name = "sysUserID";
            // 
            // companyClass
            // 
            this.companyClass.DataPropertyName = "所属公司";
            this.companyClass.HeaderText = "所属公司";
            this.companyClass.Name = "companyClass";
            // 
            // UserInfoListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 309);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "UserInfoListForm";
            this.Text = "用户列表";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserSelectedTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label qwe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDepart;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView UserSelectedTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn JobPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
    }
}