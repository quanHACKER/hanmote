﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Model.SystemConfig;
using MMClient.SystemConfig.PermissionManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserRoleManageForm : Form
    {
        class RoleStruct
        {
            public String roleId;
            public String roleName;
            public int leftValue;
            public int rightValue;
            public int layer;
        }

        private TreeNode clickedNode;

        private int rowIndex;

        public UserRoleManageForm()
        {
            InitializeComponent();
            initUserTreeView();
        }

        /// <summary>
        /// 查找树根节点
        /// </summary>
        /// <param name="roleInforTable"></param>
        /// <returns></returns>
        private RoleStruct findRootStruct(DataTable roleInforTable)
        {
            RoleStruct rootNode = null;
            foreach (DataRow row in roleInforTable.Rows)
            {
                int layer = Convert.ToInt32(row["Level"]);
                if (layer == 0)
                {
                    rootNode = new RoleStruct();
                    rootNode.roleId = Convert.ToString(row["Role_ID"]);
                    rootNode.roleName = Convert.ToString(row["Role_Name"]);
                    rootNode.leftValue = Convert.ToInt32(row["Left_Value"]);
                    rootNode.rightValue = Convert.ToInt32(row["Right_Value"]);
                    rootNode.layer = Convert.ToInt32(row["Level"]);
                    break;
                }
            }
            return rootNode;
        }

        /// <summary>
        /// 查询某节点的孩子节点
        /// </summary>
        /// <param name="roleInforTable"></param>
        /// <param name="childCacheNodeList"></param>
        /// <param name="parentNode"></param>
        private void findChildStruct(DataTable roleInforTable,
                                                           LinkedList<RoleStruct> childCacheNodeList,
                                                           RoleStruct parentNode)
        {
            //清空孩子节点缓存队列
            childCacheNodeList.Clear();

            //遍历DataTable，查找parentNode的孩子节点
            foreach (DataRow row in roleInforTable.Rows)
            {
                int leftValue = Convert.ToInt32(row["Left_Value"]);
                int rightValue = Convert.ToInt32(row["Right_Value"]);
                int layer = Convert.ToInt32(row["Level"]);
                if (parentNode.layer == layer - 1 && parentNode.leftValue < leftValue && parentNode.rightValue > rightValue)
                {
                    bool addedFlag = false;

                    LinkedListNode<RoleStruct> firstNode = childCacheNodeList.First;
                    LinkedListNode<RoleStruct> lastNode = childCacheNodeList.Last;
                    LinkedListNode<RoleStruct> tmpNode = firstNode;

                    //childCacheNodeList按照Left_Value和Right_Value升序排序
                    while (tmpNode != lastNode)
                    {
                        RoleStruct nodeValue = tmpNode.Value;
                        if (leftValue < nodeValue.leftValue && rightValue < nodeValue.rightValue)
                        {
                            RoleStruct newNode = new RoleStruct();
                            newNode.roleId = Convert.ToString(row["Role_ID"]);
                            newNode.roleName = Convert.ToString(row["Role_Name"]);
                            newNode.leftValue = leftValue;
                            newNode.rightValue = rightValue;
                            newNode.layer = layer;
                            childCacheNodeList.AddBefore(tmpNode,newNode);
                            addedFlag = true;
                            break;
                        }
                        tmpNode = tmpNode.Next;
                    }
                    //判断末端节点
                    if (!addedFlag && tmpNode != null)
                    {
                        RoleStruct nodeValue = tmpNode.Value;
                        if (leftValue < nodeValue.leftValue && rightValue < nodeValue.rightValue)
                        {
                            RoleStruct newNode = new RoleStruct();
                            newNode.roleId = Convert.ToString(row["Role_ID"]);
                            newNode.roleName = Convert.ToString(row["Role_Name"]);
                            newNode.leftValue = leftValue;
                            newNode.rightValue = rightValue;
                            newNode.layer = layer;
                            childCacheNodeList.AddBefore(tmpNode, newNode);
                            addedFlag = true;
                        }
                    }
                    //前面都不满足则插入链表尾部
                    if (!addedFlag)
                    {
                        RoleStruct newNode = new RoleStruct();
                        newNode.roleId = Convert.ToString(row["Role_ID"]);
                        newNode.roleName = Convert.ToString(row["Role_Name"]);
                        newNode.leftValue = leftValue;
                        newNode.rightValue = rightValue;
                        newNode.layer = layer;
                        childCacheNodeList.AddLast(newNode);
                    }
                }
            }
        }

        /// <summary>
        /// 构建树并保存节点到队列
        /// </summary>
        /// <param name="tmpNode"></param>
        /// <param name="childCacheNodeList"></param>
        /// <param name="sumCacheNodeList"></param>
        private void insertAndSaveTreeNode(TreeNode parentNode,
                                                                            LinkedList<RoleStruct> childCacheNodeList,
                                                                            LinkedList<TreeNode> sumCacheNodeList)
        {
            foreach (RoleStruct roleStruct in childCacheNodeList)
            {
                TreeNode treeNode = createTreeNode(roleStruct);
                parentNode.Nodes.Add(treeNode);
                sumCacheNodeList.AddLast(treeNode);
            }
        }

        /// <summary>
        /// 初始化用户TreeView
        /// </summary>
        private void initUserTreeView()
        {
            //总节点缓存队列
            LinkedList<TreeNode> sumCacheNodeList = new LinkedList<TreeNode>();
            //某节点孩子节点缓存队列
            LinkedList<RoleStruct> childCacheNodeList = new LinkedList<RoleStruct>();

            try
            {
                UserRoleManageBLL roleManageBLL = new UserRoleManageBLL();
                DataTable roleInforTable = roleManageBLL.queryAllRoleInfor();
                if (roleInforTable == null || roleInforTable.Rows.Count <= 0)
                {
                    return;
                }

                //找到树根节点
                RoleStruct rootStruct = findRootStruct(roleInforTable);
                //treeView中插入根节点
                this.userRoleTreeView.Nodes.Add(createTreeNode(rootStruct));
                //获取TreeView根节点的TreeNode
                TreeNode rootNode = this.userRoleTreeView.Nodes[0];
                //插入根节点到总节点缓存队列
                sumCacheNodeList.AddLast(rootNode);

                while (sumCacheNodeList.Count > 0)
                {
                    //队头元素出队
                    TreeNode tmpNode = sumCacheNodeList.First.Value;
                    sumCacheNodeList.RemoveFirst();
                    //查找tmpNode的孩子节点
                    findChildStruct(roleInforTable, childCacheNodeList, (RoleStruct)tmpNode.Tag);
                    //构建树并保存节点到队列
                    insertAndSaveTreeNode(tmpNode,childCacheNodeList,sumCacheNodeList);
                }
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// 创建树节点
        /// </summary>
        /// <param name="nodeStruct"></param>
        /// <returns></returns>
        private TreeNode createTreeNode(RoleStruct nodeStruct)
        {
            TreeNode newNode = new TreeNode();
            newNode.Name = nodeStruct.roleId;
            newNode.Text = nodeStruct.roleName;
            newNode.Tag = nodeStruct;
            return newNode;
        }

        /// <summary>
        /// 创建树节点
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="roleName"></param>
        private TreeNode createTreeNode(String roleID,String roleName)
        {
            TreeNode newNode = new TreeNode();
            newNode.Name = roleID;
            newNode.Text = roleName;
            return newNode;
        }

        /// <summary>
        /// 插入新的角色信息
        /// </summary>
        /// <param name="userRoleID"></param>
        /// <param name="userRoleName"></param>
        /// <param name="userRoleDescription"></param>
        private void insertRole(String userRoleID,String userRoleName,String userRoleDescription)
        {
            UserRoleManageBLL roleManageBLL = new UserRoleManageBLL();
            BaseRoleModel roleModel = new BaseRoleModel();
            roleModel.Role_ID = userRoleID;
            roleModel.Role_Name = userRoleName;
            roleModel.Description = userRoleDescription;
            roleModel.Compang_ID = "2018012000001";
            if (clickedNode == null)//treeView为空，插入根节点
            {
                roleModel.Left_Value = 1;
                roleModel.Right_Value = 2;
                roleModel.Level = 0;
            }
            else//treeView不为空，插入clickedNode的子节点
            {
                BaseRoleModel parentRoleModel = new BaseRoleModel();
                parentRoleModel.Role_ID = this.clickedNode.Name;
                //查询父角色信息
                UserRoleDetailInforBLL userRoleInforBLL = new UserRoleDetailInforBLL();
                userRoleInforBLL.queryRoleInforById(parentRoleModel);

                roleModel.Left_Value = parentRoleModel.Right_Value;
                roleModel.Right_Value = parentRoleModel.Right_Value + 1;
                roleModel.Level = parentRoleModel.Level + 1;
            }

            try
            {
                //插入角色信息到数据库
                roleManageBLL.insertRole(roleModel);

                //在TreeView上建立插入的节点
                TreeNode newNode = createTreeNode(userRoleID,userRoleName);
                if (clickedNode == null)
                {
                    this.userRoleTreeView.Nodes.Add(newNode);
                }
                else
                {
                    this.clickedNode.Nodes.Add(newNode);
                }
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
            }
        }

        /// <summary>
        /// 刷新员工列表
        /// </summary>
        private void fillUserDataGridView()
        {
            if (this.clickedNode != null)
            {
                BaseRoleModel roleModel = new BaseRoleModel();
                roleModel.Role_ID = this.clickedNode.Name;

                UserRoleManageBLL roleManage = new UserRoleManageBLL();
                this.userDataGridView.AutoGenerateColumns = false;
                this.userDataGridView.DataSource = roleManage.queryUserInforByRoleId(roleModel);
            }
        }

        #region 菜单响应
        /// <summary>
        /// 新建角色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addRoleMenuItem_Click(object sender, EventArgs e)
        {
            UserRoleDetailInforForm roleDetail = new UserRoleDetailInforForm(0);
            DialogResult rest = roleDetail.ShowDialog(this);
            if (rest == DialogResult.OK)
            {
                String userRoleID = roleDetail.getUserRoleId();
                String userRoleName = roleDetail.getUserRoleName();
                String userRoleDescription = roleDetail.getUserDescription();
                insertRole(userRoleID,userRoleName,userRoleDescription);
            }
        }

        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editRoleMenuItem_Click(object sender, EventArgs e)
        {
            if (this.clickedNode != null)
            {
                UserRoleDetailInforForm roleDetailInfor = new UserRoleDetailInforForm(1,this.clickedNode.Name);
                DialogResult rest = roleDetailInfor.ShowDialog(this);
                if (rest == DialogResult.OK)
                {
                    BaseRoleModel roleModel = new BaseRoleModel();
                    roleModel.Role_ID = roleDetailInfor.getUserRoleId();
                    roleModel.Role_Name = roleDetailInfor.getUserRoleName();
                    roleModel.Description = roleDetailInfor.getUserDescription();

                    try
                    {
                        //更新数据库角色信息
                        UserRoleDetailInforBLL userRoleDetailInfor = new UserRoleDetailInforBLL();
                        userRoleDetailInfor.updateRoleInforById(roleModel);

                        //更新TreeView节点
                        this.clickedNode.Text = roleDetailInfor.getUserRoleName();
                    }
                    catch (Exception exp)
                    {
                        MessageUtil.ShowError(exp.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeRoleMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定删除当前角色及下属角色？");
            if (rest == DialogResult.OK)
            {
                try
                {
                    BaseRoleModel roleModel = new BaseRoleModel();
                    roleModel.Role_ID = this.clickedNode.Name;

                    //获取当前节点的左右值信息
                    UserRoleDetailInforBLL roleDetailBLL = new UserRoleDetailInforBLL();
                    roleDetailBLL.queryRoleInforById(roleModel);

                    //获取当前节点及所有子孙节点的编号
                    UserRoleManageBLL roleManageBLL = new UserRoleManageBLL();
                    DataTable offspringTable = roleManageBLL.queryOffspringInfor(roleModel);

                    //解除当前角色及子角色和员工的关联
                    JoinUserRoleModel userRoleModel = new JoinUserRoleModel();
                    foreach (DataRow row in offspringTable.Rows)
                    {
                        userRoleModel.Role_ID = Convert.ToString(row["Role_ID"]);
                        roleManageBLL.deleteUserRoleLinkByRoleId(userRoleModel);
                    }

                    //删除数据库中当前角色及所有子角色
                    //更新受影响的左右值
                    roleManageBLL.deleteOffspringOrganizationAndUpdateNodes(roleModel);

                    //从界面TreeView中删除选中节点及所有子节点
                    this.clickedNode.Nodes.Clear();
                    this.clickedNode.Remove();

                    //刷新员工列表
                    TreeNode selectedNode = this.userRoleTreeView.SelectedNode;
                    this.clickedNode = selectedNode != null ? selectedNode : this.clickedNode;
                    fillUserDataGridView();
                }
                catch (Exception exp)
                {
                    MessageUtil.ShowError(exp.Message);
                }
            }
        }

        /// <summary>
        /// 添加员工到角色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addStaffMenuItem_Click(object sender, EventArgs e)
        {
            UserListForSelectionForm userListForSelectionForm = new UserListForSelectionForm();
            DialogResult rest = userListForSelectionForm.ShowDialog(this);
            if (rest == DialogResult.OK)
            {
                Dictionary<String, String> userDict = userListForSelectionForm.getSelectedUserDict();
                if (userDict.Count > 0)
                {
                    try
                    {
                        //将选择的员工和当前角色建立联系
                        UserRoleManageBLL roleManage = new UserRoleManageBLL();
                        JoinUserRoleModel userRoleModel = new JoinUserRoleModel();
                        userRoleModel.Role_ID = this.clickedNode.Name;
                        userRoleModel.Role_Name = this.clickedNode.Text;
                        foreach (var item in userDict)
                        {
                            userRoleModel.User_ID = item.Key;
                            userRoleModel.Username = item.Value;
                            try
                            {
                                roleManage.insertUserRoleLink(userRoleModel);
                            }
                            catch(Exception exp)
                            {
                                Console.Write(exp.Message);
                            }
                        }

                        //刷新员工列表
                        fillUserDataGridView();
                    }
                    catch (Exception exp)
                    {
                        MessageUtil.ShowError(exp.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 给角色进行功能授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorizeRoleMenuItem_Click(object sender, EventArgs e)
        {
            if (this.clickedNode != null)
            {
                BaseRoleModel roleModel = new BaseRoleModel();
                roleModel.Role_ID = this.clickedNode.Name;
                roleModel.Role_Name = this.clickedNode.Text;
                FunctionLevelPermissionManageForm functionPermission = new FunctionLevelPermissionManageForm(roleModel,3);
                functionPermission.ShowDialog(this);
            }
        }

        /// <summary>
        /// 从角色中移除用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeUserMenuItem_Click(object sender, EventArgs e)
        {
            if (this.clickedNode != null && this.rowIndex >= 0)
            {
                DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定移除当前用户？");
                if (rest == DialogResult.OK)
                {
                    JoinUserRoleModel userRoleModel = new JoinUserRoleModel();
                    userRoleModel.Role_ID = this.clickedNode.Name;
                    userRoleModel.User_ID = Convert.ToString(this.userDataGridView.Rows[this.rowIndex].Cells[0].Value);
                    try
                    {
                        UserRoleManageBLL roleManageBLL = new UserRoleManageBLL();
                        roleManageBLL.removeRoleFromUser(userRoleModel);

                        fillUserDataGridView();
                    }
                    catch (Exception exp)
                    {
                        MessageUtil.ShowError(exp.Message);
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// TreeView为空，只显示新建菜单
        /// </summary>
        private void setMenuItemBlank()
        {
            this.addRoleMenuItem.Enabled = true;
            this.editRoleMenuItem.Enabled = false;
            this.removeRoleMenuItem.Enabled = false;
            this.addStaffMenuItem.Enabled = false;
            this.authorizeRoleMenuItem.Enabled = false;
        }

        /// <summary>
        /// TreeView不为空，点击节点时显示所有菜单
        /// </summary>
        private void setMenuItemNode()
        {
            this.addRoleMenuItem.Enabled = true;
            this.editRoleMenuItem.Enabled = true;
            this.removeRoleMenuItem.Enabled = true;
            this.addStaffMenuItem.Enabled = true;
            this.authorizeRoleMenuItem.Enabled = true;
        }

        /// <summary>
        /// 角色树右键事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userRoleTreeView_MouseUp(object sender, MouseEventArgs e)
        {
            //TreeView为空时，右键
            if (this.userRoleTreeView.Nodes.Count <= 0 && e.Button == MouseButtons.Right)
            {
                setMenuItemBlank();
                this.roleTreeViewMenu.Show(MousePosition.X, MousePosition.Y);
                return;
            }

            //TreeView不为空时，右键在某个节点上
            clickedNode = this.userRoleTreeView.GetNodeAt(e.X,e.Y);
            if (clickedNode != null)
            {
                this.userRoleTreeView.SelectedNode = clickedNode;
                if (e.Button == MouseButtons.Right)
                {
                    setMenuItemNode();
                    this.roleTreeViewMenu.Show(MousePosition.X,MousePosition.Y);
                }
                else if (e.Button == MouseButtons.Left)
                {
                    fillUserDataGridView();
                }
            }
        }

        /// <summary>
        /// 用户表右键事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex >= 0)
            {
                this.rowIndex = e.RowIndex;
                this.userDataGridViewMenu.Show(MousePosition.X,MousePosition.Y);
            }
        }

    }
}
