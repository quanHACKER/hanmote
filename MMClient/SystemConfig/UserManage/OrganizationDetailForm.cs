﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Model.SystemConfig;

namespace MMClient.SystemConfig.UserManage
{
    public partial class OrganizationDetailForm : Form
    {
        private int flag;   //0表示新建；1表示编辑

        public OrganizationDetailForm(int flag,String organizationID="")
        {
            InitializeComponent();

            this.flag = flag;
            if (flag == 0)
            {
                initOrganizationID();
            }
            else
            {
                initOrganizationInfor(organizationID);
            }
        }

        /// <summary>
        /// 初始化组织机构编号
        /// </summary>
        private void initOrganizationID()
        {
            OrganizationDetailBLL organizationBLL = new OrganizationDetailBLL();
            try
            {
                this.organizationId.Text = organizationBLL.getUserfulOrganizationID();
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// 初始化组织机构信息
        /// </summary>
        /// <param name="organizationID"></param>
        private void initOrganizationInfor(String organizationID)
        {
            OrganizationManageBLL organizationBLL = new OrganizationManageBLL();
            BaseOrganizationModel organizationModel = new BaseOrganizationModel();
            organizationModel.Organization_ID = organizationID;
            try
            {
                organizationBLL.queryOrganizationInfor(organizationModel);
                this.organizationId.Text = organizationID;
                this.organizationName.Text = organizationModel.Organization_Name;
                this.organizationRemarks.Text = organizationModel.Description;
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// 检查填写机构信息完整性
        /// </summary>
        /// <returns></returns>
        private bool checkIntegrity()
        {
            if (String.IsNullOrEmpty(this.organizationId.Text))
            {
                MessageUtil.ShowWarning("编号不能为空，无法提交！");
                return false;
            }

            if (String.IsNullOrEmpty(this.organizationName.Text))
            {
                MessageUtil.ShowWarning("名称不能为空，请填写！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            if (!checkIntegrity())
            {
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region 外部调用方法
        public String getOrganizationID()
        {
            return this.organizationId.Text;
        }

        public String getOrganizationName()
        {
            return this.organizationName.Text;
        }

        public String getOrganizationDescription()
        {
            return this.organizationRemarks.Text;
        }
        #endregion
    }
}
