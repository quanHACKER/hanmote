﻿namespace MMClient.SystemConfig.UserManage
{
    partial class OrganizationManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.topPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.userDataGridView = new System.Windows.Forms.DataGridView();
            this.userId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.organizationTreeView = new System.Windows.Forms.TreeView();
            this.organizationTreeViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStaffMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userDataGridViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.unbondMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).BeginInit();
            this.organizationTreeViewMenu.SuspendLayout();
            this.userDataGridViewMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label3);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.userDataGridView);
            this.topPanel.Controls.Add(this.organizationTreeView);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(927, 511);
            this.topPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(274, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = ">>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(298, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "员工信息";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(28, 17);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "组织机构";
            // 
            // userDataGridView
            // 
            this.userDataGridView.AllowUserToAddRows = false;
            this.userDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.userDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.userDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.userDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userId,
            this.userName,
            this.gender,
            this.birthDay,
            this.phoneNumber});
            this.userDataGridView.Location = new System.Drawing.Point(302, 40);
            this.userDataGridView.Name = "userDataGridView";
            this.userDataGridView.RowHeadersVisible = false;
            this.userDataGridView.RowTemplate.Height = 23;
            this.userDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userDataGridView.Size = new System.Drawing.Size(604, 457);
            this.userDataGridView.TabIndex = 1;
            this.userDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.userDataGridView_CellMouseClick);
            // 
            // userId
            // 
            this.userId.DataPropertyName = "User_ID";
            this.userId.HeaderText = "员工编号";
            this.userId.Name = "userId";
            // 
            // userName
            // 
            this.userName.DataPropertyName = "Username";
            this.userName.HeaderText = "姓名";
            this.userName.Name = "userName";
            // 
            // gender
            // 
            this.gender.DataPropertyName = "Gender";
            this.gender.HeaderText = "性别";
            this.gender.Name = "gender";
            // 
            // birthDay
            // 
            this.birthDay.DataPropertyName = "Birthday";
            this.birthDay.HeaderText = "出生日期";
            this.birthDay.Name = "birthDay";
            // 
            // phoneNumber
            // 
            this.phoneNumber.DataPropertyName = "Mobile";
            this.phoneNumber.HeaderText = "联系方式";
            this.phoneNumber.Name = "phoneNumber";
            // 
            // organizationTreeView
            // 
            this.organizationTreeView.Location = new System.Drawing.Point(30, 40);
            this.organizationTreeView.Name = "organizationTreeView";
            this.organizationTreeView.Size = new System.Drawing.Size(234, 457);
            this.organizationTreeView.TabIndex = 0;
            this.organizationTreeView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.organizationTreeView_MouseUp);
            // 
            // organizationTreeViewMenu
            // 
            this.organizationTreeViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createMenuItem,
            this.editMenuItem,
            this.deleteMenuItem,
            this.addStaffMenuItem});
            this.organizationTreeViewMenu.Name = "organizationTreeViewMenu";
            this.organizationTreeViewMenu.Size = new System.Drawing.Size(125, 92);
            // 
            // createMenuItem
            // 
            this.createMenuItem.Name = "createMenuItem";
            this.createMenuItem.Size = new System.Drawing.Size(124, 22);
            this.createMenuItem.Text = "新建";
            this.createMenuItem.Click += new System.EventHandler(this.createMenuItem_Click);
            // 
            // editMenuItem
            // 
            this.editMenuItem.Name = "editMenuItem";
            this.editMenuItem.Size = new System.Drawing.Size(124, 22);
            this.editMenuItem.Text = "编辑";
            this.editMenuItem.Click += new System.EventHandler(this.editMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(124, 22);
            this.deleteMenuItem.Text = "删除";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // addStaffMenuItem
            // 
            this.addStaffMenuItem.Name = "addStaffMenuItem";
            this.addStaffMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addStaffMenuItem.Text = "添加员工";
            this.addStaffMenuItem.Click += new System.EventHandler(this.addStaffMenuItem_Click);
            // 
            // userDataGridViewMenu
            // 
            this.userDataGridViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unbondMenuItem});
            this.userDataGridViewMenu.Name = "userDataGridViewMenu";
            this.userDataGridViewMenu.Size = new System.Drawing.Size(101, 26);
            // 
            // unbondMenuItem
            // 
            this.unbondMenuItem.Name = "unbondMenuItem";
            this.unbondMenuItem.Size = new System.Drawing.Size(100, 22);
            this.unbondMenuItem.Text = "移除";
            this.unbondMenuItem.Click += new System.EventHandler(this.unbondMenuItem_Click);
            // 
            // OrganizationManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 511);
            this.Controls.Add(this.topPanel);
            this.MaximumSize = new System.Drawing.Size(943, 549);
            this.MinimumSize = new System.Drawing.Size(943, 549);
            this.Name = "OrganizationManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "组织机构管理";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).EndInit();
            this.organizationTreeViewMenu.ResumeLayout(false);
            this.userDataGridViewMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TreeView organizationTreeView;
        private System.Windows.Forms.DataGridView userDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip organizationTreeViewMenu;
        private System.Windows.Forms.ToolStripMenuItem createMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStaffMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn userId;
        private System.Windows.Forms.DataGridViewTextBoxColumn userName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneNumber;
        private System.Windows.Forms.ContextMenuStrip userDataGridViewMenu;
        private System.Windows.Forms.ToolStripMenuItem unbondMenuItem;
    }
}