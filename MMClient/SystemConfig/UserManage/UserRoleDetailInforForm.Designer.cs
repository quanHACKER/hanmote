﻿namespace MMClient.SystemConfig.UserManage
{
    partial class UserRoleDetailInforForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.userRoleDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.userRoleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.userRoleId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cancelBtn
            // 
            this.cancelBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cancelBtn.Location = new System.Drawing.Point(394, 240);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 25);
            this.cancelBtn.TabIndex = 17;
            this.cancelBtn.Text = "取消";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.okBtn.Location = new System.Drawing.Point(295, 240);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 25);
            this.okBtn.TabIndex = 16;
            this.okBtn.Text = "确定";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // userRoleDescription
            // 
            this.userRoleDescription.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userRoleDescription.Location = new System.Drawing.Point(119, 99);
            this.userRoleDescription.Multiline = true;
            this.userRoleDescription.Name = "userRoleDescription";
            this.userRoleDescription.Size = new System.Drawing.Size(368, 122);
            this.userRoleDescription.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(46, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "角色描述：";
            // 
            // userRoleName
            // 
            this.userRoleName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userRoleName.Location = new System.Drawing.Point(119, 62);
            this.userRoleName.Name = "userRoleName";
            this.userRoleName.Size = new System.Drawing.Size(164, 23);
            this.userRoleName.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(46, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "角色名称：";
            // 
            // userRoleId
            // 
            this.userRoleId.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userRoleId.Location = new System.Drawing.Point(119, 26);
            this.userRoleId.Name = "userRoleId";
            this.userRoleId.ReadOnly = true;
            this.userRoleId.Size = new System.Drawing.Size(164, 23);
            this.userRoleId.TabIndex = 11;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(46, 28);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "角色编号：";
            // 
            // UserRoleDetailInforForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 291);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.userRoleDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.userRoleName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.userRoleId);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(552, 329);
            this.MinimumSize = new System.Drawing.Size(552, 329);
            this.Name = "UserRoleDetailInforForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "角色信息";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.TextBox userRoleDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox userRoleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox userRoleId;
        private System.Windows.Forms.Label label1;
    }
}