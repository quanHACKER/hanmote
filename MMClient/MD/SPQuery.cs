﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.MD
{
    public partial class SPQuery : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SPQuery()
        {
            InitializeComponent();
        }

        private void btn_查询_Click(object sender, EventArgs e)
        {
            string spid1 = cbb_供应商编码1.Text;
            string spid2 = cbb_供应商编码2.Text;
            string spname1 = cbb_供应商名称1.Text;
            string spname2 = cbb_供应商名称2.Text;
            string splevel1 = cbb_供应商级别1.Text;
            string splevel2 = cbb_供应商级别2.Text;
            string top = tb_最大命中数.Text;
            if (spid1 == "" || spid2 == "" || spname1 == "" || spname2 == "" || splevel1 == "" || splevel2 == "" || top == "")
                MessageBox.Show("请输入完整查询信息");
            else
            {
                string sql = "SELECT top " + top + " * FROM [MM].[dbo].[Supplier_Base] where (Supplier_ID='" + spid1 + "' or  Supplier_ID='" + spid2 + "')  and (Supplier_Name='" + spname1 + "' or  Supplier_Name='" + spname2 + "')";
                dgv_数据结果.DataSource = DBHelper.ExecuteQueryDT(sql);
            }
        }

        private void btn_清除_Click(object sender, EventArgs e)
        {
            cbb_供应商编码1.Text="";
            cbb_供应商编码2.Text="";
            cbb_供应商名称1.Text="";
            cbb_供应商名称2.Text="";
            cbb_供应商级别1.Text="";
            cbb_供应商级别2.Text="";
            tb_最大命中数.Text="";
        }

        private void dgv_数据结果_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        int rowindex = e.RowIndex;
            string s = "";
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();//产生一个新行  
            for (int i = 0; i < dgv_数据结果.Columns.Count; i++)
            {
                DataColumn dataColumn = new DataColumn();
                dataColumn.DataType = typeof(string);//数据类型  
                dataColumn.ColumnName = "列名" + i.ToString();//列名  
                dt.Columns.Add(dataColumn);//列添加到tempTable  
                s = dgv_数据结果.Rows[rowindex].Cells[i].Value.ToString();
                dr["列名" + i.ToString()] = s;
            }
            dt.Rows.Add(dr);//行添加到tempTable  
            this.dgv_详细信息.DataSource = dt;//绑定到dataGridView1
            //41251400
        }

        private void btn_数据分析_Click(object sender, EventArgs e)
        {
         string spid1 = cbb_供应商编码1.Text;
            string spid2 = cbb_供应商编码2.Text;
            string spname1 = cbb_供应商名称1.Text;
            string spname2 = cbb_供应商名称2.Text;
            string splevel1 = cbb_供应商级别1.Text;
            string splevel2 = cbb_供应商级别2.Text;
            string top = tb_最大命中数.Text;
            string sql = "SELECT top " + top + " * FROM [MM].[dbo].[Supplier_Base] where (Supplier_ID='" + spid1 + "' or  Supplier_ID='" +  spid2+ "')  and (Supplier_Name='" + spname1 + "' or  Supplier_Name='" + spname2 + "')";
            Console.Write(sql);
            DataTable dataTable1 = new System.Data.DataTable();
            dataTable1 = DBHelper.ExecuteQueryDT(sql);
            chart2.Series.Clear();
            Series dataTable1Series = new Series("dataTable1");
            dataTable1Series.Points.DataBind(dataTable1.AsEnumerable(), "供应商编码", "供应商名称", "");
            dataTable1Series.ChartType = SeriesChartType.Line; 
            chart2.Series.Add(dataTable1Series);
        }

       

      

        }

        
    }

