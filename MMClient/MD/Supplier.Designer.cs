﻿namespace MMClient.MD
{
    partial class Supplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl8 = new System.Windows.Forms.TabControl();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.panel17 = new System.Windows.Forms.Panel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.listBox46 = new System.Windows.Forms.ListBox();
            this.label85 = new System.Windows.Forms.Label();
            this.listBox47 = new System.Windows.Forms.ListBox();
            this.label86 = new System.Windows.Forms.Label();
            this.listBox48 = new System.Windows.Forms.ListBox();
            this.label87 = new System.Windows.Forms.Label();
            this.listBox49 = new System.Windows.Forms.ListBox();
            this.label88 = new System.Windows.Forms.Label();
            this.listBox50 = new System.Windows.Forms.ListBox();
            this.label89 = new System.Windows.Forms.Label();
            this.listBox51 = new System.Windows.Forms.ListBox();
            this.label90 = new System.Windows.Forms.Label();
            this.listBox52 = new System.Windows.Forms.ListBox();
            this.label91 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.panel19 = new System.Windows.Forms.Panel();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.listBox53 = new System.Windows.Forms.ListBox();
            this.label102 = new System.Windows.Forms.Label();
            this.listBox54 = new System.Windows.Forms.ListBox();
            this.label103 = new System.Windows.Forms.Label();
            this.listBox55 = new System.Windows.Forms.ListBox();
            this.label104 = new System.Windows.Forms.Label();
            this.listBox56 = new System.Windows.Forms.ListBox();
            this.label105 = new System.Windows.Forms.Label();
            this.listBox57 = new System.Windows.Forms.ListBox();
            this.label106 = new System.Windows.Forms.Label();
            this.listBox58 = new System.Windows.Forms.ListBox();
            this.label107 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.panel20 = new System.Windows.Forms.Panel();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.listBox59 = new System.Windows.Forms.ListBox();
            this.label109 = new System.Windows.Forms.Label();
            this.listBox60 = new System.Windows.Forms.ListBox();
            this.label110 = new System.Windows.Forms.Label();
            this.listBox61 = new System.Windows.Forms.ListBox();
            this.label111 = new System.Windows.Forms.Label();
            this.listBox62 = new System.Windows.Forms.ListBox();
            this.label112 = new System.Windows.Forms.Label();
            this.listBox63 = new System.Windows.Forms.ListBox();
            this.label113 = new System.Windows.Forms.Label();
            this.listBox64 = new System.Windows.Forms.ListBox();
            this.label114 = new System.Windows.Forms.Label();
            this.listBox65 = new System.Windows.Forms.ListBox();
            this.label115 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.listBox67 = new System.Windows.Forms.ListBox();
            this.label118 = new System.Windows.Forms.Label();
            this.listBox68 = new System.Windows.Forms.ListBox();
            this.label119 = new System.Windows.Forms.Label();
            this.listBox73 = new System.Windows.Forms.ListBox();
            this.label124 = new System.Windows.Forms.Label();
            this.listBox74 = new System.Windows.Forms.ListBox();
            this.label125 = new System.Windows.Forms.Label();
            this.listBox75 = new System.Windows.Forms.ListBox();
            this.label126 = new System.Windows.Forms.Label();
            this.listBox76 = new System.Windows.Forms.ListBox();
            this.label127 = new System.Windows.Forms.Label();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.panel21 = new System.Windows.Forms.Panel();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.listBox66 = new System.Windows.Forms.ListBox();
            this.label117 = new System.Windows.Forms.Label();
            this.listBox69 = new System.Windows.Forms.ListBox();
            this.label120 = new System.Windows.Forms.Label();
            this.listBox70 = new System.Windows.Forms.ListBox();
            this.label121 = new System.Windows.Forms.Label();
            this.listBox71 = new System.Windows.Forms.ListBox();
            this.label122 = new System.Windows.Forms.Label();
            this.listBox72 = new System.Windows.Forms.ListBox();
            this.label123 = new System.Windows.Forms.Label();
            this.tabControl3.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl8.SuspendLayout();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.panel17.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.panel19.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.panel20.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.panel21.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage12);
            this.tabControl3.Controls.Add(this.tabPage13);
            this.tabControl3.Controls.Add(this.tabPage14);
            this.tabControl3.Controls.Add(this.tabPage15);
            this.tabControl3.Controls.Add(this.tabPage16);
            this.tabControl3.Controls.Add(this.tabPage17);
            this.tabControl3.Location = new System.Drawing.Point(12, 3);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(916, 668);
            this.tabControl3.TabIndex = 1;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.groupBox15);
            this.tabPage12.Controls.Add(this.groupBox14);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Controls.Add(this.panel17);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(908, 642);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "查询";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.chart2);
            this.groupBox15.Location = new System.Drawing.Point(564, 238);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(341, 378);
            this.groupBox15.TabIndex = 6;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "数据分析";
            // 
            // chart2
            // 
            chartArea1.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart2.Legends.Add(legend1);
            this.chart2.Location = new System.Drawing.Point(6, 20);
            this.chart2.Name = "chart2";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(315, 352);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart2";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl8);
            this.groupBox14.Location = new System.Drawing.Point(3, 428);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(554, 192);
            this.groupBox14.TabIndex = 5;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "详细信息";
            // 
            // tabControl8
            // 
            this.tabControl8.Controls.Add(this.tabPage33);
            this.tabControl8.Controls.Add(this.tabPage34);
            this.tabControl8.Location = new System.Drawing.Point(1, 20);
            this.tabControl8.Name = "tabControl8";
            this.tabControl8.SelectedIndex = 0;
            this.tabControl8.Size = new System.Drawing.Size(590, 172);
            this.tabControl8.TabIndex = 0;
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this.dataGridView8);
            this.tabPage33.Location = new System.Drawing.Point(4, 22);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage33.Size = new System.Drawing.Size(582, 146);
            this.tabPage33.TabIndex = 0;
            this.tabPage33.Text = "详细记录";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // dataGridView8
            // 
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(1, 3);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.RowTemplate.Height = 23;
            this.dataGridView8.Size = new System.Drawing.Size(539, 137);
            this.dataGridView8.TabIndex = 0;
            // 
            // tabPage34
            // 
            this.tabPage34.Location = new System.Drawing.Point(4, 22);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage34.Size = new System.Drawing.Size(582, 146);
            this.tabPage34.TabIndex = 1;
            this.tabPage34.Text = "其他项";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.button27);
            this.panel18.Controls.Add(this.button28);
            this.panel18.Controls.Add(this.button29);
            this.panel18.Controls.Add(this.dataGridView7);
            this.panel18.Location = new System.Drawing.Point(3, 237);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(554, 185);
            this.panel18.TabIndex = 4;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(377, 159);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(75, 23);
            this.button27.TabIndex = 3;
            this.button27.Text = "导出数据";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(206, 159);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(75, 23);
            this.button28.TabIndex = 2;
            this.button28.Text = "数据分析";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(50, 159);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(75, 23);
            this.button29.TabIndex = 1;
            this.button29.Text = "删除";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // dataGridView7
            // 
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Location = new System.Drawing.Point(3, 4);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.RowTemplate.Height = 23;
            this.dataGridView7.Size = new System.Drawing.Size(548, 150);
            this.dataGridView7.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.groupBox13);
            this.panel17.Location = new System.Drawing.Point(3, 6);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(906, 225);
            this.panel17.TabIndex = 3;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBox26);
            this.groupBox13.Controls.Add(this.label84);
            this.groupBox13.Controls.Add(this.button25);
            this.groupBox13.Controls.Add(this.button26);
            this.groupBox13.Controls.Add(this.listBox46);
            this.groupBox13.Controls.Add(this.label85);
            this.groupBox13.Controls.Add(this.listBox47);
            this.groupBox13.Controls.Add(this.label86);
            this.groupBox13.Controls.Add(this.listBox48);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.listBox49);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.listBox50);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.listBox51);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this.listBox52);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Location = new System.Drawing.Point(7, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(631, 219);
            this.groupBox13.TabIndex = 18;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "条件选择";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(459, 170);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(117, 21);
            this.textBox26.TabIndex = 35;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(344, 170);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(65, 12);
            this.label84.TabIndex = 34;
            this.label84.Text = "最大命中数";
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(231, 170);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 33;
            this.button25.Text = "清除";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(96, 170);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 32;
            this.button26.Text = "查询";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // listBox46
            // 
            this.listBox46.FormattingEnabled = true;
            this.listBox46.ItemHeight = 12;
            this.listBox46.Location = new System.Drawing.Point(154, 148);
            this.listBox46.Name = "listBox46";
            this.listBox46.Size = new System.Drawing.Size(120, 16);
            this.listBox46.TabIndex = 31;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(40, 148);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(29, 12);
            this.label85.TabIndex = 30;
            this.label85.Text = "其他";
            // 
            // listBox47
            // 
            this.listBox47.FormattingEnabled = true;
            this.listBox47.ItemHeight = 12;
            this.listBox47.Location = new System.Drawing.Point(459, 105);
            this.listBox47.Name = "listBox47";
            this.listBox47.Size = new System.Drawing.Size(120, 16);
            this.listBox47.TabIndex = 29;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(345, 105);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(17, 12);
            this.label86.TabIndex = 28;
            this.label86.Text = "至";
            // 
            // listBox48
            // 
            this.listBox48.FormattingEnabled = true;
            this.listBox48.ItemHeight = 12;
            this.listBox48.Location = new System.Drawing.Point(154, 105);
            this.listBox48.Name = "listBox48";
            this.listBox48.Size = new System.Drawing.Size(120, 16);
            this.listBox48.TabIndex = 27;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(40, 105);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(65, 12);
            this.label87.TabIndex = 26;
            this.label87.Text = "供应商级别";
            // 
            // listBox49
            // 
            this.listBox49.FormattingEnabled = true;
            this.listBox49.ItemHeight = 12;
            this.listBox49.Location = new System.Drawing.Point(459, 60);
            this.listBox49.Name = "listBox49";
            this.listBox49.Size = new System.Drawing.Size(120, 16);
            this.listBox49.TabIndex = 25;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(345, 60);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(17, 12);
            this.label88.TabIndex = 24;
            this.label88.Text = "至";
            // 
            // listBox50
            // 
            this.listBox50.FormattingEnabled = true;
            this.listBox50.ItemHeight = 12;
            this.listBox50.Location = new System.Drawing.Point(154, 60);
            this.listBox50.Name = "listBox50";
            this.listBox50.Size = new System.Drawing.Size(120, 16);
            this.listBox50.TabIndex = 23;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(40, 60);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(65, 12);
            this.label89.TabIndex = 22;
            this.label89.Text = "供应商名称";
            // 
            // listBox51
            // 
            this.listBox51.FormattingEnabled = true;
            this.listBox51.ItemHeight = 12;
            this.listBox51.Location = new System.Drawing.Point(459, 20);
            this.listBox51.Name = "listBox51";
            this.listBox51.Size = new System.Drawing.Size(120, 16);
            this.listBox51.TabIndex = 21;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(345, 20);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(17, 12);
            this.label90.TabIndex = 20;
            this.label90.Text = "至";
            // 
            // listBox52
            // 
            this.listBox52.FormattingEnabled = true;
            this.listBox52.ItemHeight = 12;
            this.listBox52.Location = new System.Drawing.Point(154, 20);
            this.listBox52.Name = "listBox52";
            this.listBox52.Size = new System.Drawing.Size(120, 16);
            this.listBox52.TabIndex = 19;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(40, 20);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(65, 12);
            this.label91.TabIndex = 18;
            this.label91.Text = "供应商编码";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.groupBox16);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(908, 642);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "创建";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.button30);
            this.groupBox16.Controls.Add(this.button31);
            this.groupBox16.Controls.Add(this.textBox27);
            this.groupBox16.Controls.Add(this.label92);
            this.groupBox16.Controls.Add(this.textBox28);
            this.groupBox16.Controls.Add(this.label93);
            this.groupBox16.Controls.Add(this.textBox29);
            this.groupBox16.Controls.Add(this.label94);
            this.groupBox16.Controls.Add(this.textBox30);
            this.groupBox16.Controls.Add(this.label95);
            this.groupBox16.Controls.Add(this.textBox31);
            this.groupBox16.Controls.Add(this.label96);
            this.groupBox16.Controls.Add(this.textBox32);
            this.groupBox16.Controls.Add(this.label97);
            this.groupBox16.Controls.Add(this.textBox33);
            this.groupBox16.Controls.Add(this.label98);
            this.groupBox16.Controls.Add(this.textBox34);
            this.groupBox16.Controls.Add(this.label99);
            this.groupBox16.Controls.Add(this.textBox35);
            this.groupBox16.Controls.Add(this.label100);
            this.groupBox16.Controls.Add(this.textBox36);
            this.groupBox16.Controls.Add(this.label101);
            this.groupBox16.Location = new System.Drawing.Point(6, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(656, 619);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "创建新数据";
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(340, 556);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(75, 23);
            this.button30.TabIndex = 21;
            this.button30.Text = "重置";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(78, 556);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 20;
            this.button31.Text = "确定";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(409, 283);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(128, 21);
            this.textBox27.TabIndex = 19;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(338, 288);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(29, 12);
            this.label92.TabIndex = 18;
            this.label92.Text = "其他";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(78, 283);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(128, 21);
            this.textBox28.TabIndex = 17;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(7, 288);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(65, 12);
            this.label93.TabIndex = 16;
            this.label93.Text = "采购员编码";
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(409, 213);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(128, 21);
            this.textBox29.TabIndex = 15;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(338, 218);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(53, 12);
            this.label94.TabIndex = 14;
            this.label94.Text = "物料编码";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(78, 213);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(128, 21);
            this.textBox30.TabIndex = 13;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(7, 218);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(29, 12);
            this.label95.TabIndex = 12;
            this.label95.Text = "网址";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(409, 151);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(128, 21);
            this.textBox31.TabIndex = 11;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(338, 156);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(53, 12);
            this.label96.TabIndex = 10;
            this.label96.Text = "详细地址";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(78, 147);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(128, 21);
            this.textBox32.TabIndex = 9;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(7, 152);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(53, 12);
            this.label97.TabIndex = 8;
            this.label97.Text = "所在国家";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(409, 80);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(128, 21);
            this.textBox33.TabIndex = 7;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(338, 85);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 12);
            this.label98.TabIndex = 6;
            this.label98.Text = "企业性质";
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(77, 82);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(128, 21);
            this.textBox34.TabIndex = 5;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 87);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(77, 12);
            this.label99.TabIndex = 4;
            this.label99.Text = "供应商注册名";
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(409, 19);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(128, 21);
            this.textBox35.TabIndex = 3;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(338, 24);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(65, 12);
            this.label100.TabIndex = 2;
            this.label100.Text = "供应商名称";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(78, 21);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(128, 21);
            this.textBox36.TabIndex = 1;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(7, 26);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(65, 12);
            this.label101.TabIndex = 0;
            this.label101.Text = "供应商编码";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.panel19);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(908, 642);
            this.tabPage14.TabIndex = 2;
            this.tabPage14.Text = "修改";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.groupBox17);
            this.panel19.Location = new System.Drawing.Point(3, 6);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(651, 640);
            this.panel19.TabIndex = 1;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.listBox53);
            this.groupBox17.Controls.Add(this.label102);
            this.groupBox17.Controls.Add(this.listBox54);
            this.groupBox17.Controls.Add(this.label103);
            this.groupBox17.Controls.Add(this.listBox55);
            this.groupBox17.Controls.Add(this.label104);
            this.groupBox17.Controls.Add(this.listBox56);
            this.groupBox17.Controls.Add(this.label105);
            this.groupBox17.Controls.Add(this.listBox57);
            this.groupBox17.Controls.Add(this.label106);
            this.groupBox17.Controls.Add(this.listBox58);
            this.groupBox17.Controls.Add(this.label107);
            this.groupBox17.Controls.Add(this.button32);
            this.groupBox17.Controls.Add(this.button33);
            this.groupBox17.Location = new System.Drawing.Point(3, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(640, 616);
            this.groupBox17.TabIndex = 1;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "修改新数据";
            // 
            // listBox53
            // 
            this.listBox53.FormattingEnabled = true;
            this.listBox53.ItemHeight = 12;
            this.listBox53.Location = new System.Drawing.Point(436, 113);
            this.listBox53.Name = "listBox53";
            this.listBox53.Size = new System.Drawing.Size(120, 16);
            this.listBox53.TabIndex = 35;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(322, 113);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(41, 12);
            this.label102.TabIndex = 34;
            this.label102.Text = "修改至";
            // 
            // listBox54
            // 
            this.listBox54.FormattingEnabled = true;
            this.listBox54.ItemHeight = 12;
            this.listBox54.Location = new System.Drawing.Point(131, 113);
            this.listBox54.Name = "listBox54";
            this.listBox54.Size = new System.Drawing.Size(120, 16);
            this.listBox54.TabIndex = 33;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(17, 113);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(53, 12);
            this.label103.TabIndex = 32;
            this.label103.Text = "物料编码";
            // 
            // listBox55
            // 
            this.listBox55.FormattingEnabled = true;
            this.listBox55.ItemHeight = 12;
            this.listBox55.Location = new System.Drawing.Point(436, 68);
            this.listBox55.Name = "listBox55";
            this.listBox55.Size = new System.Drawing.Size(120, 16);
            this.listBox55.TabIndex = 31;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(322, 68);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(41, 12);
            this.label104.TabIndex = 30;
            this.label104.Text = "修改至";
            // 
            // listBox56
            // 
            this.listBox56.FormattingEnabled = true;
            this.listBox56.ItemHeight = 12;
            this.listBox56.Location = new System.Drawing.Point(131, 68);
            this.listBox56.Name = "listBox56";
            this.listBox56.Size = new System.Drawing.Size(120, 16);
            this.listBox56.TabIndex = 29;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(17, 68);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(65, 12);
            this.label105.TabIndex = 28;
            this.label105.Text = "供应商名称";
            // 
            // listBox57
            // 
            this.listBox57.FormattingEnabled = true;
            this.listBox57.ItemHeight = 12;
            this.listBox57.Location = new System.Drawing.Point(436, 28);
            this.listBox57.Name = "listBox57";
            this.listBox57.Size = new System.Drawing.Size(120, 16);
            this.listBox57.TabIndex = 27;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(322, 28);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(41, 12);
            this.label106.TabIndex = 26;
            this.label106.Text = "修改至";
            // 
            // listBox58
            // 
            this.listBox58.FormattingEnabled = true;
            this.listBox58.ItemHeight = 12;
            this.listBox58.Location = new System.Drawing.Point(131, 28);
            this.listBox58.Name = "listBox58";
            this.listBox58.Size = new System.Drawing.Size(120, 16);
            this.listBox58.TabIndex = 25;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(17, 28);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(65, 12);
            this.label107.TabIndex = 24;
            this.label107.Text = "供应商编码";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(392, 558);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(75, 23);
            this.button32.TabIndex = 23;
            this.button32.Text = "重置";
            this.button32.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(130, 558);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(75, 23);
            this.button33.TabIndex = 22;
            this.button33.Text = "确定";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.groupBox20);
            this.tabPage15.Controls.Add(this.groupBox19);
            this.tabPage15.Controls.Add(this.panel20);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(908, 642);
            this.tabPage15.TabIndex = 3;
            this.tabPage15.Text = "分发";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.dataGridView10);
            this.groupBox20.Location = new System.Drawing.Point(3, 471);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(634, 163);
            this.groupBox20.TabIndex = 6;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "分发的子系统或者单位";
            // 
            // dataGridView10
            // 
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Location = new System.Drawing.Point(0, 12);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.RowTemplate.Height = 23;
            this.dataGridView10.Size = new System.Drawing.Size(628, 150);
            this.dataGridView10.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dataGridView9);
            this.groupBox19.Location = new System.Drawing.Point(3, 234);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(636, 231);
            this.groupBox19.TabIndex = 5;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "查询结果";
            // 
            // dataGridView9
            // 
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(7, 21);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.RowTemplate.Height = 23;
            this.dataGridView9.Size = new System.Drawing.Size(623, 204);
            this.dataGridView9.TabIndex = 0;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.groupBox18);
            this.panel20.Location = new System.Drawing.Point(3, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(906, 225);
            this.panel20.TabIndex = 4;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.textBox37);
            this.groupBox18.Controls.Add(this.label108);
            this.groupBox18.Controls.Add(this.button34);
            this.groupBox18.Controls.Add(this.button35);
            this.groupBox18.Controls.Add(this.listBox59);
            this.groupBox18.Controls.Add(this.label109);
            this.groupBox18.Controls.Add(this.listBox60);
            this.groupBox18.Controls.Add(this.label110);
            this.groupBox18.Controls.Add(this.listBox61);
            this.groupBox18.Controls.Add(this.label111);
            this.groupBox18.Controls.Add(this.listBox62);
            this.groupBox18.Controls.Add(this.label112);
            this.groupBox18.Controls.Add(this.listBox63);
            this.groupBox18.Controls.Add(this.label113);
            this.groupBox18.Controls.Add(this.listBox64);
            this.groupBox18.Controls.Add(this.label114);
            this.groupBox18.Controls.Add(this.listBox65);
            this.groupBox18.Controls.Add(this.label115);
            this.groupBox18.Location = new System.Drawing.Point(7, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(631, 219);
            this.groupBox18.TabIndex = 18;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "条件选择";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(459, 170);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(117, 21);
            this.textBox37.TabIndex = 35;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(344, 170);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(65, 12);
            this.label108.TabIndex = 34;
            this.label108.Text = "最大命中数";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(231, 170);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(75, 23);
            this.button34.TabIndex = 33;
            this.button34.Text = "清除";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(96, 170);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(75, 23);
            this.button35.TabIndex = 32;
            this.button35.Text = "查询";
            this.button35.UseVisualStyleBackColor = true;
            // 
            // listBox59
            // 
            this.listBox59.FormattingEnabled = true;
            this.listBox59.ItemHeight = 12;
            this.listBox59.Location = new System.Drawing.Point(154, 148);
            this.listBox59.Name = "listBox59";
            this.listBox59.Size = new System.Drawing.Size(120, 16);
            this.listBox59.TabIndex = 31;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(40, 148);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(29, 12);
            this.label109.TabIndex = 30;
            this.label109.Text = "其他";
            // 
            // listBox60
            // 
            this.listBox60.FormattingEnabled = true;
            this.listBox60.ItemHeight = 12;
            this.listBox60.Location = new System.Drawing.Point(459, 105);
            this.listBox60.Name = "listBox60";
            this.listBox60.Size = new System.Drawing.Size(120, 16);
            this.listBox60.TabIndex = 29;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(345, 105);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(17, 12);
            this.label110.TabIndex = 28;
            this.label110.Text = "至";
            // 
            // listBox61
            // 
            this.listBox61.FormattingEnabled = true;
            this.listBox61.ItemHeight = 12;
            this.listBox61.Location = new System.Drawing.Point(154, 105);
            this.listBox61.Name = "listBox61";
            this.listBox61.Size = new System.Drawing.Size(120, 16);
            this.listBox61.TabIndex = 27;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(40, 105);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(65, 12);
            this.label111.TabIndex = 26;
            this.label111.Text = "供应商级别";
            // 
            // listBox62
            // 
            this.listBox62.FormattingEnabled = true;
            this.listBox62.ItemHeight = 12;
            this.listBox62.Location = new System.Drawing.Point(459, 60);
            this.listBox62.Name = "listBox62";
            this.listBox62.Size = new System.Drawing.Size(120, 16);
            this.listBox62.TabIndex = 25;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(345, 60);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(17, 12);
            this.label112.TabIndex = 24;
            this.label112.Text = "至";
            // 
            // listBox63
            // 
            this.listBox63.FormattingEnabled = true;
            this.listBox63.ItemHeight = 12;
            this.listBox63.Location = new System.Drawing.Point(154, 60);
            this.listBox63.Name = "listBox63";
            this.listBox63.Size = new System.Drawing.Size(120, 16);
            this.listBox63.TabIndex = 23;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(40, 60);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(65, 12);
            this.label113.TabIndex = 22;
            this.label113.Text = "供应商名称";
            // 
            // listBox64
            // 
            this.listBox64.FormattingEnabled = true;
            this.listBox64.ItemHeight = 12;
            this.listBox64.Location = new System.Drawing.Point(459, 20);
            this.listBox64.Name = "listBox64";
            this.listBox64.Size = new System.Drawing.Size(120, 16);
            this.listBox64.TabIndex = 21;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(345, 20);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(17, 12);
            this.label114.TabIndex = 20;
            this.label114.Text = "至";
            // 
            // listBox65
            // 
            this.listBox65.FormattingEnabled = true;
            this.listBox65.ItemHeight = 12;
            this.listBox65.Location = new System.Drawing.Point(154, 20);
            this.listBox65.Name = "listBox65";
            this.listBox65.Size = new System.Drawing.Size(120, 16);
            this.listBox65.TabIndex = 19;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(40, 20);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(65, 12);
            this.label115.TabIndex = 18;
            this.label115.Text = "供应商编码";
            // 
            // tabPage16
            // 
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(908, 642);
            this.tabPage16.TabIndex = 4;
            this.tabPage16.Text = "维护";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.groupBox23);
            this.tabPage17.Controls.Add(this.groupBox22);
            this.tabPage17.Controls.Add(this.panel21);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(908, 642);
            this.tabPage17.TabIndex = 5;
            this.tabPage17.Text = "制造商管理";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.listBox67);
            this.groupBox23.Controls.Add(this.label118);
            this.groupBox23.Controls.Add(this.listBox68);
            this.groupBox23.Controls.Add(this.label119);
            this.groupBox23.Controls.Add(this.listBox73);
            this.groupBox23.Controls.Add(this.label124);
            this.groupBox23.Controls.Add(this.listBox74);
            this.groupBox23.Controls.Add(this.label125);
            this.groupBox23.Controls.Add(this.listBox75);
            this.groupBox23.Controls.Add(this.label126);
            this.groupBox23.Controls.Add(this.listBox76);
            this.groupBox23.Controls.Add(this.label127);
            this.groupBox23.Controls.Add(this.button38);
            this.groupBox23.Controls.Add(this.button39);
            this.groupBox23.Location = new System.Drawing.Point(5, 471);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(622, 194);
            this.groupBox23.TabIndex = 7;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "修改查询结果";
            // 
            // listBox67
            // 
            this.listBox67.DisplayMember = "Material_Name";
            this.listBox67.FormattingEnabled = true;
            this.listBox67.ItemHeight = 12;
            this.listBox67.Location = new System.Drawing.Point(436, 113);
            this.listBox67.Name = "listBox67";
            this.listBox67.Size = new System.Drawing.Size(120, 16);
            this.listBox67.TabIndex = 35;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(322, 113);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(41, 12);
            this.label118.TabIndex = 34;
            this.label118.Text = "修改至";
            // 
            // listBox68
            // 
            this.listBox68.DisplayMember = "Material_Name";
            this.listBox68.FormattingEnabled = true;
            this.listBox68.ItemHeight = 12;
            this.listBox68.Location = new System.Drawing.Point(131, 113);
            this.listBox68.Name = "listBox68";
            this.listBox68.Size = new System.Drawing.Size(120, 16);
            this.listBox68.TabIndex = 33;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(17, 113);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(65, 12);
            this.label119.TabIndex = 32;
            this.label119.Text = "供应商编码";
            // 
            // listBox73
            // 
            this.listBox73.DisplayMember = "Material_Name";
            this.listBox73.FormattingEnabled = true;
            this.listBox73.ItemHeight = 12;
            this.listBox73.Location = new System.Drawing.Point(436, 68);
            this.listBox73.Name = "listBox73";
            this.listBox73.Size = new System.Drawing.Size(120, 16);
            this.listBox73.TabIndex = 31;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(322, 68);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(41, 12);
            this.label124.TabIndex = 30;
            this.label124.Text = "修改至";
            // 
            // listBox74
            // 
            this.listBox74.DisplayMember = "Material_Name";
            this.listBox74.FormattingEnabled = true;
            this.listBox74.ItemHeight = 12;
            this.listBox74.Location = new System.Drawing.Point(131, 68);
            this.listBox74.Name = "listBox74";
            this.listBox74.Size = new System.Drawing.Size(120, 16);
            this.listBox74.TabIndex = 29;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(17, 68);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(65, 12);
            this.label125.TabIndex = 28;
            this.label125.Text = "制造商名称";
            // 
            // listBox75
            // 
            this.listBox75.DisplayMember = "Material_Name";
            this.listBox75.FormattingEnabled = true;
            this.listBox75.ItemHeight = 12;
            this.listBox75.Location = new System.Drawing.Point(436, 28);
            this.listBox75.Name = "listBox75";
            this.listBox75.Size = new System.Drawing.Size(120, 16);
            this.listBox75.TabIndex = 27;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(322, 28);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(41, 12);
            this.label126.TabIndex = 26;
            this.label126.Text = "修改至";
            // 
            // listBox76
            // 
            this.listBox76.DisplayMember = "Material_Name";
            this.listBox76.FormattingEnabled = true;
            this.listBox76.ItemHeight = 12;
            this.listBox76.Location = new System.Drawing.Point(131, 28);
            this.listBox76.Name = "listBox76";
            this.listBox76.Size = new System.Drawing.Size(120, 16);
            this.listBox76.TabIndex = 25;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(17, 28);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(65, 12);
            this.label127.TabIndex = 24;
            this.label127.Text = "制造商编码";
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(382, 149);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(75, 23);
            this.button38.TabIndex = 23;
            this.button38.Text = "重置";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(165, 149);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(75, 23);
            this.button39.TabIndex = 22;
            this.button39.Text = "确定";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.dataGridView11);
            this.groupBox22.Location = new System.Drawing.Point(5, 234);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(636, 231);
            this.groupBox22.TabIndex = 6;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "查询结果";
            // 
            // dataGridView11
            // 
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Location = new System.Drawing.Point(7, 21);
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.RowTemplate.Height = 23;
            this.dataGridView11.Size = new System.Drawing.Size(623, 204);
            this.dataGridView11.TabIndex = 0;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.groupBox21);
            this.panel21.Location = new System.Drawing.Point(3, 3);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(906, 225);
            this.panel21.TabIndex = 5;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.textBox38);
            this.groupBox21.Controls.Add(this.label116);
            this.groupBox21.Controls.Add(this.button36);
            this.groupBox21.Controls.Add(this.button37);
            this.groupBox21.Controls.Add(this.listBox66);
            this.groupBox21.Controls.Add(this.label117);
            this.groupBox21.Controls.Add(this.listBox69);
            this.groupBox21.Controls.Add(this.label120);
            this.groupBox21.Controls.Add(this.listBox70);
            this.groupBox21.Controls.Add(this.label121);
            this.groupBox21.Controls.Add(this.listBox71);
            this.groupBox21.Controls.Add(this.label122);
            this.groupBox21.Controls.Add(this.listBox72);
            this.groupBox21.Controls.Add(this.label123);
            this.groupBox21.Location = new System.Drawing.Point(7, 3);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(631, 219);
            this.groupBox21.TabIndex = 18;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "条件选择";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(459, 170);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(117, 21);
            this.textBox38.TabIndex = 35;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(344, 170);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(65, 12);
            this.label116.TabIndex = 34;
            this.label116.Text = "最大命中数";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(231, 170);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(75, 23);
            this.button36.TabIndex = 33;
            this.button36.Text = "清除";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(96, 170);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(75, 23);
            this.button37.TabIndex = 32;
            this.button37.Text = "查询";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // listBox66
            // 
            this.listBox66.FormattingEnabled = true;
            this.listBox66.ItemHeight = 12;
            this.listBox66.Location = new System.Drawing.Point(154, 115);
            this.listBox66.Name = "listBox66";
            this.listBox66.Size = new System.Drawing.Size(120, 16);
            this.listBox66.TabIndex = 31;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(40, 115);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(29, 12);
            this.label117.TabIndex = 30;
            this.label117.Text = "其他";
            // 
            // listBox69
            // 
            this.listBox69.FormattingEnabled = true;
            this.listBox69.ItemHeight = 12;
            this.listBox69.Location = new System.Drawing.Point(459, 60);
            this.listBox69.Name = "listBox69";
            this.listBox69.Size = new System.Drawing.Size(120, 16);
            this.listBox69.TabIndex = 25;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(345, 60);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(17, 12);
            this.label120.TabIndex = 24;
            this.label120.Text = "至";
            // 
            // listBox70
            // 
            this.listBox70.FormattingEnabled = true;
            this.listBox70.ItemHeight = 12;
            this.listBox70.Location = new System.Drawing.Point(154, 60);
            this.listBox70.Name = "listBox70";
            this.listBox70.Size = new System.Drawing.Size(120, 16);
            this.listBox70.TabIndex = 23;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(40, 60);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(65, 12);
            this.label121.TabIndex = 22;
            this.label121.Text = "制造商名称";
            // 
            // listBox71
            // 
            this.listBox71.FormattingEnabled = true;
            this.listBox71.ItemHeight = 12;
            this.listBox71.Location = new System.Drawing.Point(459, 20);
            this.listBox71.Name = "listBox71";
            this.listBox71.Size = new System.Drawing.Size(120, 16);
            this.listBox71.TabIndex = 21;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(345, 20);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(17, 12);
            this.label122.TabIndex = 20;
            this.label122.Text = "至";
            // 
            // listBox72
            // 
            this.listBox72.FormattingEnabled = true;
            this.listBox72.ItemHeight = 12;
            this.listBox72.Location = new System.Drawing.Point(154, 20);
            this.listBox72.Name = "listBox72";
            this.listBox72.Size = new System.Drawing.Size(120, 16);
            this.listBox72.TabIndex = 19;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(40, 20);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(65, 12);
            this.label123.TabIndex = 18;
            this.label123.Text = "制造商编码";
            // 
            // MaterialCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 658);
            this.Controls.Add(this.tabControl3);
            this.Name = "MaterialCreate";
            this.Text = "MaterialCreate";
            this.tabControl3.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl8.ResumeLayout(false);
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.panel17.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.tabPage14.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.panel20.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.panel21.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TabControl tabControl8;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.ListBox listBox46;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.ListBox listBox47;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.ListBox listBox48;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ListBox listBox49;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.ListBox listBox50;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.ListBox listBox51;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ListBox listBox52;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.ListBox listBox53;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.ListBox listBox54;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.ListBox listBox55;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.ListBox listBox56;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.ListBox listBox57;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.ListBox listBox58;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.ListBox listBox59;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.ListBox listBox60;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.ListBox listBox61;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.ListBox listBox62;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ListBox listBox63;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.ListBox listBox64;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.ListBox listBox65;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.ListBox listBox67;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.ListBox listBox68;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.ListBox listBox73;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.ListBox listBox74;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.ListBox listBox75;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.ListBox listBox76;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.ListBox listBox66;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ListBox listBox69;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.ListBox listBox70;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.ListBox listBox71;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.ListBox listBox72;
        private System.Windows.Forms.Label label123;

    }
}