﻿namespace MMClient.MD
{
    partial class SPQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.cbb_供应商级别2 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商级别1 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商名称2 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商名称1 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商编码2 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商编码1 = new System.Windows.Forms.ComboBox();
            this.tb_最大命中数 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.button27 = new System.Windows.Forms.Button();
            this.btn_数据分析 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.dgv_数据结果 = new System.Windows.Forms.DataGridView();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl8 = new System.Windows.Forms.TabControl();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this.dgv_详细信息 = new System.Windows.Forms.DataGridView();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox13.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_数据结果)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl8.SuspendLayout();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_详细信息)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.cbb_供应商级别2);
            this.groupBox13.Controls.Add(this.cbb_供应商级别1);
            this.groupBox13.Controls.Add(this.cbb_供应商名称2);
            this.groupBox13.Controls.Add(this.cbb_供应商名称1);
            this.groupBox13.Controls.Add(this.cbb_供应商编码2);
            this.groupBox13.Controls.Add(this.cbb_供应商编码1);
            this.groupBox13.Controls.Add(this.tb_最大命中数);
            this.groupBox13.Controls.Add(this.label84);
            this.groupBox13.Controls.Add(this.btn_清除);
            this.groupBox13.Controls.Add(this.btn_查询);
            this.groupBox13.Controls.Add(this.label86);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Location = new System.Drawing.Point(16, 16);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox13.Size = new System.Drawing.Size(841, 292);
            this.groupBox13.TabIndex = 19;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "条件选择";
            // 
            // cbb_供应商级别2
            // 
            this.cbb_供应商级别2.FormattingEnabled = true;
            this.cbb_供应商级别2.Location = new System.Drawing.Point(612, 137);
            this.cbb_供应商级别2.Name = "cbb_供应商级别2";
            this.cbb_供应商级别2.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商级别2.TabIndex = 41;
            // 
            // cbb_供应商级别1
            // 
            this.cbb_供应商级别1.FormattingEnabled = true;
            this.cbb_供应商级别1.Location = new System.Drawing.Point(205, 133);
            this.cbb_供应商级别1.Name = "cbb_供应商级别1";
            this.cbb_供应商级别1.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商级别1.TabIndex = 40;
            // 
            // cbb_供应商名称2
            // 
            this.cbb_供应商名称2.FormattingEnabled = true;
            this.cbb_供应商名称2.Location = new System.Drawing.Point(612, 80);
            this.cbb_供应商名称2.Name = "cbb_供应商名称2";
            this.cbb_供应商名称2.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商名称2.TabIndex = 39;
            // 
            // cbb_供应商名称1
            // 
            this.cbb_供应商名称1.FormattingEnabled = true;
            this.cbb_供应商名称1.Location = new System.Drawing.Point(205, 80);
            this.cbb_供应商名称1.Name = "cbb_供应商名称1";
            this.cbb_供应商名称1.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商名称1.TabIndex = 38;
            // 
            // cbb_供应商编码2
            // 
            this.cbb_供应商编码2.FormattingEnabled = true;
            this.cbb_供应商编码2.Location = new System.Drawing.Point(612, 27);
            this.cbb_供应商编码2.Name = "cbb_供应商编码2";
            this.cbb_供应商编码2.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商编码2.TabIndex = 37;
            // 
            // cbb_供应商编码1
            // 
            this.cbb_供应商编码1.FormattingEnabled = true;
            this.cbb_供应商编码1.Location = new System.Drawing.Point(205, 27);
            this.cbb_供应商编码1.Name = "cbb_供应商编码1";
            this.cbb_供应商编码1.Size = new System.Drawing.Size(159, 24);
            this.cbb_供应商编码1.TabIndex = 36;
            // 
            // tb_最大命中数
            // 
            this.tb_最大命中数.Location = new System.Drawing.Point(612, 227);
            this.tb_最大命中数.Margin = new System.Windows.Forms.Padding(4);
            this.tb_最大命中数.Name = "tb_最大命中数";
            this.tb_最大命中数.Size = new System.Drawing.Size(155, 22);
            this.tb_最大命中数.TabIndex = 35;
           
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(459, 227);
            this.label84.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(78, 17);
            this.label84.TabIndex = 34;
            this.label84.Text = "最大命中数";
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(308, 227);
            this.btn_清除.Margin = new System.Windows.Forms.Padding(4);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(100, 31);
            this.btn_清除.TabIndex = 33;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            this.btn_清除.Click += new System.EventHandler(this.btn_清除_Click);
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(128, 227);
            this.btn_查询.Margin = new System.Windows.Forms.Padding(4);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(100, 31);
            this.btn_查询.TabIndex = 32;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            this.btn_查询.Click += new System.EventHandler(this.btn_查询_Click);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(460, 140);
            this.label86.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(22, 17);
            this.label86.TabIndex = 28;
            this.label86.Text = "至";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(53, 140);
            this.label87.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(78, 17);
            this.label87.TabIndex = 26;
            this.label87.Text = "供应商级别";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(460, 80);
            this.label88.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 17);
            this.label88.TabIndex = 24;
            this.label88.Text = "至";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(53, 80);
            this.label89.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(78, 17);
            this.label89.TabIndex = 22;
            this.label89.Text = "供应商名称";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(460, 27);
            this.label90.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 17);
            this.label90.TabIndex = 20;
            this.label90.Text = "至";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(53, 27);
            this.label91.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(78, 17);
            this.label91.TabIndex = 18;
            this.label91.Text = "供应商编码";
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.button27);
            this.panel18.Controls.Add(this.btn_数据分析);
            this.panel18.Controls.Add(this.button29);
            this.panel18.Controls.Add(this.dgv_数据结果);
            this.panel18.Location = new System.Drawing.Point(16, 316);
            this.panel18.Margin = new System.Windows.Forms.Padding(4);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(743, 247);
            this.panel18.TabIndex = 20;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(503, 212);
            this.button27.Margin = new System.Windows.Forms.Padding(4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(100, 31);
            this.button27.TabIndex = 3;
            this.button27.Text = "导出数据";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // btn_数据分析
            // 
            this.btn_数据分析.Location = new System.Drawing.Point(275, 212);
            this.btn_数据分析.Margin = new System.Windows.Forms.Padding(4);
            this.btn_数据分析.Name = "btn_数据分析";
            this.btn_数据分析.Size = new System.Drawing.Size(100, 31);
            this.btn_数据分析.TabIndex = 2;
            this.btn_数据分析.Text = "数据分析";
            this.btn_数据分析.UseVisualStyleBackColor = true;
            this.btn_数据分析.Click += new System.EventHandler(this.btn_数据分析_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(67, 212);
            this.button29.Margin = new System.Windows.Forms.Padding(4);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(100, 31);
            this.button29.TabIndex = 1;
            this.button29.Text = "删除";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // dgv_数据结果
            // 
            this.dgv_数据结果.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_数据结果.Location = new System.Drawing.Point(4, 5);
            this.dgv_数据结果.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_数据结果.Name = "dgv_数据结果";
            this.dgv_数据结果.RowTemplate.Height = 23;
            this.dgv_数据结果.Size = new System.Drawing.Size(731, 200);
            this.dgv_数据结果.TabIndex = 0;
            this.dgv_数据结果.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_数据结果_CellContentClick);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl8);
            this.groupBox14.Location = new System.Drawing.Point(20, 599);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox14.Size = new System.Drawing.Size(739, 256);
            this.groupBox14.TabIndex = 21;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "详细信息";
            // 
            // tabControl8
            // 
            this.tabControl8.Controls.Add(this.tabPage33);
            this.tabControl8.Controls.Add(this.tabPage34);
            this.tabControl8.Location = new System.Drawing.Point(1, 27);
            this.tabControl8.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl8.Name = "tabControl8";
            this.tabControl8.SelectedIndex = 0;
            this.tabControl8.Size = new System.Drawing.Size(729, 229);
            this.tabControl8.TabIndex = 0;
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this.dgv_详细信息);
            this.tabPage33.Location = new System.Drawing.Point(4, 25);
            this.tabPage33.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage33.Size = new System.Drawing.Size(721, 200);
            this.tabPage33.TabIndex = 0;
            this.tabPage33.Text = "详细记录";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // dgv_详细信息
            // 
            this.dgv_详细信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_详细信息.Location = new System.Drawing.Point(1, 4);
            this.dgv_详细信息.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_详细信息.Name = "dgv_详细信息";
            this.dgv_详细信息.RowTemplate.Height = 23;
            this.dgv_详细信息.Size = new System.Drawing.Size(719, 183);
            this.dgv_详细信息.TabIndex = 0;
            // 
            // tabPage34
            // 
            this.tabPage34.Location = new System.Drawing.Point(4, 25);
            this.tabPage34.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage34.Size = new System.Drawing.Size(721, 200);
            this.tabPage34.TabIndex = 1;
            this.tabPage34.Text = "其他项";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.chart2);
            this.groupBox15.Location = new System.Drawing.Point(796, 316);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox15.Size = new System.Drawing.Size(707, 545);
            this.groupBox15.TabIndex = 22;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "数据分析";
            // 
            // chart2
            // 
            chartArea1.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart2.Legends.Add(legend1);
            this.chart2.Location = new System.Drawing.Point(8, 27);
            this.chart2.Margin = new System.Windows.Forms.Padding(4);
            this.chart2.Name = "chart2";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(660, 499);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart2";
            // 
            // SPQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1519, 877);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.groupBox13);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SPQuery";
            this.Text = "SPQuery";
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_数据结果)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl8.ResumeLayout(false);
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_详细信息)).EndInit();
            this.groupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox tb_最大命中数;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button btn_数据分析;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.DataGridView dgv_数据结果;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TabControl tabControl8;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.DataGridView dgv_详细信息;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.ComboBox cbb_供应商级别2;
        private System.Windows.Forms.ComboBox cbb_供应商级别1;
        private System.Windows.Forms.ComboBox cbb_供应商名称2;
        private System.Windows.Forms.ComboBox cbb_供应商名称1;
        private System.Windows.Forms.ComboBox cbb_供应商编码2;
        private System.Windows.Forms.ComboBox cbb_供应商编码1;
    }
}