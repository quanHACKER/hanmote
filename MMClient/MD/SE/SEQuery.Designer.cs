﻿namespace MMClient.MD.SE
{
    partial class SEQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dgv_数据结果 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbb_专家编号1 = new System.Windows.Forms.ComboBox();
            this.cbb_专家编号2 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编号1 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编号2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_最大命中数 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_数据结果)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tb_最大命中数);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbb_物料编号2);
            this.groupBox1.Controls.Add(this.cbb_物料编号1);
            this.groupBox1.Controls.Add(this.cbb_专家编号2);
            this.groupBox1.Controls.Add(this.cbb_专家编号1);
            this.groupBox1.Controls.Add(this.btn_重置);
            this.groupBox1.Controls.Add(this.btn_查询);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(-7, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1036, 383);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件选择";
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(599, 341);
            this.btn_重置.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(100, 31);
            this.btn_重置.TabIndex = 15;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(447, 341);
            this.btn_查询.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(100, 31);
            this.btn_查询.TabIndex = 14;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            this.btn_查询.Click += new System.EventHandler(this.btn_查询_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(399, 124);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "至";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 124);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "物料编号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(399, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "至";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "专家编号";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.dgv_数据结果);
            this.groupBox2.Location = new System.Drawing.Point(8, 397);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(575, 269);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(420, 231);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 31);
            this.button5.TabIndex = 3;
            this.button5.Text = "导出数据";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(216, 231);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 31);
            this.button4.TabIndex = 2;
            this.button4.Text = "数据分析";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(15, 231);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 31);
            this.button3.TabIndex = 1;
            this.button3.Text = "删除";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dgv_数据结果
            // 
            this.dgv_数据结果.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_数据结果.Location = new System.Drawing.Point(-55, 23);
            this.dgv_数据结果.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgv_数据结果.Name = "dgv_数据结果";
            this.dgv_数据结果.RowTemplate.Height = 23;
            this.dgv_数据结果.Size = new System.Drawing.Size(621, 169);
            this.dgv_数据结果.TabIndex = 0;
            this.dgv_数据结果.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_数据结果_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(8, 675);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(569, 231);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "详细信息";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chart1);
            this.groupBox4.Location = new System.Drawing.Point(592, 397);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(453, 508);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "数据分析";
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(39, 60);
            this.chart1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(400, 400);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // cbb_专家编号1
            // 
            this.cbb_专家编号1.FormattingEnabled = true;
            this.cbb_专家编号1.Location = new System.Drawing.Point(144, 56);
            this.cbb_专家编号1.Name = "cbb_专家编号1";
            this.cbb_专家编号1.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家编号1.TabIndex = 16;
            // 
            // cbb_专家编号2
            // 
            this.cbb_专家编号2.FormattingEnabled = true;
            this.cbb_专家编号2.Location = new System.Drawing.Point(513, 56);
            this.cbb_专家编号2.Name = "cbb_专家编号2";
            this.cbb_专家编号2.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家编号2.TabIndex = 17;
            // 
            // cbb_物料编号1
            // 
            this.cbb_物料编号1.FormattingEnabled = true;
            this.cbb_物料编号1.Location = new System.Drawing.Point(144, 124);
            this.cbb_物料编号1.Name = "cbb_物料编号1";
            this.cbb_物料编号1.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编号1.TabIndex = 18;
            // 
            // cbb_物料编号2
            // 
            this.cbb_物料编号2.FormattingEnabled = true;
            this.cbb_物料编号2.Location = new System.Drawing.Point(513, 124);
            this.cbb_物料编号2.Name = "cbb_物料编号2";
            this.cbb_物料编号2.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编号2.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 193);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "最大命中数";
            // 
            // tb_最大命中数
            // 
            this.tb_最大命中数.Location = new System.Drawing.Point(144, 190);
            this.tb_最大命中数.Name = "tb_最大命中数";
            this.tb_最大命中数.Size = new System.Drawing.Size(87, 22);
            this.tb_最大命中数.TabIndex = 21;
            // 
            // SEQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 956);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SEQuery";
            this.Text = "SEQuery";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_数据结果)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dgv_数据结果;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ComboBox cbb_物料编号2;
        private System.Windows.Forms.ComboBox cbb_物料编号1;
        private System.Windows.Forms.ComboBox cbb_专家编号2;
        private System.Windows.Forms.ComboBox cbb_专家编号1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_最大命中数;
    }
}