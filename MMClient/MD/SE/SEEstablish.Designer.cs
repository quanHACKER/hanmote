﻿namespace MMClient.MD.SE
{
    partial class SEEstablish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_创建 = new System.Windows.Forms.Button();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.cbb_专家编码 = new System.Windows.Forms.ComboBox();
            this.cbb_专家分类 = new System.Windows.Forms.ComboBox();
            this.cbb_所在国家 = new System.Windows.Forms.ComboBox();
            this.cbb_邮箱地址 = new System.Windows.Forms.ComboBox();
            this.cbb_专家名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_所属单位 = new System.Windows.Forms.ComboBox();
            this.cbb_详细地址 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编码 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "专家编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(345, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "专家名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 123);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "专家分类";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(345, 123);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "所属单位";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 191);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "所在国家";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(345, 191);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "详细地址";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 267);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "邮箱地址";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(345, 267);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "物料编码";
            // 
            // btn_创建
            // 
            this.btn_创建.Location = new System.Drawing.Point(621, 400);
            this.btn_创建.Margin = new System.Windows.Forms.Padding(4);
            this.btn_创建.Name = "btn_创建";
            this.btn_创建.Size = new System.Drawing.Size(100, 31);
            this.btn_创建.TabIndex = 16;
            this.btn_创建.Text = "创建";
            this.btn_创建.UseVisualStyleBackColor = true;
            this.btn_创建.Click += new System.EventHandler(this.btn_创建_Click);
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(773, 400);
            this.btn_重置.Margin = new System.Windows.Forms.Padding(4);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(100, 31);
            this.btn_重置.TabIndex = 17;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // cbb_专家编码
            // 
            this.cbb_专家编码.FormattingEnabled = true;
            this.cbb_专家编码.Location = new System.Drawing.Point(141, 44);
            this.cbb_专家编码.Name = "cbb_专家编码";
            this.cbb_专家编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家编码.TabIndex = 18;
            // 
            // cbb_专家分类
            // 
            this.cbb_专家分类.FormattingEnabled = true;
            this.cbb_专家分类.Location = new System.Drawing.Point(141, 120);
            this.cbb_专家分类.Name = "cbb_专家分类";
            this.cbb_专家分类.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家分类.TabIndex = 19;
            // 
            // cbb_所在国家
            // 
            this.cbb_所在国家.FormattingEnabled = true;
            this.cbb_所在国家.Location = new System.Drawing.Point(141, 191);
            this.cbb_所在国家.Name = "cbb_所在国家";
            this.cbb_所在国家.Size = new System.Drawing.Size(121, 24);
            this.cbb_所在国家.TabIndex = 20;
            // 
            // cbb_邮箱地址
            // 
            this.cbb_邮箱地址.FormattingEnabled = true;
            this.cbb_邮箱地址.Location = new System.Drawing.Point(141, 264);
            this.cbb_邮箱地址.Name = "cbb_邮箱地址";
            this.cbb_邮箱地址.Size = new System.Drawing.Size(121, 24);
            this.cbb_邮箱地址.TabIndex = 21;
            // 
            // cbb_专家名称
            // 
            this.cbb_专家名称.FormattingEnabled = true;
            this.cbb_专家名称.Location = new System.Drawing.Point(531, 44);
            this.cbb_专家名称.Name = "cbb_专家名称";
            this.cbb_专家名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家名称.TabIndex = 22;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_物料编码);
            this.groupBox1.Controls.Add(this.cbb_详细地址);
            this.groupBox1.Controls.Add(this.cbb_所属单位);
            this.groupBox1.Controls.Add(this.cbb_专家名称);
            this.groupBox1.Controls.Add(this.cbb_邮箱地址);
            this.groupBox1.Controls.Add(this.cbb_所在国家);
            this.groupBox1.Controls.Add(this.cbb_专家分类);
            this.groupBox1.Controls.Add(this.cbb_专家编码);
            this.groupBox1.Controls.Add(this.btn_重置);
            this.groupBox1.Controls.Add(this.btn_创建);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(5, 17);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1024, 461);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "`";
            // 
            // cbb_所属单位
            // 
            this.cbb_所属单位.FormattingEnabled = true;
            this.cbb_所属单位.Location = new System.Drawing.Point(531, 120);
            this.cbb_所属单位.Name = "cbb_所属单位";
            this.cbb_所属单位.Size = new System.Drawing.Size(121, 24);
            this.cbb_所属单位.TabIndex = 23;
            // 
            // cbb_详细地址
            // 
            this.cbb_详细地址.FormattingEnabled = true;
            this.cbb_详细地址.Location = new System.Drawing.Point(531, 191);
            this.cbb_详细地址.Name = "cbb_详细地址";
            this.cbb_详细地址.Size = new System.Drawing.Size(121, 24);
            this.cbb_详细地址.TabIndex = 24;
            // 
            // cbb_物料编码
            // 
            this.cbb_物料编码.FormattingEnabled = true;
            this.cbb_物料编码.Location = new System.Drawing.Point(531, 264);
            this.cbb_物料编码.Name = "cbb_物料编码";
            this.cbb_物料编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码.TabIndex = 25;
            // 
            // SEEstablish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 495);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SEEstablish";
            this.Text = "SEEstablish";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_创建;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.ComboBox cbb_专家编码;
        private System.Windows.Forms.ComboBox cbb_专家分类;
        private System.Windows.Forms.ComboBox cbb_所在国家;
        private System.Windows.Forms.ComboBox cbb_邮箱地址;
        private System.Windows.Forms.ComboBox cbb_专家名称;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_物料编码;
        private System.Windows.Forms.ComboBox cbb_详细地址;
        private System.Windows.Forms.ComboBox cbb_所属单位;

    }
}