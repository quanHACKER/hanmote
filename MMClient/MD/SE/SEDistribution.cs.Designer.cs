﻿namespace MMClient.MD.SE
{
    partial class SEDistribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_物料编码2 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编码1 = new System.Windows.Forms.ComboBox();
            this.cbb_分类2 = new System.Windows.Forms.ComboBox();
            this.cbb_分类1 = new System.Windows.Forms.ComboBox();
            this.cbb_专家编码2 = new System.Windows.Forms.ComboBox();
            this.cbb_专家编码1 = new System.Windows.Forms.ComboBox();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_物料编码2);
            this.groupBox1.Controls.Add(this.cbb_物料编码1);
            this.groupBox1.Controls.Add(this.cbb_分类2);
            this.groupBox1.Controls.Add(this.cbb_分类1);
            this.groupBox1.Controls.Add(this.cbb_专家编码2);
            this.groupBox1.Controls.Add(this.cbb_专家编码1);
            this.groupBox1.Controls.Add(this.btn_清除);
            this.groupBox1.Controls.Add(this.btn_查询);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1031, 297);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件选择";
            // 
            // cbb_物料编码2
            // 
            this.cbb_物料编码2.FormattingEnabled = true;
            this.cbb_物料编码2.Location = new System.Drawing.Point(548, 185);
            this.cbb_物料编码2.Name = "cbb_物料编码2";
            this.cbb_物料编码2.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码2.TabIndex = 19;
            // 
            // cbb_物料编码1
            // 
            this.cbb_物料编码1.FormattingEnabled = true;
            this.cbb_物料编码1.Location = new System.Drawing.Point(153, 185);
            this.cbb_物料编码1.Name = "cbb_物料编码1";
            this.cbb_物料编码1.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码1.TabIndex = 18;
            // 
            // cbb_分类2
            // 
            this.cbb_分类2.FormattingEnabled = true;
            this.cbb_分类2.Location = new System.Drawing.Point(548, 123);
            this.cbb_分类2.Name = "cbb_分类2";
            this.cbb_分类2.Size = new System.Drawing.Size(121, 24);
            this.cbb_分类2.TabIndex = 17;
            // 
            // cbb_分类1
            // 
            this.cbb_分类1.FormattingEnabled = true;
            this.cbb_分类1.Location = new System.Drawing.Point(153, 123);
            this.cbb_分类1.Name = "cbb_分类1";
            this.cbb_分类1.Size = new System.Drawing.Size(121, 24);
            this.cbb_分类1.TabIndex = 16;
            // 
            // cbb_专家编码2
            // 
            this.cbb_专家编码2.FormattingEnabled = true;
            this.cbb_专家编码2.Location = new System.Drawing.Point(548, 53);
            this.cbb_专家编码2.Name = "cbb_专家编码2";
            this.cbb_专家编码2.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家编码2.TabIndex = 15;
            // 
            // cbb_专家编码1
            // 
            this.cbb_专家编码1.FormattingEnabled = true;
            this.cbb_专家编码1.Location = new System.Drawing.Point(153, 50);
            this.cbb_专家编码1.Name = "cbb_专家编码1";
            this.cbb_专家编码1.Size = new System.Drawing.Size(121, 24);
            this.cbb_专家编码1.TabIndex = 14;
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(671, 257);
            this.btn_清除.Margin = new System.Windows.Forms.Padding(4);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(100, 31);
            this.btn_清除.TabIndex = 13;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            this.btn_清除.Click += new System.EventHandler(this.btn_清除_Click);
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(527, 257);
            this.btn_查询.Margin = new System.Windows.Forms.Padding(4);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(100, 31);
            this.btn_查询.TabIndex = 12;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(404, 185);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "至";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 185);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "物料编码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(404, 123);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "至";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 123);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "分类";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "至";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "专家编码";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(5, 303);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1028, 231);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查询结果";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 23);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1013, 209);
            this.dataGridView1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2);
            this.groupBox3.Location = new System.Drawing.Point(7, 547);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(1017, 316);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "分发的子系统或单位";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 25);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(997, 283);
            this.dataGridView2.TabIndex = 0;
            // 
            // SEDistribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 879);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SEDistribution";
            this.Text = "SEDistribution";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ComboBox cbb_物料编码2;
        private System.Windows.Forms.ComboBox cbb_物料编码1;
        private System.Windows.Forms.ComboBox cbb_分类2;
        private System.Windows.Forms.ComboBox cbb_分类1;
        private System.Windows.Forms.ComboBox cbb_专家编码2;
        private System.Windows.Forms.ComboBox cbb_专家编码1;
    }
}