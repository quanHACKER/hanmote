﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.SqlClient;
using Lib.SqlServerDAL;


namespace MMClient.MD.SE
{
    public partial class SEEstablish : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SEEstablish()
        {
            InitializeComponent();
        }

        private void btn_创建_Click(object sender, EventArgs e)
        {
            string workunit = cbb_所属单位.Text;
            string nation = cbb_所在国家.Text;
            string mtid = cbb_物料编码.Text;
            string address = cbb_详细地址.Text;
            string mail = cbb_邮箱地址.Text;
            string seid = cbb_专家编码.Text;
            string seclassify = cbb_专家分类.Text;
            string sename = cbb_专家名称.Text;
            string sql = "INSERT INTO [MM].[dbo].[Specialist](Source,Nation,Material_ID,Address,Mail,Specialist_ID,Specialist_Classify,Specialist_Name)VALUES('" + workunit + "','" + nation + "','" + mtid + "','" + address + "','" + mail + "','" + seid + "','" + seclassify + "', '" + sename + "')";
            DBHelper.ExecuteNonQuery(sql);
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {
            cbb_所属单位.Text = "";
            cbb_所在国家.Text = "";
            cbb_物料编码.Text = "";
            cbb_详细地址.Text = "";
            cbb_邮箱地址.Text = "";
            cbb_专家编码.Text = "";
            cbb_专家分类.Text = "";
            cbb_专家名称.Text = "";
        }
    }
}
