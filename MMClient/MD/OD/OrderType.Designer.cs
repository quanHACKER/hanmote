﻿namespace MMClient.MD.OD
{
    partial class OrderType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_创建方式 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_创建方式);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1276, 121);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "创建方式";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 40);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "订单创建方式：";
            // 
            // cbb_创建方式
            // 
            this.cbb_创建方式.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_创建方式.FormattingEnabled = true;
            this.cbb_创建方式.Items.AddRange(new object[] {
            "未选择",
            "手动选择订单项",
            "参照采购申请",
            "参照合同",
            "参照计划协议",
            "参照采购信息记录",
            "参照MRP结果",
            "参照往期订单"});
            this.cbb_创建方式.Location = new System.Drawing.Point(134, 37);
            this.cbb_创建方式.Name = "cbb_创建方式";
            this.cbb_创建方式.Size = new System.Drawing.Size(245, 24);
            this.cbb_创建方式.TabIndex = 1;
            // 
            // OrderType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.groupBox1);
            this.Name = "OrderType";
            this.Text = "创建采购";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_创建方式;
        private System.Windows.Forms.Label label1;
    }
}