﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;

namespace MMClient.MD.GN
{
    public partial class Currency : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Currency()
        {
            InitializeComponent();
        }

        private void Currency_Load(object sender, EventArgs e)
        {
            string sql = " SELECT Currency_ID as '货币编码',Currency_Name as '货币名称',Currency_ENG as '英文缩写',Currency_LOGO as '货币标识' FROM [Currency] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_货币信息.DataSource = dt;

        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCurrency ncy = new NewCurrency();
            ncy.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = " SELECT Currency_ID as '货币编码',Currency_Name as '货币名称',Currency_ENG as '英文缩写',Currency_LOGO as '货币标识' FROM [Currency] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_货币信息.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            string code = dgv_货币信息.CurrentRow.Cells[0].Value.ToString();
            string sql = " DELETE FROM [Currency] WHERE Currency_ID='" + code + "'";
            DBHelper.ExecuteNonQuery(sql);
            
            dgv_货币信息.Rows.Remove(dgv_货币信息.SelectedRows[0]);


        }

        private void pageTool_Load(object sender, EventArgs e)
        {

        }
    }
}
