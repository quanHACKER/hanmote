﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.GN
{
    public partial class NewTradeClause : Form
    {
        public NewTradeClause()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_确定_Click(object sender, EventArgs e)
        {
            string ClauseID = cbb_科目代码.Text;
            string ClauseName = cbb_科目名称.Text;
            string Description = tbx_描述.Text;
            if (ClauseID == "")
            {
                MessageUtil.ShowError("请输入正确的条件编码");
            }
            else
            {
                gn.NewTradeClauseInformation(ClauseID, ClauseName, Description);
                MessageUtil.ShowTips("新建交付条件成功");
                this.Close();
            }
        }
    }
}
