﻿namespace MMClient.MD.GN
{
    partial class NewStockLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.tbx_库位描述 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_库位编码 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.tbx_库位描述);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbx_库位编码);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(9, 8);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(627, 307);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建库位信息";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(445, 263);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(85, 36);
            this.btn_确定.TabIndex = 4;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // tbx_库位描述
            // 
            this.tbx_库位描述.Location = new System.Drawing.Point(105, 81);
            this.tbx_库位描述.Name = "tbx_库位描述";
            this.tbx_库位描述.Size = new System.Drawing.Size(325, 22);
            this.tbx_库位描述.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "库位描述";
            // 
            // tbx_库位编码
            // 
            this.tbx_库位编码.Location = new System.Drawing.Point(105, 33);
            this.tbx_库位编码.Name = "tbx_库位编码";
            this.tbx_库位编码.Size = new System.Drawing.Size(100, 22);
            this.tbx_库位编码.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "库位编码";
            // 
            // NewStockLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 319);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewStockLocation";
            this.Text = "新建库位信息";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.TextBox tbx_库位描述;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_库位编码;
        private System.Windows.Forms.Label label1;
    }
}