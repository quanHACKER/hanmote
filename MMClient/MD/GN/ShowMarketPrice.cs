﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class ShowMarketPrice : Form
    {
        string materialID;
        public ShowMarketPrice(string mtid)
        {
            materialID = mtid;
            InitializeComponent();
        }

        private void ShowMarketPrice_Load(object sender, EventArgs e)
        {
            string sql = "SELECT Material_ID AS '物料编码',Market_Price AS '市场价格',Start_Time AS '有效起始期',End_Time AS '到期日期' FROM [Market_Price] WHERE Material_ID = '" + materialID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0 && dt != null)
                dataGridView1.DataSource = dt;
            else
                MessageUtil.ShowError("没有此物料的市场价格数据");

        }

        private void btn_关闭_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
