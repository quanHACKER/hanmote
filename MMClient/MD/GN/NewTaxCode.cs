﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.GN
{
    public partial class NewTaxCode : Form
    {
        public NewTaxCode()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_确定_Click(object sender, EventArgs e)
        {
            string taxcode = cbb_税码.Text;
            double tax;
            if(cbb_税率.Text=="")
             tax = 0;
            else
                try
                {
                    tax = Convert.ToDouble(cbb_税率.Text);
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowTips("请输入正确的税率");
                    tax = 0;
                }
            string description = tbx_描述.Text;
            if (taxcode == "")
            {
                MessageUtil.ShowError("请输入正确的税码");
            }
            else
            {
                gn.NewTaxCode(taxcode, tax, description);
                MessageUtil.ShowTips("新建税码信息成功");
                this.Close();
            }
        }
    }
}
