﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class Measurement : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Measurement()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewMeasurement nmt = new NewMeasurement();
            nmt.Show();
        }

        private void Measurement_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllMeasurementInformation();
            dgv_计量单位.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllMeasurementInformation();
            dgv_计量单位.DataSource = dt;
        }
    }
}
