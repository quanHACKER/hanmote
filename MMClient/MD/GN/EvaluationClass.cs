﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class EvaluationClass : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public EvaluationClass()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void EvaluationClass_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllEvaluationClassInformation();
            dgv_评估类.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewEvaluationClass nec = new NewEvaluationClass();
            nec.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllEvaluationClassInformation();
            dgv_评估类.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_评估类.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string ClassID = dgv_评估类.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteEvaluationClass(ClassID);
                    dgv_评估类.Rows.Remove(dgv_评估类.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }
            }
        }
    }
}
