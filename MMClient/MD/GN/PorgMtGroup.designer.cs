﻿namespace MMClient.MD.GN
{
    partial class PorgMtGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_物料组分配 = new System.Windows.Forms.DataGridView();
            this.cln_采购组织代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_采购组织名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_操作 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新条目 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料组分配)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_物料组分配
            // 
            this.dgv_物料组分配.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_物料组分配.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_物料组分配.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_物料组分配.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_采购组织代码,
            this.cln_采购组织名称,
            this.cln_物料组代码,
            this.cln_物料组名称,
            this.cln_操作});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_物料组分配.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_物料组分配.Location = new System.Drawing.Point(4, 3);
            this.dgv_物料组分配.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_物料组分配.Name = "dgv_物料组分配";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_物料组分配.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_物料组分配.RowTemplate.Height = 24;
            this.dgv_物料组分配.Size = new System.Drawing.Size(686, 239);
            this.dgv_物料组分配.TabIndex = 0;
            this.dgv_物料组分配.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_物料组分配_CellContentClick);
            // 
            // cln_采购组织代码
            // 
            this.cln_采购组织代码.HeaderText = "采购组织编码";
            this.cln_采购组织代码.Name = "cln_采购组织代码";
            // 
            // cln_采购组织名称
            // 
            this.cln_采购组织名称.HeaderText = "采购组织名称";
            this.cln_采购组织名称.Name = "cln_采购组织名称";
            // 
            // cln_物料组代码
            // 
            this.cln_物料组代码.HeaderText = "物料组代码";
            this.cln_物料组代码.Name = "cln_物料组代码";
            // 
            // cln_物料组名称
            // 
            this.cln_物料组名称.HeaderText = "物料组名称";
            this.cln_物料组名称.Name = "cln_物料组名称";
            // 
            // cln_操作
            // 
            this.cln_操作.HeaderText = "操作";
            this.cln_操作.Name = "cln_操作";
            this.cln_操作.Text = "操作";
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(507, 247);
            this.btn_新条目.Margin = new System.Windows.Forms.Padding(2);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(74, 32);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新条目
            // 
            this.btn_刷新条目.Location = new System.Drawing.Point(601, 247);
            this.btn_刷新条目.Margin = new System.Windows.Forms.Padding(2);
            this.btn_刷新条目.Name = "btn_刷新条目";
            this.btn_刷新条目.Size = new System.Drawing.Size(61, 32);
            this.btn_刷新条目.TabIndex = 2;
            this.btn_刷新条目.Text = "刷新条目";
            this.btn_刷新条目.UseVisualStyleBackColor = true;
            this.btn_刷新条目.Click += new System.EventHandler(this.btn_刷新条目_Click);
            // 
            // PorgMtGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 284);
            this.Controls.Add(this.btn_刷新条目);
            this.Controls.Add(this.btn_新条目);
            this.Controls.Add(this.dgv_物料组分配);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PorgMtGroup";
            this.Text = "给采购组织分配物料组";
            this.Load += new System.EventHandler(this.PorgMtGroup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料组分配)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_物料组分配;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_采购组织代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_采购组织名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组名称;
        private System.Windows.Forms.DataGridViewButtonColumn cln_操作;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新条目;
        
    }
}