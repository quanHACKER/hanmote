﻿namespace MMClient.MD.GN
{
    partial class NewClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbx_分类名称 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_新建 = new System.Windows.Forms.Button();
            this.dgv_新建特性 = new System.Windows.Forms.DataGridView();
            this.cln_状态 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cln_特性编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_特性名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_分类编码 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_新建特性)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbx_分类名称);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btn_新建);
            this.groupBox1.Controls.Add(this.dgv_新建特性);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbx_分类编码);
            this.groupBox1.Location = new System.Drawing.Point(1, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(691, 286);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // tbx_分类名称
            // 
            this.tbx_分类名称.Location = new System.Drawing.Point(281, 30);
            this.tbx_分类名称.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_分类名称.Name = "tbx_分类名称";
            this.tbx_分类名称.Size = new System.Drawing.Size(102, 21);
            this.tbx_分类名称.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(220, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "分类名称";
            // 
            // btn_新建
            // 
            this.btn_新建.Location = new System.Drawing.Point(589, 32);
            this.btn_新建.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_新建.Name = "btn_新建";
            this.btn_新建.Size = new System.Drawing.Size(67, 26);
            this.btn_新建.TabIndex = 3;
            this.btn_新建.Text = "保存";
            this.btn_新建.UseVisualStyleBackColor = true;
            this.btn_新建.Click += new System.EventHandler(this.btn_新建_Click);
            // 
            // dgv_新建特性
            // 
            this.dgv_新建特性.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_新建特性.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_新建特性.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_状态,
            this.cln_特性编码,
            this.cln_特性名称});
            this.dgv_新建特性.Location = new System.Drawing.Point(0, 71);
            this.dgv_新建特性.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv_新建特性.Name = "dgv_新建特性";
            this.dgv_新建特性.RowTemplate.Height = 24;
            this.dgv_新建特性.Size = new System.Drawing.Size(687, 208);
            this.dgv_新建特性.TabIndex = 2;
            // 
            // cln_状态
            // 
            this.cln_状态.HeaderText = "状态";
            this.cln_状态.Name = "cln_状态";
            // 
            // cln_特性编码
            // 
            this.cln_特性编码.HeaderText = "特性编码";
            this.cln_特性编码.Name = "cln_特性编码";
            this.cln_特性编码.ReadOnly = true;
            this.cln_特性编码.Visible = false;
            // 
            // cln_特性名称
            // 
            this.cln_特性名称.HeaderText = "特性名称";
            this.cln_特性名称.Name = "cln_特性名称";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "分类编码";
            // 
            // tbx_分类编码
            // 
            this.tbx_分类编码.Location = new System.Drawing.Point(65, 30);
            this.tbx_分类编码.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_分类编码.Name = "tbx_分类编码";
            this.tbx_分类编码.Size = new System.Drawing.Size(102, 21);
            this.tbx_分类编码.TabIndex = 0;
            // 
            // NewClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 290);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NewClass";
            this.Text = "新建分类";
            this.Load += new System.EventHandler(this.NewClass_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_新建特性)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_新建特性;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_分类编码;
        private System.Windows.Forms.TextBox tbx_分类名称;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_新建;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cln_状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特性编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特性名称;
    }
}