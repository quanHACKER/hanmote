﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Model.MD.GN;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
namespace MMClient.MD.GN
{
    public partial class FactoryCompany : Form
    {
        public FactoryCompany()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        private void FactoryCompany_Load(object sender, EventArgs e)
        {
           

            List<string> list = gn.GetAllFactory();
            cbm.FuzzyQury(cbb_工厂代码, list);
            List<string> list1 = gn.GetAllCompany();
            cbm.FuzzyQury(cbb_所属公司, list1);


        }

        private void cbb_工厂代码_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lib.Model.MD.GN.Factory fac = new Lib.Model.MD.GN.Factory();
            //读取对应工厂信息
           fac = gn.GetFactoryInformationByID(cbb_工厂代码.Text);
           cbb_工厂名称.Text = fac.FactoryName;
           cbb_所属公司.Text = fac.CompanyCode;
            
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            string sql = "update [Factory] SET Company_ID = '" + cbb_所属公司.Text + "' WHERE Factory_ID = '" + cbb_工厂代码.Text + "'";
            DBHelper.ExecuteNonQuery(sql);
            MessageUtil.ShowTips("工厂分配成功");
            this.Close();
        }
    }
}
