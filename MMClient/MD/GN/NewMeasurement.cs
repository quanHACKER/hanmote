﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewMeasurement : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public NewMeasurement()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void NewMeasurement_Load(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllMeasurement();
            cbb_关联计量单位名称.DataSource = list;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_计量单位代码.Text == "")
                MessageUtil.ShowError("请输入正确的计量单位代码");
            else
            {
                List<string> list = gn.GetAllMeasurement();
                if (gn.IDInTheList(list, cbb_计量单位代码.Text) == true)
                {
                    string meid = cbb_计量单位代码.Text;
                    string mename = cbb_计量单位名称.Text;
                    string remename = cbb_关联计量单位名称.Text;
                    string type = cbb_计量单位类型.Text;
                    float ratio;
                    try
                    {
                         ratio = (float)Convert.ToDouble(cbb_转换比率.Text);
                    }
                    catch( Exception ex)
                    {
                        ratio =1;
                    }
                    gn.NewMeasurement(meid, mename, remename, ratio,type);
                    MessageUtil.ShowTips("新建计量单位信息成功");
                    this.Close();
                }
                else
                    MessageUtil.ShowError("计量单位重复，请重新输入");
            }

        }
    }
}
