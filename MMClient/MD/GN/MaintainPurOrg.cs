﻿using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.GN
{
    public partial class MaintainPurOrg : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        GeneralBLL gn = new GeneralBLL();
        public MaintainPurOrg()
        {
            InitializeComponent();
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewBuyerOrganization nbo = new NewBuyerOrganization();
            nbo.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_采购组织.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string boid = dgv_采购组织.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteBuyerOrganization(boid);
                    dgv_采购组织.Rows.Remove(dgv_采购组织.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }
            }
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerOrganizationInformation();
            dgv_采购组织.DataSource = dt;
        }

        private void MaintainPurOrg_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerOrganizationInformation();
            dgv_采购组织.DataSource = null;
            dgv_采购组织.DataSource = dt;
        }
    }
}
