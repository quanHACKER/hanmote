﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.GN
{
    public partial class NewClause : Form
    {
        public NewClause()
        {
            InitializeComponent();
        }

        private void btn_取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            float dct1 = (float)Convert.ToDouble(tbx_折扣1.Text);
            float dct2 = (float)Convert.ToDouble(tbx_折扣2.Text);
            int day1 = Convert.ToInt16(tbx_天数1.Text);
            int day2 = Convert.ToInt16(tbx_天数2.Text);
            int day3 = Convert.ToInt16(tbx_天数3.Text);
            string description = tbx_描述.Text;
            string ID = tbx_条件编码.Text;
            GeneralBLL gn = new GeneralBLL();
            gn.NewPaymentClause(ID, description, dct1, day1, dct2, day2, day3);
            MessageBox.Show("创建付款条件成功");
                this.Close();
        }

      
    }
}
