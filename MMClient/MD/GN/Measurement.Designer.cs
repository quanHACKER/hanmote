﻿namespace MMClient.MD.GN
{
    partial class Measurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.dgv_计量单位 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_计量单位)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.dgv_计量单位);
            this.groupBox1.Location = new System.Drawing.Point(0, 2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(921, 374);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "计量单位信息";
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(11, 197);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(75, 48);
            this.btn_删除.TabIndex = 3;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(11, 124);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(75, 48);
            this.btn_刷新.TabIndex = 2;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(11, 56);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(75, 48);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // dgv_计量单位
            // 
            this.dgv_计量单位.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_计量单位.Location = new System.Drawing.Point(126, 21);
            this.dgv_计量单位.Name = "dgv_计量单位";
            this.dgv_计量单位.RowTemplate.Height = 24;
            this.dgv_计量单位.Size = new System.Drawing.Size(773, 353);
            this.dgv_计量单位.TabIndex = 0;
            // 
            // Measurement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 378);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Measurement";
            this.Text = "维护计量单位";
            this.Load += new System.EventHandler(this.Measurement_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_计量单位)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.DataGridView dgv_计量单位;
    }
}