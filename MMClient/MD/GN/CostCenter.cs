﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class CostCenter : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public CostCenter()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void CostCenter_Load(object sender, EventArgs e)
        {
            string sql = "SELECT Cost_Center_ID AS '成本中心代码',Cost_Center_Name AS '成本中心名称',Company_ID AS '公司代码' FROM [Cost_Center]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_成本中心.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT Cost_Center_ID AS '成本中心代码',Cost_Center_Name AS '成本中心名称',Company_ID AS '公司代码' FROM [Cost_Center]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_成本中心.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCostCenter ncc = new NewCostCenter();
            ncc.Show();
        } 

    }
}
