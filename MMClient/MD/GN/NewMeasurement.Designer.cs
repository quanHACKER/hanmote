﻿namespace MMClient.MD.GN
{
    partial class NewMeasurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_转换比率 = new System.Windows.Forms.ComboBox();
            this.cbb_关联计量单位名称 = new System.Windows.Forms.ComboBox();
            this.cbb_计量单位名称 = new System.Windows.Forms.ComboBox();
            this.cbb_计量单位代码 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_计量单位类型 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(354, 200);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 26;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_转换比率
            // 
            this.cbb_转换比率.FormattingEnabled = true;
            this.cbb_转换比率.Location = new System.Drawing.Point(190, 149);
            this.cbb_转换比率.Name = "cbb_转换比率";
            this.cbb_转换比率.Size = new System.Drawing.Size(121, 24);
            this.cbb_转换比率.TabIndex = 25;
            // 
            // cbb_关联计量单位名称
            // 
            this.cbb_关联计量单位名称.FormattingEnabled = true;
            this.cbb_关联计量单位名称.Location = new System.Drawing.Point(190, 113);
            this.cbb_关联计量单位名称.Name = "cbb_关联计量单位名称";
            this.cbb_关联计量单位名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_关联计量单位名称.TabIndex = 24;
            // 
            // cbb_计量单位名称
            // 
            this.cbb_计量单位名称.FormattingEnabled = true;
            this.cbb_计量单位名称.Location = new System.Drawing.Point(190, 78);
            this.cbb_计量单位名称.Name = "cbb_计量单位名称";
            this.cbb_计量单位名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_计量单位名称.TabIndex = 23;
            // 
            // cbb_计量单位代码
            // 
            this.cbb_计量单位代码.FormattingEnabled = true;
            this.cbb_计量单位代码.Location = new System.Drawing.Point(190, 45);
            this.cbb_计量单位代码.Name = "cbb_计量单位代码";
            this.cbb_计量单位代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_计量单位代码.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 21;
            this.label4.Text = "转换比率";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "相关联计量单位";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "计量单位名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 48);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "计量单位代码";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 17);
            this.label5.TabIndex = 27;
            this.label5.Text = "计量单位类型";
            // 
            // cbb_计量单位类型
            // 
            this.cbb_计量单位类型.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_计量单位类型.FormattingEnabled = true;
            this.cbb_计量单位类型.Items.AddRange(new object[] {
            "时间单位",
            "重量单位",
            "体积单位",
            "数量单位",
            "其他单位"});
            this.cbb_计量单位类型.Location = new System.Drawing.Point(190, 187);
            this.cbb_计量单位类型.Name = "cbb_计量单位类型";
            this.cbb_计量单位类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_计量单位类型.TabIndex = 28;
            // 
            // NewMeasurement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.cbb_计量单位类型);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_转换比率);
            this.Controls.Add(this.cbb_关联计量单位名称);
            this.Controls.Add(this.cbb_计量单位名称);
            this.Controls.Add(this.cbb_计量单位代码);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewMeasurement";
            this.Text = "新建计量单位";
            this.Load += new System.EventHandler(this.NewMeasurement_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_转换比率;
        private System.Windows.Forms.ComboBox cbb_关联计量单位名称;
        private System.Windows.Forms.ComboBox cbb_计量单位名称;
        private System.Windows.Forms.ComboBox cbb_计量单位代码;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_计量单位类型;
    }
}