﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General.Material_Type;
namespace MMClient.MD.GN
{
    public partial class NewBuyerGroup : Form
    {
        public NewBuyerGroup()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        MTTypeBBLL mtb = new MTTypeBBLL();


        private void NewBuyerGroup_Load(object sender, EventArgs e)
        {
            List<string> list = mtb.GetAllBigClassfyName();
            cbb_物料类型.DataSource = list;
            cbb_物料类型.Text = "";
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_采购组代码.Text == "")
            {
                MessageUtil.ShowError("请输入正确的采购组代码");
                cbb_采购组代码.Focus();
            }
            else
            {
                List<string> list = gn.GetAllFactory();
                if (gn.IDInTheList(list, cbb_采购组代码.Text) == true)
                {
                    gn.NewBuyerGroup(cbb_采购组代码.Text, cbb_采购组名称.Text, cbb_物料类型.Text);
                    MessageUtil.ShowTips("新建采购组信息成功");
                    this.Close();
                }
                else
                {
                    MessageUtil.ShowError("采购组代码重复，请重新输入");
                    cbb_采购组代码.Focus();
                }
            }
        }

    }
}
