﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;

namespace MMClient.MD.GN
{
    public partial class Country : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Country()
        {
            InitializeComponent();
        }

        private void Country_Load(object sender, EventArgs e)
        {
            string sql = " SELECT * FROM [Country] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_国家信息.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCountry ncy = new NewCountry();
            ncy.Show();

        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = " SELECT * FROM [Country] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_国家信息.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            string code = dgv_国家信息.CurrentRow.Cells[0].Value.ToString();
            string sql = " DELETE FROM [Country] WHERE Country_ID='" + code + "'";
            DBHelper.ExecuteNonQuery(sql);

            dgv_国家信息.Rows.Remove(dgv_国家信息.SelectedRows[0]);
        }


    }
}
