﻿namespace MMClient.MD.GN
{
    partial class NewCostCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_成本中心名称 = new System.Windows.Forms.ComboBox();
            this.cbb_成本中心代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_公司代码 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(406, 205);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 61;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_成本中心名称
            // 
            this.cbb_成本中心名称.FormattingEnabled = true;
            this.cbb_成本中心名称.Location = new System.Drawing.Point(242, 83);
            this.cbb_成本中心名称.Name = "cbb_成本中心名称";
            this.cbb_成本中心名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_成本中心名称.TabIndex = 60;
            // 
            // cbb_成本中心代码
            // 
            this.cbb_成本中心代码.FormattingEnabled = true;
            this.cbb_成本中心代码.Location = new System.Drawing.Point(242, 50);
            this.cbb_成本中心代码.Name = "cbb_成本中心代码";
            this.cbb_成本中心代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_成本中心代码.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 58;
            this.label3.Text = "公司代码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 57;
            this.label2.Text = "成本中心名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 53);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 56;
            this.label1.Text = "成本中心代码";
            // 
            // cbb_公司代码
            // 
            this.cbb_公司代码.FormattingEnabled = true;
            this.cbb_公司代码.Location = new System.Drawing.Point(242, 118);
            this.cbb_公司代码.Name = "cbb_公司代码";
            this.cbb_公司代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司代码.TabIndex = 62;
            // 
            // NewCostCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.cbb_公司代码);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_成本中心名称);
            this.Controls.Add(this.cbb_成本中心代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewCostCenter";
            this.Text = "新建成本中心";
            this.Load += new System.EventHandler(this.NewCostCenter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_成本中心名称;
        private System.Windows.Forms.ComboBox cbb_成本中心代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_公司代码;
    }
}