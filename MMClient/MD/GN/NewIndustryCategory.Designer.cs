﻿namespace MMClient.MD.GN
{
    partial class NewIndustryCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_工业标识代码 = new System.Windows.Forms.ComboBox();
            this.cbb_工业标识名称 = new System.Windows.Forms.ComboBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 53);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 44;
            this.label1.Text = "工业标识代码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 45;
            this.label2.Text = "工业标识名称";
            // 
            // cbb_工业标识代码
            // 
            this.cbb_工业标识代码.FormattingEnabled = true;
            this.cbb_工业标识代码.Location = new System.Drawing.Point(264, 50);
            this.cbb_工业标识代码.Name = "cbb_工业标识代码";
            this.cbb_工业标识代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_工业标识代码.TabIndex = 46;
            // 
            // cbb_工业标识名称
            // 
            this.cbb_工业标识名称.FormattingEnabled = true;
            this.cbb_工业标识名称.Location = new System.Drawing.Point(264, 83);
            this.cbb_工业标识名称.Name = "cbb_工业标识名称";
            this.cbb_工业标识名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_工业标识名称.TabIndex = 47;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(428, 205);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 48;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // NewIndustryCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_工业标识名称);
            this.Controls.Add(this.cbb_工业标识代码);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewIndustryCategory";
            this.Text = "新建工业标识";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_工业标识代码;
        private System.Windows.Forms.ComboBox cbb_工业标识名称;
        private System.Windows.Forms.Button btn_确定;

    }
}