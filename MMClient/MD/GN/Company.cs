﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class Company : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Company()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllCompanyInformation();
            dgv_公司代码.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCompany ncy = new NewCompany();
            ncy.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_公司代码.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                string CompanyID = dgv_公司代码.CurrentRow.Cells[0].Value.ToString();
                gn.DeleteCompany(CompanyID);
                dgv_公司代码.Rows.Remove(dgv_公司代码.CurrentRow);
                MessageUtil.ShowTips("删除成功");
            }
        }

        private void Company_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllCompanyInformation();
            dgv_公司代码.DataSource = dt;
        }
    }
}
