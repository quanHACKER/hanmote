﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewFactory : Form
    {
        public NewFactory()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void NewFactory_Load(object sender, EventArgs e)
        {
            List<string> list = gn.GetCompanyCode();
            cbb_所属公司.DataSource = list;
            cbb_所属公司.Text = "";
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_工厂代码.Text == "")
            {
                MessageUtil.ShowError("请输入正确的工厂代码");
                cbb_工厂代码.Focus();
            }
            else
            {
                List<string> list = gn.GetAllFactory();
                if (gn.IDInTheList(list, cbb_工厂代码.Text) == true)
                {
                    gn.NewFactory(cbb_工厂代码.Text, cbb_工厂名称.Text, cbb_所属公司.Text);
                    MessageUtil.ShowTips("新建工厂信息成功");
                    this.Close();
                }
                else
                {
                    MessageUtil.ShowError("工厂代码重复，请重新输入");
                    cbb_工厂代码.Focus();
                }
            }
            

              
        }
    }

}
