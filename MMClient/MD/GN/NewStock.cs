﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewStock : Form
    {
        public NewStock()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void NewStock_Load(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllFactory();
            cbb_所属工厂.DataSource = list;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_仓库代码.Text == "")
                MessageUtil.ShowError("请输入正确的仓库代码");
            else
            {
                List<string> listf = gn.GetAllFactory();
                List<string> lists = gn.GetAllStock();
                if (gn.IDInTheList(listf, cbb_所属工厂.Text)==true || gn.IDInTheList(lists, cbb_仓库代码.Text) == true)
                {
                    gn.NewStock(cbb_仓库代码.Text, cbb_仓库名称.Text, cbb_所属工厂.Text);
                    MessageUtil.ShowTips("新建仓库信息成功");
                    this.Close();
                }
                else
                    MessageUtil.ShowError("仓库信息重复，请重新输入");
            }
        }

    }
}
