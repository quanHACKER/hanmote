﻿namespace MMClient.MD.GN
{
    partial class NewPorgMtGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_刷新条目 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cln_采购组织代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_采购组织名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_刷新条目
            // 
            this.btn_刷新条目.Location = new System.Drawing.Point(599, 248);
            this.btn_刷新条目.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_刷新条目.Name = "btn_刷新条目";
            this.btn_刷新条目.Size = new System.Drawing.Size(61, 32);
            this.btn_刷新条目.TabIndex = 5;
            this.btn_刷新条目.Text = "保存";
            this.btn_刷新条目.UseVisualStyleBackColor = true;
            this.btn_刷新条目.Click += new System.EventHandler(this.btn_刷新条目_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_采购组织代码,
            this.cln_采购组织名称,
            this.cln_物料组代码,
            this.cln_物料组名称});
            this.dataGridView1.Location = new System.Drawing.Point(2, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(686, 239);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // cln_采购组织代码
            // 
            this.cln_采购组织代码.HeaderText = "采购组织编码";
            this.cln_采购组织代码.Name = "cln_采购组织代码";
            // 
            // cln_采购组织名称
            // 
            this.cln_采购组织名称.HeaderText = "采购组织名称";
            this.cln_采购组织名称.Name = "cln_采购组织名称";
            // 
            // cln_物料组代码
            // 
            this.cln_物料组代码.HeaderText = "物料组代码";
            this.cln_物料组代码.Name = "cln_物料组代码";
            // 
            // cln_物料组名称
            // 
            this.cln_物料组名称.HeaderText = "物料组名称";
            this.cln_物料组名称.Name = "cln_物料组名称";
            // 
            // NewPorgMtGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 284);
            this.Controls.Add(this.btn_刷新条目);
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NewPorgMtGroup";
            this.Text = "给采购组织分配物料组：新条目";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_刷新条目;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_采购组织代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_采购组织名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组名称;
    }
}