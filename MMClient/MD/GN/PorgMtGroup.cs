﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Model.MD.GN;

namespace MMClient.MD.GN
{
    
    public partial class PorgMtGroup : Form
    {
        public PorgMtGroup()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void PorgMtGroup_Load(object sender, EventArgs e)
        {

           // dgv_物料组分配.Rows.Add(10); //指定行数对输出结果存在影响，加大行数
            List<PorgMtGropRelation> list = new List<PorgMtGropRelation>();
            list =   gn.GetPorgMtgpRelation();
            dgv_物料组分配.Rows.Add(list.Count);
            for (int i = 0; i < list.Count; i++)
         {
             dgv_物料组分配.Rows[i].Cells[0].Value = list[i].Porg_ID;
             dgv_物料组分配.Rows[i].Cells[1].Value = list[i].Porg_Name;
             dgv_物料组分配.Rows[i].Cells[2].Value = list[i].MtGroup_ID;
             dgv_物料组分配.Rows[i].Cells[3].Value = list[i].MtGroup_Name;
                //添加文本点击事件
             dgv_物料组分配.Rows[i].Cells[4].Value ="修改";
         }
    }
        /// <summary>
        /// 修改采购组分配物料组信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_物料组分配_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            String  code_PurGroup = dgv_物料组分配.Rows[e.RowIndex].Cells[0].Value.ToString();
            String  name_PurGroup = dgv_物料组分配.Rows[e.RowIndex].Cells[1].Value.ToString();
            String  code_MtGroup = dgv_物料组分配.Rows[e.RowIndex].Cells[2].Value.ToString();
            String  name_MtGroup = dgv_物料组分配.Rows[e.RowIndex].Cells[3].Value.ToString();
            MDPurAndMtGroup md = new MDPurAndMtGroup(code_PurGroup,name_PurGroup, code_MtGroup, name_MtGroup);
            md.Show();
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
           // NewPurAndMtGroup gd = new NewPurAndMtGroup();
           // gd.Show();
        }

        private void btn_刷新条目_Click(object sender, EventArgs e)
        {
            // DataTable dt = gn.getAllGroupInfo();
            // dgv_物料组分配.DataSource = dt;
            this.Close();
            PorgMtGroup pmt = new PorgMtGroup();
            pmt.Show();
        }
    }
}
