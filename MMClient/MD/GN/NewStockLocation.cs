﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    
    public partial class NewStockLocation : Form
    {
        public NewStockLocation()
        {
            InitializeComponent();
        }
        string stockid;
        public NewStockLocation(string stockID)
        {
            InitializeComponent();
           stockid = stockID;
         
        }
     
        private void btn_确定_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "INSERT INTO [StockLocation] (StockLocation_ID,Description,StockID)VALUES('" + tbx_库位编码.Text + "',";
                sql += "'" + tbx_库位描述.Text + "','" + stockid + "')";
                DBHelper.ExecuteNonQuery(sql);
                MessageUtil.ShowTips("新建库位成功");

            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("库位编码重复");
            }
        }

    }
}
