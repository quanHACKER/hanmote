﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class Factory : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Factory()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void Factory_Load(object sender, EventArgs e)
        {
            
            DataTable dt = gn.GetAllFactoryInformation();
            dgv_工厂信息.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewFactory nfy = new NewFactory();
            nfy.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            
            DataTable dt = gn.GetAllFactoryInformation();
            dgv_工厂信息.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_工厂信息.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string FactoryID = dgv_工厂信息.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteFactory(FactoryID);
                    dgv_工厂信息.Rows.Remove(dgv_工厂信息.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FactoryCompany fc = new FactoryCompany();
            fc.Show();
        }

    }
}
