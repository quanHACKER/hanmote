﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class BuyerGroup : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public BuyerGroup()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void Buyer_Group_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerGroupInformation();
            dgv_采购组.DataSource = dt;

          
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewBuyerGroup ngp = new NewBuyerGroup();
            ngp.Show();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerGroupInformation();
            dgv_采购组.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_采购组.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string GroupID = dgv_采购组.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteBuyerGroup(GroupID);
                    dgv_采购组.Rows.Remove(dgv_采购组.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }

            }
        }

        //private void cbb_test_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dt = new DataTable();
        //    dt.Rows.Add("a", "b");
        //    cbb_test.DataSource = dt;
        //}
    }
}
