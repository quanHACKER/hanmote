﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class MaintainClass : Form
    {
        public MaintainClass()
        {
            InitializeComponent();
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewClass nwc = new NewClass();
            nwc.Show();
        }

        private void MaintainClass_Load(object sender, EventArgs e)
        {
            dgv_特性分类信息.Rows.Add(100);
            string sql = "select distinct Class_ID,Class_Name from [Class_Feature] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgv_特性分类信息.Rows[i].Cells["cln_分类编码"].Value = dt.Rows[i]["Class_ID"].ToString();
                dgv_特性分类信息.Rows[i].Cells["cln_分类名称"].Value = dt.Rows[i]["Class_Name"].ToString();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int rowindex = dgv_特性分类信息.CurrentRow.Index;
            string classid = dgv_特性分类信息.Rows[rowindex].Cells["cln_分类编码"].Value.ToString();
            string classname = dgv_特性分类信息.Rows[rowindex].Cells["cln_分类名称"].Value.ToString();
            MClass mc = new MClass(classid,classname);
            mc.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            int rowindex = dgv_特性分类信息.CurrentRow.Index;
            string classid = dgv_特性分类信息.Rows[rowindex].Cells["cln_分类编码"].Value.ToString();
            string sql1 = "DELETE FROM [Class_Feature] WHERE Class_ID = '" + classid + "'";
            DBHelper.ExecuteNonQuery(sql1);
            string sql2 = "DELETE FROM [Material_Feature] WHERE Class_ID='" + classid + "'";
            DBHelper.ExecuteQueryDT(sql2);
            MessageUtil.ShowTips("删除分类信息成功");
            this.Close();
            MaintainClass mtc = new MaintainClass();
            mtc.Show();
            
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            this.Close();
            MaintainClass mtc = new MaintainClass();
            mtc.Show();
            
        }
    }
}
