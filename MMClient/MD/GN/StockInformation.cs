﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class StockInformation : Form
    {
        public StockInformation()
        {
            InitializeComponent();
        }
        string stockid ;
        string factoryid;
        public StockInformation(string stockID,string factoryID)
        {
            InitializeComponent();
           stockid = stockID;
            factoryid = factoryID;
            tbx_仓库编码.Text = stockID;
            tbx_所属工厂.Text = factoryID;
        }

        private void StockInformation_Load(object sender, EventArgs e)
        {
            string sql = "SELECT StockLocation_ID AS '库位编码',Description AS '库位描述' FROM [StockLocation] WHERE Stock_ID='" + tbx_仓库编码.Text + "'";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
          dgv_库位.DataSource = dt;
            
        }

        private void btn_新建_Click(object sender, EventArgs e)
        {
            NewStockLocation nsk = new NewStockLocation(stockid);
            nsk.Show();
        }

    }
}
