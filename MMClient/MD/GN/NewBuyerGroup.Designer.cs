﻿namespace MMClient.MD.GN
{
    partial class NewBuyerGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_物料类型 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组名称 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(390, 206);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 33;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_物料类型
            // 
            this.cbb_物料类型.FormattingEnabled = true;
            this.cbb_物料类型.Location = new System.Drawing.Point(226, 119);
            this.cbb_物料类型.Name = "cbb_物料类型";
            this.cbb_物料类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料类型.TabIndex = 32;
            // 
            // cbb_采购组名称
            // 
            this.cbb_采购组名称.FormattingEnabled = true;
            this.cbb_采购组名称.Location = new System.Drawing.Point(226, 84);
            this.cbb_采购组名称.Name = "cbb_采购组名称";
            this.cbb_采购组名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_采购组名称.TabIndex = 31;
            // 
            // cbb_采购组代码
            // 
            this.cbb_采购组代码.FormattingEnabled = true;
            this.cbb_采购组代码.Location = new System.Drawing.Point(226, 51);
            this.cbb_采购组代码.Name = "cbb_采购组代码";
            this.cbb_采购组代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_采购组代码.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "负责的物料类型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 28;
            this.label2.Text = "采购组名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 54);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "采购组代码";
            // 
            // NewBuyerGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_物料类型);
            this.Controls.Add(this.cbb_采购组名称);
            this.Controls.Add(this.cbb_采购组代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewBuyerGroup";
            this.Text = "新建采购组";
            this.Load += new System.EventHandler(this.NewBuyerGroup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_物料类型;
        private System.Windows.Forms.ComboBox cbb_采购组名称;
        private System.Windows.Forms.ComboBox cbb_采购组代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}