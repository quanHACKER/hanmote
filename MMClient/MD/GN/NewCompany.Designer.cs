﻿namespace MMClient.MD.GN
{
    partial class NewCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_公司名称 = new System.Windows.Forms.ComboBox();
            this.cbb_公司代码 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(385, 205);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 40;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_公司名称
            // 
            this.cbb_公司名称.FormattingEnabled = true;
            this.cbb_公司名称.Location = new System.Drawing.Point(221, 83);
            this.cbb_公司名称.Name = "cbb_公司名称";
            this.cbb_公司名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司名称.TabIndex = 38;
            // 
            // cbb_公司代码
            // 
            this.cbb_公司代码.FormattingEnabled = true;
            this.cbb_公司代码.Location = new System.Drawing.Point(221, 50);
            this.cbb_公司代码.Name = "cbb_公司代码";
            this.cbb_公司代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司代码.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "公司名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 53);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 34;
            this.label1.Text = "公司代码";
            // 
            // NewCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_公司名称);
            this.Controls.Add(this.cbb_公司代码);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewCompany";
            this.Text = "新建公司";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_公司名称;
        private System.Windows.Forms.ComboBox cbb_公司代码;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}