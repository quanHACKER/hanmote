﻿namespace MMClient.MD.GN
{
    partial class MaintainClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.dgv_特性分类信息 = new System.Windows.Forms.DataGridView();
            this.cln_分类编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_分类名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_特性分类信息)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.dgv_特性分类信息);
            this.groupBox1.Location = new System.Drawing.Point(4, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(915, 376);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护特性分类";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 127);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 48);
            this.button1.TabIndex = 4;
            this.button1.Text = "查看";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(8, 204);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(97, 48);
            this.btn_删除.TabIndex = 3;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(8, 52);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(97, 48);
            this.btn_新条目.TabIndex = 2;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // dgv_特性分类信息
            // 
            this.dgv_特性分类信息.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_特性分类信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_特性分类信息.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_分类编码,
            this.cln_分类名称});
            this.dgv_特性分类信息.Location = new System.Drawing.Point(161, 21);
            this.dgv_特性分类信息.Name = "dgv_特性分类信息";
            this.dgv_特性分类信息.RowTemplate.Height = 24;
            this.dgv_特性分类信息.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_特性分类信息.Size = new System.Drawing.Size(744, 335);
            this.dgv_特性分类信息.TabIndex = 1;
            // 
            // cln_分类编码
            // 
            this.cln_分类编码.HeaderText = "分类编码";
            this.cln_分类编码.Name = "cln_分类编码";
            // 
            // cln_分类名称
            // 
            this.cln_分类名称.HeaderText = "分类名称";
            this.cln_分类名称.Name = "cln_分类名称";
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(8, 275);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(97, 48);
            this.btn_刷新.TabIndex = 5;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // MaintainClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 387);
            this.Controls.Add(this.groupBox1);
            this.Name = "MaintainClass";
            this.Text = "维护特性分类";
            this.Load += new System.EventHandler(this.MaintainClass_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_特性分类信息)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_特性分类信息;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_分类编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_分类名称;
        private System.Windows.Forms.Button btn_刷新;
    }
}