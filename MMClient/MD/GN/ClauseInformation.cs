﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Model.MD;

namespace MMClient.MD.GN
{
    public partial class ClauseInformation : Form
    {
        public ClauseInformation(string ID)
        {
            InitializeComponent();
            tbx_条件编码.Text = ID;
        }

        private void ClauseInformation_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
           PaymentClause clause = gn.GetClause(tbx_条件编码.Text);
           tbx_描述.Text = clause.Description;
           tbx_折扣1.Text = clause.First_Discount.ToString();
           tbx_天数1.Text = clause.First_Date.ToString();
           tbx_折扣2.Text = clause.Second_Discount.ToString();
           tbx_天数2.Text = clause.Second_Date.ToString();
           tbx_天数3.Text = clause.Final_Date.ToString();
        }
    }
}
