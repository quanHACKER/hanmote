﻿namespace MMClient.MD.GN
{
    partial class NewTaxCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_描述 = new System.Windows.Forms.TextBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_税率 = new System.Windows.Forms.ComboBox();
            this.cbb_税码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbx_描述
            // 
            this.tbx_描述.Location = new System.Drawing.Point(150, 119);
            this.tbx_描述.Name = "tbx_描述";
            this.tbx_描述.Size = new System.Drawing.Size(328, 22);
            this.tbx_描述.TabIndex = 48;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(382, 206);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 47;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_税率
            // 
            this.cbb_税率.FormattingEnabled = true;
            this.cbb_税率.Location = new System.Drawing.Point(185, 84);
            this.cbb_税率.Name = "cbb_税率";
            this.cbb_税率.Size = new System.Drawing.Size(121, 24);
            this.cbb_税率.TabIndex = 46;
            // 
            // cbb_税码
            // 
            this.cbb_税码.FormattingEnabled = true;
            this.cbb_税码.Location = new System.Drawing.Point(185, 51);
            this.cbb_税码.Name = "cbb_税码";
            this.cbb_税码.Size = new System.Drawing.Size(121, 24);
            this.cbb_税码.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 44;
            this.label3.Text = "描述";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 43;
            this.label2.Text = "税率";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 54);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 42;
            this.label1.Text = "税码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(312, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 49;
            this.label4.Text = "%";
            // 
            // NewTaxCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbx_描述);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_税率);
            this.Controls.Add(this.cbb_税码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewTaxCode";
            this.Text = "新建税码";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_描述;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_税率;
        private System.Windows.Forms.ComboBox cbb_税码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
    }
}