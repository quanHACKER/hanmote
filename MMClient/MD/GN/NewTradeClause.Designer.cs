﻿namespace MMClient.MD.GN
{
    partial class NewTradeClause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_科目名称 = new System.Windows.Forms.ComboBox();
            this.cbb_科目代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_描述 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(293, 153);
            this.btn_确定.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(56, 34);
            this.btn_确定.TabIndex = 40;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_科目名称
            // 
            this.cbb_科目名称.FormattingEnabled = true;
            this.cbb_科目名称.Location = new System.Drawing.Point(170, 62);
            this.cbb_科目名称.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbb_科目名称.Name = "cbb_科目名称";
            this.cbb_科目名称.Size = new System.Drawing.Size(92, 20);
            this.cbb_科目名称.TabIndex = 38;
            // 
            // cbb_科目代码
            // 
            this.cbb_科目代码.FormattingEnabled = true;
            this.cbb_科目代码.Location = new System.Drawing.Point(170, 37);
            this.cbb_科目代码.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbb_科目代码.Name = "cbb_科目代码";
            this.cbb_科目代码.Size = new System.Drawing.Size(92, 20);
            this.cbb_科目代码.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 36;
            this.label3.Text = "支付条件细则";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 35;
            this.label2.Text = "支付条件名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 34;
            this.label1.Text = "支付条件代码";
            // 
            // tbx_描述
            // 
            this.tbx_描述.FormattingEnabled = true;
            this.tbx_描述.Location = new System.Drawing.Point(170, 88);
            this.tbx_描述.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_描述.Name = "tbx_描述";
            this.tbx_描述.Size = new System.Drawing.Size(239, 20);
            this.tbx_描述.TabIndex = 41;
            // 
            // NewTradeClause
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 190);
            this.Controls.Add(this.tbx_描述);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_科目名称);
            this.Controls.Add(this.cbb_科目代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NewTradeClause";
            this.Text = "新建支付条件";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_科目名称;
        private System.Windows.Forms.ComboBox cbb_科目代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox tbx_描述;
    }
}