﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewCostCenter : Form
    {
        public NewCostCenter()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        private void NewCostCenter_Load(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllCompany();
            cbm.FuzzyQury(cbb_公司代码, list);
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO [Cost_Center] Values ('" + cbb_成本中心代码.Text + "','" + cbb_成本中心名称.Text + "','" + cbb_公司代码.Text + "')";
            DBHelper.ExecuteNonQuery(sql);
            MessageUtil.ShowTips("新建成本中心成功");
            this.Close();
        }
    }
}
