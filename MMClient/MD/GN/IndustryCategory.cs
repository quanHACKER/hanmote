﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;


namespace MMClient.MD.GN
{
    public partial class IndustryCategory : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public IndustryCategory()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void IndustryCategory_Load(object sender, EventArgs e)
        {
            string sql = "SELECT Industry_ID AS '工业标识编码',Industry_Category AS '工业标识名称' FROM [Industry_Category] ";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           dgv_工业标识.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewIndustryCategory nic = new NewIndustryCategory();
            nic.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT Industry_ID AS '工业标识编码',Industry_Category AS '工业标识名称' FROM [Industry_Category] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_工业标识.DataSource = dt;
        }
    }
}
