﻿namespace MMClient.MD.GN
{
    partial class NewPurAndMtGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.save_Mt_group = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MtGroupDiviedTable = new System.Windows.Forms.DataGridView();
            this.isFinished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mtCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mt_Group_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isDivied = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isVisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btn_fileUpLoad = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.name__Pur_Organic = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.code_Pur_Organic = new System.Windows.Forms.ComboBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MtGroupDiviedTable)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // save_Mt_group
            // 
            this.save_Mt_group.Location = new System.Drawing.Point(308, 505);
            this.save_Mt_group.Name = "save_Mt_group";
            this.save_Mt_group.Size = new System.Drawing.Size(129, 35);
            this.save_Mt_group.TabIndex = 8;
            this.save_Mt_group.Text = "保存 ";
            this.save_Mt_group.UseVisualStyleBackColor = true;
            this.save_Mt_group.Click += new System.EventHandler(this.save_Mt_group_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MtGroupDiviedTable);
            this.panel1.Location = new System.Drawing.Point(-1, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(851, 371);
            this.panel1.TabIndex = 13;
            // 
            // MtGroupDiviedTable
            // 
            this.MtGroupDiviedTable.AllowUserToAddRows = false;
            this.MtGroupDiviedTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MtGroupDiviedTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MtGroupDiviedTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.MtGroupDiviedTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MtGroupDiviedTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MtGroupDiviedTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MtGroupDiviedTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isFinished,
            this.mtCode,
            this.Mt_Group_Name,
            this.isDivied,
            this.isVisible,
            this.btn_fileUpLoad});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MtGroupDiviedTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.MtGroupDiviedTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.MtGroupDiviedTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.MtGroupDiviedTable.Location = new System.Drawing.Point(3, 3);
            this.MtGroupDiviedTable.Name = "MtGroupDiviedTable";
            this.MtGroupDiviedTable.RowTemplate.Height = 23;
            this.MtGroupDiviedTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MtGroupDiviedTable.Size = new System.Drawing.Size(845, 365);
            this.MtGroupDiviedTable.TabIndex = 1;
            this.MtGroupDiviedTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MtGroupDiviedTable_CellContentClick);
            this.MtGroupDiviedTable.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.MtGroupDiviedTable_RowPostPaint);
            // 
            // isFinished
            // 
            this.isFinished.ContextMenuStrip = this.contextMenuStrip1;
            this.isFinished.HeaderText = "是否分配";
            this.isFinished.Name = "isFinished";
            this.isFinished.ToolTipText = "物料组分配是否完成";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 32);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(97, 6);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // mtCode
            // 
            this.mtCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.mtCode.DataPropertyName = "mtCode";
            this.mtCode.HeaderText = "物料组代码";
            this.mtCode.Name = "mtCode";
            this.mtCode.ReadOnly = true;
            this.mtCode.Width = 150;
            // 
            // Mt_Group_Name
            // 
            this.Mt_Group_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Mt_Group_Name.DataPropertyName = "Mt_Group_Name";
            this.Mt_Group_Name.HeaderText = "物料组名称";
            this.Mt_Group_Name.Name = "Mt_Group_Name";
            this.Mt_Group_Name.Width = 150;
            // 
            // isDivied
            // 
            this.isDivied.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.isDivied.HeaderText = "评审责任人";
            this.isDivied.Name = "isDivied";
            this.isDivied.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.isDivied.ToolTipText = "指定审核该物料组的负责人";
            this.isDivied.Width = 250;
            // 
            // isVisible
            // 
            this.isVisible.FalseValue = "1";
            this.isVisible.HeaderText = "不显示物料组";
            this.isVisible.Name = "isVisible";
            this.isVisible.ReadOnly = true;
            this.isVisible.ToolTipText = "若不显示物料，请打勾";
            this.isVisible.TrueValue = "0";
            // 
            // btn_fileUpLoad
            // 
            this.btn_fileUpLoad.HeaderText = "上传文件";
            this.btn_fileUpLoad.Name = "btn_fileUpLoad";
            this.btn_fileUpLoad.Text = "上传文件";
            this.btn_fileUpLoad.ToolTipText = "上传文件";
            this.btn_fileUpLoad.UseColumnTextForButtonValue = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.name__Pur_Organic);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.code_Pur_Organic);
            this.panel2.Location = new System.Drawing.Point(2, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(845, 91);
            this.panel2.TabIndex = 14;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(360, 56);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 15;
            this.textBox2.Text = "接受主审name值";
            this.textBox2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(109, 56);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 14;
            this.textBox1.Text = "接受主审id值";
            this.textBox1.Visible = false;
            // 
            // name__Pur_Organic
            // 
            this.name__Pur_Organic.Location = new System.Drawing.Point(391, 23);
            this.name__Pur_Organic.Name = "name__Pur_Organic";
            this.name__Pur_Organic.ReadOnly = true;
            this.name__Pur_Organic.Size = new System.Drawing.Size(134, 21);
            this.name__Pur_Organic.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "采购组织名称：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "采购组织编码：";
            // 
            // code_Pur_Organic
            // 
            this.code_Pur_Organic.Enabled = false;
            this.code_Pur_Organic.FormattingEnabled = true;
            this.code_Pur_Organic.Location = new System.Drawing.Point(109, 24);
            this.code_Pur_Organic.Name = "code_Pur_Organic";
            this.code_Pur_Organic.Size = new System.Drawing.Size(134, 20);
            this.code_Pur_Organic.TabIndex = 10;
            this.code_Pur_Organic.SelectedIndexChanged += new System.EventHandler(this.code_Pur_Organic_SelectedIndexChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Mt_Group_Name";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "物料组名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "是否已分配";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.Width = 400;
            // 
            // NewPurAndMtGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 562);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.save_Mt_group);
            this.Name = "NewPurAndMtGroup";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.NewPurAndMtGroup_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MtGroupDiviedTable)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button save_Mt_group;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox name__Pur_Organic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox code_Pur_Organic;
        public System.Windows.Forms.DataGridView MtGroupDiviedTable;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isFinished;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mt_Group_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn isDivied;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isVisible;
        private System.Windows.Forms.DataGridViewButtonColumn btn_fileUpLoad;
    }
}