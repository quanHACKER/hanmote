﻿namespace MMClient.MD.GN
{
    partial class StockInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_仓库编码 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_所属工厂 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.gpx_库位 = new System.Windows.Forms.GroupBox();
            this.dgv_库位 = new System.Windows.Forms.DataGridView();
            this.btn_新建 = new System.Windows.Forms.Button();
            this.btn_修改 = new System.Windows.Forms.Button();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gpx_库位.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_库位)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.gpx_库位);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tbx_所属工厂);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbx_仓库编码);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 0);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(931, 412);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "仓库信息";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 33);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "仓库编码";
            // 
            // tbx_仓库编码
            // 
            this.tbx_仓库编码.Enabled = false;
            this.tbx_仓库编码.Location = new System.Drawing.Point(92, 33);
            this.tbx_仓库编码.Name = "tbx_仓库编码";
            this.tbx_仓库编码.Size = new System.Drawing.Size(100, 22);
            this.tbx_仓库编码.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(246, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "所属工厂";
            // 
            // tbx_所属工厂
            // 
            this.tbx_所属工厂.Enabled = false;
            this.tbx_所属工厂.Location = new System.Drawing.Point(338, 33);
            this.tbx_所属工厂.Name = "tbx_所属工厂";
            this.tbx_所属工厂.Size = new System.Drawing.Size(100, 22);
            this.tbx_所属工厂.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_确定);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(5, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(925, 85);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基本信息";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "仓库地址";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(87, 40);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(246, 22);
            this.textBox3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(401, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "负责人";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(480, 40);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 22);
            this.textBox4.TabIndex = 3;
            // 
            // gpx_库位
            // 
            this.gpx_库位.Controls.Add(this.btn_删除);
            this.gpx_库位.Controls.Add(this.btn_修改);
            this.gpx_库位.Controls.Add(this.btn_新建);
            this.gpx_库位.Controls.Add(this.dgv_库位);
            this.gpx_库位.Location = new System.Drawing.Point(7, 153);
            this.gpx_库位.Name = "gpx_库位";
            this.gpx_库位.Size = new System.Drawing.Size(922, 258);
            this.gpx_库位.TabIndex = 5;
            this.gpx_库位.TabStop = false;
            this.gpx_库位.Text = "库位信息";
            // 
            // dgv_库位
            // 
            this.dgv_库位.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_库位.Location = new System.Drawing.Point(10, 57);
            this.dgv_库位.Name = "dgv_库位";
            this.dgv_库位.RowTemplate.Height = 24;
            this.dgv_库位.Size = new System.Drawing.Size(912, 191);
            this.dgv_库位.TabIndex = 0;
            // 
            // btn_新建
            // 
            this.btn_新建.Location = new System.Drawing.Point(563, 21);
            this.btn_新建.Name = "btn_新建";
            this.btn_新建.Size = new System.Drawing.Size(100, 30);
            this.btn_新建.TabIndex = 1;
            this.btn_新建.Text = "新建库位";
            this.btn_新建.UseVisualStyleBackColor = true;
            this.btn_新建.Click += new System.EventHandler(this.btn_新建_Click);
            // 
            // btn_修改
            // 
            this.btn_修改.Location = new System.Drawing.Point(697, 21);
            this.btn_修改.Name = "btn_修改";
            this.btn_修改.Size = new System.Drawing.Size(75, 30);
            this.btn_修改.TabIndex = 2;
            this.btn_修改.Text = "修改";
            this.btn_修改.UseVisualStyleBackColor = true;
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(802, 21);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(97, 30);
            this.btn_删除.TabIndex = 3;
            this.btn_删除.Text = "删除库位";
            this.btn_删除.UseVisualStyleBackColor = true;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(804, 40);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(85, 39);
            this.btn_确定.TabIndex = 4;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            // 
            // StockInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 413);
            this.Controls.Add(this.groupBox1);
            this.Name = "StockInformation";
            this.Text = "仓库信息";
            this.Load += new System.EventHandler(this.StockInformation_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gpx_库位.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_库位)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gpx_库位;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_修改;
        private System.Windows.Forms.Button btn_新建;
        private System.Windows.Forms.DataGridView dgv_库位;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_所属工厂;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_仓库编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_确定;
    }
}