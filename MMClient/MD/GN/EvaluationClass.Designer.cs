﻿namespace MMClient.MD.GN
{
    partial class EvaluationClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.dgv_评估类 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_评估类)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.dgv_评估类);
            this.groupBox1.Location = new System.Drawing.Point(3, 2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1258, 460);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护评估类";
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(19, 144);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(92, 27);
            this.btn_删除.TabIndex = 5;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(19, 98);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(92, 28);
            this.btn_新条目.TabIndex = 4;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(19, 51);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(92, 31);
            this.btn_刷新.TabIndex = 3;
            this.btn_刷新.Text = "刷新数据";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // dgv_评估类
            // 
            this.dgv_评估类.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_评估类.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_评估类.Location = new System.Drawing.Point(193, 51);
            this.dgv_评估类.Name = "dgv_评估类";
            this.dgv_评估类.RowTemplate.Height = 24;
            this.dgv_评估类.Size = new System.Drawing.Size(1059, 381);
            this.dgv_评估类.TabIndex = 0;
            // 
            // EvaluationClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.groupBox1);
            this.Name = "EvaluationClass";
            this.Text = "维护评估类信息";
            this.Load += new System.EventHandler(this.EvaluationClass_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_评估类)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.DataGridView dgv_评估类;

    }
}