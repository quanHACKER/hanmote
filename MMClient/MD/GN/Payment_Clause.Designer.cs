﻿namespace MMClient.MD.GN
{
    partial class Payment_Clause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新数据 = new System.Windows.Forms.Button();
            this.dgv_付款条件 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_付款条件)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.btn_刷新数据);
            this.groupBox1.Controls.Add(this.dgv_付款条件);
            this.groupBox1.Location = new System.Drawing.Point(0, 2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1000, 444);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "付款条件";
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(6, 147);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(90, 27);
            this.btn_删除.TabIndex = 6;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(6, 101);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(90, 28);
            this.btn_新条目.TabIndex = 5;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新数据
            // 
            this.btn_刷新数据.Location = new System.Drawing.Point(6, 53);
            this.btn_刷新数据.Name = "btn_刷新数据";
            this.btn_刷新数据.Size = new System.Drawing.Size(90, 31);
            this.btn_刷新数据.TabIndex = 4;
            this.btn_刷新数据.Text = "刷新数据";
            this.btn_刷新数据.UseVisualStyleBackColor = true;
            this.btn_刷新数据.Click += new System.EventHandler(this.btn_刷新数据_Click);
            // 
            // dgv_付款条件
            // 
            this.dgv_付款条件.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_付款条件.Location = new System.Drawing.Point(165, 18);
            this.dgv_付款条件.Name = "dgv_付款条件";
            this.dgv_付款条件.RowTemplate.Height = 24;
            this.dgv_付款条件.Size = new System.Drawing.Size(834, 414);
            this.dgv_付款条件.TabIndex = 0;
            this.dgv_付款条件.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_付款条件_CellDoubleClick);
            // 
            // Payment_Clause
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 458);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Payment_Clause";
            this.Text = "付款条件信息";
            this.Load += new System.EventHandler(this.Payment_Clause_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_付款条件)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_付款条件;
        private System.Windows.Forms.Button btn_刷新数据;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_删除;
    }
}