﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.GN
{
    public partial class Payment_Clause : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Payment_Clause()
        {
            InitializeComponent();
        }

        private void Payment_Clause_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllPaymentClause();
            dgv_付款条件.DataSource = dt;
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllPaymentClause();
            dgv_付款条件.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewClause nclause = new NewClause();
            nclause.Show();
        }

        private void dgv_付款条件_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string clauseID = this.dgv_付款条件.CurrentRow.Cells[0].Value.ToString();
            ClauseInformation cinfo = new ClauseInformation(clauseID);
            cinfo.Show();
        }
    }
}
