﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class Stock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Stock()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewStock nsk = new NewStock();
            nsk.Show();
        }

        private void Stock_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllStockInformation();
            dgv_仓库信息.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllStockInformation();
            dgv_仓库信息.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_仓库信息.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                string StockID = dgv_仓库信息.CurrentRow.Cells[0].Value.ToString();
                gn.DeleteStock(StockID);
                dgv_仓库信息.Rows.Remove(dgv_仓库信息.CurrentRow);
                MessageUtil.ShowTips("删除成功");
            }
        }

        private void dgv_仓库信息_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //string stockid;
            //string factoryid;
            //int rindex = dgv_仓库信息.CurrentCell.RowIndex;
            //stockid = dgv_仓库信息[0,rindex].Value.ToString();
            //factoryid = dgv_仓库信息[2,rindex].Value.ToString();
            //StockInformation ski = new StockInformation(stockid,factoryid);
            //ski.Show();
        }

        private void dgv_仓库信息_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string stockid;
            string factoryid;
            int rindex = dgv_仓库信息.CurrentCell.RowIndex;
            stockid = dgv_仓库信息[0, rindex].Value.ToString();
            factoryid = dgv_仓库信息[2, rindex].Value.ToString();
            StockInformation ski = new StockInformation(stockid, factoryid);
            ski.Show();
        }

    }
}
