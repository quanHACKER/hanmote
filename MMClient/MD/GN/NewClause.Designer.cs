﻿namespace MMClient.MD.GN
{
    partial class NewClause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbx_条件编码 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbx_描述 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_取消 = new System.Windows.Forms.Button();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.tbx_天数3 = new System.Windows.Forms.TextBox();
            this.tbx_天数2 = new System.Windows.Forms.TextBox();
            this.tbx_天数1 = new System.Windows.Forms.TextBox();
            this.tbx_折扣2 = new System.Windows.Forms.TextBox();
            this.tbx_折扣1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tbx_条件编码);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbx_描述);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btn_取消);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.tbx_天数3);
            this.groupBox1.Controls.Add(this.tbx_天数2);
            this.groupBox1.Controls.Add(this.tbx_天数1);
            this.groupBox1.Controls.Add(this.tbx_折扣2);
            this.groupBox1.Controls.Add(this.tbx_折扣1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(591, 356);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建付款条件";
            // 
            // tbx_条件编码
            // 
            this.tbx_条件编码.Location = new System.Drawing.Point(472, 72);
            this.tbx_条件编码.Name = "tbx_条件编码";
            this.tbx_条件编码.Size = new System.Drawing.Size(100, 22);
            this.tbx_条件编码.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(374, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 17);
            this.label10.TabIndex = 17;
            this.label10.Text = "条件编码";
            // 
            // tbx_描述
            // 
            this.tbx_描述.Location = new System.Drawing.Point(97, 213);
            this.tbx_描述.Name = "tbx_描述";
            this.tbx_描述.Size = new System.Drawing.Size(320, 22);
            this.tbx_描述.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "描述";
            // 
            // btn_取消
            // 
            this.btn_取消.Location = new System.Drawing.Point(448, 296);
            this.btn_取消.Name = "btn_取消";
            this.btn_取消.Size = new System.Drawing.Size(72, 45);
            this.btn_取消.TabIndex = 14;
            this.btn_取消.Text = "取消";
            this.btn_取消.UseVisualStyleBackColor = true;
            this.btn_取消.Click += new System.EventHandler(this.btn_取消_Click);
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(353, 296);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 13;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // tbx_天数3
            // 
            this.tbx_天数3.Location = new System.Drawing.Point(230, 147);
            this.tbx_天数3.Name = "tbx_天数3";
            this.tbx_天数3.Size = new System.Drawing.Size(53, 22);
            this.tbx_天数3.TabIndex = 12;
            // 
            // tbx_天数2
            // 
            this.tbx_天数2.Location = new System.Drawing.Point(230, 111);
            this.tbx_天数2.Name = "tbx_天数2";
            this.tbx_天数2.Size = new System.Drawing.Size(53, 22);
            this.tbx_天数2.TabIndex = 11;
            // 
            // tbx_天数1
            // 
            this.tbx_天数1.Location = new System.Drawing.Point(230, 72);
            this.tbx_天数1.Name = "tbx_天数1";
            this.tbx_天数1.Size = new System.Drawing.Size(53, 22);
            this.tbx_天数1.TabIndex = 10;
            // 
            // tbx_折扣2
            // 
            this.tbx_折扣2.Location = new System.Drawing.Point(107, 111);
            this.tbx_折扣2.Name = "tbx_折扣2";
            this.tbx_折扣2.Size = new System.Drawing.Size(53, 22);
            this.tbx_折扣2.TabIndex = 9;
            // 
            // tbx_折扣1
            // 
            this.tbx_折扣1.Location = new System.Drawing.Point(107, 72);
            this.tbx_折扣1.Name = "tbx_折扣1";
            this.tbx_折扣1.Size = new System.Drawing.Size(53, 22);
            this.tbx_折扣1.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(236, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "天数";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(166, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(166, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(110, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "百分比";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "3.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "2.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "1.";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 37);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "期限";
            // 
            // NewClause
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 372);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewClause";
            this.Text = "新建付款条件";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_天数3;
        private System.Windows.Forms.TextBox tbx_天数2;
        private System.Windows.Forms.TextBox tbx_天数1;
        private System.Windows.Forms.TextBox tbx_折扣2;
        private System.Windows.Forms.TextBox tbx_折扣1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_取消;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.TextBox tbx_描述;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbx_条件编码;
        private System.Windows.Forms.Label label10;
    }
}