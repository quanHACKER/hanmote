﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using WeifenLuo.WinFormsUI.Docking;
using System.Windows.Forms;

namespace MMClient.MD
{
    public partial class MenuUser : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        MainDataM mdm = new MainDataM();
        DockPanel dp = null;
        public MenuUser()
        {
            InitializeComponent();
        }
        public MenuUser(DockPanel dp)
        {
            InitializeComponent();
            this.dp = dp;
        }

        private void tV_md_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //sMessageBox.Show("111s");
            MTRecord mt = new MTRecord();
            mt.Show(this.dp, DockState.DockLeft);

        }
    }
       
}
