﻿namespace MMClient.MD
{
    partial class Specialist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.tabControl10 = new System.Windows.Forms.TabControl();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.dataGridView17 = new System.Windows.Forms.DataGridView();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.panel27 = new System.Windows.Forms.Panel();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.dataGridView18 = new System.Windows.Forms.DataGridView();
            this.panel28 = new System.Windows.Forms.Panel();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.listBox108 = new System.Windows.Forms.ListBox();
            this.label173 = new System.Windows.Forms.Label();
            this.listBox109 = new System.Windows.Forms.ListBox();
            this.label174 = new System.Windows.Forms.Label();
            this.listBox110 = new System.Windows.Forms.ListBox();
            this.label175 = new System.Windows.Forms.Label();
            this.listBox111 = new System.Windows.Forms.ListBox();
            this.label176 = new System.Windows.Forms.Label();
            this.listBox112 = new System.Windows.Forms.ListBox();
            this.label177 = new System.Windows.Forms.Label();
            this.listBox113 = new System.Windows.Forms.ListBox();
            this.label178 = new System.Windows.Forms.Label();
            this.listBox114 = new System.Windows.Forms.ListBox();
            this.label179 = new System.Windows.Forms.Label();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.button60 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label182 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label183 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label184 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.label186 = new System.Windows.Forms.Label();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label187 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label188 = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this.panel29 = new System.Windows.Forms.Panel();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.listBox115 = new System.Windows.Forms.ListBox();
            this.label190 = new System.Windows.Forms.Label();
            this.listBox116 = new System.Windows.Forms.ListBox();
            this.label191 = new System.Windows.Forms.Label();
            this.listBox117 = new System.Windows.Forms.ListBox();
            this.label192 = new System.Windows.Forms.Label();
            this.listBox118 = new System.Windows.Forms.ListBox();
            this.label193 = new System.Windows.Forms.Label();
            this.listBox119 = new System.Windows.Forms.ListBox();
            this.label194 = new System.Windows.Forms.Label();
            this.listBox120 = new System.Windows.Forms.ListBox();
            this.label195 = new System.Windows.Forms.Label();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.tabPage40 = new System.Windows.Forms.TabPage();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this.dataGridView19 = new System.Windows.Forms.DataGridView();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.dataGridView20 = new System.Windows.Forms.DataGridView();
            this.panel30 = new System.Windows.Forms.Panel();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.label196 = new System.Windows.Forms.Label();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.listBox121 = new System.Windows.Forms.ListBox();
            this.label197 = new System.Windows.Forms.Label();
            this.listBox122 = new System.Windows.Forms.ListBox();
            this.label198 = new System.Windows.Forms.Label();
            this.listBox123 = new System.Windows.Forms.ListBox();
            this.label199 = new System.Windows.Forms.Label();
            this.listBox124 = new System.Windows.Forms.ListBox();
            this.label200 = new System.Windows.Forms.Label();
            this.listBox125 = new System.Windows.Forms.ListBox();
            this.label201 = new System.Windows.Forms.Label();
            this.listBox126 = new System.Windows.Forms.ListBox();
            this.label202 = new System.Windows.Forms.Label();
            this.listBox127 = new System.Windows.Forms.ListBox();
            this.label203 = new System.Windows.Forms.Label();
            this.tabPage41 = new System.Windows.Forms.TabPage();
            this.tabControl4.SuspendLayout();
            this.tabPage18.SuspendLayout();
            this.groupBox35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.groupBox36.SuspendLayout();
            this.tabControl10.SuspendLayout();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView18)).BeginInit();
            this.panel28.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.tabPage39.SuspendLayout();
            this.panel29.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.tabPage40.SuspendLayout();
            this.groupBox40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView19)).BeginInit();
            this.groupBox41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView20)).BeginInit();
            this.panel30.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage18);
            this.tabControl4.Controls.Add(this.tabPage21);
            this.tabControl4.Controls.Add(this.tabPage39);
            this.tabControl4.Controls.Add(this.tabPage40);
            this.tabControl4.Controls.Add(this.tabPage41);
            this.tabControl4.Location = new System.Drawing.Point(3, 2);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(916, 668);
            this.tabControl4.TabIndex = 2;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.groupBox35);
            this.tabPage18.Controls.Add(this.groupBox36);
            this.tabPage18.Controls.Add(this.panel27);
            this.tabPage18.Controls.Add(this.panel28);
            this.tabPage18.Location = new System.Drawing.Point(4, 22);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(908, 642);
            this.tabPage18.TabIndex = 0;
            this.tabPage18.Text = "查询";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.chart4);
            this.groupBox35.Location = new System.Drawing.Point(564, 238);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(341, 378);
            this.groupBox35.TabIndex = 6;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "数据分析";
            // 
            // chart4
            // 
            chartArea1.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart4.Legends.Add(legend1);
            this.chart4.Location = new System.Drawing.Point(6, 20);
            this.chart4.Name = "chart4";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart4.Series.Add(series1);
            this.chart4.Size = new System.Drawing.Size(315, 352);
            this.chart4.TabIndex = 0;
            this.chart4.Text = "chart4";
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.tabControl10);
            this.groupBox36.Location = new System.Drawing.Point(3, 428);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(554, 192);
            this.groupBox36.TabIndex = 5;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "详细信息";
            // 
            // tabControl10
            // 
            this.tabControl10.Controls.Add(this.tabPage19);
            this.tabControl10.Controls.Add(this.tabPage20);
            this.tabControl10.Location = new System.Drawing.Point(1, 20);
            this.tabControl10.Name = "tabControl10";
            this.tabControl10.SelectedIndex = 0;
            this.tabControl10.Size = new System.Drawing.Size(590, 172);
            this.tabControl10.TabIndex = 0;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.dataGridView17);
            this.tabPage19.Location = new System.Drawing.Point(4, 22);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(582, 146);
            this.tabPage19.TabIndex = 0;
            this.tabPage19.Text = "详细记录";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // dataGridView17
            // 
            this.dataGridView17.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView17.Location = new System.Drawing.Point(1, 3);
            this.dataGridView17.Name = "dataGridView17";
            this.dataGridView17.RowTemplate.Height = 23;
            this.dataGridView17.Size = new System.Drawing.Size(539, 137);
            this.dataGridView17.TabIndex = 0;
            // 
            // tabPage20
            // 
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(582, 146);
            this.tabPage20.TabIndex = 1;
            this.tabPage20.Text = "其他项";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.button55);
            this.panel27.Controls.Add(this.button56);
            this.panel27.Controls.Add(this.button57);
            this.panel27.Controls.Add(this.dataGridView18);
            this.panel27.Location = new System.Drawing.Point(3, 237);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(554, 185);
            this.panel27.TabIndex = 4;
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(377, 159);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(75, 23);
            this.button55.TabIndex = 3;
            this.button55.Text = "导出数据";
            this.button55.UseVisualStyleBackColor = true;
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(206, 159);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(75, 23);
            this.button56.TabIndex = 2;
            this.button56.Text = "数据分析";
            this.button56.UseVisualStyleBackColor = true;
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(50, 159);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(75, 23);
            this.button57.TabIndex = 1;
            this.button57.Text = "删除";
            this.button57.UseVisualStyleBackColor = true;
            // 
            // dataGridView18
            // 
            this.dataGridView18.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView18.Location = new System.Drawing.Point(3, 4);
            this.dataGridView18.Name = "dataGridView18";
            this.dataGridView18.RowTemplate.Height = 23;
            this.dataGridView18.Size = new System.Drawing.Size(548, 150);
            this.dataGridView18.TabIndex = 0;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.groupBox37);
            this.panel28.Location = new System.Drawing.Point(3, 6);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(906, 225);
            this.panel28.TabIndex = 3;
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.textBox52);
            this.groupBox37.Controls.Add(this.label172);
            this.groupBox37.Controls.Add(this.button58);
            this.groupBox37.Controls.Add(this.button59);
            this.groupBox37.Controls.Add(this.listBox108);
            this.groupBox37.Controls.Add(this.label173);
            this.groupBox37.Controls.Add(this.listBox109);
            this.groupBox37.Controls.Add(this.label174);
            this.groupBox37.Controls.Add(this.listBox110);
            this.groupBox37.Controls.Add(this.label175);
            this.groupBox37.Controls.Add(this.listBox111);
            this.groupBox37.Controls.Add(this.label176);
            this.groupBox37.Controls.Add(this.listBox112);
            this.groupBox37.Controls.Add(this.label177);
            this.groupBox37.Controls.Add(this.listBox113);
            this.groupBox37.Controls.Add(this.label178);
            this.groupBox37.Controls.Add(this.listBox114);
            this.groupBox37.Controls.Add(this.label179);
            this.groupBox37.Location = new System.Drawing.Point(7, 3);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(631, 219);
            this.groupBox37.TabIndex = 18;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "条件选择";
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(459, 170);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(117, 21);
            this.textBox52.TabIndex = 35;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(344, 170);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(65, 12);
            this.label172.TabIndex = 34;
            this.label172.Text = "最大命中数";
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(231, 170);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(75, 23);
            this.button58.TabIndex = 33;
            this.button58.Text = "清除";
            this.button58.UseVisualStyleBackColor = true;
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(96, 170);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(75, 23);
            this.button59.TabIndex = 32;
            this.button59.Text = "查询";
            this.button59.UseVisualStyleBackColor = true;
            // 
            // listBox108
            // 
            this.listBox108.FormattingEnabled = true;
            this.listBox108.ItemHeight = 12;
            this.listBox108.Location = new System.Drawing.Point(154, 148);
            this.listBox108.Name = "listBox108";
            this.listBox108.Size = new System.Drawing.Size(120, 16);
            this.listBox108.TabIndex = 31;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(40, 148);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(29, 12);
            this.label173.TabIndex = 30;
            this.label173.Text = "其他";
            // 
            // listBox109
            // 
            this.listBox109.FormattingEnabled = true;
            this.listBox109.ItemHeight = 12;
            this.listBox109.Location = new System.Drawing.Point(459, 105);
            this.listBox109.Name = "listBox109";
            this.listBox109.Size = new System.Drawing.Size(120, 16);
            this.listBox109.TabIndex = 29;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(345, 105);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(17, 12);
            this.label174.TabIndex = 28;
            this.label174.Text = "至";
            // 
            // listBox110
            // 
            this.listBox110.FormattingEnabled = true;
            this.listBox110.ItemHeight = 12;
            this.listBox110.Location = new System.Drawing.Point(154, 105);
            this.listBox110.Name = "listBox110";
            this.listBox110.Size = new System.Drawing.Size(120, 16);
            this.listBox110.TabIndex = 27;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(40, 105);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(53, 12);
            this.label175.TabIndex = 26;
            this.label175.Text = "物料编码";
            // 
            // listBox111
            // 
            this.listBox111.FormattingEnabled = true;
            this.listBox111.ItemHeight = 12;
            this.listBox111.Location = new System.Drawing.Point(459, 60);
            this.listBox111.Name = "listBox111";
            this.listBox111.Size = new System.Drawing.Size(120, 16);
            this.listBox111.TabIndex = 25;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(345, 60);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(17, 12);
            this.label176.TabIndex = 24;
            this.label176.Text = "至";
            // 
            // listBox112
            // 
            this.listBox112.FormattingEnabled = true;
            this.listBox112.ItemHeight = 12;
            this.listBox112.Location = new System.Drawing.Point(154, 60);
            this.listBox112.Name = "listBox112";
            this.listBox112.Size = new System.Drawing.Size(120, 16);
            this.listBox112.TabIndex = 23;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(40, 60);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(53, 12);
            this.label177.TabIndex = 22;
            this.label177.Text = "专家名称";
            // 
            // listBox113
            // 
            this.listBox113.FormattingEnabled = true;
            this.listBox113.ItemHeight = 12;
            this.listBox113.Location = new System.Drawing.Point(459, 20);
            this.listBox113.Name = "listBox113";
            this.listBox113.Size = new System.Drawing.Size(120, 16);
            this.listBox113.TabIndex = 21;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(345, 20);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(17, 12);
            this.label178.TabIndex = 20;
            this.label178.Text = "至";
            // 
            // listBox114
            // 
            this.listBox114.FormattingEnabled = true;
            this.listBox114.ItemHeight = 12;
            this.listBox114.Location = new System.Drawing.Point(154, 20);
            this.listBox114.Name = "listBox114";
            this.listBox114.Size = new System.Drawing.Size(120, 16);
            this.listBox114.TabIndex = 19;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(40, 20);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(53, 12);
            this.label179.TabIndex = 18;
            this.label179.Text = "专家编码";
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.groupBox38);
            this.tabPage21.Location = new System.Drawing.Point(4, 22);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(908, 642);
            this.tabPage21.TabIndex = 1;
            this.tabPage21.Text = "创建";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.button60);
            this.groupBox38.Controls.Add(this.button61);
            this.groupBox38.Controls.Add(this.textBox55);
            this.groupBox38.Controls.Add(this.label182);
            this.groupBox38.Controls.Add(this.textBox56);
            this.groupBox38.Controls.Add(this.label183);
            this.groupBox38.Controls.Add(this.textBox57);
            this.groupBox38.Controls.Add(this.label184);
            this.groupBox38.Controls.Add(this.textBox58);
            this.groupBox38.Controls.Add(this.label185);
            this.groupBox38.Controls.Add(this.textBox59);
            this.groupBox38.Controls.Add(this.label186);
            this.groupBox38.Controls.Add(this.textBox60);
            this.groupBox38.Controls.Add(this.label187);
            this.groupBox38.Controls.Add(this.textBox61);
            this.groupBox38.Controls.Add(this.label188);
            this.groupBox38.Controls.Add(this.textBox62);
            this.groupBox38.Controls.Add(this.label189);
            this.groupBox38.Location = new System.Drawing.Point(6, 6);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(656, 619);
            this.groupBox38.TabIndex = 1;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "创建新数据";
            // 
            // button60
            // 
            this.button60.Location = new System.Drawing.Point(340, 556);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(75, 23);
            this.button60.TabIndex = 21;
            this.button60.Text = "重置";
            this.button60.UseVisualStyleBackColor = true;
            // 
            // button61
            // 
            this.button61.Location = new System.Drawing.Point(78, 556);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(75, 23);
            this.button61.TabIndex = 20;
            this.button61.Text = "确定";
            this.button61.UseVisualStyleBackColor = true;
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(409, 213);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(128, 21);
            this.textBox55.TabIndex = 15;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(338, 218);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(53, 12);
            this.label182.TabIndex = 14;
            this.label182.Text = "物料编码";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(78, 213);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(128, 21);
            this.textBox56.TabIndex = 13;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(7, 218);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(29, 12);
            this.label183.TabIndex = 12;
            this.label183.Text = "网址";
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(409, 151);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(128, 21);
            this.textBox57.TabIndex = 11;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(338, 156);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(53, 12);
            this.label184.TabIndex = 10;
            this.label184.Text = "详细地址";
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(78, 147);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(128, 21);
            this.textBox58.TabIndex = 9;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(7, 152);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(53, 12);
            this.label185.TabIndex = 8;
            this.label185.Text = "所在国家";
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(409, 80);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(128, 21);
            this.textBox59.TabIndex = 7;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(338, 85);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(53, 12);
            this.label186.TabIndex = 6;
            this.label186.Text = "所属单位";
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(77, 82);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(128, 21);
            this.textBox60.TabIndex = 5;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(6, 87);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(53, 12);
            this.label187.TabIndex = 4;
            this.label187.Text = "专家分类";
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(409, 19);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(128, 21);
            this.textBox61.TabIndex = 3;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(338, 24);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(53, 12);
            this.label188.TabIndex = 2;
            this.label188.Text = "专家名称";
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(78, 21);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(128, 21);
            this.textBox62.TabIndex = 1;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(7, 26);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(53, 12);
            this.label189.TabIndex = 0;
            this.label189.Text = "专家编码";
            // 
            // tabPage39
            // 
            this.tabPage39.Controls.Add(this.panel29);
            this.tabPage39.Location = new System.Drawing.Point(4, 22);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Size = new System.Drawing.Size(908, 642);
            this.tabPage39.TabIndex = 2;
            this.tabPage39.Text = "修改";
            this.tabPage39.UseVisualStyleBackColor = true;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.groupBox39);
            this.panel29.Location = new System.Drawing.Point(3, 6);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(651, 640);
            this.panel29.TabIndex = 1;
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this.listBox115);
            this.groupBox39.Controls.Add(this.label190);
            this.groupBox39.Controls.Add(this.listBox116);
            this.groupBox39.Controls.Add(this.label191);
            this.groupBox39.Controls.Add(this.listBox117);
            this.groupBox39.Controls.Add(this.label192);
            this.groupBox39.Controls.Add(this.listBox118);
            this.groupBox39.Controls.Add(this.label193);
            this.groupBox39.Controls.Add(this.listBox119);
            this.groupBox39.Controls.Add(this.label194);
            this.groupBox39.Controls.Add(this.listBox120);
            this.groupBox39.Controls.Add(this.label195);
            this.groupBox39.Controls.Add(this.button62);
            this.groupBox39.Controls.Add(this.button63);
            this.groupBox39.Location = new System.Drawing.Point(3, 3);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(640, 616);
            this.groupBox39.TabIndex = 1;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "修改新数据";
            // 
            // listBox115
            // 
            this.listBox115.FormattingEnabled = true;
            this.listBox115.ItemHeight = 12;
            this.listBox115.Location = new System.Drawing.Point(436, 113);
            this.listBox115.Name = "listBox115";
            this.listBox115.Size = new System.Drawing.Size(120, 16);
            this.listBox115.TabIndex = 35;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(322, 113);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(41, 12);
            this.label190.TabIndex = 34;
            this.label190.Text = "修改至";
            // 
            // listBox116
            // 
            this.listBox116.FormattingEnabled = true;
            this.listBox116.ItemHeight = 12;
            this.listBox116.Location = new System.Drawing.Point(131, 113);
            this.listBox116.Name = "listBox116";
            this.listBox116.Size = new System.Drawing.Size(120, 16);
            this.listBox116.TabIndex = 33;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(17, 113);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(53, 12);
            this.label191.TabIndex = 32;
            this.label191.Text = "物料编码";
            // 
            // listBox117
            // 
            this.listBox117.FormattingEnabled = true;
            this.listBox117.ItemHeight = 12;
            this.listBox117.Location = new System.Drawing.Point(436, 68);
            this.listBox117.Name = "listBox117";
            this.listBox117.Size = new System.Drawing.Size(120, 16);
            this.listBox117.TabIndex = 31;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(322, 68);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(41, 12);
            this.label192.TabIndex = 30;
            this.label192.Text = "修改至";
            // 
            // listBox118
            // 
            this.listBox118.FormattingEnabled = true;
            this.listBox118.ItemHeight = 12;
            this.listBox118.Location = new System.Drawing.Point(131, 68);
            this.listBox118.Name = "listBox118";
            this.listBox118.Size = new System.Drawing.Size(120, 16);
            this.listBox118.TabIndex = 29;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(17, 68);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(53, 12);
            this.label193.TabIndex = 28;
            this.label193.Text = "专家名称";
            // 
            // listBox119
            // 
            this.listBox119.FormattingEnabled = true;
            this.listBox119.ItemHeight = 12;
            this.listBox119.Location = new System.Drawing.Point(436, 28);
            this.listBox119.Name = "listBox119";
            this.listBox119.Size = new System.Drawing.Size(120, 16);
            this.listBox119.TabIndex = 27;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(322, 28);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(41, 12);
            this.label194.TabIndex = 26;
            this.label194.Text = "修改至";
            // 
            // listBox120
            // 
            this.listBox120.FormattingEnabled = true;
            this.listBox120.ItemHeight = 12;
            this.listBox120.Location = new System.Drawing.Point(131, 28);
            this.listBox120.Name = "listBox120";
            this.listBox120.Size = new System.Drawing.Size(120, 16);
            this.listBox120.TabIndex = 25;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(17, 28);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(53, 12);
            this.label195.TabIndex = 24;
            this.label195.Text = "专家编码";
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(392, 558);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(75, 23);
            this.button62.TabIndex = 23;
            this.button62.Text = "重置";
            this.button62.UseVisualStyleBackColor = true;
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(130, 558);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(75, 23);
            this.button63.TabIndex = 22;
            this.button63.Text = "确定";
            this.button63.UseVisualStyleBackColor = true;
            // 
            // tabPage40
            // 
            this.tabPage40.Controls.Add(this.groupBox40);
            this.tabPage40.Controls.Add(this.groupBox41);
            this.tabPage40.Controls.Add(this.panel30);
            this.tabPage40.Location = new System.Drawing.Point(4, 22);
            this.tabPage40.Name = "tabPage40";
            this.tabPage40.Size = new System.Drawing.Size(908, 642);
            this.tabPage40.TabIndex = 3;
            this.tabPage40.Text = "分发";
            this.tabPage40.UseVisualStyleBackColor = true;
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this.dataGridView19);
            this.groupBox40.Location = new System.Drawing.Point(3, 471);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(634, 163);
            this.groupBox40.TabIndex = 6;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "分发的子系统或者单位";
            // 
            // dataGridView19
            // 
            this.dataGridView19.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView19.Location = new System.Drawing.Point(0, 12);
            this.dataGridView19.Name = "dataGridView19";
            this.dataGridView19.RowTemplate.Height = 23;
            this.dataGridView19.Size = new System.Drawing.Size(628, 150);
            this.dataGridView19.TabIndex = 0;
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this.dataGridView20);
            this.groupBox41.Location = new System.Drawing.Point(3, 234);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(636, 231);
            this.groupBox41.TabIndex = 5;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "查询结果";
            // 
            // dataGridView20
            // 
            this.dataGridView20.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView20.Location = new System.Drawing.Point(7, 21);
            this.dataGridView20.Name = "dataGridView20";
            this.dataGridView20.RowTemplate.Height = 23;
            this.dataGridView20.Size = new System.Drawing.Size(623, 204);
            this.dataGridView20.TabIndex = 0;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.groupBox42);
            this.panel30.Location = new System.Drawing.Point(3, 3);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(906, 225);
            this.panel30.TabIndex = 4;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.textBox63);
            this.groupBox42.Controls.Add(this.label196);
            this.groupBox42.Controls.Add(this.button64);
            this.groupBox42.Controls.Add(this.button65);
            this.groupBox42.Controls.Add(this.listBox121);
            this.groupBox42.Controls.Add(this.label197);
            this.groupBox42.Controls.Add(this.listBox122);
            this.groupBox42.Controls.Add(this.label198);
            this.groupBox42.Controls.Add(this.listBox123);
            this.groupBox42.Controls.Add(this.label199);
            this.groupBox42.Controls.Add(this.listBox124);
            this.groupBox42.Controls.Add(this.label200);
            this.groupBox42.Controls.Add(this.listBox125);
            this.groupBox42.Controls.Add(this.label201);
            this.groupBox42.Controls.Add(this.listBox126);
            this.groupBox42.Controls.Add(this.label202);
            this.groupBox42.Controls.Add(this.listBox127);
            this.groupBox42.Controls.Add(this.label203);
            this.groupBox42.Location = new System.Drawing.Point(7, 3);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(631, 219);
            this.groupBox42.TabIndex = 18;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "条件选择";
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(459, 170);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(117, 21);
            this.textBox63.TabIndex = 35;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(344, 170);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(65, 12);
            this.label196.TabIndex = 34;
            this.label196.Text = "最大命中数";
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(231, 170);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(75, 23);
            this.button64.TabIndex = 33;
            this.button64.Text = "清除";
            this.button64.UseVisualStyleBackColor = true;
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(96, 170);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(75, 23);
            this.button65.TabIndex = 32;
            this.button65.Text = "查询";
            this.button65.UseVisualStyleBackColor = true;
            // 
            // listBox121
            // 
            this.listBox121.FormattingEnabled = true;
            this.listBox121.ItemHeight = 12;
            this.listBox121.Location = new System.Drawing.Point(154, 148);
            this.listBox121.Name = "listBox121";
            this.listBox121.Size = new System.Drawing.Size(120, 16);
            this.listBox121.TabIndex = 31;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(40, 148);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(29, 12);
            this.label197.TabIndex = 30;
            this.label197.Text = "其他";
            // 
            // listBox122
            // 
            this.listBox122.FormattingEnabled = true;
            this.listBox122.ItemHeight = 12;
            this.listBox122.Location = new System.Drawing.Point(459, 105);
            this.listBox122.Name = "listBox122";
            this.listBox122.Size = new System.Drawing.Size(120, 16);
            this.listBox122.TabIndex = 29;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(345, 105);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(17, 12);
            this.label198.TabIndex = 28;
            this.label198.Text = "至";
            // 
            // listBox123
            // 
            this.listBox123.FormattingEnabled = true;
            this.listBox123.ItemHeight = 12;
            this.listBox123.Location = new System.Drawing.Point(154, 105);
            this.listBox123.Name = "listBox123";
            this.listBox123.Size = new System.Drawing.Size(120, 16);
            this.listBox123.TabIndex = 27;
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(40, 105);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(29, 12);
            this.label199.TabIndex = 26;
            this.label199.Text = "分类";
            // 
            // listBox124
            // 
            this.listBox124.FormattingEnabled = true;
            this.listBox124.ItemHeight = 12;
            this.listBox124.Location = new System.Drawing.Point(459, 60);
            this.listBox124.Name = "listBox124";
            this.listBox124.Size = new System.Drawing.Size(120, 16);
            this.listBox124.TabIndex = 25;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(345, 60);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(17, 12);
            this.label200.TabIndex = 24;
            this.label200.Text = "至";
            // 
            // listBox125
            // 
            this.listBox125.FormattingEnabled = true;
            this.listBox125.ItemHeight = 12;
            this.listBox125.Location = new System.Drawing.Point(154, 60);
            this.listBox125.Name = "listBox125";
            this.listBox125.Size = new System.Drawing.Size(120, 16);
            this.listBox125.TabIndex = 23;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(40, 60);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(53, 12);
            this.label201.TabIndex = 22;
            this.label201.Text = "专家名称";
            // 
            // listBox126
            // 
            this.listBox126.FormattingEnabled = true;
            this.listBox126.ItemHeight = 12;
            this.listBox126.Location = new System.Drawing.Point(459, 20);
            this.listBox126.Name = "listBox126";
            this.listBox126.Size = new System.Drawing.Size(120, 16);
            this.listBox126.TabIndex = 21;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(345, 20);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(17, 12);
            this.label202.TabIndex = 20;
            this.label202.Text = "至";
            // 
            // listBox127
            // 
            this.listBox127.FormattingEnabled = true;
            this.listBox127.ItemHeight = 12;
            this.listBox127.Location = new System.Drawing.Point(154, 20);
            this.listBox127.Name = "listBox127";
            this.listBox127.Size = new System.Drawing.Size(120, 16);
            this.listBox127.TabIndex = 19;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(40, 20);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(53, 12);
            this.label203.TabIndex = 18;
            this.label203.Text = "专家编码";
            // 
            // tabPage41
            // 
            this.tabPage41.Location = new System.Drawing.Point(4, 22);
            this.tabPage41.Name = "tabPage41";
            this.tabPage41.Size = new System.Drawing.Size(908, 642);
            this.tabPage41.TabIndex = 4;
            this.tabPage41.Text = "维护";
            this.tabPage41.UseVisualStyleBackColor = true;
            // 
            // Specialist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 628);
            this.Controls.Add(this.tabControl4);
            this.Name = "Specialist";
            this.Text = "Specialist";
            this.tabControl4.ResumeLayout(false);
            this.tabPage18.ResumeLayout(false);
            this.groupBox35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.groupBox36.ResumeLayout(false);
            this.tabControl10.ResumeLayout(false);
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).EndInit();
            this.panel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView18)).EndInit();
            this.panel28.ResumeLayout(false);
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.tabPage21.ResumeLayout(false);
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.tabPage39.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.tabPage40.ResumeLayout(false);
            this.groupBox40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView19)).EndInit();
            this.groupBox41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView20)).EndInit();
            this.panel30.ResumeLayout(false);
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.TabControl tabControl10;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.DataGridView dataGridView17;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.DataGridView dataGridView18;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.ListBox listBox108;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.ListBox listBox109;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.ListBox listBox110;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.ListBox listBox111;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.ListBox listBox112;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.ListBox listBox113;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.ListBox listBox114;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.ListBox listBox115;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.ListBox listBox116;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.ListBox listBox117;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.ListBox listBox118;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.ListBox listBox119;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.ListBox listBox120;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.TabPage tabPage40;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.DataGridView dataGridView19;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.DataGridView dataGridView20;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.ListBox listBox121;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.ListBox listBox122;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.ListBox listBox123;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.ListBox listBox124;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.ListBox listBox125;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.ListBox listBox126;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.ListBox listBox127;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.TabPage tabPage41;
    }
}