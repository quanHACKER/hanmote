﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.MD.MT;
using MMClient.MD.SP;
using MMClient.MD.GN;
using Lib.Bll;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.FI
{
    public partial class actquery : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public actquery()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        private void btn_查询_Click(object sender, EventArgs e)
        {
            AccountBLL ACT = new AccountBLL();

            string code;
            DataTable dtinner;
            DataTable dtfrount;
            List<string> list = ACT.GetAllVocherID();
            cbm.FuzzyQury(cbb_财务凭证编码, list);
            code = cbb_财务凭证编码.Text;
            dtfrount = ACT.ReadAccountFrount(code);
            dtinner = ACT.ReadAccountInner(code);
            try
            {
                
                this.dgv_项目.Rows.Add();
                tbx_凭证编码.Text = dtfrount.Rows[0][0].ToString();
                tbx_凭证日期.Text = dtfrount.Rows[0][1].ToString();
                tbx_凭证类型.Text = "";
                tbx_公司代码.Text = dtfrount.Rows[0][7].ToString();
                this.dgv_项目.Rows[0].Cells[1].Value = dtinner.Rows[0][3].ToString();
                this.dgv_项目.Rows[0].Cells[3].Value = dtinner.Rows[0][4].ToString();
                this.dgv_项目.Rows[0].Cells[5].Value = dtinner.Rows[0][5].ToString();
                this.dgv_项目.Rows[0].Cells[6].Value = dtinner.Rows[0][6].ToString();
                this.dgv_项目.Rows[0].Cells[9].Value = dtinner.Rows[0][8].ToString();
                this.dgv_项目.Rows[0].Cells[10].Value = dtinner.Rows[0][9].ToString();
                this.dgv_项目.Rows[1].Cells[1].Value = dtinner.Rows[1][3].ToString();
                this.dgv_项目.Rows[1].Cells[3].Value = dtinner.Rows[1][4].ToString();
                this.dgv_项目.Rows[1].Cells[5].Value = dtinner.Rows[1][5].ToString();
                this.dgv_项目.Rows[1].Cells[6].Value = dtinner.Rows[1][6].ToString();
                this.dgv_项目.Rows[1].Cells[9].Value = dtinner.Rows[1][8].ToString();
                this.dgv_项目.Rows[1].Cells[10].Value = dtinner.Rows[1][9].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("所查询财务凭证不存在");
            }
        }
    }
}
