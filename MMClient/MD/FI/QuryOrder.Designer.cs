﻿namespace MMClient.MD.FI
{
    partial class QuryOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn_查看凭证信息 = new System.Windows.Forms.Button();
            this.btn_选择订单 = new System.Windows.Forms.Button();
            this.dgv_查询结果 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.订单数量2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbx_订单数量1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbx_总金额2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbx_总金额1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbx_供应商编码2 = new System.Windows.Forms.TextBox();
            this.tbx_供应商编码1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbx_物料编码2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbx_物料编码1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbx_订单编码2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_订单编码1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_精确订单编码 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_查询结果)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(2, 6);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1275, 619);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "订单-凭证信息查询";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn_查看凭证信息);
            this.groupBox5.Controls.Add(this.btn_选择订单);
            this.groupBox5.Controls.Add(this.dgv_查询结果);
            this.groupBox5.Location = new System.Drawing.Point(13, 319);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1255, 294);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "查询结果";
            // 
            // btn_查看凭证信息
            // 
            this.btn_查看凭证信息.Location = new System.Drawing.Point(958, 243);
            this.btn_查看凭证信息.Name = "btn_查看凭证信息";
            this.btn_查看凭证信息.Size = new System.Drawing.Size(182, 45);
            this.btn_查看凭证信息.TabIndex = 17;
            this.btn_查看凭证信息.Text = "查看凭证信息";
            this.btn_查看凭证信息.UseVisualStyleBackColor = true;
            this.btn_查看凭证信息.Click += new System.EventHandler(this.btn_查看凭证信息_Click);
            // 
            // btn_选择订单
            // 
            this.btn_选择订单.Location = new System.Drawing.Point(766, 243);
            this.btn_选择订单.Name = "btn_选择订单";
            this.btn_选择订单.Size = new System.Drawing.Size(142, 45);
            this.btn_选择订单.TabIndex = 16;
            this.btn_选择订单.Text = "选择订单";
            this.btn_选择订单.UseVisualStyleBackColor = true;
            this.btn_选择订单.Click += new System.EventHandler(this.btn_选择订单_Click);
            // 
            // dgv_查询结果
            // 
            this.dgv_查询结果.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_查询结果.Location = new System.Drawing.Point(5, 21);
            this.dgv_查询结果.Name = "dgv_查询结果";
            this.dgv_查询结果.RowTemplate.Height = 24;
            this.dgv_查询结果.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_查询结果.Size = new System.Drawing.Size(1244, 198);
            this.dgv_查询结果.TabIndex = 0;
            this.dgv_查询结果.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_查询结果_CellMouseDoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(7, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1261, 291);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查询条件";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.订单数量2);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.tbx_订单数量1);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.tbx_总金额2);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.tbx_总金额1);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.tbx_供应商编码2);
            this.groupBox4.Controls.Add(this.tbx_供应商编码1);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.tbx_物料编码2);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.tbx_物料编码1);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.tbx_订单编码2);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.tbx_订单编码1);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(9, 98);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1246, 128);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "模糊查询";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // 订单数量2
            // 
            this.订单数量2.Location = new System.Drawing.Point(875, 28);
            this.订单数量2.Name = "订单数量2";
            this.订单数量2.Size = new System.Drawing.Size(141, 22);
            this.订单数量2.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(833, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 17);
            this.label17.TabIndex = 24;
            this.label17.Text = "到";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(614, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 17);
            this.label16.TabIndex = 23;
            this.label16.Text = "从";
            // 
            // tbx_订单数量1
            // 
            this.tbx_订单数量1.Location = new System.Drawing.Point(677, 28);
            this.tbx_订单数量1.Name = "tbx_订单数量1";
            this.tbx_订单数量1.Size = new System.Drawing.Size(141, 22);
            this.tbx_订单数量1.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(553, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 17);
            this.label15.TabIndex = 21;
            this.label15.Text = "订单数量";
            // 
            // tbx_总金额2
            // 
            this.tbx_总金额2.Location = new System.Drawing.Point(875, 68);
            this.tbx_总金额2.Name = "tbx_总金额2";
            this.tbx_总金额2.Size = new System.Drawing.Size(141, 22);
            this.tbx_总金额2.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(833, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 17);
            this.label14.TabIndex = 19;
            this.label14.Text = "到";
            // 
            // tbx_总金额1
            // 
            this.tbx_总金额1.Location = new System.Drawing.Point(677, 68);
            this.tbx_总金额1.Name = "tbx_总金额1";
            this.tbx_总金额1.Size = new System.Drawing.Size(141, 22);
            this.tbx_总金额1.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(614, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "从";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(553, 69);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 17);
            this.label12.TabIndex = 16;
            this.label12.Text = "总金额";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "到";
            // 
            // tbx_供应商编码2
            // 
            this.tbx_供应商编码2.Location = new System.Drawing.Point(332, 97);
            this.tbx_供应商编码2.Name = "tbx_供应商编码2";
            this.tbx_供应商编码2.Size = new System.Drawing.Size(159, 22);
            this.tbx_供应商编码2.TabIndex = 13;
            // 
            // tbx_供应商编码1
            // 
            this.tbx_供应商编码1.Location = new System.Drawing.Point(118, 97);
            this.tbx_供应商编码1.Name = "tbx_供应商编码1";
            this.tbx_供应商编码1.Size = new System.Drawing.Size(141, 22);
            this.tbx_供应商编码1.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(78, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "从";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "供应商编码";
            // 
            // tbx_物料编码2
            // 
            this.tbx_物料编码2.Location = new System.Drawing.Point(332, 63);
            this.tbx_物料编码2.Name = "tbx_物料编码2";
            this.tbx_物料编码2.Size = new System.Drawing.Size(159, 22);
            this.tbx_物料编码2.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "到";
            // 
            // tbx_物料编码1
            // 
            this.tbx_物料编码1.Location = new System.Drawing.Point(118, 66);
            this.tbx_物料编码1.Name = "tbx_物料编码1";
            this.tbx_物料编码1.Size = new System.Drawing.Size(141, 22);
            this.tbx_物料编码1.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "从";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "物料编码 ";
            // 
            // tbx_订单编码2
            // 
            this.tbx_订单编码2.Location = new System.Drawing.Point(332, 28);
            this.tbx_订单编码2.Name = "tbx_订单编码2";
            this.tbx_订单编码2.Size = new System.Drawing.Size(159, 22);
            this.tbx_订单编码2.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(277, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "到";
            // 
            // tbx_订单编码1
            // 
            this.tbx_订单编码1.Location = new System.Drawing.Point(118, 28);
            this.tbx_订单编码1.Name = "tbx_订单编码1";
            this.tbx_订单编码1.Size = new System.Drawing.Size(141, 22);
            this.tbx_订单编码1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "从";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "订单编码";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1048, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 45);
            this.button1.TabIndex = 15;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_精确订单编码);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1249, 67);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "精确查询";
            // 
            // cbb_精确订单编码
            // 
            this.cbb_精确订单编码.FormattingEnabled = true;
            this.cbb_精确订单编码.Location = new System.Drawing.Point(93, 30);
            this.cbb_精确订单编码.Name = "cbb_精确订单编码";
            this.cbb_精确订单编码.Size = new System.Drawing.Size(141, 24);
            this.cbb_精确订单编码.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "订单编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // QuryOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 637);
            this.Controls.Add(this.groupBox1);
            this.Name = "QuryOrder";
            this.Text = "订单-凭证信息查询";
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_查询结果)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbb_精确订单编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_订单编码2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_订单编码1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbx_供应商编码2;
        private System.Windows.Forms.TextBox tbx_供应商编码1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbx_物料编码2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbx_物料编码1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgv_查询结果;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbx_总金额2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbx_总金额1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox 订单数量2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbx_订单数量1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btn_查看凭证信息;
        private System.Windows.Forms.Button btn_选择订单;
    }
}