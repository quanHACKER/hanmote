﻿namespace MMClient.MD.FI
{
    partial class actsummy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_科目 = new System.Windows.Forms.ComboBox();
            this.dgv_项目 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择要统计的科目：";
            // 
            // cbb_科目
            // 
            this.cbb_科目.FormattingEnabled = true;
            this.cbb_科目.Location = new System.Drawing.Point(165, 36);
            this.cbb_科目.Name = "cbb_科目";
            this.cbb_科目.Size = new System.Drawing.Size(158, 24);
            this.cbb_科目.TabIndex = 1;
            this.cbb_科目.SelectedIndexChanged += new System.EventHandler(this.cbb_科目_SelectedIndexChanged);
            // 
            // dgv_项目
            // 
            this.dgv_项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_项目.Location = new System.Drawing.Point(7, 88);
            this.dgv_项目.Name = "dgv_项目";
            this.dgv_项目.RowTemplate.Height = 24;
            this.dgv_项目.Size = new System.Drawing.Size(1073, 322);
            this.dgv_项目.TabIndex = 2;
            // 
            // actsummy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 438);
            this.Controls.Add(this.dgv_项目);
            this.Controls.Add(this.cbb_科目);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "actsummy";
            this.Text = "查询科目信息";
            this.Load += new System.EventHandler(this.actsummy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_科目;
        private System.Windows.Forms.DataGridView dgv_项目;
    }
}