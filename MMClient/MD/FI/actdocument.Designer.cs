﻿namespace MMClient.MD.FI
{
    partial class actdocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_凭证类型 = new System.Windows.Forms.ComboBox();
            this.btn_load = new System.Windows.Forms.Button();
            this.tbx_凭证编码 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbx_移动类型 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_过账 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgv_物料凭证 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgv_项目 = new System.Windows.Forms.DataGridView();
            this.cln_评估类 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cln_供应商 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cln_供应商描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_会计科目 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cln_会计科目描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_借方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_贷方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_税码 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cln_税码描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_工厂 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cln_库存地 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tbx_货币描述 = new System.Windows.Forms.TextBox();
            this.cbb_货币编码 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_抬头文本 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_公司代码 = new System.Windows.Forms.ComboBox();
            this.tbx_公司描述 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtp_过账日期 = new System.Windows.Forms.DateTimePicker();
            this.dtb_凭证日期 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料凭证)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.cbb_凭证类型);
            this.groupBox1.Controls.Add(this.btn_load);
            this.groupBox1.Controls.Add(this.tbx_凭证编码);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbx_移动类型);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btn_过账);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tbx_货币描述);
            this.groupBox1.Controls.Add(this.cbb_货币编码);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbx_抬头文本);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbb_公司代码);
            this.groupBox1.Controls.Add(this.tbx_公司描述);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtp_过账日期);
            this.groupBox1.Controls.Add(this.dtb_凭证日期);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1242, 1668);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本头数据";
            // 
            // cbb_凭证类型
            // 
            this.cbb_凭证类型.FormattingEnabled = true;
            this.cbb_凭证类型.Location = new System.Drawing.Point(110, 110);
            this.cbb_凭证类型.Name = "cbb_凭证类型";
            this.cbb_凭证类型.Size = new System.Drawing.Size(165, 24);
            this.cbb_凭证类型.TabIndex = 24;
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(748, 67);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(75, 23);
            this.btn_load.TabIndex = 23;
            this.btn_load.Text = "button1";
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_Click);
            // 
            // tbx_凭证编码
            // 
            this.tbx_凭证编码.Location = new System.Drawing.Point(1042, 119);
            this.tbx_凭证编码.Name = "tbx_凭证编码";
            this.tbx_凭证编码.Size = new System.Drawing.Size(158, 22);
            this.tbx_凭证编码.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(960, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "凭证编码";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(911, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 17);
            this.label10.TabIndex = 20;
            // 
            // tbx_移动类型
            // 
            this.tbx_移动类型.Location = new System.Drawing.Point(811, 120);
            this.tbx_移动类型.Name = "tbx_移动类型";
            this.tbx_移动类型.Size = new System.Drawing.Size(100, 22);
            this.tbx_移动类型.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(727, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "移动类型";
            // 
            // btn_过账
            // 
            this.btn_过账.Location = new System.Drawing.Point(952, 31);
            this.btn_过账.Name = "btn_过账";
            this.btn_过账.Size = new System.Drawing.Size(115, 38);
            this.btn_过账.TabIndex = 17;
            this.btn_过账.Text = "过账";
            this.btn_过账.UseVisualStyleBackColor = true;
            this.btn_过账.Click += new System.EventHandler(this.btn_过账_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(12, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1224, 1502);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "项目";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgv_物料凭证);
            this.groupBox4.Location = new System.Drawing.Point(6, 300);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1163, 159);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "对应的物料凭证信息";
            // 
            // dgv_物料凭证
            // 
            this.dgv_物料凭证.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_物料凭证.Location = new System.Drawing.Point(21, 18);
            this.dgv_物料凭证.Name = "dgv_物料凭证";
            this.dgv_物料凭证.RowTemplate.Height = 24;
            this.dgv_物料凭证.Size = new System.Drawing.Size(1052, 127);
            this.dgv_物料凭证.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgv_项目);
            this.groupBox3.Location = new System.Drawing.Point(3, 61);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1167, 233);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // dgv_项目
            // 
            this.dgv_项目.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_评估类,
            this.cln_供应商,
            this.cln_供应商描述,
            this.cln_会计科目,
            this.cln_会计科目描述,
            this.cln_借方,
            this.cln_贷方,
            this.cln_税码,
            this.cln_税码描述,
            this.cln_工厂,
            this.cln_库存地});
            this.dgv_项目.Location = new System.Drawing.Point(24, 9);
            this.dgv_项目.Name = "dgv_项目";
            this.dgv_项目.RowTemplate.Height = 24;
            this.dgv_项目.Size = new System.Drawing.Size(1053, 284);
            this.dgv_项目.TabIndex = 0;
            this.dgv_项目.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_项目_CellValueChanged);
            // 
            // cln_评估类
            // 
            this.cln_评估类.HeaderText = "评估类";
            this.cln_评估类.Name = "cln_评估类";
            this.cln_评估类.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_评估类.Sorted = true;
            this.cln_评估类.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cln_供应商
            // 
            this.cln_供应商.HeaderText = "供应商";
            this.cln_供应商.Name = "cln_供应商";
            // 
            // cln_供应商描述
            // 
            this.cln_供应商描述.HeaderText = "供应商描述";
            this.cln_供应商描述.Name = "cln_供应商描述";
            this.cln_供应商描述.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_供应商描述.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_会计科目
            // 
            this.cln_会计科目.HeaderText = "会计科目";
            this.cln_会计科目.Name = "cln_会计科目";
            this.cln_会计科目.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_会计科目.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cln_会计科目描述
            // 
            this.cln_会计科目描述.HeaderText = "会计科目描述";
            this.cln_会计科目描述.Name = "cln_会计科目描述";
            // 
            // cln_借方
            // 
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = "0";
            this.cln_借方.DefaultCellStyle = dataGridViewCellStyle3;
            this.cln_借方.HeaderText = "借方";
            this.cln_借方.Name = "cln_借方";
            // 
            // cln_贷方
            // 
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = "0";
            this.cln_贷方.DefaultCellStyle = dataGridViewCellStyle4;
            this.cln_贷方.HeaderText = "贷方";
            this.cln_贷方.Name = "cln_贷方";
            // 
            // cln_税码
            // 
            this.cln_税码.HeaderText = "税码";
            this.cln_税码.Name = "cln_税码";
            this.cln_税码.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_税码.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cln_税码描述
            // 
            this.cln_税码描述.HeaderText = "税码描述";
            this.cln_税码描述.Name = "cln_税码描述";
            // 
            // cln_工厂
            // 
            this.cln_工厂.HeaderText = "工厂";
            this.cln_工厂.Name = "cln_工厂";
            // 
            // cln_库存地
            // 
            this.cln_库存地.HeaderText = "库存地";
            this.cln_库存地.Name = "cln_库存地";
            // 
            // tbx_货币描述
            // 
            this.tbx_货币描述.Location = new System.Drawing.Point(505, 117);
            this.tbx_货币描述.Name = "tbx_货币描述";
            this.tbx_货币描述.Size = new System.Drawing.Size(80, 22);
            this.tbx_货币描述.TabIndex = 16;
            // 
            // cbb_货币编码
            // 
            this.cbb_货币编码.FormattingEnabled = true;
            this.cbb_货币编码.Location = new System.Drawing.Point(441, 117);
            this.cbb_货币编码.Name = "cbb_货币编码";
            this.cbb_货币编码.Size = new System.Drawing.Size(58, 24);
            this.cbb_货币编码.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(371, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "货币";
            // 
            // tbx_抬头文本
            // 
            this.tbx_抬头文本.Location = new System.Drawing.Point(441, 70);
            this.tbx_抬头文本.Name = "tbx_抬头文本";
            this.tbx_抬头文本.Size = new System.Drawing.Size(256, 22);
            this.tbx_抬头文本.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(334, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "凭证抬头文本";
            // 
            // cbb_公司代码
            // 
            this.cbb_公司代码.FormattingEnabled = true;
            this.cbb_公司代码.Location = new System.Drawing.Point(441, 28);
            this.cbb_公司代码.Name = "cbb_公司代码";
            this.cbb_公司代码.Size = new System.Drawing.Size(58, 24);
            this.cbb_公司代码.TabIndex = 11;
            // 
            // tbx_公司描述
            // 
            this.tbx_公司描述.Location = new System.Drawing.Point(505, 28);
            this.tbx_公司描述.Name = "tbx_公司描述";
            this.tbx_公司描述.Size = new System.Drawing.Size(192, 22);
            this.tbx_公司描述.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(371, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "公司代码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "凭证类型";
            // 
            // dtp_过账日期
            // 
            this.dtp_过账日期.Location = new System.Drawing.Point(110, 73);
            this.dtp_过账日期.Name = "dtp_过账日期";
            this.dtp_过账日期.Size = new System.Drawing.Size(165, 22);
            this.dtp_过账日期.TabIndex = 5;
            // 
            // dtb_凭证日期
            // 
            this.dtb_凭证日期.Location = new System.Drawing.Point(110, 31);
            this.dtb_凭证日期.Name = "dtb_凭证日期";
            this.dtb_凭证日期.Size = new System.Drawing.Size(165, 22);
            this.dtb_凭证日期.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "过账日期";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "凭证日期";
            // 
            // actdocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 703);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "actdocument";
            this.Text = "会计凭证";
            this.Load += new System.EventHandler(this.actdocument_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料凭证)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtp_过账日期;
        private System.Windows.Forms.DateTimePicker dtb_凭证日期;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_公司代码;
        private System.Windows.Forms.TextBox tbx_公司描述;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbx_抬头文本;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbx_货币描述;
        private System.Windows.Forms.ComboBox cbb_货币编码;
        private System.Windows.Forms.DataGridView dgv_项目;
        private System.Windows.Forms.Button btn_过账;
        private System.Windows.Forms.TextBox tbx_移动类型;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbx_凭证编码;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgv_物料凭证;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_评估类;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_供应商描述;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_会计科目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_会计科目描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_借方;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_贷方;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_税码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_税码描述;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_工厂;
        private System.Windows.Forms.DataGridViewComboBoxColumn cln_库存地;
        private System.Windows.Forms.ComboBox cbb_凭证类型;
    }
}