﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Model.MD.Order;
using Lib.Bll.MDBll.Order;
using Lib.Bll.MDBll.MT;
using Lib.SqlServerDAL.MDDAL.Order;


namespace MMClient.MD.FI
{
    public partial class Order : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        int flag=0;
        string orderid;
        public Order()
        {
            InitializeComponent();
            flag = 0;
        }
        public Order(string Order_ID)
        {
            InitializeComponent();
            orderid = Order_ID;
            flag = 1;
        }
       
        ComboBoxItem cbm = new ComboBoxItem();
        GeneralBLL gn = new GeneralBLL();
      
        List<OrderItem> listo = new List<OrderItem>();

        OrderBLL odbl = new OrderBLL();
        OrderDAL odal = new OrderDAL();
        int rowcount = 0;
        string supplierID = "";
        Dictionary<string, string> dic = new Dictionary<string, string>();
      

        
       
        
        
        
       
        
        
        
        private void tpg_组织机构_Click(object sender, EventArgs e)
        {

        }

        private void Order_Load(object sender, EventArgs e)
        {
            List<string> list1 = gn.GetAllBuyerOrganization();
            cbm.FuzzyQury(cbb_采购组织, list1);
            List<string> list2 = gn.GetAllSupplier();
            cbm.FuzzyQury(cbb_供应商, list2);
            List<string> list3 = gn.GetAllCompany();
            cbm.FuzzyQury(cbb_公司代码, list3);
            dgv_采购项目.Rows.Add(100);
            dgv_采购申请.Rows.Add(100);
            dgv_信息记录.Rows.Add(100);
            dgv_发票信息.Rows.Add(100);
            dgv_收货信息.Rows.Add(100);
            if (flag ==0)
            {
                DateTime dt = DateTime.Now;
                string ds = dt.ToString("yyyyMMddHHmmss");
                double dd = Convert.ToDouble(ds);
                tbx_订单编码.Text = Convert.ToString(dd);
            }
            if (flag == 1)
            {
                int summtdocnu = 0;
                float summtdocvl = 0;
                int suminvonu = 0;
                float suminvovl=0;
                tbx_订单编码.Text = orderid;
                List<InvoiceFront> listinvo = odal.GetInvoiceFrontByID(orderid);
                for (int i = 0; i < listinvo.Count; i++)
                {
                    dgv_发票信息.Rows[i].Cells["Invoice_Code"].Value = listinvo[i].Invoice_Code;
                    dgv_发票信息.Rows[i].Cells["Invoice_Number"].Value = listinvo[i].Invoice_Number;
                    dgv_发票信息.Rows[i].Cells["Certificate_Type"].Value = listinvo[i].Certificate_Type;
                    dgv_发票信息.Rows[i].Cells["Certificate_Code"].Value = listinvo[i].Certificate_Code;
                    dgv_发票信息.Rows[i].Cells["Invoice_Makeout_Time"].Value = listinvo[i].Invoice_Makeout_Time;
                    dgv_发票信息.Rows[i].Cells["Create_Time"].Value = listinvo[i].Create_Time;
                    dgv_发票信息.Rows[i].Cells["Payment_Type"].Value = listinvo[i].Payment_Type;
                    dgv_发票信息.Rows[i].Cells["sum"].Value = listinvo[i].sum;
                    dgv_发票信息.Rows[i].Cells["Currency"].Value = listinvo[i].Currency;
                    dgv_发票信息.Rows[i].Cells["Sum_Number"].Value = listinvo[i].Sum_Number;
                    suminvonu += listinvo[i].Sum_Number;
                    suminvovl +=(float)Convert.ToDouble( listinvo[i].sum);
                }
                List<MaterialDocumentMM> listmtdoc = odal.GetMaterialDocumentMMByID(orderid);
                for (int j = 0; j < listmtdoc.Count; j++)
                {
                    dgv_收货信息.Rows[j].Cells["StockDocumentId"].Value = listmtdoc[j].StockDocumentId;
                    dgv_收货信息.Rows[j].Cells["Order_ID"].Value = listmtdoc[j].Order_ID;
                    dgv_收货信息.Rows[j].Cells["Delivery_ID"].Value = listmtdoc[j].Delivery_ID;
                    dgv_收货信息.Rows[j].Cells["ReceiptNote_ID"].Value = listmtdoc[j].ReceiptNote_ID;
                    dgv_收货信息.Rows[j].Cells["Posting_Date"].Value = listmtdoc[j].Posting_Date;
                    dgv_收货信息.Rows[j].Cells["Document_Date"].Value = listmtdoc[j].Document_Date;
                    dgv_收货信息.Rows[j].Cells["StockManager"].Value = listmtdoc[j].StockManager;
                    dgv_收货信息.Rows[j].Cells["Move_Type"].Value = listmtdoc[j].Move_Type;
                    dgv_收货信息.Rows[j].Cells["Reversed"].Value = listmtdoc[j].Reversed;
                    dgv_收货信息.Rows[j].Cells["Total_Number"].Value = listmtdoc[j].Total_Number;
                    dgv_收货信息.Rows[j].Cells["Total_Value"].Value = listmtdoc[j].Total_Value;
                    summtdocnu += listmtdoc[j].Total_Number;
                    summtdocvl += listmtdoc[j].Total_Value;
                }
                List<OrderItem> listodim = odal.GetOrderItemByID(orderid);
                for (int k = 0; k < listodim.Count; k++)
                {
                    dgv_采购项目.Rows[k].Cells["cln_物料编码"].Value = listodim[k].Material_ID;
                    dgv_采购项目.Rows[k].Cells["cln_物料描述"].Value = "";
                    dgv_采购项目.Rows[k].Cells["cln_数量"].Value = listodim[k].Number;
                    dgv_采购项目.Rows[k].Cells["cln_单位"].Value = listodim[k].Unit;
                    dgv_采购项目.Rows[k].Cells["cln_净价"].Value = listodim[k].Net_Price;
                    dgv_采购项目.Rows[k].Cells["cln_货币"].Value = "RMB";
                    dgv_采购项目.Rows[k].Cells["cln_物料组"].Value = listodim[k].Material_Group;
                    dgv_采购项目.Rows[k].Cells["cln_工厂"].Value = listodim[k].Factory_ID;
                    dgv_采购项目.Rows[k].Cells["cln_库存地点"].Value = listodim[k].Stock_ID;
                    dgv_采购项目.Rows[k].Cells["cln_批次"].Value = listodim[k].Batch_ID;
                    dgv_采购项目.Rows[k].Cells["cln_请求号码"].Value = listodim[k].Purchase_ID;
                    dgv_采购项目.Rows[k].Cells["cln_信息记录"].Value = listodim[k].Info_Number;


                }
                OrderInfo odinfo = odal.GetOrderInfoByID(orderid);
                cbb_公司代码.Text = odinfo.Company_Code;
                dtp_创建订单时间.Value = odinfo.Create_Time;
                tbx_交货地点.Text = odinfo.Delivery_Points;
                try
                {
                    dtp_交货时间.Value = odinfo.Delivery_Ttime;
                }
                catch (Exception)
                {
                    dtp_交货时间.Value = DateTime.Now;
                }
                int demandnum = odinfo.Total_Number - summtdocnu;
                float demandvalue = odinfo.Total_Value - summtdocvl;
                tbx_交货方式.Text = odinfo.Delivery_Type;
                cbb_订单状态.Text = odinfo.Order_Status;
                cbb_订单类型.Text = "标准采购订单";
                cbb_采购组织.Text = odinfo.Purchase_Organization;
                cbb_供应商.Text = odinfo.Supplier_ID;
                tbx_总价值.Text =Convert.ToString( odinfo.Total_Value);
                tbx_已订购数量.Text = odinfo.Total_Number.ToString();
                tbx_已订购金额.Text = Convert.ToString(odinfo.Total_Value);
                tbx_已交货数量.Text = summtdocnu.ToString();
                tbx_已交货金额.Text = summtdocvl.ToString();
                tbx_需交货数量.Text = demandnum.ToString();
                tbx_需交货金额.Text = demandvalue.ToString();
                tbx_开发票数量.Text = suminvonu.ToString();
                tbx_开发票金额.Text = suminvovl.ToString();
                ///控件不可修改
                dgv_采购项目.ReadOnly = true;
                dgv_发票信息.ReadOnly = true;
                dgv_收货信息.ReadOnly = true;
                btn_参照选定的信息记录.Visible = false;
                btn_导出.Visible = false;
                btn_读取信息记录.Visible = false;
                btn_确定.Visible = false;
                btn_提交订单.Visible = false;
                


            }
            tbx_订单编码.Enabled = false;
        }

        private void btn_提交订单_Click(object sender, EventArgs e)
        {
            bool errorflag = true;
            OrderInfo odif = new OrderInfo();

            float sum = 0;
            long sumpc = 0;
            odif.Company_Code = cbb_公司代码.Text;
            odif.Create_Time =Convert.ToDateTime(dtp_创建订单时间.Value);
            odif.Delivery_Points = tbx_交货地点.Text;
            odif.Delivery_Ttime = Convert.ToDateTime(dtp_交货时间.Value);

            odif.Delivery_Type = tbx_交货方式.Text;
            if (tbx_订单编码.Text == "")
            {
                MessageUtil.ShowError("订单编码不能为空");
                errorflag = false;
            }
            else
                odif.Order_ID = tbx_订单编码.Text;
            odif.Order_Status = cbb_订单状态.Text;
            odif.Order_Type = cbb_订单类型.Text;
            odif.Purchase_Organization = cbb_采购组织.Text;
            odif.Supplier_ID = cbb_供应商.Text;


            //以下设计待定
            odif.Total_Value = 0;
            odif.Value_Paid = 0;
            odif.Value_Received = 0;
            int count = 0;
            for(int i=0;i<dgv_采购项目.Rows.Count;i++)
            {
                OrderItem odim = new OrderItem();
                odim.Delivery_Time = Convert.ToDateTime(dtp_交货时间.Value);
                string Select_Value = dgv_采购项目.Rows[i].Cells["cln_状态"].EditedFormattedValue.ToString();
               if (Select_Value == "True")
               {
                   count++;
                
                    try
                    {
                        odim.Batch_ID = dgv_采购项目.Rows[i].Cells["cln_批次"].Value.ToString();
                    }
                    catch (Exception )
                    {
                        odim.Batch_ID = "";
                    }

                    //odim.Delivery_Time = dgv_采购项目.Rows[i].Cells[cln
                    try
                    {
                        odim.Factory_ID = dgv_采购项目.Rows[i].Cells["cln_工厂"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Factory_ID = "";
                    }
                    try
                    {
                        odim.Info_Number = dgv_采购项目.Rows[i].Cells["cln_信息记录"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Info_Number = "";
                    }
                    try
                    {
                        odim.Material_Group = dgv_采购项目.Rows[i].Cells["cln_物料组"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Material_Group = "";
                    }
                    try
                    {
                        odim.Material_ID = dgv_采购项目.Rows[i].Cells["cln_物料编码"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("物料编码录入错误");
                        errorflag = false;
                        break;
                    }
                    try
                    {
                        odim.Net_Price = (float)Convert.ToDouble(dgv_采购项目.Rows[i].Cells["cln_净价"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("净价录入错误");
                        errorflag = false;
                        break;
                    }
                    try
                    {
                        odim.Number = Convert.ToInt16(dgv_采购项目.Rows[i].Cells["cln_数量"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("数量录入错误");
                        errorflag = false;
                        break;
                    }
                    odim.Order_ID = tbx_订单编码.Text;
                    //货源确定号从信息记录读取
                    odim.PR_ID = "";
                    //
                    try
                    {
                        odim.Stock_ID = dgv_采购项目.Rows[i].Cells["cln_库存地点"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Stock_ID = "";
                    }
                    //odim.Total_Price = dgv_采购项目.Rows[i].Cells["cln_净价"]
                    try
                    {
                        odim.Unit = dgv_采购项目.Rows[i].Cells["cln_单位"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Unit = "EA";
                    }
                    odim.Purchase_ID = dgv_采购项目.Rows[i].Cells["cln_请求号码"].Value.ToString();

                    sum += odim.Net_Price * odim.Number;
                    sumpc += odim.Number;

                    //将订单项目加入列表
                    if (errorflag == true)
                    {
                        listo.Add(odim);

                    }
                }
                
            }
            // 
            if (count == 0)
                MessageUtil.ShowError("没有选中任何项目");
            else
            {
                if (errorflag == true)
                {
                    //odbl.InsertOrderInfo(odif, listo);
                    odif.Total_Value = sum;
                    tbx_总价值.Text = Convert.ToString(sum);
                    tbx_已订购数量.Text = Convert.ToString(sumpc);
                    
                    tbx_需交货数量.Text = Convert.ToString(sumpc);
                    tbx_已订购金额.Text = Convert.ToString(sum);
                    tbx_需交货金额.Text = Convert.ToString(sum); 
                    odal.InsertOrderInfo(odif, listo);
                    MessageUtil.ShowTips("订单录入成功");
                }
            }
            

        }

        private void tpg_定价信息_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgv_定价项目.Refresh();
            dgv_定价项目.Rows.Add(10);
            string mtid = cbb_项目.Text;
            string determineid = "";
            var v1 = dic.TryGetValue(mtid, out determineid);
            string sql1 = "SELECT * FROM [PriceDetermine] WHERE priceDeterminedId='" + determineid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql1);
            int count = dt.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                dgv_定价项目.Rows[i].Cells["cln_类型名称"].Value = dt.Rows[i]["itemName"].ToString();
                dgv_定价项目.Rows[i].Cells["cln_金额"].Value = dt.Rows[i]["value"].ToString();
                dgv_定价项目.Rows[i].Cells["cln_单位1"].Value = "PC";
                dgv_定价项目.Rows[i].Cells["cln_定价值"].Value = dt.Rows[i]["value"].ToString();
                dgv_定价项目.Rows[i].Cells["cln_货币1"].Value = "CNY";
            }

        }

        private void dgv_采购项目_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            List<PR_Supplier> list = new List<PR_Supplier>();
            list = odal.GetAllPrSupplierRelation(false);
            rowcount = list.Count;
          for (int i = 0; i < list.Count; i++)
          {

              dgv_采购申请.Rows[i].Cells["PR_ID"].Value = list[i].PR_ID;
              dgv_采购申请.Rows[i].Cells["Material_ID"].Value = list[i].Material_ID;
              dgv_采购申请.Rows[i].Cells["Supplier_ID"].Value = list[i].Supplier_ID;
              dgv_采购申请.Rows[i].Cells["Number"].Value = list[i].Number;
              dgv_采购申请.Rows[i].Cells["Finished_Mark"].Value = list[i].Finished_Mark;
          }
        }

        private void btn_导出_Click(object sender, EventArgs e)
        {
            //List<PR_Supplier> list = new List<PR_Supplier>();
            //PR_Supplier prs = new PR_Supplier();
            bool samesupplier = true;
             //正确标记
            bool flag = true;
            ///是否加入list的标记
            bool add = false;
            List<MaterialSupplier> listmtsp = new List<MaterialSupplier>();
           
            int j = 0;
            for (int i = 0; i < rowcount; i++)
            {
                MaterialSupplier mtsp = new MaterialSupplier();
                string Select_Value = dgv_采购申请.Rows[i].Cells["Check_Mark"].EditedFormattedValue.ToString();
                if (Select_Value == "True")
                {
                   add = true;
                    mtsp.Material_ID = dgv_采购申请.Rows[i].Cells["Material_ID"].Value.ToString();
                    if (samesupplier == true)
                    {
                        supplierID = dgv_采购申请.Rows[i].Cells["Supplier_ID"].Value.ToString();
                        mtsp.Supplier_ID = supplierID;
                        samesupplier = false;
                        dgv_采购项目.Rows[j].Cells["cln_物料编码"].Value = dgv_采购申请.Rows[i].Cells["Material_ID"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_数量"].Value = dgv_采购申请.Rows[i].Cells["Number"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_请求号码"].Value = dgv_采购申请.Rows[i].Cells["PR_ID"].Value;
                        dgv_采购项目.Rows[j].Cells["CLN_单位"].Value = "PC";
                        j++;
                    }
                    else
                    {
                        if (dgv_采购申请.Rows[i].Cells["Supplier_ID"].Value.ToString() == supplierID)
                        {
                            mtsp.Supplier_ID = supplierID;
                            dgv_采购项目.Rows[j].Cells["cln_物料编码"].Value = dgv_采购申请.Rows[i].Cells["Material_ID"].Value;
                            dgv_采购项目.Rows[j].Cells["cln_数量"].Value = dgv_采购申请.Rows[i].Cells["Number"].Value;
                            dgv_采购项目.Rows[j].Cells["cln_请求号码"].Value = dgv_采购申请.Rows[i].Cells["PR_ID"].Value;
                            dgv_采购项目.Rows[j].Cells["CLN_单位"].Value = "PC";
                            j++;
                        }
                        else
                        {
                            MessageUtil.ShowError("错误供应商");
                            flag = false;
                        }
                    }
                 
                    
                }
                if (flag == true)
                {
                    if (add == true)
                    {
                       
                        listmtsp.Add(mtsp);
                        add = false;
                    }
                }
                //重置标记
                flag = true;
            }
               //标记重置
                    samesupplier = true;
                   
             cbb_供应商.Text = supplierID;

            //筛选所选项的信息记录
             List<InfoRecord> listinfo = odal.GetInforecordByMaterilSuplier(listmtsp);
            //信息记录写入datagridview
             for (int k = 0; k < listinfo.Count; k++)
             {
                 dgv_信息记录.Rows[k].Cells["Record_ID"].Value = listinfo[k].Record_ID;
                 dgv_信息记录.Rows[k].Cells["Supplier_ID1"].Value = listinfo[k].Supplier_ID;
                 dgv_信息记录.Rows[k].Cells["Supplier_Name"].Value = listinfo[k].Supplier_Name;
                 dgv_信息记录.Rows[k].Cells["Material_ID1"].Value = listinfo[k].Material_ID;
                 dgv_信息记录.Rows[k].Cells["Material_Name"].Value = listinfo[k].Material_Name;
                 dgv_信息记录.Rows[k].Cells["Net_Price"].Value = listinfo[k].NetPrice;
                 dgv_信息记录.Rows[k].Cells["Price_Determine"].Value = listinfo[k].Price_Determine;
                 dgv_信息记录.Rows[k].Cells["Factory_ID"].Value = listinfo[k].Factory_ID;
                 dgv_信息记录.Rows[k].Cells["Stock_ID"].Value = listinfo[k].Stock_ID;
             }
        }

        private void btn_参照选定的信息记录_Click(object sender, EventArgs e)
        {
           
            try
            {
                int i = dgv_信息记录.CurrentCell.RowIndex;
                string mtid = dgv_信息记录.Rows[i].Cells["Material_ID1"].Value.ToString();
                cbb_采购组织.Text = "总部采购组织";
                for (int j = 0; j < rowcount; j++)
                {
                    if (dgv_采购项目.Rows[j].Cells["cln_物料编码"].Value.ToString() == mtid)
                    {
                        string determineid = dgv_信息记录.Rows[i].Cells["Price_Determine"].Value.ToString();
                        dgv_采购项目.Rows[j].Cells["cln_物料描述"].Value = dgv_信息记录.Rows[i].Cells["Material_Name"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_净价"].Value = dgv_信息记录.Rows[i].Cells["Net_Price"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_价格确定编号"].Value = dgv_信息记录.Rows[i].Cells["Price_Determine"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_工厂"].Value = dgv_信息记录.Rows[i].Cells["Factory_ID"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_库存地点"].Value = dgv_信息记录.Rows[i].Cells["Stock_ID"].Value;
                        dgv_采购项目.Rows[j].Cells["cln_货币"].Value = "RMB";
                        dgv_采购项目.Rows[j].Cells["cln_信息记录"].Value = dgv_信息记录.Rows[i].Cells["Record_ID"].Value;
                        dic.Add(mtid, determineid);
                        cbb_项目.Items.Add(mtid);


                    }

                }
            }
            catch (Exception ex)
            {


            }
        }

        private void dgv_定价项目_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            QuryOrder qo = new QuryOrder();
            qo.Show();
        }
    }
}
