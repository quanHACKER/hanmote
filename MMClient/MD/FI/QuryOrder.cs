﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.MD.FI
{
    public partial class QuryOrder : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public QuryOrder()
        {
            InitializeComponent();
        }
        public QuryOrder(int identify)
        {
            InitializeComponent();
            if (identify == 0)
            {

            }
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbb_精确订单编码.Text != null)
            {
                string sql = "SELECT * FROM [Order_Info] WHERE Order_ID= '" + cbb_精确订单编码.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt.Rows.Count == 0)
                {
                    MessageUtil.ShowError("没有查询的订单编码");
                }
                else
                    dgv_查询结果.DataSource = dt;

            }
            else
            {
                string sql = "SELECT * FROM [Order_Info] WHERE ";
                if (tbx_订单编码1.Text != null && tbx_订单编码2.Text != null)
                {
                    sql += "Order_ID BETWEEN '" + tbx_订单编码1.Text + "' AND '" + tbx_订单编码2.Text + "'";
                }
                if (tbx_物料编码1.Text != null && tbx_物料编码2.Text != null)
                {
                    sql += "Material_ID BETWEEN '" + tbx_物料编码1.Text + "' AND '" + tbx_物料编码2.Text + "'";
                }
                if (tbx_供应商编码1.Text != null && tbx_供应商编码2.Text != null)
                {
                    sql += "Supplier_ID Between '" + tbx_供应商编码1.Text + "'AND '" + tbx_供应商编码2.Text + "'";
                }
              DataTable dt =  DBHelper.ExecuteQueryDT(sql);
              if (dt.Rows.Count == 0)
              {
                  MessageUtil.ShowError("没有查询的订单编码");
              }
              else
                  dgv_查询结果.DataSource = dt;
             
                
            }

        }

        private void dgv_查询结果_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void btn_查看凭证信息_Click(object sender, EventArgs e)
        {
            ProofRelationship prt = new ProofRelationship();
            prt.Show();
        }

        private void btn_选择订单_Click(object sender, EventArgs e)
        {
            if (dgv_查询结果.CurrentRow == null)
            {
                MessageUtil.ShowError("没有选中任何项目");
            }
            else
            {
                int rowindex = dgv_查询结果.CurrentRow.Index;
                string odid = dgv_查询结果.Rows[rowindex].Cells["Order_ID"].Value.ToString();
                Order oder = new Order(odid);
                oder.Show();
            }
        }

    }
}
