﻿namespace MMClient.MD.FI
{
    partial class actquery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_项目 = new System.Windows.Forms.DataGridView();
            this.cln_评估类 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_供应商 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_供应商描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_会计科目 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_会计科目描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_借方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_贷方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_税码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_税码描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_工厂 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_库存地 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_凭证类型 = new System.Windows.Forms.TextBox();
            this.tbx_凭证编码 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_公司代码 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_凭证日期 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_财务凭证编码 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_财务凭证编码);
            this.groupBox1.Controls.Add(this.btn_查询);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1140, 86);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询框";
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(495, 29);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(75, 32);
            this.btn_查询.TabIndex = 2;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            this.btn_查询.Click += new System.EventHandler(this.btn_查询_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 29);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(148, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "请输入财务凭证编码：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_项目);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbx_凭证类型);
            this.groupBox2.Controls.Add(this.tbx_凭证编码);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tbx_公司代码);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tbx_凭证日期);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1140, 501);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "会计凭证";
            // 
            // dgv_项目
            // 
            this.dgv_项目.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_评估类,
            this.cln_供应商,
            this.cln_供应商描述,
            this.cln_会计科目,
            this.cln_会计科目描述,
            this.cln_借方,
            this.cln_贷方,
            this.cln_税码,
            this.cln_税码描述,
            this.cln_工厂,
            this.cln_库存地});
            this.dgv_项目.Location = new System.Drawing.Point(9, 89);
            this.dgv_项目.Name = "dgv_项目";
            this.dgv_项目.RowTemplate.Height = 24;
            this.dgv_项目.Size = new System.Drawing.Size(1053, 284);
            this.dgv_项目.TabIndex = 8;
            // 
            // cln_评估类
            // 
            this.cln_评估类.HeaderText = "评估类";
            this.cln_评估类.Name = "cln_评估类";
            this.cln_评估类.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_供应商
            // 
            this.cln_供应商.HeaderText = "供应商";
            this.cln_供应商.Name = "cln_供应商";
            this.cln_供应商.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_供应商.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_供应商描述
            // 
            this.cln_供应商描述.HeaderText = "供应商描述";
            this.cln_供应商描述.Name = "cln_供应商描述";
            this.cln_供应商描述.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_供应商描述.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_会计科目
            // 
            this.cln_会计科目.HeaderText = "会计科目";
            this.cln_会计科目.Name = "cln_会计科目";
            this.cln_会计科目.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_会计科目描述
            // 
            this.cln_会计科目描述.HeaderText = "会计科目描述";
            this.cln_会计科目描述.Name = "cln_会计科目描述";
            // 
            // cln_借方
            // 
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = "0";
            this.cln_借方.DefaultCellStyle = dataGridViewCellStyle1;
            this.cln_借方.HeaderText = "借方";
            this.cln_借方.Name = "cln_借方";
            // 
            // cln_贷方
            // 
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = "0";
            this.cln_贷方.DefaultCellStyle = dataGridViewCellStyle2;
            this.cln_贷方.HeaderText = "贷方";
            this.cln_贷方.Name = "cln_贷方";
            // 
            // cln_税码
            // 
            this.cln_税码.HeaderText = "税码";
            this.cln_税码.Name = "cln_税码";
            this.cln_税码.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_税码描述
            // 
            this.cln_税码描述.HeaderText = "税码描述";
            this.cln_税码描述.Name = "cln_税码描述";
            // 
            // cln_工厂
            // 
            this.cln_工厂.HeaderText = "工厂";
            this.cln_工厂.Name = "cln_工厂";
            this.cln_工厂.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_工厂.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_库存地
            // 
            this.cln_库存地.HeaderText = "库存地";
            this.cln_库存地.Name = "cln_库存地";
            this.cln_库存地.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_库存地.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(785, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "凭证类型";
            // 
            // tbx_凭证类型
            // 
            this.tbx_凭证类型.Location = new System.Drawing.Point(893, 52);
            this.tbx_凭证类型.Name = "tbx_凭证类型";
            this.tbx_凭证类型.Size = new System.Drawing.Size(100, 22);
            this.tbx_凭证类型.TabIndex = 6;
            // 
            // tbx_凭证编码
            // 
            this.tbx_凭证编码.Location = new System.Drawing.Point(625, 49);
            this.tbx_凭证编码.Name = "tbx_凭证编码";
            this.tbx_凭证编码.Size = new System.Drawing.Size(100, 22);
            this.tbx_凭证编码.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(506, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "凭证编码";
            // 
            // tbx_公司代码
            // 
            this.tbx_公司代码.Location = new System.Drawing.Point(355, 46);
            this.tbx_公司代码.Name = "tbx_公司代码";
            this.tbx_公司代码.Size = new System.Drawing.Size(100, 22);
            this.tbx_公司代码.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(255, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "公司代码";
            // 
            // tbx_凭证日期
            // 
            this.tbx_凭证日期.Location = new System.Drawing.Point(95, 46);
            this.tbx_凭证日期.Name = "tbx_凭证日期";
            this.tbx_凭证日期.Size = new System.Drawing.Size(100, 22);
            this.tbx_凭证日期.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "凭证日期";
            // 
            // cbb_财务凭证编码
            // 
            this.cbb_财务凭证编码.FormattingEnabled = true;
            this.cbb_财务凭证编码.Location = new System.Drawing.Point(192, 26);
            this.cbb_财务凭证编码.Name = "cbb_财务凭证编码";
            this.cbb_财务凭证编码.Size = new System.Drawing.Size(145, 24);
            this.cbb_财务凭证编码.TabIndex = 3;
            // 
            // actquery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 700);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "actquery";
            this.Text = "查询会计凭证";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbx_凭证日期;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_公司代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_凭证编码;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_凭证类型;
        private System.Windows.Forms.DataGridView dgv_项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_评估类;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_供应商描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_会计科目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_会计科目描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_借方;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_贷方;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_税码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_税码描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_工厂;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_库存地;
        private System.Windows.Forms.ComboBox cbb_财务凭证编码;
    }
}