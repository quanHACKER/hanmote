﻿namespace MMClient.MD.FI
{
    partial class ManualPosting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gpx_头数据 = new System.Windows.Forms.GroupBox();
            this.cbb_所属订单号 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.btn_清除科目 = new System.Windows.Forms.Button();
            this.btn_过账 = new System.Windows.Forms.Button();
            this.tbx_凭证编码 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_模拟过账 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbx_抬头文本 = new System.Windows.Forms.TextBox();
            this.cbb_货币 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_公司代码 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_凭证类型 = new System.Windows.Forms.TextBox();
            this.cbb_凭证类型 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtp_过账日期 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtp_凭证日期 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_temp = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.trv_科目 = new System.Windows.Forms.TreeView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbx_temp = new System.Windows.Forms.TextBox();
            this.dgv_项目 = new System.Windows.Forms.DataGridView();
            this.cln_行项目 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cln_供应商 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_供应商描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_会计科目 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_科目描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_借方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_贷方 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_税码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_税码描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_清除行 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpg_项目 = new System.Windows.Forms.TabPage();
            this.btn_应用2 = new System.Windows.Forms.Button();
            this.cbb_利润中心 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbb_成本中心 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_起息日 = new System.Windows.Forms.DateTimePicker();
            this.tbc_详细信息 = new System.Windows.Forms.TabControl();
            this.tpg_详细信息 = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbx_天数百分比1 = new System.Windows.Forms.TextBox();
            this.cbb_付款条件1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_应用 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtp_基准日期1 = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dtp_基准日期 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.cbb_付款条件 = new System.Windows.Forms.ComboBox();
            this.tbx_天数百分比 = new System.Windows.Forms.TextBox();
            this.tbx_折扣金额 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.ckb_负过账 = new System.Windows.Forms.CheckBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.gpx_头数据.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).BeginInit();
            this.tpg_项目.SuspendLayout();
            this.tbc_详细信息.SuspendLayout();
            this.tpg_详细信息.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpx_头数据
            // 
            this.gpx_头数据.Controls.Add(this.cbb_所属订单号);
            this.gpx_头数据.Controls.Add(this.label21);
            this.gpx_头数据.Controls.Add(this.btn_清除科目);
            this.gpx_头数据.Controls.Add(this.btn_过账);
            this.gpx_头数据.Controls.Add(this.tbx_凭证编码);
            this.gpx_头数据.Controls.Add(this.label7);
            this.gpx_头数据.Controls.Add(this.btn_模拟过账);
            this.gpx_头数据.Controls.Add(this.label6);
            this.gpx_头数据.Controls.Add(this.tbx_抬头文本);
            this.gpx_头数据.Controls.Add(this.cbb_货币);
            this.gpx_头数据.Controls.Add(this.label5);
            this.gpx_头数据.Controls.Add(this.cbb_公司代码);
            this.gpx_头数据.Controls.Add(this.label4);
            this.gpx_头数据.Controls.Add(this.tbx_凭证类型);
            this.gpx_头数据.Controls.Add(this.cbb_凭证类型);
            this.gpx_头数据.Controls.Add(this.label3);
            this.gpx_头数据.Controls.Add(this.dtp_过账日期);
            this.gpx_头数据.Controls.Add(this.label2);
            this.gpx_头数据.Controls.Add(this.dtp_凭证日期);
            this.gpx_头数据.Controls.Add(this.label1);
            this.gpx_头数据.Location = new System.Drawing.Point(10, 6);
            this.gpx_头数据.Name = "gpx_头数据";
            this.gpx_头数据.Size = new System.Drawing.Size(913, 170);
            this.gpx_头数据.TabIndex = 0;
            this.gpx_头数据.TabStop = false;
            this.gpx_头数据.Text = "基本头数据";
            // 
            // cbb_所属订单号
            // 
            this.cbb_所属订单号.FormattingEnabled = true;
            this.cbb_所属订单号.Location = new System.Drawing.Point(91, 146);
            this.cbb_所属订单号.Name = "cbb_所属订单号";
            this.cbb_所属订单号.Size = new System.Drawing.Size(157, 24);
            this.cbb_所属订单号.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 147);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 17);
            this.label21.TabIndex = 21;
            this.label21.Text = "所属订单号";
            // 
            // btn_清除科目
            // 
            this.btn_清除科目.Location = new System.Drawing.Point(545, 129);
            this.btn_清除科目.Name = "btn_清除科目";
            this.btn_清除科目.Size = new System.Drawing.Size(106, 35);
            this.btn_清除科目.TabIndex = 19;
            this.btn_清除科目.Text = "清除科目";
            this.btn_清除科目.UseVisualStyleBackColor = true;
            this.btn_清除科目.Click += new System.EventHandler(this.btn_清除科目_Click);
            // 
            // btn_过账
            // 
            this.btn_过账.Enabled = false;
            this.btn_过账.Location = new System.Drawing.Point(793, 129);
            this.btn_过账.Name = "btn_过账";
            this.btn_过账.Size = new System.Drawing.Size(99, 35);
            this.btn_过账.TabIndex = 18;
            this.btn_过账.Text = "过账";
            this.btn_过账.UseVisualStyleBackColor = true;
            this.btn_过账.Click += new System.EventHandler(this.btn_过账_Click_1);
            // 
            // tbx_凭证编码
            // 
            this.tbx_凭证编码.Enabled = false;
            this.tbx_凭证编码.Location = new System.Drawing.Point(761, 31);
            this.tbx_凭证编码.Name = "tbx_凭证编码";
            this.tbx_凭证编码.Size = new System.Drawing.Size(116, 22);
            this.tbx_凭证编码.TabIndex = 17;
            this.tbx_凭证编码.Text = "2017412423245";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(691, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "凭证编码";
            // 
            // btn_模拟过账
            // 
            this.btn_模拟过账.Location = new System.Drawing.Point(667, 129);
            this.btn_模拟过账.Name = "btn_模拟过账";
            this.btn_模拟过账.Size = new System.Drawing.Size(106, 35);
            this.btn_模拟过账.TabIndex = 15;
            this.btn_模拟过账.Text = "模拟过账";
            this.btn_模拟过账.UseVisualStyleBackColor = true;
            this.btn_模拟过账.Click += new System.EventHandler(this.btn_过账_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "抬头文本";
            // 
            // tbx_抬头文本
            // 
            this.tbx_抬头文本.Location = new System.Drawing.Point(384, 109);
            this.tbx_抬头文本.Name = "tbx_抬头文本";
            this.tbx_抬头文本.Size = new System.Drawing.Size(157, 22);
            this.tbx_抬头文本.TabIndex = 13;
            // 
            // cbb_货币
            // 
            this.cbb_货币.FormattingEnabled = true;
            this.cbb_货币.Location = new System.Drawing.Point(384, 69);
            this.cbb_货币.Name = "cbb_货币";
            this.cbb_货币.Size = new System.Drawing.Size(157, 24);
            this.cbb_货币.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(299, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "货币";
            // 
            // cbb_公司代码
            // 
            this.cbb_公司代码.FormattingEnabled = true;
            this.cbb_公司代码.Location = new System.Drawing.Point(384, 29);
            this.cbb_公司代码.Name = "cbb_公司代码";
            this.cbb_公司代码.Size = new System.Drawing.Size(288, 24);
            this.cbb_公司代码.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(299, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "公司代码";
            // 
            // tbx_凭证类型
            // 
            this.tbx_凭证类型.Location = new System.Drawing.Point(148, 109);
            this.tbx_凭证类型.Name = "tbx_凭证类型";
            this.tbx_凭证类型.Size = new System.Drawing.Size(100, 22);
            this.tbx_凭证类型.TabIndex = 6;
            // 
            // cbb_凭证类型
            // 
            this.cbb_凭证类型.FormattingEnabled = true;
            this.cbb_凭证类型.Location = new System.Drawing.Point(91, 109);
            this.cbb_凭证类型.Name = "cbb_凭证类型";
            this.cbb_凭证类型.Size = new System.Drawing.Size(51, 24);
            this.cbb_凭证类型.TabIndex = 5;
            this.cbb_凭证类型.SelectedIndexChanged += new System.EventHandler(this.cbb_凭证类型_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "凭证类型";
            // 
            // dtp_过账日期
            // 
            this.dtp_过账日期.Location = new System.Drawing.Point(91, 72);
            this.dtp_过账日期.Name = "dtp_过账日期";
            this.dtp_过账日期.Size = new System.Drawing.Size(121, 22);
            this.dtp_过账日期.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "过账日期";
            // 
            // dtp_凭证日期
            // 
            this.dtp_凭证日期.Location = new System.Drawing.Point(91, 27);
            this.dtp_凭证日期.Name = "dtp_凭证日期";
            this.dtp_凭证日期.Size = new System.Drawing.Size(121, 22);
            this.dtp_凭证日期.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "凭证日期";
            // 
            // cbb_temp
            // 
            this.cbb_temp.FormattingEnabled = true;
            this.cbb_temp.Location = new System.Drawing.Point(43, 30);
            this.cbb_temp.Name = "cbb_temp";
            this.cbb_temp.Size = new System.Drawing.Size(121, 24);
            this.cbb_temp.TabIndex = 15;
            this.cbb_temp.Visible = false;
            this.cbb_temp.SelectedIndexChanged += new System.EventHandler(this.cbb_temp_SelectedIndexChanged);
            this.cbb_temp.TextChanged += new System.EventHandler(this.cbb_temp_TextChanged);
            this.cbb_temp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbb_temp_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.trv_科目);
            this.groupBox2.Location = new System.Drawing.Point(923, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(356, 593);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "科目表层次结构";
            // 
            // trv_科目
            // 
            this.trv_科目.Location = new System.Drawing.Point(15, 17);
            this.trv_科目.Name = "trv_科目";
            this.trv_科目.Size = new System.Drawing.Size(342, 566);
            this.trv_科目.TabIndex = 0;
            this.trv_科目.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.trv_科目_NodeMouseDoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbx_temp);
            this.groupBox3.Controls.Add(this.cbb_temp);
            this.groupBox3.Controls.Add(this.dgv_项目);
            this.groupBox3.Location = new System.Drawing.Point(8, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(915, 411);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "项目";
            // 
            // tbx_temp
            // 
            this.tbx_temp.Location = new System.Drawing.Point(528, 30);
            this.tbx_temp.Name = "tbx_temp";
            this.tbx_temp.Size = new System.Drawing.Size(100, 22);
            this.tbx_temp.TabIndex = 18;
            this.tbx_temp.Visible = false;
            this.tbx_temp.TextChanged += new System.EventHandler(this.tbx_temp_TextChanged);
            this.tbx_temp.VisibleChanged += new System.EventHandler(this.tbx_temp_VisibleChanged);
            this.tbx_temp.Leave += new System.EventHandler(this.tbx_temp_Leave);
            // 
            // dgv_项目
            // 
            this.dgv_项目.AllowUserToAddRows = false;
            this.dgv_项目.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_行项目,
            this.cln_供应商,
            this.cln_供应商描述,
            this.cln_会计科目,
            this.cln_科目描述,
            this.cln_借方,
            this.cln_贷方,
            this.cln_税码,
            this.cln_税码描述,
            this.cln_清除行});
            this.dgv_项目.Location = new System.Drawing.Point(2, 0);
            this.dgv_项目.Name = "dgv_项目";
            this.dgv_项目.RowTemplate.Height = 24;
            this.dgv_项目.Size = new System.Drawing.Size(915, 233);
            this.dgv_项目.TabIndex = 1;
            this.dgv_项目.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_项目_CellClick);
            this.dgv_项目.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_项目_CellContentClick);
            this.dgv_项目.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_项目_CellValueChanged);
            this.dgv_项目.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_项目_EditingControlShowing);
            // 
            // cln_行项目
            // 
            this.cln_行项目.HeaderText = "行项目";
            this.cln_行项目.Name = "cln_行项目";
            this.cln_行项目.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_行项目.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cln_行项目.Text = "详细信息";
            this.cln_行项目.UseColumnTextForButtonValue = true;
            // 
            // cln_供应商
            // 
            this.cln_供应商.HeaderText = "供应商";
            this.cln_供应商.Name = "cln_供应商";
            this.cln_供应商.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_供应商.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_供应商描述
            // 
            this.cln_供应商描述.HeaderText = "供应商描述";
            this.cln_供应商描述.Name = "cln_供应商描述";
            this.cln_供应商描述.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_供应商描述.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_会计科目
            // 
            this.cln_会计科目.HeaderText = "会计科目";
            this.cln_会计科目.Name = "cln_会计科目";
            this.cln_会计科目.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_科目描述
            // 
            this.cln_科目描述.HeaderText = "科目描述";
            this.cln_科目描述.Name = "cln_科目描述";
            // 
            // cln_借方
            // 
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = "0";
            this.cln_借方.DefaultCellStyle = dataGridViewCellStyle5;
            this.cln_借方.HeaderText = "借方";
            this.cln_借方.Name = "cln_借方";
            // 
            // cln_贷方
            // 
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.cln_贷方.DefaultCellStyle = dataGridViewCellStyle6;
            this.cln_贷方.HeaderText = "贷方";
            this.cln_贷方.Name = "cln_贷方";
            // 
            // cln_税码
            // 
            this.cln_税码.HeaderText = "税码";
            this.cln_税码.Name = "cln_税码";
            this.cln_税码.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_税码描述
            // 
            this.cln_税码描述.HeaderText = "税码描述";
            this.cln_税码描述.Name = "cln_税码描述";
            // 
            // cln_清除行
            // 
            this.cln_清除行.HeaderText = "清除行";
            this.cln_清除行.Name = "cln_清除行";
            this.cln_清除行.Text = "清除行";
            this.cln_清除行.UseColumnTextForButtonValue = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "行项目";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 97;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "供应商";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 97;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "供应商描述";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 97;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "会计科目";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.Width = 97;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "科目描述";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 96;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle7.Format = "C2";
            dataGridViewCellStyle7.NullValue = "0";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn6.HeaderText = "借方";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 97;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle8.Format = "C2";
            dataGridViewCellStyle8.NullValue = "0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn7.HeaderText = "贷方";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 97;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "税码";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn8.Width = 97;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "税码描述";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 97;
            // 
            // tpg_项目
            // 
            this.tpg_项目.Controls.Add(this.btn_应用2);
            this.tpg_项目.Controls.Add(this.cbb_利润中心);
            this.tpg_项目.Controls.Add(this.label20);
            this.tpg_项目.Controls.Add(this.label19);
            this.tpg_项目.Controls.Add(this.cbb_成本中心);
            this.tpg_项目.Controls.Add(this.label8);
            this.tpg_项目.Controls.Add(this.dtp_起息日);
            this.tpg_项目.Location = new System.Drawing.Point(4, 25);
            this.tpg_项目.Name = "tpg_项目";
            this.tpg_项目.Padding = new System.Windows.Forms.Padding(3);
            this.tpg_项目.Size = new System.Drawing.Size(901, 143);
            this.tpg_项目.TabIndex = 1;
            this.tpg_项目.Text = "项目";
            this.tpg_项目.UseVisualStyleBackColor = true;
            // 
            // btn_应用2
            // 
            this.btn_应用2.Location = new System.Drawing.Point(800, 105);
            this.btn_应用2.Name = "btn_应用2";
            this.btn_应用2.Size = new System.Drawing.Size(99, 35);
            this.btn_应用2.TabIndex = 26;
            this.btn_应用2.Text = "应用";
            this.btn_应用2.UseVisualStyleBackColor = true;
            // 
            // cbb_利润中心
            // 
            this.cbb_利润中心.FormattingEnabled = true;
            this.cbb_利润中心.Location = new System.Drawing.Point(492, 25);
            this.cbb_利润中心.Name = "cbb_利润中心";
            this.cbb_利润中心.Size = new System.Drawing.Size(121, 24);
            this.cbb_利润中心.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(388, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 17);
            this.label20.TabIndex = 24;
            this.label20.Text = "利润中心";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(18, 77);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 17);
            this.label19.TabIndex = 23;
            this.label19.Text = "成本中心";
            // 
            // cbb_成本中心
            // 
            this.cbb_成本中心.FormattingEnabled = true;
            this.cbb_成本中心.Location = new System.Drawing.Point(98, 74);
            this.cbb_成本中心.Name = "cbb_成本中心";
            this.cbb_成本中心.Size = new System.Drawing.Size(121, 24);
            this.cbb_成本中心.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 21;
            this.label8.Text = "起息日";
            // 
            // dtp_起息日
            // 
            this.dtp_起息日.Location = new System.Drawing.Point(98, 23);
            this.dtp_起息日.Name = "dtp_起息日";
            this.dtp_起息日.Size = new System.Drawing.Size(200, 22);
            this.dtp_起息日.TabIndex = 20;
            // 
            // tbc_详细信息
            // 
            this.tbc_详细信息.Controls.Add(this.tpg_详细信息);
            this.tbc_详细信息.Controls.Add(this.tpg_项目);
            this.tbc_详细信息.Location = new System.Drawing.Point(8, 421);
            this.tbc_详细信息.Name = "tbc_详细信息";
            this.tbc_详细信息.SelectedIndex = 0;
            this.tbc_详细信息.Size = new System.Drawing.Size(909, 172);
            this.tbc_详细信息.TabIndex = 19;
            this.tbc_详细信息.Visible = false;
            // 
            // tpg_详细信息
            // 
            this.tpg_详细信息.Controls.Add(this.checkBox1);
            this.tpg_详细信息.Controls.Add(this.comboBox3);
            this.tpg_详细信息.Controls.Add(this.textBox2);
            this.tpg_详细信息.Controls.Add(this.tbx_天数百分比1);
            this.tpg_详细信息.Controls.Add(this.cbb_付款条件1);
            this.tpg_详细信息.Controls.Add(this.label9);
            this.tpg_详细信息.Controls.Add(this.btn_应用);
            this.tpg_详细信息.Controls.Add(this.label15);
            this.tpg_详细信息.Controls.Add(this.label16);
            this.tpg_详细信息.Controls.Add(this.label17);
            this.tpg_详细信息.Controls.Add(this.dtp_基准日期1);
            this.tpg_详细信息.Controls.Add(this.label18);
            this.tpg_详细信息.Location = new System.Drawing.Point(4, 25);
            this.tpg_详细信息.Name = "tpg_详细信息";
            this.tpg_详细信息.Padding = new System.Windows.Forms.Padding(3);
            this.tpg_详细信息.Size = new System.Drawing.Size(901, 143);
            this.tpg_详细信息.TabIndex = 0;
            this.tpg_详细信息.Text = "详细信息";
            this.tpg_详细信息.UseVisualStyleBackColor = true;
            this.tpg_详细信息.Click += new System.EventHandler(this.tpg_详细信息_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(382, 110);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 21);
            this.checkBox1.TabIndex = 36;
            this.checkBox1.Text = "负过账";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(543, 68);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(161, 24);
            this.comboBox3.TabIndex = 35;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(543, 28);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(159, 22);
            this.textBox2.TabIndex = 34;
            // 
            // tbx_天数百分比1
            // 
            this.tbx_天数百分比1.Location = new System.Drawing.Point(89, 111);
            this.tbx_天数百分比1.Name = "tbx_天数百分比1";
            this.tbx_天数百分比1.Size = new System.Drawing.Size(272, 22);
            this.tbx_天数百分比1.TabIndex = 33;
            // 
            // cbb_付款条件1
            // 
            this.cbb_付款条件1.FormattingEnabled = true;
            this.cbb_付款条件1.Location = new System.Drawing.Point(89, 66);
            this.cbb_付款条件1.Name = "cbb_付款条件1";
            this.cbb_付款条件1.Size = new System.Drawing.Size(272, 24);
            this.cbb_付款条件1.TabIndex = 32;
            this.cbb_付款条件1.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 31;
            this.label9.Text = "付款条件";
            // 
            // btn_应用
            // 
            this.btn_应用.Location = new System.Drawing.Point(793, 101);
            this.btn_应用.Name = "btn_应用";
            this.btn_应用.Size = new System.Drawing.Size(99, 35);
            this.btn_应用.TabIndex = 30;
            this.btn_应用.Text = "应用";
            this.btn_应用.UseVisualStyleBackColor = true;
            this.btn_应用.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(379, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 17);
            this.label15.TabIndex = 29;
            this.label15.Text = "付款方式";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(379, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 17);
            this.label16.TabIndex = 28;
            this.label16.Text = "折扣金额";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 111);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 17);
            this.label17.TabIndex = 27;
            this.label17.Text = "天数百分比";
            // 
            // dtp_基准日期1
            // 
            this.dtp_基准日期1.Location = new System.Drawing.Point(91, 26);
            this.dtp_基准日期1.Name = "dtp_基准日期1";
            this.dtp_基准日期1.Size = new System.Drawing.Size(121, 22);
            this.dtp_基准日期1.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 17);
            this.label18.TabIndex = 25;
            this.label18.Text = "基准日期";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 17);
            this.label14.TabIndex = 0;
            // 
            // dtp_基准日期
            // 
            this.dtp_基准日期.Location = new System.Drawing.Point(91, 27);
            this.dtp_基准日期.Name = "dtp_基准日期";
            this.dtp_基准日期.Size = new System.Drawing.Size(121, 22);
            this.dtp_基准日期.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 17);
            this.label12.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(379, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(379, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 17);
            this.label10.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(793, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 35);
            this.button1.TabIndex = 18;
            this.button1.Text = "应用";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 17);
            this.label13.TabIndex = 19;
            // 
            // cbb_付款条件
            // 
            this.cbb_付款条件.FormattingEnabled = true;
            this.cbb_付款条件.Location = new System.Drawing.Point(89, 66);
            this.cbb_付款条件.Name = "cbb_付款条件";
            this.cbb_付款条件.Size = new System.Drawing.Size(272, 24);
            this.cbb_付款条件.TabIndex = 20;
            // 
            // tbx_天数百分比
            // 
            this.tbx_天数百分比.Location = new System.Drawing.Point(89, 112);
            this.tbx_天数百分比.Name = "tbx_天数百分比";
            this.tbx_天数百分比.Size = new System.Drawing.Size(272, 22);
            this.tbx_天数百分比.TabIndex = 21;
            // 
            // tbx_折扣金额
            // 
            this.tbx_折扣金额.Location = new System.Drawing.Point(543, 29);
            this.tbx_折扣金额.Name = "tbx_折扣金额";
            this.tbx_折扣金额.Size = new System.Drawing.Size(159, 22);
            this.tbx_折扣金额.TabIndex = 22;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(543, 69);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(161, 24);
            this.comboBox2.TabIndex = 23;
            // 
            // ckb_负过账
            // 
            this.ckb_负过账.AutoSize = true;
            this.ckb_负过账.Location = new System.Drawing.Point(382, 111);
            this.ckb_负过账.Name = "ckb_负过账";
            this.ckb_负过账.Size = new System.Drawing.Size(72, 21);
            this.ckb_负过账.TabIndex = 24;
            this.ckb_负过账.Text = "负过账";
            this.ckb_负过账.UseVisualStyleBackColor = true;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "行项目";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 97;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Width = 97;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "会计科目";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 97;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "税码";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 97;
            // 
            // ManualPosting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.tbc_详细信息);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gpx_头数据);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ManualPosting";
            this.Text = "生成会计凭证";
            this.Load += new System.EventHandler(this.ManualPosting_Load);
            this.gpx_头数据.ResumeLayout(false);
            this.gpx_头数据.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).EndInit();
            this.tpg_项目.ResumeLayout(false);
            this.tpg_项目.PerformLayout();
            this.tbc_详细信息.ResumeLayout(false);
            this.tpg_详细信息.ResumeLayout(false);
            this.tpg_详细信息.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpx_头数据;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtp_过账日期;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_凭证日期;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbx_抬头文本;
        private System.Windows.Forms.ComboBox cbb_货币;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_公司代码;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbx_凭证类型;
        private System.Windows.Forms.ComboBox cbb_凭证类型;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_项目;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.ComboBox cbb_temp;
        private System.Windows.Forms.TextBox tbx_凭证编码;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_模拟过账;
        private System.Windows.Forms.TextBox tbx_temp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button btn_过账;
        private System.Windows.Forms.TreeView trv_科目;
        private System.Windows.Forms.Button btn_清除科目;
        private System.Windows.Forms.DataGridViewButtonColumn cln_行项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_供应商描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_会计科目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_科目描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_借方;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_贷方;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_税码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_税码描述;
        private System.Windows.Forms.DataGridViewButtonColumn cln_清除行;
        private System.Windows.Forms.TabControl tbc_详细信息;
        private System.Windows.Forms.TabPage tpg_详细信息;
        private System.Windows.Forms.TabPage tpg_项目;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtp_基准日期;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbb_付款条件;
        private System.Windows.Forms.TextBox tbx_天数百分比;
        private System.Windows.Forms.TextBox tbx_折扣金额;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.CheckBox ckb_负过账;
        private System.Windows.Forms.Button btn_应用2;
        private System.Windows.Forms.ComboBox cbb_利润中心;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbb_成本中心;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtp_起息日;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbx_天数百分比1;
        private System.Windows.Forms.ComboBox cbb_付款条件1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_应用;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dtp_基准日期1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbb_所属订单号;
    }
}