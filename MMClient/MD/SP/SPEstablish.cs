﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.MD
{
    public partial class SPEstablish : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SPEstablish()
        {
            InitializeComponent();
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {
            cbb_员工总数量.Text = "";
            cbb_供应商编码.Text = "";
            cbb_供应商名称.Text = "";
            cbb_供应商注册名.Text = "";
            cbb_企业性质.Text = "";
            cbb_所在国家.Text = "";
            cbb_网址.Text = "";
            cbb_管理人员数量.Text = "";
            cbb_详细地址.Text = "";
        }

        private void btn_创建_Click(object sender, EventArgs e)
        {
            string byid = cbb_员工总数量.Text;
            string spid = cbb_供应商编码.Text;
            string spname = cbb_供应商名称.Text;
            string spinstal = cbb_供应商注册名.Text;
            string epnature = cbb_企业性质.Text;
            string country = cbb_所在国家.Text;
            string network = cbb_网址.Text;
            string mtid = cbb_管理人员数量.Text;
            string address = cbb_详细地址.Text;
            string sql = "INSERT INTO [MM].[dbo].[Supplier_Base](Staff_num,Supplier_ID,Supplier_Name,Supplier_loginName,Enterprise,Nation,URL,Staff_manger,Address,Register_time,Representtative,Islistedcompany,Stock_code,Export_right,Supelier_ENName,Zip_Code,Fax,Payer_NO,Bank_Name,Bank_Accout,Bank_currency,Bank_branch,Contact_person,Company_Code,Contact_phone1,Contact_phone2,Contact_Email,Staff_num)VALUES('" + byid + "','" + spid + "','" + spid + "','" + spname + "','" + spinstal + "','" + epnature + "','" + country + "', '" + network + "','" + mtid + "','" + address +"')";
            DBHelper.ExecuteNonQuery(sql);
            MessageBox.Show("创建成功");
        }
    }
}
