﻿namespace MMClient.MD.SP
{
    partial class SupplierClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbx_供应商名称 = new System.Windows.Forms.TextBox();
            this.tbx_供应商编码 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_特性类 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_维护特征 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_类特性 = new System.Windows.Forms.DataGridView();
            this.cln_特征描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_特性编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_维护值 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_类特性)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tbx_供应商名称);
            this.groupBox1.Controls.Add(this.tbx_供应商编码);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1167, 67);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "分类视图";
            // 
            // tbx_供应商名称
            // 
            this.tbx_供应商名称.Location = new System.Drawing.Point(262, 29);
            this.tbx_供应商名称.Name = "tbx_供应商名称";
            this.tbx_供应商名称.Size = new System.Drawing.Size(163, 22);
            this.tbx_供应商名称.TabIndex = 6;
            // 
            // tbx_供应商编码
            // 
            this.tbx_供应商编码.Location = new System.Drawing.Point(93, 29);
            this.tbx_供应商编码.Name = "tbx_供应商编码";
            this.tbx_供应商编码.Size = new System.Drawing.Size(163, 22);
            this.tbx_供应商编码.TabIndex = 5;
            this.tbx_供应商编码.TextChanged += new System.EventHandler(this.tbx_供应商编码_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1056, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 37);
            this.button1.TabIndex = 4;
            this.button1.Text = "选择供应商";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "供应商";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_特性类);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btn_维护特征);
            this.groupBox3.Location = new System.Drawing.Point(2, 84);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1167, 66);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "分配";
            // 
            // cbb_特性类
            // 
            this.cbb_特性类.FormattingEnabled = true;
            this.cbb_特性类.Location = new System.Drawing.Point(93, 32);
            this.cbb_特性类.Name = "cbb_特性类";
            this.cbb_特性类.Size = new System.Drawing.Size(163, 24);
            this.cbb_特性类.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "特性类";
            // 
            // btn_维护特征
            // 
            this.btn_维护特征.Location = new System.Drawing.Point(1068, 28);
            this.btn_维护特征.Name = "btn_维护特征";
            this.btn_维护特征.Size = new System.Drawing.Size(93, 37);
            this.btn_维护特征.TabIndex = 1;
            this.btn_维护特征.Text = "维护特征";
            this.btn_维护特征.UseVisualStyleBackColor = true;
            this.btn_维护特征.Click += new System.EventHandler(this.btn_维护特征_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_类特性);
            this.groupBox2.Location = new System.Drawing.Point(3, 156);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1166, 390);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "常规";
            // 
            // dgv_类特性
            // 
            this.dgv_类特性.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_类特性.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_类特性.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_特征描述,
            this.cln_特性编码,
            this.cln_值});
            this.dgv_类特性.Location = new System.Drawing.Point(5, 21);
            this.dgv_类特性.Name = "dgv_类特性";
            this.dgv_类特性.RowTemplate.Height = 24;
            this.dgv_类特性.Size = new System.Drawing.Size(1155, 498);
            this.dgv_类特性.TabIndex = 0;
            // 
            // cln_特征描述
            // 
            this.cln_特征描述.HeaderText = "特征描述";
            this.cln_特征描述.Name = "cln_特征描述";
            this.cln_特征描述.ReadOnly = true;
            // 
            // cln_特性编码
            // 
            this.cln_特性编码.HeaderText = "特性编码";
            this.cln_特性编码.Name = "cln_特性编码";
            this.cln_特性编码.Visible = false;
            // 
            // cln_值
            // 
            this.cln_值.HeaderText = "值";
            this.cln_值.Name = "cln_值";
            // 
            // btn_维护值
            // 
            this.btn_维护值.Location = new System.Drawing.Point(1070, 564);
            this.btn_维护值.Name = "btn_维护值";
            this.btn_维护值.Size = new System.Drawing.Size(93, 37);
            this.btn_维护值.TabIndex = 5;
            this.btn_维护值.Text = "维护特性值";
            this.btn_维护值.UseVisualStyleBackColor = true;
            this.btn_维护值.Click += new System.EventHandler(this.btn_维护值_Click);
            // 
            // SupplierClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.btn_维护值);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "SupplierClass";
            this.Text = "供应商主数据分类视图";
            this.Load += new System.EventHandler(this.SupplierClass_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_类特性)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbb_特性类;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_维护特征;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_类特性;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特征描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特性编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_值;
        public System.Windows.Forms.TextBox tbx_供应商名称;
        public System.Windows.Forms.TextBox tbx_供应商编码;
        private System.Windows.Forms.Button btn_维护值;
    }
}