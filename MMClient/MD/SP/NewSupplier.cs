﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.Bll;
using MMClient.MD;

namespace MMClient.MD.SP
{
    public partial class NewSupplier : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        SPMaintain spm;
        public NewSupplier()
        {
            InitializeComponent();
        }

        public NewSupplier(SPMaintain spm)
        {
            InitializeComponent();
            this.spm = spm;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if(cbb_供应商编码.Text == "")
            {
                MessageUtil.ShowError("请输入正确的供应商编码");
                cbb_供应商名称.Focus();
            }
            else
            {
                if (cbb_供应商名称.Text == "") 
                {
                    MessageUtil.ShowError("请输入正确的供应商名称");
                    cbb_供应商名称.Focus();
                }
                else
                {
                    try
                    {
                        string sql = " INSERT INTO [Supplier_Base] (Supplier_ID,Supplier_Name,Supplier_AccountGroup) Values ('" + cbb_供应商编码.Text + "','" + cbb_供应商名称.Text + "','" + cbb_供应商账户组.Text + "')";

                        //try
                        //{
                        DBHelper.ExecuteNonQuery(sql);
                        MessageBox.Show("新建成功");
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("供应商编码重复");

                    }
                }
            }
            //this.spm.cbb_供应商编码.Items.Add(cbb_供应商编码.Text);
            //this.spm.cbb_供应商编码.Text = cbb_供应商编码.Text;
            //this.spm.cbb_供应商名称.Text = cbb_供应商名称.Text;
            //this.spm.cbb_账户组.Text = cbb_供应商账户组.Text;
            this.Close();

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("供应商编码重复，请重新输入");
            //}
        }
    }
}
