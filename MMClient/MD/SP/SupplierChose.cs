﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.SP;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.SqlServerDAL;
using MMClient.MD.MT;

namespace MMClient.MD.SP
{
    public partial class SupplierChose : Form
    {
        SupplierClass spc = new SupplierClass();
        SPMaintain spm = new SPMaintain();
        SPCPY1 spy = new SPCPY1();
        SPORG spo = new SPORG();
        SupplierBaseBLL sbb = new SupplierBaseBLL();

        public SupplierChose()
        {
            InitializeComponent();
        }
        public SupplierChose(SupplierClass spc)
        {
            InitializeComponent();
            this.spc = spc;
            
        }
        public SupplierChose(SPMaintain spm)
        {
            InitializeComponent();
            this.spm = spm;
        }
        public SupplierChose(SPORG spo)
        {
            InitializeComponent();
            this.spo = spo;
        }
        public SupplierChose(SPCPY1 spy)
        {
            InitializeComponent();
            this.spy = spy;
        }

        //TODO:此按钮似乎没有什么用处
        private void button1_Click(object sender, EventArgs e)
        {
            int rowindex = -1;
            rowindex = supplierInforDataGridView.CurrentRow.Index;
            if (rowindex == -1)
            {
                MessageUtil.ShowError("没有选中任何行");
            }
            else
            {
                string spid = supplierInforDataGridView.Rows[rowindex].Cells[1].Value.ToString();
                string spname = supplierInforDataGridView.Rows[rowindex].Cells[3].Value.ToString();
                this.spc.tbx_供应商编码.Text = spid;
                this.spc.tbx_供应商名称.Text = spname;
                this.spc.tbx_供应商编码.Enabled = false;
                this.spc.tbx_供应商名称.Enabled = false;
                this.Close();

            }
                
        }

        //窗体打开，调用分页函数加载数据并列表显示
        private void SupplierChose_Load(object sender, EventArgs e)
        {
            supplierInforDataGridView.DataSource = sbb.FindSupplierInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            int rowindex = -1;
            rowindex = supplierInforDataGridView.CurrentRow.Index;
            if (rowindex == -1)
            {
                MessageUtil.ShowError("没有选中任何行");
            }
            else
            {
                string spid = supplierInforDataGridView.Rows[rowindex].Cells[0].Value.ToString();
                string spname = supplierInforDataGridView.Rows[rowindex].Cells[2].Value.ToString();
             if (this.spm != null)
            {
                this.spm.cbb_供应商编码.Text = spid;
                this.spm.cbb_供应商名称.Text = spname;
            }
             if (this.spo != null)
                 {
                     this.spo.cbb_供应商编码.Text = spid;
                     this.spo.cbb_供应商名称.Text = spname;
                 }
             if (this.spy != null)
                 {
                     this.spy.cbb_供应商编码.Text = spid;
                     this.spy.cbb_供应商名称.Text = spname;
                 }
            }
            this.Close();
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            supplierInforDataGridView.DataBindings.Clear();
            string sql = "SELECT Supplier_ID,Supplier_AccountGroup,Supplier_Name,Supplier_Title,Nation,Supplier_OName,Client,Trade_Partner,Group_Code FROM [Supplier_Base] WHERE Supplier_ID Like '%" + supplierTextBox.Text + "%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            supplierInforDataGridView.DataSource = dt;
        }

        private void dgv_供应商信息_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {





        }

        //加载分页控件数据，并设置分页控件事件
        private void PageTool_Load(object sender, EventArgs e)
        {
            int totalNum = sbb.TotalCount();
            pageTool.DrawControl(totalNum);
            //事件改变调用函数
            pageTool.OnPageChanged += PageTool_OnPageChanged;
        }

        private void PageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        public void LoadData()
        {
            try
            {
                supplierInforDataGridView.DataSource = sbb.FindSupplierInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch(BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
            //GeneralBLL gn = new GeneralBLL();
            //dgv_物料组.DataSource = gn.GetAllMaterialGroup(this.pageNext1.PageSize, this.pageNext1.PageIndex);
        }
    }
}
