﻿namespace MMClient.MD
{
    partial class SPEstablish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_注册时间 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_管理人员数量 = new System.Windows.Forms.ComboBox();
            this.cbb_详细地址 = new System.Windows.Forms.ComboBox();
            this.cbb_企业性质 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商名称 = new System.Windows.Forms.ComboBox();
            this.cbb_员工总数量 = new System.Windows.Forms.ComboBox();
            this.cbb_网址 = new System.Windows.Forms.ComboBox();
            this.cbb_所在国家 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商注册名 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商编码 = new System.Windows.Forms.ComboBox();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.btn_创建 = new System.Windows.Forms.Button();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.groupBox16.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label15);
            this.groupBox16.Controls.Add(this.label14);
            this.groupBox16.Controls.Add(this.label13);
            this.groupBox16.Controls.Add(this.label12);
            this.groupBox16.Controls.Add(this.label11);
            this.groupBox16.Controls.Add(this.label10);
            this.groupBox16.Controls.Add(this.label9);
            this.groupBox16.Controls.Add(this.comboBox1);
            this.groupBox16.Controls.Add(this.label8);
            this.groupBox16.Controls.Add(this.label7);
            this.groupBox16.Controls.Add(this.label6);
            this.groupBox16.Controls.Add(this.label5);
            this.groupBox16.Controls.Add(this.label4);
            this.groupBox16.Controls.Add(this.label3);
            this.groupBox16.Controls.Add(this.label2);
            this.groupBox16.Controls.Add(this.cbb_注册时间);
            this.groupBox16.Controls.Add(this.label1);
            this.groupBox16.Controls.Add(this.cbb_管理人员数量);
            this.groupBox16.Controls.Add(this.cbb_详细地址);
            this.groupBox16.Controls.Add(this.cbb_企业性质);
            this.groupBox16.Controls.Add(this.cbb_供应商名称);
            this.groupBox16.Controls.Add(this.cbb_员工总数量);
            this.groupBox16.Controls.Add(this.cbb_网址);
            this.groupBox16.Controls.Add(this.cbb_所在国家);
            this.groupBox16.Controls.Add(this.cbb_供应商注册名);
            this.groupBox16.Controls.Add(this.cbb_供应商编码);
            this.groupBox16.Controls.Add(this.btn_重置);
            this.groupBox16.Controls.Add(this.btn_创建);
            this.groupBox16.Controls.Add(this.label93);
            this.groupBox16.Controls.Add(this.label94);
            this.groupBox16.Controls.Add(this.label95);
            this.groupBox16.Controls.Add(this.label96);
            this.groupBox16.Controls.Add(this.label97);
            this.groupBox16.Controls.Add(this.label98);
            this.groupBox16.Controls.Add(this.label99);
            this.groupBox16.Controls.Add(this.label100);
            this.groupBox16.Controls.Add(this.label101);
            this.groupBox16.Location = new System.Drawing.Point(3, 2);
            this.groupBox16.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox16.Size = new System.Drawing.Size(1211, 1006);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "创建新数据";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(868, 695);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 17);
            this.label15.TabIndex = 47;
            this.label15.Text = "联系人";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(451, 695);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 17);
            this.label14.TabIndex = 46;
            this.label14.Text = "公司代码";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 695);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 17);
            this.label13.TabIndex = 45;
            this.label13.Text = "分行";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(868, 593);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 17);
            this.label12.TabIndex = 44;
            this.label12.Text = "币种";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(451, 593);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 43;
            this.label11.Text = "银行账号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 593);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 17);
            this.label10.TabIndex = 42;
            this.label10.Text = "银行名称";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(868, 483);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 41;
            this.label9.Text = "纳税人登记号";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(160, 480);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(451, 483);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 17);
            this.label8.TabIndex = 39;
            this.label8.Text = "传真";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 483);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 38;
            this.label7.Text = "邮政编码";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(868, 384);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "供应商原名称";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(868, 291);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "是否有出口权";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(868, 203);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "股票代码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(868, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 34;
            this.label3.Text = "是否上市公司";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(868, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "法人代表";
            // 
            // cbb_注册时间
            // 
            this.cbb_注册时间.FormattingEnabled = true;
            this.cbb_注册时间.Location = new System.Drawing.Point(637, 381);
            this.cbb_注册时间.Name = "cbb_注册时间";
            this.cbb_注册时间.Size = new System.Drawing.Size(121, 24);
            this.cbb_注册时间.TabIndex = 32;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(451, 384);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "注册时间";
            // 
            // cbb_管理人员数量
            // 
            this.cbb_管理人员数量.FormattingEnabled = true;
            this.cbb_管理人员数量.Location = new System.Drawing.Point(637, 288);
            this.cbb_管理人员数量.Name = "cbb_管理人员数量";
            this.cbb_管理人员数量.Size = new System.Drawing.Size(121, 24);
            this.cbb_管理人员数量.TabIndex = 30;
            // 
            // cbb_详细地址
            // 
            this.cbb_详细地址.FormattingEnabled = true;
            this.cbb_详细地址.Location = new System.Drawing.Point(637, 200);
            this.cbb_详细地址.Name = "cbb_详细地址";
            this.cbb_详细地址.Size = new System.Drawing.Size(121, 24);
            this.cbb_详细地址.TabIndex = 29;
            // 
            // cbb_企业性质
            // 
            this.cbb_企业性质.FormattingEnabled = true;
            this.cbb_企业性质.Location = new System.Drawing.Point(637, 110);
            this.cbb_企业性质.Name = "cbb_企业性质";
            this.cbb_企业性质.Size = new System.Drawing.Size(121, 24);
            this.cbb_企业性质.TabIndex = 28;
            // 
            // cbb_供应商名称
            // 
            this.cbb_供应商名称.FormattingEnabled = true;
            this.cbb_供应商名称.Location = new System.Drawing.Point(637, 32);
            this.cbb_供应商名称.Name = "cbb_供应商名称";
            this.cbb_供应商名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商名称.TabIndex = 27;
            // 
            // cbb_员工总数量
            // 
            this.cbb_员工总数量.FormattingEnabled = true;
            this.cbb_员工总数量.Location = new System.Drawing.Point(160, 381);
            this.cbb_员工总数量.Name = "cbb_员工总数量";
            this.cbb_员工总数量.Size = new System.Drawing.Size(121, 24);
            this.cbb_员工总数量.TabIndex = 26;
            // 
            // cbb_网址
            // 
            this.cbb_网址.FormattingEnabled = true;
            this.cbb_网址.Location = new System.Drawing.Point(160, 288);
            this.cbb_网址.Name = "cbb_网址";
            this.cbb_网址.Size = new System.Drawing.Size(121, 24);
            this.cbb_网址.TabIndex = 25;
            // 
            // cbb_所在国家
            // 
            this.cbb_所在国家.FormattingEnabled = true;
            this.cbb_所在国家.Location = new System.Drawing.Point(160, 200);
            this.cbb_所在国家.Name = "cbb_所在国家";
            this.cbb_所在国家.Size = new System.Drawing.Size(121, 24);
            this.cbb_所在国家.TabIndex = 24;
            // 
            // cbb_供应商注册名
            // 
            this.cbb_供应商注册名.FormattingEnabled = true;
            this.cbb_供应商注册名.Location = new System.Drawing.Point(160, 113);
            this.cbb_供应商注册名.Name = "cbb_供应商注册名";
            this.cbb_供应商注册名.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商注册名.TabIndex = 23;
            // 
            // cbb_供应商编码
            // 
            this.cbb_供应商编码.FormattingEnabled = true;
            this.cbb_供应商编码.Location = new System.Drawing.Point(160, 32);
            this.cbb_供应商编码.Name = "cbb_供应商编码";
            this.cbb_供应商编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商编码.TabIndex = 22;
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(658, 941);
            this.btn_重置.Margin = new System.Windows.Forms.Padding(4);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(100, 31);
            this.btn_重置.TabIndex = 21;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // btn_创建
            // 
            this.btn_创建.Location = new System.Drawing.Point(486, 941);
            this.btn_创建.Margin = new System.Windows.Forms.Padding(4);
            this.btn_创建.Name = "btn_创建";
            this.btn_创建.Size = new System.Drawing.Size(100, 31);
            this.btn_创建.TabIndex = 20;
            this.btn_创建.Text = "创建";
            this.btn_创建.UseVisualStyleBackColor = true;
            this.btn_创建.Click += new System.EventHandler(this.btn_创建_Click);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(9, 384);
            this.label93.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(78, 17);
            this.label93.TabIndex = 16;
            this.label93.Text = "员工总数量";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(451, 291);
            this.label94.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(92, 17);
            this.label94.TabIndex = 14;
            this.label94.Text = "管理人员数量";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(9, 291);
            this.label95.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(36, 17);
            this.label95.TabIndex = 12;
            this.label95.Text = "网址";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(451, 208);
            this.label96.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(64, 17);
            this.label96.TabIndex = 10;
            this.label96.Text = "详细地址";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(9, 203);
            this.label97.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(64, 17);
            this.label97.TabIndex = 8;
            this.label97.Text = "所在国家";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(451, 113);
            this.label98.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(64, 17);
            this.label98.TabIndex = 6;
            this.label98.Text = "企业性质";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(8, 116);
            this.label99.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(92, 17);
            this.label99.TabIndex = 4;
            this.label99.Text = "供应商注册名";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(451, 32);
            this.label100.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(78, 17);
            this.label100.TabIndex = 2;
            this.label100.Text = "供应商名称";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(9, 35);
            this.label101.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(78, 17);
            this.label101.TabIndex = 0;
            this.label101.Text = "供应商编码";
            // 
            // SPEstablish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 1045);
            this.Controls.Add(this.groupBox16);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SPEstablish";
            this.Text = "供应商管理";
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_创建;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ComboBox cbb_管理人员数量;
        private System.Windows.Forms.ComboBox cbb_详细地址;
        private System.Windows.Forms.ComboBox cbb_企业性质;
        private System.Windows.Forms.ComboBox cbb_供应商名称;
        private System.Windows.Forms.ComboBox cbb_员工总数量;
        private System.Windows.Forms.ComboBox cbb_网址;
        private System.Windows.Forms.ComboBox cbb_所在国家;
        private System.Windows.Forms.ComboBox cbb_供应商注册名;
        private System.Windows.Forms.ComboBox cbb_供应商编码;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_注册时间;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}