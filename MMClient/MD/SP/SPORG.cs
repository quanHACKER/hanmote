﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;
using MMClient.MD.SP;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;
using Lib.Common.CommonUtils;

namespace MMClient.MD.SP
{
    public partial class SPORG : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SPORG()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        SupplierPurchaseORGBLL spo = new SupplierPurchaseORGBLL();
        SupplierBaseBLL spb = new SupplierBaseBLL();

        // 页面数据初始化函数
        public void SPORG_Load(object sender, EventArgs e)
        {
            //加载采购组织编码列表
            List<string> buyerGroupCodeList = gn.GetAllBuyerOrganization();
            if(buyerGroupCodeList != null)
            {
                //填充页面上部
                cbm.FuzzyQury(cbb_采购组织编码, buyerGroupCodeList);
                //填充页面底部
                cbm.FuzzyQury(cbb_采购组, buyerGroupCodeList);
            }

            //加载货币列表
            List<string> currencyList = gn.GetAllCurrency();
            if(currencyList != null)
            {
                cbm.FuzzyQury(cbb_订单货币, currencyList);
            }

            //加载付款条件列表
            List<string> paymentClauseNameList = gn.GetAllPaymentClauseName();
            if(paymentClauseNameList != null)
            {
                cbm.FuzzyQury(cbb_付款条件, paymentClauseNameList);
            }

            //加载贸易条件列表
            List<string> list4 = gn.GetAllTradeClause();
            if(list4 != null)
            {
                cbm.FuzzyQury(cbb_贸易条件, list4);
            } 

        }

        private void cbb_采购组织编码_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sl = "SELECT * FROM [Buyer_Org] WHERE Buyer_Org='" + cbb_采购组织编码.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sl);
            if (dt.Rows.Count != 0)
            {
                cbb_采购组织名称.Text = dt.Rows[0][1].ToString();
            }
            List<string> l1 = spb.GetAllSupplierID();
            SupplierPurchaseOrganization sporg = new SupplierPurchaseOrganization();
            if (cbb_供应商编码.Text == ""||gn.IDInTheList(l1,cbb_供应商编码.Text)==true)
                MessageUtil.ShowError("请选择正确的供应商编码");
          sporg =  spo.GetSupplierPurchaseOrganizationInformation(cbb_供应商编码.Text, cbb_采购组织编码.Text);
          cbb_订单货币.Text = sporg.Order_Units;
          cbb_付款条件.Text = sporg.Payment_Clause;
          cbb_贸易条件.Text = sporg.Trade_Clause;
          cbb_最小订单值.Text = sporg.MIN_OrderValue;
          cbb_方案组.Text = sporg.Supplier_Group;
          cbb_定价日期控制.Text = sporg.PricingDate_Control;
          cbb_订单优化限制.Text = sporg.Order_Limit;
          cbb_供应商累计.Text = sporg.Supplier_Has;
          ckb_基于收货的发票验证.Checked = sporg.Invoice_Verification;
          ckb_自动评估结算交货.Checked = sporg.Delivery_Settlement;
          ckb_自动评估结算保留.Checked = sporg.Settlement_Reserves;
          ckb_回执需求.Checked = sporg.ReturnReceipt_Requirements;
          ckb_自动建立采购订单.Checked = sporg.Automatic_PurchaseOrder;
          ckb_后续结算.Checked = sporg.Subsequent_Settlement;
          ckb_后续结算索引.Checked = sporg.Settlement_Index;
          ckb_所需业务量比较.Checked = sporg.Business_Comparison;
          ckb_单据索引有效的.Checked = sporg.Document_Indexin;
          ckb_退货供应商.Checked = sporg.Return_Supplier;
          ckb_基于服务的开票校验.Checked = sporg.Service_Verification;
          ckb_允许重新评估.Checked = sporg.Allow_Reevaluation;
          ckb_准许折扣.Checked = sporg.Allow_Discount;
          ckb_相关价格确定.Checked = sporg.Price_Determination;
          ckb_相关代理业务.Checked = sporg.Agent_Service;
          cbb_装运条件.Text = sporg.Shipment_Terms;
          cbb_ABC标识.Text = sporg.ABC_Identify;
          cbb_运输方式.Text = sporg.Transportation_Mode;
          cbb_输入管理处.Text = sporg.Input_ManagementOffice;
          cbb_排序标准.Text = sporg.Sort_Criteria;
          cbb_控制参数文件.Text = sporg.PRO_ControlData;
          cbb_采购组.Text = sporg.Buyer_Group;
          try
          {
              dtp_计划交货时间.Value = sporg.Delivery_Time;
          }
          catch (Exception ex)
          {
              
          }

          cbb_确认控制.Text = sporg.Control_Confirm;
          cbb_计量单位组.Text = sporg.Measurment_Group;
          cbb_舍入参数文件.Text = sporg.Rounding_Parameters;
          

            
            //foreach (Control col in this.Controls)
            //{
            //    if (col.GetType().Name.Equals("ComboBox") && col.Name.Equals("cbb_供应商编码") && col.Name.Equals("cbb_采购组织编码"))
            //    {
            //        ((ComboBox)col).Text = string.Empty;
            //    }
            //}
            //try
            //{
            //    string sql3 = "SELECT * FROM [Supplier_Purchasing_Org] where Supplier_ID='" + cbb_供应商编码.Text + "' and PurchasingORG_ID='" + cbb_采购组织编码.Text + "'";
            //    DataTable dt = DBHelper.ExecuteQueryDT(sql3);
            //    cbb_订单货币.Text = dt.Rows[0][5].ToString();
            //    cbb_供应商名称.Text = dt.Rows[0][2].ToString();
            //    cbb_采购组织名称.Text = dt.Rows[0][4].ToString();
                
            //    cbb_付款条件.Text = dt.Rows[0][6].ToString();
            //    cbb_贸易条件.Text = dt.Rows[0][7].ToString();
            //    cbb_最小订单值.Text = dt.Rows[0][8].ToString();
            //    cbb_方案组.Text = dt.Rows[0][9].ToString();
            //    cbb_定价日期控制.Text = dt.Rows[0][10].ToString();
            //    cbb_订单优化限制.Text = dt.Rows[0][11].ToString();
            //    cbb_供应商累计.Text = dt.Rows[0][12].ToString();

            //    if (dt.Rows[0][13].ToString().Equals("True"))
            //    {

            //        ckb_基于收货的发票验证.Checked = true;
            //    }
            //    if (dt.Rows[0][14].ToString().Equals("True"))
            //    {

            //        ckb_自动评估结算交货.Checked = true;
            //    }
            //    if (dt.Rows[0][15].ToString().Equals("True"))
            //    {
            //        ckb_自动评估结算保留.Checked = true;
            //    }
            //    if (dt.Rows[0][16].ToString().Equals("True"))
            //    {
            //        ckb_回执需求.Checked = true;
            //    }
            //    if (dt.Rows[0][17].ToString().Equals("True"))
            //    {
            //        ckb_自动建立采购订单.Checked = true;
            //    }
            //    if (dt.Rows[0][18].ToString().Equals("True"))
            //    {
            //        ckb_后续结算.Checked = true;
            //    }
            //    if (dt.Rows[0][19].ToString().Equals("True"))
            //    {
            //        ckb_后续结算索引.Checked = true;
            //    }
            //    if (dt.Rows[0][20].ToString().Equals("True"))
            //    {
            //        ckb_所需业务量比较.Checked = true;
            //    }
            //    if (dt.Rows[0][21].ToString().Equals("True"))
            //    {
            //        ckb_单据索引有效的.Checked = true;
            //    }
            //    if (dt.Rows[0][22].ToString().Equals("True"))
            //    {
            //        ckb_退货供应商.Checked = true;
            //    }
            //    if (dt.Rows[0][23].ToString().Equals("True"))
            //    {
            //        ckb_基于服务的开票校验.Checked = true;
            //    }
            //    if (dt.Rows[0][24].ToString().Equals("True"))
            //    {
            //        ckb_允许重新评估.Checked = true;
            //    }
            //    if (dt.Rows[0][25].ToString().Equals("True"))
            //    {
            //        ckb_准许折扣.Checked = true;
            //    }
            //    if (dt.Rows[0][26].ToString().Equals("True"))
            //    {
            //        ckb_相关价格确定.Checked = true;
            //    }
            //    if (dt.Rows[0][27].ToString().Equals("True"))
            //    {
            //        ckb_相关代理业务.Checked = true;
            //    }
            //    cbb_装运条件.Text = dt.Rows[0][28].ToString();
            //    cbb_ABC标识.Text = dt.Rows[0][29].ToString();
            //    cbb_运输方式.Text = dt.Rows[0][30].ToString();
            //    cbb_输入管理处.Text = dt.Rows[0][31].ToString();
            //    cbb_排序标准.Text = dt.Rows[0][32].ToString();
            //    cbb_控制参数文件.Text = dt.Rows[0][33].ToString();
            //    cbb_采购组.Text = dt.Rows[0][34].ToString();
            //    cbb_计划交货时间.Text = dt.Rows[0][35].ToString();
            //    cbb_确认控制.Text = dt.Rows[0][36].ToString();
            //    cbb_计量单位组.Text = dt.Rows[0][37].ToString();
            //    cbb_舍入参数文件.Text = dt.Rows[0][38].ToString();
            //}
            //catch (Exception ex)
            //{
            //    //cbb_ABC标识.Text = "";
            //    //cbb_采购组.Text = "";
            //    //cbb_采购组织编码.Text = "";
            //    //cbb_采购组织名称.Text = "";
            //    //cbb_订单货币.Text = "";
            //    //cbb
            //    string sql4 = " SELECT * FROM [Supplier_Base] WHERE Supplier_ID='" + cbb_供应商编码.Text + "' ";
            //    DataTable dt4 = DBHelper.ExecuteQueryDT(sql4);
            //    string sql5 = " SELECT * FROM [Buyer_Org] WHERE Buyer_Org='" + cbb_采购组织编码.Text + "' ";
            //    DataTable dt5 = DBHelper.ExecuteQueryDT(sql5);
                

            //    cbb_供应商名称.Text = dt4.Rows[0][3].ToString();
            //    cbb_采购组织名称.Text = dt5.Rows[0][1].ToString();
            //}

        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
           

        }

        private void btn_重置_Click(object sender, EventArgs e)
        {
           
        }

        private void cbb_供应商编码_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SupplierChose spc = new SupplierChose(this);
            spc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                string sql4 = " INSERT INTO [Supplier_Purchasing_Org](Supplier_ID,Supplier_Name,PurchasingORG_ID,PurchasingORG_Name,Order_Units,Payment_Clause,Trade_Clause,MIN_OrderValue,Supplier_Group,PricingDate_Control,Order_Limit,Supplier_Has,Invoice_Verification,Delivery_Settlement,Settlement_Reserves,ReturnReceipt_Requirements,Automatic_PurchaseOrder,Subsequent_Settlement,Settlement_Index,Business_Comparison,Document_Indexin,Return_Supplier,Service_Verification,Allow_Reevaluation,Allow_Discount,Price_Determination,Agent_Service,Shipment_Terms,ABC_Identify,Transportation_Mode,Input_ManagementOffice,Sort_Criteria,PRO_ControlData,Buyer_Group,Delivery_Time,Control_Confirm,Measurment_Group,Rounding_Parameters) Values ('" + cbb_供应商编码.Text + "','" + cbb_供应商名称.Text + "','" + cbb_采购组织编码.Text + "','" + cbb_采购组织名称.Text + "','" + cbb_订单货币.Text + "','" + cbb_付款条件.Text + "','" + cbb_贸易条件.Text + "','" + cbb_最小订单值.Text + "','" + cbb_方案组.Text + "','" + cbb_定价日期控制.Text + "','" + cbb_订单优化限制.Text + "','" + cbb_供应商累计.Text + "','" + ckb_基于收货的发票验证.Checked + "','" + ckb_自动评估结算交货.Checked + "','" + ckb_自动评估结算保留.Checked + "','" + ckb_回执需求.Checked + "','" + ckb_自动建立采购订单.Checked + "','" + ckb_后续结算.Checked + "','" + ckb_后续结算索引.Checked + "','" + ckb_所需业务量比较.Checked + "','" + ckb_单据索引有效的.Checked + "','" + ckb_退货供应商.Checked + "','" + ckb_基于服务的开票校验.Checked + "','" + ckb_允许重新评估.Checked + "','" + ckb_准许折扣.Checked + "','" + ckb_相关价格确定.Checked + "','" + ckb_相关代理业务.Checked + "','" + cbb_装运条件.Text + "','" + cbb_ABC标识.Text + "','" + cbb_运输方式.Text + "','" + cbb_输入管理处.Text + "','" + cbb_排序标准.Text + "','" + cbb_控制参数文件.Text + "','" + cbb_采购组.Text + "','" + dtp_计划交货时间.Value + "','" + cbb_确认控制.Text + "','" + cbb_计量单位组.Text + "','" + cbb_舍入参数文件.Text + "')";
                DBHelper.ExecuteNonQuery(sql4);
                MessageBox.Show("创建成功");
            }

            catch (Exception ex)
            {
                string sql5 = " UPDATE [Supplier_Purchasing_Org] SET Order_Units='" + cbb_订单货币.Text + "',Payment_Clause='" + cbb_付款条件.Text + "',Trade_Clause='" + cbb_贸易条件.Text + "',MIN_OrderValue='" + cbb_最小订单值.Text + "',Supplier_Group='" + cbb_方案组.Text + "',PricingDate_Control='" + cbb_定价日期控制.Text + "',Order_Limit='" + cbb_订单优化限制.Text + "',Supplier_Has='" + cbb_供应商累计.Text + "',Invoice_Verification='" + ckb_基于收货的发票验证.Checked + "',Delivery_Settlement='" + ckb_自动评估结算交货.Checked + "',Settlement_Reserves='" + ckb_自动评估结算保留.Checked + "',ReturnReceipt_Requirements='" + ckb_回执需求.Checked + "',Automatic_PurchaseOrder='" + ckb_自动建立采购订单.Checked + "',Subsequent_Settlement='" + ckb_后续结算.Checked + "',Settlement_Index='" + ckb_后续结算索引.Checked + "',Business_Comparison='" + ckb_所需业务量比较.Checked + "',Document_Indexin='" + ckb_单据索引有效的.Checked + "',Return_Supplier='" + ckb_退货供应商.Checked + "',Service_Verification='" + ckb_基于服务的开票校验.Checked + "',Allow_Reevaluation='" + ckb_允许重新评估.Checked + "',Allow_Discount='" + ckb_准许折扣.Checked + "',Price_Determination='" + ckb_相关价格确定.Checked + "',Agent_Service='" + ckb_相关代理业务.Checked + "',Shipment_Terms='" + cbb_装运条件.Text + "',ABC_Identify='" + cbb_ABC标识.Text + "',Transportation_Mode='" + cbb_运输方式.Text + "',Input_ManagementOffice='" + cbb_输入管理处.Text + "',Sort_Criteria='" + cbb_排序标准.Text + "',PRO_ControlData='" + cbb_控制参数文件.Text + "',Buyer_Group='" + cbb_采购组.Text + "',Delivery_Time='" + dtp_计划交货时间.Value + "',Control_Confirm='" + cbb_确认控制.Text + "',Measurment_Group='" + cbb_计量单位组.Text + "',Rounding_Parameters='" + cbb_舍入参数文件.Text + "' WHERE Supplier_ID='" + cbb_供应商编码.Text + "' AND PurchasingORG_ID='" + cbb_采购组织编码.Text + "'";
                DBHelper.ExecuteNonQuery(sql5);
                MessageBox.Show("维护成功");
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            foreach (Control col in this.Controls)
            {
                if (col.GetType().Name.Equals("ComboBox"))
                {
                    ((ComboBox)col).Text = string.Empty;
                }
                if (col.GetType().Name.Equals("CheckBox"))
                {
                    ((CheckBox)col).Text = string.Empty;
                }
            }
            cbb_订单货币.Text = "";
            cbb_订单货币.Text = "";
            cbb_供应商名称.Text = "";
            cbb_订单货币.Text = "";
            cbb_采购组织名称.Text = "";
            cbb_付款条件.Text = "";
            cbb_贸易条件.Text = "";
            cbb_最小订单值.Text = "";
            cbb_方案组.Text = "";
            cbb_定价日期控制.Text = "";
            cbb_订单优化限制.Text = "";
            cbb_供应商累计.Text = "";


            ckb_基于收货的发票验证.Checked = false;


            ckb_自动评估结算交货.Checked = false;

            ckb_自动评估结算保留.Checked = false;

            ckb_回执需求.Checked = false;

            ckb_自动建立采购订单.Checked = false;

            ckb_后续结算.Checked = false;

            ckb_后续结算索引.Checked = false;

            ckb_所需业务量比较.Checked = false;

            ckb_单据索引有效的.Checked = false;

            ckb_退货供应商.Checked = false;

            ckb_基于服务的开票校验.Checked = false;

            ckb_允许重新评估.Checked = false;

            ckb_准许折扣.Checked = false;

            ckb_相关价格确定.Checked = false;

            ckb_相关代理业务.Checked = false;

            cbb_装运条件.Text = "";
            cbb_ABC标识.Text = "";
            cbb_运输方式.Text = "";
            cbb_输入管理处.Text = "";
            cbb_排序标准.Text = "";
            cbb_控制参数文件.Text = "";
            cbb_采购组.Text = "";
            dtp_计划交货时间.Text = "";
            cbb_确认控制.Text = "";
            cbb_计量单位组.Text = "";
            cbb_舍入参数文件.Text = "";
        }

        private void cbb_采购组织名称_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
