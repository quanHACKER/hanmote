﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MMClient.MD.MT.StrategInfo
{
    public partial class stategeTypePopUp : Form
    {
        public stategeTypePopUp()
        {
            InitializeComponent();
        }

        private void stategeTypePopUp_Load(object sender, EventArgs e)
        {
            //查询类型信息
            dgv_StategyType.DataSource = DBHelper.ExecuteQueryDT("select typeId,typeName from TabstrategyType");
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv_StategyType.CurrentRow == null)
            {
                MessageUtil.ShowWarning("请选择要保存的行！");
                return;
            }
            else {
                string sqltext = @"if EXISTS (select * from TabstrategyType where typeId=@typeId)
	                                UPDATE TabstrategyType set typeName=@typeName where typeId=@typeId
                                else
	                                INSERT into TabstrategyType(typeId,typeName) VALUES (@typeId,@typeName)";
                string typeId = dgv_StategyType.CurrentRow.Cells["typeId"].Value.ToString().Trim();
                string typeName = dgv_StategyType.CurrentRow.Cells["typeName"].Value.ToString().Trim();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@typeId",typeId),
                    new SqlParameter("@typeName",typeName)
                };

                try
                {
                    if (DBHelper.ExecuteNonQuery(sqltext, sqlParameters) == 1)
                    {

                        MessageUtil.ShowTips("保存成功！");
                    }
                }
                catch (Exception ex)
                {

                    MessageUtil.ShowError("保存失败！"+ex.Message);
                }

               
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int flag = DBHelper.ExecuteNonQuery("delete from TabstrategyType where typeId='" + dgv_StategyType.CurrentRow.Cells["typeId"].Value + "'");
                if (flag > 0)
                {
                    MessageUtil.ShowTips("删除成功！");

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("删除失败！" + ex.Message);

            }
        }

        private void 保存ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            保存ToolStripMenuItem_Click(sender, e);
        }
    }
}
