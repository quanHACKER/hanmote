﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT.StrategInfo
{
    public partial class StrateInfo : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public StrateInfo()
        {
            InitializeComponent();
        }

        private void StrateInfo_Load(object sender, EventArgs e)
        {
            List<string> typeId = new List<string>();

            DataTable dt = DBHelper.ExecuteQueryDT("select typeId,typeName from TabstrategyType");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    typeId.Add(dt.Rows[i]["typeId"].ToString());

                }
            }
            else {
                MessageUtil.ShowTips("暂无数据！");
            }

            CB_strategyTypeName.DataSource = typeId;
        }

        private void CB_strategyTypeName_TextChanged(object sender, EventArgs e)
        {

            try
            {
                dgv_strategyInfo.DataSource = DBHelper.ExecuteQueryDT("select strategyId, strategyInfo from TabStrategyInfo where typeId='" + CB_strategyTypeName.Text + "'");
                lb_typeName.Text = DBHelper.ExecuteQueryDT("select typeName from TabstrategyType where typeId='" + CB_strategyTypeName.Text + "'").Rows[0][0].ToString();
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("初始化错误！"+ex.Message);
            } 
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv_strategyInfo.CurrentRow == null)
            {
                MessageUtil.ShowWarning("请选择要保存的行！");
                return;
            }
            else
            {
                string sqltext = @"IF EXISTS ( SELECT * FROM TabStrategyInfo WHERE strategyId = @strategyId and typeId=@typeId) 
                                            UPDATE TabStrategyInfo  SET strategyInfo = @strategyInfo where strategyId = @strategyId and typeId=@typeId 
                                    ELSE 
                                            INSERT INTO TabStrategyInfo ( strategyId, strategyInfo, typeId ) VALUES   ( @strategyId, @strategyInfo,@typeId)";
                string typeId = CB_strategyTypeName.Text;
                string strategyId = dgv_strategyInfo.CurrentRow.Cells["strategyId"].Value.ToString().Trim();
                string strategyInfo = dgv_strategyInfo.CurrentRow.Cells["strategyInfo"].Value.ToString().Trim();
                if (strategyId.Equals("") || strategyInfo.Equals("")) {
                    MessageUtil.ShowTips("选择行的信息不完整！请补充完整");
                    return;

                }


                SqlParameter[] sqlParameters = {
                    new SqlParameter("@typeId",typeId),
                    new SqlParameter("@strategyId",strategyId),
                    new SqlParameter("@strategyInfo",strategyInfo),

                };

                try
                {
                    if (DBHelper.ExecuteNonQuery(sqltext, sqlParameters) == 1)
                    {

                        MessageUtil.ShowTips("保存成功！");
                    }
                }
                catch (Exception ex)
                {

                    MessageUtil.ShowError("保存失败！" + ex.Message);
                }


            }

        }

        private void 区分类型维护ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stategeTypePopUp stategeTypePopUp = new stategeTypePopUp();
            stategeTypePopUp.ShowDialog();
            StrateInfo_Load(sender, e);
            
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int flag = DBHelper.ExecuteNonQuery("delete from TabStrategyInfo where strategyId='" + dgv_strategyInfo.CurrentRow.Cells["strategyId"].Value + "'");
                if (flag > 0)
                {
                    MessageUtil.ShowTips("删除成功！");

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("删除失败！"+ex.Message);

            }
        }

        private void 保存ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            保存ToolStripMenuItem_Click(sender, e);
        }
    }
}
