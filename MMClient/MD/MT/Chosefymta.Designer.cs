﻿namespace MMClient.MD.MT
{
    partial class Chosefymta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chosefymta));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_工厂 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.groupBox1.Location = new System.Drawing.Point(3, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(928, 303);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "组织级别";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(935, 67);
            this.toolStripExecutionMonitorSetting.TabIndex = 18;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // certainButton
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("certainButton.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "certainButton";
            this.toolStripButton4.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(9, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(923, 181);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "组织级别";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_确定);
            this.groupBox3.Controls.Add(this.cbb_工厂);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(6, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(905, 217);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "组织层次";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(492, 165);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 35);
            this.btn_确定.TabIndex = 2;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            // 
            // cbb_工厂
            // 
            this.cbb_工厂.FormattingEnabled = true;
            this.cbb_工厂.Location = new System.Drawing.Point(162, 55);
            this.cbb_工厂.Name = "cbb_工厂";
            this.cbb_工厂.Size = new System.Drawing.Size(102, 31);
            this.cbb_工厂.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 58);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(44, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "工厂";
            // 
            // Chosefymta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 331);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Chosefymta";
            this.Text = "会计视图：选择工厂";
            this.groupBox1.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_确定;
        public System.Windows.Forms.ComboBox cbb_工厂;
        private System.Windows.Forms.Label label1;
    }
}