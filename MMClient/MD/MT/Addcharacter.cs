﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.MT
{
    public partial class Addcharacter : Form
    {
        public Addcharacter()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (dgv_cha.CurrentRow == null)
                MessageUtil.ShowError("请选择要修改的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
                {
                    string id = "0";
                    string userid = dgv_cha.CurrentRow.Cells[0].Value.ToString(); 
                    string name = dgv_cha.CurrentRow.Cells[1].Value.ToString();
                    string password = dgv_cha.CurrentRow.Cells[2].Value.ToString();
                    string fengong = dgv_cha.CurrentRow.Cells[3].Value.ToString();
                    string email = dgv_cha.CurrentRow.Cells[4].Value.ToString();
                    string phone = dgv_cha.CurrentRow.Cells[5].Value.ToString();
                    string sphone = dgv_cha.CurrentRow.Cells[6].Value.ToString();
                    
                    gn.UpdateSupplier_pinshen(id, userid,name,password,fengong, email, phone,sphone);
                    // dgv_物料组.Rows.Remove(dgv_物料组.CurrentRow);
                    MessageUtil.ShowTips("修改成功");
                }
            }
        }

        private void btn_bao_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            //string id = textBox1.Text;
            string userid = textBox1.Text;
            string name = textBox2 .Text;
            string fengong = comboBox1.Text;
            string email = textBox3.Text;
            string phone = textBox4.Text;
            string sphone = staticPhone.Text ;
            string password =passWord .Text;



            try
            {
                gn.NewSupplier_pinshen(userid, name, fengong, email,phone,sphone ,password );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            MessageBox.Show("评审员信息录入成功");
        }

        private void btn_shua_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllSupplier_pinshen();
            dgv_cha.DataSource = dt;
        }

        private void btn_shan_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (dgv_cha.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string id = dgv_cha.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteSupplier_pinshen(id);
                    dgv_cha.Rows.Remove(dgv_cha.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }

            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void dgv_cha_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
