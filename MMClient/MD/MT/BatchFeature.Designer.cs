﻿namespace MMClient.MD.MT
{
    partial class BatchFeature
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchFeature));
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_批次编码 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_项目 = new System.Windows.Forms.DataGridView();
            this.cln_特性名称 = new MDBll.Batch.DataGridViewComboEditBoxColumn();
            this.cln_特性值 = new MDBll.Batch.DataGridViewComboEditBoxColumn();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).BeginInit();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 100);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "批次编码";
            // 
            // tbx_批次编码
            // 
            this.tbx_批次编码.Location = new System.Drawing.Point(76, 95);
            this.tbx_批次编码.Name = "tbx_批次编码";
            this.tbx_批次编码.Size = new System.Drawing.Size(125, 19);
            this.tbx_批次编码.TabIndex = 1;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dgv_项目);
            this.groupBox1.Location = new System.Drawing.Point(9, 132);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(720, 346);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "批次特性";
            // 
            // dgv_项目
            // 
            this.dgv_项目.AllowUserToAddRows = false;
            this.dgv_项目.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_特性名称,
            this.cln_特性值});
            this.dgv_项目.Location = new System.Drawing.Point(7, 21);
            this.dgv_项目.Name = "dgv_项目";
            this.dgv_项目.RowTemplate.Height = 24;
            this.dgv_项目.Size = new System.Drawing.Size(707, 388);
            this.dgv_项目.TabIndex = 0;
            this.dgv_项目.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dgv_项目.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_项目_DataError);
            // 
            // cln_特性名称
            // 
            this.cln_特性名称.HeaderText = "特性名称";
            this.cln_特性名称.Name = "cln_特性名称";
            this.cln_特性名称.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cln_特性值
            // 
            this.cln_特性值.HeaderText = "特性值";
            this.cln_特性值.Name = "cln_特性值";
            this.cln_特性值.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(741, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 18;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // certainButton
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("certainButton.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "certainButton";
            this.toolStripButton4.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // BatchFeature
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 498);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbx_批次编码);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "BatchFeature";
            this.Text = "建立批次";
            this.Load += new System.EventHandler(this.BatchFeature_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_项目)).EndInit();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_批次编码;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_项目;
        private MDBll.Batch.DataGridViewComboEditBoxColumn cln_特性名称;
        private MDBll.Batch.DataGridViewComboEditBoxColumn cln_特性值;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
    }
}