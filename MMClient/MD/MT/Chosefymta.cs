﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Model.MD.MT;
using Lib.Bll.MDBll.MT;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;


namespace MMClient.MD.MT
{
    public partial class Chosefymta : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        MTACT mta;
        MaterialFactory mtfty = new MaterialFactory();
        MaterialAccountBLL MAB = new MaterialAccountBLL();
        GeneralBLL gn = new GeneralBLL();
        
        public Chosefymta(MTACT mta)
        {
            InitializeComponent();
            this.mta = mta;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_工厂.Text != "")
            {
                this.mta.cbb_工厂编码.Text = cbb_工厂.Text;
                string sql = "SELECT * FROM [Factory] WHERE Factory_ID='" + cbb_工厂.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.mta.cbb_工厂名称.Text = dt.Rows[0][2].ToString();
                this.Close();
            }
            else
            {
                this.mta.cbb_工厂编码.Text = "";
                this.Close();

                this.mta.cbb_本期.Visible = false;
                this.mta.cbb_标准价格.Visible = false;
                this.mta.cbb_工厂编码.Visible = false;
                this.mta.cbb_工厂名称.Visible = false;
                this.mta.cbb_货币.Visible = false;
                this.mta.cbb_价格单位.Visible = false;
                this.mta.cbb_价格控制.Visible = false;
                this.mta.cbb_价格确定.Visible = false;
                this.mta.cbb_评估分类.Visible = false;
                this.mta.cbb_评估类.Visible = false;
                this.mta.cbb_评估类别.Visible = false;
                this.mta.cbb_未来价格.Visible = false;
                this.mta.cbb_销售订单库存.Visible = false;
                this.mta.cbb_移动平均价.Visible = false;
                this.mta.cbb_总价值.Visible = false;
                this.mta.cbb_总库存.Visible = false;
                this.mta.cbb_本期.Enabled = false;
                this.mta.cbb_标准价格.Enabled = false;
                this.mta.cbb_工厂编码.Enabled = false;
                this.mta.cbb_工厂名称.Enabled = false;
                this.mta.cbb_货币.Enabled = false;
                this.mta.cbb_价格单位.Enabled = false;
                this.mta.cbb_价格控制.Enabled = false;
                this.mta.cbb_价格确定.Enabled = false;
                this.mta.cbb_评估分类.Enabled = false;
                this.mta.cbb_评估类.Enabled = false;
                this.mta.cbb_评估类别.Enabled = false;
                this.mta.cbb_未来价格.Enabled = false;
                this.mta.cbb_销售订单库存.Enabled = false;
                this.mta.cbb_移动平均价.Enabled = false;
                this.mta.cbb_总价值.Enabled = false;
                this.mta.cbb_总库存.Enabled = false;
                this.mta.gbx_当前估价.Visible = false;
                



            }


        }

        private void cbb_工厂_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            //string sql3 = "SELECT * FROM [MT_FTY] WHERE Material_ID='" + this.mta.cbb_物料编号.Text + "' AND Factory_ID='" + cbb_工厂.Text + "'";
            //DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            //try
            //{
            //    this.mta.cbb_本期.Text = dt3.Rows[0][12].ToString();
            //    this.mta.cbb_标准价格.Text = dt3.Rows[0][21].ToString();
            //    this.mta.cbb_货币.Text = dt3.Rows[0][10].ToString();
            //    this.mta.cbb_价格单位.Text = dt3.Rows[0][20].ToString();
            //    this.mta.cbb_价格控制.Text = dt3.Rows[0][16].ToString();
            //    this.mta.cbb_价格确定.Text = dt3.Rows[0][13].ToString();
            //    this.mta.cbb_评估分类.Text = dt3.Rows[0][19].ToString();
            //    this.mta.cbb_评估类.Text = dt3.Rows[0][14].ToString();
            //    this.mta.cbb_评估类别.Text = dt3.Rows[0][11].ToString();
            //    this.mta.cbb_起始期.Text = dt3.Rows[0][9].ToString();
            //    this.mta.cbb_未来价格.Text = dt3.Rows[0][23].ToString();
            //    this.mta.cbb_销售订单库存.Text = dt3.Rows[0][15].ToString();
            //    this.mta.cbb_移动平均价.Text = dt3.Rows[0][17].ToString();
            //    this.mta.cbb_总价值.Text = dt3.Rows[0][22].ToString();
            //    this.mta.cbb_总库存.Text = dt3.Rows[0][18].ToString();
            //}
            //catch (Exception ex)
            //{
            //    this.mta.cbb_本期.Text = "";
            //    this.mta.cbb_标准价格.Text = "0.0";
            //    this.mta.cbb_货币.Text = "";
            //    this.mta.cbb_价格单位.Text = "";
            //    this.mta.cbb_价格控制.Text = "";
            //    this.mta.cbb_价格确定.Text = "";
            //    this.mta.cbb_评估分类.Text = "";
            //    this.mta.cbb_评估类.Text = "";
            //    this.mta.cbb_评估类别.Text = "";
            //    this.mta.cbb_起始期.Text = "";
            //    this.mta.cbb_未来价格.Text = "0.0";
            //    this.mta.cbb_销售订单库存.Text = "";
            //    this.mta.cbb_移动平均价.Text = "0.0";
            //    this.mta.cbb_总价值.Text = "0.0";
            //    this.mta.cbb_总库存.Text = "";
            //}

        }

        private void cbb_工厂_Leave(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllFactory();
            if (cbb_工厂.Text != "" && gn.IDInTheList(list, cbb_工厂.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的工厂");
                cbb_工厂.Focus();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (cbb_工厂.Text != "")
            {
                this.mta.cbb_工厂编码.Text = cbb_工厂.Text;
                string sql = "SELECT * FROM [Factory] WHERE Factory_ID='" + cbb_工厂.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.mta.cbb_工厂名称.Text = dt.Rows[0][2].ToString();
                this.mta.cbb_计量单位.Enabled = false;
                this.mta.cbb_产品组.Enabled = false;
                MaterialFactory mtftyinformation = MAB.GetMtFtyInformation(this.mta.cbb_物料编号.Text, cbb_工厂.Text);
                this.mta.cbb_价格控制.Text = mtftyinformation.Price_Control;

                this.mta.cbb_标准价格.Text = mtftyinformation.Normal_Price.ToString();
                this.mta.cbb_货币.Text = mtftyinformation.Currency_Unit.ToString();
                this.mta.cbb_价格单位.Text = mtftyinformation.Price_Unit.ToString();
                this.mta.cbb_评估类.Text = mtftyinformation.Evaluation_Class.ToString();
                this.mta.cbb_评估类别.Text = mtftyinformation.Assessment_Category.ToString();
                this.mta.cbb_未来价格.Text =Convert.ToString( Convert.ToDouble( mtftyinformation.Future_Price.ToString()));
                this.mta.cbb_移动平均价.Text = mtftyinformation.Moving_AGPrice.ToString();
                this.mta.cbb_总价值.Text = mtftyinformation.TotalValue.ToString();
                this.mta.cbb_总库存.Text = mtftyinformation.Gross_Inventory.ToString();
                if (mtftyinformation.Price_Control != null)
                    this.mta.cbb_价格控制.Text = mtftyinformation.Price_Control.ToString();
                else
                    this.mta.cbb_价格控制.Text = "";
                this.Close();
            }
            else
            {
                this.mta.cbb_工厂编码.Text = "";
                this.Close();

                this.mta.cbb_本期.Visible = false;
                this.mta.cbb_标准价格.Visible = false;
                this.mta.cbb_工厂编码.Visible = false;
                this.mta.cbb_工厂名称.Visible = false;
                this.mta.cbb_货币.Visible = false;
                this.mta.cbb_价格单位.Visible = false;
                this.mta.cbb_价格控制.Visible = false;
                this.mta.cbb_价格确定.Visible = false;
                this.mta.cbb_评估分类.Visible = false;
                this.mta.cbb_评估类.Visible = false;
                this.mta.cbb_评估类别.Visible = false;
                this.mta.cbb_未来价格.Visible = false;
                this.mta.cbb_销售订单库存.Visible = false;
                this.mta.cbb_移动平均价.Visible = false;
                this.mta.cbb_总价值.Visible = false;
                this.mta.cbb_总库存.Visible = false;
                this.mta.cbb_本期.Enabled = false;
                this.mta.cbb_标准价格.Enabled = false;
                this.mta.cbb_工厂编码.Enabled = false;
                this.mta.cbb_工厂名称.Enabled = false;
                this.mta.cbb_货币.Enabled = false;
                this.mta.cbb_价格单位.Enabled = false;
                this.mta.cbb_价格控制.Enabled = false;
                this.mta.cbb_价格确定.Enabled = false;
                this.mta.cbb_评估分类.Enabled = false;
                this.mta.cbb_评估类.Enabled = false;
                this.mta.cbb_评估类别.Enabled = false;
                this.mta.cbb_未来价格.Enabled = false;
                this.mta.cbb_销售订单库存.Enabled = false;
                this.mta.cbb_移动平均价.Enabled = false;
                this.mta.cbb_总价值.Enabled = false;
                this.mta.cbb_总库存.Enabled = false;
                this.mta.gbx_当前估价.Visible = false;




            }
        }
    }
}
