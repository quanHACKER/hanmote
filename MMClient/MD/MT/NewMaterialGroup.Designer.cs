﻿namespace MMClient.MD.MT
{
    partial class NewMaterialGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TB_PFLowValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TB_PFGreenValue = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TB_CLowValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_CGreenValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CB_mGroupLevel = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_物料组描述 = new System.Windows.Forms.TextBox();
            this.tbx_物料组编号 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.CB_mGroupLevel);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbx_物料组描述);
            this.groupBox1.Controls.Add(this.tbx_物料组编号);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Location = new System.Drawing.Point(5, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(575, 482);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建物料组";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TB_PFLowValue);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TB_PFGreenValue);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TB_CLowValue);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.TB_CGreenValue);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(86, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(426, 208);
            this.groupBox2.TabIndex = 73;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "绿色目标值与最低目标值";
            // 
            // TB_PFLowValue
            // 
            this.TB_PFLowValue.Location = new System.Drawing.Point(132, 167);
            this.TB_PFLowValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_PFLowValue.Name = "TB_PFLowValue";
            this.TB_PFLowValue.Size = new System.Drawing.Size(147, 21);
            this.TB_PFLowValue.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 176);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 12);
            this.label8.TabIndex = 80;
            this.label8.Text = "绩效最低目标值";
            this.toolTip1.SetToolTip(this.label8, "绩效评估标准，A级:大于等于绿色目标值、B级：小于绿色目标值大于等于最低目标值，C级:小于最低目标值");
            // 
            // TB_PFGreenValue
            // 
            this.TB_PFGreenValue.Location = new System.Drawing.Point(132, 128);
            this.TB_PFGreenValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_PFGreenValue.Name = "TB_PFGreenValue";
            this.TB_PFGreenValue.Size = new System.Drawing.Size(147, 21);
            this.TB_PFGreenValue.TabIndex = 79;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 131);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 12);
            this.label9.TabIndex = 78;
            this.label9.Text = "绩效绿色目标值";
            this.toolTip1.SetToolTip(this.label9, "绩效评估的标准");
            // 
            // TB_CLowValue
            // 
            this.TB_CLowValue.Location = new System.Drawing.Point(132, 82);
            this.TB_CLowValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_CLowValue.Name = "TB_CLowValue";
            this.TB_CLowValue.Size = new System.Drawing.Size(147, 21);
            this.TB_CLowValue.TabIndex = 77;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 85);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 76;
            this.label6.Text = "认证最低目标值";
            this.toolTip1.SetToolTip(this.label6, "供应商认证时的分级标准");
            // 
            // TB_CGreenValue
            // 
            this.TB_CGreenValue.Location = new System.Drawing.Point(132, 37);
            this.TB_CGreenValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_CGreenValue.Name = "TB_CGreenValue";
            this.TB_CGreenValue.Size = new System.Drawing.Size(147, 21);
            this.TB_CGreenValue.TabIndex = 75;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 40);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 74;
            this.label5.Text = "认证绿色目标值";
            this.toolTip1.SetToolTip(this.label5, "供应商认证时的分级标准");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(310, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 70;
            this.label3.Text = "物料组等级";
            // 
            // CB_mGroupLevel
            // 
            this.CB_mGroupLevel.FormattingEnabled = true;
            this.CB_mGroupLevel.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.CB_mGroupLevel.Location = new System.Drawing.Point(390, 130);
            this.CB_mGroupLevel.Name = "CB_mGroupLevel";
            this.CB_mGroupLevel.Size = new System.Drawing.Size(121, 20);
            this.CB_mGroupLevel.TabIndex = 69;
            this.toolTip1.SetToolTip(this.CB_mGroupLevel, "物料等级为审批等级：\r\nA:4\r\nB:3\r\nC:2\r\nD:1");
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "生产性物料",
            "生产性资本性物料",
            "维持公司运营物料"});
            this.comboBox1.Location = new System.Drawing.Point(146, 125);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(147, 20);
            this.comboBox1.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 67;
            this.label7.Text = "所属分类";
            // 
            // tbx_物料组描述
            // 
            this.tbx_物料组描述.Location = new System.Drawing.Point(394, 61);
            this.tbx_物料组描述.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组描述.Name = "tbx_物料组描述";
            this.tbx_物料组描述.Size = new System.Drawing.Size(117, 21);
            this.tbx_物料组描述.TabIndex = 7;
            // 
            // tbx_物料组编号
            // 
            this.tbx_物料组编号.Location = new System.Drawing.Point(146, 60);
            this.tbx_物料组编号.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组编号.Name = "tbx_物料组编号";
            this.tbx_物料组编号.Size = new System.Drawing.Size(147, 21);
            this.tbx_物料组编号.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "物料组描述";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料组编号";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(11, 442);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(147, 21);
            this.textBox6.TabIndex = 64;
            this.textBox6.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 413);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 34;
            this.button2.Text = "文件上传";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 387);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "物料组文档上传";
            this.label4.Visible = false;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(452, 409);
            this.btn_确定.Margin = new System.Windows.Forms.Padding(2);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(59, 27);
            this.btn_确定.TabIndex = 8;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // NewMaterialGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 497);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NewMaterialGroup";
            this.Text = "新建物料组";
            this.Load += new System.EventHandler(this.NewMaterialGroup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_物料组描述;
        private System.Windows.Forms.TextBox tbx_物料组编号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_mGroupLevel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TB_PFLowValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TB_PFGreenValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TB_CLowValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_CGreenValue;
        private System.Windows.Forms.Label label5;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}