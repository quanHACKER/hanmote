﻿using Lib.Bll.MDBll;
using Lib.Common.CommonUtils;
using Lib.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{
    public partial class GreenValueAndLowValueForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private GoalValueAndLowValueModel gl = new GoalValueAndLowValueModel();
        private GoalValueAndLowValueBll glBll = new GoalValueAndLowValueBll();
        public GreenValueAndLowValueForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GreenValueAndLowValueForm_Load(object sender, EventArgs e)
        {
            string tip="请录入信息";
            gl = glBll.getGoalsAndVlaue();
            if ( gl!= null)
            {
                this.TB_CGreenValue.Text = gl.CGreenValue;
                this.TB_CLowValue.Text = gl.CLowValue;
                this.TB_PFGreenValue.Text = gl.PGreenValue;
                this.TB_PFLowValue.Text = gl.PLowValue;

            }
            else {
                this.TB_PFGreenValue.Text = tip;
                this.TB_CLowValue.Text = tip;
                this.TB_PFGreenValue.Text = tip;
                this.TB_PFLowValue.Text = tip;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            foreach (Control cur in Controls)
            {
                if (cur is TextBox)
                {
                    if (!this.errorProvider1.GetError(cur).Equals("")) {

                        MessageUtil.ShowError("输入数据不合法");
                    }
                }
            }




            gl.CGreenValue = this.TB_CGreenValue.Text;
            gl.CLowValue = this.TB_CLowValue.Text;
            gl.PGreenValue = this.TB_PFGreenValue.Text;
            gl.PLowValue = this.TB_PFLowValue.Text;
            




            try
            {
                if (MessageUtil.ShowOKCancelAndQuestion("确定修改？") == DialogResult.OK) {
                    if (glBll.newGoalsAndLowValue(gl))
                    {
                        MessageUtil.ShowTips("保存成功！");
                    }

                }
            }
            catch (Exception ex)
            {

                MessageUtil.ShowTips("保存失败！"+ex.Message);
            }
        }

        //检测输入
       void checkedInput(TextBox textBox) {
  
            errorProvider1.SetError(textBox, "");
            if (textBox.Text.Equals(""))
            {
                this.errorProvider1.SetError(textBox, "不能为空！");
                
            }
            else if (Convert.ToSingle(textBox.Text) < 0 || Convert.ToSingle(textBox.Text) > 100)
            {
                this.errorProvider1.SetError(textBox, "输入不合法，请输入0-100");
                
            }
            

        }

        private void TB_CGreenValue_Leave(object sender, EventArgs e)
        {
            checkedInput(TB_CGreenValue);
        }

        private void TB_CLowValue_Leave(object sender, EventArgs e)
        {
            checkedInput(TB_CLowValue);
            if (Convert.ToSingle(TB_CGreenValue.Text) < Convert.ToSingle(TB_CLowValue.Text))
            {

                this.errorProvider1.SetError(TB_CLowValue, "输入不合法，最低值不能大于目标值");
                
            }
            else {
         
            }

        }

        private void TB_PFGreenValue_Leave(object sender, EventArgs e)
        {
            
            checkedInput(TB_PFGreenValue);

        }

        private void TB_PFLowValue_Leave(object sender, EventArgs e)
        {
            checkedInput(TB_PFLowValue);
            if (Convert.ToSingle(TB_PFGreenValue.Text) < Convert.ToSingle(TB_PFLowValue.Text))
            {

                this.errorProvider1.SetError(TB_PFLowValue, "输入不合法，最低值不能大于目标值");
                
            }
          

        }
    }
}
