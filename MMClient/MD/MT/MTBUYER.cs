﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;


namespace MMClient.MD.MT
{
    public partial class MTBUYER : WeifenLuo.WinFormsUI.Docking.DockContent
    {


        public MTBUYER()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        MaterialBLL mt = new MaterialBLL();
        MaterialBuyerBLL mtb = new MaterialBuyerBLL();
        ComboBoxItem cbm = new ComboBoxItem();




        public void cbb_物料编号_SelectedIndexChanged(object sender, EventArgs e)
        {
            Choseftymtb chf = new Choseftymtb(this);
            chf.Show(this);
            List<string> list = gn.GetAllFactory();
            cbm.FuzzyQury(chf.cbb_工厂, list);
            MaterialBase mab = mtb.GetBasicInformation(cbb_物料编号.Text);
            cbb_物料名称.Text = mab.Material_Name;
            cbb_物料组.Text = mab.Material_Group;
            cbb_计量单位.Text = mab.Measurement;
            cbb_订单单位.Text = mab.Order_Unit;
            cbb_可变单位.Text = mab.Variable_Unit;
            cbb_资格.Text = mab.Discount_Qualifications;
            ckb_批次管理.Checked = mab.Batch_Mark;
            ckb_MPN.Checked = mab.MPN_Mark;

            //string sql1 = "SELECT * FROM [Factory]";
            //string sql2 = "SELECT * FROM [Material] WHERE Material_ID='" + cbb_物料编号.Text + "'";
            ////string sql3 = "SELECT * FROM [MTBUYER] WHERE Material_ID='" + cbb_物料编号.Text + "' AND Factory_ID='" + cbb_工厂编码.Text +"'";
            //DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            //DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            ////DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            //int j;
            //for (j = 0; j < Convert.ToInt32(dt1.Rows.Count.ToString()); j++)
            //{
            //    chf.cbb_工厂.Items.Add(dt1.Rows[j][1]);
            //}
            //cbb_物料名称.Text = dt2.Rows[0][2].ToString();
            //cbb_物料组.Text = dt2.Rows[0][17].ToString();
            //cbb_计量单位.Text = dt2.Rows[0][4].ToString();
            //cbb_订单单位.Text = dt2.Rows[0][36].ToString();
            //cbb_可变单位.Text = dt2.Rows[0][37].ToString();
            //cbb_资格.Text = dt2.Rows[0][32].ToString();
            ////cbb_采购组.Text = dt3.Rows[0][5].ToString();
            ////cbb_物料状态.Text = dt3.Rows[0][6].ToString();
            ////cbb_税收状态.Text = dt3.Rows[0][7].ToString();
            ////cbb_运输组.Text = dt3.Rows[0][8].ToString();
            ////cbb_起始期.Text = dt3.Rows[0][9].ToString();



        }

        private void btn_重置_Click(object sender, EventArgs e)
        {


        }

        private void btn_确定_Click(object sender, EventArgs e)
        {

        }

        private void MTBUYER_Load(object sender, EventArgs e)
        {
            List<string> list0 = mt.GetAllMaterialID();
            cbm.FuzzyQury(cbb_物料编号, list0);
            List<string> list1 = gn.GetAllMeasurement();
            cbm.FuzzyQury(cbb_计量单位, list1);
            cbm.FuzzyQury(cbb_订单单位, list1);
            cbm.FuzzyQury(cbb_可变单位, list1);
            List<string> list2 = gn.GetAllGroupName();
            cbm.FuzzyQury(cbb_物料组, list2);
            List<string> list3 = gn.GetAllBuyerGroup();
            cbm.FuzzyQury(cbb_采购组, list3);
            cbb_工厂编码.Enabled = false;
            cbb_工厂名称.Enabled = false;
        }

        private void cbb_采购组_Leave(object sender, EventArgs e)
        {

            List<string> list = gn.GetAllBuyerGroup();
            if (cbb_采购组.Text != "" && gn.IDInTheList(list, cbb_采购组.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的采购组");
                cbb_采购组.Focus();
            }
        }
        //del method
        //private void cbb_物料组_TextChanged(object sender, EventArgs e)
        //{
        //    List<string> list = gn.GetAllGroupName();
        //    if (cbb_物料组.Text != "" && gn.IDInTheList(list, cbb_物料组.Text) == true)
        //    {
        //        MessageUtil.ShowError("请选择正确的物料组");
        //        cbb_物料组.Focus();
        //    }
        //}

        private void btn_维护制造商物料_Click(object sender, EventArgs e)
        {
            cbb_物料编号.Items.Clear();
            List<string> list2 = mt.GetAllMaterialNameMPN();
            string sql = "SELECT Material_Name,Material_Type,Type_Name,Material_Standard FROM [Material] WHERE MPN_NO ='true'";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql);
            cbm.FuzzyQury(cbb_物料编号, list2, dt2);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            cbb_采购组.Text = "";
            cbb_工厂编码.Text = "";
            cbb_工厂名称.Text = "";
            cbb_计量单位.Text = "";
            cbb_价值代码.Text = "";
            cbb_起始期.Text = "";
            cbb_物料编号.Text = "";
            cbb_物料名称.Text = "";
            cbb_物料状态.Text = "";
            cbb_物料组.Text = "";
            cbb_运输组.Text = "";
            cbb_资格.Text = "";
            cbb_可变单位.Text = "";
            cbb_订单单位.Text = "";
            foreach (Control control in this.Controls)
            {
                if (control.GetType().Name == "ComboBox")
                {
                    ((ComboBox)control).Text = string.Empty;
                }
            }

            cbb_采购组.Enabled = true;
            cbb_物料状态.Enabled = true;
            cbb_运输组.Enabled = true;
            cbb_资格.Enabled = true;
            cbb_税收状态.Enabled = true;
            cbb_起始期.Enabled = true;
            cbb_订单单位.Enabled = true;
            cbb_计量单位.Enabled = true;
            cbb_可变单位.Enabled = true;
            cbb_物料组.Enabled = true;
            cbb_采购组.Visible = true;
            cbb_物料状态.Visible = true;
            cbb_工厂编码.Visible = true;
            cbb_工厂名称.Visible = true;
            cbb_运输组.Visible = true;
            cbb_起始期.Visible = true;
            cbb_税收状态.Visible = true;
            lb_采购组.Visible = true;
            lb_税收状态.Visible = true;
            lb_物料状态.Visible = true;
            lb_有效起始期.Visible = true;
            lb_运输组.Visible = true;
            gbx_采购值.Visible = true;
            ckb_MPN.Enabled = true;
            ckb_MPN.Checked = false;
            ckb_批次管理.Enabled = true;
            ckb_批次管理.Checked = false;
            cbb_物料编号.Items.Clear();
            List<string> list0 = mt.GetAllMaterialID();
            cbm.FuzzyQury(cbb_物料编号, list0);
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (cbb_物料编号.Text == ""|| cbb_物料编号.Text.ToString().Trim()=="")
            {
                MessageUtil.ShowTips("请先选择物料");
                return;
            }
            int flag = 0;

            List<string> list2 = mt.GetAllMaterialID();
            //if (cbb_物料编号.Text == "" || gn.IDInTheList(list2, cbb_物料编号.Text) == true)
            //{
            //    MessageUtil.ShowError("请选择正确的物料编号");
            //    cbb_物料编号.Focus();
            //    flag = 1;
            //}
            if (flag == 0 && MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            {
                if (cbb_工厂编码.Text == "")
                {
                    MaterialBase mb = new MaterialBase();
                    mb.Material_ID = cbb_物料编号.Text;
                    mb.Material_Name = cbb_物料名称.Text;
                    mb.Measurement = cbb_计量单位.Text;
                    mb.Order_Unit = cbb_订单单位.Text;
                    mb.Variable_Unit = cbb_可变单位.Text;
                    mb.Material_Group = cbb_物料组.Text;
                    mb.Discount_Qualifications = cbb_资格.Text;
                    mb.Batch_Mark = ckb_批次管理.Checked;
                    mb.MPN_Mark = ckb_MPN.Checked;

                    //最小采购数量
                    mb.minDeliveryNum = this.minDeliveryNum.Text+"%";
                    mtb.UpdateBasicInformation(mb);
                    MessageUtil.ShowTips("修改成功");
                }
                else
                {
                    MaterialFactory mtfty = new MaterialFactory();
                    mtfty.Material_ID = cbb_物料编号.Text;
                    mtfty.Material_Name = cbb_物料名称.Text;
                    mtfty.Factory_ID = cbb_工厂编码.Text;
                    mtfty.Factory_Name = cbb_工厂名称.Text;
                    mtfty.Buyer_Group = cbb_采购组.Text;
                    mtfty.Ptmtl_Status = cbb_物料状态.Text;
                    mtfty.MTtax_Status = cbb_税收状态.Text;
                    mtfty.MTtransport_Group = cbb_运输组.Text;
                    mtfty.Source_List = ckb_源清单.Checked;
                  
                    mtb.UpdateMtFtyInformation(mtfty);
                    //采购值未实现

                    MessageUtil.ShowTips("修改成功");
                  
                }
            }
        }

        private void cbb_物料状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbb_物料状态.Text)
            {
                case "01":
                    //不能下采购订单和库存移动
                    tbx_工厂物料状态.Text = " 因采购/仓库而被冻结 ";
                    break;
                case "02":
                    //禁止与BOM有关的操作，库存可用，可采购
                    tbx_工厂物料状态.Text = " 因任务空间/BOM而被冻结 ";
                    break;
                case "DC":
                    //禁止一切业务操作
                    tbx_工厂物料状态.Text = " 失效状态 ";
                    break;
                //新建物料，只可用于BOM的建立，不可进行其他业务操作
                case "EC":
                    tbx_工厂物料状态.Text = " 初始状态 ";
                    break;
                default:
                    //正常状态
                    tbx_工厂物料状态.Text = " 正常状态 ";
                    break;
            }




        }

        private void ckb_源清单_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
