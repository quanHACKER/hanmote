﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.Bll;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;
using MMClient.MD.SP;

namespace MMClient.MD.MT
{
    public partial class SPCPY1 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SPCPY1()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        SupplierBaseBLL sbb = new SupplierBaseBLL();
        SupplierCompanyBLL scb = new SupplierCompanyBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        private void SPCPY1_Load(object sender, EventArgs e)
        {
            //填充公司代码下拉列表
            List<string> companyList = gn.GetAllCompany();
            cbm.FuzzyQury(cbb_公司代码, companyList);
        }

        private void cbb_公司代码_SelectedIndexChanged(object sender, EventArgs e)
        {
            SupplierCompanyCode scompany = new SupplierCompanyCode();
           scompany = scb.GetSupplierCompanyInformation(cbb_供应商编码.Text, cbb_公司代码.Text);
           //cbb_公司名称.Text = scompany.Company_Name;
           string sql = "SELECT * FROM [Company] WHERE Company_ID='" + cbb_公司代码.Text + "'";
         DataTable dt =  DBHelper.ExecuteQueryDT(sql);
         cbb_公司名称.Text = dt.Rows[0]["Company_Name"].ToString();

           cbb_统驭科目.Text = scompany.Controlling_Accounts;
           cbb_总部.Text = scompany.Head_Office;
           cbb_权限组.Text = scompany.Permissions_Set;
           cbb_少数标志.Text = scompany.Few_Marks;
           cbb_排序码.Text = scompany.Sort_Code;
           cbb_现金管理组.Text = scompany.CashManagement_Group;
           cbb_批准组.Text = scompany.Approval_Group;
           if (scompany.Certificate_Date == Convert.ToDateTime("0001/1/1 0:00:00"))
               dtp_证书日期.Value = new DateTime(1900, 1, 1);
           else
           dtp_证书日期.Value = scompany.Certificate_Date;
           cbb_利息计算标志.Text = scompany.InterestCalculated_Logo;
           cbb_计息周期.Text = scompany.Interest_Cycle;
           if (scompany.LastCalculation_Date == Convert.ToDateTime("0001/1/1 0:00:00"))
               dtp_上次计算日期.Value = new DateTime(1900, 1, 1);
           else
           dtp_上次计算日期.Value = scompany.LastCalculation_Date;
           if (scompany.Last_Runtime == Convert.ToDateTime("0001/1/1 0:00:00"))
               dtp_上次利息运行.Value = new DateTime(1900, 1, 1);
           else
           dtp_上次利息运行.Value = scompany.Last_Runtime;
           cbb_预扣税款代码.Text = scompany.Withholding_TaxCode;
           cbb_折扣所得税国家.Text = scompany.Income_Country;
           cbb_收受人类型.Text = scompany.Receiptor_Type;
           cbb_免税号.Text = scompany.Duty_Free;
           if (scompany.Effective_EndDate == Convert.ToDateTime("0001/1/1 0:00:00"))
               dtp_有效结束日.Value = new DateTime(1900, 1, 1);
           else
           dtp_有效结束日.Value = scompany.Effective_EndDate;
           cbb_免税的权限.Text = scompany.DutyFree_Access;
           cbb_先前的账户号码.Text = scompany.Previous_Account;
           cbb_人事编号.Text = scompany.Personnel_Number;


          
            //try
            //{
            //    string sql3 = "SELECT * FROM [Supplier_CompanyCode] where Supplier_ID='" + cbb_供应商编码.Text + "' and Company_ID='" + cbb_公司代码.Text + "'";
            //    DataTable dt = DBHelper.ExecuteQueryDT(sql3);
            //    cbb_统驭科目.Text = dt.Rows[0][5].ToString();
            //    cbb_供应商名称.Text = dt.Rows[0][2].ToString();
            //    cbb_公司名称.Text = dt.Rows[0][4].ToString();
            //    cbb_总部.Text = dt.Rows[0][6].ToString();
            //    cbb_权限组.Text = dt.Rows[0][7].ToString();
            //    cbb_少数标志.Text = dt.Rows[0][8].ToString();
            //    cbb_排序码.Text = dt.Rows[0][9].ToString();
            //    cbb_现金管理组.Text = dt.Rows[0][10].ToString();
            //    cbb_批准组.Text = dt.Rows[0][11].ToString();
            //    //cbb_证书日期.Text = dt.Rows[0][12].ToString();
            //    cbb_利息计算标志.Text = dt.Rows[0][13].ToString();
            //    cbb_计息周期.Text = dt.Rows[0][14].ToString();
            //    //cbb_上次计算日期.Text = dt.Rows[0][15].ToString();
            //    //cbb_上次利息运行.Text = dt.Rows[0][16].ToString();
            //    cbb_预扣税款代码.Text = dt.Rows[0][17].ToString();
            //    cbb_折扣所得税国家.Text = dt.Rows[0][18].ToString();
            //    cbb_收受人类型.Text = dt.Rows[0][19].ToString();
            //    cbb_免税号.Text = dt.Rows[0][20].ToString();
            //    cbb_有效结束日.Text = dt.Rows[0][21].ToString();
            //    cbb_免税的权限.Text = dt.Rows[0][22].ToString();
            //    cbb_先前的账户号码.Text = dt.Rows[0][23].ToString();
            //    cbb_人事编号.Text = dt.Rows[0][24].ToString();
            //}
            //catch (Exception ex)
            //{
            //    string sql4 = " SELECT * FROM [Supplier_Base] WHERE Supplier_ID='" + cbb_供应商编码.Text + "' ";
            //    DataTable dt4 = DBHelper.ExecuteQueryDT(sql4);
            //    string sql5 = " SELECT * FROM [Company] WHERE Company_ID='" + cbb_公司代码.Text + "' ";
            //    DataTable dt5 = DBHelper.ExecuteQueryDT(sql5);


            //    cbb_供应商名称.Text = dt4.Rows[0][3].ToString();
            //    cbb_公司名称.Text = dt5.Rows[0][1].ToString();

            //}

        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            
           
           
            //try
            //{
            //    string sql4 = " INSERT INTO [Supplier_CompanyCode](Supplier_ID,Supplier_Name,Company_ID,Company_Name,Controlling_Accounts,Head_Office,Permissions_Set,Few_Marks,Sort_Code,CashManagement_Group,Approval_Group,Certificate_Date,InterestCalculated_Logo,Interest_Cycle,LastCalculation_Date,Last_Runtime,Withholding_TaxCode,Income_Country,Receiptor_Type,Duty_Free,Effective_EndDate,DutyFree_Access,Previous_Account,Personnel_Number) Values ('" + cbb_供应商编码.Text + "','" + cbb_供应商名称.Text + "','" + cbb_公司代码.Text + "','" + cbb_公司名称.Text + "','" + cbb_统驭科目.Text + "','" + cbb_总部.Text + "','" + cbb_权限组.Text + "','" + cbb_少数标志.Text + "','" + cbb_排序码.Text + "','" + cbb_现金管理组.Text + "','" + cbb_批准组.Text + "','" + dtp_证书日期.Text + "','" + cbb_利息计算标志.Text + "','" + cbb_计息周期.Text + "','" + dtp_上次计算日期.Text + "','" + dtp_上次利息运行.Text + "','" + cbb_预扣税款代码.Text + "','" + cbb_折扣所得税国家.Text + "','" + cbb_收受人类型.Text + "','" + cbb_免税号.Text + "','" + cbb_有效结束日.Text + "','" + cbb_免税的权限.Text + "','" + cbb_先前的账户号码.Text + "','" + cbb_人事编号.Text + "')";
            //    DBHelper.ExecuteNonQuery(sql4);
            //    MessageBox.Show("创建成功");
            //}

            //catch (Exception ex)
            //{
            //    string sql5 = " UPDATE [Supplier_CompanyCode] SET Controlling_Accounts='" + cbb_统驭科目.Text + "',Head_Office='" + cbb_总部.Text + "',Permissions_Set='" + cbb_权限组.Text + "',Few_Marks='" + cbb_少数标志.Text + "',Sort_Code='" + cbb_排序码.Text + "',CashManagement_Group='" + cbb_现金管理组.Text + "',Approval_Group='" + cbb_批准组.Text + "',Certificate_Date='" + dtp_证书日期.Text + "',InterestCalculated_Logo='" + cbb_利息计算标志.Text + "',Interest_Cycle='" + cbb_计息周期.Text + "',LastCalculation_Date='" + dtp_上次计算日期.Text + "',Last_Runtime='" + dtp_上次利息运行.Text + "',Withholding_TaxCode='" + cbb_预扣税款代码.Text + "',Income_Country='" + cbb_折扣所得税国家.Text + "',Receiptor_Type='" + cbb_收受人类型.Text + "',Duty_Free='" + cbb_免税号.Text + "',Effective_EndDate='" + cbb_有效结束日.Text + "',DutyFree_Access='" + cbb_免税的权限.Text + "',Previous_Account='" + cbb_先前的账户号码.Text + "',Personnel_Number='" + cbb_人事编号.Text + "' WHERE Supplier_ID='" + cbb_供应商编码.Text + "' AND Company_ID='" + cbb_公司代码.Text + "'";
            //    DBHelper.ExecuteNonQuery(sql5);
            //    MessageBox.Show("维护成功");
            //}
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {
            
        }

        private void cbb_供应商编码_SelectedIndexChanged(object sender, EventArgs e)
        {
            SupplierBase sbase = sbb.GetSupplierBasicInformation(cbb_供应商编码.Text);
            cbb_供应商名称.Text = sbase.Supplier_Name;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SupplierChose spc = new SupplierChose(this);
            spc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
             SupplierCompanyCode scompany = new SupplierCompanyCode();
            scompany.Supplier_ID = cbb_供应商编码.Text;
            scompany.Supplier_Name = cbb_供应商名称.Text;
            scompany.Company_ID = cbb_公司代码.Text;
            scompany.Company_Name = cbb_公司名称.Text;
            scompany.Controlling_Accounts = cbb_统驭科目.Text;
            scompany.Head_Office = cbb_总部.Text;
            scompany.Permissions_Set = cbb_权限组.Text;
            scompany.Few_Marks = cbb_少数标志.Text;
            scompany.Sort_Code = cbb_排序码.Text;
            scompany.CashManagement_Group = cbb_现金管理组.Text;
            scompany.Approval_Group = cbb_批准组.Text;
            scompany.Certificate_Date = dtp_证书日期.Value;
            scompany.InterestCalculated_Logo = cbb_利息计算标志.Text;
            scompany.Interest_Cycle = cbb_计息周期.Text;
            scompany.LastCalculation_Date = dtp_上次计算日期.Value;
            scompany.Last_Runtime = dtp_上次利息运行.Value;
            scompany.Withholding_TaxCode = cbb_预扣税款代码.Text;
            scompany.Income_Country = cbb_折扣所得税国家.Text;
            scompany.Receiptor_Type = cbb_收受人类型.Text;
            scompany.Duty_Free = cbb_免税号.Text;
            scompany.Effective_EndDate = dtp_有效结束日.Value;
            scompany.DutyFree_Access = cbb_免税的权限.Text;
            scompany.Previous_Account = cbb_先前的账户号码.Text;
            scompany.Personnel_Number = cbb_人事编号.Text;
            List<string> l1 = sbb.GetAllSupplierID();
            List<string> l2 = gn.GetAllCompany();
            if (cbb_供应商编码.Text == "" || gn.IDInTheList(l1, cbb_供应商编码.Text) == true)
            {
                MessageUtil.ShowError("请输入或选择正确的供应商编码");
                cbb_供应商编码.Focus();
            }
            else
            {
                if (cbb_公司代码.Text == "" || gn.IDInTheList(l2, cbb_公司代码.Text) == true)
                {
                    MessageUtil.ShowError("请输入或选择正确的公司代码");
                    cbb_公司代码.Focus();
                }
                else
                {
                    scb.UpdateSupplierCompanyInformation(scompany);
                    MessageUtil.ShowTips("维护成功");
                }
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            cbb_公司代码.Text = "";
            cbb_公司名称.Text = "";
            cbb_供应商编码.Text = "";
            cbb_供应商名称.Text = "";
            cbb_计息周期.Text = "";
            cbb_利息计算标志.Text = "";
            cbb_免税的权限.Text = "";
            cbb_免税号.Text = "";
            cbb_排序码.Text = "";
            cbb_批准组.Text = "";
            cbb_权限组.Text = "";
            cbb_人事编号.Text = "";
            dtp_上次计算日期.Text = "";
            dtp_上次利息运行.Text = "";
            cbb_少数标志.Text = "";
            cbb_收受人类型.Text = "";
            cbb_统驭科目.Text = "";
            cbb_先前的账户号码.Text = "";
            cbb_现金管理组.Text = "";
            dtp_有效结束日.Text = "";
            cbb_预扣税款代码.Text = "";
            cbb_折扣所得税国家.Text = "";
            dtp_证书日期.Text = "";
            cbb_总部.Text = "";
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
