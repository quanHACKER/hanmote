﻿using Lib.Bll.MDBll.CertiFileBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{
    public partial class CertiTypeForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        CertiFileBll certiFileBll = new CertiFileBll();

        public CertiTypeForm()
        {
            InitializeComponent();
        }

        private void CertiTypeForm_Load(object sender, EventArgs e)
        {
            //读取证书信息
            dgv_CertiFileInfo.DataSource = certiFileBll.getCertiFileTypeInfo();

        }

        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCertiTypeForm addCertiTypeForm = new AddCertiTypeForm();
            if (addCertiTypeForm.ShowDialog() == DialogResult.Cancel) {
                dgv_CertiFileInfo.DataSource = certiFileBll.getCertiFileTypeInfo();

            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv_CertiFileInfo.Columns["choose"].Visible != true)
            {
                MessageUtil.ShowTips("请选择要删除数据！");
                dgv_CertiFileInfo.Columns["choose"].Visible = true;
            }
            else {

                int flag = 0;
                foreach (DataGridViewRow r in dgv_CertiFileInfo.Rows)
                {
                    if ((bool)r.Cells["choose"].EditedFormattedValue == true)
                    {
                        flag = certiFileBll.delCertiFileTypeInfo(r.Cells["CertiFileId"].Value.ToString());
                        if (flag > 0) {

                            dgv_CertiFileInfo.Rows.Remove(r);
                            MessageUtil.ShowTips("删除成功！");
                        }
                        
                    }


                }
                    
                dgv_CertiFileInfo.Columns["choose"].Visible = false;
            }
            
        }
    }
}
