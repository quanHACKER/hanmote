﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General.Material_Type;

namespace MMClient.MD.MT.MaterialType
{
    public partial class NewTypeC : Form
    {
        public NewTypeC()
        {
            InitializeComponent();
        }
       

      

        private void NewTypeC_Load_1(object sender, EventArgs e)
        {
            MTTypeCBLL mtb = new MTTypeCBLL();
            List<string> list = mtb.GetAllBigClassfyName();
            cbb_大分类.DataSource = list;
        }

        private void btn_确定_Click_1(object sender, EventArgs e)
        {
            MTTypeCBLL mtb = new MTTypeCBLL();
            mtb.NewLittleClassfy(tbx_编号.Text, tbx_小分类.Text, cbb_大分类.Text);
            MessageBox.Show("新建物料类型成功");
            this.Close();
        }
    }
}
