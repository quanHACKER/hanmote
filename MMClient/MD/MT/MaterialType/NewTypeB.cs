﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General.Material_Type;

namespace MMClient.MD.MT.MaterialType
{
    public partial class NewTypeB : Form
    {
        public NewTypeB()
        {
            InitializeComponent();
        }

        private void NewTypeB_Load(object sender, EventArgs e)
        {
            MTTypeBBLL mtb = new MTTypeBBLL();
            List<string> list = mtb.GetAllBigClassfyName();
            cbb_大分类.DataSource = list;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            MTTypeBBLL mtb = new MTTypeBBLL();
            mtb.NewLittleClassfy(tbx_编号.Text, tbx_小分类.Text, cbb_大分类.Text);
            MessageBox.Show("新建小分类成功");
            this.Close();
        }
    }
}
