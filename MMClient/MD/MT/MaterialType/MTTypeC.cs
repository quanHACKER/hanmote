﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General.Material_Type;

namespace MMClient.MD.MT.MaterialType
{
    public partial class MTTypeC : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTTypeC()
        {
            InitializeComponent();
        }
       

        private void MTTypeC_Load_1(object sender, EventArgs e)
        {
            MTTypeCBLL mtb = new MTTypeCBLL();
            dgv_物料类型.DataSource = mtb.GetAllLittleClassfy();
        }

        private void btn_刷新数据_Click_1(object sender, EventArgs e)
        {
            MTTypeCBLL mtb = new MTTypeCBLL();
            dgv_物料类型.DataSource = mtb.GetAllLittleClassfy();
        }

        private void btn_新条目_Click_1(object sender, EventArgs e)
        {
            NewTypeC ntb = new NewTypeC();
            ntb.Show();
        }
    }
}
