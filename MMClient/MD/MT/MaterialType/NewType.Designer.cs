﻿namespace MMClient.MD.MT
{
    partial class NewType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_类型编号 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_英文缩写 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_类型名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 51);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料类型名称";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btn_重置);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.cbb_类型编号);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_英文缩写);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_类型名称);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1018, 379);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建物料类型";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "（输入三位数字）";
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(479, 287);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(75, 42);
            this.btn_重置.TabIndex = 7;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(354, 287);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 42);
            this.btn_确定.TabIndex = 6;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_类型编号
            // 
            this.cbb_类型编号.FormattingEnabled = true;
            this.cbb_类型编号.Location = new System.Drawing.Point(190, 186);
            this.cbb_类型编号.Name = "cbb_类型编号";
            this.cbb_类型编号.Size = new System.Drawing.Size(121, 24);
            this.cbb_类型编号.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "类型编号";
            // 
            // cbb_英文缩写
            // 
            this.cbb_英文缩写.FormattingEnabled = true;
            this.cbb_英文缩写.Location = new System.Drawing.Point(190, 114);
            this.cbb_英文缩写.Name = "cbb_英文缩写";
            this.cbb_英文缩写.Size = new System.Drawing.Size(121, 24);
            this.cbb_英文缩写.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "英文缩写";
            // 
            // cbb_类型名称
            // 
            this.cbb_类型名称.FormattingEnabled = true;
            this.cbb_类型名称.Location = new System.Drawing.Point(190, 48);
            this.cbb_类型名称.Name = "cbb_类型名称";
            this.cbb_类型名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_类型名称.TabIndex = 1;
            // 
            // NewType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 389);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewType";
            this.Text = "NewType";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_类型编号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_英文缩写;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_类型名称;
        private System.Windows.Forms.Label label4;
    }
}