﻿namespace MMClient.MD.MT
{
    partial class MTType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_物料类型 = new System.Windows.Forms.DataGridView();
            this.类型编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料类型)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_物料类型);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 450);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "修改视图  物料大分类";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 416);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "按Tab键分页";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(115, 367);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(660, 37);
            this.pageTool.TabIndex = 5;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // dgv_物料类型
            // 
            this.dgv_物料类型.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_物料类型.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_物料类型.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_物料类型.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_物料类型.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.类型编码,
            this.类型描述});
            this.dgv_物料类型.Location = new System.Drawing.Point(115, 31);
            this.dgv_物料类型.Name = "dgv_物料类型";
            this.dgv_物料类型.RowTemplate.Height = 23;
            this.dgv_物料类型.Size = new System.Drawing.Size(654, 329);
            this.dgv_物料类型.TabIndex = 4;
            this.dgv_物料类型.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgv_物料类型_MouseDoubleClick);
            // 
            // 类型编码
            // 
            this.类型编码.DataPropertyName = "ENG";
            this.类型编码.HeaderText = "类型编码";
            this.类型编码.Name = "类型编码";
            this.类型编码.Width = 250;
            // 
            // 类型描述
            // 
            this.类型描述.DataPropertyName = "Bigclassfy_Name";
            this.类型描述.HeaderText = "类型描述";
            this.类型描述.Name = "类型描述";
            this.类型描述.Width = 200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 221);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "删除";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(6, 122);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(75, 28);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // MTType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 449);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTType";
            this.Text = "物料大分类";
            this.Load += new System.EventHandler(this.MTType_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料类型)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button button1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView dgv_物料类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型描述;
        private System.Windows.Forms.Label label2;
    }
}