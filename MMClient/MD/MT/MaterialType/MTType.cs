﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.MT
{
    public partial class MTType : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTType()
        {
            InitializeComponent();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        
        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewType ntp = new NewType();
            ntp.Show();
            LoadData();
        }

        private void MTType_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Bigclassfy";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }


        }

        private void LoadData()
        {
            try
            {
                dgv_物料类型.DataSource = FindBigclassfyInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindBigclassfyInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql  = @"SELECT top " + pageSize + " ENG , Bigclassfy_Name  FROM Bigclassfy   where ClassfyID not in(select top " + pageSize * (pageIndex - 1) + " ClassfyID from Bigclassfy ORDER BY ClassfyID ASC)ORDER BY ClassfyID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("分页查询数据出现异常！", ex);
            };
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.dgv_物料类型.Rows[currentIndex];
            String ENG = Convert.ToString(row.Cells["类型编码"].Value);
            string sql = "DELETE FROM Bigclassfy WHERE ENG ='"+ENG+"' ";
            try
            {
                DBHelper.ExecuteNonQuery(sql);
                LoadData();
            }catch(BllException ex)
            {
                MessageUtil.ShowTips("数据库异，请稍后重试");
            }
            pageNext1_Load(sender, e);
        }
        /// <summary>
        /// 当前行
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.dgv_物料类型.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.dgv_物料类型.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.dgv_物料类型.CurrentRow.Index;
        }

        private void dgv_物料类型_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            typeinfo tpin = new typeinfo();
            int i;
            //string[] str = new string[dgv_物料类型.Rows.Count];
            for (i = 0; i < dgv_物料类型.Rows.Count; i++)
            {
                if (dgv_物料类型.Rows[i].Selected == true)
                { 
                    tpin.cbb_物料类型名称.Text = dgv_物料类型.Rows[i].Cells[1].Value.ToString();
                    tpin.cbb_英文缩写.Text = dgv_物料类型.Rows[i].Cells[0].Value.ToString();
                }
            }
            tpin.Owner = this;
            tpin.Show();
            
        }
    }
}
