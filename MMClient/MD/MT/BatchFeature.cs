﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MDBll.Batch;
using Lib.Bll;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;
using Lib.Model.MD.MT;
/**
 *                             _ooOoo_
 *                            o8888888o
 *                            88" . "88
 *                            (| -_- |)
 *                            O\  =  /O
 *                         ____/`---'\____
 *                       .'  \\|     |//  `.
 *                      /  \\|||  :  |||//  \
 *                     /  _||||| -:- |||||-  \
 *                     |   | \\\  -  /// |   |
 *                     | \_|  ''\---/''  |   |
 *                     \  .-\__  `-`  ___/-. /
 *                   ___`. .'  /--.--\  `. . __
 *                ."" '<  `.___\_<|>_/___.'  >'"".
 *               | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *               \  \ `-.   \_ __\ /__ _/   .-` /  /
 *          ======`-.____`-.___\_____/___.-`____.-'======
 *                             `=---='
 *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *                     佛祖保佑        永无BUG
 */                     
namespace MMClient.MD.MT
{
    public partial class BatchFeature : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        

        public BatchFeature()
        {
            InitializeComponent();
            dgv_项目.Rows.Add(10);
        }
        ComboBoxItem cbm = new ComboBoxItem();
        private void BatchFeature_Load(object sender, EventArgs e)
        {
            BatchFeatureBLL BFB = new BatchFeatureBLL();
            List<string> list = BFB.GetAllFixedFeatures();
            cln_特性名称.DataSource = list;
            
        }
  
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
             BatchFeatureBLL BFB = new BatchFeatureBLL();
             if (dgv_项目.CurrentCell != null && dgv_项目.CurrentCell.ColumnIndex == 0)
             {
                 DataGridViewComboBoxCell d = dgv_项目.Rows[dgv_项目.CurrentCell.RowIndex].Cells[1] as DataGridViewComboBoxCell;
                 d.DataSource = BFB.GetAllValue(dgv_项目.CurrentCell.Value.ToString());
             }
        }

        private void btn_录入_Click(object sender, EventArgs e)
        {
           
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.dgv_项目.Rows.Add();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dgv_项目.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    dgv_项目.Rows.Remove(r);
                }
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            BatchFeatureBLL BFB = new BatchFeatureBLL();
           
            List<Batch_Feature> listf = new List<Batch_Feature>();
            string ID = tbx_批次编码.Text;
            if (tbx_批次编码.Text == "")
                MessageUtil.ShowError("请输入正确的批次编码");
            else
            {
                //try
                //{
                    if (dgv_项目.Rows.Count == 0)
                        MessageUtil.ShowError("无任何特性");
                    else
                    {
                        int count = dgv_项目.Rows.Count;
                        int flag = 1;
                        bool b = true;
                        string batchid = tbx_批次编码.Text;
                        string featurename;
                        //for (int k = 0; k < count; k++)
                        //{
                        //    featurename = "";
                        //    featurename = dgv_项目.Rows[k].Cells[0].Value.ToString();
                        //    b = BFB.RepeatedBatchFeatureInformation(batchid, featurename);
                        //    if (b == false)
                        //    {
                        //        MessageUtil.ShowError("录入了重复的特性，请检查后重新录入");
                        //        flag = 0;
                        //        break;
                        //    }
                        //}
                        //int j = 0; int l = 0;
                        //for (j = 0; j < count; j++)
                        //    for (l = count - 1; l > j; l--)
                        //    {
                        //        if (dgv_项目.Rows[j].Cells[0].Value.ToString() == dgv_项目.Rows[l].Cells[0].Value.ToString())
                        //        {
                        //            MessageUtil.ShowError("录入了重复的特性，请检查后重新录入");
                        //            flag = 0;
                        //            break;
                        //        }
                        //    }
                        if (flag == 1)
                        {

                            for (int i = 0; i < dgv_项目.Rows.Count; i++)
                            {
                                if ( dgv_项目.Rows[i].Cells[0].Value!= null&& dgv_项目.Rows[i].Cells[1].Value != null)
                                {
                                    Batch_Feature btfe = new Batch_Feature();
                                    btfe.Feature_Name = dgv_项目.Rows[i].Cells[0].Value.ToString();
                                    btfe.Feature_Value = dgv_项目.Rows[i].Cells[1].Value.ToString();
                                    listf.Add(btfe);
                                }

                            }
                            for (int k = 0; k < listf.Count; k++)
                            {

                                BFB.InsertBatchFeatures(ID, listf[k].Feature_Name, listf[k].Feature_Value);
                            }
                            MessageUtil.ShowTips("录入批次特性成功");
                        }

                    }
                //}
                //catch (Exception ex)
                //{
                //    MessageUtil.ShowError("特性行中有空行，请删除空行");
                //}

            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgv_项目_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            


        }
    }
}
