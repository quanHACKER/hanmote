﻿namespace MMClient.MD.MT
{
    partial class MTEstablish1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MTEstablish1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_物料名称 = new System.Windows.Forms.TextBox();
            this.cbMtGroupName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_行业领域 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_物资型号 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_分类 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_物料类型 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_物料名称);
            this.groupBox1.Controls.Add(this.cbMtGroupName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbb_行业领域);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbb_物资型号);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_分类);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_物料类型);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 70);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1275, 533);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "创建新数据";
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Location = new System.Drawing.Point(278, 39);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(121, 19);
            this.cbb_物料名称.TabIndex = 18;
            // 
            // cbMtGroupName
            // 
            this.cbMtGroupName.FormattingEnabled = true;
            this.cbMtGroupName.Location = new System.Drawing.Point(278, 129);
            this.cbMtGroupName.Name = "cbMtGroupName";
            this.cbMtGroupName.Size = new System.Drawing.Size(121, 21);
            this.cbMtGroupName.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(106, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "选择新建物料所属物料组";
            // 
            // cbb_行业领域
            // 
            this.cbb_行业领域.FormattingEnabled = true;
            this.cbb_行业领域.Location = new System.Drawing.Point(278, 295);
            this.cbb_行业领域.Name = "cbb_行业领域";
            this.cbb_行业领域.Size = new System.Drawing.Size(121, 21);
            this.cbb_行业领域.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(94, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "选择新建物料所属行业领域";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(166, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "输入物料名称";
            // 
            // cbb_物资型号
            // 
            this.cbb_物资型号.FormattingEnabled = true;
            this.cbb_物资型号.Location = new System.Drawing.Point(278, 242);
            this.cbb_物资型号.Name = "cbb_物资型号";
            this.cbb_物资型号.Size = new System.Drawing.Size(121, 21);
            this.cbb_物资型号.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "选择新建物料所属物资型号";
            // 
            // cbb_分类
            // 
            this.cbb_分类.FormattingEnabled = true;
            this.cbb_分类.Location = new System.Drawing.Point(278, 182);
            this.cbb_分类.Name = "cbb_分类";
            this.cbb_分类.Size = new System.Drawing.Size(121, 21);
            this.cbb_分类.TabIndex = 3;
            this.cbb_分类.SelectedIndexChanged += new System.EventHandler(this.cbb_分类_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "选择新建物料所属分类";
            // 
            // cbb_物料类型
            // 
            this.cbb_物料类型.FormattingEnabled = true;
            this.cbb_物料类型.Location = new System.Drawing.Point(278, 82);
            this.cbb_物料类型.Name = "cbb_物料类型";
            this.cbb_物料类型.Size = new System.Drawing.Size(121, 21);
            this.cbb_物料类型.TabIndex = 1;
            this.cbb_物料类型.SelectedIndexChanged += new System.EventHandler(this.cbb_物料类型_SelectedIndexChanged);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 85);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择新建物料所属类型";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1282, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 20;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // certainButton
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("certainButton.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "certainButton";
            this.toolStripButton4.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // MTEstablish1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTEstablish1";
            this.Text = "创建新数据";
            this.Load += new System.EventHandler(this.MTEstablish1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_物资型号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_分类;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_物料类型;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_行业领域;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.TextBox cbb_物料名称;
        private System.Windows.Forms.ComboBox cbMtGroupName;
        private System.Windows.Forms.Label label6;
    }
}