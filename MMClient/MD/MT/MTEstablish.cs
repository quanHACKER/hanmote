﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.SqlClient;
using Lib.SqlServerDAL;

namespace MMClient.MD
{
    public partial class MTEstablish : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTEstablish()
        {
            InitializeComponent();
        }

        private void MTEstablish_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {

            cbb_材料标准.Text = "";
            cbb_材料规格.Text = "";
            cbb_采购组.Text = "";
            cbb_产品特殊要求.Text = "";
            cbb_存货仓库.Text = "";
            cbb_工业标识.Text = "";
            cbb_供应商电话.Text = "";
            cbb_计量单位.Text = "";
            cbb_批次管理.Text = "";
            cbb_数据是否修改.Text = "";
            cbb_物料编码.Text = "";
            cbb_物料类型.Text = "";
            cbb_物料名称.Text = "";
            cbb_物料组.Text = "";
            cbb_用途.Text = "";

        }

        private void btn_创建_Click(object sender, EventArgs e)
        {
            string mtgp = cbb_物料组.Text;
            string measurement = cbb_计量单位.Text;
            string batch = cbb_批次管理.Text;
            string bygp = cbb_采购组.Text;
            string mtid = cbb_物料编码.Text;
            string mttype = cbb_物料类型.Text;
            string itlogo = cbb_工业标识.Text;
            string mtname = cbb_物料名称.Text;
            string stge = cbb_存货仓库.Text;
            string use = cbb_用途.Text;
            string change = cbb_数据是否修改.Text;
            string phone = cbb_供应商电话.Text;
            string spec = cbb_产品特殊要求.Text;
            string mtsc = cbb_材料规格.Text;
            string mtsd = cbb_材料标准.Text;
            string sql = "INSERT INTO [MM].[dbo].[Material](Material_Group,Measurement,Batch_ID,Buyer_Group,Material_ID,Material_Type,In_Identification,Material_Name,Stock_ID,Purpose,Change,Telephone,Special_Requirements,Format,Material_Standard)VALUES('" + mtgp + "','" + measurement + "','" + batch + "','" + bygp + "','" + mtid + "','" + mttype + "','" + itlogo + "', '" + mtname + "','" + stge + "','" + use + "','" + change + "','" + phone + "','" + spec + "','" +　mtsc + "','" + mtsd +"' )";
            DBHelper.ExecuteNonQuery(sql);
            MessageBox.Show("创建成功");
            }

    }
}
