﻿namespace MMClient.MD.MT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbb_test = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cbb_test
            // 
            this.cbb_test.FormattingEnabled = true;
            this.cbb_test.Location = new System.Drawing.Point(97, 75);
            this.cbb_test.Name = "cbb_test";
            this.cbb_test.Size = new System.Drawing.Size(244, 24);
            this.cbb_test.TabIndex = 0;
            this.cbb_test.TextChanged += new System.EventHandler(this.cbb_test_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 358);
            this.Controls.Add(this.cbb_test);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbb_test;
    }
}