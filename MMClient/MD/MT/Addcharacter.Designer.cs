﻿namespace MMClient.MD.MT
{
    partial class Addcharacter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_bao = new System.Windows.Forms.Button();
            this.dgv_cha = new System.Windows.Forms.DataGridView();
            this.btn_shua = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_shan = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.passWord = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.staticPhone = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cha)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 38);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户ID:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "用户姓名";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(318, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "分工";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "质量",
            "成本",
            "交付",
            "技术",
            "财务"});
            this.comboBox1.Location = new System.Drawing.Point(94, 74);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "邮箱";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(318, 72);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 21);
            this.textBox3.TabIndex = 7;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(491, 74);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 21);
            this.textBox4.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(444, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "手机";
            // 
            // btn_bao
            // 
            this.btn_bao.Location = new System.Drawing.Point(516, 144);
            this.btn_bao.Name = "btn_bao";
            this.btn_bao.Size = new System.Drawing.Size(75, 23);
            this.btn_bao.TabIndex = 10;
            this.btn_bao.Text = "保存信息";
            this.btn_bao.UseVisualStyleBackColor = true;
            this.btn_bao.Click += new System.EventHandler(this.btn_bao_Click);
            // 
            // dgv_cha
            // 
            this.dgv_cha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cha.Location = new System.Drawing.Point(59, 173);
            this.dgv_cha.Name = "dgv_cha";
            this.dgv_cha.RowTemplate.Height = 23;
            this.dgv_cha.Size = new System.Drawing.Size(604, 236);
            this.dgv_cha.TabIndex = 11;
            this.dgv_cha.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_cha_CellContentClick);
            // 
            // btn_shua
            // 
            this.btn_shua.Location = new System.Drawing.Point(43, 146);
            this.btn_shua.Name = "btn_shua";
            this.btn_shua.Size = new System.Drawing.Size(75, 23);
            this.btn_shua.TabIndex = 12;
            this.btn_shua.Text = "刷新";
            this.btn_shua.UseVisualStyleBackColor = true;
            this.btn_shua.Click += new System.EventHandler(this.btn_shua_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(192, 146);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "修改";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_shan
            // 
            this.btn_shan.Location = new System.Drawing.Point(343, 144);
            this.btn_shan.Name = "btn_shan";
            this.btn_shan.Size = new System.Drawing.Size(75, 23);
            this.btn_shan.TabIndex = 14;
            this.btn_shan.Text = "删除";
            this.btn_shan.UseVisualStyleBackColor = true;
            this.btn_shan.Click += new System.EventHandler(this.btn_shan_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(444, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "密码";
            // 
            // passWord
            // 
            this.passWord.Location = new System.Drawing.Point(491, 38);
            this.passWord.Name = "passWord";
            this.passWord.Size = new System.Drawing.Size(100, 21);
            this.passWord.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "固定电话";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // staticPhone
            // 
            this.staticPhone.Location = new System.Drawing.Point(95, 109);
            this.staticPhone.Name = "staticPhone";
            this.staticPhone.Size = new System.Drawing.Size(100, 21);
            this.staticPhone.TabIndex = 18;
            // 
            // Addcharacter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 423);
            this.Controls.Add(this.staticPhone);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.passWord);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_shan);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_shua);
            this.Controls.Add(this.dgv_cha);
            this.Controls.Add(this.btn_bao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "Addcharacter";
            this.Text = "Addcharacter";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cha)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_bao;
        private System.Windows.Forms.DataGridView dgv_cha;
        private System.Windows.Forms.Button btn_shua;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_shan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox passWord;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox staticPhone;
    }
}