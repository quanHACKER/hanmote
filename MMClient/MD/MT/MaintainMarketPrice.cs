﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using MMClient.MD.GN;
using Lib.Bll.MDBll.General;
using Lib.Bll;

namespace MMClient.MD.MT
{
    public partial class MaintainMarketPrice : Form
    {
        string materialID;
        public MaintainMarketPrice()
        {
            InitializeComponent();
        }
        public MaintainMarketPrice(string mtid)
        {
            materialID = mtid;
            InitializeComponent();

        }
        GeneralBLL gn = new GeneralBLL();
        FormHelper formh = new FormHelper();
        AccountBLL act = new AccountBLL();
        
        private void MaintainMarketPrice_Load(object sender, EventArgs e)
        {
            tbx_物料.Text = materialID;
            tbx_物料.Enabled = false;
           List<string> list = act.GetCurrencyType();
           cbb_货币.DataSource = list;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tbx_市场价格.Text == "")
            {
                MessageUtil.ShowError("请输入正确的价格数值");
            }
            else
            {
                string sql = "INSERT INTO [Market_Price] (Material_ID,Market_Price,Start_Time,End_Time)VALUES";
                sql += "('" + tbx_物料.Text + "','" + tbx_市场价格.Text + "','" + dtp_起始期.Value + "','" + dtp_终止期.Value + "')";
                DBHelper.ExecuteNonQuery(sql);
                MessageUtil.ShowTips("市场价格维护成功");
            }
            //this.Close();
        }

        private void btn_查看_Click(object sender, EventArgs e)
        {
            ShowMarketPrice smp = new ShowMarketPrice(materialID);
            smp.Show();
        }

        private void tbx_市场价格_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "市场价格";
            dict_temp.Add(key, "");
            formh.onlyNumber(tbx_市场价格, key, dict_temp);
        }

    }
}
