﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Common.CommonUtils;
using Lib.Model.MD.MT;
using MMClient.MD.FI;
using MMClient.MD.GN;

namespace MMClient.MD.MT
{
    public partial class MPN1 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MPN1()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        MaterialBLL mtb = new MaterialBLL();
        private void MPN1_Load(object sender, EventArgs e)
        {
            List<string> list2 = mtb.GetAllMaterialID();
            string sql = "SELECT Material_Name,Material_Type,Type_Name,Material_Standard FROM [Material] WHERE MPN_NO <>'true' or MPN_NO is null";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql);
            cbm.FuzzyQury(cbb_物料编号, list2, dt2);
            string sql1 = "SELECT * FROM [Manufacturer] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql1);
            List<string> list = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(dt.Rows[i][0].ToString());
            }
            cbm.FuzzyQury(cbb_制造商, list);
        }

        private void cbb_制造商_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = cbb_物料编号.Text;
            string[] atrArr = str.Split(' ');
            tbx_制造商.Text = "";
            tbx_制造商物料号.Text = "";
            string sql = "SELECT Manufacturer_Name FROM [Manufacturer] WHERE Manufacturer_ID = '" + cbb_制造商.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt.Rows.Count!=0&&dt!=null)
            tbx_制造商.Text = dt.Rows[0][0].ToString();
            tbx_制造商物料号.Text = atrArr[0] +"-" + cbb_制造商.Text;
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO [MPN] (MPN_ID,Material_ID,Manufacturer_ID,Producing_Area,Description)VALUES";
            sql += "('" + tbx_制造商物料号.Text + "','" + cbb_物料编号.Text + "','" + cbb_制造商.Text + "','" + tbx_产地.Text + "','" + tbx_特性描述.Text + "')";
            DBHelper.ExecuteNonQuery(sql);
            MessageUtil.ShowTips("新建制造商物料成功");
            try
            {
                string sql1 = "INSERT INTO [Material](Material_ID,MPN_NO)VALUES('" + tbx_制造商物料号.Text + "','" + true + "')";
                DBHelper.ExecuteNonQuery(sql1);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("已存在制造商物料信息");
            }


        }
    }
}
