﻿namespace MMClient.MD
{
    partial class MTRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_物料编码1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_物料编码2 = new System.Windows.Forms.ComboBox();
            this.cbb_物料名称1 = new System.Windows.Forms.ComboBox();
            this.cbb_物料名称2 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组1 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_最大命中数 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_修改选中数据 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(4, 6);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1146, 452);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "记录变更";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_清除);
            this.groupBox2.Controls.Add(this.btn_查询);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tb_最大命中数);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbb_采购组2);
            this.groupBox2.Controls.Add(this.cbb_采购组1);
            this.groupBox2.Controls.Add(this.cbb_物料名称2);
            this.groupBox2.Controls.Add(this.cbb_物料名称1);
            this.groupBox2.Controls.Add(this.cbb_物料编码2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbb_物料编码1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(9, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1122, 434);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "条件选择";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 41);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料编码";
            // 
            // cbb_物料编码1
            // 
            this.cbb_物料编码1.FormattingEnabled = true;
            this.cbb_物料编码1.Location = new System.Drawing.Point(140, 38);
            this.cbb_物料编码1.Name = "cbb_物料编码1";
            this.cbb_物料编码1.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(338, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "至";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "物料名称";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "采购组";
            // 
            // cbb_物料编码2
            // 
            this.cbb_物料编码2.FormattingEnabled = true;
            this.cbb_物料编码2.Location = new System.Drawing.Point(418, 38);
            this.cbb_物料编码2.Name = "cbb_物料编码2";
            this.cbb_物料编码2.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码2.TabIndex = 5;
            // 
            // cbb_物料名称1
            // 
            this.cbb_物料名称1.FormattingEnabled = true;
            this.cbb_物料名称1.Location = new System.Drawing.Point(140, 116);
            this.cbb_物料名称1.Name = "cbb_物料名称1";
            this.cbb_物料名称1.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料名称1.TabIndex = 6;
            // 
            // cbb_物料名称2
            // 
            this.cbb_物料名称2.FormattingEnabled = true;
            this.cbb_物料名称2.Location = new System.Drawing.Point(418, 116);
            this.cbb_物料名称2.Name = "cbb_物料名称2";
            this.cbb_物料名称2.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料名称2.TabIndex = 7;
            // 
            // cbb_采购组1
            // 
            this.cbb_采购组1.FormattingEnabled = true;
            this.cbb_采购组1.Location = new System.Drawing.Point(140, 187);
            this.cbb_采购组1.Name = "cbb_采购组1";
            this.cbb_采购组1.Size = new System.Drawing.Size(121, 24);
            this.cbb_采购组1.TabIndex = 8;
            // 
            // cbb_采购组2
            // 
            this.cbb_采购组2.FormattingEnabled = true;
            this.cbb_采购组2.Location = new System.Drawing.Point(418, 187);
            this.cbb_采购组2.Name = "cbb_采购组2";
            this.cbb_采购组2.Size = new System.Drawing.Size(121, 24);
            this.cbb_采购组2.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(338, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "至";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(338, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "至";
            // 
            // tb_最大命中数
            // 
            this.tb_最大命中数.Location = new System.Drawing.Point(836, 38);
            this.tb_最大命中数.Name = "tb_最大命中数";
            this.tb_最大命中数.Size = new System.Drawing.Size(100, 22);
            this.tb_最大命中数.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(679, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "最大命中数";
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(227, 266);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(75, 32);
            this.btn_查询.TabIndex = 14;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(354, 266);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(75, 32);
            this.btn_清除.TabIndex = 15;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 475);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1041, 190);
            this.dataGridView1.TabIndex = 1;
            // 
            // btn_修改选中数据
            // 
            this.btn_修改选中数据.Location = new System.Drawing.Point(240, 687);
            this.btn_修改选中数据.Name = "btn_修改选中数据";
            this.btn_修改选中数据.Size = new System.Drawing.Size(75, 45);
            this.btn_修改选中数据.TabIndex = 2;
            this.btn_修改选中数据.Text = "修改选中数据";
            this.btn_修改选中数据.UseVisualStyleBackColor = true;
            // 
            // MTRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 744);
            this.Controls.Add(this.btn_修改选中数据);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTRecord";
            this.Text = "MTRecord";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_物料编码1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_最大命中数;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_采购组2;
        private System.Windows.Forms.ComboBox cbb_采购组1;
        private System.Windows.Forms.ComboBox cbb_物料名称2;
        private System.Windows.Forms.ComboBox cbb_物料名称1;
        private System.Windows.Forms.ComboBox cbb_物料编码2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_修改选中数据;
    }
}