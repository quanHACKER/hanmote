﻿namespace MMClient.MD.MT
{
    partial class SPCPY1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SPCPY1));
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_供应商编码 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商名称 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_公司代码 = new System.Windows.Forms.ComboBox();
            this.cbb_公司名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtp_证书日期 = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.cbb_批准组 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbb_现金管理组 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbb_排序码 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbb_少数标志 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_权限组 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_总部 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_统驭科目 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtp_上次利息运行 = new System.Windows.Forms.DateTimePicker();
            this.dtp_上次计算日期 = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbb_计息周期 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbb_利息计算标志 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtp_有效结束日 = new System.Windows.Forms.DateTimePicker();
            this.cbb_免税的权限 = new System.Windows.Forms.ComboBox();
            this.cbb_免税号 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbb_收受人类型 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbb_折扣所得税国家 = new System.Windows.Forms.ComboBox();
            this.cbb_预扣税款代码 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbb_人事编号 = new System.Windows.Forms.ComboBox();
            this.cbb_先前的账户号码 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 76);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "供应商";
            // 
            // cbb_供应商编码
            // 
            this.cbb_供应商编码.Enabled = false;
            this.cbb_供应商编码.FormattingEnabled = true;
            this.cbb_供应商编码.Location = new System.Drawing.Point(185, 73);
            this.cbb_供应商编码.Name = "cbb_供应商编码";
            this.cbb_供应商编码.Size = new System.Drawing.Size(174, 24);
            this.cbb_供应商编码.TabIndex = 11;
            this.cbb_供应商编码.Tag = "1";
            this.cbb_供应商编码.SelectedIndexChanged += new System.EventHandler(this.cbb_供应商编码_SelectedIndexChanged);
            // 
            // cbb_供应商名称
            // 
            this.cbb_供应商名称.Enabled = false;
            this.cbb_供应商名称.FormattingEnabled = true;
            this.cbb_供应商名称.Location = new System.Drawing.Point(364, 73);
            this.cbb_供应商名称.Name = "cbb_供应商名称";
            this.cbb_供应商名称.Size = new System.Drawing.Size(208, 24);
            this.cbb_供应商名称.TabIndex = 12;
            this.cbb_供应商名称.Tag = "2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "公司代码";
            // 
            // cbb_公司代码
            // 
            this.cbb_公司代码.FormattingEnabled = true;
            this.cbb_公司代码.Location = new System.Drawing.Point(185, 103);
            this.cbb_公司代码.Name = "cbb_公司代码";
            this.cbb_公司代码.Size = new System.Drawing.Size(174, 24);
            this.cbb_公司代码.TabIndex = 14;
            this.cbb_公司代码.Tag = "1";
            this.cbb_公司代码.SelectedIndexChanged += new System.EventHandler(this.cbb_公司代码_SelectedIndexChanged);
            // 
            // cbb_公司名称
            // 
            this.cbb_公司名称.FormattingEnabled = true;
            this.cbb_公司名称.Items.AddRange(new object[] {
            "B602实验室"});
            this.cbb_公司名称.Location = new System.Drawing.Point(365, 103);
            this.cbb_公司名称.Name = "cbb_公司名称";
            this.cbb_公司名称.Size = new System.Drawing.Size(208, 24);
            this.cbb_公司名称.TabIndex = 15;
            this.cbb_公司名称.Tag = "2";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dtp_证书日期);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbb_批准组);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbb_现金管理组);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbb_排序码);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbb_少数标志);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbb_权限组);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbb_总部);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_统驭科目);
            this.groupBox1.Location = new System.Drawing.Point(8, 128);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1166, 184);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "会计信息";
            // 
            // dtp_证书日期
            // 
            this.dtp_证书日期.Location = new System.Drawing.Point(607, 153);
            this.dtp_证书日期.Name = "dtp_证书日期";
            this.dtp_证书日期.Size = new System.Drawing.Size(200, 22);
            this.dtp_证书日期.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(409, 154);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "证书日期";
            // 
            // cbb_批准组
            // 
            this.cbb_批准组.FormattingEnabled = true;
            this.cbb_批准组.Location = new System.Drawing.Point(607, 116);
            this.cbb_批准组.Name = "cbb_批准组";
            this.cbb_批准组.Size = new System.Drawing.Size(121, 24);
            this.cbb_批准组.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(409, 119);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "批准组";
            // 
            // cbb_现金管理组
            // 
            this.cbb_现金管理组.FormattingEnabled = true;
            this.cbb_现金管理组.Location = new System.Drawing.Point(607, 79);
            this.cbb_现金管理组.Name = "cbb_现金管理组";
            this.cbb_现金管理组.Size = new System.Drawing.Size(121, 24);
            this.cbb_现金管理组.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(409, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "现金管理组";
            // 
            // cbb_排序码
            // 
            this.cbb_排序码.FormattingEnabled = true;
            this.cbb_排序码.Location = new System.Drawing.Point(607, 44);
            this.cbb_排序码.Name = "cbb_排序码";
            this.cbb_排序码.Size = new System.Drawing.Size(121, 24);
            this.cbb_排序码.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(409, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "排序码";
            // 
            // cbb_少数标志
            // 
            this.cbb_少数标志.FormattingEnabled = true;
            this.cbb_少数标志.Location = new System.Drawing.Point(180, 151);
            this.cbb_少数标志.Name = "cbb_少数标志";
            this.cbb_少数标志.Size = new System.Drawing.Size(121, 24);
            this.cbb_少数标志.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "少数标志";
            // 
            // cbb_权限组
            // 
            this.cbb_权限组.FormattingEnabled = true;
            this.cbb_权限组.Location = new System.Drawing.Point(180, 116);
            this.cbb_权限组.Name = "cbb_权限组";
            this.cbb_权限组.Size = new System.Drawing.Size(121, 24);
            this.cbb_权限组.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "权限组";
            // 
            // cbb_总部
            // 
            this.cbb_总部.FormattingEnabled = true;
            this.cbb_总部.Location = new System.Drawing.Point(180, 79);
            this.cbb_总部.Name = "cbb_总部";
            this.cbb_总部.Size = new System.Drawing.Size(121, 24);
            this.cbb_总部.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "总部";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "统驭科目";
            // 
            // cbb_统驭科目
            // 
            this.cbb_统驭科目.FormattingEnabled = true;
            this.cbb_统驭科目.Location = new System.Drawing.Point(180, 40);
            this.cbb_统驭科目.Name = "cbb_统驭科目";
            this.cbb_统驭科目.Size = new System.Drawing.Size(121, 24);
            this.cbb_统驭科目.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtp_上次利息运行);
            this.groupBox2.Controls.Add(this.dtp_上次计算日期);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.cbb_计息周期);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbb_利息计算标志);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(8, 318);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1161, 117);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "利息计算";
            // 
            // dtp_上次利息运行
            // 
            this.dtp_上次利息运行.Location = new System.Drawing.Point(603, 84);
            this.dtp_上次利息运行.Name = "dtp_上次利息运行";
            this.dtp_上次利息运行.Size = new System.Drawing.Size(200, 22);
            this.dtp_上次利息运行.TabIndex = 15;
            // 
            // dtp_上次计算日期
            // 
            this.dtp_上次计算日期.Location = new System.Drawing.Point(603, 40);
            this.dtp_上次计算日期.Name = "dtp_上次计算日期";
            this.dtp_上次计算日期.Size = new System.Drawing.Size(200, 22);
            this.dtp_上次计算日期.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(405, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "上次利息运行";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(405, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 11;
            this.label13.Text = "上次计算日期";
            // 
            // cbb_计息周期
            // 
            this.cbb_计息周期.FormattingEnabled = true;
            this.cbb_计息周期.Location = new System.Drawing.Point(176, 86);
            this.cbb_计息周期.Name = "cbb_计息周期";
            this.cbb_计息周期.Size = new System.Drawing.Size(121, 24);
            this.cbb_计息周期.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "计息周期";
            // 
            // cbb_利息计算标志
            // 
            this.cbb_利息计算标志.FormattingEnabled = true;
            this.cbb_利息计算标志.Location = new System.Drawing.Point(176, 42);
            this.cbb_利息计算标志.Name = "cbb_利息计算标志";
            this.cbb_利息计算标志.Size = new System.Drawing.Size(121, 24);
            this.cbb_利息计算标志.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "利息计算标志";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtp_有效结束日);
            this.groupBox3.Controls.Add(this.cbb_免税的权限);
            this.groupBox3.Controls.Add(this.cbb_免税号);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.cbb_收受人类型);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.cbb_折扣所得税国家);
            this.groupBox3.Controls.Add(this.cbb_预扣税款代码);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Location = new System.Drawing.Point(8, 441);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1161, 145);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "预扣税";
            // 
            // dtp_有效结束日
            // 
            this.dtp_有效结束日.Location = new System.Drawing.Point(604, 78);
            this.dtp_有效结束日.Name = "dtp_有效结束日";
            this.dtp_有效结束日.Size = new System.Drawing.Size(200, 22);
            this.dtp_有效结束日.TabIndex = 22;
            // 
            // cbb_免税的权限
            // 
            this.cbb_免税的权限.FormattingEnabled = true;
            this.cbb_免税的权限.Location = new System.Drawing.Point(604, 116);
            this.cbb_免税的权限.Name = "cbb_免税的权限";
            this.cbb_免税的权限.Size = new System.Drawing.Size(121, 24);
            this.cbb_免税的权限.TabIndex = 21;
            // 
            // cbb_免税号
            // 
            this.cbb_免税号.FormattingEnabled = true;
            this.cbb_免税号.Location = new System.Drawing.Point(604, 37);
            this.cbb_免税号.Name = "cbb_免税号";
            this.cbb_免税号.Size = new System.Drawing.Size(121, 24);
            this.cbb_免税号.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(406, 116);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 17);
            this.label20.TabIndex = 18;
            this.label20.Text = "免税的权限";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(406, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 17);
            this.label19.TabIndex = 17;
            this.label19.Text = "有效结束日";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(406, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 17);
            this.label18.TabIndex = 16;
            this.label18.Text = "免税号";
            // 
            // cbb_收受人类型
            // 
            this.cbb_收受人类型.FormattingEnabled = true;
            this.cbb_收受人类型.Location = new System.Drawing.Point(177, 116);
            this.cbb_收受人类型.Name = "cbb_收受人类型";
            this.cbb_收受人类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_收受人类型.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 17);
            this.label17.TabIndex = 14;
            this.label17.Text = "收受人类型";
            // 
            // cbb_折扣所得税国家
            // 
            this.cbb_折扣所得税国家.FormattingEnabled = true;
            this.cbb_折扣所得税国家.Location = new System.Drawing.Point(177, 76);
            this.cbb_折扣所得税国家.Name = "cbb_折扣所得税国家";
            this.cbb_折扣所得税国家.Size = new System.Drawing.Size(121, 24);
            this.cbb_折扣所得税国家.TabIndex = 13;
            // 
            // cbb_预扣税款代码
            // 
            this.cbb_预扣税款代码.FormattingEnabled = true;
            this.cbb_预扣税款代码.Location = new System.Drawing.Point(177, 37);
            this.cbb_预扣税款代码.Name = "cbb_预扣税款代码";
            this.cbb_预扣税款代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_预扣税款代码.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 79);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 17);
            this.label16.TabIndex = 1;
            this.label16.Text = "折扣所得税国家";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "预扣税款代码";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbb_人事编号);
            this.groupBox4.Controls.Add(this.cbb_先前的账户号码);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Location = new System.Drawing.Point(12, 582);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1155, 68);
            this.groupBox4.TabIndex = 19;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "参考数据";
            // 
            // cbb_人事编号
            // 
            this.cbb_人事编号.FormattingEnabled = true;
            this.cbb_人事编号.Location = new System.Drawing.Point(599, 35);
            this.cbb_人事编号.Name = "cbb_人事编号";
            this.cbb_人事编号.Size = new System.Drawing.Size(121, 24);
            this.cbb_人事编号.TabIndex = 17;
            // 
            // cbb_先前的账户号码
            // 
            this.cbb_先前的账户号码.FormattingEnabled = true;
            this.cbb_先前的账户号码.Location = new System.Drawing.Point(172, 35);
            this.cbb_先前的账户号码.Name = "cbb_先前的账户号码";
            this.cbb_先前的账户号码.Size = new System.Drawing.Size(121, 24);
            this.cbb_先前的账户号码.TabIndex = 16;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(401, 38);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 17);
            this.label22.TabIndex = 1;
            this.label22.Text = "人事编号";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 38);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "先前的账户号码";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1261, 67);
            this.toolStripExecutionMonitorSetting.TabIndex = 25;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(88, 64);
            this.toolStripButton1.Text = "选择供应商";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // certainButton
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("certainButton.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "certainButton";
            this.toolStripButton4.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // SPCPY1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbb_公司名称);
            this.Controls.Add(this.cbb_公司代码);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbb_供应商名称);
            this.Controls.Add(this.cbb_供应商编码);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SPCPY1";
            this.Text = "公司代码视图";
            this.Load += new System.EventHandler(this.SPCPY1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbb_供应商编码;
        public System.Windows.Forms.ComboBox cbb_供应商名称;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cbb_公司代码;
        public System.Windows.Forms.ComboBox cbb_公司名称;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_统驭科目;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbb_少数标志;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbb_权限组;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_总部;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbb_批准组;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbb_现金管理组;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbb_排序码;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbb_计息周期;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbb_利息计算标志;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbb_免税的权限;
        private System.Windows.Forms.ComboBox cbb_免税号;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbb_收受人类型;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbb_折扣所得税国家;
        private System.Windows.Forms.ComboBox cbb_预扣税款代码;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbb_人事编号;
        private System.Windows.Forms.ComboBox cbb_先前的账户号码;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtp_证书日期;
        private System.Windows.Forms.DateTimePicker dtp_上次利息运行;
        private System.Windows.Forms.DateTimePicker dtp_上次计算日期;
        private System.Windows.Forms.DateTimePicker dtp_有效结束日;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
    }
}