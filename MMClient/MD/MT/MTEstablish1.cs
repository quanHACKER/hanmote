﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.General.Material_Type;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.MT;
using MMClient.MD.FI;
using MMClient.MD.GN;
using MMClient.MD.SP;

namespace MMClient.MD.MT
{
    public partial class MTEstablish1 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        MTTypeBBLL mtb = new MTTypeBBLL();
        MTTypeCBLL mtc = new MTTypeCBLL();
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbx = new ComboBoxItem();
        MaterialBLL mt = new MaterialBLL();
        public MTEstablish1()
        {
            InitializeComponent();
        }
      
       
        private void cbb_物料类型_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbb_分类.Items.Clear();
            cbb_物资型号.Items.Clear();
            cbb_分类.Text = "";
            cbb_物资型号.Text = "";
           List<string> list = mtb.GetLittleClassfyByBig(cbb_物料类型.Text);
           if (list.Count == 0)
           {
               MessageUtil.ShowError("无可用分类，请在一般设置中维护");
               cbb_物料类型.Text = "";
           }
           else
           {
               cbx.FuzzyQury(cbb_分类, list);
           }
            
   
        }

        private void cbb_分类_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbb_物资型号.Items.Clear();
            cbb_物资型号.Text = "";
            string sql = "SELECT * FROM [Typeclassfy] WHERE Littleclassfy_Name='" + cbb_分类.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowError("无可用物资型号，请在一般设置中维护");
                cbb_分类.Text = "";
            }
            else
            {
                int i;
                for (i = 0; i < Convert.ToInt32(dt.Rows.Count.ToString()); i++)
                {
                    cbb_物资型号.Items.Add(dt.Rows[i][0]);

                }
            }
        }
//以上需要修改
        private void MTEstablish1_Load(object sender, EventArgs e)
        {
            //物料类型
            List<string> list = mtb.GetAllBigClassfyName();      
                cbx.FuzzyQury(cbb_物料类型, list);
            //物料组
            List<string> list_MtGroupNames = mtb.GetAllMtGroupName();
            cbx.FuzzyQury(cbMtGroupName, list_MtGroupNames);
            //行业领域
            List<string> list_filed = mtb.GetAllfiled();
            cbx.FuzzyQury(cbb_行业领域, list_filed);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Order ord = new Order();
            ord.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MaintainClass mtc = new MaintainClass();
            mtc.Show();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            cbb_分类.Text = "";
            cbb_物资型号.Text = "";
            cbb_物料类型.Text = "";
            cbb_物料名称.Text = "";
            cbb_分类.Items.Clear();
            cbb_物资型号.Items.Clear();
            cbb_物料类型.Items.Clear();
            List<string> list = mtb.GetAllBigClassfyName();
            cbx.FuzzyQury(cbb_物料类型, list);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (cbb_物料名称.Text == "")
            {
                MessageUtil.ShowError("请输入物料名称");
                cbb_物料名称.Focus();
            }
            else
            {
                try
                {
                    string mttp = cbb_物料类型.Text;
                    string mtcl = cbb_分类.Text;
                    string mtdt = cbb_物资型号.Text;
                    string sqltp = " SELECT TOP 1  * FROM [Bigclassfy] WHERE Bigclassfy_Name='" + mttp + "'";
                    DataTable dttp = DBHelper.ExecuteQueryDT(sqltp);
                    string tpid1 = (string)dttp.Rows[0][1];
                    string tpid = tpid1.Trim();
                    string sqlcl = "SELECT TOP 1 * FROM [littleclassfy] WHERE Littleclassfy_Name='" + mtcl + "'";
                    DataTable dtcl = DBHelper.ExecuteQueryDT(sqlcl);
                    string clid1 = (string)dtcl.Rows[0][1];
                    string clid = clid1.Trim();
                    string sqldt = " SELECT TOP 1 * FROM [Typeclassfy] WHERE Type_Name='" + mtdt + "'";
                    DataTable dtdt = DBHelper.ExecuteQueryDT(sqldt);
                    string dtid1 = (string)dtdt.Rows[0][1];
                    string dtid = dtid1.Trim();
                    string sqlct = "select count(*) from [Material] WHERE Purpose='" + mtdt + "'";
                    DataTable dtct = DBHelper.ExecuteQueryDT(sqlct);
                    string ctid1 = dtct.Rows[0][0].ToString();
                    string ctid = ctid1.Trim();
                    string s = ctid.PadLeft(8, '0');
                    string s1 = s.Trim();
                    string mtid = tpid + clid + dtid + s;
                   
                    string sql = "INSERT INTO [Material](Material_ID,Material_Type,Type_Name,Purpose,Material_Name,Industry_Category,Material_Group)VALUES('" + mtid + "','" + mttp + "','" + mtcl + "','" + mtdt + "','" + cbb_物料名称.Text + "','" + cbb_行业领域.Text + "','"+this.cbMtGroupName.Text.ToString()+"')";
                    DBHelper.ExecuteNonQuery(sql);
                    
                    MessageUtil.ShowTips("创建成功");
                    cbb_分类.Text = "";
                    cbb_行业领域.Text = "";
                    cbb_物料类型.Text = "";
                    cbb_物料名称.Text = "";
                    cbb_物资型号.Text = "";
                    
                }
                catch (Exception ex)
                {
                    List<string> list1 = mtb.GetAllBigClassfyName();
                    List<string> list2 = mtc.GetAllBigClassfyName();
                    List<string> list3 = mtc.GetAllTypeName();

                    if (cbb_物料类型.Text == "" || gn.IDInTheList(list1, cbb_物料类型.Text) == true)
                    {
                        MessageUtil.ShowError("物料类型输入错误，请重新输入");
                        cbb_物料类型.Focus();
                    }
                    else
                    {
                        if (cbb_分类.Text == "" || gn.IDInTheList(list2, cbb_分类.Text) == true)
                        {
                            MessageUtil.ShowError("分类输入错误，请重新输入");
                            cbb_分类.Focus();
                        }
                        else
                        {
                            if (cbb_物资型号.Text == "" || gn.IDInTheList(list3, cbb_物资型号.Text) == true)
                            {
                                MessageUtil.ShowError("物资型号输入错误，请重新输入");
                                cbb_物资型号.Focus();
                            }
                            else
                            {
                                throw (ex);
                                MessageUtil.ShowError("未知错误");
                            }

                        }

                    }


                }
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

      

        

    }
}
