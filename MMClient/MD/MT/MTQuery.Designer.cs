﻿namespace MMClient.MD
{
    partial class MTQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbB_采购组2 = new System.Windows.Forms.ComboBox();
            this.cbB_物料名称2 = new System.Windows.Forms.ComboBox();
            this.tB_其他 = new System.Windows.Forms.TextBox();
            this.cbB_采购组1 = new System.Windows.Forms.ComboBox();
            this.cbB_物料名称1 = new System.Windows.Forms.ComboBox();
            this.cbB_物资类别2 = new System.Windows.Forms.ComboBox();
            this.cbB_物资类别1 = new System.Windows.Forms.ComboBox();
            this.tB_最大命中数 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.btn_数据分析 = new System.Windows.Forms.Button();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.dgV_数据结果 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage27 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btn_修改 = new System.Windows.Forms.Button();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgV_数据结果)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPage26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Aqua;
            this.groupBox5.Controls.Add(this.cbB_采购组2);
            this.groupBox5.Controls.Add(this.cbB_物料名称2);
            this.groupBox5.Controls.Add(this.tB_其他);
            this.groupBox5.Controls.Add(this.cbB_采购组1);
            this.groupBox5.Controls.Add(this.cbB_物料名称1);
            this.groupBox5.Controls.Add(this.cbB_物资类别2);
            this.groupBox5.Controls.Add(this.cbB_物资类别1);
            this.groupBox5.Controls.Add(this.tB_最大命中数);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.btn_清除);
            this.groupBox5.Controls.Add(this.btn_查询);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(961, 149);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "条件选择";
            // 
            // cbB_采购组2
            // 
            this.cbB_采购组2.FormattingEnabled = true;
            this.cbB_采购组2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "服务",
            "半成品"});
            this.cbB_采购组2.Location = new System.Drawing.Point(778, 20);
            this.cbB_采购组2.Name = "cbB_采购组2";
            this.cbB_采购组2.Size = new System.Drawing.Size(121, 23);
            this.cbB_采购组2.TabIndex = 44;
            // 
            // cbB_物料名称2
            // 
            this.cbB_物料名称2.FormattingEnabled = true;
            this.cbB_物料名称2.Items.AddRange(new object[] {
            "大梁中板",
            "天然橡胶",
            "钢丸",
            "轴承",
            "轮胎",
            "轿车"});
            this.cbB_物料名称2.Location = new System.Drawing.Point(303, 57);
            this.cbB_物料名称2.Name = "cbB_物料名称2";
            this.cbB_物料名称2.Size = new System.Drawing.Size(121, 23);
            this.cbB_物料名称2.TabIndex = 43;
            this.cbB_物料名称2.Text = "钢丸";
            // 
            // tB_其他
            // 
            this.tB_其他.Location = new System.Drawing.Point(569, 60);
            this.tB_其他.Name = "tB_其他";
            this.tB_其他.Size = new System.Drawing.Size(100, 25);
            this.tB_其他.TabIndex = 42;
            // 
            // cbB_采购组1
            // 
            this.cbB_采购组1.FormattingEnabled = true;
            this.cbB_采购组1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "服务",
            "半成品"});
            this.cbB_采购组1.Location = new System.Drawing.Point(565, 20);
            this.cbB_采购组1.Name = "cbB_采购组1";
            this.cbB_采购组1.Size = new System.Drawing.Size(121, 23);
            this.cbB_采购组1.TabIndex = 40;
            // 
            // cbB_物料名称1
            // 
            this.cbB_物料名称1.FormattingEnabled = true;
            this.cbB_物料名称1.Items.AddRange(new object[] {
            "大梁中板",
            "天然橡胶",
            "钢丸",
            "轴承",
            "轮胎",
            "轿车"});
            this.cbB_物料名称1.Location = new System.Drawing.Point(114, 57);
            this.cbB_物料名称1.Name = "cbB_物料名称1";
            this.cbB_物料名称1.Size = new System.Drawing.Size(121, 23);
            this.cbB_物料名称1.TabIndex = 38;
            this.cbB_物料名称1.Text = "钢丸";
            // 
            // cbB_物资类别2
            // 
            this.cbB_物资类别2.FormattingEnabled = true;
            this.cbB_物资类别2.Items.AddRange(new object[] {
            "原材料",
            "服务",
            "半成品",
            "100",
            "200",
            "300",
            "41251100"});
            this.cbB_物资类别2.Location = new System.Drawing.Point(303, 20);
            this.cbB_物资类别2.Name = "cbB_物资类别2";
            this.cbB_物资类别2.Size = new System.Drawing.Size(121, 23);
            this.cbB_物资类别2.TabIndex = 37;
            this.cbB_物资类别2.Text = "原材料";
            // 
            // cbB_物资类别1
            // 
            this.cbB_物资类别1.FormattingEnabled = true;
            this.cbB_物资类别1.Items.AddRange(new object[] {
            "原材料",
            "服务",
            "半成品",
            "41251400",
            "100",
            "200",
            "300"});
            this.cbB_物资类别1.Location = new System.Drawing.Point(114, 20);
            this.cbB_物资类别1.Name = "cbB_物资类别1";
            this.cbB_物资类别1.Size = new System.Drawing.Size(121, 23);
            this.cbB_物资类别1.TabIndex = 36;
            this.cbB_物资类别1.Text = "原材料";
            // 
            // tB_最大命中数
            // 
            this.tB_最大命中数.Location = new System.Drawing.Point(569, 107);
            this.tB_最大命中数.Name = "tB_最大命中数";
            this.tB_最大命中数.Size = new System.Drawing.Size(117, 25);
            this.tB_最大命中数.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(454, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 15);
            this.label10.TabIndex = 34;
            this.label10.Text = "最大命中数";
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(340, 107);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(75, 23);
            this.btn_清除.TabIndex = 33;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(205, 107);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(75, 23);
            this.btn_查询.TabIndex = 32;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            this.btn_查询.Click += new System.EventHandler(this.btn_查询_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(454, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 15);
            this.label9.TabIndex = 30;
            this.label9.Text = "其他";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(716, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 15);
            this.label8.TabIndex = 28;
            this.label8.Text = "至";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(454, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "采购组";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(257, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "至";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 22;
            this.label5.Text = "物料名称";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(257, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "至";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "物料编码";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(401, 386);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "导出数据";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btn_数据分析
            // 
            this.btn_数据分析.Location = new System.Drawing.Point(272, 386);
            this.btn_数据分析.Name = "btn_数据分析";
            this.btn_数据分析.Size = new System.Drawing.Size(75, 23);
            this.btn_数据分析.TabIndex = 2;
            this.btn_数据分析.Text = "数据分析";
            this.btn_数据分析.UseVisualStyleBackColor = true;
            this.btn_数据分析.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(149, 386);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(75, 23);
            this.btn_删除.TabIndex = 1;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // dgV_数据结果
            // 
            this.dgV_数据结果.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgV_数据结果.Location = new System.Drawing.Point(23, 177);
            this.dgV_数据结果.Name = "dgV_数据结果";
            this.dgV_数据结果.RowTemplate.Height = 23;
            this.dgV_数据结果.Size = new System.Drawing.Size(542, 192);
            this.dgV_数据结果.TabIndex = 0;
            this.dgV_数据结果.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgV_数据结果_CellClick);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.tabControl6);
            this.groupBox1.Location = new System.Drawing.Point(15, 433);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(549, 281);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "详细信息";
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPage26);
            this.tabControl6.Controls.Add(this.tabPage27);
            this.tabControl6.Location = new System.Drawing.Point(1, 20);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(538, 261);
            this.tabControl6.TabIndex = 0;
            // 
            // tabPage26
            // 
            this.tabPage26.Controls.Add(this.dataGridView2);
            this.tabPage26.Location = new System.Drawing.Point(4, 25);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage26.Size = new System.Drawing.Size(530, 232);
            this.tabPage26.TabIndex = 0;
            this.tabPage26.Text = "详细记录";
            this.tabPage26.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(524, 226);
            this.dataGridView2.TabIndex = 0;
            // 
            // tabPage27
            // 
            this.tabPage27.Location = new System.Drawing.Point(4, 25);
            this.tabPage27.Name = "tabPage27";
            this.tabPage27.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage27.Size = new System.Drawing.Size(530, 232);
            this.tabPage27.TabIndex = 1;
            this.tabPage27.Text = "其他项";
            this.tabPage27.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox2.Controls.Add(this.chart1);
            this.groupBox2.Location = new System.Drawing.Point(581, 177);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(440, 537);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据分析";
            // 
            // chart1
            // 
            this.chart1.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.TileFlipY;
            this.chart1.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chart1.BorderlineColor = System.Drawing.Color.Maroon;
            this.chart1.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Emboss;
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(17, 20);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chart1.Size = new System.Drawing.Size(417, 450);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title2.Name = "物料编码的数量";
            this.chart1.Titles.Add(title2);
            // 
            // btn_修改
            // 
            this.btn_修改.Location = new System.Drawing.Point(34, 386);
            this.btn_修改.Name = "btn_修改";
            this.btn_修改.Size = new System.Drawing.Size(75, 23);
            this.btn_修改.TabIndex = 22;
            this.btn_修改.Text = "修改";
            this.btn_修改.UseVisualStyleBackColor = true;
            this.btn_修改.Click += new System.EventHandler(this.btn_修改_Click);
            // 
            // MTQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1033, 726);
            this.Controls.Add(this.btn_修改);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_数据分析);
            this.Controls.Add(this.btn_删除);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.dgV_数据结果);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTQuery";
            this.Text = "Query";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MTQuery_FormClosed);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgV_数据结果)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tabControl6.ResumeLayout(false);
            this.tabPage26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tB_最大命中数;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_数据分析;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.DataGridView dgV_数据结果;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPage26;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabPage tabPage27;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbB_物资类别2;
        private System.Windows.Forms.ComboBox cbB_物资类别1;
        private System.Windows.Forms.ComboBox cbB_采购组2;
        private System.Windows.Forms.ComboBox cbB_物料名称2;
        private System.Windows.Forms.TextBox tB_其他;
        private System.Windows.Forms.ComboBox cbB_采购组1;
        private System.Windows.Forms.ComboBox cbB_物料名称1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btn_修改;
    }
}