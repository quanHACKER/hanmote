﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll;
using System.IO;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;

namespace MMClient.MD.MT
{
    public partial class NewMaterialGroup : Form
    {
        FTPHelper fTPHelper = new FTPHelper("");
        public NewMaterialGroup()
        {
            InitializeComponent();
        }

        private void NewMaterialGroup_Load(object sender, EventArgs e)
        {
            AccountBLL ACT = new AccountBLL();
          List<string> list =  ACT.GetAllFactory();


        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            MaterialGroupModel materialGroupModel = new MaterialGroupModel();
            GeneralBLL gn = new GeneralBLL();
            materialGroupModel.MtGroupId = tbx_物料组编号.Text;
            
            //string fid = cbb_所属工厂编号.Text;
            string fid = "10001";
            string evaluate = "";//评级
            string weight= "";
            string standard = "";//合格标准
            string filepath = "" ;//路径
            //standard = "A级_" + A1.Text + "_" + A2.Text + "," + "B级_" + B1.Text + "_" + B2.Text + "," + "C级_" + C1.Text + "_" + C2.Text;
            string evalid= "";//评级标准

            evaluate = CB_mGroupLevel.Text.ToString();
            switch (evaluate) {
                case "A":evalid ="4";break;
                case "B": evalid = "3"; break;
                case "C": evalid = "2"; break;
                case "D": evalid = "1"; break;
                case "E": evalid = "1"; break;
                default:break;
            }
            //物料组层级值
            materialGroupModel.MtGroupLevelValue = evalid;
            //物料组类型
            materialGroupModel.MtGroupClass = comboBox1.Text;
            //物料组编号
            materialGroupModel.MtGroupId = tbx_物料组编号.Text;
            //物料组等级
            materialGroupModel.MtGroupLevel = evaluate;
            //物料组名称
            materialGroupModel.MtGroupName = tbx_物料组描述.Text;
            //物料组认证绿色目标值
           /* materialGroupModel.CGreenValue = TB_CGreenValue.Text;
            //物料组认证最低目标值
            materialGroupModel.CLowValue = TB_CLowValue.Text;
            //物料组绩效绿色目标值
            materialGroupModel.PGreenValue = TB_PFGreenValue.Text;
            //物料组绩效最低目标值
            materialGroupModel.PLowValue = TB_PFLowValue.Text;*/

            if (gn.NStatus(materialGroupModel.MtGroupName))
            {
                try
                {
                    gn.NewMaterialGroup(materialGroupModel);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                MessageBox.Show("新建物料组成功");
                this.Close();
            }
            else
            {
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                DialogResult dr = MessageBox.Show("已经建立过此物料组，要重新建立吗?", "重新建立", messButton);
                if (dr == DialogResult.OK)//如果点击“确定”按钮
                {
                    gn.Ndelete(materialGroupModel.MtGroupName);
                    try
                    {
                        gn.NewMaterialGroup(materialGroupModel);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    MessageBox.Show("新建物料组成功");
                    this.Close();
                }
                else//如果点击“取消”按钮
                {

                }
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox3.Checked)
                textBox3.Enabled = true;
            else
                textBox3.Enabled = false;*/
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

       private void textBox1_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox1.Checked)
                textBox1.Enabled = true;
            else
                textBox1.Enabled = false;*/
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox2.Checked)
                textBox2.Enabled = true;
            else
                textBox2.Enabled = false;*/
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox3.Checked)
                textBox3.Enabled = true;
            else
                textBox3.Enabled = false;*/
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox4.Checked)
                textBox4.Enabled = true;
            else
                textBox4.Enabled = false;*/
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox5.Checked)
                textBox5.Enabled = true;
            else
                textBox5.Enabled = false;*/
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            /*if (checkBox6.Checked)
                textBox6.Enabled = true;
            else
                textBox6.Enabled = false;*/
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox1.Checked)
                textBox1.Enabled = true;
            else
                textBox1.Enabled = false;*/
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox2.Checked)
                textBox2.Enabled = true;
            else
                textBox2.Enabled = false;*/
        }




        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox6.Checked)
                textBox6.Enabled = true;
            else
                textBox6.Enabled = false;*/
        }

        private void button1_Click(object sender, EventArgs e)//选择文件
        {
            
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "contract files(*.doc;*.docx;*.pdf)|*.doc;*.docx;*.pdf";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
               /* this.textBox8.Text = openFileDialog.SafeFileName;
                this.textBox9.Text = openFileDialog.FileName;*///路径
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)//上传文件
        {
            /*string p = "C:\\file\\" + textBox8.Text;
            if (File.Exists(p))
            {
                File.Delete(p);
                string destPath1 = Path.Combine(@"C:\file", Path.GetFileName(textBox9.Text));
                System.IO.File.Copy(textBox9.Text, destPath1);
                MessageBox.Show("文件上传成功");
            }
            else
            {
                string destPath = Path.Combine(@"C:\file", Path.GetFileName(textBox9.Text));

                System.IO.File.Copy(textBox9.Text, destPath);
                MessageBox.Show("文件上传成功");
            }
            //this.Close();*/
            //this.fTPHelper.MakeDir(this.id);
            string[] resultFile = null;
            string File = null;

            FTPHelper fTPHelper2 = new FTPHelper("thingGroup");
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileNames;

            }
            for (int i = 0; i < resultFile.Length; i++)
                fTPHelper2.Upload(resultFile[i]);
            for (int i = 0; i < resultFile.Length; i++)
                File = File + ";" + resultFile[i];
            textBox6.Text = File;
            MessageUtil.ShowWarning("上传成功！");
            return;
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox7.Checked)
                textBox7.Enabled = true;
            else
                textBox7.Enabled = false;*/
        }

       

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            /*if (checkBox9.Checked)
                textBox11.Enabled = true;
            else
                textBox11.Enabled = false;*/
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
           /* if (checkBox10.Checked)
                textBox12.Enabled = true;
            else
                textBox12.Enabled = false;*/
        }

        private void cbb_所属工厂编号_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {

        }

    
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           /* string fengong = profCheckbox.Text;
                GeneralBLL gn = new GeneralBLL();
                DataTable dt = gn.GetSelectedEval(fengong);
                comboBox1.DataSource = dt;
                comboBox1.DisplayMember = "User_Name";
                comboBox1.ValueMember = "ID";*/
            



        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           /* string fengong ="质量";
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetSelectedEval(fengong);
            dataGridView1.DataSource = dt;*/
        }

        private void button3_Click(object sender, EventArgs e)
        {
           /* string fengong = "质量";
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetSelectedEval(fengong);
            dataGridView1.DataSource = dt;*/
        }


       
    }
}
