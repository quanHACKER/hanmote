﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.MT;
using Lib.Bll.MDBll.General;
using Lib.Model.MD.MT;
using Lib.Common.CommonUtils;

namespace MMClient.MD.MT
{
    public partial class Choseftymts : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        存储视图 mts;
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        MaterialStorageBLL mtsb = new MaterialStorageBLL();
        public Choseftymts(存储视图 mts)
        {

            InitializeComponent();
            this.mts = mts;
           
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_工厂.Text != "" && cbb_仓库.Text == "")
            {
                MaterialFactory mtfty = mtsb.GetMtFtyInformation(this.mts.cbb_物料编号.Text, cbb_工厂.Text);
                this.mts.cbb_工厂编码.Text = mtfty.Factory_ID;
                this.mts.cbb_工厂名称.Text = mtfty.Factory_Name;
                this.mts.ckb_盘点标识.Checked = mtfty.Check_Mark;
                this.mts.cbb_发货单位.Text = mtfty.Delivery_Unit;
                this.mts.cbb_最大仓储时间.Text = mtfty.MAX_STPeriod.ToString();
                this.mts.cbb_时间单位.Text = mtfty.Hourly_Basis;
                this.Close();

            }
            else
            {
                if (cbb_工厂.Text != "")
                {
                    this.mts.cbb_工厂编码.Text = cbb_工厂.Text;
                    string sql = "SELECT * FROM [Factory] WHERE Factory_ID='" + cbb_工厂.Text + "'";
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    this.mts.cbb_工厂名称.Text = dt.Rows[0][2].ToString();
                    this.mts.cbb_仓库编码.Text = cbb_仓库.Text;
                    string sql1 = "SELECT * FROM [Stock] WHERE Stock_ID='" + cbb_仓库.Text + "'";
                    DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
                    this.mts.cbb_仓库名称.Text = dt1.Rows[0][2].ToString();
                    this.Close();
                }
                else
                {
                    this.mts.cbb_工厂编码.Text = "";
                    this.Close();
                    this.mts.cbb_标号类型.Visible = false;
                    this.mts.cbb_仓库名称.Visible = false;
                    this.mts.cbb_发货单位.Visible = false;
                    this.mts.cbb_工厂编码.Visible = false;
                    this.mts.cbb_工厂名称.Visible = false;
                    this.mts.cbb_时间单位.Visible = false;
                    this.mts.cbb_仓库编码.Visible = false;
                    this.mts.lb_标号类型.Visible = false;
                    this.mts.lb_盘点标识.Visible = false;
                    this.mts.lb_发货单位.Visible = false;

                }
            }

        }

        private void cbb_工厂_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbb_仓库.Text = "";
            cbb_仓库.Items.Clear();
            List<string> list = gn.GetAllStock(cbb_工厂.Text);
            if (list == null)
                MessageUtil.ShowTips("该工厂无下属库存地");
            else
            cbm.FuzzyQury(cbb_仓库, list);
            //string sql = "SELECT * FROM [Stock] WHERE Factory_ID='" + cbb_工厂.Text + "'";
            //DataTable dt = DBHelper.ExecuteQueryDT(sql);
            //int k;
            //for (k = 0; k < Convert.ToInt32(dt.Rows.Count.ToString()); k++)
            //{
            //    cbb_仓库.Items.Add(dt.Rows[k][1].ToString());
            //}
            this.mts.cbb_存储条件.Enabled = false;
            this.mts.cbb_集装箱需求.Enabled = false;
            this.mts.cbb_计量单位.Enabled = false;
            this.mts.cbb_收货单据数.Enabled = false;
            this.mts.cbb_危险物料号.Enabled = false;
            this.mts.cbb_温度条件.Enabled = false;
            this.mts.tbx_总货架寿命.Enabled = false;
            this.mts.tbx_最小剩余货架寿命.Enabled = false;
            MaterialFactory mtfty = new MaterialFactory();
            mtfty = mtsb.GetMtFtyInformation(this.mts.cbb_物料编号.Text, cbb_工厂.Text);
            this.mts.cbb_标号类型.Text = "";
            this.mts.ckb_盘点标识.Checked = mtfty.Check_Mark;
            this.mts.cbb_发货单位.Text = mtfty.Delivery_Unit;
            this.mts.cbb_时间单位.Text = mtfty.Hourly_Basis;
            this.mts.cbb_最大仓储时间.Text = mtfty.MAX_STPeriod.ToString();
            //string sql3 = "SELECT * FROM [MT_FTY] WHERE Material_ID='" + this.mts.cbb_物料编号.Text + "' AND Factory_ID='" + cbb_工厂.Text + "'";
            //DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            //try
            //{
            //    this.mts.cbb_标号类型.Text = "";
            //    this.mts.cbb_时间单位.Text = dt3.Rows[0][26].ToString();
            //    this.mts.cbb_发货单位.Text = dt3.Rows[0][27].ToString();
            //}
            //catch (Exception ex)
            //{
            //    this.mts.cbb_标号类型.Text = "";
            //    this.mts.cbb_时间单位.Text = "";
            //    this.mts.cbb_发货单位.Text = "";
            //}
        }

        private void cbb_仓库_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbb_工厂.Text == "")
            {
                MessageUtil.ShowError("请选择工厂编码");

            }
            else
            {
               MaterialStorage mtsmodel= mtsb.GetMTSteInformation(this.mts.cbb_物料编号.Text, cbb_工厂.Text, cbb_仓库.Text);
               this.mts.cbb_仓库编码.Text = mtsmodel.Stock_ID;
               this.mts.cbb_仓库名称.Text = mtsmodel.Stock_Name;
               this.mts.cbb_发货单位.Text = mtsmodel.Delivery_Unit;
               this.mts.cbb_工厂编码.Text = mtsmodel.Factory_ID;
               this.mts.cbb_工厂名称.Text = mtsmodel.Factory_Name;
               this.mts.cbb_库存仓位.Text = mtsmodel.Inventory_Position;
               this.mts.cbb_最大仓储时间.Text = mtsmodel.MAX_STPeriod;
               this.mts.ckb_盘点标识.Checked = mtsmodel.Check_Mark;


            }
            //string sql3 = "SELECT * FROM [MT_STE] WHERE Material_ID='" + this.mts.cbb_物料编号.Text + "' AND Factory_ID='" + cbb_工厂.Text + "'AND Stock_ID='"+cbb_仓库.Text+"'";
            //DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            //try
            //{
            //    this.mts.cbb_标号类型.Text = "";
            //    this.mts.cbb_时间单位.Text = dt3.Rows[0][7].ToString();
            //    this.mts.cbb_发货单位.Text = dt3.Rows[0][8].ToString();
            //    this.mts.cbb_库存仓位.Text = dt3.Rows[0][11].ToString();
            //}
            //catch (Exception ex)
            //{
            //    this.mts.cbb_库存仓位.Text = "";
            //    this.mts.cbb_时间单位.Text = "";
            //    this.mts.cbb_发货单位.Text = "";
            //}
        }

        private void Choseftymts_Load(object sender, EventArgs e)
        {
            List<string> list1 = gn.GetAllFactory();
            cbm.FuzzyQury(cbb_工厂, list1);
          
        }

        private void cbb_工厂_Leave(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllFactory();
            if (cbb_工厂.Text != "" && gn.IDInTheList(list, cbb_工厂.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的工厂");
                cbb_工厂.Focus();
            }
        }

        private void cbb_仓库_TextChanged(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllStock();
            if (cbb_仓库.Text != "" && gn.IDInTheList(list, cbb_仓库.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的仓库");
                cbb_仓库.Focus();
            }
        }
    }
}
