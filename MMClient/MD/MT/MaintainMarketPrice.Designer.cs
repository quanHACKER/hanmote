﻿namespace MMClient.MD.MT
{
    partial class MaintainMarketPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_查看 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tbx_物料 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtp_终止期 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_货币 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtp_起始期 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_市场价格 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_查看);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.tbx_物料);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtp_终止期);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbb_货币);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtp_起始期);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbx_市场价格);
            this.groupBox1.Location = new System.Drawing.Point(6, 7);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(831, 335);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护市场价格";
            // 
            // btn_查看
            // 
            this.btn_查看.Location = new System.Drawing.Point(705, 300);
            this.btn_查看.Name = "btn_查看";
            this.btn_查看.Size = new System.Drawing.Size(75, 29);
            this.btn_查看.TabIndex = 12;
            this.btn_查看.Text = "查看";
            this.btn_查看.UseVisualStyleBackColor = true;
            this.btn_查看.Click += new System.EventHandler(this.btn_查看_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(624, 300);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 29);
            this.button2.TabIndex = 11;
            this.button2.Text = "重置";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(543, 300);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 29);
            this.button1.TabIndex = 10;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbx_物料
            // 
            this.tbx_物料.Location = new System.Drawing.Point(130, 28);
            this.tbx_物料.Name = "tbx_物料";
            this.tbx_物料.Size = new System.Drawing.Size(100, 22);
            this.tbx_物料.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "物料";
            // 
            // dtp_终止期
            // 
            this.dtp_终止期.Location = new System.Drawing.Point(130, 183);
            this.dtp_终止期.Name = "dtp_终止期";
            this.dtp_终止期.Size = new System.Drawing.Size(200, 22);
            this.dtp_终止期.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "到期日期";
            // 
            // cbb_货币
            // 
            this.cbb_货币.FormattingEnabled = true;
            this.cbb_货币.Location = new System.Drawing.Point(130, 97);
            this.cbb_货币.Name = "cbb_货币";
            this.cbb_货币.Size = new System.Drawing.Size(100, 24);
            this.cbb_货币.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "货币";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "有效起始期";
            // 
            // dtp_起始期
            // 
            this.dtp_起始期.Location = new System.Drawing.Point(130, 134);
            this.dtp_起始期.Name = "dtp_起始期";
            this.dtp_起始期.Size = new System.Drawing.Size(200, 22);
            this.dtp_起始期.TabIndex = 2;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 71);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "市场价格值";
            // 
            // tbx_市场价格
            // 
            this.tbx_市场价格.Location = new System.Drawing.Point(130, 66);
            this.tbx_市场价格.Name = "tbx_市场价格";
            this.tbx_市场价格.Size = new System.Drawing.Size(100, 22);
            this.tbx_市场价格.TabIndex = 0;
            this.tbx_市场价格.TextChanged += new System.EventHandler(this.tbx_市场价格_TextChanged);
            // 
            // MaintainMarketPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 348);
            this.Controls.Add(this.groupBox1);
            this.Name = "MaintainMarketPrice";
            this.Text = "维护市场价格";
            this.Load += new System.EventHandler(this.MaintainMarketPrice_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbx_物料;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtp_终止期;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_货币;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_起始期;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_市场价格;
        private System.Windows.Forms.Button btn_查看;
    }
}