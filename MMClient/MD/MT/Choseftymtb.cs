﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;

namespace MMClient.MD.MT
{
    public partial class Choseftymtb : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        GeneralBLL gn = new GeneralBLL();
        MaterialBLL mt = new MaterialBLL();
        MaterialBuyerBLL mtl = new MaterialBuyerBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        
        MTBUYER mtb;
        public Choseftymtb(MTBUYER mtb)
        {

            InitializeComponent();
            this.mtb = mtb;
            
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
           



        }

        private void cbb_工厂_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.mtb.cbb_计量单位.Enabled = false;
            this.mtb.cbb_资格.Enabled = false;
            this.mtb.cbb_物料组.Enabled = false;
            this.mtb.cbb_可变单位.Enabled = false;
            this.mtb.cbb_订单单位.Enabled = false;
            MaterialFactory mtfty = mtl.GetMtFtyInformation(mtb.cbb_物料编号.Text, cbb_工厂.Text);
            this.mtb.cbb_采购组.Text = mtfty.Buyer_Group;
            this.mtb.cbb_物料状态.Text = mtfty.Ptmtl_Status;
            this.mtb.cbb_税收状态.Text = mtfty.MTtax_Status;
            this.mtb.cbb_运输组.Text = mtfty.MTtransport_Group;
            this.mtb.ckb_源清单.Checked = mtfty.Source_List;
            //string sql3 = "SELECT * FROM [MT_FTY] WHERE Material_ID='" + this.mtb.cbb_物料编号.Text + "' AND Factory_ID='" + cbb_工厂.Text + "'";
            //DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            //try
            //{
            //    this.mtb.cbb_采购组.Text = dt3.Rows[0][5].ToString();
            //    this.mtb.cbb_物料状态.Text = dt3.Rows[0][6].ToString();
            //    this.mtb.cbb_税收状态.Text = dt3.Rows[0][7].ToString();
            //    this.mtb.cbb_运输组.Text = dt3.Rows[0][8].ToString();
            //    this.mtb.cbb_起始期.Text = dt3.Rows[0][9].ToString();
            //}
            //catch (Exception ex)
            //{
            //    this.mtb.cbb_采购组.Text = "";
            //    this.mtb.cbb_物料状态.Text = "";
            //    this.mtb.cbb_税收状态.Text = "";
            //    this.mtb.cbb_运输组.Text = "";
            //    this.mtb.cbb_起始期.Text = "";
            //}
            

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (cbb_工厂.Text != "")
            {
                this.mtb.cbb_工厂编码.Text = cbb_工厂.Text;
                string sql = "SELECT * FROM [Factory] WHERE Factory_ID='" + cbb_工厂.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.mtb.cbb_工厂名称.Text = dt.Rows[0][2].ToString();
                this.mtb.ckb_MPN.Enabled = false;
                this.mtb.ckb_批次管理.Enabled = false;
                this.Close();
            }
            else
            {
                this.mtb.cbb_工厂编码.Text = "";
                this.Close();

                this.mtb.cbb_采购组.Enabled = false;
                this.mtb.cbb_物料状态.Enabled = false;
                this.mtb.cbb_工厂编码.Enabled = false;
                this.mtb.cbb_工厂名称.Enabled = false;
                this.mtb.cbb_运输组.Enabled = false;
               // this.mtb.cbb_起始期.Enabled = false;
                this.mtb.cbb_税收状态.Enabled = false;
                this.mtb.cbb_采购组.Visible = false;
                this.mtb.cbb_物料状态.Visible = false;
                this.mtb.cbb_工厂编码.Visible = false;
                this.mtb.cbb_工厂名称.Visible = false;
                this.mtb.cbb_运输组.Visible = false;
               // this.mtb.cbb_起始期.Visible = false;
                this.mtb.cbb_税收状态.Visible = false;
                this.mtb.lb_采购组.Visible = false;
                this.mtb.lb_税收状态.Visible = false;
                this.mtb.lb_物料状态.Visible = false;
                this.mtb.lb_有效起始期.Visible = false;
                this.mtb.lb_运输组.Visible = false;
                this.mtb.gbx_采购值.Visible = false;




            }
        }
    }
}
