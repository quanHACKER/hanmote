﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.MD.MT
{
    public partial class MTBatchQY : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTBatchQY()
        {
            InitializeComponent();
        }

        private void btn_清除_Click(object sender, EventArgs e)
        {
            cbb_出货日期.Text = "";
            cbb_批次ID.Text = "";
            cbb_批次级别.Text = "";
            cbb_批次名称.Text = "";
            cbb_生产日期.Text = "";
            cbb_收货日期.Text = "";
            cbb_到期日期.Text = "";
            cbb_物料编码.Text = "";
        }

        private void btn_查询_Click(object sender, EventArgs e)
        {
            string dydate = cbb_出货日期.Text;
            string bhid = cbb_批次ID.Text;
            string bhgrade = cbb_批次级别.Text;
            string bhname = cbb_批次名称.Text;
            string pedate = cbb_生产日期.Text;
            string redate = cbb_收货日期.Text;
            string duedate = cbb_到期日期.Text;
            string mtid = cbb_物料编码.Text;
            string sql = "SELECT * FROM [MM].[dbo].[Batch] where Batch_ID='" + bhid + "' and  Batch_Name='" + bhname + "' and Batch_Grade='" + bhgrade + "' and  Material_ID='" + mtid + "' and Production_Date='" + pedate + "' and Due_Date='" + duedate + "' and Receipt_Date='" + redate + "' and Delivery_Date='" + dydate + "'";
            dgv_详细信息.DataSource = DBHelper.ExecuteQueryDT(sql);
        }

        private void dgv_详细信息_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = e.RowIndex;
            string s = "";
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow(); 
            for (int i = 0; i < dgv_详细信息.Columns.Count; i++)
            {
                DataColumn dataColumn = new DataColumn();
                dataColumn.DataType = typeof(string);//数据类型  
                dataColumn.ColumnName = "列名" + i.ToString();//列名  
                dt.Columns.Add(dataColumn);//列添加到tempTable  
                s = dgv_详细信息.Rows[rowindex].Cells[i].Value.ToString();
                dr["列名" + i.ToString()] = s;
            }
            dt.Rows.Add(dr);//行添加到tempTable 
        }

        
    }
}
