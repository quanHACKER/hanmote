﻿using Lib.SqlServerDAL;
using MMClient.MD.NewMMarketPrice.createRecordInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class AddPurRecordInfoForm :DockContent
    {
        private string supplierName;
        private string purName;
        private string mtGroupName;
        private string mtName;
        private string recordCode;
        private string factory;
        private string recordClass;
        private string supplierId;
        private saveAddInfoPUrRecordForm1 saveaddInfoPUrRecordForm1 = null;
        int pre = 0;
        public AddPurRecordInfoForm()
        {
            InitializeComponent();
            this.txtCode.Text = DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss");
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if(init())
            {
                if (this.saveaddInfoPUrRecordForm1 == null || this.saveaddInfoPUrRecordForm1.IsDisposed)
                {
                    this.saveaddInfoPUrRecordForm1 = new saveAddInfoPUrRecordForm1(supplierName, supplierId, purName, mtGroupName, mtName, recordCode, factory, recordClass);
                }
                saveaddInfoPUrRecordForm1.Show();
            }
            
        }


        private void suppllierName_Load(object sender, EventArgs e)
        {
            string sql_1 = "select  DISTINCT Supplier_ID as name from Supplier_Base";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);
            this.cbSupplierName.DataSource = dt_1;
            this.cbSupplierName.DisplayMember = "name";
            this.cbSupplierName.ValueMember = "name";
            dt_1 = null;
        }

        private void cbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql_1 = "select Supplier_Name from Supplier_Base where Supplier_ID = '" + this.cbSupplierName.Text.ToString() + "'";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);
            if (dt_1.Rows.Count > 0 && !String.IsNullOrEmpty(dt_1.Rows[0][0].ToString()))
            {
                this.supplierID.Text = dt_1.Rows[0][0].ToString();
            }
            dt_1 = null;
            string sql_2 = "select SelectedDep from SR_Info where SupplierId = '" + this.cbSupplierName.Text.ToString() + "'";
            DataTable dt_2 = DBHelper.ExecuteQueryDT(sql_2);
            if(dt_2.Rows.Count > 0 && !String.IsNullOrEmpty(dt_2.Rows[0][0].ToString()))
            {
                //采购组织
                this.txtPurName.Text = dt_2.Rows[0][0].ToString();
                //物料组
                string sql_3 = "select SelectedMType from SR_Info where SupplierId = '" + this.cbSupplierName.Text.ToString() + "'";
                DataTable dt_3 = DBHelper.ExecuteQueryDT(sql_3);
                if (dt_3.Rows.Count > 0 && !String.IsNullOrEmpty(dt_3.Rows[0][0].ToString()))
                {
                    this.txtGroupName.Text = dt_3.Rows[0][0].ToString();
                    //物料
                    string sql_4 = "select MtID as id  from Mt_MtGroup_Relationship where MtGroupName='" + this.txtGroupName.Text .ToString()+ "'";
                    DataTable dt_4 = DBHelper.ExecuteQueryDT(sql_4);
                    if (dt_4.Rows.Count > 0 && !String.IsNullOrEmpty(dt_4.Rows[0][0].ToString()))
                    {
                        this.comboBox1.DataSource = dt_4;
                        this.comboBox1.DisplayMember = "id";
                        this.comboBox1.ValueMember = "id";
                        pre = 1;
                    }
                    dt_4 = null;
                }
                dt_3 = null;
            }
            dt_2 = null;
        }
        private bool init()
        {
            //先检查
            recordCode = this.txtCode.Text.ToString();
            recordClass = this.cbClassInfo.Text.ToString();
            supplierName = this.cbSupplierName.Text.ToString();
            purName = this.txtPurName.Text.ToString();
            mtGroupName = this.txtGroupName.Text.ToString();
            mtName = this.comboBox1.Text.ToString();
            factory = this.txtFactory.Text.ToString();
            if(isCheck(recordCode))
            {
                string sql = "select SupplierId from SR_Info where CompanyName = '" + this.cbSupplierName.Text.ToString() + "'";
                DataTable dt_1 = DBHelper.ExecuteQueryDT(sql);
                if (dt_1.Rows.Count > 0 && !String.IsNullOrEmpty(dt_1.Rows[0][0].ToString()))
                {
                    this.supplierID.Text = dt_1.Rows[0][0].ToString();
                }
                dt_1 = null;
                supplierId = this.supplierID.Text.ToString();
                if (String.IsNullOrEmpty(mtName) || String.IsNullOrEmpty(supplierId) || String.IsNullOrEmpty(recordCode) || String.IsNullOrEmpty(recordClass) || String.IsNullOrEmpty(supplierName) ||
                    String.IsNullOrEmpty(purName) || String.IsNullOrEmpty(mtGroupName) || String.IsNullOrEmpty(factory))
                {
                    MessageBox.Show("信息不完整，无法进入下一步");
                    return false;
                }
            }
            else
            {
                MessageBox.Show("该记录已存在，如需继续添加强点击重置按钮");
                return false;
            }
            return true;
        }

        private bool isCheck(string recordCode)
        {
            string sql = "select * from  PurcharseInfoRecord where recordCode='"+recordCode+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt.Rows.Count>0)
            {
               
                return false;
            }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.txtCode.Text = DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss");
            MessageBox.Show("重置成功");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mtID = this.comboBox1.Text.ToString();
            if(!String.IsNullOrEmpty(mtID) && pre==1)
            {
                string sql = "select DISTINCT Material_Name from Material  where Material_ID  = '" + mtID+ "'";
                string name = DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
                this.txtMTID.Text = name;
               // pre = 0;
            }
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            comboBox1_SelectedIndexChanged(sender,e);
        }
    }
}
