﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    public partial class PurchasesRecordInfoViewForm : DockContent
    {
        private string supplierId;
        private string supplierName;
        private string recordClass;
        private string factory;
        private string recordCode;
        private string mtGroupName;
        private string purName;
        private string mtName;
        private modifyPurRecordInfoForm modify = null;
        int flag = 0;
        int global = 0;
        public PurchasesRecordInfoViewForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplierName_Load(object sender, EventArgs e)
        {
            string sql_1 = "select  DISTINCT Supplier_ID as name from Supplier_Base";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);
            this.cbSupplierName.DataSource = dt_1;
            this.cbSupplierName.DisplayMember = "name";
            this.cbSupplierName.ValueMember = "name";
            dt_1 = null;
        }
        /// <summary>
        /// 
        /// </summary>
        private void fillPurPriceTable()
        {
            this.PurPriceTable.DataSource = null;
            string time = "";
            if("30天".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 30 and datediff(dd,startTime,endTime) >= 14  ";
            }
            else if("60天".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 60 and datediff(dd,startTime,endTime) >= 30  ";
            }
            else if ("一周".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 7 and datediff(dd,startTime,endTime) >= 0  ";
            }
            else if ("两周".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 14 and datediff(dd,startTime,endTime) >= 7  ";
            }
            else if("90天".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 90 and datediff(dd,startTime,endTime) >= 60  ";
            }
            else if("120天".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 120 and datediff(dd,startTime,endTime) >= 90  ";
            }
            else if ("一年".Equals(cbtime.Text.ToString()))
            {
                time = " datediff(dd,startTime,endTime) <= 365 and datediff(dd,startTime,endTime) >= 90  ";
            }
            else 
            {
                time = " datediff(dd,startTime,endTime) > 365  ";
            }
            string sql = @"SELECT
	                            recordCode AS 信息记录ID,
	                            mtName AS 物料ID,
	                            PriceNum AS 金额,
	                            unit AS 单位,
                                CurrencyClass AS 币种,
                                CurrencyClass AS 过期,
                                endTime
                            FROM
	                            [PurcharseInfoRecord] where recordClass = '"+this.cbClassInfo .Text.ToString()+ "' and  delFlag = 0 and SupplierID = '" + this.cbSupplierName.Text.ToString() + "' and "+time;
            
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(flag == 1 )
            {
                if (!String.IsNullOrEmpty(this.txtsupplierID.Text.ToString()))
                {
                    if (dt.Rows.Count <= 0 || dt.Rows[0][0] == null)
                    {
                        MessageBox.Show("没有采购信息记录可查看");
                        return;
                    }
                }
                //  for (int i = 0; i < dt.Rows.Count; i++)
                // {
                //      DateTimeFormatInfo dtFormat = new DateTimeFormatInfo();
                //      dtFormat.ShortDatePattern = "yyyy/MM/dd";
                //      DateTime EndTime = Convert.ToDateTime(dt.Rows[i][6].ToString(), dtFormat);
                //      DateTime today = Convert.ToDateTime(DateTime.Now.ToLocalTime(),dtFormat);
                //       double outtime = ConvertToUnixOfTime(EndTime) - ConvertToUnixOfTime(today);
                //       if(outtime<0)
                //       {
                //           dt.Rows[i][6] = "已过期";
                //       }
                //       else
                //       {
                //         dt.Rows[i][6] = "未过期";
                //      }
                //   }
                this.PurPriceTable.AutoGenerateColumns = false;
                this.PurPriceTable.DataSource = dt;
                dt = null;
            }
            

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurPriceTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.PurPriceTable.Rows[currentIndex];
                String infoId = Convert.ToString(row.Cells["信息记录ID"].Value);
                String mtId = Convert.ToString(row.Cells["物料ID"].Value);
                String price = Convert.ToString(row.Cells["金额"].Value);
                //String recordClass = Convert.ToString(row.Cells["记录类型"].Value);
                String currencyClass = Convert.ToString(row.Cells["币种"].Value);
               // String outTime = Convert.ToString(row.Cells["过期"].Value);
                String countUnit = Convert.ToString(row.Cells["单位"].Value);
                string sql = "select recordCode,supplierName, supplierID,PurName, mtGroupName, mtName,recordClass, factoryID from PurcharseInfoRecord  where recordCode='" + infoId + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                try
                {
                    recordCode = dt.Rows[0][0].ToString();
                    supplierName = dt.Rows[0][1].ToString();
                    supplierId = dt.Rows[0][2].ToString();
                    purName = dt.Rows[0][3].ToString();
                    mtGroupName = dt.Rows[0][4].ToString();
                    mtName = dt.Rows[0][5].ToString();
                    recordClass = dt.Rows[0][6].ToString();
                    factory = dt.Rows[0][7].ToString();
                }
                catch
                {
                    //MessageBox.Show("暂无数据");
                    return;
                }
                if (this.PurPriceTable.Columns[e.ColumnIndex].Name == "修改")
                {
                    //修改窗体
                    DataTable dt_M = null;
                    string sql_M = @"SELECT
	                        texCode AS texCode,
	                        enterRule AS cbRule,
	                        discountClass AS cbDiscountClass,
	                        PriceNum  AS txtPrice,
	                        validationPrice AS txtValidatePrice,
	                        cashDiscount AS txtcashDiscount,
	                        CurrencyClass AS cbCurClass,
	                        unit AS cbUnit,
                            startTime,endTime
                        FROM
	                        PurcharseInfoRecord
                        WHERE
                        recordCode = '" + recordCode + "'";
                   
                    try
                    {
                         dt_M= DBHelper.ExecuteQueryDT(sql_M);
                        if (dt == null || dt.Rows.Count <= 0 || dt.Rows[0][0] == null)
                        {
                            MessageBox.Show("读取信息失败");
                        }
                    }
                    catch
                    {
                        MessageBox.Show("读取信息失败");
                    }
                    if (this.modify == null || this.modify.IsDisposed)
                    {
                        this.modify = new modifyPurRecordInfoForm(recordCode,dt_M);
                    }
                    modify.Show();
                    
                }
                if (this.PurPriceTable.Columns[e.ColumnIndex].Name == "删除")
                {
                    //逻辑删除信息
                    string del = "UPDATE  PurcharseInfoRecord  SET delFlag = 1 WHERE recordCode  = '" + infoId + "'";
                    try
                    {
                        MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                        DialogResult dr = MessageBox.Show("是否确定删除该条记录？", "警  告", messButton);
                        if (dr == DialogResult.OK)
                        {
                            DBHelper.ExecuteNonQuery(del);
                            MessageBox.Show("删除成功");
                            fillPurPriceTable();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("删除失败");
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.PurPriceTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.PurPriceTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.PurPriceTable.CurrentRow.Index;
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            fillPurPriceTable();
        }
        public  double ConvertToUnixOfTime(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        private void cbClassInfo_SelectedIndexChanged(object sender, EventArgs e)
        { 
            fillPurPriceTable();
        }

        private void cbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            this.txtsupplierID.Text = "";
            string s = "select Supplier_Name from Supplier_Base where Supplier_ID='" + this.cbSupplierName.Text.ToString() + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(s);
            try
            {
                this.txtsupplierID.Text = dt1.Rows[0][0].ToString();
            }
            catch
            {
                this.txtsupplierID.Text = "";
            }
            dt1 = null;
            string sql = "select SelectedDep from SR_Info where SupplierId='"+this.cbSupplierName.Text.ToString()+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count > 0)
            {
                purName = this.txtPurName.Text = dt.Rows[0][0].ToString();
            }
            dt = null;
            string sql2 = "select Material_Name as name from Material where Material_Group = (select Material_Group from Material_Group where Description  = (select SelectedMType from SR_Info where SupplierId='" + this.cbSupplierName.Text.ToString() + "'))";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            if (dt2.Rows.Count > 0)
            {
                this.cbmatrialName.DataSource = null;
                this.cbmatrialName.DataSource = dt2;
                this.cbmatrialName.DisplayMember = "name";
                this.cbmatrialName.ValueMember = "name";
            }
            dt2 = null;
        }

        private void cbmatrialName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillPurPriceTable();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            flag = 1;
            fillPurPriceTable();
        }
        
    }
}
