﻿using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    public partial class modifyPurRecordInfoForm : Form
    {
        private string recordCode;
        private DataTable dt = null;
        private GeneralBLL gn = new GeneralBLL();
        public modifyPurRecordInfoForm(string recordCode,DataTable dt)
        {
            InitializeComponent();
            this.recordCode = recordCode;
            this.txtRecordID.Text = recordCode;
            this.dt = dt;
            List<string> list1 = gn.GetAllMeasurement();
            cbUnit.DataSource = list1;
            initial();
        }

        private void initial()
        {
            texCode.Text = dt.Rows[0][0].ToString(); 
            cbRule.Text = dt.Rows[0][1].ToString();
            cbDiscountClass.Text = dt.Rows[0][2].ToString();
            txtPrice.Text = dt.Rows[0][3].ToString();
            txtValidatePrice.Text = dt.Rows[0][4].ToString();
            txtcashDiscount.Text = dt.Rows[0][5].ToString();
            cbCurClass.Text = dt.Rows[0][6].ToString();
            cbUnit.Text = dt.Rows[0][7].ToString();
            dateTimePicker1.Value = Convert.ToDateTime(dt.Rows[0][8].ToString());
            dateTimePicker2.Value = Convert.ToDateTime(dt.Rows[0][9].ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isNullOrEmpty())
            {
                MessageBox.Show("信息不完整或不合法");
                return ;
            }
            string update = @"UPDATE PurcharseInfoRecord
                                SET [texCode] = '" + texCode.Text.ToString() + @"',
                                 [enterRule] = '" + cbRule.Text.ToString() + @"',
                                 [discountClass] = '" + cbDiscountClass.Text.ToString() + @"',
                                 [PriceNum] = '" + txtPrice.Text.ToString() + @"',
                                 [validationPrice] = '" + txtValidatePrice.Text.ToString() + @"',
                                 [cashDiscount] = '" + txtcashDiscount.Text.ToString() + @"',
                                 [CurrencyClass] ='" + cbCurClass.Text.ToString() + @"',
                                 [unit] = '" + cbUnit.Text.ToString() + "' WHERE  recordCode='"+recordCode+"' ";
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show("是否确定修改该条记录？", "警  告", messButton);
            if (dr == DialogResult.OK)
            {
                DBHelper.ExecuteNonQuery(update);
                MessageBox.Show("修改成功");
                this.Close();
            }
        }
        private bool isNullOrEmpty()
        {
            if (String.IsNullOrEmpty(texCode.Text.ToString()) || String.IsNullOrEmpty(cbRule.Text.ToString()) || String.IsNullOrEmpty(cbDiscountClass.Text.ToString())
                || String.IsNullOrEmpty(txtPrice.Text.ToString()) || String.IsNullOrEmpty(txtValidatePrice.Text.ToString()) || String.IsNullOrEmpty(txtcashDiscount.Text.ToString())
                || String.IsNullOrEmpty(cbCurClass.Text.ToString()) || String.IsNullOrEmpty(cbUnit.Text.ToString()))
            {
                try
                {
                    float.Parse(txtPrice.Text.ToString());
                    float.Parse(txtValidatePrice.Text.ToString());
                }
                catch
                {
                    return true;
                }

                return true;
            }
            return false;
        }
    }
}
