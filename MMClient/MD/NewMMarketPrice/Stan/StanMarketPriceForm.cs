﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class StanMarketPriceForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private StanAddMarketPriceForm addMarketPriceForm = null;
        private ModifyMarketPriceForm modifyMarketPriceForm = null;
        private string recordtype;
        public StanMarketPriceForm()
        {
            InitializeComponent();
            this.stanMarketPriceTbl.DataSource = null;
            fillMarketPriceTbl();
        }
        private void fillMarketPriceTbl()
        {
            this.stanMarketPriceTbl.DataSource = null;
            string PurName = this.MtGroupNames.Text.ToString();
            string recordtype = this.txtBusType.Text.ToString();
            string sql = "";
            if ("全部".Equals(PurName) || "全部" == PurName)
            {
                sql = @"SELECT
                                PurGroupName AS 采购组织,
                                MtGroupName AS 物料组,
                                MtName AS 物料,
                                PriceUnit AS 货币,
                                PriceNum AS 净价,
                                countUnit AS 单位,
                                StartTime AS 开始时间,
                                EndTime AS 结束时间,
                                createTime AS 最近修改时间
                            FROM
                                New_MarketPrice
                            WHERE
                                BusType = '" + recordtype + "'";
            }
            else
            {
                sql = @"SELECT
                                PurGroupName AS 采购组织,
                                MtGroupName AS 物料组,
                                MtName AS 物料,
                                PriceUnit AS 货币,
                                PriceNum AS 净价,
                                countUnit AS 单位,
                                StartTime AS 开始时间,
                                EndTime AS 结束时间,
                                createTime AS 最近修改时间
                            FROM
                                New_MarketPrice
                            WHERE
                                PurGroupName = '" + PurName + "' and MtGroupName = '"+this.cbMtGroups.Text.ToString()+"' and BusType = '" + recordtype + "'";
            }
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.stanMarketPriceTbl.AutoGenerateColumns = false;
            this.stanMarketPriceTbl.DataSource = dt;

        }
        /// <summary>
        /// 初始化下来列表的值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurGroupName_Load(object sender, EventArgs e)
        {
            string sql = "select DISTINCT Porg_Name as name from Porg_MtGroup_Relationship";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            MtGroupNames.DataSource = dt;
            this.MtGroupNames.DisplayMember = "name";
            this.MtGroupNames.ValueMember = "name";
        }

        //添加市场价格记录按钮出发函数
        private void button2_Click(object sender, EventArgs e)
        {
            this.recordtype = this.txtBusType.Text.ToString();
            if (this.addMarketPriceForm == null || this.addMarketPriceForm.IsDisposed)
            {
                 this.addMarketPriceForm = new StanAddMarketPriceForm(recordtype);
                 addMarketPriceForm.Show();
            }
            this.addMarketPriceForm = null;
        }
        /// <summary>
        /// 采购组织
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MtGroupNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "select DISTINCT MtGroup_Name as name from Porg_MtGroup_Relationship where Porg_Name='"+this.MtGroupNames.Text.ToString()+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.cbMtGroups.DataSource = dt;
            this.cbMtGroups.DisplayMember = "name";
            this.cbMtGroups.ValueMember = "name";
            fillMarketPriceTbl();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.stanMarketPriceTbl.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.stanMarketPriceTbl.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.stanMarketPriceTbl.CurrentRow.Index;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MarketPriceTbl_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.stanMarketPriceTbl.Rows[currentIndex];
                String Purname = Convert.ToString(row.Cells["采购组织"].Value);
                String MtGroupname = Convert.ToString(row.Cells["物料组"].Value);
                String Mtname = Convert.ToString(row.Cells["物料"].Value);
                String Priceunit = Convert.ToString(row.Cells["货币"].Value);
                String Price = Convert.ToString(row.Cells["净价"].Value);
                String countUnit = Convert.ToString(row.Cells["单位"].Value);
                String Bustype = this.txtBusType.Text.ToString();
                String Starttime = Convert.ToString(row.Cells["开始时间"].Value);
                String endtime = Convert.ToString(row.Cells["结束时间"].Value);
                if (this.stanMarketPriceTbl.Columns[e.ColumnIndex].Name == "修改")
                {
                    //修改窗体
                    if (this.modifyMarketPriceForm == null || this.modifyMarketPriceForm.IsDisposed)
                    {
                        this.modifyMarketPriceForm = new ModifyMarketPriceForm(Purname, MtGroupname, Mtname,Bustype, Priceunit, Price,countUnit,Starttime, endtime);
                        modifyMarketPriceForm.Show();
                    }
                    this.modifyMarketPriceForm = null;
                }
                if (this.stanMarketPriceTbl.Columns[e.ColumnIndex].Name == "删除")
                {
                    //删除信息New_MarketPrice
                    string sql = "delete from New_MarketPrice  WHERE PurGroupName = '" + Purname + "' AND BusType = '" + Bustype + "' AND MtGroupName = '" + MtGroupname + "' AND MtName='"+Mtname+"'";
                    //删除信息MarketPrice
                   // string del = "delete ";
                    try
                    {
                        DBHelper.ExecuteNonQuery(sql);
                    }
                    catch
                    {
                        MessageBox.Show("删除失败");
                    }
                    MessageBox.Show("删除成功");
                    fillMarketPriceTbl();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("此按钮为系统外导入市场价格信息记录的预留接口");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fillMarketPriceTbl();
        }

        private void txtBusType_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillMarketPriceTbl();
        }

        private void cbMtGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillMarketPriceTbl();
        }
    }
}
