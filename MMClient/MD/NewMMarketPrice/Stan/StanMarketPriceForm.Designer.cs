﻿namespace MMClient.MD.NewMMarketPrice
{
    partial class StanMarketPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtBusType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbMtGroups = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.MtGroupNames = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stanMarketPriceTbl = new System.Windows.Forms.DataGridView();
            this.采购组织 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.净价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.货币 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.开始时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.结束时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.最近修改时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.修改 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.删除 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanMarketPriceTbl)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtBusType);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.cbMtGroups);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.MtGroupNames);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(1, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(990, 113);
            this.panel3.TabIndex = 1;
            // 
            // txtBusType
            // 
            this.txtBusType.FormattingEnabled = true;
            this.txtBusType.Items.AddRange(new object[] {
            "保准信息记录",
            "委托加工记录",
            "寄售信息记录",
            "管道信息记录"});
            this.txtBusType.Location = new System.Drawing.Point(107, 69);
            this.txtBusType.Name = "txtBusType";
            this.txtBusType.Size = new System.Drawing.Size(121, 20);
            this.txtBusType.TabIndex = 27;
            this.txtBusType.Text = "保准信息记录";
            this.txtBusType.SelectedIndexChanged += new System.EventHandler(this.txtBusType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 26;
            this.label7.Text = "记录类型";
            // 
            // cbMtGroups
            // 
            this.cbMtGroups.FormattingEnabled = true;
            this.cbMtGroups.Location = new System.Drawing.Point(321, 19);
            this.cbMtGroups.Name = "cbMtGroups";
            this.cbMtGroups.Size = new System.Drawing.Size(121, 20);
            this.cbMtGroups.TabIndex = 8;
            this.cbMtGroups.SelectedIndexChanged += new System.EventHandler(this.cbMtGroups_SelectedIndexChanged);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(259, 25);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "物料组";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(829, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "刷新列表";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(540, 60);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(155, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "导入市场价格记录";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(540, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(155, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "添加市场价格记录";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MtGroupNames
            // 
            this.MtGroupNames.FormattingEnabled = true;
            this.MtGroupNames.Location = new System.Drawing.Point(107, 19);
            this.MtGroupNames.Name = "MtGroupNames";
            this.MtGroupNames.Size = new System.Drawing.Size(121, 20);
            this.MtGroupNames.TabIndex = 1;
            this.MtGroupNames.SelectedIndexChanged += new System.EventHandler(this.MtGroupNames_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "采购组织";
            // 
            // stanMarketPriceTbl
            // 
            this.stanMarketPriceTbl.AllowUserToAddRows = false;
            this.stanMarketPriceTbl.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.stanMarketPriceTbl.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.stanMarketPriceTbl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.stanMarketPriceTbl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.stanMarketPriceTbl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stanMarketPriceTbl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.采购组织,
            this.物料,
            this.净价,
            this.货币,
            this.单位,
            this.开始时间,
            this.结束时间,
            this.最近修改时间,
            this.修改,
            this.删除});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.stanMarketPriceTbl.DefaultCellStyle = dataGridViewCellStyle4;
            this.stanMarketPriceTbl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.stanMarketPriceTbl.GridColor = System.Drawing.SystemColors.ControlLight;
            this.stanMarketPriceTbl.Location = new System.Drawing.Point(1, 121);
            this.stanMarketPriceTbl.Name = "stanMarketPriceTbl";
            this.stanMarketPriceTbl.RowTemplate.Height = 23;
            this.stanMarketPriceTbl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.stanMarketPriceTbl.Size = new System.Drawing.Size(990, 312);
            this.stanMarketPriceTbl.TabIndex = 6;
            this.stanMarketPriceTbl.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MarketPriceTbl_CellContentClick);
            // 
            // 采购组织
            // 
            this.采购组织.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.采购组织.DataPropertyName = "采购组织";
            this.采购组织.Frozen = true;
            this.采购组织.HeaderText = "采购组织";
            this.采购组织.Name = "采购组织";
            this.采购组织.Width = 120;
            // 
            // 物料
            // 
            this.物料.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.物料.DataPropertyName = "物料";
            this.物料.Frozen = true;
            this.物料.HeaderText = "物料";
            this.物料.Name = "物料";
            this.物料.Width = 157;
            // 
            // 净价
            // 
            this.净价.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.净价.DataPropertyName = "净价";
            this.净价.HeaderText = "净价";
            this.净价.Name = "净价";
            this.净价.Width = 80;
            // 
            // 货币
            // 
            this.货币.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.货币.DataPropertyName = "货币";
            this.货币.HeaderText = "货币";
            this.货币.Name = "货币";
            this.货币.Width = 70;
            // 
            // 单位
            // 
            this.单位.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.单位.DataPropertyName = "单位";
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            this.单位.Width = 70;
            // 
            // 开始时间
            // 
            this.开始时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.开始时间.DataPropertyName = "开始时间";
            this.开始时间.HeaderText = "开始时间";
            this.开始时间.Name = "开始时间";
            this.开始时间.Width = 110;
            // 
            // 结束时间
            // 
            this.结束时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.结束时间.DataPropertyName = "结束时间";
            this.结束时间.HeaderText = "结束时间";
            this.结束时间.Name = "结束时间";
            this.结束时间.Width = 110;
            // 
            // 最近修改时间
            // 
            this.最近修改时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.最近修改时间.DataPropertyName = "最近修改时间";
            this.最近修改时间.HeaderText = "最近修改时间";
            this.最近修改时间.Name = "最近修改时间";
            this.最近修改时间.Width = 110;
            // 
            // 修改
            // 
            this.修改.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "修改";
            this.修改.DefaultCellStyle = dataGridViewCellStyle2;
            this.修改.HeaderText = "修改";
            this.修改.Name = "修改";
            this.修改.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.修改.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.修改.Text = "修改";
            this.修改.Width = 60;
            // 
            // 删除
            // 
            this.删除.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = "删除";
            this.删除.DefaultCellStyle = dataGridViewCellStyle3;
            this.删除.HeaderText = "删除";
            this.删除.Name = "删除";
            this.删除.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.删除.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.删除.Text = "删除";
            this.删除.Width = 60;
            // 
            // StanMarketPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 458);
            this.Controls.Add(this.stanMarketPriceTbl);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "StanMarketPriceForm";
            this.Text = "维护市场价格-标准信息记录";
            this.Load += new System.EventHandler(this.PurGroupName_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanMarketPriceTbl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox MtGroupNames;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView stanMarketPriceTbl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbMtGroups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox txtBusType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn 采购组织;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料;
        private System.Windows.Forms.DataGridViewTextBoxColumn 净价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 货币;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 开始时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 结束时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 最近修改时间;
        private System.Windows.Forms.DataGridViewButtonColumn 修改;
        private System.Windows.Forms.DataGridViewButtonColumn 删除;
    }
}