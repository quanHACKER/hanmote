﻿namespace MMClient.Material_group_positioning.GoalAndAim
{
    partial class PrdMtGroupGoals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_Aim = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_Material = new System.Windows.Forms.DataGridView();
            this.CB_Prd = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgv_Indicator = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnImport = new System.Windows.Forms.Button();
            this.LB_prdName = new System.Windows.Forms.Label();
            this.mtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSaveGoals = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.supplierGoal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Aim)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Material)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Indicator)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnSaveGoals);
            this.groupBox1.Controls.Add(this.dgv_Aim);
            this.groupBox1.Location = new System.Drawing.Point(10, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(528, 716);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "供应目标";
            // 
            // dgv_Aim
            // 
            this.dgv_Aim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Aim.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Aim.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Aim.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_Aim.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Aim.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supplierGoal,
            this.goalText,
            this.goalScore});
            this.dgv_Aim.Location = new System.Drawing.Point(6, 20);
            this.dgv_Aim.Name = "dgv_Aim";
            this.dgv_Aim.RowTemplate.Height = 23;
            this.dgv_Aim.Size = new System.Drawing.Size(516, 611);
            this.dgv_Aim.TabIndex = 0;
            this.dgv_Aim.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Aim_CellMouseLeave);
            this.dgv_Aim.SelectionChanged += new System.EventHandler(this.dgv_Aim_SelectionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.dgv_Material);
            this.groupBox2.Location = new System.Drawing.Point(544, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(352, 716);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料";
            // 
            // dgv_Material
            // 
            this.dgv_Material.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Material.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Material.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Material.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_Material.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Material.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mtId,
            this.mtName});
            this.dgv_Material.Location = new System.Drawing.Point(6, 47);
            this.dgv_Material.Name = "dgv_Material";
            this.dgv_Material.RowTemplate.Height = 23;
            this.dgv_Material.Size = new System.Drawing.Size(340, 584);
            this.dgv_Material.TabIndex = 0;
            // 
            // CB_Prd
            // 
            this.CB_Prd.FormattingEnabled = true;
            this.CB_Prd.Items.AddRange(new object[] {
            "汽车",
            "轮船",
            "飞机",
            "火箭"});
            this.CB_Prd.Location = new System.Drawing.Point(73, 15);
            this.CB_Prd.Name = "CB_Prd";
            this.CB_Prd.Size = new System.Drawing.Size(140, 20);
            this.CB_Prd.TabIndex = 7;
            this.CB_Prd.SelectedIndexChanged += new System.EventHandler(this.CB_Prd_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "产品名称";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(457, 13);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 8;
            this.btn_Save.Text = "保存";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.dgv_Indicator);
            this.groupBox3.Location = new System.Drawing.Point(902, 50);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(638, 716);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "供应指标";
            // 
            // dgv_Indicator
            // 
            this.dgv_Indicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Indicator.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Indicator.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Indicator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_Indicator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Indicator.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgv_Indicator.Location = new System.Drawing.Point(6, 20);
            this.dgv_Indicator.Name = "dgv_Indicator";
            this.dgv_Indicator.RowTemplate.Height = 23;
            this.dgv_Indicator.Size = new System.Drawing.Size(611, 611);
            this.dgv_Indicator.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "供应指标";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "分数";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(569, 13);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 9;
            this.btnImport.Text = "导入";
            this.btnImport.UseVisualStyleBackColor = true;
            // 
            // LB_prdName
            // 
            this.LB_prdName.AutoSize = true;
            this.LB_prdName.Location = new System.Drawing.Point(231, 18);
            this.LB_prdName.Name = "LB_prdName";
            this.LB_prdName.Size = new System.Drawing.Size(41, 12);
            this.LB_prdName.TabIndex = 10;
            this.LB_prdName.Text = "label2";
            // 
            // mtId
            // 
            this.mtId.DataPropertyName = "Material_ID";
            this.mtId.HeaderText = "物料编号";
            this.mtId.Name = "mtId";
            this.mtId.ReadOnly = true;
            // 
            // mtName
            // 
            this.mtName.DataPropertyName = "Material_Name";
            this.mtName.HeaderText = "物料名称";
            this.mtName.Name = "mtName";
            this.mtName.ReadOnly = true;
            // 
            // btnSaveGoals
            // 
            this.btnSaveGoals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveGoals.Location = new System.Drawing.Point(409, 683);
            this.btnSaveGoals.Name = "btnSaveGoals";
            this.btnSaveGoals.Size = new System.Drawing.Size(75, 23);
            this.btnSaveGoals.TabIndex = 1;
            this.btnSaveGoals.Text = "保存";
            this.btnSaveGoals.UseVisualStyleBackColor = true;
            this.btnSaveGoals.Click += new System.EventHandler(this.btnSaveGoals_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(147, 21);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "物料组";
            // 
            // supplierGoal
            // 
            this.supplierGoal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.supplierGoal.FillWeight = 60.571F;
            this.supplierGoal.HeaderText = "供应目标编号";
            this.supplierGoal.Name = "supplierGoal";
            this.supplierGoal.Width = 110;
            // 
            // goalText
            // 
            this.goalText.FillWeight = 47.90615F;
            this.goalText.HeaderText = "供应目标";
            this.goalText.Name = "goalText";
            // 
            // goalScore
            // 
            this.goalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.goalScore.HeaderText = "供应目标分数";
            this.goalScore.Name = "goalScore";
            this.goalScore.Width = 72;
            // 
            // PrdMtGroupGoals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1552, 768);
            this.Controls.Add(this.LB_prdName);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.CB_Prd);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "PrdMtGroupGoals";
            this.Text = "生产性物料";
            this.Load += new System.EventHandler(this.PrdMtGroupGoals_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Aim)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Material)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Indicator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_Aim;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_Material;
        private System.Windows.Forms.ComboBox CB_Prd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgv_Indicator;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label LB_prdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtName;
        private System.Windows.Forms.Button btnSaveGoals;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierGoal;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalText;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalScore;
    }
}