﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class PrdMtGroupGoals : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        GoalAndAimBll goalAndAimBll = new GoalAndAimBll();

        public PrdMtGroupGoals()
        {
            InitializeComponent();
        }
        //初始化数据
        private void PrdMtGroupGoals_Load(object sender, EventArgs e)
        {
            //获取产品信息
            CB_Prd.DataSource = goalAndAimBll.getPrdId();
            //加载目标信息
           // LoadprdData(CB_Prd.Text);
        }
        //下拉框数据改变，列表数据改变
        private void CB_Prd_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载产品名称
            LB_prdName.Text = goalAndAimBll.getPrdName(CB_Prd.Text);
            //获取组成产品的物料信息
            dgv_Material.DataSource = goalAndAimBll.getMaterialInfo(CB_Prd.Text);
        }

        //读取产品信息
        private void LoadprdData(string prdId) {



        }
        //保存输入数据
        private void btn_Save_Click(object sender, EventArgs e)
        {

        }
        //选择事件
        private void dgv_Aim_SelectionChanged(object sender, EventArgs e)
        {
             


        }
        //鼠标离开事件
        private void dgv_Aim_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
      
        }

        private void btnSaveGoals_Click(object sender, EventArgs e)
        {
            prdGoalModel prdGoalModel = new prdGoalModel();
            prdGoalModel.PrdId = CB_Prd.Text.Trim();
            for (int i = 0; i < dgv_Aim.Rows.Count - 1; i++) {
        
                prdGoalModel.GoalId = dgv_Aim.Rows[i].Cells["supplierGoal"].Value.ToString().Trim();
                prdGoalModel.GoalText = dgv_Aim.Rows[i].Cells["goalText"].Value.ToString().Trim();
                prdGoalModel.GoalSocre = dgv_Aim.Rows[i].Cells["goalScore"].Value.ToString().Trim();
                //保存供应目标信息
                try
                {
                    goalAndAimBll.insertGoalInfo(prdGoalModel);
                }
                catch (BllException ex)
                {
                    MessageUtil.ShowTips("保存失败！"+ex.Message);
                }

            }

           


        }
    }
}
