﻿namespace MMClient.Material_group_positioning
{
    partial class AddNewAttribute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B1 = new System.Windows.Forms.Button();
            this.Z1 = new System.Windows.Forms.RichTextBox();
            this.L11 = new System.Windows.Forms.Label();
            this.G1 = new System.Windows.Forms.RichTextBox();
            this.L1 = new System.Windows.Forms.Label();
            this.code_MtGroup = new System.Windows.Forms.ComboBox();
            this.name_MtGroup = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.B2 = new System.Windows.Forms.Button();
            this.Z2 = new System.Windows.Forms.RichTextBox();
            this.L12 = new System.Windows.Forms.Label();
            this.G2 = new System.Windows.Forms.RichTextBox();
            this.L2 = new System.Windows.Forms.Label();
            this.B3 = new System.Windows.Forms.Button();
            this.Z3 = new System.Windows.Forms.RichTextBox();
            this.L13 = new System.Windows.Forms.Label();
            this.G3 = new System.Windows.Forms.RichTextBox();
            this.L3 = new System.Windows.Forms.Label();
            this.B4 = new System.Windows.Forms.Button();
            this.Z4 = new System.Windows.Forms.RichTextBox();
            this.L14 = new System.Windows.Forms.Label();
            this.G4 = new System.Windows.Forms.RichTextBox();
            this.L4 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox21 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ZH1 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.GO1 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ZH2 = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.GO2 = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ZH3 = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.GO3 = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ZH4 = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.GO4 = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // B1
            // 
            this.B1.Location = new System.Drawing.Point(282, 162);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(75, 23);
            this.B1.TabIndex = 36;
            this.B1.Text = "添加";
            this.B1.UseVisualStyleBackColor = true;
            this.B1.Visible = false;
            this.B1.Click += new System.EventHandler(this.B1_Click);
            // 
            // Z1
            // 
            this.Z1.Location = new System.Drawing.Point(394, 116);
            this.Z1.Name = "Z1";
            this.Z1.Size = new System.Drawing.Size(160, 40);
            this.Z1.TabIndex = 35;
            this.Z1.Text = "";
            this.Z1.Visible = false;
            // 
            // L11
            // 
            this.L11.AutoSize = true;
            this.L11.Location = new System.Drawing.Point(358, 119);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(29, 12);
            this.L11.TabIndex = 34;
            this.L11.Text = "指标";
            this.L11.Visible = false;
            // 
            // G1
            // 
            this.G1.Location = new System.Drawing.Point(128, 119);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(160, 37);
            this.G1.TabIndex = 33;
            this.G1.Text = "";
            this.G1.Visible = false;
            // 
            // L1
            // 
            this.L1.AutoSize = true;
            this.L1.Location = new System.Drawing.Point(44, 119);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(53, 12);
            this.L1.TabIndex = 32;
            this.L1.Text = "供应目标";
            this.L1.Visible = false;
            // 
            // code_MtGroup
            // 
            this.code_MtGroup.FormattingEnabled = true;
            this.code_MtGroup.Location = new System.Drawing.Point(128, 46);
            this.code_MtGroup.Name = "code_MtGroup";
            this.code_MtGroup.Size = new System.Drawing.Size(100, 20);
            this.code_MtGroup.TabIndex = 31;
            this.code_MtGroup.SelectedIndexChanged += new System.EventHandler(this.code_MtGroup_SelectedIndexChanged);
            // 
            // name_MtGroup
            // 
            this.name_MtGroup.Location = new System.Drawing.Point(128, 81);
            this.name_MtGroup.Name = "name_MtGroup";
            this.name_MtGroup.ReadOnly = true;
            this.name_MtGroup.Size = new System.Drawing.Size(100, 21);
            this.name_MtGroup.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 28;
            this.label2.Text = "物料组编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 46);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "物料组名称";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(420, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // B2
            // 
            this.B2.Location = new System.Drawing.Point(282, 233);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(75, 23);
            this.B2.TabIndex = 42;
            this.B2.Text = "添加";
            this.B2.UseVisualStyleBackColor = true;
            this.B2.Visible = false;
            this.B2.Click += new System.EventHandler(this.B2_Click);
            // 
            // Z2
            // 
            this.Z2.Location = new System.Drawing.Point(394, 187);
            this.Z2.Name = "Z2";
            this.Z2.Size = new System.Drawing.Size(160, 40);
            this.Z2.TabIndex = 41;
            this.Z2.Text = "";
            this.Z2.Visible = false;
            // 
            // L12
            // 
            this.L12.AutoSize = true;
            this.L12.Location = new System.Drawing.Point(358, 187);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(29, 12);
            this.L12.TabIndex = 40;
            this.L12.Text = "指标";
            this.L12.Visible = false;
            this.L12.Click += new System.EventHandler(this.label3_Click);
            // 
            // G2
            // 
            this.G2.Location = new System.Drawing.Point(128, 190);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(160, 37);
            this.G2.TabIndex = 39;
            this.G2.Text = "";
            this.G2.Visible = false;
            // 
            // L2
            // 
            this.L2.AutoSize = true;
            this.L2.Location = new System.Drawing.Point(44, 190);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(53, 12);
            this.L2.TabIndex = 38;
            this.L2.Text = "供应目标";
            this.L2.Visible = false;
            // 
            // B3
            // 
            this.B3.Location = new System.Drawing.Point(282, 313);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(75, 23);
            this.B3.TabIndex = 47;
            this.B3.Text = "添加";
            this.B3.UseVisualStyleBackColor = true;
            this.B3.Visible = false;
            this.B3.Click += new System.EventHandler(this.B3_Click);
            // 
            // Z3
            // 
            this.Z3.Location = new System.Drawing.Point(394, 267);
            this.Z3.Name = "Z3";
            this.Z3.Size = new System.Drawing.Size(160, 40);
            this.Z3.TabIndex = 46;
            this.Z3.Text = "";
            this.Z3.Visible = false;
            // 
            // L13
            // 
            this.L13.AutoSize = true;
            this.L13.Location = new System.Drawing.Point(358, 267);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(29, 12);
            this.L13.TabIndex = 45;
            this.L13.Text = "指标";
            this.L13.Visible = false;
            // 
            // G3
            // 
            this.G3.Location = new System.Drawing.Point(128, 270);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(160, 37);
            this.G3.TabIndex = 44;
            this.G3.Text = "";
            this.G3.Visible = false;
            // 
            // L3
            // 
            this.L3.AutoSize = true;
            this.L3.Location = new System.Drawing.Point(44, 270);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(53, 12);
            this.L3.TabIndex = 43;
            this.L3.Text = "供应目标";
            this.L3.Visible = false;
            // 
            // B4
            // 
            this.B4.Location = new System.Drawing.Point(282, 385);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(75, 23);
            this.B4.TabIndex = 52;
            this.B4.Text = "添加";
            this.B4.UseVisualStyleBackColor = true;
            this.B4.Visible = false;
            // 
            // Z4
            // 
            this.Z4.Location = new System.Drawing.Point(394, 339);
            this.Z4.Name = "Z4";
            this.Z4.Size = new System.Drawing.Size(160, 40);
            this.Z4.TabIndex = 51;
            this.Z4.Text = "";
            this.Z4.Visible = false;
            // 
            // L14
            // 
            this.L14.AutoSize = true;
            this.L14.Location = new System.Drawing.Point(358, 339);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(29, 12);
            this.L14.TabIndex = 50;
            this.L14.Text = "指标";
            this.L14.Visible = false;
            // 
            // G4
            // 
            this.G4.Location = new System.Drawing.Point(128, 342);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(160, 37);
            this.G4.TabIndex = 49;
            this.G4.Text = "";
            this.G4.Visible = false;
            // 
            // L4
            // 
            this.L4.AutoSize = true;
            this.L4.Location = new System.Drawing.Point(44, 342);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(53, 12);
            this.L4.TabIndex = 48;
            this.L4.Text = "供应目标";
            this.L4.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(128, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 54;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 53;
            this.label3.Text = "采购组织名称";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(578, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(86, 21);
            this.textBox1.TabIndex = 56;
            this.textBox1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(478, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 55;
            this.label4.Text = "采购组织编号";
            this.label4.Visible = false;
            // 
            // comboBox21
            // 
            this.comboBox21.FormattingEnabled = true;
            this.comboBox21.Location = new System.Drawing.Point(346, 82);
            this.comboBox21.Name = "comboBox21";
            this.comboBox21.Size = new System.Drawing.Size(100, 20);
            this.comboBox21.TabIndex = 57;
            this.comboBox21.SelectedIndexChanged += new System.EventHandler(this.comboBox21_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(275, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 58;
            this.label5.Text = "模板选择";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // ZH1
            // 
            this.ZH1.Location = new System.Drawing.Point(394, 116);
            this.ZH1.Name = "ZH1";
            this.ZH1.Size = new System.Drawing.Size(160, 40);
            this.ZH1.TabIndex = 62;
            this.ZH1.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(358, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 61;
            this.label6.Text = "指标";
            // 
            // GO1
            // 
            this.GO1.Location = new System.Drawing.Point(128, 119);
            this.GO1.Name = "GO1";
            this.GO1.Size = new System.Drawing.Size(160, 37);
            this.GO1.TabIndex = 60;
            this.GO1.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 59;
            this.label7.Text = "供应目标";
            // 
            // ZH2
            // 
            this.ZH2.Location = new System.Drawing.Point(394, 184);
            this.ZH2.Name = "ZH2";
            this.ZH2.Size = new System.Drawing.Size(160, 40);
            this.ZH2.TabIndex = 66;
            this.ZH2.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(358, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 65;
            this.label8.Text = "指标";
            // 
            // GO2
            // 
            this.GO2.Location = new System.Drawing.Point(128, 187);
            this.GO2.Name = "GO2";
            this.GO2.Size = new System.Drawing.Size(160, 37);
            this.GO2.TabIndex = 64;
            this.GO2.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 63;
            this.label9.Text = "供应目标";
            // 
            // ZH3
            // 
            this.ZH3.Location = new System.Drawing.Point(394, 267);
            this.ZH3.Name = "ZH3";
            this.ZH3.Size = new System.Drawing.Size(160, 40);
            this.ZH3.TabIndex = 70;
            this.ZH3.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(358, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 69;
            this.label10.Text = "指标";
            // 
            // GO3
            // 
            this.GO3.Location = new System.Drawing.Point(128, 270);
            this.GO3.Name = "GO3";
            this.GO3.Size = new System.Drawing.Size(160, 37);
            this.GO3.TabIndex = 68;
            this.GO3.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 67;
            this.label11.Text = "供应目标";
            // 
            // ZH4
            // 
            this.ZH4.Location = new System.Drawing.Point(393, 336);
            this.ZH4.Name = "ZH4";
            this.ZH4.Size = new System.Drawing.Size(160, 40);
            this.ZH4.TabIndex = 74;
            this.ZH4.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(357, 336);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 73;
            this.label12.Text = "指标";
            // 
            // GO4
            // 
            this.GO4.Location = new System.Drawing.Point(127, 339);
            this.GO4.Name = "GO4";
            this.GO4.Size = new System.Drawing.Size(160, 37);
            this.GO4.TabIndex = 72;
            this.GO4.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(43, 339);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 71;
            this.label13.Text = "供应目标";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(277, 43);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 23);
            this.button2.TabIndex = 75;
            this.button2.Text = "保存为模板并保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(364, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 77;
            this.textBox2.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(257, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 12);
            this.label14.TabIndex = 76;
            this.label14.Text = "新保存的模板名称";
            this.label14.Visible = false;
            // 
            // AddNewAttribute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(664, 424);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ZH4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.GO4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ZH3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.GO3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ZH2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.GO2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ZH1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.GO1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox21);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.Z4);
            this.Controls.Add(this.L14);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.L4);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.Z3);
            this.Controls.Add(this.L13);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.L3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.Z2);
            this.Controls.Add(this.L12);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.L2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.Z1);
            this.Controls.Add(this.L11);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.code_MtGroup);
            this.Controls.Add(this.name_MtGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddNewAttribute";
            this.Text = "添加";
            this.Load += new System.EventHandler(this.AddNewAttribute_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button B1;
        private System.Windows.Forms.RichTextBox Z1;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.RichTextBox G1;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.ComboBox code_MtGroup;
        private System.Windows.Forms.TextBox name_MtGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button B2;
        private System.Windows.Forms.RichTextBox Z2;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.RichTextBox G2;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Button B3;
        private System.Windows.Forms.RichTextBox Z3;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.RichTextBox G3;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Button B4;
        private System.Windows.Forms.RichTextBox Z4;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.RichTextBox G4;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox ZH1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox GO1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox ZH2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox GO2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox ZH3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox GO3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox ZH4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox GO4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label14;
    }
}