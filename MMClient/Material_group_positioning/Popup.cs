﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using MMClient.Material_group_positioning;
using Lib.SqlServerDAL;


namespace MMClient.Material_group_positioning
{
    public partial class Popup : Form
    {
        private string area1;
        private string aim1;
            private string goal1=null;

        public Popup(string area, string aim,string goal)
        {
            InitializeComponent();
            this.area1 = area;
            this.aim1 = aim;
            this.goal1 = goal;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//保存
        {
            string name = textBox1.Text;
            GeneralBLL gn = new GeneralBLL();
            //gn.SaveModel(name,aim1,goal1);
            gn.SaveModel(name, area1, aim1, goal1);
            MessageUtil.ShowWarning("保存成功！");
            
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)//取消
        {
            this.Close();
        }

        private void Popup_Load(object sender, EventArgs e)
        {

        }
    }
}
