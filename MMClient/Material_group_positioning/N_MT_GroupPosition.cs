﻿using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.Material_group_positioning
{
    public partial class N_MT_GroupPosition : DockContent
    {
        Boolean complete = false;
        //物料组ID
        string id = "";
        //物料组名称
        string name = "";
        //采购组名称
        string purOrgName = "";
        //定位分数
        double resultPosScore = 0;
        //定位等级
        String influenceScore = "";
        String riskScore = "";
        String resultLevel = "";
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        MtItemScoreInfoModle mtItemScoreInfoModle = new MtItemScoreInfoModle();
        public N_MT_GroupPosition(String id, String name, String purOrgName)
        {
            InitializeComponent();
            this.id = id;
            this.name = name;
            this.purOrgName = purOrgName;
        }


        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void N_MT_GroupPosition_Load(object sender, EventArgs e)
        {
            label5.Location = new Point(label2.Location.X + 80, label2.Location.Y - 80);
            label6.Location = new Point(label5.Location.X + 80, label5.Location.Y - 80);
            label7.Location = new Point(label6.Location.X + 80, label6.Location.Y - 80);
            label2.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
            label6.BackColor = Color.Transparent;
            label7.BackColor = Color.Transparent;
            dataGridView1.DataSource = null;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = n_RiskAssessmentBll.getMtGroupData(id, purOrgName);
            this.mtName.Text = name;
            this.mtID.Text = id;
            this.purchOrgName.Text = purOrgName;
            double[] r = { 2, 8, 18 };
            DrawCircl(r.Length, this.chart1, r);
            initalChart();//初始化图表
            getTotalScoreInfo();//获取评分信息
            complete = true;
        }



        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow r = this.dataGridView1.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.dataGridView1.Refresh();
        }
        /// <summary>
        /// 刷新按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void evaluationBtn_Click(object sender, EventArgs e)
        {
            Series series1 = this.chart1.Series["无影响"];
            Series series2 = this.chart1.Series["低影响"];
            Series series3 = this.chart1.Series["中影响"];
            this.chart1.Series.Clear();

            chart1.Series.Add(series1);
            chart1.Series.Add(series2);
            chart1.Series.Add(series3);
            N_MT_GroupPosition_Load(sender,e);

        }
        /// <summary>
        /// 获取评分信息
        /// </summary>
        public void getTotalScoreInfo()
        {
            //加载图表
            chart_Load();
            //查询评估结果

            DataTable dt = n_RiskAssessmentBll.getMtGroupResultData(id, this.purchOrgName.Text);
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowWarning("请先评估！");
                return;

            }
            //平均值开平方
            resultPosScore = Math.Sqrt((Math.Pow(float.Parse(dt.Rows[0]["AimsScore"].ToString()), 2) + Math.Pow(float.Parse(dt.Rows[0]["RisKScore"].ToString()), 2)) / 2);
            if (resultPosScore < 1)
            {
                resultLevel = "N";
            }
            else if (resultPosScore < 2)
            {
                resultLevel = "L";
            }
            else if (resultPosScore < 3)
            {
                resultLevel = "M";
            }
            else
            {
                resultLevel = "H";
            }

            resultPosScore = Math.Round(resultPosScore, 2);

            this.result.Text = "影响力最终得分:" + dt.Rows[0]["AimsScore"] + "\n\n供应风险最终得分：" + dt.Rows[0]["RisKScore"] + "\n\n影响力等级:" + dt.Rows[0]["aimLevel"] + "\n\n风险等级：" + dt.Rows[0]["RiskLevel"] + "\n\n综合得分：" + resultPosScore + "\n\n物料定位等级：" + resultLevel;
            influenceScore = dt.Rows[0]["AimsScore"].ToString();
            riskScore = dt.Rows[0]["RisKScore"].ToString();

        }




        /// <summary>
        /// 加载图表
        /// </summary>
        private void chart_Load()
        {

            List<double> listY = new List<double>();
            List<double> listX = new List<double>();

            double r = 0;
            DataTable dt = n_RiskAssessmentBll.getMtGroupFinishData(id, purOrgName);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Series series = this.chart1.Series[dt.Rows[i]["Aims"].ToString()];
                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["AimsScore"].ToString().Equals(""))
                {

                    listY.Add(float.Parse(dt.Rows[i]["AimsScore"].ToString()));
                    listX.Add(float.Parse(dt.Rows[i]["RiskScore"].ToString()));
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }
                //设置颜色

                if (r < 2)
                {
                    series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                }
                else if (r < 8)
                {
                    series.MarkerColor = System.Drawing.Color.Orange;
                }
                else if (r < 18)
                { series.MarkerColor = System.Drawing.Color.Red; }
                else
                { series.MarkerColor = System.Drawing.Color.Black; }
                //绑定数据
                series.Points.DataBindXY(listX, listY);
            }

        }

        /// <summary>
        /// 绘制参考线
        /// </summary>
        /// <param name="n"></param>
        /// <param name="chart"></param>
        /// <param name="r"></param>
        private void DrawCircl(int n, Chart chart, double[] r)
        {
            List<Double> y1 = new List<Double>();
            List<Double> x1 = new List<Double>();

            for (int m = 0; m < n; m++)
            {
                for (double i = 0.00; i <= r[m]; i = i + 0.01)
                {
                    for (double j = 0.00; j <= r[m]; j = j + 0.01)
                    {
                        i = Math.Round(i, 2);
                        j = Math.Round(j, 2);
                        if (r[m] - (i * i + j * j) <= 10e-5)
                        {
                            x1.Add(i);
                            y1.Add(j);
                            break;

                        }


                    }


                }
                this.chart1.Series[m].Points.DataBindXY(x1, y1);
                x1.Clear();
                y1.Clear();


            }


        }
        /// <summary>
        /// 打分界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int CIndex = e.ColumnIndex;
            if (CIndex == 9)
            {
                string aim = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
                string gloab = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.Aim = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
                mtItemScoreInfoModle.Global = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.InfluenLevel = dataGridView1.CurrentRow.Cells["影响力PIP等级"].Value.ToString();
                mtItemScoreInfoModle.InfluenScore = dataGridView1.CurrentRow.Cells["影响力得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskLevel = dataGridView1.CurrentRow.Cells["供应风险等级"].Value.ToString();
                mtItemScoreInfoModle.RiskScore = dataGridView1.CurrentRow.Cells["供应风险得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskText = dataGridView1.CurrentRow.Cells["风险"].Value.ToString().Trim();
                mtItemScoreInfoModle.Id = id;

                if (mtItemScoreInfoModle.InfluenScore.Equals("待评估") || mtItemScoreInfoModle.InfluenScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("待评估"))
                {
                    MessageUtil.ShowError("请输入分数信息");
                    return;
                }
                if (mtItemScoreInfoModle.RiskText.Equals(""))
                {
                    MessageUtil.ShowError("请输入风险！");
                    return;
                }

                //插入分数信息
                Boolean flag = n_RiskAssessmentBll.updateSingleMtSubScoreInfo(mtItemScoreInfoModle, purOrgName);
                if (flag)
                {
                    MessageUtil.ShowTips("评估完成！");
                    dataGridView1.DataSource = n_RiskAssessmentBll.getMtGroupData(id, purOrgName);

                }
                else
                {
                    MessageUtil.ShowError("评估失败");
                }


            }



        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            if (!complete)
            {
                return;
            }

            string level = "";
            int Cindex = e.ColumnIndex;
            string scoreStr = "";
            string levelStr = "";
            if (Cindex == 4)
            {
                scoreStr = "影响力得分";
                levelStr = "影响力PIP等级";

            }
            else if (Cindex == 7)
            {
                scoreStr = "供应风险得分";
                levelStr = "供应风险等级";
            }
            else
            {

                return;
            }




            level = "";


            if (dataGridView1.CurrentRow.Cells[scoreStr].Value.Equals("待评估") || dataGridView1.CurrentRow.Cells[scoreStr].Value.Equals(""))
            {
                return;
            }

            try
            {
                float score = float.Parse(dataGridView1.CurrentRow.Cells[scoreStr].Value.ToString());
                if (score > 4 || score < 0)
                {
                    MessageUtil.ShowError("打分范围为：0-4");
                    dataGridView1.CurrentRow.Cells[scoreStr].Value = "";
                    return;

                }
                if (score >= 0 && score < 1)
                {
                    level = "N";
                }
                else if (score < 2)
                {
                    level = "L";
                }
                else if (score < 3)
                {
                    level = "M";
                }
                else
                {
                    level = "H";
                }
                dataGridView1.CurrentRow.Cells[levelStr].Value = level;

            }
            catch (Exception)
            {
                MessageUtil.ShowError("打分只能为数字！");
            }


        }




        private void initalChart()
        {
            List<float> listY = new List<float>();
            List<float> listX = new List<float>();
            double r = 0;
            DataTable dt = n_RiskAssessmentBll.getMtGroupFinishData(id, purOrgName);


            for (int i = 0; i < dt.Rows.Count; i++)
            {

                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["AimsScore"].ToString().Equals(""))
                {

                    listY.Add(float.Parse(dt.Rows[i]["AimsScore"].ToString()));
                    listX.Add(float.Parse(dt.Rows[i]["RiskScore"].ToString()));
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }


                //求欧式距离


                Series series = new Series();
                series.ChartArea = "ChartArea1";
                series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bubble;
                series.IsValueShownAsLabel = false;
                //series.Label = dt.Rows[i]["Aims"].ToString();
                series.LabelToolTip = dt.Rows[i]["Aims"].ToString();
                series.Legend = "Legend1";

                if (dt.Rows[i]["Aims"].ToString().Length > 4)
                {
                    series.LegendText = dt.Rows[i]["Aims"].ToString().Substring(0, 4) + "..";
                }
                series.LegendToolTip = dt.Rows[i]["Aims"].ToString();
                //设置颜色

                if (r < 2)
                {
                    series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                }
                else if (r < 8)
                {
                    series.MarkerColor = System.Drawing.Color.Orange;
                }
                else if (r < 18)
                { series.MarkerColor = System.Drawing.Color.Red; }
                else
                { series.MarkerColor = System.Drawing.Color.Black; }

                series.MarkerSize = 10;
                series.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                series.Name = dt.Rows[i]["Aims"].ToString();
                series.ToolTip = dt.Rows[i]["Aims"].ToString();
                series.ToolTip = "指标：#SERIESNAME\n影响力指数：#VALY\n供应风险：#VALX";
                series.YValuesPerPoint = 2;
                series.Points.DataBindXY(listX, listY);

                try
                {
                    this.chart1.Series.Add(series);

                }
                catch (Exception e)
                {
                    MessageUtil.ShowError("有重复的数据，请正确定义评估项！");
                }

            }






        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //将评估结果写入数据库

        private void button1_Click(object sender, EventArgs e)
        {

            int flag = n_RiskAssessmentBll.insertIntoPosResInfo(resultPosScore, resultLevel, id, purOrgName, riskScore, influenceScore, mtName.Text);
            string info = "保存失败！";
            if (flag > 0)
            {
                info = "保存成功！";
            }

            MessageUtil.ShowTips(info);


        }

        private void mutEvlBtn_Click(object sender, EventArgs e)
        {
            int n = 0;
            int count = this.dataGridView1.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                string aim = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString();
                string gloab = dataGridView1.Rows[i].Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.Aim = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString();
                mtItemScoreInfoModle.Global = dataGridView1.Rows[i].Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.InfluenLevel = dataGridView1.Rows[i].Cells["影响力PIP等级"].Value.ToString();
                mtItemScoreInfoModle.InfluenScore = dataGridView1.Rows[i].Cells["影响力得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskLevel = dataGridView1.Rows[i].Cells["供应风险等级"].Value.ToString();
                mtItemScoreInfoModle.RiskScore = dataGridView1.Rows[i].Cells["供应风险得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskText = dataGridView1.Rows[i].Cells["风险"].Value.ToString().Trim();
                mtItemScoreInfoModle.Id = id;

                if (mtItemScoreInfoModle.RiskText.Equals("") || mtItemScoreInfoModle.InfluenScore.Equals("待评估") || mtItemScoreInfoModle.InfluenScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("待评估"))
                {
                    MessageUtil.ShowError("还未评估完成");
                    return;
                }

                //插入分数信息
                Boolean flag = n_RiskAssessmentBll.updateSingleMtSubScoreInfo(mtItemScoreInfoModle, purOrgName);
                if (flag)
                {

                    n++;

                }
                else
                {
                    MessageUtil.ShowError("评估失败，请重试");
                    return;
                }


            }
            if (n == count)
            {
                MessageUtil.ShowTips("评估成功！");

            }
            dataGridView1.DataSource = n_RiskAssessmentBll.getMtGroupData(id, purOrgName);
            getTotalScoreInfo();

        }
        //勾选显示
        private void button2_Click(object sender, EventArgs e)
        {
            List<float> listY = new List<float>();
            List<float> listX = new List<float>();
            double r = 0;
            Series series1 = this.chart1.Series["无影响"];
            Series series2 = this.chart1.Series["低影响"];
            Series series3 = this.chart1.Series["中影响"];
            this.chart1.Series.Clear();

            chart1.Series.Add(series1);
            chart1.Series.Add(series2);
            chart1.Series.Add(series3);


            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                listX.Clear();
                listY.Clear();
                if ((bool)dataGridView1.Rows[i].Cells["选择"].EditedFormattedValue == true)
                {
                    listX.Clear();
                    listY.Clear();
                    if (!dataGridView1.Rows[i].Cells["供应目标"].Value.ToString().Equals(""))
                    {

                        listY.Add(float.Parse(dataGridView1.Rows[i].Cells["影响力得分"].Value.ToString()));
                        listX.Add(float.Parse(dataGridView1.Rows[i].Cells["供应风险得分"].Value.ToString()));
                        r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                    }


                    //求欧式距离


                    Series series = new Series();
                    series.ChartArea = "ChartArea1";
                    series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bubble;
                    series.IsValueShownAsLabel = false;
                    //series.Label = dt.Rows[i]["Aims"].ToString();
                    series.LabelToolTip = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString();
                    series.Legend = "Legend1";

                    if (dataGridView1.Rows[i].Cells["供应目标"].Value.ToString().Length > 4)
                    {
                        series.LegendText = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString().Substring(0, 4) + "..";
                    }
                    series.LegendToolTip = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString();
                    //设置颜色

                    if (r < 2)
                    {
                        series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                    }
                    else if (r < 8)
                    {
                        series.MarkerColor = System.Drawing.Color.Orange;
                    }
                    else if (r < 18)
                    { series.MarkerColor = System.Drawing.Color.Red; }
                    else
                    { series.MarkerColor = System.Drawing.Color.Black; }

                    series.MarkerSize = 10;
                    series.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                    series.Name = dataGridView1.Rows[i].Cells["供应目标"].Value.ToString();

                    series.ToolTip = "指标：#SERIESNAME\n影响力指数：#VALY\n供应风险：#VALX";
                    series.YValuesPerPoint = 2;
                    series.Points.DataBindXY(listX, listY);

                    try
                    {
                        this.chart1.Series.Add(series);

                    }
                    catch (Exception)
                    {
                        MessageUtil.ShowError("有重复的数据，请正确定义评估项！");
                    }





                }


            }


        }
    }
}
