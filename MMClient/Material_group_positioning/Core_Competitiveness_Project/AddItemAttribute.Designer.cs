﻿namespace MMClient.Material_group_positioning.Core_Competitiveness_Project
{
    partial class AddItemAttribute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.ZH4 = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.GO4 = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ZH3 = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.GO3 = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ZH2 = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.GO2 = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ZH1 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.GO1 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.B4 = new System.Windows.Forms.Button();
            this.Z4 = new System.Windows.Forms.RichTextBox();
            this.L14 = new System.Windows.Forms.Label();
            this.G4 = new System.Windows.Forms.RichTextBox();
            this.L4 = new System.Windows.Forms.Label();
            this.B3 = new System.Windows.Forms.Button();
            this.Z3 = new System.Windows.Forms.RichTextBox();
            this.L13 = new System.Windows.Forms.Label();
            this.G3 = new System.Windows.Forms.RichTextBox();
            this.L3 = new System.Windows.Forms.Label();
            this.B2 = new System.Windows.Forms.Button();
            this.Z2 = new System.Windows.Forms.RichTextBox();
            this.L12 = new System.Windows.Forms.Label();
            this.G2 = new System.Windows.Forms.RichTextBox();
            this.L2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.B1 = new System.Windows.Forms.Button();
            this.Z1 = new System.Windows.Forms.RichTextBox();
            this.L11 = new System.Windows.Forms.Label();
            this.G1 = new System.Windows.Forms.RichTextBox();
            this.L1 = new System.Windows.Forms.Label();
            this.code_MtGroup = new System.Windows.Forms.ComboBox();
            this.name_MtGroup = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(339, 15);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 127;
            this.textBox2.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(232, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 12);
            this.label14.TabIndex = 126;
            this.label14.Text = "新保存的模板名称";
            this.label14.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(252, 46);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 23);
            this.button2.TabIndex = 125;
            this.button2.Text = "保存为模板并保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ZH4
            // 
            this.ZH4.Location = new System.Drawing.Point(368, 339);
            this.ZH4.Name = "ZH4";
            this.ZH4.Size = new System.Drawing.Size(160, 40);
            this.ZH4.TabIndex = 124;
            this.ZH4.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(332, 339);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 123;
            this.label12.Text = "指标";
            // 
            // GO4
            // 
            this.GO4.Location = new System.Drawing.Point(102, 342);
            this.GO4.Name = "GO4";
            this.GO4.Size = new System.Drawing.Size(160, 37);
            this.GO4.TabIndex = 122;
            this.GO4.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 342);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 121;
            this.label13.Text = "供应目标";
            // 
            // ZH3
            // 
            this.ZH3.Location = new System.Drawing.Point(369, 270);
            this.ZH3.Name = "ZH3";
            this.ZH3.Size = new System.Drawing.Size(160, 40);
            this.ZH3.TabIndex = 120;
            this.ZH3.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(333, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 119;
            this.label10.Text = "指标";
            // 
            // GO3
            // 
            this.GO3.Location = new System.Drawing.Point(103, 273);
            this.GO3.Name = "GO3";
            this.GO3.Size = new System.Drawing.Size(160, 37);
            this.GO3.TabIndex = 118;
            this.GO3.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 273);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 117;
            this.label11.Text = "供应目标";
            // 
            // ZH2
            // 
            this.ZH2.Location = new System.Drawing.Point(369, 187);
            this.ZH2.Name = "ZH2";
            this.ZH2.Size = new System.Drawing.Size(160, 40);
            this.ZH2.TabIndex = 116;
            this.ZH2.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(333, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 115;
            this.label8.Text = "指标";
            // 
            // GO2
            // 
            this.GO2.Location = new System.Drawing.Point(103, 190);
            this.GO2.Name = "GO2";
            this.GO2.Size = new System.Drawing.Size(160, 37);
            this.GO2.TabIndex = 114;
            this.GO2.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 113;
            this.label9.Text = "供应目标";
            // 
            // ZH1
            // 
            this.ZH1.Location = new System.Drawing.Point(369, 119);
            this.ZH1.Name = "ZH1";
            this.ZH1.Size = new System.Drawing.Size(160, 40);
            this.ZH1.TabIndex = 112;
            this.ZH1.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 111;
            this.label6.Text = "指标";
            // 
            // GO1
            // 
            this.GO1.Location = new System.Drawing.Point(103, 122);
            this.GO1.Name = "GO1";
            this.GO1.Size = new System.Drawing.Size(160, 37);
            this.GO1.TabIndex = 110;
            this.GO1.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 109;
            this.label7.Text = "供应目标";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 108;
            this.label5.Text = "模板选择";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(321, 85);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 20);
            this.comboBox2.TabIndex = 107;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(553, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(86, 21);
            this.textBox1.TabIndex = 106;
            this.textBox1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(453, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 105;
            this.label4.Text = "采购组织编号";
            this.label4.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(103, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 104;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 103;
            this.label3.Text = "采购组织名称";
            // 
            // B4
            // 
            this.B4.Location = new System.Drawing.Point(257, 388);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(75, 23);
            this.B4.TabIndex = 102;
            this.B4.Text = "添加";
            this.B4.UseVisualStyleBackColor = true;
            this.B4.Visible = false;
            // 
            // Z4
            // 
            this.Z4.Location = new System.Drawing.Point(369, 342);
            this.Z4.Name = "Z4";
            this.Z4.Size = new System.Drawing.Size(160, 40);
            this.Z4.TabIndex = 101;
            this.Z4.Text = "";
            this.Z4.Visible = false;
            // 
            // L14
            // 
            this.L14.AutoSize = true;
            this.L14.Location = new System.Drawing.Point(333, 342);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(29, 12);
            this.L14.TabIndex = 100;
            this.L14.Text = "指标";
            this.L14.Visible = false;
            // 
            // G4
            // 
            this.G4.Location = new System.Drawing.Point(103, 345);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(160, 37);
            this.G4.TabIndex = 99;
            this.G4.Text = "";
            this.G4.Visible = false;
            // 
            // L4
            // 
            this.L4.AutoSize = true;
            this.L4.Location = new System.Drawing.Point(19, 345);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(53, 12);
            this.L4.TabIndex = 98;
            this.L4.Text = "供应目标";
            this.L4.Visible = false;
            // 
            // B3
            // 
            this.B3.Location = new System.Drawing.Point(257, 316);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(75, 23);
            this.B3.TabIndex = 97;
            this.B3.Text = "添加";
            this.B3.UseVisualStyleBackColor = true;
            this.B3.Visible = false;
            // 
            // Z3
            // 
            this.Z3.Location = new System.Drawing.Point(369, 270);
            this.Z3.Name = "Z3";
            this.Z3.Size = new System.Drawing.Size(160, 40);
            this.Z3.TabIndex = 96;
            this.Z3.Text = "";
            this.Z3.Visible = false;
            // 
            // L13
            // 
            this.L13.AutoSize = true;
            this.L13.Location = new System.Drawing.Point(333, 270);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(29, 12);
            this.L13.TabIndex = 95;
            this.L13.Text = "指标";
            this.L13.Visible = false;
            // 
            // G3
            // 
            this.G3.Location = new System.Drawing.Point(103, 273);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(160, 37);
            this.G3.TabIndex = 94;
            this.G3.Text = "";
            this.G3.Visible = false;
            // 
            // L3
            // 
            this.L3.AutoSize = true;
            this.L3.Location = new System.Drawing.Point(19, 273);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(53, 12);
            this.L3.TabIndex = 93;
            this.L3.Text = "供应目标";
            this.L3.Visible = false;
            // 
            // B2
            // 
            this.B2.Location = new System.Drawing.Point(257, 236);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(75, 23);
            this.B2.TabIndex = 92;
            this.B2.Text = "添加";
            this.B2.UseVisualStyleBackColor = true;
            this.B2.Visible = false;
            // 
            // Z2
            // 
            this.Z2.Location = new System.Drawing.Point(369, 190);
            this.Z2.Name = "Z2";
            this.Z2.Size = new System.Drawing.Size(160, 40);
            this.Z2.TabIndex = 91;
            this.Z2.Text = "";
            this.Z2.Visible = false;
            // 
            // L12
            // 
            this.L12.AutoSize = true;
            this.L12.Location = new System.Drawing.Point(333, 190);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(29, 12);
            this.L12.TabIndex = 90;
            this.L12.Text = "指标";
            this.L12.Visible = false;
            // 
            // G2
            // 
            this.G2.Location = new System.Drawing.Point(103, 193);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(160, 37);
            this.G2.TabIndex = 89;
            this.G2.Text = "";
            this.G2.Visible = false;
            // 
            // L2
            // 
            this.L2.AutoSize = true;
            this.L2.Location = new System.Drawing.Point(19, 193);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(53, 12);
            this.L2.TabIndex = 88;
            this.L2.Text = "供应目标";
            this.L2.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(395, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 87;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // B1
            // 
            this.B1.Location = new System.Drawing.Point(257, 165);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(75, 23);
            this.B1.TabIndex = 86;
            this.B1.Text = "添加";
            this.B1.UseVisualStyleBackColor = true;
            this.B1.Visible = false;
            // 
            // Z1
            // 
            this.Z1.Location = new System.Drawing.Point(369, 119);
            this.Z1.Name = "Z1";
            this.Z1.Size = new System.Drawing.Size(160, 40);
            this.Z1.TabIndex = 85;
            this.Z1.Text = "";
            this.Z1.Visible = false;
            // 
            // L11
            // 
            this.L11.AutoSize = true;
            this.L11.Location = new System.Drawing.Point(333, 122);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(29, 12);
            this.L11.TabIndex = 84;
            this.L11.Text = "指标";
            this.L11.Visible = false;
            // 
            // G1
            // 
            this.G1.Location = new System.Drawing.Point(103, 122);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(160, 37);
            this.G1.TabIndex = 83;
            this.G1.Text = "";
            this.G1.Visible = false;
            // 
            // L1
            // 
            this.L1.AutoSize = true;
            this.L1.Location = new System.Drawing.Point(19, 122);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(53, 12);
            this.L1.TabIndex = 82;
            this.L1.Text = "供应目标";
            this.L1.Visible = false;
            // 
            // code_MtGroup
            // 
            this.code_MtGroup.FormattingEnabled = true;
            this.code_MtGroup.Location = new System.Drawing.Point(103, 49);
            this.code_MtGroup.Name = "code_MtGroup";
            this.code_MtGroup.Size = new System.Drawing.Size(100, 20);
            this.code_MtGroup.TabIndex = 81;
            this.code_MtGroup.SelectedIndexChanged += new System.EventHandler(this.code_MtGroup_SelectedIndexChanged);
            // 
            // name_MtGroup
            // 
            this.name_MtGroup.Location = new System.Drawing.Point(103, 84);
            this.name_MtGroup.Name = "name_MtGroup";
            this.name_MtGroup.ReadOnly = true;
            this.name_MtGroup.Size = new System.Drawing.Size(100, 21);
            this.name_MtGroup.TabIndex = 80;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 79;
            this.label2.Text = "物料组编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 78;
            this.label1.Text = "物料组名称";
            // 
            // AddItemAttribute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 427);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ZH4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.GO4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ZH3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.GO3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ZH2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.GO2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ZH1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.GO1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.Z4);
            this.Controls.Add(this.L14);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.L4);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.Z3);
            this.Controls.Add(this.L13);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.L3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.Z2);
            this.Controls.Add(this.L12);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.L2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.Z1);
            this.Controls.Add(this.L11);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.code_MtGroup);
            this.Controls.Add(this.name_MtGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddItemAttribute";
            this.Text = "添加";
            this.Load += new System.EventHandler(this.AddItemAttribute_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox ZH4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox GO4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox ZH3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox GO3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox ZH2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox GO2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox ZH1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox GO1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button B4;
        private System.Windows.Forms.RichTextBox Z4;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.RichTextBox G4;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.Button B3;
        private System.Windows.Forms.RichTextBox Z3;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.RichTextBox G3;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Button B2;
        private System.Windows.Forms.RichTextBox Z2;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.RichTextBox G2;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button B1;
        private System.Windows.Forms.RichTextBox Z1;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.RichTextBox G1;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.ComboBox code_MtGroup;
        private System.Windows.Forms.TextBox name_MtGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}