﻿using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.Genaral_Project
{
    public partial class ModifyGeneralItemAttribute : Form
    {
        private string G;
        private string Z;
        public ModifyGeneralItemAttribute(string code_ORG, string name_ORG, String code_MTG1, String name_MTG1, String G11, String Z11)
        {
            InitializeComponent();
            ORGID.Text = code_ORG;
            ORGName.Text = name_ORG;
            MTGName.Text = name_MTG1;
            MTGID.Text = code_MTG1;
            G1.Text = G11;
            Z1.Text = Z11;
            this.G = G11;
            this.Z = Z11;
        }

        private void ModifyGeneralItemAttribute_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//修改
        {
            GeneralBLL gn = new GeneralBLL();

            
            gn.UpdategeneralitemMtGroup(this.G, this.Z, G1.Text, Z1.Text);
            MessageUtil.ShowWarning("修改成功！");

        }
    }
}
