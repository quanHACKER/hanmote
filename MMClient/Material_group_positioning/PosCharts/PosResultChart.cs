﻿using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.Material_group_positioning.PosCharts
{
    public partial class PosResultChart : DockContent
    {
        String mtId, purOrgName;
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        MtItemScoreInfoModle mtItemScoreInfoModle = new MtItemScoreInfoModle();
        public PosResultChart(String purOrgName)
        {
            this.purOrgName = purOrgName;
            InitializeComponent();
            this.label11.Text = purOrgName;
            double[] r = { 2, 8, 18 };
            DrawCircl(r.Length,this.chart1,r);
            initalChart();

        }



        private void initalChart()
        {
            double x=0, y=0,total=0;
            String Level = "";
            List<double> listY = new List<double>();
            List<double> listX = new List<double>();
            double r = 0;
            DataTable dt = n_RiskAssessmentBll.getMtGroupImportedData(purOrgName);
            if (dt.Rows.Count == 0) {
                MessageUtil.ShowTips("尚无物料组已评估，请先完成影响/供应风险评估！");
                
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["评分"].ToString().Equals(""))
                {

                    total = float.Parse(dt.Rows[i]["累计支出"].ToString().TrimEnd('%'));
                    x = float.Parse(dt.Rows[i]["支出金额"].ToString().TrimEnd('%'));
                    y = Math.Round(float.Parse(dt.Rows[i]["评分"].ToString()),2);
                    if (total <=80)
                    {
                        x = Math.Round(2 + 2 * (x) / 100,2);
                    }
                    else {
                        x = Math.Round(x * 2 / 100,2);

                    }

                    listY.Add(y);
                    listX.Add(x);
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }
                else {

                    MessageUtil.ShowError("数据不完整！");
                }


                //求欧式距离


                Series series = new Series();
                series.ChartArea = "ChartArea1";
                series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bubble;
                series.IsValueShownAsLabel = false;
                
                series.LabelToolTip = dt.Rows[i]["MtGroupName"].ToString();
                series.Legend = "Legend1";
                if (dt.Rows[i]["MtGroupName"].ToString().Length > 4)
                {
                    series.LegendText = dt.Rows[i]["MtGroupName"].ToString().Substring(0, 4) + "..";
                }
                series.LegendToolTip = dt.Rows[i]["MtGroupName"].ToString();
                //设置颜色

                if (r < 2)
                {
                    series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                    Level = "N";

                }
                else if (r < 8)
                {
                    series.MarkerColor = System.Drawing.Color.Orange;
                    Level = "L";
                }
                else if (r < 18)
                {
                    series.MarkerColor = System.Drawing.Color.Red;
                    Level = "M";

                }
                else
                { series.MarkerColor = System.Drawing.Color.Black;
                    Level = "H";
                }

                series.MarkerSize = 10;
                series.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                series.Name = dt.Rows[i]["MtGroupName"].ToString();
                series.ToolTip = dt.Rows[i]["MtGroupName"].ToString();
                double cost = 0;
                if (total <= 80)
                {
                    cost = (x - 2) / 2 * 100;
                }
                else
                {
                    cost = x*50;

                }


                series.ToolTip = "指标：#SERIESNAME\n影响/风险指数："+y+"\n支出金额："+cost+"%";
                series.YValuesPerPoint = 2;
                series.Points.DataBindXY(listX, listY);
                this.dataGridView1.Rows.Add(false,dt.Rows[i]["MtGroupName"].ToString(),y, cost + "%",Level);


                try
                {
                    this.chart1.Series.Add(series);

                }
                catch (Exception e)
                {
                    MessageUtil.ShowError("有重复的数据，请正确定义评估项！");
                }

            }


        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            //保存评估结果

            PosResultInfoModel posResultInfoModel = new PosResultInfoModel();
            int flag = 0,c=0;
            posResultInfoModel.PurOrgName = purOrgName;
            int n = dataGridView1.Rows.Count;
            for (int i = 0; i < n; i++) {
                posResultInfoModel.MtName = dataGridView1.Rows[i].Cells["物料组"].Value.ToString();
                posResultInfoModel.InfluAndRiskScore = dataGridView1.Rows[i].Cells["影响风险评分"].Value.ToString();
                posResultInfoModel.Cost = dataGridView1.Rows[i].Cells["支出金额"].Value.ToString();
                posResultInfoModel.PosResult = dataGridView1.Rows[i].Cells["定位结果"].Value.ToString();
                flag = n_RiskAssessmentBll.insertLastPosResultInfo(posResultInfoModel);
                if (flag > 0) {
                    c++;
                }

            }

            if (c == n) { MessageUtil.ShowTips("保存成功！"); } else { MessageUtil.ShowError("保存失败，请重试！"); }
                

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = n_RiskAssessmentBll.SearchMtGroupByName(this.textBox1.Text, label11.Text);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow r = this.dataGridView1.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.dataGridView1.Refresh();
        }



        /// <summary>
        /// 绘制参考线
        /// </summary>
        /// <param name="n"></param>
        /// <param name="chart"></param>
        /// <param name="r"></param>
        private void DrawCircl(int n, Chart chart, double[] r)
        {
            List<Double> y1 = new List<Double>();
            List<Double> x1 = new List<Double>();

            for (int m = 0; m < n; m++)
            {
                for (double i = 0.00; i <= r[m]; i = i + 0.01)
                {
                    for (double j = 0.00; j <= r[m]; j = j + 0.01)
                    {
                        i = Math.Round(i, 2);
                        j = Math.Round(j, 2);
                        if (r[m] - (i * i + j * j) <= 10e-5)
                        {
                            x1.Add(i);
                            y1.Add(j);
                            break;

                        }


                    }


                }
                this.chart1.Series[m].Points.DataBindXY(x1, y1);
                x1.Clear();
                y1.Clear();


            }


        }


    }
  




}
