﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.PurchaseOrderExecution;
using Lib.Common.CommonUtils;
using Lib.Bll.PurchaseOrderExecutionBLL;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;

namespace MMClient.OrderExecution.InvoiceAndPayment
{
    public partial class NewInvoiceForm : DockContent
    {
        private Invoice invoice = null;
        // 操作类型
        private string opType;
        // 业务逻辑处理工具
        private InvoiceBLL invoiceTool = new InvoiceBLL();
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private MaterialAccountBLL materialAccountTool = new MaterialAccountBLL();
        // 货币类型
        private string currencyType = "";
        private List<string> currencyTypeList = null;
        // 所有的物料ID
        private List<string> materialIDList = null;

        private double max = 0.0;
        private bool isInitialized = false;

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public NewInvoiceForm()
        {
            InitializeComponent();

            //新建发票
            invoice = new Invoice();
            invoice.Invoice_ID = "FP" + System.DateTime.Now.ToString("yyyyMMddHHmmss")
                + "2016001";
            this.opType = "new";

            initialForm();
            isInitialized = true;
        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="oldInvoice"></param>
        public NewInvoiceForm(Invoice oldInvoice, string _opType) {
            InitializeComponent();
            initialForm();

            this.opType = _opType;

            this.invoice = oldInvoice;
            this.tbInvoiceCode.Text = invoice.Invoice_Code;
            this.tbInvoiceNumber.Text = invoice.Invoice_Number;
            this.cbxSupplierID.SelectedIndex =
                this.cbxSupplierID.Items.IndexOf(invoice.Supplier_ID);
            this.cbxSupplierName.SelectedIndex =
                this.cbxSupplierName.Items.IndexOf(invoice.Supplier_Name);
            this.cbxCertificateType.SelectedIndex =
                this.cbxCertificateType.Items.IndexOf(invoice.Certificate_Type);
            this.cbxCertificateID.SelectedIndex =
                this.cbxCertificateID.Items.IndexOf(invoice.Certificate_ID);
            this.dtpMakeOut.Value = invoice.Invoice_MakeOut_Time;
            this.dtpCreate.Value = invoice.Create_Time;
            this.tbPayerName.Text = invoice.Payer_Name;
            this.tbPayerIdentifyNumber.Text = invoice.Payer_Identify_Number;
            this.tbRecipientName.Text = invoice.Recipient_Name;
            this.tbRecipientIdentifyNumber.Text = invoice.Recipient_Identify_Number;
            this.tbPaymentType.Text = invoice.Payment_Type;
            this.tbSum.Text = Convert.ToString(invoice.Sum);
            this.cbxCurrencyType.SelectedIndex =
                this.cbxCurrencyType.Items.IndexOf(invoice.Currency);
            this.tbMaker.Text = invoice.Invoice_Maker;

            // 发票项
            List<Invoice_Item> items = invoiceTool.getInvoiceItemByInvoiceID(invoice.Invoice_ID);
            int i = 0;
            foreach (Invoice_Item item in items) {
                this.dgvItems.Rows.Add();
                DataGridViewRow row = this.dgvItems.Rows[i];
                //绑定数据源
                DataGridViewComboBoxCell cell =
                    (DataGridViewComboBoxCell)row.Cells[0];
                cell.DataSource = materialIDList;
                cell.Value = item.Material_ID;
                row.Cells[1].Value = item.Material_Name;
                row.Cells[2].Value = item.Unit_Price;
                row.Cells[3].Value = item.Quantity;
                row.Cells[4].Value = item.Sum;
                row.Cells[5].Value = item.Currency;

                i++;
            }

            if (opType.Equals("preview")) {
                //禁用所有的控件
                foreach (Control c in this.groupBox1.Controls) {
                    string name = c.Name;
                    if(name.Contains("label"))
                        c.Enabled = false;
                }
                this.dgvItems.Enabled = false;
            }
            isInitialized = true;
        }

        /// <summary>
        /// 初始化窗体
        /// </summary>
        private void initialForm() {
            //初始化供应商
            //绑定数据源
            /*
            string sql = "select * from Supplier_Base";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.cbxSupplierID.DataSource = dt;
            this.cbxSupplierID.DisplayMember = "Supplier_ID";
            this.cbxSupplierID.ValueMember = "Supplier_ID";
            this.cbxSupplierName.DataSource = dt;
            this.cbxSupplierName.DisplayMember = "Supplier_Name";
            this.cbxSupplierName.ValueMember = "Supplier_Name";
             * */
            // 货币类型绑定数据源
            currencyTypeList = generalTool.GetCurrencyType();
            this.cbxCurrencyType.DataSource = currencyTypeList;
           // materialIDList = materialTool.GetAllMaterialID();
            // 凭证类型
            this.cbxCertificateType.SelectedIndex = 0;
            string sql1 = "select distinct(Order_ID) from Order_Info";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            List<string> cList = new List<string>();
            if (dt1 != null)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    cList.Add(dt1.Rows[i][0].ToString());
                }
            }
            this.cbxCertificateID.DataSource = cList;
            this.cbxCertificateID.SelectedIndex = 0;
            string certificate = this.cbxCertificateID.SelectedText.Trim();
            if(certificate.Equals(""))
            {
                materialIDList = materialTool.GetAllMaterialID();
            }
            else
            {
                string sql = "select distinct(Mterial_ID) from Order_Item where Order_ID = '" + certificate + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if(dt != null)
                {
                    for(int i = 0;i < dt.Rows.Count;i++)
                    {
                        materialIDList.Add(dt.Rows[i][0].ToString());
                    }
                }
            }
        }

        /// <summary>
        /// 展示已读取到的信息
        /// </summary>
        /// <param name="invoice"></param>
        private void displayInvoice(Invoice invoice) {
            this.tbInvoiceCode.Text = invoice.Invoice_Code;
            this.tbInvoiceNumber.Text = invoice.Invoice_Number;
            this.cbxCertificateType.SelectedIndex =
                this.cbxCertificateType.Items.IndexOf(invoice.Certificate_Type);
            int supplierIndex = this.cbxSupplierID.Items
                .IndexOf(invoice.Supplier_ID);
            if (supplierIndex >= 0 && supplierIndex < this.cbxSupplierID.Items.Count) {
                this.cbxSupplierID.SelectedIndex = supplierIndex;
            }
        }

        /// <summary>
        /// 点击增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //增加一行
            this.dgvItems.Rows.Add();

            int rowCount = this.dgvItems.Rows.Count;
            DataGridViewRow row = this.dgvItems.Rows[rowCount - 1];
            //设置物料ID源
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[0];
            cell.DataSource = materialIDList;
            row.Cells[5].Value = currencyType;
        }

        /// <summary>
        /// 点击删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvItems.Rows.Count == 0)
            {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvItems.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvItems.Rows.Count)
            {
                this.dgvItems.Rows.RemoveAt(rowIndex);
            }
        }

        /// <summary>
        /// 点击计算
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalculate_Click(object sender, EventArgs e)
        {
            //计算合计金额
            double sum = 0.0;
            try{
                foreach (DataGridViewRow row in this.dgvItems.Rows) {
                    double amount = DataGridViewCellTool
                        .getDataGridViewCellValueDouble(row.Cells[4]);
                    sum += amount;
                }
            }
            catch(Exception){
                MessageUtil.ShowError("请正确填写！");
                return;
            }

            string orderId = this.cbxCertificateID.SelectedValue.ToString();
            string sql = "select Total_Value from Order_Info where Order_ID = '" + orderId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                max = Convert.ToDouble(dt.Rows[0][0].ToString());
            }

            if (sum > max)
            {
                MessageBox.Show("发票金额超过订单总金额，请认真核对！订单总金额： " + max);
                return;
            }

            this.tbSum.Text = Convert.ToString(sum);
        }

        /// <summary>
        /// 点击保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //先计算
            double sum = 0.0;
            try
            {
                foreach (DataGridViewRow row in this.dgvItems.Rows)
                {
                    double amount = DataGridViewCellTool
                        .getDataGridViewCellValueDouble(row.Cells[4]);
                    sum += amount;
                }
            }
            catch (Exception)
            {
                MessageUtil.ShowError("请正确填写！");
                return;
            }

            this.tbSum.Text = Convert.ToString(sum);

            //检验是否完整
            invoice.Invoice_Code = this.tbInvoiceCode.Text.Trim();
            invoice.Invoice_Number = this.tbInvoiceNumber.Text.Trim();
            invoice.Supplier_ID = this.cbxSupplierID.Text.Trim();
            invoice.Supplier_Name = this.cbxSupplierName.Text.Trim();
            invoice.Certificate_Type = this.cbxCertificateType.Text.Trim();
            invoice.Certificate_ID = this.cbxCertificateID.Text.Trim();
            invoice.Invoice_MakeOut_Time = this.dtpMakeOut.Value;
            invoice.Create_Time = this.dtpCreate.Value;
            invoice.Payer_Name = this.tbPayerName.Text.Trim();
            invoice.Payer_Identify_Number = this.tbPayerIdentifyNumber.Text.Trim();
            invoice.Recipient_Name = this.tbRecipientName.Text.Trim();
            invoice.Recipient_Identify_Number = this.tbRecipientIdentifyNumber.Text.Trim();
            invoice.Payment_Type = this.tbPaymentType.Text.Trim();
            try
            {
                invoice.Sum = Convert.ToDouble(this.tbSum.Text);
            }
            catch (Exception) {
                MessageUtil.ShowError("总金额填写错误!");
                return;
            }
            invoice.Currency = this.cbxCurrencyType.Text;

            if (invoice.Invoice_Code.Equals("")
                || invoice.Invoice_Number.Equals("")
                || invoice.Certificate_ID.Equals("")) {
                    
                MessageUtil.ShowError("请填写完整！");
                //检查是否重复的发票 *****************************************
                
                return;
            }

            List<Invoice_Item> itemList = new List<Invoice_Item>();
            if(this.dgvItems.Rows.Count == 0){
                MessageUtil.ShowError("请添加发票项目！");
                this.btnAdd.Focus();
                return;
            }
            try
            {
                foreach (DataGridViewRow row in this.dgvItems.Rows)
                {
                    Invoice_Item item = new Invoice_Item();
                    item.Invoice_ID = invoice.Invoice_ID;
                    item.Material_ID = row.Cells[0].Value.ToString();
                    item.Material_Name = row.Cells[1].Value.ToString();
                    item.Unit_Price = Convert.ToDouble(row.Cells[2].Value.ToString());
                    item.Quantity = Convert.ToDouble(row.Cells[3].Value.ToString());
                    item.Sum = Convert.ToDouble(row.Cells[4].Value.ToString());
                    item.Currency = row.Cells[5].Value.ToString();

                    itemList.Add(item);
                }
            }
            catch (Exception) {
                MessageUtil.ShowError("发票项目数据不正确！");
                return;
            }

            if (opType.Equals("new")) {
                //保存
                if (invoiceTool.addInvoice(invoice, itemList) > 0) { 
                    MessageBox.Show("保存成功!");
                }
                else{
                    MessageBox.Show("保存失败！");
                }
            }
            else if (opType.Equals("edit")) { 
                //更新
                if (invoiceTool.updateInvoice(invoice, itemList) > 0)
                {
                    MessageBox.Show("更新成功!");
                }
                else
                {
                    MessageBox.Show("更新失败！");
                }
            }
        }

        /// <summary>
        /// 货币类型改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //更改下面的货币类型
            if (isInitialized)
            {
                currencyType = this.cbxCurrencyType.Text.Trim();
                foreach (DataGridViewRow row in this.dgvItems.Rows)
                {
                    row.Cells[5].Value = currencyType;
                }
            }
        }

        /// <summary>
        /// 单元格编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvItems_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = this.dgvItems.CurrentCell.ColumnIndex;
            if (columnIndex == 0 && e.Control is ComboBox)
            {
                ComboBox combo = e.Control as ComboBox;
                combo.SelectedIndexChanged += materialID_SelectedIndexChanged;
            }
        }

        private void materialID_SelectedIndexChanged(object sender, EventArgs e) {
            if (isInitialized)
            {
                int rowIndex = this.dgvItems.CurrentCell.RowIndex;
                int columnIndex = this.dgvItems.CurrentCell.ColumnIndex;
                //可以转换
                if (rowIndex >= 0)
                {
                    var sendingCB = sender as DataGridViewComboBoxEditingControl;
                    string materialID = sendingCB.EditingControlFormattedValue.ToString();
                    if (!materialID.Equals(""))
                    {
                        //DataTable dt = generalTool.getMaterialInfo(materialID);
                        MaterialBase materialInfo = materialAccountTool
                            .GetBasicInformation(materialID);

                        //DataRow row = dt.Rows[0];
                        DataGridViewRow dgvRow = this.dgvItems.Rows[rowIndex];
                        //短文本(物料名称)
                        dgvRow.Cells[1].Value = materialInfo.Material_Name;
                    }
                }
            }
        }

        private void dgvItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = e.ColumnIndex;
            if (columnIndex == 2 || columnIndex == 3)
            {
                try
                {
                    int rowIndex = this.dgvItems.CurrentCell.RowIndex;
                    DataGridViewRow row = this.dgvItems.Rows[rowIndex];
                    double price = DataGridViewCellTool.getDataGridViewCellValueDouble(
                        row.Cells[2]);
                    double num = DataGridViewCellTool.getDataGridViewCellValueDouble(
                        row.Cells[3]);
                    double amount = price * num;

                    row.Cells[4].Value = amount;
                }
                catch (Exception)
                {
                    // do nothing
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cbxCertificateID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                string orderId = this.cbxCertificateID.SelectedValue.ToString();
                if (orderId.Trim().Equals(""))
                    return;
                //更改物料数据源
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                string sql11 = "select Mterial_ID from Order_Item where Order_ID = '" + orderId + "'";
                DataTable dt11 = DBHelper.ExecuteQueryDT(sql11);
                List<string> temp = new List<string>();
                if (dt11 != null)
                {
                    for (int i = 0; i < dt11.Rows.Count; i++)
                    {
                        temp.Add(dt11.Rows[i][0].ToString());
                    }
                }
                materialIDList = temp;
                foreach(DataGridViewRow row in this.dgvItems.Rows)
                {
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[0];
                    cell.DataSource = materialIDList;
                }
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //更改供应商编号数据源
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                string sql12 = "select Supplier_ID from Order_Info where Order_ID = '" + orderId + "'";
                DataTable dt12 = DBHelper.ExecuteQueryDT(sql12);
                List<string> temp12 = new List<string>();
                if (dt12 != null)
                {
                    for (int i = 0; i < dt12.Rows.Count; i++)
                    {
                        temp12.Add(dt12.Rows[i][0].ToString());
                    }
                }
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                this.cbxSupplierID.DataSource = temp12;
                this.cbxSupplierID.SelectedIndex = 0;
            }
        }

        private void dgvItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void NewInvoiceForm_Load(object sender, EventArgs e)
        {
          //  initialForm();
        }

        private void cbxSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                string supplierId = this.cbxSupplierID.SelectedValue.ToString();
                string sql11 = "select Supplier_Name from Supplier_Base where Supplier_ID = '" + supplierId + "'";
                DataTable dt11 = DBHelper.ExecuteQueryDT(sql11);
                List<string> temp = new List<string>();
                if (dt11 != null)
                {
                    for (int i = 0; i < dt11.Rows.Count; i++)
                    {
                        temp.Add(dt11.Rows[i][0].ToString());
                    }
                }
                this.cbxSupplierName.DataSource = temp;
            }
        }
    }
}
