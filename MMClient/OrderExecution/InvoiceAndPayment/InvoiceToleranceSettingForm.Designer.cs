﻿namespace MMClient.OrderExecution.InvoiceAndPayment
{
    partial class InvoiceToleranceSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpNum = new System.Windows.Forms.TabPage();
            this.tbNumPerTop = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbNumPerTop = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbNumAbValueTop = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbNumAbValueTop = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbNumPerFloor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbNumPerFloor = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbNumAbValueFloor = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbNumAbValueFloor = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tpPrice = new System.Windows.Forms.TabPage();
            this.tbPricePerTop = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbPricePerTop = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbPriceAbValueTop = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbPriceAbValueTop = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbPricePerFloor = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cbPricePerFloor = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbPriceAbValueFloor = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cbPriceAbValueFloor = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tpAmount = new System.Windows.Forms.TabPage();
            this.tbAmountPerTop = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cbAmountPerTop = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbAmountAbValueTop = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cbAmountAbValueTop = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tbAmountPerFloor = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbAmountPerFloor = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbAmountAbValueFloor = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cbAmountAbValueFloor = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpNum.SuspendLayout();
            this.tpPrice.SuspendLayout();
            this.tpAmount.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(619, 116);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "发票容差设置";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(513, 74);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpNum);
            this.tabControl1.Controls.Add(this.tpPrice);
            this.tabControl1.Controls.Add(this.tpAmount);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 116);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(619, 497);
            this.tabControl1.TabIndex = 2;
            // 
            // tpNum
            // 
            this.tpNum.Controls.Add(this.tbNumPerTop);
            this.tpNum.Controls.Add(this.label16);
            this.tpNum.Controls.Add(this.cbNumPerTop);
            this.tpNum.Controls.Add(this.label17);
            this.tpNum.Controls.Add(this.tbNumAbValueTop);
            this.tpNum.Controls.Add(this.label18);
            this.tpNum.Controls.Add(this.cbNumAbValueTop);
            this.tpNum.Controls.Add(this.label19);
            this.tpNum.Controls.Add(this.label20);
            this.tpNum.Controls.Add(this.tbNumPerFloor);
            this.tpNum.Controls.Add(this.label14);
            this.tpNum.Controls.Add(this.cbNumPerFloor);
            this.tpNum.Controls.Add(this.label15);
            this.tpNum.Controls.Add(this.tbNumAbValueFloor);
            this.tpNum.Controls.Add(this.label13);
            this.tpNum.Controls.Add(this.cbNumAbValueFloor);
            this.tpNum.Controls.Add(this.label12);
            this.tpNum.Controls.Add(this.label11);
            this.tpNum.Location = new System.Drawing.Point(4, 22);
            this.tpNum.Name = "tpNum";
            this.tpNum.Padding = new System.Windows.Forms.Padding(3);
            this.tpNum.Size = new System.Drawing.Size(611, 471);
            this.tpNum.TabIndex = 0;
            this.tpNum.Text = "数量";
            this.tpNum.UseVisualStyleBackColor = true;
            // 
            // tbNumPerTop
            // 
            this.tbNumPerTop.Location = new System.Drawing.Point(287, 230);
            this.tbNumPerTop.Name = "tbNumPerTop";
            this.tbNumPerTop.Size = new System.Drawing.Size(118, 21);
            this.tbNumPerTop.TabIndex = 17;
            this.tbNumPerTop.Text = "1.0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(203, 233);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 12);
            this.label16.TabIndex = 16;
            this.label16.Text = "容差限制(%)：";
            // 
            // cbNumPerTop
            // 
            this.cbNumPerTop.AutoSize = true;
            this.cbNumPerTop.Location = new System.Drawing.Point(100, 232);
            this.cbNumPerTop.Name = "cbNumPerTop";
            this.cbNumPerTop.Size = new System.Drawing.Size(72, 16);
            this.cbNumPerTop.TabIndex = 15;
            this.cbNumPerTop.Text = "检查限制";
            this.cbNumPerTop.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 233);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 14;
            this.label17.Text = "百分比：";
            // 
            // tbNumAbValueTop
            // 
            this.tbNumAbValueTop.Location = new System.Drawing.Point(287, 187);
            this.tbNumAbValueTop.Name = "tbNumAbValueTop";
            this.tbNumAbValueTop.Size = new System.Drawing.Size(118, 21);
            this.tbNumAbValueTop.TabIndex = 13;
            this.tbNumAbValueTop.Text = "1.0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(220, 189);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 12;
            this.label18.Text = "数量：";
            // 
            // cbNumAbValueTop
            // 
            this.cbNumAbValueTop.AutoSize = true;
            this.cbNumAbValueTop.Location = new System.Drawing.Point(100, 189);
            this.cbNumAbValueTop.Name = "cbNumAbValueTop";
            this.cbNumAbValueTop.Size = new System.Drawing.Size(72, 16);
            this.cbNumAbValueTop.TabIndex = 11;
            this.cbNumAbValueTop.Text = "检查限制";
            this.cbNumAbValueTop.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 190);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 10;
            this.label19.Text = "绝对值：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(19, 152);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 9;
            this.label20.Text = "上限：";
            // 
            // tbNumPerFloor
            // 
            this.tbNumPerFloor.Location = new System.Drawing.Point(287, 108);
            this.tbNumPerFloor.Name = "tbNumPerFloor";
            this.tbNumPerFloor.Size = new System.Drawing.Size(118, 21);
            this.tbNumPerFloor.TabIndex = 8;
            this.tbNumPerFloor.Text = "1.0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(203, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 12);
            this.label14.TabIndex = 7;
            this.label14.Text = "容差限制(%)：";
            // 
            // cbNumPerFloor
            // 
            this.cbNumPerFloor.AutoSize = true;
            this.cbNumPerFloor.Location = new System.Drawing.Point(100, 110);
            this.cbNumPerFloor.Name = "cbNumPerFloor";
            this.cbNumPerFloor.Size = new System.Drawing.Size(72, 16);
            this.cbNumPerFloor.TabIndex = 6;
            this.cbNumPerFloor.Text = "检查限制";
            this.cbNumPerFloor.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 111);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 5;
            this.label15.Text = "百分比：";
            // 
            // tbNumAbValueFloor
            // 
            this.tbNumAbValueFloor.Location = new System.Drawing.Point(287, 65);
            this.tbNumAbValueFloor.Name = "tbNumAbValueFloor";
            this.tbNumAbValueFloor.Size = new System.Drawing.Size(118, 21);
            this.tbNumAbValueFloor.TabIndex = 4;
            this.tbNumAbValueFloor.Text = "1.0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(220, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 3;
            this.label13.Text = "数量：";
            // 
            // cbNumAbValueFloor
            // 
            this.cbNumAbValueFloor.AutoSize = true;
            this.cbNumAbValueFloor.Location = new System.Drawing.Point(100, 67);
            this.cbNumAbValueFloor.Name = "cbNumAbValueFloor";
            this.cbNumAbValueFloor.Size = new System.Drawing.Size(72, 16);
            this.cbNumAbValueFloor.TabIndex = 2;
            this.cbNumAbValueFloor.Text = "检查限制";
            this.cbNumAbValueFloor.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 1;
            this.label12.Text = "绝对值：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "下限：";
            // 
            // tpPrice
            // 
            this.tpPrice.Controls.Add(this.tbPricePerTop);
            this.tpPrice.Controls.Add(this.label21);
            this.tpPrice.Controls.Add(this.cbPricePerTop);
            this.tpPrice.Controls.Add(this.label22);
            this.tpPrice.Controls.Add(this.tbPriceAbValueTop);
            this.tpPrice.Controls.Add(this.label23);
            this.tpPrice.Controls.Add(this.cbPriceAbValueTop);
            this.tpPrice.Controls.Add(this.label24);
            this.tpPrice.Controls.Add(this.label25);
            this.tpPrice.Controls.Add(this.tbPricePerFloor);
            this.tpPrice.Controls.Add(this.label26);
            this.tpPrice.Controls.Add(this.cbPricePerFloor);
            this.tpPrice.Controls.Add(this.label27);
            this.tpPrice.Controls.Add(this.tbPriceAbValueFloor);
            this.tpPrice.Controls.Add(this.label28);
            this.tpPrice.Controls.Add(this.cbPriceAbValueFloor);
            this.tpPrice.Controls.Add(this.label29);
            this.tpPrice.Controls.Add(this.label30);
            this.tpPrice.Location = new System.Drawing.Point(4, 22);
            this.tpPrice.Name = "tpPrice";
            this.tpPrice.Padding = new System.Windows.Forms.Padding(3);
            this.tpPrice.Size = new System.Drawing.Size(611, 471);
            this.tpPrice.TabIndex = 1;
            this.tpPrice.Text = "价格";
            this.tpPrice.UseVisualStyleBackColor = true;
            // 
            // tbPricePerTop
            // 
            this.tbPricePerTop.Location = new System.Drawing.Point(287, 230);
            this.tbPricePerTop.Name = "tbPricePerTop";
            this.tbPricePerTop.Size = new System.Drawing.Size(118, 21);
            this.tbPricePerTop.TabIndex = 35;
            this.tbPricePerTop.Text = "1.0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(203, 233);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 12);
            this.label21.TabIndex = 34;
            this.label21.Text = "容差限制(%)：";
            // 
            // cbPricePerTop
            // 
            this.cbPricePerTop.AutoSize = true;
            this.cbPricePerTop.Location = new System.Drawing.Point(100, 232);
            this.cbPricePerTop.Name = "cbPricePerTop";
            this.cbPricePerTop.Size = new System.Drawing.Size(72, 16);
            this.cbPricePerTop.TabIndex = 33;
            this.cbPricePerTop.Text = "检查限制";
            this.cbPricePerTop.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(19, 233);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 32;
            this.label22.Text = "百分比：";
            // 
            // tbPriceAbValueTop
            // 
            this.tbPriceAbValueTop.Location = new System.Drawing.Point(287, 187);
            this.tbPriceAbValueTop.Name = "tbPriceAbValueTop";
            this.tbPriceAbValueTop.Size = new System.Drawing.Size(118, 21);
            this.tbPriceAbValueTop.TabIndex = 31;
            this.tbPriceAbValueTop.Text = "1.0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(220, 189);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 30;
            this.label23.Text = "数量：";
            // 
            // cbPriceAbValueTop
            // 
            this.cbPriceAbValueTop.AutoSize = true;
            this.cbPriceAbValueTop.Location = new System.Drawing.Point(100, 189);
            this.cbPriceAbValueTop.Name = "cbPriceAbValueTop";
            this.cbPriceAbValueTop.Size = new System.Drawing.Size(72, 16);
            this.cbPriceAbValueTop.TabIndex = 29;
            this.cbPriceAbValueTop.Text = "检查限制";
            this.cbPriceAbValueTop.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(19, 190);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 28;
            this.label24.Text = "绝对值：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(19, 152);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 12);
            this.label25.TabIndex = 27;
            this.label25.Text = "上限：";
            // 
            // tbPricePerFloor
            // 
            this.tbPricePerFloor.Location = new System.Drawing.Point(287, 108);
            this.tbPricePerFloor.Name = "tbPricePerFloor";
            this.tbPricePerFloor.Size = new System.Drawing.Size(118, 21);
            this.tbPricePerFloor.TabIndex = 26;
            this.tbPricePerFloor.Text = "1.0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(203, 111);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 12);
            this.label26.TabIndex = 25;
            this.label26.Text = "容差限制(%)：";
            // 
            // cbPricePerFloor
            // 
            this.cbPricePerFloor.AutoSize = true;
            this.cbPricePerFloor.Location = new System.Drawing.Point(100, 110);
            this.cbPricePerFloor.Name = "cbPricePerFloor";
            this.cbPricePerFloor.Size = new System.Drawing.Size(72, 16);
            this.cbPricePerFloor.TabIndex = 24;
            this.cbPricePerFloor.Text = "检查限制";
            this.cbPricePerFloor.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(19, 111);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 23;
            this.label27.Text = "百分比：";
            // 
            // tbPriceAbValueFloor
            // 
            this.tbPriceAbValueFloor.Location = new System.Drawing.Point(287, 65);
            this.tbPriceAbValueFloor.Name = "tbPriceAbValueFloor";
            this.tbPriceAbValueFloor.Size = new System.Drawing.Size(118, 21);
            this.tbPriceAbValueFloor.TabIndex = 22;
            this.tbPriceAbValueFloor.Text = "1.0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(220, 67);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 12);
            this.label28.TabIndex = 21;
            this.label28.Text = "数量：";
            // 
            // cbPriceAbValueFloor
            // 
            this.cbPriceAbValueFloor.AutoSize = true;
            this.cbPriceAbValueFloor.Location = new System.Drawing.Point(100, 67);
            this.cbPriceAbValueFloor.Name = "cbPriceAbValueFloor";
            this.cbPriceAbValueFloor.Size = new System.Drawing.Size(72, 16);
            this.cbPriceAbValueFloor.TabIndex = 20;
            this.cbPriceAbValueFloor.Text = "检查限制";
            this.cbPriceAbValueFloor.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(19, 68);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 19;
            this.label29.Text = "绝对值：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(19, 30);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 18;
            this.label30.Text = "下限：";
            // 
            // tpAmount
            // 
            this.tpAmount.Controls.Add(this.tbAmountPerTop);
            this.tpAmount.Controls.Add(this.label31);
            this.tpAmount.Controls.Add(this.cbAmountPerTop);
            this.tpAmount.Controls.Add(this.label32);
            this.tpAmount.Controls.Add(this.tbAmountAbValueTop);
            this.tpAmount.Controls.Add(this.label33);
            this.tpAmount.Controls.Add(this.cbAmountAbValueTop);
            this.tpAmount.Controls.Add(this.label34);
            this.tpAmount.Controls.Add(this.label35);
            this.tpAmount.Controls.Add(this.tbAmountPerFloor);
            this.tpAmount.Controls.Add(this.label36);
            this.tpAmount.Controls.Add(this.cbAmountPerFloor);
            this.tpAmount.Controls.Add(this.label37);
            this.tpAmount.Controls.Add(this.tbAmountAbValueFloor);
            this.tpAmount.Controls.Add(this.label38);
            this.tpAmount.Controls.Add(this.cbAmountAbValueFloor);
            this.tpAmount.Controls.Add(this.label39);
            this.tpAmount.Controls.Add(this.label40);
            this.tpAmount.Location = new System.Drawing.Point(4, 22);
            this.tpAmount.Name = "tpAmount";
            this.tpAmount.Size = new System.Drawing.Size(611, 471);
            this.tpAmount.TabIndex = 2;
            this.tpAmount.Text = "金额";
            this.tpAmount.UseVisualStyleBackColor = true;
            // 
            // tbAmountPerTop
            // 
            this.tbAmountPerTop.Location = new System.Drawing.Point(287, 230);
            this.tbAmountPerTop.Name = "tbAmountPerTop";
            this.tbAmountPerTop.Size = new System.Drawing.Size(118, 21);
            this.tbAmountPerTop.TabIndex = 35;
            this.tbAmountPerTop.Text = "1.0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(203, 233);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 12);
            this.label31.TabIndex = 34;
            this.label31.Text = "容差限制(%)：";
            // 
            // cbAmountPerTop
            // 
            this.cbAmountPerTop.AutoSize = true;
            this.cbAmountPerTop.Location = new System.Drawing.Point(100, 232);
            this.cbAmountPerTop.Name = "cbAmountPerTop";
            this.cbAmountPerTop.Size = new System.Drawing.Size(72, 16);
            this.cbAmountPerTop.TabIndex = 33;
            this.cbAmountPerTop.Text = "检查限制";
            this.cbAmountPerTop.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(19, 233);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 32;
            this.label32.Text = "百分比：";
            // 
            // tbAmountAbValueTop
            // 
            this.tbAmountAbValueTop.Location = new System.Drawing.Point(287, 187);
            this.tbAmountAbValueTop.Name = "tbAmountAbValueTop";
            this.tbAmountAbValueTop.Size = new System.Drawing.Size(118, 21);
            this.tbAmountAbValueTop.TabIndex = 31;
            this.tbAmountAbValueTop.Text = "1.0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(220, 189);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 30;
            this.label33.Text = "数量：";
            // 
            // cbAmountAbValueTop
            // 
            this.cbAmountAbValueTop.AutoSize = true;
            this.cbAmountAbValueTop.Location = new System.Drawing.Point(100, 189);
            this.cbAmountAbValueTop.Name = "cbAmountAbValueTop";
            this.cbAmountAbValueTop.Size = new System.Drawing.Size(72, 16);
            this.cbAmountAbValueTop.TabIndex = 29;
            this.cbAmountAbValueTop.Text = "检查限制";
            this.cbAmountAbValueTop.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(19, 190);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 28;
            this.label34.Text = "绝对值：";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(19, 152);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 12);
            this.label35.TabIndex = 27;
            this.label35.Text = "上限：";
            // 
            // tbAmountPerFloor
            // 
            this.tbAmountPerFloor.Location = new System.Drawing.Point(287, 108);
            this.tbAmountPerFloor.Name = "tbAmountPerFloor";
            this.tbAmountPerFloor.Size = new System.Drawing.Size(118, 21);
            this.tbAmountPerFloor.TabIndex = 26;
            this.tbAmountPerFloor.Text = "1.0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(203, 111);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(83, 12);
            this.label36.TabIndex = 25;
            this.label36.Text = "容差限制(%)：";
            // 
            // cbAmountPerFloor
            // 
            this.cbAmountPerFloor.AutoSize = true;
            this.cbAmountPerFloor.Location = new System.Drawing.Point(100, 110);
            this.cbAmountPerFloor.Name = "cbAmountPerFloor";
            this.cbAmountPerFloor.Size = new System.Drawing.Size(72, 16);
            this.cbAmountPerFloor.TabIndex = 24;
            this.cbAmountPerFloor.Text = "检查限制";
            this.cbAmountPerFloor.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(19, 111);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 12);
            this.label37.TabIndex = 23;
            this.label37.Text = "百分比：";
            // 
            // tbAmountAbValueFloor
            // 
            this.tbAmountAbValueFloor.Location = new System.Drawing.Point(287, 65);
            this.tbAmountAbValueFloor.Name = "tbAmountAbValueFloor";
            this.tbAmountAbValueFloor.Size = new System.Drawing.Size(118, 21);
            this.tbAmountAbValueFloor.TabIndex = 22;
            this.tbAmountAbValueFloor.Text = "1.0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(220, 67);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 21;
            this.label38.Text = "数量：";
            // 
            // cbAmountAbValueFloor
            // 
            this.cbAmountAbValueFloor.AutoSize = true;
            this.cbAmountAbValueFloor.Location = new System.Drawing.Point(100, 67);
            this.cbAmountAbValueFloor.Name = "cbAmountAbValueFloor";
            this.cbAmountAbValueFloor.Size = new System.Drawing.Size(72, 16);
            this.cbAmountAbValueFloor.TabIndex = 20;
            this.cbAmountAbValueFloor.Text = "检查限制";
            this.cbAmountAbValueFloor.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(19, 68);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 19;
            this.label39.Text = "绝对值：";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(19, 30);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 12);
            this.label40.TabIndex = 18;
            this.label40.Text = "下限：";
            // 
            // InvoiceToleranceSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 613);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "InvoiceToleranceSettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "发票容差设置";
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpNum.ResumeLayout(false);
            this.tpNum.PerformLayout();
            this.tpPrice.ResumeLayout(false);
            this.tpPrice.PerformLayout();
            this.tpAmount.ResumeLayout(false);
            this.tpAmount.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpNum;
        private System.Windows.Forms.TextBox tbNumPerTop;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox cbNumPerTop;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbNumAbValueTop;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cbNumAbValueTop;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbNumPerFloor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cbNumPerFloor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbNumAbValueFloor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbNumAbValueFloor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tpPrice;
        private System.Windows.Forms.TextBox tbPricePerTop;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox cbPricePerTop;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbPriceAbValueTop;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox cbPriceAbValueTop;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbPricePerFloor;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox cbPricePerFloor;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbPriceAbValueFloor;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox cbPriceAbValueFloor;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tpAmount;
        private System.Windows.Forms.TextBox tbAmountPerTop;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox cbAmountPerTop;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbAmountAbValueTop;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox cbAmountAbValueTop;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbAmountPerFloor;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox cbAmountPerFloor;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbAmountAbValueFloor;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox cbAmountAbValueFloor;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;

    }
}