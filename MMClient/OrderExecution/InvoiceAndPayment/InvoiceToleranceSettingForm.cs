﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.PurchaseOrderExecution;
using Lib.Bll.PurchaseOrderExecutionBLL;
using Lib.Common.CommonUtils;

namespace MMClient.OrderExecution.InvoiceAndPayment
{
    public partial class InvoiceToleranceSettingForm : DockContent
    {
        // 保存设置内容
        private List<Invoice_Tolerance> invoiceToleranceList = null;
        // 
        InvoiceToleranceBLL invoiceToleranceTool = new InvoiceToleranceBLL();

        public InvoiceToleranceSettingForm()
        {
            InitializeComponent();

            invoiceToleranceList = invoiceToleranceTool.getInvoiceTolerance();
            initialSetting();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initialSetting() {
            foreach (Invoice_Tolerance tolerance in invoiceToleranceList) {
                // 数量
                if (tolerance.Param_Name.Equals("Num")) {
                    // 下限
                    if (tolerance.Border == 0)
                    {
                        this.tbNumAbValueFloor.Text = Convert.ToString(tolerance.AbValue);
                        this.cbNumAbValueFloor.Checked = tolerance.AbValue_Is_Check;
                        this.tbNumPerFloor.Text = Convert.ToString(tolerance.Per);
                        this.cbNumPerFloor.Checked = tolerance.Per_Is_Check;
                    }
                    // 上限
                    else {
                        this.tbNumAbValueTop.Text = Convert.ToString(tolerance.AbValue);
                        this.cbNumAbValueTop.Checked = tolerance.AbValue_Is_Check;
                        this.tbNumPerTop.Text = Convert.ToString(tolerance.Per);
                        this.cbNumPerTop.Checked = tolerance.Per_Is_Check;
                    }
                }
                // 价格
                else if (tolerance.Param_Name.Equals("Price")) {
                    // 下限
                    if (tolerance.Border == 0)
                    {
                        this.tbPriceAbValueFloor.Text = Convert.ToString(tolerance.AbValue);
                        this.cbPriceAbValueFloor.Checked = tolerance.AbValue_Is_Check;
                        this.tbPricePerFloor.Text = Convert.ToString(tolerance.Per);
                        this.cbPricePerFloor.Checked = tolerance.Per_Is_Check;
                    }
                    // 上限
                    else
                    {
                        this.tbPriceAbValueTop.Text = Convert.ToString(tolerance.AbValue);
                        this.cbPriceAbValueTop.Checked = tolerance.AbValue_Is_Check;
                        this.tbPricePerTop.Text = Convert.ToString(tolerance.Per);
                        this.cbPricePerTop.Checked = tolerance.Per_Is_Check;
                    }
                }
                // 金额
                else if (tolerance.Param_Name.Equals("Amount")) {
                    // 下限
                    if (tolerance.Border == 0)
                    {
                        this.tbAmountAbValueFloor.Text = Convert.ToString(tolerance.AbValue);
                        this.cbAmountAbValueFloor.Checked = tolerance.AbValue_Is_Check;
                        this.tbAmountPerFloor.Text = Convert.ToString(tolerance.Per);
                        this.cbAmountPerFloor.Checked = tolerance.Per_Is_Check;
                    }
                    // 上限
                    else
                    {
                        this.tbAmountAbValueTop.Text = Convert.ToString(tolerance.AbValue);
                        this.cbAmountAbValueTop.Checked = tolerance.AbValue_Is_Check;
                        this.tbAmountPerTop.Text = Convert.ToString(tolerance.Per);
                        this.cbAmountPerTop.Checked = tolerance.Per_Is_Check;
                    }
                }
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                # region 读取

                foreach (Invoice_Tolerance tolerance in invoiceToleranceList)
                {
                    // 数量
                    if (tolerance.Param_Name.Equals("Num"))
                    {
                        // 下限
                        if (tolerance.Border == 0)
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbNumAbValueFloor.Text);
                            tolerance.AbValue_Is_Check = this.cbNumAbValueFloor.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbNumPerFloor.Text);
                            tolerance.Per_Is_Check = this.cbNumPerFloor.Checked;
                        }
                        // 上限
                        else
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbNumAbValueTop.Text);
                            tolerance.AbValue_Is_Check = this.cbNumAbValueTop.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbNumPerTop.Text);
                            tolerance.Per_Is_Check = this.cbNumPerTop.Checked;
                        }
                    }
                    // 价格
                    else if (tolerance.Param_Name.Equals("Price"))
                    {
                        // 下限
                        if (tolerance.Border == 0)
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbPriceAbValueFloor.Text);
                            tolerance.AbValue_Is_Check = this.cbPriceAbValueFloor.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbPricePerFloor.Text);
                            tolerance.Per_Is_Check = this.cbPricePerFloor.Checked;
                        }
                        // 上限
                        else
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbPriceAbValueTop.Text);
                            tolerance.AbValue_Is_Check = this.cbPriceAbValueTop.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbPricePerTop.Text);
                            tolerance.Per_Is_Check = this.cbPricePerTop.Checked;
                        }
                    }
                    // 金额
                    else if (tolerance.Param_Name.Equals("Amount"))
                    {
                        // 下限
                        if (tolerance.Border == 0)
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbAmountAbValueFloor.Text);
                            tolerance.AbValue_Is_Check = this.cbAmountAbValueFloor.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbAmountPerFloor.Text);
                            tolerance.Per_Is_Check = this.cbAmountPerFloor.Checked;
                        }
                        // 上限
                        else
                        {
                            tolerance.AbValue = DataGridViewCellTool.convertStringToDouble(this.tbAmountAbValueTop.Text);
                            tolerance.AbValue_Is_Check = this.cbAmountAbValueTop.Checked;
                            tolerance.Per = DataGridViewCellTool.convertStringToDouble(this.tbAmountPerTop.Text);
                            tolerance.Per_Is_Check = this.cbAmountPerTop.Checked;
                        }
                    }
                }

                #endregion
            }
            catch (Exception) {
                MessageUtil.ShowError("请正确填写！");
            }

            int result = invoiceToleranceTool.updateInvoiceTolerance(invoiceToleranceList);
            MessageBox.Show("保存成功！");
        }
    }
}
