﻿namespace MMClient.OrderExecution.InvoiceAndPayment
{
    partial class InvoiceCheckResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbPOID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSumAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbArrivalAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbInvoiceAmount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCheckResult = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rtbReason = new System.Windows.Forms.RichTextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lblCurrency1 = new System.Windows.Forms.Label();
            this.lblCurrency2 = new System.Windows.Forms.Label();
            this.lblCurrency3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 31);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "订单编号：";
            // 
            // tbPOID
            // 
            this.tbPOID.Enabled = false;
            this.tbPOID.Location = new System.Drawing.Point(107, 28);
            this.tbPOID.Name = "tbPOID";
            this.tbPOID.Size = new System.Drawing.Size(244, 21);
            this.tbPOID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "订单金额：";
            // 
            // tbSumAmount
            // 
            this.tbSumAmount.Enabled = false;
            this.tbSumAmount.Location = new System.Drawing.Point(107, 69);
            this.tbSumAmount.Name = "tbSumAmount";
            this.tbSumAmount.Size = new System.Drawing.Size(178, 21);
            this.tbSumAmount.TabIndex = 3;
            this.tbSumAmount.TextChanged += new System.EventHandler(this.tbSumAmount_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "到货金额：";
            // 
            // tbArrivalAmount
            // 
            this.tbArrivalAmount.Enabled = false;
            this.tbArrivalAmount.Location = new System.Drawing.Point(107, 110);
            this.tbArrivalAmount.Name = "tbArrivalAmount";
            this.tbArrivalAmount.Size = new System.Drawing.Size(178, 21);
            this.tbArrivalAmount.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "发票金额：";
            // 
            // tbInvoiceAmount
            // 
            this.tbInvoiceAmount.Enabled = false;
            this.tbInvoiceAmount.Location = new System.Drawing.Point(108, 148);
            this.tbInvoiceAmount.Name = "tbInvoiceAmount";
            this.tbInvoiceAmount.Size = new System.Drawing.Size(177, 21);
            this.tbInvoiceAmount.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "校验结果：";
            // 
            // tbCheckResult
            // 
            this.tbCheckResult.Enabled = false;
            this.tbCheckResult.Location = new System.Drawing.Point(107, 187);
            this.tbCheckResult.Name = "tbCheckResult";
            this.tbCheckResult.Size = new System.Drawing.Size(244, 21);
            this.tbCheckResult.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "原    因：";
            // 
            // rtbReason
            // 
            this.rtbReason.Enabled = false;
            this.rtbReason.Location = new System.Drawing.Point(107, 230);
            this.rtbReason.Name = "rtbReason";
            this.rtbReason.Size = new System.Drawing.Size(244, 150);
            this.rtbReason.TabIndex = 11;
            this.rtbReason.Text = "";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(276, 419);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 12;
            this.btnConfirm.Text = "确定";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // lblCurrency1
            // 
            this.lblCurrency1.AutoSize = true;
            this.lblCurrency1.Location = new System.Drawing.Point(310, 72);
            this.lblCurrency1.Name = "lblCurrency1";
            this.lblCurrency1.Size = new System.Drawing.Size(41, 12);
            this.lblCurrency1.TabIndex = 13;
            this.lblCurrency1.Text = "label7";
            // 
            // lblCurrency2
            // 
            this.lblCurrency2.AutoSize = true;
            this.lblCurrency2.Location = new System.Drawing.Point(310, 113);
            this.lblCurrency2.Name = "lblCurrency2";
            this.lblCurrency2.Size = new System.Drawing.Size(41, 12);
            this.lblCurrency2.TabIndex = 14;
            this.lblCurrency2.Text = "label8";
            // 
            // lblCurrency3
            // 
            this.lblCurrency3.AutoSize = true;
            this.lblCurrency3.Location = new System.Drawing.Point(310, 151);
            this.lblCurrency3.Name = "lblCurrency3";
            this.lblCurrency3.Size = new System.Drawing.Size(41, 12);
            this.lblCurrency3.TabIndex = 15;
            this.lblCurrency3.Text = "label9";
            // 
            // InvoiceCheckResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 474);
            this.Controls.Add(this.lblCurrency3);
            this.Controls.Add(this.lblCurrency2);
            this.Controls.Add(this.lblCurrency1);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.rtbReason);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbCheckResult);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbInvoiceAmount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbArrivalAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbSumAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPOID);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "InvoiceCheckResultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "校验结果";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPOID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSumAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbArrivalAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbInvoiceAmount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbCheckResult;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rtbReason;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label lblCurrency1;
        private System.Windows.Forms.Label lblCurrency2;
        private System.Windows.Forms.Label lblCurrency3;
    }
}