﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.PurchaseOrderExecution;
using Lib.Bll.PurchaseOrderExecutionBLL;

namespace MMClient.OrderExecution.InvoiceAndPayment
{
    public partial class InvoiceCheckResultForm : Form
    {
        //发票列表
        private List<Invoice> invoiceList = null;
        private InvoiceToleranceBLL invoiceToleranceTool =
            new InvoiceToleranceBLL();
        private List<Invoice_Tolerance> invoiceToleranceList = null;
        private string POID;

        public InvoiceCheckResultForm(List<Invoice> _invoiceList,
            string _POID)
        {
            InitializeComponent();

            invoiceList = _invoiceList;
            this.POID = _POID;
            invoiceToleranceList = invoiceToleranceTool.getInvoiceTolerance();
            
            checkInvoice();
        }

        /// <summary>
        /// 校验
        /// </summary>
        private void checkInvoice() { 
            double invoiceSum = 0.0;
            foreach(Invoice invoice in this.invoiceList){
                invoiceSum += invoice.Sum;
            }
            //显示校验结果
            this.tbPOID.Text = POID;
            string currency = "CNY";
            this.lblCurrency1.Text = currency;
            this.lblCurrency2.Text = currency;
            this.lblCurrency3.Text = currency;
            this.tbSumAmount.Text = "10000";
            double POArrivalAmount = invoiceSum + 20;
            this.tbArrivalAmount.Text = Convert.ToString(POArrivalAmount);
            this.tbInvoiceAmount.Text = Convert.ToString(invoiceSum);

            StringBuilder reason = new StringBuilder();
            # region 检测
            foreach (Invoice_Tolerance tolerance in invoiceToleranceList) {
                if (tolerance.Param_Name.Equals("Amount")) {
                    //金额
                    if (tolerance.Border == 0)
                    {
                        //下限
                        if (tolerance.AbValue_Is_Check)
                        {
                            //绝对的
                            double result = POArrivalAmount - invoiceSum;
                            if (result > tolerance.AbValue)
                            {
                                //超标
                                reason.Append("超过金额值检测下限\r\n");
                            }
                        }
                        if (tolerance.Per_Is_Check)
                        {
                            //百分比
                            double result = POArrivalAmount * (100 - tolerance.Per) / 100;
                            if (result > invoiceSum)
                            {
                                //超标
                                reason.Append("超过金额百分比检测下限\r\n");
                            }
                        }
                    }
                    else {
                        // 上限
                        if (tolerance.AbValue_Is_Check)
                        {
                            // 绝对的
                            double result = invoiceSum - POArrivalAmount;
                            if (result > tolerance.AbValue)
                            {
                                //超标
                                reason.Append("超过金额值检测上限\r\n");
                            }
                        }
                        if (tolerance.Per_Is_Check)
                        {
                            //百分比
                            double result = POArrivalAmount * (100 + tolerance.Per) / 100;
                            if (result < invoiceSum)
                            {
                                //超标
                                reason.Append("超过金额百分比检测上限\r\n");
                            }
                        }
                    }
                }
                else if (tolerance.Param_Name.Equals("Price")) { 
                    //价格
                }
                else if (tolerance.Param_Name.Equals("Num")) { 
                    //数量
                }
            }
            #endregion

            if (!reason.ToString().Equals(""))
            {
                this.tbCheckResult.Text = "校验未通过";
                this.rtbReason.Text = reason.ToString();
            }
            else {
                this.tbCheckResult.Text = "校验通过";
            }
        }

        /// <summary>
        /// 点击确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            //保存

            this.Close();
        }

        private void tbSumAmount_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
