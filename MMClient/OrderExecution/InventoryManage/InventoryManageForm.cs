﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.OrderExecution.InventoryManage
{
    public partial class InventoryManageForm : DockContent
    {
        DataTable dt = new DataTable();
        public InventoryManageForm()
        {
            InitializeComponent();
        }

        private void InventoryManageForm_Load(object sender, EventArgs e)
        {
            //下面代码无用，为了写论文截图
            //默认添加些数据
            //dt = new DataTable();
            dt.Columns.Add("序号", typeof(string));
            dt.Columns.Add("仓库代码", typeof(string));
            dt.Columns.Add("仓库名称", typeof(string));
            dt.Columns.Add("库存类型", typeof(string));
            dt.Columns.Add("库存状态", typeof(string));
            dt.Columns.Add("物料编号", typeof(string));
            dt.Columns.Add("物料名称", typeof(string));

            dt.Rows.Add(new object[] { "001", "20140032", "武汉仓库", "质检库存", "特殊库存", "20140412001", "铝合金" });
            dt.Rows.Add(new object[] { "002", "20140032", "武汉仓库", "非限制性库存", "非特殊库存", "20140412002", "螺纹钢" });
            dt.Rows.Add(new object[] { "003", "20140032", "武汉仓库", "非限制性库存", "非特殊库存", "20140412003", "盘条" });
        }
    }
}
