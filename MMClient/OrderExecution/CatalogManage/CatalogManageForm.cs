﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.OrderExecution.CatalogManage
{
    public partial class CatalogManageForm : DockContent
    {
        DataTable dt = new DataTable();

        public CatalogManageForm()
        {
            InitializeComponent();
        }

        private void CatalogManageForm_Load(object sender, EventArgs e)
        {
            //下面代码无用，为了写论文截图
            //默认添加些数据
            //dt = new DataTable();
            dt.Columns.Add("产品编号", typeof(string));
            dt.Columns.Add("产品名称", typeof(string));
            dt.Columns.Add("产品类别", typeof(string));
            dt.Columns.Add("供应商", typeof(string));
            dt.Columns.Add("供货地点", typeof(string));
            dt.Columns.Add("单价", typeof(string));
            dt.Columns.Add("库存数量", typeof(string));

            dt.Rows.Add(new object[] { "001", "中性笔", "文具", "晨光文具公司", "武汉", "2.0(￥)", "234" });
            dt.Rows.Add(new object[] { "002", "A4书写纸", "文具", "晨光文具公司", "武汉", "1.0(￥)", "500" });
            dt.Rows.Add(new object[] { "003", "橡皮", "文具", "晨光文具公司", "武汉", "1.0(￥)", "100" });

        }
    }
}
