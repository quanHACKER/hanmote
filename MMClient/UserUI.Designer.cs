namespace MMClient
{
    partial class UserUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserUI));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("新建物料数据");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("会计视图");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("采购视图");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("存储视图");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("建立批次");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("查询批次");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("批次管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("分类视图");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("物料主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode8,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("采购组织视图");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("公司代码视图");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("市场价格记录");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("记录查看");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("添加记录");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("采购记录", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("供应商主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("生成会计凭证");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("财务主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode19});
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("维护物料组");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("维护物料类型");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("维护货币");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("维护国家信息");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("维护付款条件");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("维护计量单位");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("维护评估类");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("维护公司代码");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("维护工厂");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("维护库存地");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("维护采购组");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("维护采购组织");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("维护产品组");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("维护交付条件");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("维护成本中心");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("维护工业标识");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("物料组分配物料");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("采购组织分配物料组");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("维护评估要素");
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("维护目标值和最低值");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("维护策略信息库");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("维护证书类型");
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("维护税率");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("一般设置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25,
            treeNode26,
            treeNode27,
            treeNode28,
            treeNode29,
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33,
            treeNode34,
            treeNode35,
            treeNode36,
            treeNode37,
            treeNode38,
            treeNode39,
            treeNode40,
            treeNode41,
            treeNode42,
            treeNode43});
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("数据管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode18,
            treeNode20,
            treeNode44});
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("查看计划");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("计划列表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode46});
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("新建");
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("查看");
            System.Windows.Forms.TreeNode treeNode51 = new System.Windows.Forms.TreeNode("删除");
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("申请部门", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode48,
            treeNode49,
            treeNode50,
            treeNode51});
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("审核");
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("汇总");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("生成");
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("寻源");
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("采购部门", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode53,
            treeNode54,
            treeNode55,
            treeNode56});
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("计划管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode47,
            treeNode52,
            treeNode57});
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("执行货源");
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("寻源舱", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode59});
            System.Windows.Forms.TreeNode treeNode61 = new System.Windows.Forms.TreeNode("清单列表");
            System.Windows.Forms.TreeNode treeNode62 = new System.Windows.Forms.TreeNode("新建源清单");
            System.Windows.Forms.TreeNode treeNode63 = new System.Windows.Forms.TreeNode("更改源清单");
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("源清单", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode61,
            treeNode62,
            treeNode63});
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("查看询价");
            System.Windows.Forms.TreeNode treeNode66 = new System.Windows.Forms.TreeNode("创建询价");
            System.Windows.Forms.TreeNode treeNode67 = new System.Windows.Forms.TreeNode("更改询价");
            System.Windows.Forms.TreeNode treeNode68 = new System.Windows.Forms.TreeNode("执行报价");
            System.Windows.Forms.TreeNode treeNode69 = new System.Windows.Forms.TreeNode("执行比价");
            System.Windows.Forms.TreeNode treeNode70 = new System.Windows.Forms.TreeNode("询价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode65,
            treeNode66,
            treeNode67,
            treeNode68,
            treeNode69});
            System.Windows.Forms.TreeNode treeNode71 = new System.Windows.Forms.TreeNode("查看招标");
            System.Windows.Forms.TreeNode treeNode72 = new System.Windows.Forms.TreeNode("创建招标");
            System.Windows.Forms.TreeNode treeNode73 = new System.Windows.Forms.TreeNode("发布招标");
            System.Windows.Forms.TreeNode treeNode74 = new System.Windows.Forms.TreeNode("修改招标");
            System.Windows.Forms.TreeNode treeNode75 = new System.Windows.Forms.TreeNode("审批招标");
            System.Windows.Forms.TreeNode treeNode76 = new System.Windows.Forms.TreeNode("处理投标");
            System.Windows.Forms.TreeNode treeNode77 = new System.Windows.Forms.TreeNode("进行评标");
            System.Windows.Forms.TreeNode treeNode78 = new System.Windows.Forms.TreeNode("查看谈判");
            System.Windows.Forms.TreeNode treeNode79 = new System.Windows.Forms.TreeNode("创建谈判");
            System.Windows.Forms.TreeNode treeNode80 = new System.Windows.Forms.TreeNode("处理谈判");
            System.Windows.Forms.TreeNode treeNode81 = new System.Windows.Forms.TreeNode("谈判", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode78,
            treeNode79,
            treeNode80});
            System.Windows.Forms.TreeNode treeNode82 = new System.Windows.Forms.TreeNode("招标", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode71,
            treeNode72,
            treeNode73,
            treeNode74,
            treeNode75,
            treeNode76,
            treeNode77,
            treeNode81});
            System.Windows.Forms.TreeNode treeNode83 = new System.Windows.Forms.TreeNode("竞价");
            System.Windows.Forms.TreeNode treeNode84 = new System.Windows.Forms.TreeNode("竞拍");
            System.Windows.Forms.TreeNode treeNode85 = new System.Windows.Forms.TreeNode("拍卖", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode83,
            treeNode84});
            System.Windows.Forms.TreeNode treeNode86 = new System.Windows.Forms.TreeNode("历史信息记录");
            System.Windows.Forms.TreeNode treeNode87 = new System.Windows.Forms.TreeNode("采购信息记录", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode86});
            System.Windows.Forms.TreeNode treeNode88 = new System.Windows.Forms.TreeNode("条件类型");
            System.Windows.Forms.TreeNode treeNode89 = new System.Windows.Forms.TreeNode("查看条件类型");
            System.Windows.Forms.TreeNode treeNode90 = new System.Windows.Forms.TreeNode("创建条件类型");
            System.Windows.Forms.TreeNode treeNode91 = new System.Windows.Forms.TreeNode("更改条件类型");
            System.Windows.Forms.TreeNode treeNode92 = new System.Windows.Forms.TreeNode("条件定价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode88,
            treeNode89,
            treeNode90,
            treeNode91});
            System.Windows.Forms.TreeNode treeNode93 = new System.Windows.Forms.TreeNode("货源确定", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode60,
            treeNode64,
            treeNode70,
            treeNode82,
            treeNode85,
            treeNode87,
            treeNode92});
            System.Windows.Forms.TreeNode treeNode94 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode95 = new System.Windows.Forms.TreeNode("合同模板", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode94});
            System.Windows.Forms.TreeNode treeNode96 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode97 = new System.Windows.Forms.TreeNode("合同文本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode96});
            System.Windows.Forms.TreeNode treeNode98 = new System.Windows.Forms.TreeNode("合同管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode95,
            treeNode97});
            System.Windows.Forms.TreeNode treeNode99 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode100 = new System.Windows.Forms.TreeNode("框架协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode99});
            System.Windows.Forms.TreeNode treeNode101 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode102 = new System.Windows.Forms.TreeNode("配额协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode101});
            System.Windows.Forms.TreeNode treeNode103 = new System.Windows.Forms.TreeNode("自助采购", 1, 1);
            System.Windows.Forms.TreeNode treeNode104 = new System.Windows.Forms.TreeNode("生成订单");
            System.Windows.Forms.TreeNode treeNode105 = new System.Windows.Forms.TreeNode("订单管理");
            System.Windows.Forms.TreeNode treeNode106 = new System.Windows.Forms.TreeNode("发票管理");
            System.Windows.Forms.TreeNode treeNode107 = new System.Windows.Forms.TreeNode("订单管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode104,
            treeNode105,
            treeNode106});
            System.Windows.Forms.TreeNode treeNode108 = new System.Windows.Forms.TreeNode("供应目标和指标");
            System.Windows.Forms.TreeNode treeNode109 = new System.Windows.Forms.TreeNode("核心竞争力");
            System.Windows.Forms.TreeNode treeNode110 = new System.Windows.Forms.TreeNode("公司采购项目");
            System.Windows.Forms.TreeNode treeNode111 = new System.Windows.Forms.TreeNode("风险等级");
            System.Windows.Forms.TreeNode treeNode112 = new System.Windows.Forms.TreeNode("等级定位");
            System.Windows.Forms.TreeNode treeNode113 = new System.Windows.Forms.TreeNode("生产性物料组");
            System.Windows.Forms.TreeNode treeNode114 = new System.Windows.Forms.TreeNode("维护性物料组");
            System.Windows.Forms.TreeNode treeNode115 = new System.Windows.Forms.TreeNode("资产性物料组");
            System.Windows.Forms.TreeNode treeNode116 = new System.Windows.Forms.TreeNode("供应目标设置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode113,
            treeNode114,
            treeNode115});
            System.Windows.Forms.TreeNode treeNode117 = new System.Windows.Forms.TreeNode("物料定位", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode108,
            treeNode109,
            treeNode110,
            treeNode111,
            treeNode112,
            treeNode116});
            System.Windows.Forms.TreeNode treeNode118 = new System.Windows.Forms.TreeNode("供应商前期准备");
            System.Windows.Forms.TreeNode treeNode119 = new System.Windows.Forms.TreeNode("供应商认证", 1, 1);
            System.Windows.Forms.TreeNode treeNode120 = new System.Windows.Forms.TreeNode("供应商认证", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode118,
            treeNode119});
            System.Windows.Forms.TreeNode treeNode121 = new System.Windows.Forms.TreeNode("手工维护");
            System.Windows.Forms.TreeNode treeNode122 = new System.Windows.Forms.TreeNode("自动评估");
            System.Windows.Forms.TreeNode treeNode123 = new System.Windows.Forms.TreeNode("批量评估");
            System.Windows.Forms.TreeNode treeNode124 = new System.Windows.Forms.TreeNode("结果显示");
            System.Windows.Forms.TreeNode treeNode125 = new System.Windows.Forms.TreeNode("清单显示");
            System.Windows.Forms.TreeNode treeNode126 = new System.Windows.Forms.TreeNode("外部服务");
            System.Windows.Forms.TreeNode treeNode127 = new System.Windows.Forms.TreeNode("一般服务");
            System.Windows.Forms.TreeNode treeNode128 = new System.Windows.Forms.TreeNode("维护服务占比");
            System.Windows.Forms.TreeNode treeNode129 = new System.Windows.Forms.TreeNode("服务评估", new System.Windows.Forms.TreeNode[] {
            treeNode126,
            treeNode127,
            treeNode128});
            System.Windows.Forms.TreeNode treeNode130 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode131 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode132 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode133 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode134 = new System.Windows.Forms.TreeNode("交货评估", new System.Windows.Forms.TreeNode[] {
            treeNode130,
            treeNode131,
            treeNode132,
            treeNode133});
            System.Windows.Forms.TreeNode treeNode135 = new System.Windows.Forms.TreeNode("绩效评估", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode121,
            treeNode122,
            treeNode123,
            treeNode124,
            treeNode125,
            treeNode129,
            treeNode134});
            System.Windows.Forms.TreeNode treeNode136 = new System.Windows.Forms.TreeNode("体系/过程审核");
            System.Windows.Forms.TreeNode treeNode137 = new System.Windows.Forms.TreeNode("产品审核");
            System.Windows.Forms.TreeNode treeNode138 = new System.Windows.Forms.TreeNode("质量审核", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode136,
            treeNode137});
            System.Windows.Forms.TreeNode treeNode139 = new System.Windows.Forms.TreeNode("抱怨/拒绝情况录入");
            System.Windows.Forms.TreeNode treeNode140 = new System.Windows.Forms.TreeNode("服务情况录入");
            System.Windows.Forms.TreeNode treeNode141 = new System.Windows.Forms.TreeNode("收货问卷调查");
            System.Windows.Forms.TreeNode treeNode142 = new System.Windows.Forms.TreeNode("供应商营业额录入");
            System.Windows.Forms.TreeNode treeNode143 = new System.Windows.Forms.TreeNode("绩效评估数据维护", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode138,
            treeNode139,
            treeNode140,
            treeNode141,
            treeNode142});
            System.Windows.Forms.TreeNode treeNode144 = new System.Windows.Forms.TreeNode("执行分级");
            System.Windows.Forms.TreeNode treeNode145 = new System.Windows.Forms.TreeNode("分级查看");
            System.Windows.Forms.TreeNode treeNode146 = new System.Windows.Forms.TreeNode("分组查看分级结果");
            System.Windows.Forms.TreeNode treeNode147 = new System.Windows.Forms.TreeNode("供应商分级", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode144,
            treeNode145,
            treeNode146});
            System.Windows.Forms.TreeNode treeNode148 = new System.Windows.Forms.TreeNode("低效能供应商警告");
            System.Windows.Forms.TreeNode treeNode149 = new System.Windows.Forms.TreeNode("低效能供应商管理三级流程");
            System.Windows.Forms.TreeNode treeNode150 = new System.Windows.Forms.TreeNode("低效能供应商淘汰");
            System.Windows.Forms.TreeNode treeNode151 = new System.Windows.Forms.TreeNode("低效能供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode148,
            treeNode149,
            treeNode150});
            System.Windows.Forms.TreeNode treeNode152 = new System.Windows.Forms.TreeNode("降成本计算");
            System.Windows.Forms.TreeNode treeNode153 = new System.Windows.Forms.TreeNode("降成本结果查询");
            System.Windows.Forms.TreeNode treeNode154 = new System.Windows.Forms.TreeNode("采购降成本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode152,
            treeNode153});
            System.Windows.Forms.TreeNode treeNode155 = new System.Windows.Forms.TreeNode("供应商区分");
            System.Windows.Forms.TreeNode treeNode156 = new System.Windows.Forms.TreeNode("管理策略");
            System.Windows.Forms.TreeNode treeNode157 = new System.Windows.Forms.TreeNode("供应商价值", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode155,
            treeNode156});
            System.Windows.Forms.TreeNode treeNode158 = new System.Windows.Forms.TreeNode("供应商绩效比较");
            System.Windows.Forms.TreeNode treeNode159 = new System.Windows.Forms.TreeNode("基于物料组的供应商评估");
            System.Windows.Forms.TreeNode treeNode160 = new System.Windows.Forms.TreeNode("基于行业的供应商评估");
            System.Windows.Forms.TreeNode treeNode161 = new System.Windows.Forms.TreeNode("供应商对比雷达图");
            System.Windows.Forms.TreeNode treeNode162 = new System.Windows.Forms.TreeNode("供应商分析气泡图");
            System.Windows.Forms.TreeNode treeNode163 = new System.Windows.Forms.TreeNode("绩效评估报表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode158,
            treeNode159,
            treeNode160,
            treeNode161,
            treeNode162});
            System.Windows.Forms.TreeNode treeNode164 = new System.Windows.Forms.TreeNode("已认证供应商分析");
            System.Windows.Forms.TreeNode treeNode165 = new System.Windows.Forms.TreeNode("现行供应商业绩分析");
            System.Windows.Forms.TreeNode treeNode166 = new System.Windows.Forms.TreeNode("替代供应商分析");
            System.Windows.Forms.TreeNode treeNode167 = new System.Windows.Forms.TreeNode("供应商清单", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode164,
            treeNode165,
            treeNode166});
            System.Windows.Forms.TreeNode treeNode168 = new System.Windows.Forms.TreeNode("供货信息查看");
            System.Windows.Forms.TreeNode treeNode169 = new System.Windows.Forms.TreeNode("供货信息", new System.Windows.Forms.TreeNode[] {
            treeNode168});
            System.Windows.Forms.TreeNode treeNode170 = new System.Windows.Forms.TreeNode("供应商证书");
            System.Windows.Forms.TreeNode treeNode171 = new System.Windows.Forms.TreeNode("供应商表现", new System.Windows.Forms.TreeNode[] {
            treeNode170});
            System.Windows.Forms.TreeNode treeNode172 = new System.Windows.Forms.TreeNode("供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode117,
            treeNode120,
            treeNode135,
            treeNode143,
            treeNode147,
            treeNode151,
            treeNode154,
            treeNode157,
            treeNode163,
            treeNode167,
            treeNode169,
            treeNode171});
            System.Windows.Forms.TreeNode treeNode173 = new System.Windows.Forms.TreeNode("物料的ABC类分析");
            System.Windows.Forms.TreeNode treeNode174 = new System.Windows.Forms.TreeNode("供应商的ABC类分析");
            System.Windows.Forms.TreeNode treeNode175 = new System.Windows.Forms.TreeNode("采购监控分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode173,
            treeNode174});
            System.Windows.Forms.TreeNode treeNode176 = new System.Windows.Forms.TreeNode("采购花费分布");
            System.Windows.Forms.TreeNode treeNode177 = new System.Windows.Forms.TreeNode("成本结构分析");
            System.Windows.Forms.TreeNode treeNode178 = new System.Windows.Forms.TreeNode("成本比较分析");
            System.Windows.Forms.TreeNode treeNode179 = new System.Windows.Forms.TreeNode("成本趋势分析");
            System.Windows.Forms.TreeNode treeNode180 = new System.Windows.Forms.TreeNode("采购花费分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode176,
            treeNode177,
            treeNode178,
            treeNode179});
            System.Windows.Forms.TreeNode treeNode181 = new System.Windows.Forms.TreeNode("价格分析");
            System.Windows.Forms.TreeNode treeNode182 = new System.Windows.Forms.TreeNode("供应商竞价分析");
            System.Windows.Forms.TreeNode treeNode183 = new System.Windows.Forms.TreeNode("寻源分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode181,
            treeNode182});
            System.Windows.Forms.TreeNode treeNode184 = new System.Windows.Forms.TreeNode("合同管理分析");
            System.Windows.Forms.TreeNode treeNode185 = new System.Windows.Forms.TreeNode("合同执行分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode184});
            System.Windows.Forms.TreeNode treeNode186 = new System.Windows.Forms.TreeNode("供应商对比雷达图");
            System.Windows.Forms.TreeNode treeNode187 = new System.Windows.Forms.TreeNode("供应商趋势报表");
            System.Windows.Forms.TreeNode treeNode188 = new System.Windows.Forms.TreeNode("供应商得分排名");
            System.Windows.Forms.TreeNode treeNode189 = new System.Windows.Forms.TreeNode("供应商绩效分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode186,
            treeNode187,
            treeNode188});
            System.Windows.Forms.TreeNode treeNode190 = new System.Windows.Forms.TreeNode("库龄分析明细表");
            System.Windows.Forms.TreeNode treeNode191 = new System.Windows.Forms.TreeNode("库存物料ABC类分析");
            System.Windows.Forms.TreeNode treeNode192 = new System.Windows.Forms.TreeNode("库存分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode190,
            treeNode191});
            System.Windows.Forms.TreeNode treeNode193 = new System.Windows.Forms.TreeNode("供应商分析");
            System.Windows.Forms.TreeNode treeNode194 = new System.Windows.Forms.TreeNode("供应商依赖因素分析");
            System.Windows.Forms.TreeNode treeNode195 = new System.Windows.Forms.TreeNode("定制");
            System.Windows.Forms.TreeNode treeNode196 = new System.Windows.Forms.TreeNode("报表分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode175,
            treeNode180,
            treeNode183,
            treeNode185,
            treeNode189,
            treeNode192,
            treeNode193,
            treeNode194,
            treeNode195});
            System.Windows.Forms.TreeNode treeNode197 = new System.Windows.Forms.TreeNode("库存类型");
            System.Windows.Forms.TreeNode treeNode198 = new System.Windows.Forms.TreeNode("库存状态");
            System.Windows.Forms.TreeNode treeNode199 = new System.Windows.Forms.TreeNode("查看");
            System.Windows.Forms.TreeNode treeNode200 = new System.Windows.Forms.TreeNode("事务/事件");
            System.Windows.Forms.TreeNode treeNode201 = new System.Windows.Forms.TreeNode("移动类型", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode199,
            treeNode200});
            System.Windows.Forms.TreeNode treeNode202 = new System.Windows.Forms.TreeNode("基础配置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode197,
            treeNode198,
            treeNode201});
            System.Windows.Forms.TreeNode treeNode203 = new System.Windows.Forms.TreeNode("收货", 0, 0);
            System.Windows.Forms.TreeNode treeNode204 = new System.Windows.Forms.TreeNode("货物移动", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode203});
            System.Windows.Forms.TreeNode treeNode205 = new System.Windows.Forms.TreeNode("库存总览");
            System.Windows.Forms.TreeNode treeNode206 = new System.Windows.Forms.TreeNode("工厂库存");
            System.Windows.Forms.TreeNode treeNode207 = new System.Windows.Forms.TreeNode("仓库库存");
            System.Windows.Forms.TreeNode treeNode208 = new System.Windows.Forms.TreeNode("寄售库存");
            System.Windows.Forms.TreeNode treeNode209 = new System.Windows.Forms.TreeNode("在途库存");
            System.Windows.Forms.TreeNode treeNode210 = new System.Windows.Forms.TreeNode("收发存报表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode205,
            treeNode206,
            treeNode207,
            treeNode208,
            treeNode209});
            System.Windows.Forms.TreeNode treeNode211 = new System.Windows.Forms.TreeNode("手动设置安全库存");
            System.Windows.Forms.TreeNode treeNode212 = new System.Windows.Forms.TreeNode("查看安全库存");
            System.Windows.Forms.TreeNode treeNode213 = new System.Windows.Forms.TreeNode("更新安全库存");
            System.Windows.Forms.TreeNode treeNode214 = new System.Windows.Forms.TreeNode("输入需求量");
            System.Windows.Forms.TreeNode treeNode215 = new System.Windows.Forms.TreeNode("系统计算安全库存");
            System.Windows.Forms.TreeNode treeNode216 = new System.Windows.Forms.TreeNode("安全库存", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode211,
            treeNode212,
            treeNode213,
            treeNode214,
            treeNode215});
            System.Windows.Forms.TreeNode treeNode217 = new System.Windows.Forms.TreeNode("集中创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode218 = new System.Windows.Forms.TreeNode("单个创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode219 = new System.Windows.Forms.TreeNode("盘点冻结与解冻");
            System.Windows.Forms.TreeNode treeNode220 = new System.Windows.Forms.TreeNode("输入盘点结果");
            System.Windows.Forms.TreeNode treeNode221 = new System.Windows.Forms.TreeNode("差异清单一览");
            System.Windows.Forms.TreeNode treeNode222 = new System.Windows.Forms.TreeNode("盘点数据更新");
            System.Windows.Forms.TreeNode treeNode223 = new System.Windows.Forms.TreeNode("年度盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode217,
            treeNode218,
            treeNode219,
            treeNode220,
            treeNode221,
            treeNode222});
            System.Windows.Forms.TreeNode treeNode224 = new System.Windows.Forms.TreeNode("显示物料清单");
            System.Windows.Forms.TreeNode treeNode225 = new System.Windows.Forms.TreeNode("差异调整");
            System.Windows.Forms.TreeNode treeNode226 = new System.Windows.Forms.TreeNode("显示差异调整");
            System.Windows.Forms.TreeNode treeNode227 = new System.Windows.Forms.TreeNode("取消差异调整");
            System.Windows.Forms.TreeNode treeNode228 = new System.Windows.Forms.TreeNode("循环盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode224,
            treeNode225,
            treeNode226,
            treeNode227});
            System.Windows.Forms.TreeNode treeNode229 = new System.Windows.Forms.TreeNode("库存盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode223,
            treeNode228});
            System.Windows.Forms.TreeNode treeNode230 = new System.Windows.Forms.TreeNode("质检评分");
            System.Windows.Forms.TreeNode treeNode231 = new System.Windows.Forms.TreeNode("装运评分");
            System.Windows.Forms.TreeNode treeNode232 = new System.Windows.Forms.TreeNode("非原材料拒收数量");
            System.Windows.Forms.TreeNode treeNode233 = new System.Windows.Forms.TreeNode("收货评分", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode230,
            treeNode231,
            treeNode232});
            System.Windows.Forms.TreeNode treeNode234 = new System.Windows.Forms.TreeNode("设置提前提醒天数");
            System.Windows.Forms.TreeNode treeNode235 = new System.Windows.Forms.TreeNode("到期物料提醒");
            System.Windows.Forms.TreeNode treeNode236 = new System.Windows.Forms.TreeNode("到期提醒", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode234,
            treeNode235});
            System.Windows.Forms.TreeNode treeNode237 = new System.Windows.Forms.TreeNode("库存管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode202,
            treeNode204,
            treeNode210,
            treeNode216,
            treeNode229,
            treeNode233,
            treeNode236});
            System.Windows.Forms.TreeNode treeNode238 = new System.Windows.Forms.TreeNode("文档管理", 1, 1);
            System.Windows.Forms.TreeNode treeNode239 = new System.Windows.Forms.TreeNode("用户管理");
            System.Windows.Forms.TreeNode treeNode240 = new System.Windows.Forms.TreeNode("用户组管理");
            System.Windows.Forms.TreeNode treeNode241 = new System.Windows.Forms.TreeNode("角色管理");
            System.Windows.Forms.TreeNode treeNode242 = new System.Windows.Forms.TreeNode("组织机构管理");
            System.Windows.Forms.TreeNode treeNode243 = new System.Windows.Forms.TreeNode("数据备份");
            System.Windows.Forms.TreeNode treeNode244 = new System.Windows.Forms.TreeNode("数据恢复");
            System.Windows.Forms.TreeNode treeNode245 = new System.Windows.Forms.TreeNode("系统管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode239,
            treeNode240,
            treeNode241,
            treeNode242,
            treeNode243,
            treeNode244});
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.任务查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.LKB_UserName = new System.Windows.Forms.LinkLabel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.leftStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.leftBlankHolder = new System.Windows.Forms.ToolStripStatusLabel();
            this.middleStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.rightBlankHolder = new System.Windows.Forms.ToolStripStatusLabel();
            this.rightStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMiddle = new System.Windows.Forms.Panel();
            this.dockPnlForm = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.pnlMiddleLeft = new System.Windows.Forms.Panel();
            this.tvFunction = new System.Windows.Forms.TreeView();
            this.tvImgList = new System.Windows.Forms.ImageList(this.components);
            this.pnlMiddleLeftTop = new System.Windows.Forms.Panel();
            this.menuStrip.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            this.pnlMiddleLeft.SuspendLayout();
            this.pnlMiddleLeftTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.任务查看ToolStripMenuItem,
            this.systemMenuItem,
            this.toolMenuItem,
            this.helpMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(963, 25);
            this.menuStrip.TabIndex = 10;
            this.menuStrip.Text = "menuStrip1";
            // 
            // 任务查看ToolStripMenuItem
            // 
            this.任务查看ToolStripMenuItem.Name = "任务查看ToolStripMenuItem";
            this.任务查看ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.任务查看ToolStripMenuItem.Text = "待办任务";
            this.任务查看ToolStripMenuItem.Click += new System.EventHandler(this.任务查看ToolStripMenuItem_Click);
            // 
            // systemMenuItem
            // 
            this.systemMenuItem.Name = "systemMenuItem";
            this.systemMenuItem.Size = new System.Drawing.Size(59, 21);
            this.systemMenuItem.Text = "系统(S)";
            // 
            // toolMenuItem
            // 
            this.toolMenuItem.Name = "toolMenuItem";
            this.toolMenuItem.Size = new System.Drawing.Size(59, 21);
            this.toolMenuItem.Text = "工具(T)";
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Size = new System.Drawing.Size(61, 21);
            this.helpMenuItem.Text = "帮助(H)";
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 25);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(963, 55);
            this.pnlTop.TabIndex = 11;
            // 
            // LKB_UserName
            // 
            this.LKB_UserName.AutoSize = true;
            this.LKB_UserName.BackColor = System.Drawing.Color.Transparent;
            this.LKB_UserName.Font = new System.Drawing.Font("宋体", 15F);
            this.LKB_UserName.Location = new System.Drawing.Point(11, 8);
            this.LKB_UserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LKB_UserName.Name = "LKB_UserName";
            this.LKB_UserName.Size = new System.Drawing.Size(109, 20);
            this.LKB_UserName.TabIndex = 2;
            this.LKB_UserName.TabStop = true;
            this.LKB_UserName.Text = "linkLabel1";
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.statusStrip);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 536);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(963, 16);
            this.pnlBottom.TabIndex = 15;
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftStatusLabel,
            this.leftBlankHolder,
            this.middleStatusLabel,
            this.rightBlankHolder,
            this.rightStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, -6);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(963, 22);
            this.statusStrip.TabIndex = 0;
            // 
            // leftStatusLabel
            // 
            this.leftStatusLabel.Name = "leftStatusLabel";
            this.leftStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.leftStatusLabel.Text = "左边状态栏内容...";
            // 
            // leftBlankHolder
            // 
            this.leftBlankHolder.Name = "leftBlankHolder";
            this.leftBlankHolder.Size = new System.Drawing.Size(322, 17);
            this.leftBlankHolder.Spring = true;
            // 
            // middleStatusLabel
            // 
            this.middleStatusLabel.Name = "middleStatusLabel";
            this.middleStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.middleStatusLabel.Text = "中间状态栏内容...";
            // 
            // rightBlankHolder
            // 
            this.rightBlankHolder.Name = "rightBlankHolder";
            this.rightBlankHolder.Size = new System.Drawing.Size(322, 17);
            this.rightBlankHolder.Spring = true;
            // 
            // rightStatusLabel
            // 
            this.rightStatusLabel.Name = "rightStatusLabel";
            this.rightStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.rightStatusLabel.Text = "右边状态栏内容...";
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlMiddle.Controls.Add(this.dockPnlForm);
            this.pnlMiddle.Controls.Add(this.pnlMiddleLeft);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(0, 80);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(963, 456);
            this.pnlMiddle.TabIndex = 16;
            // 
            // dockPnlForm
            // 
            this.dockPnlForm.ActiveAutoHideContent = null;
            this.dockPnlForm.AutoScroll = true;
            this.dockPnlForm.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dockPnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPnlForm.DockBackColor = System.Drawing.SystemColors.Control;
            this.dockPnlForm.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPnlForm.Location = new System.Drawing.Point(176, 0);
            this.dockPnlForm.Name = "dockPnlForm";
            this.dockPnlForm.Size = new System.Drawing.Size(787, 456);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPnlForm.Skin = dockPanelSkin1;
            this.dockPnlForm.TabIndex = 15;
            this.dockPnlForm.ActiveContentChanged += new System.EventHandler(this.dockPnlForm_ActiveContentChanged);
            // 
            // pnlMiddleLeft
            // 
            this.pnlMiddleLeft.Controls.Add(this.tvFunction);
            this.pnlMiddleLeft.Controls.Add(this.pnlMiddleLeftTop);
            this.pnlMiddleLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMiddleLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlMiddleLeft.Name = "pnlMiddleLeft";
            this.pnlMiddleLeft.Size = new System.Drawing.Size(176, 456);
            this.pnlMiddleLeft.TabIndex = 14;
            // 
            // tvFunction
            // 
            this.tvFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFunction.ImageIndex = 0;
            this.tvFunction.ImageList = this.tvImgList;
            this.tvFunction.Location = new System.Drawing.Point(0, 31);
            this.tvFunction.Name = "tvFunction";
            treeNode1.Name = "NewMaterialData";
            treeNode1.Text = "新建物料数据";
            treeNode2.Name = "MaterialBasicView";
            treeNode2.Text = "基本视图";
            treeNode3.Name = "MaterialAccountView";
            treeNode3.Text = "会计视图";
            treeNode4.Name = "MaterialBuyerView";
            treeNode4.Text = "采购视图";
            treeNode5.Name = "MaterialStockView";
            treeNode5.Text = "存储视图";
            treeNode6.Name = "NewBatch";
            treeNode6.Text = "建立批次";
            treeNode7.Name = "QuryBatch";
            treeNode7.Text = "查询批次";
            treeNode8.ImageIndex = 1;
            treeNode8.Name = "Batch";
            treeNode8.SelectedImageIndex = 1;
            treeNode8.Text = "批次管理";
            treeNode9.Name = "MaterialClassView";
            treeNode9.Text = "分类视图";
            treeNode10.ImageIndex = 1;
            treeNode10.Name = "MaterialData";
            treeNode10.SelectedImageIndex = 1;
            treeNode10.Text = "物料主数据";
            treeNode11.Name = "SupplierBasicView";
            treeNode11.Text = "基本视图";
            treeNode12.Name = "SupplierBuyerOrgView";
            treeNode12.Text = "采购组织视图";
            treeNode13.Name = "SupplierCompanyCodeView";
            treeNode13.Text = "公司代码视图";
            treeNode14.Name = "MMartketPriceForm";
            treeNode14.Text = "市场价格记录";
            treeNode15.Name = "PurchasesRecordInfoViewForm";
            treeNode15.Text = "记录查看";
            treeNode16.Name = "AddPurRecordInfoForm";
            treeNode16.Text = "添加记录";
            treeNode17.ImageIndex = 1;
            treeNode17.Name = "采购记录";
            treeNode17.SelectedImageIndex = 1;
            treeNode17.Text = "采购记录";
            treeNode18.ImageIndex = 1;
            treeNode18.Name = "SupplierData";
            treeNode18.SelectedImageIndex = 1;
            treeNode18.Text = "供应商主数据";
            treeNode19.Name = "NewVocher";
            treeNode19.Text = "生成会计凭证";
            treeNode20.ImageIndex = 1;
            treeNode20.Name = "AccountData";
            treeNode20.SelectedImageIndex = 1;
            treeNode20.Text = "财务主数据";
            treeNode21.Name = "MaintainMaterialGroup";
            treeNode21.Text = "维护物料组";
            treeNode22.Name = "MaterialTypeA";
            treeNode22.Text = "维护物料类型";
            treeNode23.Name = "MaintainCurrency";
            treeNode23.Text = "维护货币";
            treeNode24.Name = "MaintainCountry";
            treeNode24.Text = "维护国家信息";
            treeNode25.Name = "MaintainPaymentClause";
            treeNode25.Text = "维护付款条件";
            treeNode26.Name = "MaintainMeasurement";
            treeNode26.Text = "维护计量单位";
            treeNode27.Name = "MaintainEvaluationClass";
            treeNode27.Text = "维护评估类";
            treeNode28.Name = "MaintainCompanyCode";
            treeNode28.Text = "维护公司代码";
            treeNode29.Name = "MaintainFactory";
            treeNode29.Text = "维护工厂";
            treeNode30.Name = "MaintainStock";
            treeNode30.Text = "维护库存地";
            treeNode31.Name = "MaintainBuyerGroup";
            treeNode31.Text = "维护采购组";
            treeNode32.Name = "MaintainBuyerOrganization";
            treeNode32.Text = "维护采购组织";
            treeNode33.Name = "MaintainDivision";
            treeNode33.Text = "维护产品组";
            treeNode34.Name = "MaintainTradeClause";
            treeNode34.Text = "维护交付条件";
            treeNode35.Name = "MaintainCostCenter";
            treeNode35.Text = "维护成本中心";
            treeNode36.Name = "MaintainIndustryCategory";
            treeNode36.Text = "维护工业标识";
            treeNode37.Name = "MaterialDisMtGroupForm";
            treeNode37.Text = "物料组分配物料";
            treeNode38.Name = "purOrgAllotmtGroup";
            treeNode38.Text = "采购组织分配物料组";
            treeNode39.Name = "MaintainMaterEvalFactor";
            treeNode39.Text = "维护评估要素";
            treeNode39.ToolTipText = "维护评估要素";
            treeNode40.Name = "GoalValueAndLowValueForm";
            treeNode40.Text = "维护目标值和最低值";
            treeNode41.Name = "strategyInfoForm";
            treeNode41.Text = "维护策略信息库";
            treeNode42.Name = "MainCertiType";
            treeNode42.Text = "维护证书类型";
            treeNode43.Name = "newTaxeClassForm";
            treeNode43.Text = "维护税率";
            treeNode44.ImageIndex = 1;
            treeNode44.Name = "GeneralSettings";
            treeNode44.SelectedImageIndex = 1;
            treeNode44.Text = "一般设置";
            treeNode45.ImageIndex = 1;
            treeNode45.Name = "MainData";
            treeNode45.SelectedImageIndex = 1;
            treeNode45.Text = "数据管理";
            treeNode46.Name = "showDemand";
            treeNode46.Text = "查看计划";
            treeNode47.ImageIndex = 1;
            treeNode47.Name = "demandList";
            treeNode47.SelectedImageIndex = 1;
            treeNode47.Text = "计划列表";
            treeNode48.Name = "newStandardDemand";
            treeNode48.Text = "新建";
            treeNode49.Name = "maintenancePlan";
            treeNode49.Text = "修改";
            treeNode50.Name = "more_Info";
            treeNode50.Text = "查看";
            treeNode51.Name = "delete_Info";
            treeNode51.Text = "删除";
            treeNode52.ImageIndex = 1;
            treeNode52.Name = "apply_Dep";
            treeNode52.SelectedImageIndex = 1;
            treeNode52.Text = "申请部门";
            treeNode53.Name = "auditSubmitStandard";
            treeNode53.Text = "审核";
            treeNode54.Name = "aggregate_Info";
            treeNode54.Text = "汇总";
            treeNode55.Name = "createPurchasing";
            treeNode55.Text = "生成";
            treeNode56.Name = "source";
            treeNode56.Text = "寻源";
            treeNode57.ImageIndex = 1;
            treeNode57.Name = "buy_Dep";
            treeNode57.SelectedImageIndex = 1;
            treeNode57.Text = "采购部门";
            treeNode58.ImageIndex = 1;
            treeNode58.Name = "procurementMM";
            treeNode58.SelectedImageIndex = 1;
            treeNode58.Text = "计划管理";
            treeNode59.Name = "createSource";
            treeNode59.Text = "执行货源";
            treeNode60.ImageIndex = 1;
            treeNode60.Name = "节点1";
            treeNode60.SelectedImageIndex = 1;
            treeNode60.Text = "寻源舱";
            treeNode61.Name = "sourceLists";
            treeNode61.Text = "清单列表";
            treeNode62.Name = "newSourceList";
            treeNode62.Text = "新建源清单";
            treeNode63.Name = "maintenanceSourceList";
            treeNode63.Text = "更改源清单";
            treeNode64.ImageIndex = 1;
            treeNode64.Name = "sourceListInfo";
            treeNode64.SelectedImageIndex = 1;
            treeNode64.Text = "源清单";
            treeNode65.Name = "viewInquirys";
            treeNode65.Text = "查看询价";
            treeNode66.Name = "newInquiry";
            treeNode66.Text = "创建询价";
            treeNode67.Name = "editInquiry";
            treeNode67.Text = "更改询价";
            treeNode68.Name = "executeoffer";
            treeNode68.Text = "执行报价";
            treeNode69.Name = "executeCompare";
            treeNode69.Text = "执行比价";
            treeNode70.ImageIndex = 1;
            treeNode70.Name = "inquiry";
            treeNode70.SelectedImageIndex = 1;
            treeNode70.Text = "询价";
            treeNode71.Name = "showBids";
            treeNode71.Text = "查看招标";
            treeNode72.Name = "newBidding";
            treeNode72.Text = "创建招标";
            treeNode73.Name = "releaseBid";
            treeNode73.Text = "发布招标";
            treeNode74.Name = "changeTender";
            treeNode74.Text = "修改招标";
            treeNode75.Name = "checkBid";
            treeNode75.Text = "审批招标";
            treeNode76.Name = "offerBidding";
            treeNode76.Text = "处理投标";
            treeNode77.Name = "compareBidding";
            treeNode77.Text = "进行评标";
            treeNode78.Name = "transactList";
            treeNode78.Text = "查看谈判";
            treeNode79.Name = "createTransact";
            treeNode79.Text = "创建谈判";
            treeNode80.Name = "dealTrans";
            treeNode80.Text = "处理谈判";
            treeNode81.ImageIndex = 1;
            treeNode81.Name = "transact";
            treeNode81.SelectedImageIndex = 1;
            treeNode81.Text = "谈判";
            treeNode82.ImageIndex = 1;
            treeNode82.Name = "bidding";
            treeNode82.SelectedImageIndex = 1;
            treeNode82.Text = "招标";
            treeNode83.Name = "newAuction";
            treeNode83.Text = "竞价";
            treeNode84.Name = "offerAuction";
            treeNode84.Text = "竞拍";
            treeNode85.ImageIndex = 1;
            treeNode85.Name = "auction";
            treeNode85.SelectedImageIndex = 1;
            treeNode85.Text = "拍卖";
            treeNode86.Name = "allRecords";
            treeNode86.Text = "历史信息记录";
            treeNode87.ImageIndex = 1;
            treeNode87.Name = "sourceRecord";
            treeNode87.SelectedImageIndex = 1;
            treeNode87.Text = "采购信息记录";
            treeNode88.Name = "condition";
            treeNode88.Text = "条件类型";
            treeNode89.Name = "conditionList";
            treeNode89.Text = "查看条件类型";
            treeNode90.Name = "addCondition";
            treeNode90.Text = "创建条件类型";
            treeNode91.Name = "editCondition";
            treeNode91.Text = "更改条件类型";
            treeNode92.ImageIndex = 1;
            treeNode92.Name = "conditionType";
            treeNode92.SelectedImageIndex = 1;
            treeNode92.Text = "条件定价";
            treeNode93.ImageIndex = 1;
            treeNode93.Name = "sourceMM";
            treeNode93.SelectedImageIndex = 1;
            treeNode93.Text = "货源确定";
            treeNode94.Name = "manageContractTemplateNode";
            treeNode94.Text = "管理";
            treeNode95.ImageIndex = 1;
            treeNode95.Name = "contractTemplateNode";
            treeNode95.SelectedImageIndex = 1;
            treeNode95.Text = "合同模板";
            treeNode96.Name = "manageContractTextNode";
            treeNode96.Text = "管理";
            treeNode97.ImageIndex = 1;
            treeNode97.Name = "contractTextNode";
            treeNode97.SelectedImageIndex = 1;
            treeNode97.Text = "合同文本";
            treeNode98.ImageIndex = 1;
            treeNode98.Name = "contractManageNode";
            treeNode98.SelectedImageIndex = 1;
            treeNode98.Text = "合同管理";
            treeNode99.ImageIndex = 0;
            treeNode99.Name = "manageAgreementContractNode";
            treeNode99.Text = "管理";
            treeNode100.ImageIndex = 1;
            treeNode100.Name = "outlineAgreementNode";
            treeNode100.SelectedImageIndex = 1;
            treeNode100.Text = "框架协议";
            treeNode101.ImageKey = "Table4.png";
            treeNode101.Name = "quotaArrangementManageNode";
            treeNode101.Text = "管理";
            treeNode102.ImageIndex = 1;
            treeNode102.Name = "quotaArrangementNode";
            treeNode102.SelectedImageIndex = 1;
            treeNode102.Text = "配额协议";
            treeNode103.ImageIndex = 1;
            treeNode103.Name = "SelfServicePurchasingNode";
            treeNode103.SelectedImageIndex = 1;
            treeNode103.Text = "自助采购";
            treeNode104.Name = "OrderCreate";
            treeNode104.Text = "生成订单";
            treeNode105.ImageKey = "Table4.png";
            treeNode105.Name = "POManageNode";
            treeNode105.Text = "订单管理";
            treeNode106.Name = "InvoiceAndPaymentNode";
            treeNode106.Text = "发票管理";
            treeNode107.ImageIndex = 1;
            treeNode107.Name = "节点6";
            treeNode107.SelectedImageIndex = 1;
            treeNode107.Text = "订单管理";
            treeNode108.Name = "AddMTGgoals";
            treeNode108.Text = "供应目标和指标";
            treeNode109.Name = "AddItemGoals";
            treeNode109.Text = "核心竞争力";
            treeNode110.Name = "AddGeneralItemGoals";
            treeNode110.Text = "公司采购项目";
            treeNode111.Name = "RiskAssessment";
            treeNode111.Text = "风险等级";
            treeNode112.Name = "importData";
            treeNode112.Text = "等级定位";
            treeNode113.Name = "prdGoalAndAimForm";
            treeNode113.Text = "生产性物料组";
            treeNode114.Name = "maintainMtGpForm";
            treeNode114.Text = "维护性物料组";
            treeNode115.Name = "assetMtGroupForm";
            treeNode115.Text = "资产性物料组";
            treeNode116.ImageIndex = 1;
            treeNode116.Name = "节点0";
            treeNode116.SelectedImageIndex = 1;
            treeNode116.Text = "供应目标设置";
            treeNode117.ImageIndex = 1;
            treeNode117.Name = "物料定位";
            treeNode117.SelectedImageIndex = 1;
            treeNode117.Text = "物料定位";
            treeNode118.Name = "certificationPreparationNode";
            treeNode118.Text = "供应商前期准备";
            treeNode119.ImageIndex = 1;
            treeNode119.Name = "供应商认证";
            treeNode119.SelectedImageIndex = 1;
            treeNode119.Text = "供应商认证";
            treeNode120.ImageIndex = 1;
            treeNode120.Name = "供应商认证";
            treeNode120.SelectedImageIndex = 1;
            treeNode120.Text = "供应商认证";
            treeNode121.Name = "SPAddNode";
            treeNode121.Text = "手工维护";
            treeNode122.Name = "SPAutoNode";
            treeNode122.Text = "自动评估";
            treeNode123.Name = "SPBatchAddNode";
            treeNode123.Text = "批量评估";
            treeNode124.Name = "SPResultNode";
            treeNode124.Text = "结果显示";
            treeNode125.Name = "SPListNode";
            treeNode125.Text = "清单显示";
            treeNode126.Name = "ExService";
            treeNode126.Tag = "外部服务";
            treeNode126.Text = "外部服务";
            treeNode127.Name = "InterForm";
            treeNode127.Text = "一般服务";
            treeNode128.Name = "IntialServiceRate";
            treeNode128.Text = "维护服务占比";
            treeNode129.Name = "服务评估";
            treeNode129.Tag = "";
            treeNode129.Text = "服务评估";
            treeNode130.Name = "deliveryForm321";
            treeNode130.Text = "节点1";
            treeNode131.Name = "节点2";
            treeNode131.Text = "节点2";
            treeNode132.Name = "节点3";
            treeNode132.Text = "节点3";
            treeNode133.Name = "节点4";
            treeNode133.Text = "节点4";
            treeNode134.Name = "deliveryForm";
            treeNode134.Text = "交货评估";
            treeNode135.ImageIndex = 1;
            treeNode135.Name = "SupplierPerformanceNode";
            treeNode135.SelectedImageIndex = 1;
            treeNode135.Text = "绩效评估";
            treeNode136.Name = "SystemProcessAuditNode";
            treeNode136.Text = "体系/过程审核";
            treeNode137.Name = "ProductAuditNode";
            treeNode137.Text = "产品审核";
            treeNode138.ImageIndex = 1;
            treeNode138.Name = "QualityAuditNode";
            treeNode138.SelectedImageIndex = 1;
            treeNode138.Text = "质量审核";
            treeNode139.Name = "ComplaintRejectAddNode";
            treeNode139.Text = "抱怨/拒绝情况录入";
            treeNode140.Name = "ServiceAddNode";
            treeNode140.Text = "服务情况录入";
            treeNode141.Name = "QuestionaireNode";
            treeNode141.Text = "收货问卷调查";
            treeNode142.Name = "SupplierTurnoverNode";
            treeNode142.Text = "供应商营业额录入";
            treeNode143.ImageIndex = 1;
            treeNode143.Name = "SPDataAddNode";
            treeNode143.SelectedImageIndex = 1;
            treeNode143.Text = "绩效评估数据维护";
            treeNode144.Name = "SupplierClassifyNode";
            treeNode144.Text = "执行分级";
            treeNode145.Name = "ClassificationResultNode";
            treeNode145.Text = "分级查看";
            treeNode146.Name = "ResultGroupNode";
            treeNode146.Text = "分组查看分级结果";
            treeNode147.ImageIndex = 1;
            treeNode147.Name = "SupplierClassifyNode";
            treeNode147.SelectedImageIndex = 1;
            treeNode147.Text = "供应商分级";
            treeNode148.Name = "LPSWarningNode";
            treeNode148.Text = "低效能供应商警告";
            treeNode149.Name = "LPSProcessNode";
            treeNode149.Text = "低效能供应商管理三级流程";
            treeNode150.Name = "LPSOutNode";
            treeNode150.Text = "低效能供应商淘汰";
            treeNode151.ImageIndex = 1;
            treeNode151.Name = "LPSNode";
            treeNode151.SelectedImageIndex = 1;
            treeNode151.Text = "低效能供应商管理";
            treeNode152.Name = "CostReductionCalculateNode";
            treeNode152.Text = "降成本计算";
            treeNode153.Name = "CostReductionResultNode";
            treeNode153.Text = "降成本结果查询";
            treeNode154.ImageIndex = 1;
            treeNode154.Name = "CostReductionNode";
            treeNode154.SelectedImageIndex = 1;
            treeNode154.Text = "采购降成本";
            treeNode155.Name = "BusinessValueAssessNode";
            treeNode155.Text = "供应商区分";
            treeNode156.Name = "SupplierManageStrategy";
            treeNode156.Text = "管理策略";
            treeNode157.ImageIndex = 1;
            treeNode157.Name = "SupplierValueNode";
            treeNode157.SelectedImageIndex = 1;
            treeNode157.Text = "供应商价值";
            treeNode158.Name = "SP_Compare";
            treeNode158.Text = "供应商绩效比较";
            treeNode159.Name = "SP_CompareBaseOnMaterialGroup";
            treeNode159.Text = "基于物料组的供应商评估";
            treeNode160.Name = "SP_CompareBaseOnIndustry";
            treeNode160.Text = "基于行业的供应商评估";
            treeNode161.Name = "SP_CompareRadarChart";
            treeNode161.Text = "供应商对比雷达图";
            treeNode162.Name = "SP_CompareBubbleChart";
            treeNode162.Text = "供应商分析气泡图";
            treeNode163.ImageIndex = 1;
            treeNode163.Name = "SP_Report";
            treeNode163.SelectedImageIndex = 1;
            treeNode163.Text = "绩效评估报表";
            treeNode164.Name = "SupplierListForm";
            treeNode164.Text = "已认证供应商分析";
            treeNode165.Name = "ExistedSupplierAnalysis";
            treeNode165.Text = "现行供应商业绩分析";
            treeNode166.Name = "ReplacedSupplierAnalysis";
            treeNode166.Text = "替代供应商分析";
            treeNode167.ImageIndex = 1;
            treeNode167.Name = "SupplierList";
            treeNode167.SelectedImageIndex = 1;
            treeNode167.Text = "供应商清单";
            treeNode168.Name = "supplyInfoShowForm";
            treeNode168.Text = "供货信息查看";
            treeNode169.ImageIndex = 1;
            treeNode169.Name = "节点0";
            treeNode169.Text = "供货信息";
            treeNode170.Name = "newSupplierCertiForm";
            treeNode170.Text = "供应商证书";
            treeNode171.ImageIndex = 1;
            treeNode171.Name = "节点0";
            treeNode171.Text = "供应商表现";
            treeNode172.ImageIndex = 1;
            treeNode172.Name = "供应商管理";
            treeNode172.SelectedImageIndex = 1;
            treeNode172.Text = "供应商管理";
            treeNode173.Name = "MaterialABCAnalysis";
            treeNode173.Tag = "100";
            treeNode173.Text = "物料的ABC类分析";
            treeNode173.ToolTipText = "物料的ABC类分析";
            treeNode174.Name = "SupplierABCAnalysis";
            treeNode174.Tag = "101";
            treeNode174.Text = "供应商的ABC类分析";
            treeNode175.ImageIndex = 1;
            treeNode175.Name = "PurchaseMonitor";
            treeNode175.SelectedImageIndex = 1;
            treeNode175.Tag = "100";
            treeNode175.Text = "采购监控分析";
            treeNode175.ToolTipText = "采购监控分析";
            treeNode176.Name = "PurchaseConstDistribution";
            treeNode176.Tag = "201";
            treeNode176.Text = "采购花费分布";
            treeNode176.ToolTipText = "采购花费分布";
            treeNode177.Name = "CostStructureAnalysis";
            treeNode177.Tag = "202";
            treeNode177.Text = "成本结构分析";
            treeNode177.ToolTipText = "成本结构分析";
            treeNode178.Name = "CostComparativeAnalysis";
            treeNode178.Tag = "203";
            treeNode178.Text = "成本比较分析";
            treeNode178.ToolTipText = "成本比较分析";
            treeNode179.Name = "CostTrenAnalysis";
            treeNode179.Tag = "204";
            treeNode179.Text = "成本趋势分析";
            treeNode179.ToolTipText = "成本趋势分析";
            treeNode180.ImageIndex = 1;
            treeNode180.Name = "PurchaseExpense";
            treeNode180.SelectedImageIndex = 1;
            treeNode180.Tag = "200";
            treeNode180.Text = "采购花费分析";
            treeNode180.ToolTipText = "采购花费分析";
            treeNode181.Name = "MaterialPriceAnalysis";
            treeNode181.Tag = "300";
            treeNode181.Text = "价格分析";
            treeNode181.ToolTipText = "价格分析";
            treeNode182.Name = "SupplierBiddingAnalysis";
            treeNode182.Tag = "301";
            treeNode182.Text = "供应商竞价分析";
            treeNode182.ToolTipText = "供应商竞价分析";
            treeNode183.ImageIndex = 1;
            treeNode183.Name = "SourseTracing";
            treeNode183.SelectedImageIndex = 1;
            treeNode183.Tag = "300";
            treeNode183.Text = "寻源分析";
            treeNode183.ToolTipText = "寻源分析";
            treeNode184.Name = "ConstractionManagement";
            treeNode184.Tag = "400";
            treeNode184.Text = "合同管理分析";
            treeNode184.ToolTipText = "合同管理分析";
            treeNode185.ImageIndex = 1;
            treeNode185.Name = "ConstractionExecutuon";
            treeNode185.SelectedImageIndex = 1;
            treeNode185.Tag = "400";
            treeNode185.Text = "合同执行分析";
            treeNode185.ToolTipText = "合同执行分析";
            treeNode186.Name = "SupplierComparativeAnalysis";
            treeNode186.Tag = "500";
            treeNode186.Text = "供应商对比雷达图";
            treeNode186.ToolTipText = "供应商对比雷达图";
            treeNode187.Name = "SupplierTrendAnalysis";
            treeNode187.Tag = "501";
            treeNode187.Text = "供应商趋势报表";
            treeNode187.ToolTipText = "供应商趋势报表";
            treeNode188.Name = "SupplierScoreRanking";
            treeNode188.Tag = "502";
            treeNode188.Text = "供应商得分排名";
            treeNode188.ToolTipText = "供应商得分排名";
            treeNode189.ImageIndex = 1;
            treeNode189.Name = "SupplierPerformance";
            treeNode189.SelectedImageIndex = 1;
            treeNode189.Tag = "500";
            treeNode189.Text = "供应商绩效分析";
            treeNode189.ToolTipText = "供应商绩效分析";
            treeNode190.Name = "StockAgeAnalysis";
            treeNode190.Tag = "600";
            treeNode190.Text = "库龄分析明细表";
            treeNode190.ToolTipText = "库龄分析明细表";
            treeNode191.Name = "StockABCAnalysis";
            treeNode191.Tag = "601";
            treeNode191.Text = "库存物料ABC类分析";
            treeNode191.ToolTipText = "库存物料ABC类分析";
            treeNode192.ImageIndex = 1;
            treeNode192.Name = "StockAnalysis";
            treeNode192.SelectedImageIndex = 1;
            treeNode192.Tag = "600";
            treeNode192.Text = "库存分析";
            treeNode192.ToolTipText = "库存分析";
            treeNode193.Name = "SupplierAnalysis";
            treeNode193.Tag = "700";
            treeNode193.Text = "供应商分析";
            treeNode193.ToolTipText = "供应商分析";
            treeNode194.Name = "SuppplierDependentFactors";
            treeNode194.Tag = "800";
            treeNode194.Text = "供应商依赖因素分析";
            treeNode194.ToolTipText = "供应商依赖因素分析";
            treeNode195.Name = "Customized";
            treeNode195.Tag = "900";
            treeNode195.Text = "定制";
            treeNode195.ToolTipText = "定制";
            treeNode196.ImageIndex = 1;
            treeNode196.Name = "节点9";
            treeNode196.SelectedImageIndex = 1;
            treeNode196.Text = "报表分析";
            treeNode197.Name = "node_StockType";
            treeNode197.Tag = "10101";
            treeNode197.Text = "库存类型";
            treeNode198.Name = "node_StockState";
            treeNode198.Tag = "10102";
            treeNode198.Text = "库存状态";
            treeNode199.Name = "FindMoveType";
            treeNode199.Text = "查看";
            treeNode199.ToolTipText = "查看";
            treeNode200.Name = "TransactionEvent";
            treeNode200.Text = "事务/事件";
            treeNode200.ToolTipText = "事务/事件";
            treeNode201.ImageIndex = 1;
            treeNode201.Name = "MoveType";
            treeNode201.SelectedImageIndex = 1;
            treeNode201.Text = "移动类型";
            treeNode201.ToolTipText = "移动类型";
            treeNode202.ImageIndex = 1;
            treeNode202.Name = "基础配置";
            treeNode202.SelectedImageIndex = 1;
            treeNode202.Text = "基础配置";
            treeNode203.ImageIndex = 0;
            treeNode203.Name = "Receiving";
            treeNode203.SelectedImageIndex = 0;
            treeNode203.Text = "收货";
            treeNode203.ToolTipText = "收货";
            treeNode204.ImageIndex = 1;
            treeNode204.Name = "MaterialMove";
            treeNode204.SelectedImageIndex = 1;
            treeNode204.Tag = "10";
            treeNode204.Text = "货物移动";
            treeNode204.ToolTipText = "货物移动";
            treeNode205.Name = "StockOverview";
            treeNode205.Text = "库存总览";
            treeNode206.Name = "FactoryStock";
            treeNode206.Text = "工厂库存";
            treeNode207.Name = "StockStock";
            treeNode207.Text = "仓库库存";
            treeNode208.Name = "ConsignmentStock";
            treeNode208.Text = "寄售库存";
            treeNode209.Name = "TransitStock";
            treeNode209.Text = "在途库存";
            treeNode210.ImageIndex = 1;
            treeNode210.Name = "Report";
            treeNode210.SelectedImageIndex = 1;
            treeNode210.Text = "收发存报表";
            treeNode210.ToolTipText = "收发存报表";
            treeNode211.Name = "SaftyStockSet";
            treeNode211.Text = "手动设置安全库存";
            treeNode212.Name = "ShowSaftyInventory";
            treeNode212.Text = "查看安全库存";
            treeNode213.Name = "UpdateSaftyStock";
            treeNode213.Text = "更新安全库存";
            treeNode214.Name = "InputDemand";
            treeNode214.Text = "输入需求量";
            treeNode215.Name = "SaftyStockComp";
            treeNode215.Text = "系统计算安全库存";
            treeNode216.ImageIndex = 1;
            treeNode216.Name = "SaveStock";
            treeNode216.SelectedImageIndex = 1;
            treeNode216.Text = "安全库存";
            treeNode216.ToolTipText = "安全库存";
            treeNode217.Name = "createDocument";
            treeNode217.Text = "集中创建盘点凭证";
            treeNode218.Name = "CreateDocument";
            treeNode218.Text = "单个创建盘点凭证";
            treeNode219.Name = "Freezing";
            treeNode219.Text = "盘点冻结与解冻";
            treeNode220.Name = "InputResult";
            treeNode220.Text = "输入盘点结果";
            treeNode221.Name = "ShowDifference";
            treeNode221.Text = "差异清单一览";
            treeNode222.Name = "CheckPost";
            treeNode222.Text = "盘点数据更新";
            treeNode223.ImageIndex = 1;
            treeNode223.Name = "节点0";
            treeNode223.SelectedImageIndex = 1;
            treeNode223.Text = "年度盘点";
            treeNode224.Name = "StockList";
            treeNode224.Text = "显示物料清单";
            treeNode225.Name = "AdjustDifference";
            treeNode225.Text = "差异调整";
            treeNode226.Name = "ShowDiffLoop";
            treeNode226.Text = "显示差异调整";
            treeNode227.Name = "CancelAdjust";
            treeNode227.Text = "取消差异调整";
            treeNode228.ImageIndex = 1;
            treeNode228.Name = "节点1";
            treeNode228.SelectedImageIndex = 1;
            treeNode228.Text = "循环盘点";
            treeNode229.ImageIndex = 1;
            treeNode229.Name = "StockCheck";
            treeNode229.SelectedImageIndex = 1;
            treeNode229.Text = "库存盘点";
            treeNode229.ToolTipText = "库存盘点";
            treeNode230.Name = "ReceiveScore";
            treeNode230.Text = "质检评分";
            treeNode231.Name = "ShipmentScore";
            treeNode231.Text = "装运评分";
            treeNode232.Name = "NonRawMaterialScore";
            treeNode232.Text = "非原材料拒收数量";
            treeNode233.ImageIndex = 1;
            treeNode233.Name = "ReceivingScore";
            treeNode233.SelectedImageIndex = 1;
            treeNode233.Text = "收货评分";
            treeNode233.ToolTipText = "收货评分";
            treeNode234.Name = "Remind";
            treeNode234.Text = "设置提前提醒天数";
            treeNode235.Name = "RemindHandle";
            treeNode235.Text = "到期物料提醒";
            treeNode236.ImageIndex = 1;
            treeNode236.Name = "ExpirationReminder";
            treeNode236.SelectedImageIndex = 1;
            treeNode236.Text = "到期提醒";
            treeNode236.ToolTipText = "到期提醒";
            treeNode237.ImageIndex = 1;
            treeNode237.Name = "节点12";
            treeNode237.SelectedImageIndex = 1;
            treeNode237.Text = "库存管理";
            treeNode238.ImageIndex = 1;
            treeNode238.Name = "fileManageNode";
            treeNode238.SelectedImageIndex = 1;
            treeNode238.Text = "文档管理";
            treeNode239.Name = "sysUserManager";
            treeNode239.Text = "用户管理";
            treeNode240.Name = "userGroupManageNode";
            treeNode240.Text = "用户组管理";
            treeNode241.Name = "userRoleManageNode";
            treeNode241.Text = "角色管理";
            treeNode242.Name = "organizationManageNode";
            treeNode242.Text = "组织机构管理";
            treeNode243.Name = "backupDataNode";
            treeNode243.Text = "数据备份";
            treeNode244.Name = "restoreDataNode";
            treeNode244.Text = "数据恢复";
            treeNode245.ImageIndex = 1;
            treeNode245.Name = "systemConfigNode";
            treeNode245.SelectedImageIndex = 1;
            treeNode245.Text = "系统管理";
            this.tvFunction.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode45,
            treeNode58,
            treeNode93,
            treeNode98,
            treeNode100,
            treeNode102,
            treeNode103,
            treeNode107,
            treeNode172,
            treeNode196,
            treeNode237,
            treeNode238,
            treeNode245});
            this.tvFunction.SelectedImageIndex = 0;
            this.tvFunction.Size = new System.Drawing.Size(176, 425);
            this.tvFunction.TabIndex = 1;
            this.tvFunction.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFunction_NodeMouseClick);
            // 
            // tvImgList
            // 
            this.tvImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("tvImgList.ImageStream")));
            this.tvImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.tvImgList.Images.SetKeyName(0, "Table4.png");
            this.tvImgList.Images.SetKeyName(1, "Folder2.png");
            // 
            // pnlMiddleLeftTop
            // 
            this.pnlMiddleLeftTop.Controls.Add(this.LKB_UserName);
            this.pnlMiddleLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMiddleLeftTop.Location = new System.Drawing.Point(0, 0);
            this.pnlMiddleLeftTop.Name = "pnlMiddleLeftTop";
            this.pnlMiddleLeftTop.Size = new System.Drawing.Size(176, 31);
            this.pnlMiddleLeftTop.TabIndex = 0;
            // 
            // UserUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 552);
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "UserUI";
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.loadSelectedNodes);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.pnlMiddle.ResumeLayout(false);
            this.pnlMiddleLeft.ResumeLayout(false);
            this.pnlMiddleLeftTop.ResumeLayout(false);
            this.pnlMiddleLeftTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Panel pnlMiddle;
        private System.Windows.Forms.Panel pnlMiddleLeft;
        private System.Windows.Forms.Panel pnlMiddleLeftTop;
        private System.Windows.Forms.ImageList tvImgList;
        private System.Windows.Forms.ToolStripMenuItem systemMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel leftStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel leftBlankHolder;
        private System.Windows.Forms.ToolStripStatusLabel middleStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel rightBlankHolder;
        private System.Windows.Forms.ToolStripStatusLabel rightStatusLabel;
        public WeifenLuo.WinFormsUI.Docking.DockPanel dockPnlForm;
        private System.Windows.Forms.TreeView tvFunction;
        private System.Windows.Forms.ToolStripMenuItem 任务查看ToolStripMenuItem;
        private System.Windows.Forms.LinkLabel LKB_UserName;
    }
}