﻿using Lib.Model.ServiceEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.ServiceIDAL
{
    public interface ServiceIDAL
    {
        //获取登陆者对应的供应商编号
        Dictionary<string, string> getSupplierId(String loginId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceData"></param>
        /// <returns></returns>
        Boolean insertServiceData(ServiceModel serviceData);

    }
}
