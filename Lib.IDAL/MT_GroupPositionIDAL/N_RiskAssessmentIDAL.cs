﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
/// <summary>
/// 物料定位接口
/// </summary>
namespace Lib.IDAL.MT_GroupPositionIDAL
{
   public interface N_RiskAssessmentIDAL
    {
         DataTable GetDefinedGroup(String purOrgName);

    }
}
