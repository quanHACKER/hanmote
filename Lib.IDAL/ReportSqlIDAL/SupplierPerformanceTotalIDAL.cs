﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.SqlIDAL
{
    public interface SupplierPerformanceTotalIDAL
    {
        DataTable GetSupplierPerformanceTotal(string purchaseOrg, string materialGroup);
    }
}
