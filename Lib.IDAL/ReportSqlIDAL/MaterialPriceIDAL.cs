﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.SqlIDAL
{
    public interface MaterialPriceIDAL
    {
        /// <summary>
        /// 通过采购组织，供应商名称或者供应商ID来取得物料的价格信息
        /// </summary>
        /// <param name="purchaseOrg"></param>
        /// <param name="supplierNameOrID"></param>
        /// <returns></returns>
        DataTable GetMaterialDetailPrice(string purchaseOrg, string supplierNameOrID);
    }
}
