﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;

namespace Lib.SqlIDAL
{
     public interface SupplierBaseInformationIDAL
    {
         /// <summary>
         /// 根据供应商名称查询供应商信息(接口)
         /// </summary>
         /// <param name="SupplierName"></param>
         /// <returns></returns>
         SupplierBaseInformation FindBySupplierName(string SupplierName);

         /// <summary>
         /// 根据供应商ID查询供应商信息（接口）
         /// </summary>
         /// <param name="SupplierID"></param>
         /// <returns></returns>
         SupplierBaseInformation FindBySupplierID(string SupplierID);
    }
}
