﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.SqlIDAL
{
    public interface BuyerOrganizationIDAL
    {
        /// <summary>
        /// 取得全部采购组织名称
        /// </summary>
        /// <returns></returns>
        DataTable GetAllBuyerOrganizationName();
    }
}
