﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface SupplierValueIDAL
    {
        #region 业务价值
        
        //查询全年采购业务量
        DataTable queryPurchasingTurnover(String supplierID, String year);

        //查询供应商全年营业额
        DataTable querySupplierTurnover(String supplierID, String year);

        //保存业务价值，写入数据表Supplier_Business_Value中
        int saveBusinessValue(List<Object> savePara);

        #endregion

        #region 品项定位结果

        /// <summary>
        /// 查询品项定位结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        String querySupplyType(String supplierID);

        #endregion

        #region  供应商区分
        int saveSupplierPosition(List<Object> savePara);
        #endregion

        #region 供应商状态跟踪

        /// <summary>
        /// 查询当前年份所有供应商的状态
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        DataTable querySupplierState(String year,String industry);

        /// <summary>
        /// 修改供应商状态，Supplier_Base表
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        int changeSupplierState(String supplierID, String state);

        /// <summary>
        /// 查询行业类别
        /// </summary>
        /// <returns></returns>
        DataTable queryIndustry();

        /// <summary>
        /// 保存供应商营业额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <param name="turnover"></param>
        /// <returns></returns>
        int saveSupplierTurnover(String supplierID, String year ,String turnover);

        #endregion
    }
}
