﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface BusinessValueIDAL
    {
        #region 业务价值
        
        //查询全年采购业务量
        DataTable queryPurchasingTurnover(String supplierID, String year);

        //查询供应商全年营业额
        DataTable querySupplierTurnover(String supplierID, String year);

        //保存业务价值，写入数据表Supplier_Business_Value中
        int saveBusinessValue(List<Object> savePara);

        //保存供应商营业额
        int saveBusinessValue(String supplierID, String year, String turnover);
        #endregion
    }
}
