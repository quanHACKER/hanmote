﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface SPR_BaseIDAL
    {
        /// <summary>
        /// 获取所有采购组织信息
        /// </summary>
        /// <returns></returns>
        DataTable getAllBuyerOrgInfo();

        /// <summary>
        /// 根据采购组织编码查找该采购组织下的供应商名称
        /// </summary>
        /// <param name="purchaseOrgId">采购组织编码</param>
        /// <returns></returns>
        DataTable getAllSupplierInfoByPurchaseOrgId(string purchaseOrgId);

    }
}
