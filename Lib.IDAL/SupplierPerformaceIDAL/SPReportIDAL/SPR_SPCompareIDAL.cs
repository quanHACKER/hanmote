﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface SPR_SPCompareIDAL:SPR_BaseIDAL
    {
        /// <summary>
        /// 查询物料信息
        /// </summary>
        /// <param name="selectMaterialConditionSettings">查询条件</param>
        /// <returns></returns>
        DataTable getAllMaterialInfoBySelectMaterialConditon(SelectMaterialConditionSettings selectMaterialConditionSettings);

        /// <summary>
        /// 获得物料评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        DataTable getMaterialEvaluatedScore(SPCompareConditionValue sPCompareConditionValue);

        /// <summary>
        /// 获得供应商评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        DataTable getSupplierEvaluatedScore(SPCompareConditionValue sPCompareConditionValue);
    }
}
