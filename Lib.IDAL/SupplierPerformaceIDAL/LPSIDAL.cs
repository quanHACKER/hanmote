﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface LPSIDAL
    {
        #region 低效能供应商警告

        /// <summary>
        /// 根据5个主标准的分数，查询进入低效能供应商流程的供应商
        /// </summary>
        /// <param name="scoreThresholdList"></param>
        /// <param name="timePeriod"></param>
        /// <returns></returns>
        DataTable queryLPSByScore(List<Decimal> scoreThresholdList,String timePeriod);

        /// <summary>
        /// 根据供应商分级，查询进入低效能供应商流程的供应商
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        DataTable queryLPSByClassification(String year);

        /// <summary>
        /// 查询是否已经存在记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        bool queryRecordOrNot(String supplierID);

        /// <summary>
        /// 记录LPS流程状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="LPSState"></param>
        /// <param name="recordTime"></param>
        /// <param name="recordName"></param>
        /// <returns></returns>
        int saveLPSState(String supplierID, String LPSState, DateTime recordTime, String recordName);

        /// <summary>
        /// 查询LPS流程中的所有供应商
        /// </summary>
        /// <returns></returns>
        DataTable queryLPSSupplier();

        /// <summary>
        /// 查询LPS流程状态
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        String queryLPSDegree(String SupplierID);

        /// <summary>
        /// 保存关键指数及陈述
        /// </summary>
        /// <param name="suppler"></param>
        /// <param name="kpiList"></param>
        /// <param name="statement"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        int saveKPIStatement(String supplierID, List<String> kpiList, String statement, DateTime time, String name);
        #endregion

        #region LPS三级流程

        /// <summary>
        /// 查询供应商在特定时间范围的绩效表现 
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="timePeriod"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        DataTable querySupplierPerformance(String supplierID, String timePeriod,DateTime startTime,DateTime endTime);

        /// <summary>
        /// 退出LPS流程
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        int deleteLPSRecord(String supplierID);

        /// <summary>
        /// 更新LPS状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        int updateLPSState(String supplierID, String state);

        /// <summary>
        /// 添加至待淘汰供应商列表
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="outIdentity"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        int saveLPSOutList(String supplierID, String outIdentity, DateTime time, String name);

        /// <summary>
        /// 检查是否在LPS_Out_List中有记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        int checkLPSOutList(String supplierID);

        #endregion

        #region 供应商淘汰

        /// <summary>
        /// 查询待淘汰的低效能供应商
        /// </summary>
        /// <returns></returns>
        DataTable queryOutSupplier();

        /// <summary>
        /// 淘汰低效能供应商(从LPS_Out_List数据表中删除)
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        int deleteLPSupplier(String supplierID);

        /// <summary>
        /// 在Source_List数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int updateSupplierOutInSourceList(String ID);

        /// <summary>
        /// 在Supplier_Base数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        int updateSupplierOutInSupplierBase(String supplierID);

        /// <summary>
        /// 在Eliminate_Supplier_Record数据表中记录淘汰供应商记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        int insertEliminateSupplierRecord(String supplierID, String time, String name);

        #endregion
    }
}
