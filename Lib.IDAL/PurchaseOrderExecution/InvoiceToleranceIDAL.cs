﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.PurchaseOrderExecution;

namespace Lib.IDAL.PurchaseOrderExecution
{
    public interface InvoiceToleranceIDAL
    {
        int updateInvoiceTolerance(List<Invoice_Tolerance> toleranceList);
        List<Invoice_Tolerance> getInvoiceTolerance();
    }
}
