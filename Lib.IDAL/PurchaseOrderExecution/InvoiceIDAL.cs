﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.PurchaseOrderExecution;

namespace Lib.IDAL.PurchaseOrderExecution
{
    public interface InvoiceIDAL
    {
        int addInvoice(Invoice invoice, List<Invoice_Item> itemList);
        int deleteInvoice(string invoiceID);
        int updateInvoice(Invoice invoice, List<Invoice_Item> itemList);
        Invoice getInvoice(string invoiceID);
        List<Invoice> getInvoiceListByConditions(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime);
        List<Invoice_Item> getInvoiceItemByInvoiceID(string invoiceID);
    }
}
