﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.CatalogPurchase;

namespace Lib.IDAL.CatalogManageIDAL
{
    public interface CatalogMaterialIDAL
    {
        int addNewCatalogMaterial(CatalogMaterial catalogMaterial);
        CatalogMaterial getCatalogMaterial(string materialID, string supplierID);
        CatalogMaterial getCatalogMaterial(string materialCatalogID);
        DataTable getCatalogMaterialDataTable(string materialType, string supplierID, string[] keywords);
        List<string> getAllMaterialTypes();
        List<string> getAllSuppliers();
    }
}
