﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockTypeIDAL
    {
        /// <summary>
        /// 新增一个库存类型
        /// </summary>
        /// <param name="stockTypeModel">库存类型模板</param>
        /// <returns></returns>
        int insertStockType(StockTypeModel stockTypeModel);

        /// <summary>
        /// 根据类型Id修改库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        int updateStockTypeByTypeId(StockTypeModel stockTypeModel);

        /// <summary>
        /// 根据类型Id删除库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        int deleteStockTypeByTypeId(int typeId);

        /// <summary>
        /// 根据类型Id恢复库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        int restoreStockTypeByTypeId(int typeId);

        /// <summary>
        /// 获得最新获得的TypeId
        /// </summary>
        /// <returns></returns>
        int getLatestTypeId();

        /// <summary>
        /// 获得所有的库存类型
        /// </summary>
        /// <returns></returns>
        DataTable getAllStockType();

        /// <summary>
        /// 获得所有有效的库存类型
        /// </summary>
        /// <returns></returns>
        DataTable getValidStockType();

    }
}
