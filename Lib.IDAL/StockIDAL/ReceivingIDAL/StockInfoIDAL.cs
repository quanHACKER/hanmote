﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockInfoIDAL
    {
        /// <summary>
        /// 根据工厂ID查询仓库信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId);

        /// <summary>
        /// 根据工厂ID和库存id查询仓库信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId,string stockOrName);

        /// <summary>
        /// 根据
        /// </summary>
        /// <param name="stockId"></param>
        /// <returns></returns>
        DataTable getStockInfoByStockId(string stockId);
    }
}
