﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface DocumentDetailInfoIDAL
    {
        /// <summary>
        /// 插入凭证详细信息
        /// </summary>
        /// <param name="documentDetailInfoModel"></param>
        /// <returns></returns>
        int insertDocumentDetailInfo(List<DocumentDetailInfoModel> documentDetailInfoModelList);

        DataTable getMDocItemByDocId(string mDocId);
    }
}
