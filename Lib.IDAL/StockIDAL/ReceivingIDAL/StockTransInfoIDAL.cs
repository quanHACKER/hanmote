﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockTransInfoIDAL
    {
        /// <summary>
        /// 查找所有事务/事件信息
        /// </summary>
        /// <returns></returns>
        List<StockTransExtendModel> findAllStockTransInfo();

        /// <summary>
        /// 根据id查找事务/事件信息
        /// </summary>
        /// <param name="stockTransId">事务id</param>
        /// <returns></returns>
        StockTransExtendModel findStockTransInfoByID(string stockTransId);

        /// <summary>
        /// 插入新的事务/事件信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        int insertNewStockTransInfo(StockTransModel stockTransModel);

        /// <summary>
        /// 修改事务信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        int updateStockTransInfo(StockTransModel stockTransModel);

        /// <summary>
        /// 根据id删除事务信息
        /// </summary>
        /// <param name="stockTransModel">事务id</param>
        /// <returns></returns>
        int deleteStockTransInfo(string stockTransId);
    }
}
