﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface MaterialDocumentNewIDAL
    {
        /// <summary>
        /// 向数据库中插入物料凭证
        /// </summary>
        /// <param name="materialDocumentNewModel"></param>
        /// <returns></returns>
        int insertMaterialNewDocument(MaterialDocumentNewModel materialDocumentNewModel);

        /// <summary>
        /// 根据凭证号获取凭证头部信息
        /// </summary>
        /// <param name="mDocId"></param>
        /// <returns></returns>
        DataTable getMaterialNewDocument(string mDocId);
    }
}
