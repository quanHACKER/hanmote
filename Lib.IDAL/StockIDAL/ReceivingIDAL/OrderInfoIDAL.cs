﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface OrderInfoIDAL
    {
        /// <summary>
        /// 根据采购订单编号查找订单信息
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        DataTable getOrderinfoByPOId(string poId);

        /// <summary>
        /// 根据采购订单编号查找订单物料详情
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        DataTable getOrderItemByPOId(string poId);
    }
}
