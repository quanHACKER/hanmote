﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockSupplierInfoIDAL
    {
        /// <summary>
        /// 根据订单ID来查找与该订单号关联的供应商详细信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        OrderSupReceivingModel findOrderSupInfoById(string orderId);
    }
}
