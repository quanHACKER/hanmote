﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface MaterialDocumentIDAL
    {
        /// <summary>
        /// 向数据库中插入物料凭证
        /// </summary>
        /// <param name="materialDocument"></param>
        /// <returns></returns>
        int insertMaterialDocument(MaterialDocumentModel materialDocument);

        /// <summary>
        /// 查找每次刚插入的doc_ID号
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        MaterialDocumentExtendModel findMaterialDocumentByDocumentId(string stockDocumentId);

        /// <summary>
        /// 查找刚插入的doc_ID号
        /// </summary>
        /// <returns></returns>
        string findLatestMaterialDocument();
    }
}
