﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockFactoryInfoIDAL
    {
        /// <summary>
        /// 返回所有工厂信息
        /// </summary>
        /// <returns></returns>
        List<StockFactoryModel> findAllFactoryInfoBy();

        /// <summary>
        /// 根据工厂编码获取工厂名称
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        DataTable getFactoryInfoBy(string factoryId);
    }
}
