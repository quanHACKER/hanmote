﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface MoveTypeInfoIDAL
    {
        /// <summary>
        /// 查找所有移动类型信息
        /// </summary>
        /// <returns></returns>
        List<MoveTypeExtendModel> findALLMoveTypeInfo();

        /// <summary>
        /// 根据移动类型ID类查找移动类型信息
        /// </summary>
        /// <param name="moveTypeId">移动类型ID</param>
        /// <returns></returns>
        MoveTypeExtendModel findMoveTypeInfoByID(string moveTypeId);

        /// <summary>
        /// 插入新的移动类型
        /// </summary>
        /// <param name="moveTypeModel">移动类型类</param>
        /// <returns></returns>
        int insertNewMoveTypeInfo(MoveTypeModel moveTypeModel);

        /// <summary>
        /// 根据移动类型编码来修改数据
        /// </summary>
        /// <param name="moveTypeModel">移动类型类</param>
        /// <returns></returns>
        int updateMoveTypeInfo(MoveTypeModel moveTypeModel);

        /// <summary>
        /// 根据移动类型编码来删除数据
        /// </summary>
        /// <param name="moveTypeId">移动类型ID</param>
        /// <returns></returns>
        int deleteMoveTypeInfoById(string moveTypeId);

        /// <summary>
        /// 通过事务/事件名称来关联查询移动类型信息
        /// </summary>
        /// <param name="transName"></param>
        /// <returns></returns>
        List<MoveTypeExtendModel> findALLMoveTypeInfoByTransName(string transName);
    }
}
