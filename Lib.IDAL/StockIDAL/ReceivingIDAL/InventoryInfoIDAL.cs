﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface InventoryInfoIDAL
    {
        /// <summary>
        /// 查找库存信息
        /// </summary>
        /// <param name="materialIdOrName"></param>
        /// <param name="factoryIdOrName"></param>
        /// <param name="inventoryState"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        List<InventoryExtendModel> findInventoryInfo(string materialIdOrName, string factoryIdOrName, string inventoryState, string stock);

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string stockId, string batchId, string factoryId);

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string batchId);
    }
}
