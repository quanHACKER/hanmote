﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface getMaterialInfoIDAL
    {
        
        /// <summary>
        /// 由物料编码读出物料名
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        string getMaterialName(string materialid);


         /// <summary>
       /// 由物料编号获得物料类型
       /// </summary>
       /// <param name="materialid"></param>
       /// <returns></returns>
        string getMaterialType(string materialid);

        /// <summary>
        /// 由物料编号获得物料单位
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        string getMeasurement(string materialid);

         /// <summary>
        /// 由物料编号获得物料组
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        string getMaterialGroup(string materialid);

         /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        DataTable getMaterialID();

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        DataTable getFactoryID();

           /// <summary>
        /// 获取所有库存地ID
        /// </summary>
        /// <returns></returns>
        DataTable getStockID();

              /// <summary>
        /// 获取所有库存盘点凭证号
        /// </summary>
        /// <returns></returns>
        DataTable getStocktakingDocuid();

        /// <summary>
        /// 获取所有库存盘点凭证号(循环盘点)  
        /// </summary>
        /// <returns></returns>
        DataTable getloopStocktakingDocuid();

        /// <summary>
        /// 获取全部批次号
        /// </summary>
        /// <returns></returns>
        DataTable getbatchid();
        /// <summary>
        /// 获取质检结果
        /// </summary>
        /// <returns></returns>
        DataTable getCheckResult();

        /// <summary>
        /// 获取质检分数
        /// </summary>
        /// <returns></returns>
        DataTable getCheckScore(string checkresult);
    }
}