﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL.StockReport
{
    public interface StockOverviewIDAL
    {
         /// <summary>
        /// 根据用户指定的要求筛选出符合要求的数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        DataTable selectInfo(DataTable dt);
    }
}
