﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface MoveHandleIDAL
    {
        /// <summary>
        /// 维护Inventory（入库）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>

        void updateInventoryStockIn(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
           string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU);

        /// <summary>
        /// 维护收货评分记录（采购入库）
        /// </summary>
        /// <param name="stockInNoteID"></param>
        /// <param name="orderID"></param>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="receiveCount"></param>
        /// <param name="countInOrder"></param>
        /// <param name="stockInDate"></param>
        /// <param name="Material_Type"></param>

        void receiveProcess(string stockInNoteID, string orderID, string supplierID, string materialID, double receiveCount, double countInOrder, DateTime stockInDate, string Material_Type);
        void receiveCheck(string stockInNoteID, string supplierID, string materialID);
        void goodsShipment(string stockInNoteID, string orderID, string supplierID, string materialID, double receiveCount, double countInOrder, DateTime stockInDate);
        void nonRawMaterialReceive(string stockInNoteID, string supplierID, string materialID);

        /// <summary>
        /// 维护到期提醒表（入库）
        /// </summary>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Material_ID"></param>
        /// <param name="batch_ID"></param>
        /// <param name="Manufacture_Date"></param>
        void updRemind(string Factory_ID, string Stock_ID, string Material_ID, string batch_ID, DateTime Manufacture_Date);

        /// <summary>
        /// 维护安全库存（入库）
        /// </summary>
        void upSaftyInventory(string factoryid, string materialid);


        /// <summary>
        /// 维护移动平均价（采购入库）
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="avgprice"></param>
        /// <param name="count"></param>
        void moveAvgPrice(string materialid, float avgprice, float count);
        void moveAvgPrice(string materialid, float avgprice, float count, string factoryid);

        /// <summary>
        /// 维护Inventory （出库）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        void updateInventoryStockOut(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
          string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU);

        /// <summary>
        /// 维护Inventory （转移）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        void updateInventoryStockTransfer(string Material_ID, string Factory_ID, string Stock_ID, string factoryid, string stockid, string Inventory_Type,
            string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU);


         void updateInventoryStockStateTransfer(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
        string Inventory_State, string newState, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU);


        /// <summary>
        /// 账期管理
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="factoryid"></param>
        /// <param name="stockid"></param>
        /// <param name="stockinDate"></param>
         void InventoryInfo(string materialid, string factoryid, string stockid, string Inventory_Type, string Batch_ID, string Inventory_State, string Evaluate_Type, string Material_Group, float count, float price, DateTime stockinDate, string SKU);
    }
}
