﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
    public interface CheckPostIDAL
    {
                /// <summary>
        /// 根据凭证编号选择满足条件的datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        DataTable selectInfo(string documentid, string accountyear);

          /// <summary>
        /// 根据数据表（selected,documentid,materialid,batchid,factoryid,stockid,reason）
        /// 进行过账
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        bool checkPost(DataTable info);
        
        /// <summary>
        /// 根据凭证号读取工厂库存地id
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        DataTable getfactory(string documentid);
    }
}
