﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
    public interface CreateStocktakingIDAL
    {
        /// <summary>
        /// 选择符合用户指定条件的物料（集中创建）
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <param name="FactoryID"></param>
        /// <param name="StockID"></param>
        /// <param name="EvaluateType"></param>
        /// <param name="MaterialGroup"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="b3"></param>
        /// <returns></returns>
        DataTable selectMaterial(string MaterialID, string FactoryID, string StockID, string EvaluateType, string MaterialGroup, bool b1, bool b2, bool b3);

        /// <summary>
        /// 产生盘点物料凭证（集中创建和单个创建共用）
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="i"></param>
        /// <param name="dtime2"></param>
        /// <param name="referID"></param>
        /// <param name="stockID"></param>
        /// <returns></returns>
        bool createDocument(DataTable dt,  DateTime dtime2, string referID, string stockID);

        /// <summary>
        /// 选择满足用户指定条件的物料(单个创建)
        /// </summary>
        /// <returns></returns>
        DataTable selectMaterialSingle(DataTable dt);


        /// <summary>
        /// 根据物料编号确定物料名称
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        string getMaterialname(string materialid);

        /// <summary>
        /// 盘点冻结
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        bool freezingstock(string stocktakingDocuID);

        /// <summary>
        /// 盘点解冻
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        bool defreezingstock(string stocktakingDocuID);

        /// <summary>
        /// 选择物料编号（单个创建）
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        DataTable selectMaterial(string factory, string stock);
    }
}
