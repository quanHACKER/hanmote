﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
    public interface CheckResultIDAL
    {
               /// <summary>
       /// 根据凭证ID读出
       /// </summary>
       /// <param name="documentID"></param>
       /// <returns></returns>
        DataTable loadTable(string documentID);

         /// <summary>
        /// 根据凭证ID读出工厂、库存地ID
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        DataTable selectbasicInfo(string documentID);

            /// <summary>
        /// 用户输入盘点结果，点击确定后保存于数据库中
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        bool inputcount(DataTable dt);
    }
}
