﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
    public interface ShowDifferenceIDAL
    {
           /// <summary>
        /// 根据用户指定的要求筛选出符合要求的数据，按照
        /// stocktakingDocument_ID,Material_ID,batch_ID,Factory_ID,Stock_ID,StockCount,Stocktaking_count,Difference,SKU排列
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        DataTable selectInfo(DataTable dt);
    }
}
