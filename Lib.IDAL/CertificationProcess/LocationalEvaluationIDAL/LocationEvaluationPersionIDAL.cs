﻿using Lib.Model.CertificationProcess.LocationEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.CertificationProcess.LocationalEvaluationIDAL
{
    public interface LocationEvaluationPersionIDAL
    {
        ///查询信息
        DataTable findAllEvalutionPersonList(String MtGroupName,String companyID ,int pageNum, int frontNum);

        ///条件查询
        DataTable findAllEvalutionPseronListByCondition(String companyID,String name,String evalcode, int pageNum, int frontNum);
        /// <summary>
        /// 用户数
        /// </summary>
        /// <returns></returns>
        int calculateEvalutionPersonNumber();
        /// <summary>
        /// 计算数据条数 
        /// </summary>
        /// <returns></returns>
        DataTable calculateRecordNumber();
        /// <summary>
        /// 根据ids得到评审员
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        DataTable getSelectedEvaluationPerson(String ids, string companyID, int pageNum, int frontNum);
        /// <summary>
        /// 通知评审员前插入信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="MtGroupName"></param>
        /// <param name="evaluateID"></param>
        void insertLocaEvalInfo(String evalCode, String companyID,String MtGroupName,String evaluateID);
        /// <summary>
        /// 得到评审员小组状态列表
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getLocalEvalInfoStatBySupplierID(String companyID);
        /// <summary>
        /// 根据company ID 查找是否已经建立过评估小组
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable findAllLocalEvalInfoByCompanyId(string companyID);
        /// <summary>
        /// 查看评审信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="evaluateID"></param>
        /// <returns></returns>
        DataTable detailLocalInfoByCompanyIDAndEvaluateID(string companyID, string evaluateID);
        /// <summary>
        ///  插入准入信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="company_Name"></param>
        /// <param name="contactMan"></param>
        /// <param name="acount"></param>
        /// <param name="password"></param>
        void insertSsupplier_MainAccountInfo(string companyID, string company_Name, string contactMan, string acount, string password);
        /// <summary>
        /// 插入等级
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        void setUpLvelBycompanyID(string companyID);

        /// <summary>
        /// 评审员评估
        /// </summary>
        /// <param name="v"></param>
        /// <param name="pageSize"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        DataTable singlePersonEvaluationList(string v, int pageSize, int pageIndex, out int totalSize);

        /// <summary>
        /// 得到高层信箱
        /// </summary>
        /// <returns></returns>
        string getSenorLeaderMail(string userId);

        /// <summary>
        /// 已有！评审小组
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getSelectedEvaluationPersonGroup(String companyID);
        /// <summary>
        /// 插入主数据信息
        /// </summary>
        /// <returns></returns>
        int insertSupplierBase(String SupplierId);

        /// <summary>
        /// 得到公司信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getCompangInfoByCompanyID(string companyID);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="iD"></param>
        void delEvalInfoByCompanyIDAndID(string companyID, object iD);
        /// <summary>
        /// 计算评估总分
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getSumEvaluateScoreByCompanyID(string user_ID, string companyID);

        /// <summary>
        /// 插入评估信息
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="state"></param>
        /// <param name="score"></param>
        /// <param name="scoreSum"></param>
        void insertLocalEvalInfoBySingelPerson(string companyID, string UserID, string filePath, string state, string score, string scoreSum,string DenyItems);

        /// <summary>
        /// 得到现场评分详情
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getDetailEvaluateScoreByCompanyID(string User_ID,string companyID);
        /// <summary>
        /// 当前信息状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        DataTable getSinglePersonEvalStayusByCompanyAndUserID(string companyID, string v);

        /// <summary>
        /// 查找评审结果状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        int getEndResultStatusBYCompanyID(string companyID);

        void deleteTaskSpecificationSportToUpper(string mainEvalID, string v);

        void setCompanyIDTagThirdZeroBycompanyID(string companyID);

        /// <summary>
        /// getunfinishedEvalPersonInfo
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        DataTable getunfinishedEvalPersonInfo(string companyID);
        /// <summary>
        /// 评审员充值信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        void resetInfoEvaluationBySinglePerson(string companyID, string v);

        /// <summary>
        /// 根据ID得到评审员邮箱
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        String getEvaluatorMailByEvalID(string v);
        /// <summary>
        /// 存入主审意见
        /// </summary>
        /// <param name="companyID"></param>
        void saveMainAdvicesOrSSEMFile(string userID,String SSEMFilePath, String MainEvalAdvice,string companyID,string mtGroupName);

        string getMainEvalIDByMtGroupName(string mtName,string mtPor);

        /// <summary>
        /// getMtGroupNameByCompanyId
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        string getMtGroupNameByCompanyId(string companyID);


        string getMtPorNameByCompanyId(string companyID);

        /// <summary>
        /// 删除评审小组
        /// </summary>
        /// <param name="companyID"></param>
        void deleteEvalGroupByCompanyID(string companyID);
        /// <summary>
        /// 主审将评审员信息重置
        /// </summary>
        /// <param name="evaluateID"></param>
        void backSinglePersonEvalInfo(string UserID,string evaluateID,string companyID);
        /// <summary>
        /// 的到评审员id
        /// </summary>
        /// <param name="evaluateID"></param>
        /// <returns></returns>
        string getEvalInfoEmaiml(string evaluateID);
        /// <summary>
        /// 更新任务状态 为已完成
        /// </summary>
        /// <param name="user_ID"></param>
        void updateTaskSpecification(string user_ID,string companyName);
        /// <summary>
        /// 更新任务状态为0 未完成
        /// </summary>
        /// <param name="evaluateID"></param>
        void setIsFinishedByUserID(string userID, string companyName);
        /// <summary>
        /// 插入任务
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="state"></param>
        void insertTaskSpecification(string UserID, string state);
        /// <summary>
        /// 查找任务
        /// </summary>
        /// <param name="user_ID"></param>
        /// <returns></returns>
        DataTable findAllTaskByUserID(string user_ID);
        /// <summary>
        /// 重新选择评审员后删除任务列表
        /// </summary>
        /// <param name="companyName"></param>
        void deleteTaskSpecification(string userID,string companyName);
    }
}
