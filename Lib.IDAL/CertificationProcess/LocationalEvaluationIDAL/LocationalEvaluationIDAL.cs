﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.Model.CertificationProcess.LocationEvaluation;
namespace Lib.IDAL.CertificationProcess.LocationalEvaluation
{
    public interface LocationalEvaluationIDAL
    {
        ///查询信息
        DataTable findAllCompanyList(string userId,int pageNum, int frontNum,out int totalSize);

        ///条件查询
        DataTable findAllCompanyListByCondition(string userId, LocationalEvaluationSupperModel locationalEvaluationModel, int pageNum, int frontNum);
        /// <summary>
        /// 用户数
        /// </summary>
        /// <returns></returns>
        int calculateUserNumber();
        /// <summary>
        /// 计算数据条数
        /// </summary>
        /// <returns></returns>
        DataTable calculateRecordNumber();

        DataTable findSimplePersonEvalListByCondition(string userId,LocationalEvaluationSupperModel searchCondition, int pageSize, int v);
        /// <summary>
        /// 现场评估完结后修改i标志位
        /// </summary>
        /// <param name="companyID"></param>
        void updateTagSuperSRInfoByCompanyID(string companyID);
        /// <summary>
        /// zhi A 
        /// </summary>
        /// <param name="companyID"></param>
        void setSupplierLevelValueABySupplierID(string companyID);

        /// <summary>
        /// 准入列表
        /// </summary>findCompanyCertificatedListByCondition
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        DataTable findAllCertificatedLocalCompanyList(string userId,int pageSize, int v);
        /// <summary>
        /// 准入查询
        /// </summary>
        /// <param name="searchCondition"></param>
        /// <returns></returns>
        DataTable findCompanyCertificatedListByCondition(string userId,LocationalEvaluationSupperModel searchCondition);
        /// <summary>
        /// insertHanmoteDecisionTable
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        void insertHanmoteDecisionTable(string id, string companyID, string v, string suggestion);
        /// <summary>
        /// 得到需要改善的供应商
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        DataTable findAllImprovementReportSupplier(string user_ID, int pageSize, int pageIndex,out int pageCount);
        /// <summary>
        /// 条件查找改善供应商
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <param name="nationalName"></param>
        /// <param name="companyNam"></param>
        /// <returns></returns>
        DataTable findAllImprovementReportSupplierByCondition(string user_ID, int pageSize, int v, string nationalName, string companyNam,string isProvement);
        /// <summary>
        /// 得到评审状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        string getISSubmitImprovementReportBySupplierId(string companyID);

        string getPidByCurUserID(string user_ID);
        /// <summary>
        /// 拒绝供应山
        /// </summary>
        /// <param name="iD"></param>
        void updateTagRefuseSRInfoByCompanyID(string iD);
    }
}
