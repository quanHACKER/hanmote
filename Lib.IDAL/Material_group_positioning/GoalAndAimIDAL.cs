﻿using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.Material_group_positioning
{
    public interface GoalAndAimIDAL
    {
        #region 生产性物料组
        //获取产品编号
         DataTable GetPrdId();
        //获取产品名称
        DataTable GetPrdName(string text);
        //获取组成产品的物料
        DataTable getMaterialInfo(string text);
        //插入供应目标
        int insertGoalInfo(prdGoalModel goalId);
        #endregion

    }
}
