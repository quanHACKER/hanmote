﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.CertificationProcess.GN;

namespace Lib.IDAL.Material_group_positioning
{

    public interface GeneralIDAL
    {
        DataTable loadgeneralitemdata(string id);
        DataTable loaditemdata(string id);
        DataTable loaddata(string id);

        DataTable searchdata(string id, string ORGID, string ORGName, string MGTID, string MGTName);
        DataTable searchitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName);
        DataTable searchgeneralitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName);

        List<String> getALLMtGroup();
        List<String> getALLorg();//获取所有采购组织
        List<String> getModel();//获取属性源绑定模板
        List<String> getALLorgID();//获取所有采购组织ID
        List<String> getALLitemorgID();//获取所有采购组织ID
        List<String> getALLgeneralitemorgID();//获取所有采购组织ID

        void SaveGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1);
        void SaveitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1);
        void SavegeneralitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1);

        void SaveModel(string name, string area, string aim, string goal);//保存模板
        void UpdateMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1);
        void UpdateitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1);
        void UpdategeneralitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1);


        void deletedata(string b, string G1, string Z1);
        void deleteitemdata(string G1, string Z1);
        void deletegeneralitemdata(string G1, string Z1);
    }
  }
