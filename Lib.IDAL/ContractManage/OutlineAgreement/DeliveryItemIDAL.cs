﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.IDAL.ContractManage.OutlineAgreement
{
    public interface DeliveryItemIDAL
    {
        List<Delivery_Item> getDeliveryItemList(string schedulingAgreementID,
            int materialItemNum);

        int updateDeliveryItemList(string schedulingAgreementID, int materialItemNum,
            List<Delivery_Item> itemList);
    }
}
