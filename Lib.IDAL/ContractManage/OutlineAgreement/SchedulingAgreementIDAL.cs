﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.IDAL.ContractManage.OutlineAgreement
{
    public interface SchedulingAgreementIDAL
    {
        Scheduling_Agreement getSchedulingAgreementByID(string schedulingAgreementID);

        List<Scheduling_Agreement> getSchedulingAgreementByCondition(
            Dictionary<string, object> conditions, DateTime beginTime, DateTime endTime);

        int updateSchedulingAgreement(string agreementContactID,
            Scheduling_Agreement schedulingAgreement);

        int updateSchedulingAgreement(Scheduling_Agreement schedulingAgreement,
            List<Scheduling_Agreement_Item> itemList);

        int deleteSchedulingAgreementByID(string schedulingAgreementID);

        int addSchedulingAgreement(Scheduling_Agreement schedulingAgreement,
            List<Scheduling_Agreement_Item> itemList);
    }
}
