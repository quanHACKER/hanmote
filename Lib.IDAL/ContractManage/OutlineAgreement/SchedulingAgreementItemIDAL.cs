﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.IDAL.ContractManage.OutlineAgreement
{
    public interface SchedulingAgreementItemIDAL
    {
        int addSchedulingAgreementItem(Scheduling_Agreement_Item item);

        List<Scheduling_Agreement_Item> getSchedulingAgreementItemListByID(
            string schedulingAgreementID);

        int updateSchedulingAgreementItem(string schedulingAgreementID,
            Scheduling_Agreement_Item item);

        int deleteSchedulingAgreementItem(string schedulingAgreementID);
    }
}
