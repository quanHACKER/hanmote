﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.QuotaArrangement;

namespace Lib.IDAL.ContractManage.QuotaArrangement
{
    public interface QuotaArrangementIDAL
    {
        List<Quota_Arrangement> getQuotaArrangement(string materialID,
            string factoryID);
        int deleteQuotaArrangement(string quotaArrangementID);
        int addQuotaArrangement(Quota_Arrangement quotaArrangement);
        int updateQuotaArrangement(Quota_Arrangement quotaArrangement);
    }
}
