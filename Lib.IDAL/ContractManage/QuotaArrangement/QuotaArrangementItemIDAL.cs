﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.QuotaArrangement;

namespace Lib.IDAL.ContractManage.QuotaArrangement
{
    public interface QuotaArrangementItemIDAL
    {
        List<Quota_Arrangement_Item> getQuotaArrangementItemList(
            string quotaArrangementID);

        int addQuotaArrangementItemList(string quotaArrangementID,
            List<Quota_Arrangement_Item> itemList);
    }
}
