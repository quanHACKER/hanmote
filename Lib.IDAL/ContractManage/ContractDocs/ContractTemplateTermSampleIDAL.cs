﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTemplateTermSampleIDAL
    {
        Contract_Template_Term_Sample getContractTemplateTermSampleByTermSampleName(
            string termSampleName);
        int addNewContractTemplateTermSample(Contract_Template_Term_Sample newTermSample);
        DataTable getDataTableByExecSql(string sql);
        DataTable getContractTemplateTermSampleByType(string type);

    }
}
