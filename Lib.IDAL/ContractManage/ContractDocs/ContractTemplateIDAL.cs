﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTemplateIDAL
    {
        Contract_Template getContractTemplateByID(string ContractTemplateID);
        int addNewContractTemplate(Contract_Template contractTemplate);
        LinkedList<Contract_Template> getContractTemplateBySQL(string sql);
        DataTable getContractTemplateDataTableBySQL(string sql);
        int UpdateContractTemplateState(LinkedList<string> IDList, string changeToState);
        int DeleteContractTemplate(LinkedList<string> IDList);

    }
}
