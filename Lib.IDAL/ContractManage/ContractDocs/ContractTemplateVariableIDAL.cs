﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTemplateVariableIDAL
    {
        LinkedList<Contract_Template_Variable> getContractTemplateVariableByContractTemplateTermSampleID(string contractTemplateTermSampleID);
        int addNewContractTemplateVariable(Contract_Template_Variable newVariable);

    }
}
