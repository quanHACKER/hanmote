﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTemplateTermIDAL
    {
        LinkedList<Contract_Template_Term_Sample> getAllTermSample();
        int addNewContractTemplateTerm(Contract_Template_Term newTerm);
        LinkedList<Contract_Template_Term> getContractTemplateTermByContractTemplateID(string contractTemplateSectionID);

    }
}
