﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTextIDAL
    {
        DataTable GetAllContractTextInfo(DataTable dtIn);
        int addContractText(ContractText contractText);
        LinkedList<ContractText> GetContractTextByBuyerID(string BuyerID, string state);
        int UpdateContractTextState(LinkedList<string> IDList, bool changeToState);
        LinkedList<ContractText> getContractTextBySQL(string sql);
        int DeleteContractTemplate(LinkedList<string> IDList);
    }
}
