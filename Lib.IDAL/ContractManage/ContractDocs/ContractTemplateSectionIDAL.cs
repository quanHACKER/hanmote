﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;

namespace Lib.IDAL.ContractManage.ContractDocs
{
    public interface ContractTemplateSectionIDAL
    {
        int addNewContractTemplateSection(Contract_Template_Section newSection);
        LinkedList<Contract_Template_Section> getContractTemplateSectionByContractTemplateID(string contractTemplateID);

    }
}
