﻿using Lib.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.MDIDAL.GoalValueAndLowValue
{
   public interface GoalValueAndLowValueIDAL
    {
        //查询目标值和最低值
        DataTable getGoalAndLow();

        //新建目标值和最低值
        bool newGoalAndLowValue(GoalValueAndLowValueModel goalValueAndLowValueModel);

    }
}
