﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using System.Data;

namespace Lib.SqlIDAL
{
    public interface AccumentIDAL
    {
        //获取所有公司代码
        List<string> GetCompanyCode();
        //获取所有货币类型
        List<string> GetCurrencyType();
        //获取所有评估类
        List<string> GetEvaluationClass();
        //获取所有供应商
        List<string> GetAllSupplier();
        //通过所选供应商获取供应商描述
        string GetSuppplierDescription(string SupplierID);
        //获取所有税码
        List<string> GetTaxCode();
        //通过所选税码获取税码描述
        string GetTaxDescription(string TaxID);
        //获取所有工厂
        List<string> GetAllFactory();
        //通过所选工厂获取所属库存地
        List<string> GetAllStock(string FactoryID);
        //获取所有科目
        List<string> GetAllAccount();
        //获取所有科目名称
        List<string> GetAllAccountName();
        //记账原材料科目
        bool DebitRawMaterial(DateTime time, string number, float dmoney, float cmoney, string factory, string stock, string type);
       //借记服务类科目
       bool DebitServiceClass(DateTime time, string number, float money, string type);
       //借记产成品科目
       bool DebitFinishedProduct(DateTime time, string number, float money, string factory, string stock, string type);
       //借记应付账款科目
       bool DebitAccountsPayable(DateTime time, string number, float money);
        //借记应交税费科目
       bool DebitTaxPayable(DateTime time, string number, float money);
        //借记银行存款科目
       bool DebitBankDeposit(DateTime time, string number, float money);
        //借记在途物资科目
       bool DebitAfloatMaterials(DateTime time, string number, float money);
        //借记材料成本差异科目
       bool DebitMaterialCostVariances(DateTime time, string number, float money, string type);
        //借记GR/IR科目
       bool DebitGRIR(DateTime time, string number, float money, string type);
       //将会计凭证项目信息写入数据表
       bool WriteAccountQuery(string code, int rownumber, string supplierID, string AccountID, float debit, float credit, string taxcode);
        //将会计凭证抬头信息写入数据表
       bool WriteAccountFrount(string code,DateTime time,string type,string ccode);
       //读取凭证抬头数据
       DataTable ReadAccountFrount(string code);
        //读取凭证项目信息
       DataTable ReadAccountInner(string code);
        //获取所有财务凭证编码
       List<string> GetAllVocherID();
        //获取所有科目详细信息
       DataTable GetALLAccountInformation();
        //获取所有税码详细信息
       DataTable GetAllTaxCodeInformation();
        //通过税码获取税率
       DataTable GetTaxByTaxCode(string TaxCode);
        //获取所有订单编码
       List<string> GetAllOrderID();
        //获取所有订单的详细信息
       DataTable GetAllOrderInformation();
        //根据订单编号查询订单数额
       double GetOrderAmount(string orderid);
        


       // //借记质检库存科目
       //bool DebitCheckInventory(DateTime time, string number, float money, string type);
       // //借记报废库存科目
       //bool DebitScrapInventory(DateTime time, string number, float money, string type);
       //贷记原材料科目
       //bool CreditRawMaterial(DateTime time, string number, float money, string factory, string stock, string type);
       ////贷记服务类科目
       //bool CreditServiceClass(DateTime time, string number, float money, string type);
       ////贷记产成品科目
       //bool CrediFinishedProduct(DateTime time, string number, float money, string factory, string stock, string type);
       //贷记应付账款科目

       bool CreditAccountsPayable(DateTime time, string number, float money);
        //贷记应交税费科目
       //bool CreditTaxPayable(DateTime time, string number, float money);
       ////贷记银行存款科目
       //bool CreditBankDeposit(DateTime time, string number, float money);
       ////贷记在途物资科目
       //bool CreditAfloatMaterials(DateTime time, string number, float money);
       ////贷记材料成本差异科目
       //bool CreditMaterialCostVariances(DateTime t        ime, string number, float money, string type);
       ////贷记GR/IR科目
       //bool CreditGRIR(DateTime time, string number, float money, string type);
       ////贷记质检库存科目
       //bool CreditCheckInventory(DateTime time, string number, float money, string type);
       ////贷记报废库存科目
       //bool CreditScrapInventory(DateTime time, string number, float money, string type);
        //将会计凭证信息写入会计凭证查询数据表
       //bool AccountQuery(DateTime time, string number, string type, string code, string classa, string supliera, string accounta, string debita, string credita, string factorya, string stocka, string classb, string suplierb, string accountb, string debitb, string creditb, string factoryb, string stockb);
        
    }

}
