﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.SP;

namespace Lib.IDAL.MDIDAL.SP
{
   public interface SupplierCompanyIDAL
    {
       //获取供应商公司代码级别数据
       SupplierCompanyCode GetSupplierCompanyInformation(string supplierid, string companyid);
       //更新供应商公司代码级别数据
       bool UpdateSupplierCompanyInformation(SupplierCompanyCode spcpy);
    }
}
