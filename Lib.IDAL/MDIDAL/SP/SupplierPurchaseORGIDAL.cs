﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.SP;

namespace Lib.IDAL.MDIDAL.SP
{
   public interface SupplierPurchaseORGIDAL
    {

       //查询供应商采购组织级别数据
       SupplierPurchaseOrganization GetSupplierPurchaseOrganizationInformation(string supplierID, string PurchaseORGID);
       //更新供应商采购组织级别数据
       bool UpdateSupplierPurchaseORGinformation(SupplierPurchaseOrganization sporg);
    }
}
