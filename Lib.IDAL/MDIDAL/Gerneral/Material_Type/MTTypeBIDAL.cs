﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.MDIDAL.Gerneral.Material_Type
{
   public interface MTTypeBIDAL
    {
        //获取所有物料类型
        DataTable GetAllLittleClassfy();
        //获取所有小分类名称
        List<string> GetAllBigClassfyName();
        //新建物料类型信息
        bool NewLittleClassfy(string lID, string lname, string bname);
        //删除选定物料类型
        bool DeleteLittleClassfy(string ID);
       //通过一级分类获取所属二级分类名称
        List<string> GetLittleClassfyByBig(string name);


        List<string> GetAllMtGroupName();
        List<string> GetAllfiled();
    }
}
