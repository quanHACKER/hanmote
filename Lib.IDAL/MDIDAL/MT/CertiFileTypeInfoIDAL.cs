﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.MDIDAL.MT
{
    public interface CertiFileTypeInfoIDAL
    {
         DataTable GetCertiFileInfo();
        int GetCertiFileInfo(string id);
    }
}
