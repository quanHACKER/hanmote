﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.FileManage;

namespace Lib.IDAL.FileManage
{
    public interface FileAttachmentInfoIDAL
    {
        List<File_Attachment_Info> getFileAttachmentList(string fileID);
        int deleteFileAttachment(string fileID, string attachmentName);
    }
}
