﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.FileManage;

namespace Lib.IDAL.FileManage
{
    public interface FileInfoIDAL
    {
        File_Info getFileInfo(string fileID);
        List<File_Info> getFileInfoListByConditions(Dictionary<string, string> conditions,
            DateTime beginTime, DateTime endTime);
        int addFileInfo(File_Info fileInfo, List<File_Attachment_Info> fileAttachmentList);
        int deleteFileInfo(string fileID);
        int updateFileInfo(File_Info fileInfo, List<File_Attachment_Info> fileAttachmentList);
    }
}
