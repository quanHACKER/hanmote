﻿using Lib.SqlServerDAL.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.SystemConfig
{
   public interface SystemUserOrganicRelationshipIDAL
    {
        /// <summary>
        /// 查找所有的组织关系人员信息
        /// </summary>
        /// <returns></returns>
       DataTable  findAllSystemUserOrganicRelationshipInfo(int pageNum, int front,int type);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
       DataTable findAllSystemUserOrganicRelationshipInfoByCondition(SystemUserOrganicRelationshipModel model,int pageSize,int front);
        /// <summary>
        /// 删除/激活ID
        /// </summary>
        /// <param name="userID"></param>
        void delOrActivitiUser(string userID,int a);
        /// <summary>
        /// 计数
        /// </summary>
        /// <returns></returns>
        DataTable calculateRecordNumber();
        /// <summary>
        /// 得到部门名称
        /// </summary>
        /// <returns></returns>
        DataTable getOrganicNameSQL(string comanyID);
        /// <summary>
        /// 采购组织组
        /// </summary>
        /// <returns></returns>
        DataTable findAllPurGroupName();

        /// <summary>
        /// 得到决策提交信息表
        /// </summary>
        /// <returns></returns>
        DataTable getUserInfoByRoleName(String role);
        /// <summary>
        /// findAllMtGroupName
        /// </summary>
        /// <returns></returns>
        DataTable findAllMtGroupName();

        /// <summary>
        /// d的打破角色类型
        /// </summary>
        /// <returns></returns>
        DataTable getInfoByRoleUserGroup();
   

        /// <summary>
        /// 根据物料组名称得到物料组
        /// </summary>
        /// <returns></returns>
        DataTable findAllMtGroupNameByPurGroupName(string name,string userID,int type);

        /// <summary>
        /// 职务
        /// </summary>
        /// <returns></returns>
        DataTable getRoleNameSQL(string comanyID);
        /// <summary>
        /// 公司
        /// </summary>
        /// <returns></returns>
        DataTable getCompanyNameSQL();
        /// <summary>
        /// add
        /// </summary>
        /// <param name="model"></param>
        void insertNewUser(SystemUserOrganicRelationshipModel model);
        /// <summary>
        /// edit
        /// </summary>
        /// <param name="model"></param>
        void updateUserInfor(SystemUserOrganicRelationshipModel model);
        /// <summary>
        /// queryUserInforById
        /// </summary>
        /// <param name="model"></param>
        DataTable queryUserInforById(String userId);
        /// <summary>
        /// delete user info
        /// </summary>
        /// <param name="userId"></param>
        void deleteSysUserInfoByUserId(string userId);
        /// <summary>
        /// 得到物料组名字
        /// </summary>
        /// <returns></returns>
        DataTable getMtGroupName();

        DataTable getMainEvaulatorSelectdMtGroupAndPurGroupByUserID(string userID, string purGroupName);

        /// <summary>
        /// 条件查询信息<重载>
        /// </summary>  
        /// <param name="userName"></param>
        /// <param name="departName"></param>
        /// <returns></returns>
        DataTable findAllSystemUserOrganicRelationshipInfoByCondition(string userName, string departName,int type);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userID"></param>
        void insertHanmote_User_MtGroupName(string userID, string name, string purName,int type);
        /// <summary>
        /// 更新物料组-主审关系
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <param name="v"></param>
        void updateHanmote_User_MtGroupName(string sysUser_ID, string v, string purName,int type);
        /// <summary>
        /// 1 删除前有的评估员身份 2 删除前有的主审身份
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <param name="a"></param>
        void deleteHamote_User_MtGroup(string sysUser_ID,int a);
        /// <summary>
        /// 得到直接上级
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <returns></returns>
        string getUpUserNameById(string sysUser_ID);
        /// <summary>
        /// 插入评审员-评估方面的关系
        /// </summary>
        /// <param name="v"></param>
        /// <param name="code"></param>
        void insertHanmoteBaseUser_EvalCode(string v, string code,string evalDescription);
        void deleteHanmoteBaseUser_EvalCode(string v, string code);
        /// <summary>
        /// 根据用户得到物料组idf
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <returns></returns>
        string getMtGroupName(string sysUser_ID);
        /// <summary>
        /// 根据用户得到评估方面
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <returns></returns>
        string getEvalCodeByUserId(string sysUser_ID);
        /// <summary>
        /// dedaoPID
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <returns></returns>
        string getPIDByUserId(string sysUser_ID);
        /// <summary>
        /// 评估员-物料组
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <returns></returns>
        string getMtMethodGroupName(string sysUser_ID);
    }
}
