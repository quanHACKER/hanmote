﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface BaseRoleIDAL
    {
        /// <summary>
        /// 查找最大的角色编号
        /// </summary>
        /// <returns></returns>
        DataTable queryBiggestRoleID(String today);

        /// <summary>
        /// 根据角色编号查询角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        DataTable queryRoleInforById(BaseRoleModel roleModel);

        /// <summary>
        /// 更新角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        void updateRoleInforById(BaseRoleModel roleModel);

        /// <summary>
        /// 插入角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        void insertRole(BaseRoleModel roleModel);

         /// <summary>
        /// 查询所有的角色信息
        /// </summary>
        /// <returns></returns>
        DataTable queryAllRoleInfor();

        /// <summary>
        /// 查询当前角色及所有子角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        DataTable queryOffspringInfor(BaseRoleModel roleModel);

        /// <summary>
        /// 查询当前角色的子角色信息（不包括当前角色）
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        DataTable queryOffspringInforWithoutSelf(BaseRoleModel roleModel);

        /// <summary>
        /// 根据角色编号查询其关联的用户信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        DataTable queryUserInforByRoleId(BaseRoleModel roleModel);

        /// <summary>
        /// 删除某角色和用户的关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        void deleteUserRoleLinkByRoleId(JoinUserRoleModel userRoleModel);

        /// <summary>
        /// 删除数据库中当前节点及所有子节点并更新受影响节点的左右值
        /// </summary>
        /// <param name="roleModel"></param>
        void deleteOffspringOrganizationAndUpdateNodes(BaseRoleModel roleModel);

        /// <summary>
        /// 插入用户和角色关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        void insertUserRoleLink(JoinUserRoleModel userRoleModel);
       
    }
}
