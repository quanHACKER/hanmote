﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig; 

namespace Lib.SqlServerDAL.SystemConfig
{
    public interface DatabaseBackupRecordIDAL
    {
        /// <summary>
        /// 查询所有的备份记录
        /// </summary>
        /// <returns></returns>
        DataTable queryAllBackupRecord();

        /// <summary>
        /// 根据记录ID查询备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        /// <returns></returns>
        DataTable queryBackupRecordById(DatabaseBackupRecordModel recordModel);

        /// <summary>
        /// 插入新备份记录到DB
        /// </summary>
        /// <param name="recordModel"></param>
        void insertBackupRecord(DatabaseBackupRecordModel recordModel);

        /// <summary>
        /// 更新备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        void updateBackupRecordById(DatabaseBackupRecordModel recordModel);

        /// <summary>
        /// 删除备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        void deleteBackupRecordById(DatabaseBackupRecordModel recordModel);
    }
}
