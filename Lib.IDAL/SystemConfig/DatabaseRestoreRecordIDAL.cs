﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public interface DatabaseRestoreRecordIDAL
    {
        /// <summary>
        /// 查询所有恢复记录
        /// </summary>
        /// <returns></returns>
        DataTable queryAllRestoreRecord();

        /// <summary>
        /// 根据记录ID查询恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        /// <returns></returns>
        DataTable queryRestoreRecordById(DatabaseRestoreRecordModel recordModel);

        /// <summary>
        /// 插入新恢复记录到DB
        /// </summary>
        /// <param name="recordModel"></param>
        void insertRestoreRecord(DatabaseRestoreRecordModel recordModel);

        /// <summary>
        /// 更新恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        void updateRestoreRecord(DatabaseRestoreRecordModel recordModel);

        /// <summary>
        /// 删除恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        void deleteRestoreRecord(DatabaseRestoreRecordModel recordModel);
    }
}
