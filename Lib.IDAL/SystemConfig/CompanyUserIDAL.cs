﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.Model.SystemConfig;
namespace Lib.IDAL.SystemConfig
{   /// <summary>
    /// author：hezl
    /// </summary>
    public interface CompanyUserIDAL
    {
        /// <summary>
        /// 用户数
        /// </summary>
        /// <returns></returns>
        int calculateUserNumber();
        /// <summary>
        /// 计算数据条数
        /// </summary>
        /// <returns></returns>
        DataTable calculateRecordNumber();
        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="companyUserModel"></param>
        void addCompanyUserInfo(CompanyUserModel companyUserModel);
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="User_ID"></param>
        void delCompanyUserInfo(String Supplier_Id,String User_ID);
        /// <summary>
        /// 编辑信息
        /// </summary>
        /// <param name="companyUserModel"></param>
        void editCompanyUserIndo(CompanyUserModel companyUserModel);
        /// <summary>
        /// 查询当前公司所有信息
        /// </summary>
        /// <returns></returns>
        DataTable findAllCompanyuUserList(String companyID,int pageNum, int frontNum);
        /// <summary>
        /// 根据条件查询信息
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Login_Name"></param>
        /// <returns></returns>
        DataTable findAllCompanyListByCondition(String companyName, int pageNum, int frontNum);
    }
}
