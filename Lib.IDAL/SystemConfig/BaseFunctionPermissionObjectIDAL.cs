﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface BaseFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询某个菜单对应的窗口的操作项
        /// </summary>
        /// <param name="baseFunctionPermissionModel"></param>
        /// <returns></returns>
        DataTable queryPermissionByMenuName(BaseFunctionPermissionObjectModel baseFunctionPermissionModel);
    }
}
