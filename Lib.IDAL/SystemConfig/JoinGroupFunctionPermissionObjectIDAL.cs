﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinGroupFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户所有组具有的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromGroup(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 查询某用户所在的组是否有permissionId对应的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        DataTable queryPermissionByIdFormGroup(BaseUserModel baseUserModel, int permissionId);

        /// <summary>
        /// 查询一个组所具有的权限
        /// </summary>
        /// <param name="baseGroupModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromSingleGroup(BaseGroupModel baseGroupModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 收回组权限
        /// </summary>
        /// <param name="objModel"></param>
        void revokeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel);

        /// <summary>
        /// 授予组权限
        /// </summary>
        /// <param name="objModel"></param>
        void authorizeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel);

        /// <summary>
        /// 收回一个组对应的所有权限
        /// </summary>
        /// <param name="objModel"></param>
        void revokeGroupAllPermission(JoinGroupFunctionPermissionObjectModel objModel);
    }
}
