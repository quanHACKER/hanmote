﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinUserRoleIDAL
    {
        /// <summary>
        /// 检查用户和角色联系是否已存在
        /// </summary>
        /// <param name="userRoleModel"></param>
        DataTable checkUserRoleLinkExist(JoinUserRoleModel userRoleModel);

        /// <summary>
        /// 添加角色给用户，即写入用户和角色的联系
        /// </summary>
        /// <param name="userRoleModel"></param>
        void addRoleToUser(JoinUserRoleModel userRoleModel);

        /// <summary>
        /// 从用户收回角色，即移除用户和角色间联系
        /// </summary>
        /// <param name="userRoleModel"></param>
        void removeRoleFromUser(JoinUserRoleModel userRoleModel);

        /// <summary>
        /// 查询用户所具有的角色
        /// </summary>
        /// <param name="userRoleModel"></param>
        DataTable queryRoleByUserId(JoinUserRoleModel userRoleModel);
    }
}
