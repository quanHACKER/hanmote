﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinRoleFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户所有角色具有的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromRoleOfMenuName(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 查询一个用户所有角色是否具有某Permission_ID对应的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromRoleOfPermissionID(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 查询一个角色所具有的权限（对应Menu_Name）
        /// </summary>
        /// <param name="baseRoleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromSingleRoleOfMenuName(BaseRoleModel baseRoleModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 查询一个角色所具有的权限（对应Permission_ID）
        /// </summary>
        /// <param name="baseRoleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        DataTable queryPermissionFromSingleRoleOfPermissionID(BaseRoleModel baseRoleModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 收回角色权限
        /// </summary>
        /// <param name="objModel"></param>
        void revokeRolePermission(JoinRoleFunctionPermissionObjectModel objModel);

        /// <summary>
        /// 授予角色权限
        /// </summary>
        /// <param name="objModel"></param>
        void authorizeRolePermission(JoinRoleFunctionPermissionObjectModel objModel);
    }
}
