﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinUserGroupIDAL
    {
        /// <summary>
        /// 检查用户和组联系是否已存在
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        /// <returns></returns>
        DataTable checkUserGroupLinkExist(JoinUserGroupModel joinUserGroupModel);

        /// <summary>
        /// 添加用户和组的联系
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        void addUserToGroup(JoinUserGroupModel joinUserGroupModel);

        /// <summary>
        /// 从组中删除用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        void removeUserFromGroup(JoinUserGroupModel joinUserGroupModel);

        /// <summary>
        /// 查询用户的组信息
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        /// <returns></returns>
        DataTable queryGroupByUserId(JoinUserGroupModel joinUserGroupModel);

        /// <summary>
        /// 删除一个用户组中的所有用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        void deleteAllUserOfGroup(JoinUserGroupModel joinUserGroupModel);
    }
}
