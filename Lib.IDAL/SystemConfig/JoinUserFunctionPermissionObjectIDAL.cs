﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinUserFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户的直接授权权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        DataTable queryUserFunctionByID(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel);

        /// <summary>
        /// 查询用户是否直接具有某个权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        DataTable queryUserFunctionPermissionById(BaseUserModel baseUserModel, int permissionId);

        /// <summary>
        /// 收回用户被授予的权限
        /// </summary>
        /// <param name="objModel"></param>
        void revokePermission(JoinUserFunctionPermissionObjectModel objModel);

        /// <summary>
        /// 授予用户新的权限
        /// </summary>
        /// <param name="objModel"></param>
        void authorizePermission(JoinUserFunctionPermissionObjectModel objModel);
    }
}
