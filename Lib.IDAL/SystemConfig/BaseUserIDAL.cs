﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface BaseUserIDAL
    {
        /// <summary>
        /// 验证用户登录信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        DataTable validateUserInformation(BaseUserModel baseUserModel);

        /// <summary>
        /// 查询当前最大的用户编号
        /// </summary>
        /// <returns></returns>
        DataTable queryBiggestUserID(String today);

        /// <summary>
        /// 计算记录条数
        /// </summary>
        /// <returns></returns>
        DataTable calculateRecordNumber();

        /// <summary>
        /// 查询用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        DataTable queryUserInformationById(BaseUserModel baseUserModel);

        /// <summary>
        /// 根据查询条件查询用户信息
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        DataTable queryUserInformationBySearchCondition(BaseUserModel userModel);
        /// <summary>
        /// 企业用户人员
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        DataTable getSysUserList(int v1, int v2);

        /// <summary>
        /// 查询所有用户的信息
        /// </summary>
        /// <returns></returns>
        DataTable queryAllUserInformation(int pageNum, int frontNum);
        /// <summary>
        /// 根据权限得到节点
        /// </summary>
        /// <param name="user_ID"></param>
        /// <returns></returns>
        DataTable getAuthorNodes(string user_ID);
           
        /// <summary>
        /// 插入一个新用户
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        void insertUserInformation(BaseUserModel baseUserModel);

        /// <summary>
        /// 更新用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        void updateUserInformation(BaseUserModel baseUserModel);

        /// <summary>
        /// 设置用户是否可用
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        /// <param name="enable"></param>
        void setUserEnable(BaseUserModel baseUserModel);
    }
}
