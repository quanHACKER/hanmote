﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;

namespace Lib.IDAL.SourcingManage.ProcurementPlan
{
    /// <summary>
    /// 添加需求计划对应的物料
    /// </summary>
    public interface Demand_MaterialIDAL
    {
        Demand_Material findDemandMaterial(string demandId, string materialId);

        int addSummaryMaterials(Demand_Material demandMaterial);

        List<Demand_Material> findDemandMaterials(string demandid);

        int deleteDemandMaterial(string demandId, string materialId);

        List<Demand_Material> findDemandMaterialNonAggregate(List<string> demandIDs);

        int updateDemandMaterialAggregateID(Aggregate_Material am, List<string> demandIDs);

        int  updateSummaryMaterials(Demand_Material demand_material,string materialId);
    }
}
