﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceMaterialIDAL
    {
        int addSourceMaterials(SourceMaterial sm);

        List<SourceMaterial> findMaterialsBySId(string sourceId);

        SourceMaterial findSMBySIdMId(string sourceId, string materialId);

        List<SourceMaterial> findSourceMaterialsBySourceId(string p);

        List<SourceMaterial> findSourcesByMaterialId(string materialId);
    }
}
