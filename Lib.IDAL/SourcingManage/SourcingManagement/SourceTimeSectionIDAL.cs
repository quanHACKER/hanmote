﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceTimeSectionIDAL
    {

        List<SourceTimeSection> getSouTmsBySouSupMaId(string sourceId, string supplierId, string materialId);
    }
}
