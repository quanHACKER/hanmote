﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceItemIDAL
    {
        SourceItem ExistsBySSId(string sourceId, string supplierId);

        int addSourceItem(SourceItem sourceItem);

        #region 为创建合同服务
        List<string> getAllValidItemID();
        SourceItem getSourceItemByID(string itemID);
        List<string> getAllInvalidItemID();
        #endregion
    }
}
