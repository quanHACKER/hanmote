﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface LowestCostIDAL
    {

        int addLowestCost(LowestCost ls);

        List<LowestCost> getAllLowestBySId(string sid);
    }
}
