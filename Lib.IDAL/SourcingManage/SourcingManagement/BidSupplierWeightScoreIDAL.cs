﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
   public  interface BidSupplierWeightScoreIDAL
    {
        int addBidSupplierWeightScore(BidSupplierWeightScore bsws);

        int  getResultByBidSIdWeightName(string bid, string sid,string wn);

        List<BidSupplierWeightScore> getBidSWSByBIdSId(string bidId, string supplierId);

        BidSupplierWeightScore getBSWSByBIdSIdWN(string p, string supplierId, string p_2);
    }
}
