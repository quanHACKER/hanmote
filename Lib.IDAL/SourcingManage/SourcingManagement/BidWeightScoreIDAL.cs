﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface BidWeightScoreIDAL
    {
        int addBidWeightScore(BidWeightScore bws);

        List<BidWeightScore> findWeightScoresByBidId(string bidId);

    }
}
