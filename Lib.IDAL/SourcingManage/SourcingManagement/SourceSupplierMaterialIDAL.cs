﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceSupplierMaterialIDAL
    {
        int addSupplierMaterial(SourceSupplierMaterial ssm);

        List<SourceSupplierMaterial> findSSMBySId(string sourceId);

        List<SourceSupplierMaterial> getSSMBySuIdMId(string bidId, string materialId);
        List<SourceSupplierMaterial> getSSMBySIdSuId(string sourceId, string supplierId);
        SourceSupplierMaterial getSSMBySIdSIdMId(string bidId, string supplierId, string materialId);
    }
}
