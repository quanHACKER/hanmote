﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
     public interface SourceListIDAL
    {
         int addSourceList(SourceList sl);

         List<SourceList> getSourceList(string materialId,string factoryId);

         List<SourceList> getSourceList(string materialId, string factoryId,string startTime,string endTime);
        int updateSourceList(SourceList sourceList);
        SourceList getSourceListBySId(string sid);
    }
}
