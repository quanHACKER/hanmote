﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface ConditionIDAL
    {
        int addCondition(ConditionType condition);

        int updateCondition(ConditionType condition);

        List<ConditionType> getConditions();

        ConditionType findConditionTypeByConditionID(string conditionId);
    }
}
