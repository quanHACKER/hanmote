﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface EnquiryIDAL
    {
        DataTable FindSupplierById(String itemId);
    }
}
