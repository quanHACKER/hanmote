﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface BidPartnerIDAL
    {
       int addBidPartner(BidPartner bp);

       BidPartner findBidPartnerByBidId(string bidId);
    }
}
