﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceInquiryIDAL
    {
        int addNewInquiry_Table(SourceInquiry inquiry_Table);

        int updateInquiryState(String inquiry_id,String state);

        List<SourceInquiry> findSourcesByState(int state);

        SourceInquiry findSourceById(string sourceId);

        int updateSourceState(string sourceId, int newState);
    }
}
