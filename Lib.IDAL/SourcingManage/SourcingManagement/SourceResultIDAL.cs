﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceResultIDAL
    {
        int addSourceResult(SourceResult sim);

        #region 为创建合同服务
        List<SourceResult> getSourceItemMaterialByFK(string contractID);
        #endregion    
    }
}
