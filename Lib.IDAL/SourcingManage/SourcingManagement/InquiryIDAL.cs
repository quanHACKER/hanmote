﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using System.Data;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface InquiryIDAL
    {
        int addNewInquiry_Table(Inquiry_Table inquiry_Table);

        int updateInquiryState(String inquiry_id,String state);
    }
}
