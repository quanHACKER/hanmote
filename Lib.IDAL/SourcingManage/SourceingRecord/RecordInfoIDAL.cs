﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourceingRecord;

namespace Lib.IDAL.SourcingManage.SourceingRecord
{
    public interface RecordInfoIDAL
    {

        int addRecordInfo(RecordInfo ri);

        List<RecordInfo> getAllRecordInfos();

        List<RecordInfo> getRecordsByMaterialId(string materialId);
    }
}
