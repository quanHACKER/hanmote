﻿using Lib.Model.ServiceEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.SqlServerDAL
{
    public class ServiceSqlIDAL
    {
        DataTable dt = null;
        String sql = null;
        //获取供应商编号
       public DataTable getSupplierId(String loginId) {

            sql = @"select S1.Supplier_ID as 供应商编号,s1.Supplier_Name as 供应商名称 from Hanmote_User_MtGroupName m, Supplier_Base S1,SR_Info S2  where m.User_ID=@id and S1.Supplier_ID=S2.SupplierId and S2.SelectedDep=m.Pur_Group_Name and s2.SelectedMType =m.Mt_Group_Name";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@id",loginId),
            };
            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter);
            return dt;
        }

        public DataTable getExServiceData()
        {
            sql = @"select MtGroupName as 物料组名称,mtName as 物料名称,  supplierName as 供应商名称,TimeRate as 及时性权重,QualifyRate as 质量权重, exServiceId as 服务工单,supplierId as 供应商代码, serviceTime as 时间,serviceTotalScore as 综合,serviceQualifyScore as 质量评分,serviceTimeScore as 及时性评分,danWei as 单位 from Hanmote_ExtService";
          
            dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }
        /// <summary>
        /// 获取一般服务的供应商
        /// </summary>
        /// <returns></returns>
        public List<string> getGenSupplierName()
        {
            List<string> list = new List<string>();
            sql = @"select distinct supplierName from Hanmote_GenService";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }
        /// <summary>
        /// 获取一般服务所有物料组
        /// </summary>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        public List<string> getGenMateGroupName(string supplierName)
        {
            List<string> list = new List<string>();
            sql = @"select distinct MtGroupName from Hanmote_GenService where supplierName='" + supplierName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        /// <summary>
        /// 一般服务物料
        /// </summary>
        /// <param name="supplierName"></param>
        /// <param name="mateGroupName"></param>
        /// <returns></returns>
        public List<string> getGenMateName(string supplierName, string mateGroupName)
        {
            List<string> list = new List<string>();
            sql = @"select distinct mtName from Hanmote_GenService where supplierName='" + supplierName + "' and MtGroupName='" + mateGroupName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }





        public List<string> getMateName(string supplierName, string mateGroupName)
        {
            List<string> list = new List<string>();
            sql = @"select distinct mtName from Hanmote_ExtService where supplierName='"+ supplierName + "' and MtGroupName='"+ mateGroupName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows) {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public DataTable getGenServiceData(string supplierName, string mtGroupName, string mtName, string startTime, string endTime)
        {
            sql = @"select * from Hanmote_GenService ";

            if (!startTime.Equals(""))
                sql += " where ServiceTime >='" + startTime + "'";

            if (!endTime.Equals(""))
                sql += " and ServiceTime< ='" + endTime + "'";

            if (!supplierName.Equals(""))
            {
                sql += " and supplierName='" + supplierName + "'";
            }
            if (!mtGroupName.Equals(""))
                sql += " and MtGroupName='" + mtGroupName + "'";
            if (!mtName.Equals(""))
                sql += " and mtName='" + mtName + "'";


            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getExServiceData(string supplierName, string mtGroupName, string mtName,string startTime,string endTime)
        { 
            sql = @"select MtGroupName as 物料组名称,mtName as 物料名称,  supplierName as 供应商名称,TimeRate as 及时性权重,QualifyRate as 质量权重, exServiceId as 服务工单,supplierId as 供应商代码, serviceTime as 时间,serviceTotalScore as 综合,serviceQualifyScore as 质量评分,serviceTimeScore as 及时性评分,danWei as 单位 from Hanmote_ExtService ";

            if (!startTime.Equals(""))
                sql += " where serviceTime >='" + startTime + "'";

            if (!endTime.Equals(""))
                sql += " and serviceTime< ='" + endTime + "'";

            if (!supplierName.Equals("")) {
                sql += " and supplierName='" + supplierName + "'";
            }
            if (!mtGroupName.Equals(""))
                sql += " and MtGroupName='"+ mtGroupName + "'";
            if (!mtName.Equals(""))
                sql += " and mtName='" + mtName + "'";
          

            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        /// <summary>
        /// 获取服务汇总物料组
        /// </summary>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        public List<string> getMateGroupName(string supplierName)
        {
            List<string> list = new List<string>();
            sql = @"select distinct MtGroupName from Hanmote_ExtService where supplierName='"+ supplierName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public List<string> getSupplierName()
        {
            List<string> list = new List<string>();
            sql = @"select distinct supplierName from Hanmote_ExtService";
            dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public DataTable getExServiceRate(string mtGroupId)
        {
            sql = @"select serviceQuality as 服务质量,serviceTimeRate as 服务及时性 from Hanmote_MtGroup_serviceRate where mtGroupId='" + mtGroupId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getGeneServiceRate(string mtGroupId)
        {
            sql = @"select serviceReliable as 可靠性,serviceInnovate as 创新性,userService as 用户服务 from   Hanmote_MtGroup_serviceRate where  mtGroupId='"+ mtGroupId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getServiceRate(string mTGName)
        {
            sql = @"select * from Hanmote_MtGroup_serviceRate where mtGroupName like '%" + mTGName + "%'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getServiceRate()
        {
            sql = @"select   a.Material_Group as mtGroupId,a.Description as mtGroupName ,b.serviceQuality,b.serviceTimeRate,b.serviceInnovate,b.serviceReliable,b.userService   from  Material_Group a   left   join  Hanmote_MtGroup_serviceRate b     on   a.Material_Group=b.mtGroupId";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getAllMtGroupidAndName()
        {
            sql = @"select Material_Group as mtGroupId,Description as mtGroupName from Material_Group";

            dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }

        //自定义服务
        public bool insertServiceRate(DataGridView mtGroupRate)
        {
            int flag = 0;
       
        
            foreach (DataGridViewRow row in mtGroupRate.Rows) {

                sql = @"DECLARE @isexist INT;
                    select @isexist=count(*) from Hanmote_MtGroup_serviceRate  where mtGroupId='"+ row.Cells["mtGroupId"].Value.ToString()+ "'" +
                    " if(@isexist>0)" +
                    " update Hanmote_MtGroup_serviceRate set serviceQuality='"+ row.Cells["serviceQuality"].Value.ToString() + "',serviceTimeRate='" + row.Cells["serviceTimeRate"].Value.ToString() + "',serviceInnovate='" + row.Cells["serviceInnovate"].Value.ToString() + "',serviceReliable='" + row.Cells["serviceReliable"].Value.ToString() + "',userService='" + row.Cells["userService"].Value.ToString() + "' where mtGroupId='" + row.Cells["mtGroupId"].Value.ToString() 
                         + "'else " +
                    "insert into Hanmote_MtGroup_serviceRate(mtGroupId,mtGroupName,serviceQuality,serviceTimeRate,serviceInnovate,serviceReliable,userService) values('" + row.Cells["mtGroupId"].Value.ToString() + "','" + row.Cells["mtGroupName"].Value.ToString() + "','" + row.Cells["serviceQuality"].Value.ToString() + "','" + row.Cells["serviceTimeRate"].Value.ToString() + "','" + row.Cells["serviceInnovate"].Value.ToString() + "','" + row.Cells["serviceReliable"].Value.ToString() + "','" + row.Cells["userService"].Value.ToString() + "')";
                 flag+=DBHelper.ExecuteNonQuery(sql);

            }
            if (flag == mtGroupRate.Rows.Count)
            {

                return true;
            }
            else {
                return false;
            }
            
            
        }



        public DataTable getGenServiceData()
        {
            sql = @"select * from Hanmote_GenService";

            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool insertGenServiceModel(string innos, string res, string uSs, object modelName)
        {
            Boolean flag = false;
            sql = @"select * from Hanmote_genServicemodel where ServiceModelName='" + modelName + "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count > 0)
            {

                sql = @"update Hanmote_genServicemodel set Innos=@innos,Res=@res,USs=@uSs where ServiceModelName=@modelName";
            }
            else
            {
                sql = @"insert into Hanmote_genServicemodel(ServiceModelName,Innos,Res,USs) values(@modelName,@innos,@res,@uSs)";

            }

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@innos",innos),
                new SqlParameter("@res",res),
                new SqlParameter("@uSs",uSs),
                new SqlParameter("@modelName",modelName),

            };

            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }

        //获取物料信息
        public DataTable getMtidAndName(string mtGroupid)
        {
            sql = "select MtID as 物料编号,MtName as 物料名称 from MT_MTGroup_relationship where MtGroupID='" + mtGroupid + "'";
            return dt = DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getGenServiceModel(string name)
        {
            sql = "select * from Hanmote_genServicemodel where ServiceModelName='" + name + "'";
            return dt = DBHelper.ExecuteQueryDT(sql);
        }

        public List<string> getGenServiceModelName()
        {
            sql = @"select ServiceModelName from Hanmote_genServicemodel";
            List<String> modelName = new List<string>();
            dt = DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow row in dt.Rows)
            {
                modelName.Add(row[0].ToString());
            }
            return modelName;
        }
        /// <summary>
        /// 获取物料组的信息
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public DataTable getMtGroupidAndName(string supplierId)
        {
            sql = "select SelectedMType  from SR_Info where SupplierId='"+ supplierId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            string[] mtGroupName=new string[10];

            if (!(dt.Rows.Count == 0)) {
                if (dt.Rows[0][0].ToString().Contains(","))
                {
                    mtGroupName = dt.Rows[0][0].ToString().Split(',');
                }
                else
                {
                    mtGroupName[0] = dt.Rows[0][0].ToString();
                }
            }
            DataRow rw;
            DataTable myDt = new DataTable();
            foreach (String str in mtGroupName) {
                if (!(str==null)) {
                    sql = "select Material_Group as 物料组编号,Description as 物料组名称  from Material_Group where Description='" + str + "'";
                    rw = DBHelper.ExecuteQueryDT(sql).Rows[0];
                    if (myDt.Rows.Count == 0)
                    {
                        myDt = DBHelper.ExecuteQueryDT(sql);
                    }
                    else
                    {
                        myDt.Rows.Add(rw.ItemArray);

                    }
                }

                
               
            }

            return myDt;
        }

        public Boolean insertServiceModel(string sQs, string sTs, string modelName)
        {
            Boolean flag = false;
            sql = @"select *from Hanmote_exServiceModel where modelName='"+ modelName + "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count > 0)
            {

                sql = @"update Hanmote_exServiceModel set sQs=@sQs,sTs=@sTs where modelName=@modelName";
            }
            else {
                sql = @"insert into Hanmote_exServiceModel values(@modelName,@sQs,@sTs)";

            }

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@sQs",sQs),
                new SqlParameter("@sTs",sTs),
                new SqlParameter("@modelName",modelName),

            };

            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }

        public DataTable getServiceModel(string name)
        {

            sql = "select * from Hanmote_exServiceModel where modelName='" + name+"'";
           return  dt = DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 获取服务模板名称
        /// </summary>
        /// <returns></returns>
        public List<string> getServiceModelName()
        {
            sql = @"select modelName from Hanmote_exServiceModel";
            List<String> modelName = new List<string>();
             dt =DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow row in dt.Rows) {
                modelName.Add(row[0].ToString());
            }
            return modelName;
        }


        /// <summary>
        /// 保存服务数据
        /// </summary>
        /// <param name="serviceModel">服务数据对象</param>
        /// <returns></returns>
        public Boolean insertServiceData(ServiceModel serviceModel) {

            sql = @"insert into  Hanmote_ExtService(mtName,MtGroupName,TimeRate,QualifyRate,supplierName,supplierId,mtId,exServiceId,servicePlace,creator,serviceTime,serviceTimeScore,serviceTimeReason,serviceQualifyScore,serviceQualifyReson,serviceTotalScore)
                                                values(@mtName,@mtGroupName,@STRate,@SQRate,@supplierName,@supplierId,@mtId,@exServiceId,@servicePlace,@creator,getDate(),@serviceTimeScore,@serviceTimeReason,@serviceQualifyScore,@serviceQualifyReson,@serviceTotalScore)";
            Boolean flag = false;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@supplierId",serviceModel.SupplierId),
                new SqlParameter("@mtId",serviceModel.MtID),
                new SqlParameter("@mtName",serviceModel.MtName),
                new SqlParameter("@exServiceId",serviceModel.ExtServiceId),
                new SqlParameter("@servicePlace",serviceModel.ServicePlace),
                new SqlParameter("@creator",serviceModel.Creator),
                new SqlParameter("@serviceTimeScore",serviceModel.ServiceTimeScore),
                new SqlParameter("@serviceTimeReason",serviceModel.ServiceTimeReason),
                new SqlParameter("@serviceQualifyScore",serviceModel.ServiceQualityScroce),
                new SqlParameter("@serviceQualifyReson",serviceModel.ServiceQualityReason),
                new SqlParameter("@serviceTotalScore",serviceModel.ServiceTotalScore),
                new SqlParameter("@supplierName",serviceModel.SupplierName),
                new SqlParameter("@STRate",serviceModel.STRate1),
                new SqlParameter("@SQRate",serviceModel.SQRate1),
                new SqlParameter("@mtGroupName",serviceModel.MtGroupName)
            };
            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0) {
                flag = true;
            }
            return flag;
        }

        public bool insertGenServiceData(GeneralServiceModle serviceData)
        {
            sql = @"insert into  Hanmote_GenService(mtName,MtGroupName,InRate,ReRate,UserRate,supplierName,supplierId,mtId,GenServiceId,FactoryId,creator,ServiceTime,InnovationSco,InReason,RealizableSco,ReReson,UserServiceSco,UserSerReson,serviceTotalScore)
                                                values(@MtName,@MtGroupName,@InnoRate,@ReRate,@UserRate,@supplierName,@supplierId,@mtId,@GenServiceId,@FactoryId,@creator,@ServiceTime,@InnovationSco,@InReason,@RealizableSco,@ReReson,@UserServiceSco,@UserSerReson,@serviceTotalScore)";
            Boolean flag = false;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@supplierId",serviceData.SupplierId),
                new SqlParameter("@mtId",serviceData.MtID),
                new SqlParameter("@MtName",serviceData.MtName),
                new SqlParameter("@GenServiceId",serviceData.GenServiceId),
                new SqlParameter("@FactoryId",serviceData.FactoryId),
                new SqlParameter("@creator",serviceData.Creator),
                new SqlParameter("@ServiceTime",serviceData.ServiceTime),
                new SqlParameter("@InnovationSco",serviceData.InnovationSco),
                new SqlParameter("@InReason",serviceData.InReason1),
                new SqlParameter("@RealizableSco",serviceData.RealizableSco),
                new SqlParameter("@ReReson",serviceData.ReReson),
                new SqlParameter("@UserServiceSco",serviceData.UserServiceSco),
                new SqlParameter("@UserSerReson",serviceData.UserSerReson),
                new SqlParameter("@serviceTotalScore",serviceData.ServiceTotalScore),
                new SqlParameter("@supplierName",serviceData.SupplierName),
                new SqlParameter("@InnoRate",serviceData.InnoRate1),
                new SqlParameter("@ReRate",serviceData.ReRate1),
                new SqlParameter("@UserRate",serviceData.UserRate1),
                new SqlParameter("@MtGroupName",serviceData.MtGroupName),

            };
            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }
    }
}
