﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Lib.IDAL.StockIDAL;
using Lib.Common.CommonUtils;
using Lib.Model.StockModel;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public class StockTypeDAL:StockTypeIDAL    
    {
        /// <summary>
        /// 新增一个库存类型
        /// </summary>
        /// <param name="stockTypeModel">库存类型模板</param>
        /// <returns></returns>
        public int insertStockType(StockTypeModel stockTypeModel)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    INSERT INTO Stock_Type(Id,Type,Valid,Create_ts)");
            sqlText.Append("    VALUES(@Id,@Type,@Valid,@Create_ts)");

            //为带@标记的参数填充值
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Id",SqlDbType.Int),
                new SqlParameter("@Type",SqlDbType.VarChar),
                new SqlParameter("@Valid",SqlDbType.Int),
                new SqlParameter("@Create_ts",SqlDbType.DateTime)
            };
            sqlParas[0].Value = stockTypeModel.Id;
            sqlParas[1].Value = stockTypeModel.Type;
            sqlParas[2].Value = stockTypeModel.Valid;
            sqlParas[3].Value = stockTypeModel.Create_ts;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(),sqlParas);
        }

        /// <summary>
        /// 根据类型Id修改库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int updateStockTypeByTypeId(StockTypeModel stockTypeModel)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    UPDATE Stock_Type");
            sqlText.Append("    SET Type=@Type,Update_ts=@Update_ts");
            sqlText.Append("    WHERE Id=@Id");

            //为带@标记的参数填充值
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Type",SqlDbType.VarChar),
                new SqlParameter("@Update_ts",SqlDbType.DateTime),
                new SqlParameter("@Id",SqlDbType.Int)
            };
            sqlParas[0].Value = stockTypeModel.Type;
            sqlParas[1].Value = stockTypeModel.Update_ts;
            sqlParas[2].Value = stockTypeModel.Id;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据类型Id删除库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int deleteStockTypeByTypeId(int typeId)
        {
            //0代表删除了，也就是无效了
            int valid = 0;
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    UPDATE Stock_Type");
            sqlText.Append("    SET Valid=@Valid");
            sqlText.Append("    WHERE Id=@Id");

            //为带@标记的参数填充值
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Valid",SqlDbType.Int),
                new SqlParameter("@Id",SqlDbType.Int)
            };
            sqlParas[0].Value = valid;
            sqlParas[1].Value = typeId;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据类型Id恢复库存类型
        /// </summary>
        /// <param name="typeId">库存类型Id</param>
        /// <returns></returns>
        public int restoreStockTypeByTypeId(int typeId)
        {
            //1代表恢复了，也就是有效了
            int valid = 1;
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    UPDATE Stock_Type");
            sqlText.Append("    SET Valid=@Valid");
            sqlText.Append("    WHERE Id=@Id");

            //为带@标记的参数填充值
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Valid",SqlDbType.Int),
                new SqlParameter("@Id",SqlDbType.Int)
            };
            sqlParas[0].Value = valid;
            sqlParas[1].Value = typeId;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 获得最新获得的TypeId
        /// </summary>
        /// <returns></returns>
        public int getLatestTypeId()
        {
            string sqlText = @"SELECT MAX(Id) id FROM Stock_Type";
            int latestEID = 0;
            DataTable result = DBHelper.ExecuteQueryDT(sqlText);
            int count = result.Rows.Count;
            if (result != null && result.Rows.Count > 0)
            {
                DataRow dr = result.Rows[0];
                if (dr["id"] != null && dr["id"].ToString() != "")
                {
                    latestEID = Convert.ToInt32(dr["id"].ToString());
                }
            }
            return latestEID;
        }

        /// <summary>
        /// 获得所有的库存类型
        /// </summary>
        /// <returns></returns>
        public DataTable getAllStockType()
        {
            string sqlText = @"SELECT Id,Type,Valid FROM Stock_Type ORDER BY Create_ts DESC";

            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);

            return dt;
        }

        /// <summary>
        /// 获得所有有效的库存类型
        /// </summary>
        /// <returns></returns>
        public DataTable getValidStockType()
        {
            //Valid=1：代表该库存类型启用
            //Valid=0：代表该库存类型弃用
            string sqlText = @"SELECT Id,Type,Valid,Create_ts FROM Stock_Type WHERE Valid=1 ORDER BY Create_ts DESC";

            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);

            return dt;
        }

    }
}
