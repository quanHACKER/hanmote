﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class AdjustDifferenceDAL:AdjustDifferenceIDAL
    {
        /// <summary>
        /// 写入差异调整凭证
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool genAdjustDocument(DataTable dt)
        {
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StringBuilder strbld = new StringBuilder("insert into LoopCheckDocument(Document_Date,Post_Date,Material_Note,Material_ID,")
                    .Append("Material_Name,Material_Count,Material_Unit,Factory_ID,Stock_ID,Batch_ID,EvaluatE_Type,Inventory_State,Material_Group,Move_Type,Cancel,")
                        .Append("Inventory_Type) values(@dd,@pd,@mnt,@mid,@mn,@mc,@mu,@fid,@sid,@bid,@et,@is,@mg,@mt,0,@it)");

                    SqlParameter[] sqlpara = new SqlParameter[]{
            
                new SqlParameter("@dd",SqlDbType.Date),
                new SqlParameter("@pd",SqlDbType.Date),
                new SqlParameter("@mnt",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@mn",SqlDbType.VarChar),
                new SqlParameter("@mc",SqlDbType.Float),
                new SqlParameter("@mu",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@mt",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),

            };
                    sqlpara[0].Value = dt.Rows[i][0];
                    sqlpara[1].Value = dt.Rows[i][1];
                    sqlpara[2].Value = dt.Rows[i][2].ToString();
                    sqlpara[3].Value = dt.Rows[i][3].ToString();
                    sqlpara[4].Value = dt.Rows[i][4].ToString();
                    sqlpara[5].Value = Convert.ToDouble(dt.Rows[i][5].ToString());
                    sqlpara[6].Value = dt.Rows[i][6].ToString();
                    sqlpara[7].Value = dt.Rows[i][7].ToString();
                    sqlpara[8].Value = dt.Rows[i][8].ToString();
                    sqlpara[9].Value = dt.Rows[i][9].ToString();
                    sqlpara[10].Value = dt.Rows[i][10].ToString();
                    sqlpara[11].Value = dt.Rows[i][11].ToString();
                    sqlpara[12].Value = dt.Rows[i][12].ToString();
                    sqlpara[13].Value = dt.Rows[i][13].ToString();
                    sqlpara[14].Value = dt.Rows[i][14].ToString();

                    DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);
                }
                return true;
            }
            catch
            {
                return false;
            }
 
        }

        /// <summary>
        /// 由物料凭证编号获取凭证一般信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable ShowDifferenceAdjustcommonDAL(string documentid) 
        {
            StringBuilder strbld = new StringBuilder("select Document_Date,Post_Date,Material_Note from LoopCheckDocument")
                    .Append(" where Document_ID = @did");
            SqlParameter sqlpara = new SqlParameter("@did", SqlDbType.VarChar);
            sqlpara.Value = documentid;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);

            return dt;
       
        }

        /// <summary>
        /// 由凭证编号选择差异调整物料基本信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable ShowDifferenceAdjustDAL(string documentid)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID as 物料编码,")
                    .Append("Material_Name as 物料名,Material_Count as 数量,Material_Unit as 单位,Factory_ID as 工厂,Stock_ID as 库存地,")
            .Append("Batch_ID as 批次,EvaluatE_Type as 物料类型,Inventory_State as 库存状态,Material_Group as 物料组,Move_Type as 移动类型,")
            .Append(" Inventory_Type as 库存类型 from LoopCheckDocument where Document_ID = @did");
            SqlParameter sqlpara = new SqlParameter("@did", SqlDbType.VarChar);
            sqlpara.Value = documentid;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            return dt;

        }

        /// <summary>
        /// 判断凭证是否可冲销
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public bool canceled(string documentid)
        {
            try
            {
                StringBuilder strbld = new StringBuilder("select Cancel from LoopCheckDocument where Document_ID = @did");
                SqlParameter sqlpara = new SqlParameter("@did", SqlDbType.VarChar);
                sqlpara.Value = documentid;
                DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
                if (Convert.ToBoolean(dt.Rows[0][0].ToString()))
                {
                    return false;
                }
                else
                    return true;
            }
            catch {

                return false;
            }
        }

        /// <summary>
        /// 更新cancel字段
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public bool updatecancel(string documentid)
        {
            try
            {
                StringBuilder strbld = new StringBuilder("update LoopCheckDocument set Cancel = 1 where Document_ID =@did");
                SqlParameter sqlpara = new SqlParameter("@did", SqlDbType.VarChar);
                sqlpara.Value = documentid;
                DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 产生取消凭证
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool genCancelDocument(DataTable dt)
        {
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StringBuilder strbld = new StringBuilder("insert into LoopCheckDocument(Document_Date,Post_Date,Material_Note,Material_ID,")
                    .Append("Material_Name,Material_Count,Material_Unit,Factory_ID,Stock_ID,Batch_ID,EvaluatE_Type,Inventory_State,Material_Group,Move_Type,Cancel,Inventory_Type)")
                        .Append(" values(@dd,@pd,@mnt,@mid,@mn,@mc,@mu,@fid,@sid,@bid,@et,@is,@mg,@mt,1,@it)");

                    SqlParameter[] sqlpara = new SqlParameter[]{
            
                new SqlParameter("@dd",SqlDbType.Date),
                new SqlParameter("@pd",SqlDbType.Date),
                new SqlParameter("@mnt",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@mn",SqlDbType.VarChar),
                new SqlParameter("@mc",SqlDbType.VarChar),
                new SqlParameter("@mu",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@mt",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),

            };
                    sqlpara[0].Value = dt.Rows[i][0];
                    sqlpara[1].Value = dt.Rows[i][1];
                    sqlpara[2].Value = dt.Rows[i][2];
                    sqlpara[3].Value = dt.Rows[i][3];
                    sqlpara[4].Value = dt.Rows[i][4];
                    sqlpara[5].Value = dt.Rows[i][5];
                    sqlpara[6].Value = dt.Rows[i][6];
                    sqlpara[7].Value = dt.Rows[i][7];
                    sqlpara[8].Value = dt.Rows[i][8];
                    sqlpara[9].Value = dt.Rows[i][9];
                    sqlpara[10].Value = dt.Rows[i][10];
                    sqlpara[11].Value = dt.Rows[i][11];
                    sqlpara[12].Value = dt.Rows[i][12];
                    sqlpara[13].Value = dt.Rows[i][13];
                    sqlpara[14].Value = dt.Rows[i][14];

                    DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 更新Inventory表数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool updateInventory(DataTable dt) 
        {
            try
            {
                int n = dt.Rows.Count;
                for (int i = 0; i < n; i++)
                {

                    StringBuilder strbld = new StringBuilder("update Inventory set Count = Count+@c where Material_ID = @mid and Factory_ID =@fid and ")
                    .Append("Stock_ID=@sid  and Batch_ID=@bid and Inventory_State=@is and Evaluate_Type=@et")
                    .Append(" and Material_Group=@mg and Inventory_Type = @it");
                    SqlParameter[] sqlpara = new SqlParameter[] { 
            
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),

            };

                    sqlpara[0].Value = dt.Rows[i][3].ToString();
                    sqlpara[1].Value = dt.Rows[i][7].ToString();
                    sqlpara[2].Value = dt.Rows[i][8].ToString();
                    sqlpara[3].Value = dt.Rows[i][9].ToString();
                    sqlpara[4].Value = dt.Rows[i][11].ToString();
                    sqlpara[5].Value = dt.Rows[i][10].ToString();
                    sqlpara[6].Value = dt.Rows[i][12].ToString();
                    sqlpara[7].Value = dt.Rows[i][14].ToString();
                    if (dt.Rows[i][13].ToString() == "933" || dt.Rows[i][13].ToString() == "936")
                    {
                        sqlpara[8].Value = Convert.ToDouble(dt.Rows[i][5].ToString());
                    }
                    else
                    {
                        sqlpara[8].Value = 0 - Convert.ToDouble(dt.Rows[i][5].ToString());
                    }

                    DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);
                }
                return true;
            }
            catch
            {
                return false;
            }
        
        }
    }
}
