﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public class DocumentDetailInfoDAL:DocumentDetailInfoIDAL
    {
        /// <summary>
        /// 插入凭证详细信息
        /// </summary>
        /// <param name="documentDetailInfoModel"></param>
        /// <returns></returns>
        public int insertDocumentDetailInfo(List<DocumentDetailInfoModel> documentDetailInfoModelList)
        {
            List<String> sqlTextList = new List<string>();
             //声明一个List集合
            List<SqlParameter[]> sqlParamList = new List<SqlParameter[]>();

            if (documentDetailInfoModelList.Count > 0)
            {
                foreach (DocumentDetailInfoModel ddim in documentDetailInfoModelList)
                {
                    StringBuilder sqlText = new StringBuilder("INSERT INTO DocumentDetails(StockInDocument_ID,SMaterial_ID,Order_ID,Delivery_ID,ReceiptNote_ID,SFactory_ID,SStock_ID,Supplier_ID,Inventory_Type,UnitPrice,Receive_Count,Order_Count,Input_Count,SKU_Count,Notes_Count,Batch_ID,SupplierMaterial_ID,Material_Group,Evaluate_Type,Tolerance,Loading_Place,Receiving_Place,TMaterial_ID,TFactory_ID,TStock_ID,TMaterial_Name,TFactory_Name,TStock_Name,SpecialStock,ExplainText,Unit)");
                    sqlText.Append(" VALUES(@StockInDocument_ID,@SMaterial_ID,@Order_ID,@Delivery_ID,@ReceiptNote_ID,@SFactory_ID,@SStock_ID,@Supplier_ID,@Inventory_Type,@UnitPrice,@Receive_Count,@Order_Count,@Input_Count,@SKU_Count,@Notes_Count,@Batch_ID,@SupplierMaterial_ID,@Material_Group,@Evaluate_Type,@Tolerance,@Loading_Place,@Receiving_Place,@TMaterial_ID,@TFactory_ID,@TStock_ID,@TMaterial_Name,@TFactory_Name,@TStock_Name,@SpecialStock,@ExplainText,@Unit)");
                    SqlParameter[] sqlParams = new SqlParameter[] { 
                        new SqlParameter("@StockInDocument_ID",SqlDbType.VarChar),
                        new SqlParameter("@SMaterial_ID",SqlDbType.VarChar),
                        new SqlParameter("@Order_ID",SqlDbType.VarChar),
                        new SqlParameter("@Delivery_ID",SqlDbType.VarChar),
                        new SqlParameter("@ReceiptNote_ID",SqlDbType.VarChar),
                        new SqlParameter("@SFactory_ID",SqlDbType.VarChar),
                        new SqlParameter("@SStock_ID",SqlDbType.VarChar),
                        new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                        new SqlParameter("@Inventory_Type",SqlDbType.VarChar),
                        new SqlParameter("@UnitPrice",SqlDbType.Float),
                        new SqlParameter("@Receive_Count",SqlDbType.VarChar),
                        new SqlParameter("@Order_Count",SqlDbType.VarChar),
                        new SqlParameter("@Input_Count",SqlDbType.VarChar),
                        new SqlParameter("@SKU_Count",SqlDbType.VarChar),
                        new SqlParameter("@Notes_Count",SqlDbType.VarChar),
                        new SqlParameter("@Batch_ID",SqlDbType.VarChar),
                        new SqlParameter("@SupplierMaterial_ID",SqlDbType.VarChar),
                        new SqlParameter("@Material_Group",SqlDbType.VarChar),
                        new SqlParameter("@Evaluate_Type",SqlDbType.VarChar),
                        new SqlParameter("@Tolerance",SqlDbType.VarChar),
                        new SqlParameter("@Loading_Place",SqlDbType.VarChar),
                        new SqlParameter("@Receiving_Place",SqlDbType.VarChar),
                        new SqlParameter("@TMaterial_ID",SqlDbType.VarChar),
                        new SqlParameter("@TFactory_ID",SqlDbType.VarChar),
                        new SqlParameter("@TStock_ID",SqlDbType.VarChar),
                        new SqlParameter("@TMaterial_Name",SqlDbType.VarChar),
                        new SqlParameter("@TFactory_Name",SqlDbType.VarChar),
                        new SqlParameter("@TStock_Name",SqlDbType.VarChar),
                        new SqlParameter("@SpecialStock",SqlDbType.VarChar),
                        new SqlParameter("@ExplainText",SqlDbType.VarChar),
                        new SqlParameter("@Unit",SqlDbType.VarChar),
                    };

                    sqlParams[0].Value = ddim.StockInDocument_ID;
                    sqlParams[1].Value = ddim.SMaterial_ID;
                    sqlParams[2].Value = ddim.Order_ID;
                    sqlParams[3].Value = ddim.Delivery_ID;
                    sqlParams[4].Value = ddim.ReceiptNote_ID;
                    sqlParams[5].Value = ddim.SFactory_ID;
                    sqlParams[6].Value = ddim.SStock_ID;
                    sqlParams[7].Value = ddim.Supplier_ID;
                    sqlParams[8].Value = ddim.Inventory_Type;
                    sqlParams[9].Value = ddim.UnitPrice;
                    sqlParams[10].Value = ddim.Receive_Count;
                    sqlParams[11].Value = ddim.Order_Count;
                    sqlParams[12].Value = ddim.Input_Count;
                    sqlParams[13].Value = ddim.SKU_Count;
                    sqlParams[14].Value = ddim.Notes_Count;
                    sqlParams[15].Value = ddim.Batch_ID;
                    sqlParams[16].Value = ddim.SupplierMaterial_ID;
                    sqlParams[17].Value = ddim.Material_Group;
                    sqlParams[18].Value = ddim.Evaluate_Type;
                    sqlParams[19].Value = ddim.Tolerance;
                    sqlParams[20].Value = ddim.Loading_Place;
                    sqlParams[21].Value = ddim.Receiving_Place;
                    sqlParams[22].Value = ddim.TMaterial_ID;
                    sqlParams[23].Value = ddim.TFactory_ID;
                    sqlParams[24].Value = ddim.TStock_ID;
                    sqlParams[25].Value = ddim.TMaterial_Name;
                    sqlParams[26].Value = ddim.TFactory_Name;
                    sqlParams[27].Value = ddim.TStock_Name;
                    sqlParams[28].Value = ddim.SpecialStock;
                    sqlParams[29].Value = ddim.ExplainText;
                    sqlParams[30].Value = ddim.Unit;

                    //加入sqlTextList集合中
                    sqlTextList.Add(sqlText.ToString());
                    sqlParamList.Add(sqlParams);
                }
            }
            return DBHelper.ExecuteNonQuery(sqlTextList, sqlParamList);
        }

        /// <summary>
        /// 根据凭证编号查找凭证详细清单
        /// </summary>
        /// <param name="mDocId">凭证编号</param>
        /// <returns></returns>
        public DataTable getMDocItemByDocId(string mDocId)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("SELECT StockInDocument_ID,SMaterial_ID,Order_ID,Delivery_ID,ReceiptNote_ID,SFactory_ID,SStock_ID,Supplier_ID,Inventory_Type,Receive_Count,Order_Count,Input_Count,SKU_Count,Notes_Count,Batch_ID,SupplierMaterial_ID,Material_Group,Evaluate_Type,Tolerance,Loading_Place,Receiving_Place,TMaterial_ID,TFactory_ID,TStock_ID,TMaterial_Name,TFactory_Name,TStock_Name,SpecialStock,ExplainText,StorehouseUnit,Storehouse_ID,UnitPrice,Unit");
            sqlText.Append("    FROM DocumentDetails");
            sqlText.Append("    WHERE StockInDocument_ID=@mDocId ");
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@mDocId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = mDocId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);    //返回DataTable
        }
    }
}
