﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL
{
    public class StockInfoDAL:StockInfoIDAL
    {
        /// <summary>
        /// 根据工厂ID和库存id查询仓库信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId,string stockOrName)
        {
            List<StockInfoExtendModel> StockInfoList = new List<StockInfoExtendModel>();
            StringBuilder sqlText = new StringBuilder("SELECT ID,Stock_ID,Stock_Name,Address,Factory_ID FROM Stock");
            sqlText.Append(" WHERE Factory_ID=@Factory_ID ");
            if (stockOrName != null)
            {
                sqlText.Append(" AND (Stock_ID='");
                sqlText.Append(stockOrName);
                sqlText.Append("' OR Stock_Name='");
                sqlText.Append(stockOrName);
                sqlText.Append("') ");
            }
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Factory_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = factoryId;

            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        StockInfoExtendModel stockInfoExtendModel = new StockInfoExtendModel();
                        stockInfoExtendModel.ID = dr["ID"].ToString();
                        stockInfoExtendModel.Stock_ID = dr["Stock_ID"].ToString();
                        stockInfoExtendModel.Stock_Name = dr["Stock_Name"].ToString();
                        stockInfoExtendModel.Address = dr["Address"].ToString();
                        stockInfoExtendModel.Factory_ID = dr["Factory_ID"].ToString();
                        StockInfoList.Add(stockInfoExtendModel);
                    }
                    return StockInfoList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据工厂id来查找
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public List<StockInfoExtendModel> findStockInfoByFactoryId(string factoryId)
        {
            List<StockInfoExtendModel> StockInfoList = new List<StockInfoExtendModel>();
            StringBuilder sqlText = new StringBuilder("SELECT ID,Stock_ID,Stock_Name,Address,Factory_ID FROM Stock");
            sqlText.Append(" WHERE Factory_ID=@Factory_ID ");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Factory_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = factoryId;

            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        StockInfoExtendModel stockInfoExtendModel = new StockInfoExtendModel();
                        stockInfoExtendModel.ID = dr["ID"].ToString();
                        stockInfoExtendModel.Stock_ID = dr["Stock_ID"].ToString();
                        stockInfoExtendModel.Stock_Name = dr["Stock_Name"].ToString();
                        stockInfoExtendModel.Address = dr["Address"].ToString();
                        stockInfoExtendModel.Factory_ID = dr["Factory_ID"].ToString();
                        StockInfoList.Add(stockInfoExtendModel);
                    }
                    return StockInfoList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据库存ID来查找相应的库存位置信息
        /// </summary>
        /// <param name="stockId"></param>
        /// <returns></returns>
        public DataTable getStockInfoByStockId(string stockId)
        {
            StringBuilder sqlText = new StringBuilder("SELECT ID,Stock_ID,Stock_Name,Address,Factory_ID ");
            sqlText.Append("    FROM Stock");
            sqlText.Append("    WHERE Stock_ID=@stockId");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@stockId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = stockId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }
    }
}
