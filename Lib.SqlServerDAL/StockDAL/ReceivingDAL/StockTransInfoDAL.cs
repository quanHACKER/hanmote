﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;
using Lib.IDAL.StockIDAL;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.StockDAL
{
    class StockTransInfoDAL:StockTransInfoIDAL
    {
        /// <summary>
        /// 查找所有事务/事件信息
        /// </summary>
        /// <returns></returns>
        public List<StockTransExtendModel> findAllStockTransInfo()
        {
            List<StockTransExtendModel> stockTransExtendModelList = new List<StockTransExtendModel>();
            //编写SQL语句
            StringBuilder sqlText = new StringBuilder("SELECT StockTrans_ID,StockTrans_Name,StockTrans_Activate ");
            sqlText.Append(" FROM StockTrans");
            //执行查询
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        StockTransExtendModel stockTransExtendModel = new StockTransExtendModel();
                        stockTransExtendModel.StockTrans_ID = dr["StockTrans_ID"].ToString();
                        stockTransExtendModel.StockTrans_Name = dr["StockTrans_Name"].ToString();
                        stockTransExtendModel.StockTrans_Activate = Convert.ToBoolean(dr["StockTrans_Activate"].ToString());
                        stockTransExtendModelList.Add(stockTransExtendModel);
                    }
                    return stockTransExtendModelList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据id查找事务/事件信息
        /// </summary>
        /// <param name="stockTransId">事务id</param>
        /// <returns></returns>
        public StockTransExtendModel findStockTransInfoByID(string stockTransId)
        {
            StockTransExtendModel stockTransExtendModel = new StockTransExtendModel();
            StringBuilder sqlText = new StringBuilder("SELECT StockTrans_ID,StockTrans_Name,StockTrans_Activate ");
            sqlText.Append(" FROM StockTrans ");
            sqlText.Append(" WHERE StockTrans_ID=@StockTrans_ID");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@StockTrans_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = stockTransId;
            //查询移动类型信息
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    stockTransExtendModel.StockTrans_ID = dr["StockTrans_ID"].ToString();
                    stockTransExtendModel.StockTrans_Name = dr["StockTrans_Name"].ToString();
                    //stockTransExtendModel.StockTrans_Activate = dr["MT_Reason"].ToString();
                    stockTransExtendModel.StockTrans_Activate = Convert.ToBoolean(dr["StockTrans_Activate"].ToString());
                    return stockTransExtendModel;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 插入新的事务/事件信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        public int insertNewStockTransInfo(StockTransModel stockTransModel)
        {
            StringBuilder sqlText = new StringBuilder("INSERT INTO StockTrans(StockTrans_ID,StockTrans_Name,StockTrans_Activate) ");
            sqlText.Append("VALUES(@StockTrans_ID,@StockTrans_Name,@StockTrans_Activate)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@StockTrans_ID",SqlDbType.VarChar),
                new SqlParameter("@StockTrans_Name",SqlDbType.VarChar),
                new SqlParameter("@StockTrans_Activate",SqlDbType.Bit),
            };
            sqlParas[0].Value = stockTransModel.StockTrans_ID;
            sqlParas[1].Value = stockTransModel.StockTrans_Name;
            sqlParas[2].Value = stockTransModel.StockTrans_Activate;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 修改事务信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        public int updateStockTransInfo(StockTransModel stockTransModel)
        {
            //编写SQL语句
            StringBuilder sqlText = new StringBuilder("UPDATE StockTrans ");
            sqlText.Append(" SET StockTrans_Name=@StockTrans_Name, StockTrans_Activate=@StockTrans_Activate ");
            sqlText.Append(" WHERE StockTrans_ID=@StockTrans_ID");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@StockTrans_Name",SqlDbType.VarChar),
                new SqlParameter("@StockTrans_Activate",SqlDbType.Bit),
                new SqlParameter("@StockTrans_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = stockTransModel.StockTrans_Name;
            sqlParas[1].Value = stockTransModel.StockTrans_Activate;
            sqlParas[2].Value = stockTransModel.StockTrans_ID;

            //返回影响的行数
            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据id删除事务信息
        /// </summary>
        /// <param name="stockTransModel">事务id</param>
        /// <returns></returns>
        public int deleteStockTransInfo(string stockTransId)
        {
            //编写sql语句
            StringBuilder sqlText = new StringBuilder("DELETE FROM StockTrans ");
            sqlText.Append("WHERE StockTrans_ID=@StockTrans_ID");
            SqlParameter[] sqlSparas = new SqlParameter[] { 
                new SqlParameter("@StockTrans_ID", SqlDbType.VarChar)
            };
            sqlSparas[0].Value = stockTransId;

            //返回影响的行数
            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlSparas);
        }
    }
}
