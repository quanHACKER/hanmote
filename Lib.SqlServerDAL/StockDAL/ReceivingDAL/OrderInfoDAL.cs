﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class OrderInfoDAL:OrderInfoIDAL
    {
        /// <summary>
        /// 根据采购订单编号查找订单信息
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        public DataTable getOrderinfoByPOId(string poId)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("SELECT a.Order_ID,a.Order_Type,b.Supplier_ID,b.Supplier_Name,b.Address,b.Zip_Code");
            sqlText.Append("    FROM Order_Info a INNER JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID");
            sqlText.Append("    WHERE a.Order_ID=@orderId ");
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@orderId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = poId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(),sqlParas);    //返回DataTable
        }

        /// <summary>
        /// 根据采购订单编号查找订单物料清单
        /// </summary>
        /// <param name="poId">采购订单编号</param>
        /// <returns></returns>
        public DataTable getOrderItemByPOId(string poId)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("SELECT a.Order_ID,a.Mterial_ID,b.Material_Name,a.Factory_ID,a.Stock_ID,a.Material_Group,a.net_Price,a.Number,a.Unit,a.Total_price,a.Batch_ID,a.PR_ID,a.Delivery_Time,a.Info_Type,a.Info_Number,a.Purchase_ID");
            sqlText.Append("    FROM Order_Item a INNER JOIN Material b ON a.Mterial_ID=b.Material_ID");
            sqlText.Append("    WHERE a.Order_ID=@orderId ");
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@orderId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = poId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);    //返回DataTable
        }
    }
}
