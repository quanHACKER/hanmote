﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class StockSupplierInfoDAL : StockSupplierInfoIDAL
    {
        /// <summary>
        /// 根据订单ID来查找与该订单号关联的供应商详细信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public OrderSupReceivingModel findOrderSupInfoById(string orderId)
        {
            OrderSupReceivingModel orderSupReceivingModel = new OrderSupReceivingModel();
            //编写sql语句
            StringBuilder sqlText = new StringBuilder("SELECT b.Supplier_Name, b.Address,b.Supplier_ID, b.Company_ZipCode ");
            sqlText.Append("FROM [Order] a JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID ");
            sqlText.Append(" WHERE a.Order_ID = @orderId");
            //设置查询条件参数
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@orderId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = orderId;

            //执行查询语句返回一个DataTable(表)
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    orderSupReceivingModel.Supplier_name = dr["Supplier_name"].ToString();
                    orderSupReceivingModel.Address = dr["Address"].ToString();
                    orderSupReceivingModel.Supplier_ID = dr["Supplier_ID"].ToString();
                    orderSupReceivingModel.Company_ZipCode = dr["Company_ZipCode"].ToString();
                    return orderSupReceivingModel;
                }
                return null;
            }
            return null; 
        }


    }
}
