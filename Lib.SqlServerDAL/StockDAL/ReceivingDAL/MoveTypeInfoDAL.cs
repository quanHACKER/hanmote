﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class MoveTypeInfoDAL:MoveTypeInfoIDAL
    {
        /// <summary>
        /// 查找所有移动类型信息
        /// </summary>
        /// <returns></returns>
        public List<MoveTypeExtendModel> findALLMoveTypeInfo()
        {
            List<MoveTypeExtendModel> moveTypeExtendModelList = new List<MoveTypeExtendModel>();
            //编写SQL语句
            StringBuilder sqlText = new StringBuilder("SELECT MT_ID,MT_Name,MT_Reason,MT_Valid,MT_TransactionDtb FROM Move_Type");
            //执行查询
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MoveTypeExtendModel moveTypeExtendModel = new MoveTypeExtendModel();
                        moveTypeExtendModel.MT_ID = dr["MT_ID"].ToString();
                        moveTypeExtendModel.MT_Name = dr["MT_Name"].ToString();
                        moveTypeExtendModel.MT_Reason = dr["MT_Reason"].ToString();
                        moveTypeExtendModel.MT_Valid = Convert.ToBoolean(dr["MT_Valid"].ToString());
                        moveTypeExtendModel.MT_TransactionDtb = dr["MT_TransactionDtb"].ToString();
                        moveTypeExtendModelList.Add(moveTypeExtendModel);
                    }
                    return moveTypeExtendModelList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据移动类型ID类查找移动类型信息
        /// </summary>
        /// <param name="moveTypeId">移动类型ID</param>
        /// <returns></returns>
        public MoveTypeExtendModel findMoveTypeInfoByID(string moveTypeId)
        {
            MoveTypeExtendModel moveTypeExtendModel = new MoveTypeExtendModel();
            StringBuilder sqlText = new StringBuilder("SELECT MT_ID,MT_Name,MT_Reason,MT_TransactionDtb,MT_Valid ");
            sqlText.Append(" FROM Move_Type ");
            sqlText.Append(" WHERE MT_ID=@moveTypeId");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@moveTypeId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = moveTypeId;
            //查询移动类型信息
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    moveTypeExtendModel.MT_ID = dr["MT_ID"].ToString();
                    moveTypeExtendModel.MT_Name = dr["MT_Name"].ToString();
                    moveTypeExtendModel.MT_Reason = dr["MT_Reason"].ToString();
                    moveTypeExtendModel.MT_Valid = Convert.ToBoolean(dr["MT_Valid"].ToString());
                    moveTypeExtendModel.MT_TransactionDtb = dr["MT_TransactionDtb"].ToString();
                    return moveTypeExtendModel;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 插入新的移动类型
        /// </summary>
        /// <param name="moveTypeModel"></param>
        /// <returns></returns>
        public int insertNewMoveTypeInfo(MoveTypeModel moveTypeModel)
        {
            StringBuilder sqlText = new StringBuilder("INSERT INTO Move_Type(MT_ID,MT_Name,MT_Reason,MT_Valid,MT_TransactionDtb) ");
            sqlText.Append("VALUES(@MT_ID,@MT_Name,@MT_Reason,@MT_Valid,@MT_TransactionDtb)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@MT_ID",SqlDbType.VarChar),
                new SqlParameter("@MT_Name",SqlDbType.VarChar),
                new SqlParameter("@MT_Reason",SqlDbType.VarChar),
                new SqlParameter("@MT_Valid",SqlDbType.VarChar),
                new SqlParameter("@MT_TransactionDtb",SqlDbType.VarChar)
            };
            sqlParas[0].Value = moveTypeModel.MT_ID;
            sqlParas[1].Value = moveTypeModel.MT_Name;
            sqlParas[2].Value = moveTypeModel.MT_Reason;
            sqlParas[3].Value = moveTypeModel.MT_Valid;
            sqlParas[4].Value = moveTypeModel.MT_TransactionDtb;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据移动类型编码来修改数据
        /// </summary>
        /// <param name="moveTypeModel">移动类型类</param>
        /// <returns></returns>
        public int updateMoveTypeInfo(MoveTypeModel moveTypeModel)
        {
            //编写SQL语句
            StringBuilder sqlText = new StringBuilder("UPDATE Move_Type ");
            sqlText.Append(" SET MT_Name=@MT_Name, MT_Reason=@MT_Reason,MT_TransactionDtb=@MT_TransactionDtb ");
            sqlText.Append(" WHERE MT_ID=@MT_ID");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@MT_Name",SqlDbType.VarChar),
                new SqlParameter("@MT_Reason",SqlDbType.VarChar),
                new SqlParameter("@MT_TransactionDtb",SqlDbType.VarChar),
                new SqlParameter("@MT_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = moveTypeModel.MT_Name;
            sqlParas[1].Value = moveTypeModel.MT_Reason;
            sqlParas[2].Value = moveTypeModel.MT_TransactionDtb;
            sqlParas[3].Value = moveTypeModel.MT_ID;

            //返回影响的行数
            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据移动类型编码来删除数据
        /// </summary>
        /// <param name="moveTypeId"></param>
        /// <returns></returns>
        public int deleteMoveTypeInfoById(string moveTypeId)
        {
            //编写sql语句
            StringBuilder sqlText = new StringBuilder("DELETE FROM Move_Type ");
            sqlText.Append("WHERE MT_ID=@MT_ID");
            SqlParameter[] sqlSparas = new SqlParameter[] { 
                new SqlParameter("@MT_ID", SqlDbType.VarChar)
            };
            sqlSparas[0].Value = moveTypeId;

            //返回影响的行数
            return DBHelper.ExecuteNonQuery(sqlText.ToString(),sqlSparas);
        }

        /// <summary>
        /// 通过事务/事件名称来关联查询移动类型信息
        /// </summary>
        /// <param name="transName"></param>
        /// <returns></returns>
        public List<MoveTypeExtendModel> findALLMoveTypeInfoByTransName(string transName)
        {
            List<MoveTypeExtendModel> moveTypeExtendModelList = new List<MoveTypeExtendModel>();
            //编写SQL语句
            StringBuilder sqlText = new StringBuilder("SELECT a.MT_ID,a.MT_Name,a.MT_Reason,a.MT_TransactionDtb,a.MT_Valid ");
            sqlText.Append(" FROM Move_Type a JOIN StockTrans b ON a.MT_TransactionDtb=b.StockTrans_ID ");
            sqlText.Append(" WHERE b.StockTrans_Name=@StockTrans_Name");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@StockTrans_Name",SqlDbType.VarChar)
            };
            sqlParas[0].Value = transName;
            //执行查询
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(),sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MoveTypeExtendModel moveTypeExtendModel = new MoveTypeExtendModel();
                        moveTypeExtendModel.MT_ID = dr["MT_ID"].ToString();
                        moveTypeExtendModel.MT_Name = dr["MT_Name"].ToString();
                        moveTypeExtendModel.MT_Reason = dr["MT_Reason"].ToString();
                        moveTypeExtendModel.MT_Valid = Convert.ToBoolean(dr["MT_Valid"].ToString());
                        moveTypeExtendModel.MT_TransactionDtb = dr["MT_TransactionDtb"].ToString();
                        moveTypeExtendModelList.Add(moveTypeExtendModel);
                    }
                    return moveTypeExtendModelList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
