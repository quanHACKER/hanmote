﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public  class StockMaterialInfoDAL : StockMaterialInfoIDAL
    {
        /// <summary>
        /// 根据订单id来查找与该订单关联的物料信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OrderMatReceivingModel findOrderMatInfoById(string orderId)
        {
            OrderMatReceivingModel orderMatReceivingModel = new OrderMatReceivingModel();
            StringBuilder sqlText = new StringBuilder("SELECT a.Material_ID, b.Material_Name, a.Material_Count,b.Material_Group, b.Measurement  ");
            sqlText.Append("FROM [Order] a JOIN Material b ON a.Material_ID=b.Material_ID");
            sqlText.Append(" WHERE a.Order_ID = @orderId");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@orderId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = orderId;

            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    orderMatReceivingModel.Material_ID = dr["Material_ID"].ToString();
                    orderMatReceivingModel.Material_Name = dr["Material_Name"].ToString();
                    orderMatReceivingModel.Material_Group = dr["Material_Group"].ToString();
                    orderMatReceivingModel.Material_Count = Convert.ToInt32(dr["Material_Count"].ToString());
                    orderMatReceivingModel.Measurement = dr["Measurement"].ToString();
                    return orderMatReceivingModel;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }




    }
}
