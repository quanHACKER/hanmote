﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public class MaterialDocumentNewDAL:MaterialDocumentNewIDAL
    {
        /// <summary>
        /// 向数据库中插入物料凭证
        /// </summary>
        /// <param name="materialDocumentNewModel"></param>
        /// <returns></returns>
        public int insertMaterialNewDocument(MaterialDocumentNewModel materialDocumentNewModel)
        {
            StringBuilder sqlText = new StringBuilder("INSERT INTO MaterialDocument(StockInDocument_ID,Posting_Date,Document_Date,StockManager,Move_Type,Reversed,Order_ID,Delivery_ID,ReceiptNote_ID,Total_Number,Total_Value) ");
            sqlText.Append("VALUES(@StockInDocument_ID,@Posting_Date,@Document_Date,@StockManager,@Document_Type,@Reversred,@Order_ID,@Delivery_ID,@ReceiptNote_ID,@Total_Number,@Total_Value)");
            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@StockInDocument_ID",SqlDbType.VarChar),
                new SqlParameter("@Posting_Date",SqlDbType.DateTime),
                new SqlParameter("@Document_Date",SqlDbType.DateTime),
                new SqlParameter("@StockManager",SqlDbType.VarChar),
                new SqlParameter("@Document_Type",SqlDbType.VarChar),
                new SqlParameter("@Reversred",SqlDbType.Bit),
                new SqlParameter("@Order_ID",SqlDbType.VarChar),
                new SqlParameter("@Delivery_ID",SqlDbType.VarChar),
                new SqlParameter("@ReceiptNote_ID",SqlDbType.VarChar),
                new SqlParameter("@Total_Number",SqlDbType.Int),
                new SqlParameter("@Total_Value",SqlDbType.Float)
            };

            sqlParams[0].Value = materialDocumentNewModel.StockInDocument_ID;
            sqlParams[1].Value = materialDocumentNewModel.Posting_Date;
            sqlParams[2].Value = materialDocumentNewModel.Document_Date;
            sqlParams[3].Value = materialDocumentNewModel.StockManager;
            sqlParams[4].Value = materialDocumentNewModel.Document_Type;
            sqlParams[5].Value = materialDocumentNewModel.Reversred;
            sqlParams[6].Value = materialDocumentNewModel.Order_ID;
            sqlParams[7].Value = materialDocumentNewModel.Delivery_ID;
            sqlParams[8].Value = materialDocumentNewModel.ReceiptNote_ID;
            sqlParams[9].Value = materialDocumentNewModel.Total_Number;
            sqlParams[10].Value = materialDocumentNewModel.Total_Value;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(),sqlParams);
        }

        /// <summary>
        /// 获得凭证头部信息
        /// </summary>
        /// <param name="mDoc"></param>
        /// <returns></returns>
        public DataTable getMaterialNewDocument(string mDocId)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("SELECT StockInDocument_ID,Posting_Date,Document_Date,StockManager,Move_Type,Reversed,Order_ID,Delivery_ID,ReceiptNote_ID,Total_Number,Total_Value");
            sqlText.Append("    FROM MaterialDocument");
            sqlText.Append("    WHERE StockInDocument_ID=@stockInDocument ");
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@stockInDocument",SqlDbType.VarChar)
            };
            sqlParas[0].Value = mDocId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);    //返回DataTable
        }
    }
}
