﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;

namespace Lib.SqlServerDAL
{
    public class MaterialDocumentDAL:MaterialDocumentIDAL
    {
        /// <summary>
        /// 向数据库中插入物料凭证
        /// </summary>
        /// <param name="materialDocument"></param>
        /// <returns></returns>
        public int insertMaterialDocument(MaterialDocumentModel materialDocument)
        {
            StringBuilder sqlText = new StringBuilder("INSERT INTO StockInDocument (StockInDocument_ID,Material_ID,Document_Type,Order_ID,Posting_Date,Document_Date,DeliveryNote_ID,ReceiptNote_ID,Factory_ID,Stock_Place,Supplier_ID,StockManager,Inventory_Type,Receive_count,CountInOrder,InputCount,SKUCount,Batch_ID,SupplierMaterial_ID,Material_Group,Evaluate_Type,Tolerance,Loading_Place,Receiving_Place,NotesNumber,ExplainText,ToMaterial_ID,ToMaterial_Name,ToFactory_ID,ToFactory_Name,ToStock_ID,ToStock_Name,SpecialStock)");
            sqlText.Append("VALUES (@StockInDocument_ID,@Material_ID,@Document_Type,@Order_ID,@Posting_Date,@Document_Date,@DeliveryNote_ID,@ReceiptNote_ID,@Factory_ID,@Stock_Place,@Supplier_ID,@StockManager,@Inventory_Type,@Receive_count,@CountInOrder,@InputCount,@SKUCount,@Batch_ID,@SupplierMaterial_ID,@Material_Group,@Evaluate_Type,@Tolerance,@Loading_Place,@Receiving_Place,@NotesNumber,@ExplainText,@ToMaterial_ID,@ToMaterial_Name,@ToFactory_ID,@ToFactory_Name,@ToStock_ID,@ToStock_Name,@SpecialStock)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@StockInDocument_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Document_Type",SqlDbType.VarChar),
                new SqlParameter("@Order_ID",SqlDbType.VarChar),
                new SqlParameter("@Posting_Date",SqlDbType.DateTime),
                new SqlParameter("@Document_Date",SqlDbType.DateTime),
                new SqlParameter("@DeliveryNote_ID",SqlDbType.VarChar),
                new SqlParameter("@ReceiptNote_ID",SqlDbType.VarChar),
                new SqlParameter("@Factory_ID",SqlDbType.VarChar),
                new SqlParameter("@Stock_Place",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@StockManager",SqlDbType.VarChar),
                new SqlParameter("@Inventory_Type",SqlDbType.VarChar),
                new SqlParameter("@Receive_count",SqlDbType.Int),
                new SqlParameter("@CountInOrder",SqlDbType.Int),
                new SqlParameter("@InputCount",SqlDbType.Int),
                new SqlParameter("@SKUCount",SqlDbType.Int),
                new SqlParameter("@Batch_ID",SqlDbType.VarChar),
                new SqlParameter("@SupplierMaterial_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_Group",SqlDbType.VarChar),
                new SqlParameter("@Evaluate_Type",SqlDbType.VarChar),
                new SqlParameter("@Tolerance",SqlDbType.Float),
                new SqlParameter("@Loading_Place",SqlDbType.VarChar),
                new SqlParameter("@Receiving_Place",SqlDbType.VarChar),
                new SqlParameter("@NotesNumber",SqlDbType.Int),
                new SqlParameter("@ExplainText",SqlDbType.VarChar),
                new SqlParameter("@ToMaterial_ID",SqlDbType.VarChar),
                new SqlParameter("@ToMaterial_Name",SqlDbType.VarChar),
                new SqlParameter("@ToFactory_ID",SqlDbType.VarChar),
                new SqlParameter("@ToFactory_Name",SqlDbType.VarChar),
                new SqlParameter("@ToStock_ID",SqlDbType.VarChar),
                new SqlParameter("@ToStock_Name",SqlDbType.VarChar),
                new SqlParameter("@SpecialStock",SqlDbType.VarChar)
            };

            sqlParas[0].Value = materialDocument.StockInDocument_ID;
            sqlParas[1].Value = materialDocument.Material_ID;
            sqlParas[2].Value = materialDocument.Document_Type;
            sqlParas[3].Value = materialDocument.Order_ID;
            sqlParas[4].Value = materialDocument.Posting_Date;
            sqlParas[5].Value = materialDocument.Document_Date;
            sqlParas[6].Value = materialDocument.DeliveryNote_ID;
            sqlParas[7].Value = materialDocument.ReceiptNote_ID;
            sqlParas[8].Value = materialDocument.Factory_ID;
            sqlParas[9].Value = materialDocument.Stock_Place;
            sqlParas[10].Value = materialDocument.Supplier_ID;
            sqlParas[11].Value = materialDocument.StockManager;
            sqlParas[12].Value = materialDocument.Inventory_Type;
            sqlParas[13].Value = materialDocument.Receive_count;
            sqlParas[14].Value = materialDocument.CountInOrder;
            sqlParas[15].Value = materialDocument.InputCount;
            sqlParas[16].Value = materialDocument.SKUCount;
            sqlParas[17].Value = materialDocument.Batch_ID;
            sqlParas[18].Value = materialDocument.SupplierMaterial_ID;
            sqlParas[19].Value = materialDocument.Material_Group;
            sqlParas[20].Value = materialDocument.Evaluate_Type;
            sqlParas[21].Value = materialDocument.Tolerance;
            sqlParas[22].Value = materialDocument.Loading_Place;
            sqlParas[23].Value = materialDocument.Receiving_Place;
            sqlParas[24].Value = materialDocument.NotesNumber;
            sqlParas[25].Value = materialDocument.ExplainText;
            sqlParas[26].Value = materialDocument.ToMaterial_ID;
            sqlParas[27].Value = materialDocument.ToMaterial_Name;
            sqlParas[28].Value = materialDocument.ToFactory_ID;
            sqlParas[29].Value = materialDocument.ToFactory_Name;
            sqlParas[30].Value = materialDocument.ToStock_ID;
            sqlParas[31].Value = materialDocument.ToStock_Name;
            sqlParas[32].Value = materialDocument.SpecialStock;

            return DBHelper.ExecuteNonQuery(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 查找每次刚插入的doc_ID号
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public MaterialDocumentExtendModel findMaterialDocumentByDocumentId(string stockDocumentId)
        {
            MaterialDocumentExtendModel materialDocument = new MaterialDocumentExtendModel();

            StringBuilder sqlText = new StringBuilder("SELECT TOP 1 StockInDocument_ID,Document_Date,Posting_Date,DeliveryNote_ID,ReceiptNote_ID,StockManager,c.Supplier_Name,c.Supplier_ID,c.Address,c.company_ZipCode,b.Material_ID,d.Material_Name,a.Material_Group,a.CountInOrder,d.Measurement, a.Stock_Place,a.SupplierMaterial_ID,a.Evaluate_Type,a.InputCount,a.SKUCount,a.NotesNumber,a.Receive_count, a.CountInOrder,a.Tolerance,a.Document_Type,a.Inventory_Type,a.Stock_Place,a.Factory_ID,e.Factory_Name, a.Receiving_Place,a.Loading_Place,a.ExplainText,a.Batch_ID,a.Order_ID,a.Reversred,a.ToMaterial_ID,a.ToMaterial_Name,a.ToFactory_ID,a.ToFactory_Name,a.ToStock_ID,a.ToStock_Name,a.SpecialStock ");
            sqlText.Append(" FROM StockInDocument a JOIN [Order] b ON a.Order_ID = b.Order_ID  JOIN Supplier_Base c ON a.Supplier_ID = c.Supplier_ID  JOIN Material d ON a.Material_ID = d.Material_ID JOIN Factory e ON a.Factory_ID = e.Factory_ID ");
            sqlText.Append(" WHERE StockInDocument_ID = @stockInDocId");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@stockInDocId", SqlDbType.VarChar)
            };
            sqlParas[0].Value = stockDocumentId;
            //执行数据库查询语句
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(),sqlParas);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    materialDocument.Address = dr["Address"].ToString();
                    materialDocument.Batch_ID = dr["Batch_ID"].ToString();
                    materialDocument.Company_ZipCode = dr["Company_ZipCode"].ToString();
                    //materialDocument.Contact_Person = dr["Contact_Person"].ToString();
                    //materialDocument.Contact_phone1 = dr["Contact_phone1"].ToString();
                    materialDocument.CountInOrder = Convert.ToInt32(dr["CountInOrder"].ToString());
                    materialDocument.DeliveryNote_ID = dr["DeliveryNote_ID"].ToString();
                    materialDocument.Document_Date = Convert.ToDateTime(dr["Document_Date"].ToString());
                    materialDocument.Document_Type = dr["Document_Type"].ToString();
                    materialDocument.Evaluate_Type = dr["Evaluate_Type"].ToString();
                    materialDocument.ExplainText = dr["ExplainText"].ToString();
                    materialDocument.Factory_ID = dr["Factory_ID"].ToString();
                    materialDocument.Factory_Name = dr["Factory_Name"].ToString();
                    materialDocument.InputCount = Convert.ToInt32(dr["InputCount"].ToString());
                    materialDocument.Inventory_Type = dr["Inventory_Type"].ToString();
                    materialDocument.Loading_Place = dr["Loading_Place"].ToString();
                    materialDocument.Material_Count = Convert.ToInt32(dr["CountInOrder"].ToString());
                    materialDocument.Material_Group = dr["Material_Group"].ToString();
                    materialDocument.Material_ID = dr["Material_ID"].ToString();
                    materialDocument.Material_Name = dr["Material_Name"].ToString();
                    materialDocument.Measurement = dr["Measurement"].ToString();
                    materialDocument.NotesNumber = Convert.ToInt32(dr["NotesNumber"].ToString());
                    materialDocument.Order_ID = dr["Order_ID"].ToString();
                    materialDocument.Posting_Date = Convert.ToDateTime(dr["Posting_Date"].ToString());
                    materialDocument.ReceiptNote_ID = dr["ReceiptNote_ID"].ToString();
                    materialDocument.Receive_count = Convert.ToInt32(dr["Receive_count"].ToString());
                    materialDocument.Receiving_Place = dr["Receiving_Place"].ToString();
                    //materialDocument.Reversred = Convert.ToBoolean(dr["Reversred"].ToString());
                    materialDocument.SKUCount = Convert.ToInt32(dr["SKUCount"].ToString());
                    materialDocument.Stock_Place = dr["Stock_Place"].ToString();
                    materialDocument.StockManager = dr["StockManager"].ToString();
                    materialDocument.StockInDocument_ID = dr["StockInDocument_ID"].ToString();
                    materialDocument.Supplier_Name = dr["Supplier_Name"].ToString();
                    materialDocument.Supplier_ID = dr["Supplier_ID"].ToString();
                    materialDocument.SupplierMaterial_ID = dr["SupplierMaterial_ID"].ToString();
                    materialDocument.Tolerance = Convert.ToInt32(dr["Tolerance"].ToString());

                    materialDocument.ToMaterial_ID = dr["ToMaterial_ID"].ToString();
                    materialDocument.ToMaterial_Name = dr["ToMaterial_Name"].ToString();
                    materialDocument.ToFactory_ID = dr["ToFactory_ID"].ToString();
                    materialDocument.ToFactory_Name = dr["ToFactory_Name"].ToString();
                    materialDocument.ToStock_ID = dr["ToStock_ID"].ToString();
                    materialDocument.ToStock_Name = dr["ToStock_Name"].ToString();
                    materialDocument.SpecialStock = dr["SpecialStock"].ToString();
                    return materialDocument;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 找出最新的doc_ID
        /// </summary>
        /// <returns></returns>
        public string findLatestMaterialDocument()
        {
            StringBuilder sqlText = new StringBuilder("SELECT TOP 1  StockInDocument_ID  FROM StockInDocument ORDER BY StockInDocument_ID DESC");
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //只有一个值
                    string stockInDocumentID = dt.Rows[0][0].ToString();
                    return stockInDocumentID;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
