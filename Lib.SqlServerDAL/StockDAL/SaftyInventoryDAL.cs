﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class SaftyInventoryDAL : SaftyInventoryIDAL
    {
        /// <summary>
        /// 初始化待输入界面（手动设置安全库存）
        /// </summary>
        /// <returns></returns>
        public DataTable fillInput()
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID as 工厂号,Material_ID as 物料号,SaftyCountInput as 安全库存 from SaftyInventory where SaftyCountInput is null");
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;

        }

        /// <summary>
        /// 初始化待输入界面（系统计算安全库存）
        /// </summary>
        /// <returns></returns>
        public DataTable fillCompute()
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID as 工厂号,Material_ID as 物料号,SupplyCycle as 供应周期,OrderTime 订货次数,ShortTime as 缺货次数 from SaftyInventory")
            .Append(" where SaftyCountCompute is null");

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;
        }

        /// <summary>
        /// 更新安全库存表中手动输入值
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="count"></param>
        public void updateInputCount(string FactoryID, string MaterialID, float count)
        {
            StringBuilder strbld = new StringBuilder("update SaftyInventory set SaftyCountInput=@sci where Factory_ID=")
                .Append("@fid and Material_ID=@mid");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@sci",SqlDbType.Float),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar)
            };

            sqlpara[0].Value = count;
            sqlpara[1].Value = FactoryID;
            sqlpara[2].Value = MaterialID;

            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

        }

        /// <summary>
        /// 更新计算结果
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="MAD"></param>
        /// <param name="W"></param>
        /// <param name="R"></param>
        public void updateComputeCount(string FactoryID, string MaterialID, double MAD, double W, double R)
        {
            StringBuilder strbld = new StringBuilder("update SaftyInventory set SaftyCountCompute=@scc, MAD=@mad, SupplyCycle=@w,SaftyFactor=@r where Factory_ID=")
                .Append("@fid and Material_ID=@mid");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@scc",SqlDbType.Float),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@mad",SqlDbType.Float),
                new SqlParameter("@w",SqlDbType.Float),
                new SqlParameter("@r",SqlDbType.Float),
            };

            sqlpara[0].Value = Math.Round(MAD * R * Math.Sqrt(W), 2);
            sqlpara[1].Value = FactoryID;
            sqlpara[2].Value = MaterialID;
            sqlpara[3].Value = MAD;
            sqlpara[4].Value = W;
            sqlpara[5].Value = R;

            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

        }

        /// <summary>
        /// 将需求信息写入需求表中
        /// </summary>
        /// <param name="dt"></param>
        public void inputdemand(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StringBuilder strbld = new StringBuilder("insert into Demand_Time(Factory_ID,Material_ID,Demand) Values(@fid,@mid,@dm)");
                SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@dm",SqlDbType.Float),

            };
                sqlpara[0].Value = dt.Rows[i][0].ToString();
                sqlpara[1].Value = dt.Rows[i][1].ToString();
                sqlpara[2].Value = Convert.ToDouble(dt.Rows[i][2].ToString());

                DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            }
        }

        /// <summary>
        /// 读取需求量
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public DataTable selectDemand(string factoryid, string materialid)
        {
            StringBuilder strbld = new StringBuilder("select Demand from Demand_Time where Factory_ID=@fid and Material_ID= @mid");
            SqlParameter[] sqlpara = new SqlParameter[] {
            new SqlParameter("@fid",SqlDbType.VarChar),
            new SqlParameter("@mid",SqlDbType.VarChar),
            };
            sqlpara[0].Value = factoryid;
            sqlpara[1].Value = materialid;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return dt;

        }

        /// <summary>
        /// 计算MAD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public double computeMAD(DataTable dt)
        {
            int n = dt.Rows.Count;
            double[] demand = new double[n];
            double sum = 0;
            double abssum = 0;
            double mad;
            for (int i = 0; i < n; i++)
            {
                demand[i] = Convert.ToDouble(dt.Rows[i][0].ToString());
                sum = sum + demand[i];
            }
            double avgdemand = sum / n;
            for (int i = 0; i < n; i++)
            {
                abssum = abssum + Math.Abs(demand[i] - avgdemand);
            }
            mad = abssum / n;
            return mad;
        }

        /// <summary>
        /// 由订货次数和缺货次数计算安全系数
        /// </summary>
        /// <param name="ordertime"></param>
        /// <param name="shorttime"></param>
        /// <returns></returns>
        public double computeSaftyFactor(int ordertime, int shorttime)
        {
            double serviceLevel = (shorttime / ordertime) * 100;
            double r;
            if (serviceLevel == 100)
                r = 3.09;
            else if (serviceLevel >= 99.99)
                r = 3.08;
            else if (serviceLevel >= 99.87)
                r = 3;
            else if (serviceLevel >= 99.2)
                r = 2.4;
            else if (serviceLevel >= 99)
                r = 2.33;
            else if (serviceLevel >= 98)
                r = 2.05;
            else if (serviceLevel >= 97.7)
                r = 2;
            else if (serviceLevel >= 97)
                r = 1.88;
            else if (serviceLevel >= 96)
                r = 1.75;
            else if (serviceLevel >= 95)
                r = 1.65;
            else if (serviceLevel >= 90)
                r = 1.8;
            else if (serviceLevel >= 85)
                r = 1.04;
            else if (serviceLevel >= 84)
                r = 1;
            else if (serviceLevel >= 80)
                r = 0.84;
            else if (serviceLevel >= 75)
                r = 0.68;
            else
                r = 0.5;

            return r;
        }


        /// <summary>
        /// 判断需求量信息是否填写
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public bool madExist(string factoryid, string materialid)
        {
            StringBuilder strbld = new StringBuilder("select Demand from Demand_Time where Factory_ID =@fid and Material_ID=@mid");
            SqlParameter[] sqlpara = new SqlParameter[] {

                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
            };
            sqlpara[0].Value = factoryid;
            sqlpara[1].Value = materialid;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///查询安全库存
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public DataTable showSaftyStock(string factoryid, string materialid)
        {

            StringBuilder strbld = new StringBuilder("select Factory_ID as 工厂,Material_ID as 物料,SaftyCountInput as 手动输入的安全库存,")
            .Append(" SaftyCountCompute as 系统计算的安全库存  FROM SaftyInventory where Factory_ID=@fid and Material_ID=@mid ");

            SqlParameter[] sqlpara = new SqlParameter[] {
                   new SqlParameter("@fid",SqlDbType.VarChar),
                   new SqlParameter("@mid",SqlDbType.VarChar)
            };

            sqlpara[0].Value = factoryid;
            sqlpara[1].Value = materialid;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return dt;
        }

        #region
        /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable getMaterialID()
        {
            StringBuilder strbld = new StringBuilder("select Material_ID from SaftyInventory group by Material_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;
        }

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        public DataTable getFactoryID()
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID from SaftyInventory group by Factory_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;

        }
        #endregion


        /*保险储备量的计算直接给了一个公式
        ?订货点控制
        ?确定订货点应考虑的因素：?
        1、平均每天的正常耗用量，用n来表示；
        2、预计每天的最大耗用量，用m来表示；
        3、提前时间，用t来表示；
        4、预计最长提前时间，用r来表示；
        5、保险储备，用S来表示。
          保险储备量=1/2×（预计每天的最大耗用量×预计最长订货提前期－平均每天的正常耗用量×订货提前期）
        即 S=1/2×(mr-nt）
        再订货点 R=nt+S

        */
    }
}



