﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class MoveHandleDAL : MoveHandleIDAL
    {

        //维护Inventory（入库）
        public void updateInventoryStockIn(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
            string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            StringBuilder stringBuilder1 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
   .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
   .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas1 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas1[0].Value = Material_ID;
            sqlParas1[1].Value = Factory_ID;
            sqlParas1[2].Value = Stock_ID;
            sqlParas1[3].Value = Inventory_Type;
            sqlParas1[4].Value = Inventory_State;
            sqlParas1[5].Value = Evaluate_Type;
            sqlParas1[6].Value = Batch_ID;
            sqlParas1[7].Value = Material_Group;
            sqlParas1[8].Value = SKU;

            DataTable ds = DBHelper.ExecuteQueryDT(stringBuilder1.ToString(), sqlParas1);

            if (ds.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = Factory_ID;
                sqlParas2[2].Value = Stock_ID;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = Inventory_State;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder3 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = Count + (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7]));
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = Factory_ID;
                sqlParas3[3].Value = Stock_ID;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = Inventory_State;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParas3);
            }

        }

        #region 收货评分部分
        //维护收货评分记录（采购入库）
        public void receiveProcess(string stockInNoteID, string orderID, string supplierID, string materialID, double receiveCount, double countInOrder, DateTime stockInDate, string Material_Type)
        {

            goodsShipment(stockInNoteID, orderID, supplierID, materialID, receiveCount, countInOrder, stockInDate);
            if (Material_Type == "半成品" || Material_Type == "备件" || Material_Type == "成品")
            {
                receiveCheck(stockInNoteID, supplierID, materialID);
            }
            else
            {
                nonRawMaterialReceive(stockInNoteID, supplierID, materialID);
            }

        }

        /// <summary>
        /// 维护 Raw_Material_Receive_Check表
        /// </summary>
        /// <param name="stockInNoteID"></param>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        public void receiveCheck(string stockInNoteID, string supplierID, string materialID)
        {
            StringBuilder stbld = new StringBuilder("insert into Raw_Material_Receive_Check(GoodsReceiveNote_ID,Supplier_ID,Material_ID,Create_Time,Creator_Name)")
                .Append(" values(@grnid,@suid,@mid,@ct,@cn)");
            SqlParameter[] sqlp = new SqlParameter[]{
                new SqlParameter("@grnid",SqlDbType.VarChar),
                new SqlParameter("@suid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@ct",SqlDbType.Date),
                new SqlParameter("@cn",SqlDbType.VarChar),               

            };
            sqlp[0].Value = stockInNoteID;
            sqlp[1].Value = supplierID;
            sqlp[2].Value = materialID;
            sqlp[3].Value = null;
            sqlp[4].Value = null;
            DBHelper.ExecuteNonQuery(stbld.ToString(), sqlp);

        }

        /// <summary>
        /// 维护GoodsReceive_Note表
        /// </summary>
        /// <param name="stockInNoteID"></param>
        /// <param name="orderID"></param>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="receiveCount"></param>
        /// <param name="countInOrder"></param>
        /// <param name="stockInDate"></param>
        public void goodsShipment(string stockInNoteID, string orderID, string supplierID, string materialID, double receiveCount, double countInOrder, DateTime stockInDate)
        {
            StringBuilder stbld = new StringBuilder("insert into GoodsReceive_Note(GoodsReceiveNote_ID,Order_ID,Supplier_ID,Material_ID,Receive_Count,CountInOrder,StockIn_Date)")
                .Append(" values(@grnid,@oid,@suid,@mid,@rc,@cio,@sd)");
            SqlParameter[] sqlp = new SqlParameter[]{
                new SqlParameter("@grnid",SqlDbType.VarChar),
                new SqlParameter("@oid",SqlDbType.VarChar),
                new SqlParameter("@suid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@rc",SqlDbType.Float),
                new SqlParameter("@cio",SqlDbType.Float),
                new SqlParameter("@sd",SqlDbType.DateTime),

            };
            sqlp[0].Value = stockInNoteID;
            sqlp[1].Value = orderID;
            sqlp[2].Value = supplierID;
            sqlp[3].Value = materialID;
            sqlp[4].Value = receiveCount;
            sqlp[5].Value = countInOrder;
            sqlp[6].Value = stockInDate;

            DBHelper.ExecuteNonQuery(stbld.ToString(), sqlp);

        }

        /// <summary>
        /// 维护NonRaw_Material_Receive_Check表
        /// </summary>
        /// <param name="stockInNoteID"></param>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        public void nonRawMaterialReceive(string stockInNoteID, string supplierID, string materialID)
        {
            StringBuilder strbld = new StringBuilder("insert into NonRaw_Material_Receive_Check(GoodsReceiveNote_ID,Supplier_ID,Material_ID)")
                .Append(" values(@grnid,@suid,@mid)");
            SqlParameter[] sqlp = new SqlParameter[]{
                new SqlParameter("@grnid",SqlDbType.VarChar),
                new SqlParameter("@suid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),              

            };
            sqlp[0].Value = stockInNoteID;
            sqlp[1].Value = supplierID;
            sqlp[2].Value = materialID;
            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlp);
        }
        #endregion



        /// <summary>
        /// 维护到期提醒表（入库）
        /// </summary>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Material_ID"></param>
        /// <param name="batch_ID"></param>
        /// <param name="Manufacture_Date"></param>
        public void updRemind(string Factory_ID, string Stock_ID, string Material_ID, string batch_ID, DateTime Manufacture_Date)
        {
            StringBuilder stringbuilder1 = new StringBuilder("select Total_SLlife, MIN_SLlife from Material where Material_ID = @mid ");
            SqlParameter sqlParas = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlParas.Value = Material_ID;
            DataTable dt = DBHelper.ExecuteQueryDT(stringbuilder1.ToString(), sqlParas);
            StringBuilder stbld = new StringBuilder("insert into remind(Factory_ID,Stock_ID,Material_ID,batch_ID,Manufacture_Date,Shelflife,Maturity_Date) values(@fid,@sid,@mid,@bid,@md,@sl,@matud)");
            SqlParameter[] sqlpara = new SqlParameter[]{
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@md",SqlDbType.DateTime),
                new SqlParameter("@sl",SqlDbType.Int),
                new SqlParameter("@matud",SqlDbType.DateTime),

            };
            sqlpara[0].Value = Factory_ID;
            sqlpara[1].Value = Stock_ID;
            sqlpara[2].Value = Material_ID;
            sqlpara[3].Value = batch_ID;
            sqlpara[4].Value = Manufacture_Date;
            sqlpara[5].Value = ( dt==null||dt.Rows.Count<1|| dt.Rows[0][0].ToString()=="") ?1000: Convert.ToInt32(dt.Rows[0][0].ToString());
            sqlpara[6].Value = Manufacture_Date.AddDays(1000);// (Convert.ToInt32(dt.Rows[0][0].ToString()));

            DBHelper.ExecuteNonQuery(stbld.ToString(), sqlpara);
        }


        /// <summary>
        /// 维护安全库存（入库）
        /// </summary>
        public void upSaftyInventory(string factoryid, string materialid)
        {

            StringBuilder strbld = new StringBuilder("SELECT Material_ID,Factory_ID from SaftyInventory where ")
                .Append("Material_ID= @mid and Factory_ID=@fid");
            SqlParameter[] sqlParas1 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar)
              };
            sqlParas1[0].Value = materialid;
            sqlParas1[1].Value = factoryid;

            DataTable ds = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlParas1);

            if (ds.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into SaftyInventory(Material_ID,Factory_ID) values(")
   .Append(" @mid,@fid)");
                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                };

                sqlParas2[0].Value = materialid;
                sqlParas2[1].Value = factoryid;
                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);
            }
        }


        /// <summary>
        /// 维护移动平均价（采购入库）
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="avgprice"></param>
        /// <param name="count"></param>
        public void moveAvgPrice(string materialid, float avgprice, float count)
        {
            StringBuilder strbld = new StringBuilder("select MoveAveragePrice from MoveAvgPrice where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            if (dt.Rows.Count < 1)
            {
                StringBuilder strbld1 = new StringBuilder("insert into MoveAvgPrice(Material_ID,MoveAveragePrice) values(@mid,@map)");
                SqlParameter[] sqlpara1 = new SqlParameter[] { 
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@map",SqlDbType.VarChar)
                };

                sqlpara1[0].Value = materialid;
                sqlpara1[1].Value = avgprice;
                DBHelper.ExecuteNonQuery(strbld1.ToString(), sqlpara1);

            }
            else
            {
                /*   移动平均价=（原总库存*移动平均价+本次进库数量*进价）/（原库存+本次进库数量） */
                StringBuilder strbld0 = new StringBuilder("select sum(Count) from Inventory where Material_ID =@mid group by Material_ID");
                SqlParameter sqlpara0 = new SqlParameter("@mid", SqlDbType.VarChar);
                sqlpara0.Value = materialid;
                DataTable dt0 = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara0);


                StringBuilder strbld1 = new StringBuilder("update MoveAvgPrice set MoveAveragePrice=@map where Material_ID = @mid");
                SqlParameter[] sqlpara1 = new SqlParameter[] { 
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@map",SqlDbType.VarChar)
                };

                sqlpara1[0].Value = materialid;
                sqlpara1[1].Value = (Convert.ToDouble(dt0.Rows[0][0]) * Convert.ToDouble(dt.Rows[0][0]) + avgprice * count) / (Convert.ToDouble(dt0.Rows[0][0]) + count);
                DBHelper.ExecuteNonQuery(strbld1.ToString(), sqlpara1);

            }

            StringBuilder strbld2 = new StringBuilder("insert into HistoryMoveAvgPrice(Material_ID,MoveAvgPrice,Price_Date) values(@mid,@map,@pd)");
            SqlParameter[] sqlpara2 = new SqlParameter[] { 
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@map",SqlDbType.Float),
                    new SqlParameter("@pd",SqlDbType.DateTime)
                };

            sqlpara2[0].Value = materialid;
            sqlpara2[1].Value = avgprice;
            sqlpara2[2].Value = DateTime.Now.Date;
            DBHelper.ExecuteNonQuery(strbld2.ToString(), sqlpara2);
        }



        /// <summary>
        /// 维护移动平均价（采购入库）
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="avgprice"></param>
        /// <param name="count"></param>
        public void moveAvgPrice(string materialid, float avgprice, float count, string factoryid)
        {
            StringBuilder strbld = new StringBuilder("select Moving_AGPrice from MT_FTY where Material_ID = @mid and Factory_ID =@fid");
            SqlParameter[] sqlpara = new SqlParameter[] {
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@fid",SqlDbType.VarChar)
                };
            sqlpara[0].Value = materialid;
            sqlpara[1].Value = factoryid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            if (dt == null || dt.Rows.Count < 1||dt.Rows[0][0].ToString()==""||dt.Rows[0][0].Equals(DBNull.Value))
            {
                StringBuilder strbld1 = new StringBuilder("update MT_FTY set Moving_AGPrice=@mag where Material_ID=@mid AND Factory_ID=@fid");
                SqlParameter[] sqlpara1 = new SqlParameter[] {
                    new SqlParameter("@mag",SqlDbType.Float),
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@fid",SqlDbType.VarChar)
                };

                sqlpara1[0].Value = avgprice;
                sqlpara1[1].Value = materialid;
                sqlpara1[2].Value = factoryid;
                DBHelper.ExecuteNonQuery(strbld1.ToString(), sqlpara1);

            }
            else
            {
                /*   移动平均价=（原总库存*移动平均价+本次进库数量*进价）/（原库存+本次进库数量） */
                StringBuilder strbld0 = new StringBuilder("select sum(Count) from Inventory where Material_ID =@mid and Factory_ID=@fid group by Material_ID,Factory_ID");
                SqlParameter[] sqlpara0 = new SqlParameter[] {
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@fid",SqlDbType.VarChar)
                };
                sqlpara[0].Value = materialid;
                sqlpara[1].Value = factoryid;
                DataTable dt0 = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara0);


                StringBuilder strbld1 = new StringBuilder("update MT_FTY set Moving_AGPrice=@mag where Material_ID=@mid AND Factory_ID=@fid");
                SqlParameter[] sqlpara1 = new SqlParameter[] {
                    new SqlParameter("@mag",SqlDbType.Float),
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@fid",SqlDbType.VarChar)
                };

                float a1= (float)((dt0 == null || dt0.Rows.Count < 1 || dt0.Rows[0][0].ToString() == "" || dt0.Rows[0][0].Equals(DBNull.Value)) ? 0 :Convert.ToDouble(dt0.Rows[0][0]));
                float a2 = (float)Convert.ToDouble(dt.Rows[0][0]);

                sqlpara1[0].Value = (a1*a2 + avgprice * count) /(a1+ count);
                sqlpara1[1].Value = materialid;
                sqlpara1[2].Value = factoryid;
                DBHelper.ExecuteNonQuery(strbld1.ToString(), sqlpara1);

            }

            StringBuilder strbld2 = new StringBuilder("insert into HistoryMoveAvgPrice(Material_ID,MoveAvgPrice,Price_Date) values(@mid,@map,@pd)");
            SqlParameter[] sqlpara2 = new SqlParameter[] {
                    new SqlParameter("@mid",SqlDbType.VarChar),
                    new SqlParameter("@map",SqlDbType.Float ),
                    new SqlParameter("@pd",SqlDbType.Date)
                };

            sqlpara2[0].Value = materialid;
            sqlpara2[1].Value = avgprice;
            sqlpara2[2].Value = DateTime.Now.Date;
            DBHelper.ExecuteNonQuery(strbld2.ToString(), sqlpara2);
        }



        /// <summary>
        /// 维护Inventory （出库）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        public void updateInventoryStockOut(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
          string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            StringBuilder stringBuilder1 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
   .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
   .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas1 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas1[0].Value = Material_ID;
            sqlParas1[1].Value = Factory_ID;
            sqlParas1[2].Value = Stock_ID;
            sqlParas1[3].Value = Inventory_Type;
            sqlParas1[4].Value = Inventory_State;
            sqlParas1[5].Value = Evaluate_Type;
            sqlParas1[6].Value = Batch_ID;
            sqlParas1[7].Value = Material_Group;
            sqlParas1[8].Value = SKU;

            DataTable ds = DBHelper.ExecuteQueryDT(stringBuilder1.ToString(), sqlParas1);

            if (ds.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = Factory_ID;
                sqlParas2[2].Value = Stock_ID;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = Inventory_State;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = -Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder4 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7])) - Count;
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = Factory_ID;
                sqlParas3[3].Value = Stock_ID;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = Inventory_State;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder4.ToString(), sqlParas3);
            }


        }


        /// <summary>
        /// 维护Inventory （一步转移）
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        public void updateInventoryStockTransfer(string Material_ID, string Factory_ID, string Stock_ID, string factoryid, string stockid, string Inventory_Type,
          string Inventory_State, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            //转入
            #region
            StringBuilder stringBuilder1 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
  .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
  .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas1 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas1[0].Value = Material_ID;
            sqlParas1[1].Value = Factory_ID;
            sqlParas1[2].Value = Stock_ID;
            sqlParas1[3].Value = Inventory_Type;
            sqlParas1[4].Value = Inventory_State;
            sqlParas1[5].Value = Evaluate_Type;
            sqlParas1[6].Value = Batch_ID;
            sqlParas1[7].Value = Material_Group;
            sqlParas1[8].Value = SKU;

            DataTable ds = DBHelper.ExecuteQueryDT(stringBuilder1.ToString(), sqlParas1);

            if (ds.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = Factory_ID;
                sqlParas2[2].Value = Stock_ID;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = Inventory_State;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder4 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = Count + (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7]));
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = Factory_ID;
                sqlParas3[3].Value = Stock_ID;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = Inventory_State;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder4.ToString(), sqlParas3);
            }

            #endregion

            //转出
            #region
            StringBuilder stringBuilder11 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
   .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
   .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas11 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas11[0].Value = Material_ID;
            sqlParas11[1].Value = factoryid;
            sqlParas11[2].Value = stockid;
            sqlParas11[3].Value = Inventory_Type;
            sqlParas11[4].Value = Inventory_State;
            sqlParas11[5].Value = Evaluate_Type;
            sqlParas11[6].Value = Batch_ID;
            sqlParas11[7].Value = Material_Group;
            sqlParas11[8].Value = SKU;

            DataTable ds1 = DBHelper.ExecuteQueryDT(stringBuilder11.ToString(), sqlParas11);

            if (ds1.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = factoryid;
                sqlParas2[2].Value = stockid;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = Inventory_State;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = -Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder4 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7])) - Count;
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = factoryid;
                sqlParas3[3].Value = stockid;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = Inventory_State;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder4.ToString(), sqlParas3);
            }



            #endregion

        }

        /// <summary>
        /// 库存状态转移更新Inventory
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Factory_ID"></param>
        /// <param name="Stock_ID"></param>
        /// <param name="Inventory_Type"></param>
        /// <param name="Inventory_State"></param>
        /// <param name="newState"></param>
        /// <param name="Evaluate_Type"></param>
        /// <param name="Batch_ID"></param>
        /// <param name="Count"></param>
        /// <param name="Material_Group"></param>
        /// <param name="SKU"></param>
        public void updateInventoryStockStateTransfer(string Material_ID, string Factory_ID, string Stock_ID, string Inventory_Type,
          string Inventory_State, string newState, string Evaluate_Type, string Batch_ID, double Count, string Material_Group, string SKU)
        {
            //转入
            #region
            StringBuilder stringBuilder1 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
  .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
  .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas1 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas1[0].Value = Material_ID;
            sqlParas1[1].Value = Factory_ID;
            sqlParas1[2].Value = Stock_ID;
            sqlParas1[3].Value = Inventory_Type;
            sqlParas1[4].Value = newState;
            sqlParas1[5].Value = Evaluate_Type;
            sqlParas1[6].Value = Batch_ID;
            sqlParas1[7].Value = Material_Group;
            sqlParas1[8].Value = SKU;

            DataTable ds = DBHelper.ExecuteQueryDT(stringBuilder1.ToString(), sqlParas1);

            if (ds.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = Factory_ID;
                sqlParas2[2].Value = Stock_ID;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = newState;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder4 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = Count + (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7]));
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = Factory_ID;
                sqlParas3[3].Value = Stock_ID;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = newState;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder4.ToString(), sqlParas3);
            }

            #endregion

            //转出
            #region
            StringBuilder stringBuilder11 = new StringBuilder("SELECT Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Material_Group,SKU FROM Inventory where Material_ID=")
   .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it").Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
   .Append(" and Material_Group=@mg and SKU = @sku");
            // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

            SqlParameter[] sqlParas11 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),

                     };
            sqlParas11[0].Value = Material_ID;
            sqlParas11[1].Value = Factory_ID;
            sqlParas11[2].Value = Stock_ID;
            sqlParas11[3].Value = Inventory_Type;
            sqlParas11[4].Value = Inventory_State;
            sqlParas11[5].Value = Evaluate_Type;
            sqlParas11[6].Value = Batch_ID;
            sqlParas11[7].Value = Material_Group;
            sqlParas11[8].Value = SKU;

            DataTable ds1 = DBHelper.ExecuteQueryDT(stringBuilder11.ToString(), sqlParas11);

            if (ds1.Rows.Count == 0)
            {
                StringBuilder stringBuilder2 = new StringBuilder("insert into Inventory(Material_ID,Factory_ID,Stock_ID,Inventory_Type,Inventory_State,Evaluate_Type,Batch_ID,Count,Stocktaking,Material_Group,SKU) values(")
    .Append(" @mid,@fid,@sid,@it,@is,@et,@bid,@c,@stk,@mg,@sku")
    .Append(")");
                // InventoryAmountStatisticalList iasl = new InventoryAmountStatisticalList();

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@stk",SqlDbType.Bit),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                     };
                sqlParas2[0].Value = Material_ID;
                sqlParas2[1].Value = Factory_ID;
                sqlParas2[2].Value = Stock_ID;
                sqlParas2[3].Value = Inventory_Type;
                sqlParas2[4].Value = Inventory_State;
                sqlParas2[5].Value = Evaluate_Type;
                sqlParas2[6].Value = Batch_ID;
                sqlParas2[7].Value = -Count;
                sqlParas2[8].Value = false;
                sqlParas2[9].Value = Material_Group;
                sqlParas2[10].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder2.ToString(), sqlParas2);

            }

            else
            {
                StringBuilder stringBuilder4 = new StringBuilder("UPDATE Inventory SET Count =").Append("@c").Append(" where Material_ID =")
                .Append("@mid").Append(" and Factory_ID=").Append("@fid").Append(" and Stock_id=").Append("@sid").Append(" and Inventory_Type=").Append("@it")
                .Append(" and Inventory_State=").Append("@is").Append(" and Evaluate_Type=").Append("@et").Append(" and Batch_ID=").Append("@bid")
                 .Append(" and Material_Group=@mg and SKU = @sku");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                new SqlParameter("@c", SqlDbType.Float),
                new SqlParameter("@mid", SqlDbType.VarChar),
                new SqlParameter("@fid", SqlDbType.VarChar),
                new SqlParameter("@sid", SqlDbType.VarChar),
                new SqlParameter("@it", SqlDbType.VarChar),
                new SqlParameter("@is", SqlDbType.VarChar),
                new SqlParameter("@et", SqlDbType.VarChar),
                new SqlParameter("@bid", SqlDbType.VarChar),
                new SqlParameter("@mg", SqlDbType.VarChar),
                new SqlParameter("@sku", SqlDbType.VarChar),
                
                     };
                sqlParas3[0].Value = (DBNull.Value == null ? 0 : Convert.ToDouble(ds.Rows[0][7])) - Count;
                sqlParas3[1].Value = Material_ID;
                sqlParas3[2].Value = Factory_ID;
                sqlParas3[3].Value = Stock_ID;
                sqlParas3[4].Value = Inventory_Type;
                sqlParas3[5].Value = Inventory_State;
                sqlParas3[6].Value = Evaluate_Type;
                sqlParas3[7].Value = Batch_ID;
                sqlParas3[8].Value = Material_Group;
                sqlParas3[9].Value = SKU;

                DBHelper.ExecuteNonQuery(stringBuilder4.ToString(), sqlParas3);
            }



            #endregion

        }

        /// <summary>
        ///库存信息
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="factoryid"></param>
        /// <param name="stockid"></param>
        /// <param name="stockinDate"></param>
        /// [],[] ,[]
        public void InventoryInfo(string materialid, string factoryid, string stockid, string Inventory_Type, string Batch_ID, string Inventory_State, string Evaluate_Type, string Material_Group, float count, float price, DateTime stockinDate,string SKU)
        {
            StringBuilder strbld0 = new StringBuilder("insert into InventoryInfo(Factory_ID,Stock_ID,Material_ID,Inventory_Type,Batch_ID,Inventory_State,Evaluate_Type,Material_Group,Move_Date,Count,SumPrice,History_Count,History_SumPrice,curCount,curSumprice,SKU)")
            .Append("  values(@fid,@sid,@mid,@it,@bid,@is,@et,@mg,@md,@c,@sp,@hc,@hs,@cc,@csp,@sku)");

            #region 选取当前的前一次历史库存
            StringBuilder strbld1 = new StringBuilder("select top 1 History_Count,History_SumPrice,Move_Date,curCount,curSumprice from InventoryInfo")
                .Append(" where Factory_ID=@fid and Stock_ID=@sid and Material_ID=@mid and Inventory_Type=@it and ")
                .Append(" Batch_ID=@bid and Inventory_State=@is and Evaluate_Type=@et and Material_Group=@mg and SKU =@sku")
                .Append(" order by Move_Date");

            SqlParameter[] sqlpara1 = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@sku",SqlDbType.VarChar),
            
            };

            sqlpara1[0].Value = factoryid;
            sqlpara1[1].Value = stockid;
            sqlpara1[2].Value = materialid;
            sqlpara1[3].Value = Inventory_Type;
            sqlpara1[4].Value = Batch_ID;
            sqlpara1[5].Value = Inventory_State;
            sqlpara1[6].Value = Evaluate_Type;
            sqlpara1[7].Value = Material_Group;
            sqlpara1[8].Value = SKU;

            DataTable dt1 = DBHelper.ExecuteQueryDT(strbld1.ToString(), sqlpara1);
            #endregion


            SqlParameter[] sqlpara0 = new SqlParameter[] { 
            
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),

                new SqlParameter("@md",SqlDbType.DateTime),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@sp",SqlDbType.Float),
                new SqlParameter("@hc",SqlDbType.Float),
                new SqlParameter("@hs",SqlDbType.Float),
                new SqlParameter("@cc",SqlDbType.Float),
                new SqlParameter("@csp",SqlDbType.Float),
                
                new SqlParameter("@sku",SqlDbType.VarChar),
            };

            sqlpara0[0].Value = factoryid;
            sqlpara0[1].Value = stockid;
            sqlpara0[2].Value = materialid;
            sqlpara0[3].Value = Inventory_Type;
            sqlpara0[4].Value = Batch_ID;
            sqlpara0[5].Value = Inventory_State;
            sqlpara0[6].Value = Evaluate_Type;
            sqlpara0[7].Value = Material_Group;

            sqlpara0[8].Value = stockinDate;
            sqlpara0[9].Value = count;
            sqlpara0[10].Value = price * count;



            if (dt1.Rows.Count != 0 && (stockinDate.Year == Convert.ToDateTime(dt1.Rows[0][2]).Year && stockinDate.Month == Convert.ToDateTime(dt1.Rows[0][2]).Month))
            {
                sqlpara0[11].Value = Convert.ToDouble(dt1.Rows[0][0]);
                sqlpara0[12].Value = Convert.ToDouble(dt1.Rows[0][1]);
            }

            else if (dt1.Rows.Count == 0)
            {
                sqlpara0[11].Value = 0;
                sqlpara0[12].Value = 0;
            }

            else
            {
                sqlpara0[11].Value = count + Convert.ToDouble(dt1.Rows[0][0]);
                sqlpara0[12].Value = price * count + Convert.ToDouble(dt1.Rows[0][1]);
            }

            sqlpara0[13].Value = dt1.Rows.Count == 0 ? count:count + Convert.ToDouble(dt1.Rows[0][0]); ;
            sqlpara0[14].Value = dt1.Rows.Count == 0 ? price * count : price * count + Convert.ToDouble(dt1.Rows[0][1]);

            sqlpara0[15].Value = SKU;

            DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara0);


        }


        /// <summary>
        /// 注释
        /// </summary>
        /// <param name="materialid"></param>
        /// <param name="factoryid"></param>
        /// <param name="stockid"></param>
        /// <param name="stockinDate"></param>
        public void AccountTimeManagement(string materialid, string factoryid, string stockid, DateTime stockinDate)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID,Factory_ID,Stock_ID,StockInDate from InventoryInfo")
                .Append(" where Material_ID=@mid and Factory_ID=@fid, and Stock_ID =@sid and StockInDate = @sd");
            SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@sd",SqlDbType.Date),            
            };

            sqlpara[0].Value = materialid;
            sqlpara[1].Value = factoryid;
            sqlpara[2].Value = stockid;
            sqlpara[3].Value = stockinDate;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            if (dt.Rows.Count == 0)
            {
                StringBuilder strbld1 = new StringBuilder("select Count from AccountTimeManagement")
                   .Append(" where Material_ID=@mid and Factory_ID=@fid, and Stock_ID =@sid and Count>=0");
                SqlParameter[] sqlpara1 = new SqlParameter[] { 
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),          
            };

                sqlpara1[0].Value = materialid;
                sqlpara1[1].Value = factoryid;
                sqlpara1[2].Value = stockid;

                DataTable dt1 = DBHelper.ExecuteQueryDT(strbld1.ToString(), sqlpara1);

                if (dt1.Rows.Count > 0)
                {
                    StringBuilder strbld3 = new StringBuilder("insert into AccountTimeManagement(Material_ID,Factory_ID,Stock_ID,StockInDate)")
                        .Append(" values(@mid,@fid,@sid,@sd)");
                    SqlParameter[] sqlpara3 = new SqlParameter[] { 
                        new SqlParameter("@mid",SqlDbType.VarChar),
                        new SqlParameter("@fid",SqlDbType.VarChar),
                        new SqlParameter("@sid",SqlDbType.VarChar),
                        new SqlParameter("@sd",SqlDbType.Date),            
                    };

                    sqlpara3[0].Value = materialid;
                    sqlpara3[1].Value = factoryid;
                    sqlpara3[2].Value = stockid;
                    sqlpara3[3].Value = stockinDate;

                    DataTable dt3 = DBHelper.ExecuteQueryDT(strbld3.ToString(), sqlpara3);

                }
            }
        }


    }
}
