﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Model.StockModel.StockCheckModel;
using Lib.IDAL.StockIDAL;


namespace Lib.SqlServerDAL.StockDAL
{
    public class CreateStocktakingDAL:CreateStocktakingIDAL
    {
        /// <summary>
        /// 选择符合用户指定条件的物料（集中创建）
        /// 返回的datable包含列：material,factory,stock,Inventorytype,batch,count,SKU
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <param name="FactoryID"></param>
        /// <param name="StockID"></param>
        /// <param name="EvaluateType"></param>
        /// <param name="MaterialGroup"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="b3"></param>
        /// <returns></returns>
        public DataTable selectMaterial(string MaterialID, string FactoryID, string StockID, string EvaluateType, string MaterialGroup,bool b1,bool b2,bool b3)
        {
            int num = 0;
            int i = 0;
           
            if (!MaterialID.Equals(""))
                num++;
            if (!FactoryID.Equals(""))
                num++;
            if (!StockID.Equals(""))
                num++;
            if (!EvaluateType.Equals(""))
                num++;
            if (!MaterialGroup.Equals(""))
                num++;

            SqlParameter[] sqlpara = new SqlParameter[num];

            StringBuilder strbld = new StringBuilder("select Material_ID,Factory_ID,Stock_ID,Inventory_State,batch_ID, sum(Count),SKU,Evaluate_Type, Material_Group,Inventory_Type from Inventory where ");
            if (MaterialID != "")
            {
                strbld.Append("Material_ID = @mid ");
                sqlpara[i] = new SqlParameter("@mid",SqlDbType.VarChar);
                sqlpara[i].Value = MaterialID;
                i++;
            }

            if (MaterialID != "" && FactoryID != "")
            {
                strbld.Append("and Factory_ID=@fid ");
                sqlpara[i] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[i].Value = FactoryID;
                i++;
            }
            if (MaterialID == "" && FactoryID != "")
            {
                strbld.Append("Factory_ID=@fid ");
                sqlpara[i] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[i].Value = FactoryID;
                i++;
            }

            if ((MaterialID != "" || FactoryID != "")&&StockID!= "")
            {
                strbld.Append(" and Stock_ID=@sid ");
                sqlpara[i] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[i].Value = StockID;
                i++;
            }
            if ((MaterialID == "" && FactoryID == "") && StockID != "")
            {
                strbld.Append(" Stock_ID=@sid ");
                sqlpara[i] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[i].Value = StockID;
                i++;
            }

            if ((MaterialID != "" || FactoryID != "" || StockID != "")&&EvaluateType!="")
            {
                strbld.Append(" and Evaluate_Type=@et ");
                sqlpara[i] = new SqlParameter("@et", SqlDbType.VarChar);
                sqlpara[i].Value = EvaluateType;
                i++;
            }
            if ((MaterialID == "" && FactoryID == "" && StockID == "") && EvaluateType != "")
            {
                strbld.Append(" Evaluate_Type=@et ");
                sqlpara[i] = new SqlParameter("@et", SqlDbType.VarChar);
                sqlpara[i].Value = EvaluateType;
                i++;
            }

            if ((MaterialID != "" || FactoryID != "" || StockID != "" || EvaluateType != "")&&MaterialGroup!="")
            {
                strbld.Append(" and Material_Group=@mg ");
                sqlpara[i] = new SqlParameter("@mg", SqlDbType.VarChar);
                sqlpara[i].Value = MaterialGroup;
                i++;
            }
            if ((MaterialID == "" && FactoryID == "" && StockID == "" && EvaluateType == "") && MaterialGroup != "")
            {
                strbld.Append(" Material_Group=@mg ");
                sqlpara[i] = new SqlParameter("@mg", SqlDbType.VarChar);
                sqlpara[i].Value = MaterialGroup;
                i++;
            }

            if (b1 == true && (b2==false && b3 == false) )
            {
                strbld.Append(" and Inventory_State = '非限制库存'");
            }

            if (b1 == true && (b2 == true || b3 == true))
            {
                strbld.Append(" and (Inventory_State = '非限制库存'");
            }

            if (b1 == false && b2 == true && b3 == true)
            {
                strbld.Append(" and (Inventory_State = '质量检查'");
            }
            if (b1 == false && b2 == true && b3 == false)
            {
                strbld.Append(" and Inventory_State = '质量检查'");
            }

            if ((b1 == false && b2 == false) && b3 == true)
            {
                strbld.Append(" and Inventory_State = '冻结库存'");
            }

            if (b1 == true &&b2 == true)
            {
                strbld.Append(" or Inventory_State = '质量检查'");
            }
            
            if ((b1 == true || b2 == true) && b3 == true)
            {
                strbld.Append(" or Inventory_State = '冻结库存'");
            }

            if ((b1 == true && b2 == true) || (b1 == true && b3 == true) || (b2 == true && b3 == true))
            {
                strbld.Append(" )");
            }

            strbld.Append(" group by Material_ID,Factory_ID,Stock_ID,Inventory_State,batch_ID,Evaluate_Type, Material_Group,Inventory_Type,SKU");
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);
            
            return dt;
        }

        /// <summary>
        /// 产生盘点物料凭证（集中创建和单个创建共用）
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="i"></param>
        /// <param name="dtime2"></param>
        /// <param name="referID"></param>
        /// <param name="stockID"></param>
        /// <returns></returns>
        public bool createDocument(DataTable dt,DateTime dtime2,string referID,string stockID)
        {
            //确定当前凭证编号
            StringBuilder sbld = new StringBuilder("select currentID from stockCheckDocumentID");
            DataTable dcuid = DBHelper.ExecuteQueryDT(sbld.ToString());
            //创建凭证
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StringBuilder strbld = new StringBuilder("insert into stocktakingDocument(stocktakingDocument_ID,Material_ID,Document_Date,Factory_ID,")
                    .Append("Stock_ID,planStocktaking_Date,stocktakingrefer_ID,stocktaking_ID,")
                .Append("Freezingpost,Freezingsum,stockCount,State,batch_ID,Inventory_State,SKU,Rechecked,Item_State,Evaluate_Type, Material_Group,Inventory_Type)")
                .Append(" values (@sdid,@mid,@dd,@fid,@sid,@psd,@srid,")
                    .Append("@stid,@fp,@fs,@sc,@st,@bid,@is,@sku,@rcd,@ist,@et,@mg,@it)");

                SqlParameter[] sqlpara = new SqlParameter[]{
                new SqlParameter("@sdid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@dd",SqlDbType.Date),
                new SqlParameter ("@fid",SqlDbType.VarChar),
                new SqlParameter ("@sid",SqlDbType.VarChar),
                new SqlParameter ("@psd",SqlDbType.Date),
                new SqlParameter ("@srid",SqlDbType.VarChar),
                new SqlParameter ("@stid",SqlDbType.VarChar),
                new SqlParameter ("@fp",SqlDbType.Bit),
                new SqlParameter ("@fs",SqlDbType.Bit),
                new SqlParameter ("@sc",SqlDbType.Float),
                new SqlParameter ("@st",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@sku",SqlDbType.VarChar),
                new SqlParameter("@rcd",SqlDbType.Bit),
                new SqlParameter("@ist",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),

            };

                sqlpara[0].Value = dcuid.Rows[0][0].ToString();
                sqlpara[1].Value = dt.Rows[i][0].ToString();
                sqlpara[2].Value = System.DateTime.Now.Date;
                sqlpara[3].Value = dt.Rows[i][1].ToString();
                sqlpara[4].Value = dt.Rows[i][2].ToString();
                sqlpara[5].Value = dtime2;
                sqlpara[6].Value = referID;
                sqlpara[7].Value = stockID;
                sqlpara[8].Value = false;
                sqlpara[9].Value = false;
                sqlpara[10].Value = (float)Convert.ToDouble(dt.Rows[i][5].ToString());
                sqlpara[11].Value = "创建盘点凭证";
                sqlpara[12].Value = dt.Rows[i][4].ToString();
                sqlpara[13].Value = dt.Rows[i][3].ToString();
                sqlpara[14].Value = dt.Rows[i][6].ToString();
                sqlpara[15].Value = false;
                sqlpara[16].Value = "还未计数";
                sqlpara[17].Value = dt.Rows[i][7].ToString();
                sqlpara[18].Value = dt.Rows[i][8].ToString();
                sqlpara[19].Value = dt.Rows[i][9].ToString();

                DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);
            }

            //修改当前凭证编号
            StringBuilder strbld1 = new StringBuilder("update  stockCheckDocumentID set currentID = currentID +1");
            DBHelper.ExecuteNonQuery(strbld1.ToString());
            return true;
        }

        /// <summary>
        /// 选择满足用户指定条件的物料(单个创建)
        ///material,factory,stock ,Inventorystste,batch,count,SKU
        /// </summary>
        /// <returns></returns>
        public DataTable selectMaterialSingle(DataTable dt)
        {
            int n = dt.Rows.Count;
            DataTable dt2 = new DataTable("dt");
            dt2.Columns.Add("material", typeof(string));
            dt2.Columns.Add("factory", typeof(string));
            dt2.Columns.Add("stock", typeof(string));
            dt2.Columns.Add("Inventorystate", typeof(string));
            dt2.Columns.Add("batch", typeof(string));
            dt2.Columns.Add("count", typeof(double));
            dt2.Columns.Add("sku", typeof(string));
            dt2.Columns.Add("evaluatetype", typeof(string));
            dt2.Columns.Add("materialgroup", typeof(string));
            dt2.Columns.Add("Inventorytype", typeof(string));
            for(int i =0;i<n;i++)
            {
                StringBuilder strbld = new StringBuilder("select  Material_ID,Factory_ID,Stock_ID,Inventory_State,batch_ID,sum(Count),SKU,Evaluate_Type, Material_Group,Inventory_Type  from Inventory where Factory_ID = @fid and Stock_ID = @sid and ")
                .Append("Material_ID = @mid and Inventory_State=@is and Inventory_Type = @it and Batch_ID = @bid");
                strbld.Append(" group by Material_ID,Factory_ID,Stock_ID,Inventory_State,batch_ID,SKU,Evaluate_Type, Material_Group,Inventory_Type");
            SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("sid",SqlDbType .VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),     

            };
            sqlpara[0].Value = dt.Rows[i][1].ToString();
            sqlpara[1].Value = dt.Rows[i][2].ToString();
            sqlpara[2].Value = dt.Rows[i][0].ToString();
            sqlpara[3].Value = dt.Rows[i][3].ToString();
            sqlpara[4].Value = dt.Rows[i][5].ToString();
            if (!dt.Rows[i][4].ToString().Equals(""))
            {
                sqlpara[5].Value = dt.Rows[i][4].ToString();
            }
            else
            {
                sqlpara[5].Value = ""; 
            }
            
            DataTable  dt1 = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);

            for (int j = 0; j < dt1.Rows.Count; j++)
            {
                DataRow dr = dt2.NewRow();
                dr["material"] = dt1.Rows[j][0].ToString();
                dr["factory"] = dt1.Rows[j][1].ToString();
                dr["stock"] = dt1.Rows[j][2].ToString();
                dr["Inventorystate"] = dt1.Rows[j][3].ToString();
                dr["batch"] = dt1.Rows[j][4].ToString();
                dr["count"] = Convert.ToDouble(dt1.Rows[j][5].ToString());
                dr["sku"] = dt1.Rows[j][6].ToString();
                dr["evaluatetype"] = dt1.Rows[j][7].ToString();
                dr["materialgroup"] = dt1.Rows[j][8].ToString();
                dr["inventorytype"] = dt1.Rows[j][9].ToString();

                dt2.Rows.Add(dr);
            }
            }

            return dt2;
        }


        /// <summary>
        /// 根据物料编号确定物料名称
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialname(string materialid)
        {
            StringBuilder strbld = new StringBuilder("select Material_Name from Material where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid",SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            string s = dt.Rows[0][0].ToString();
            return s;

        }

        /// <summary>
        /// 盘点冻结
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        public bool freezingstock(string stocktakingDocuID)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID,Factory_ID,Stock_ID,")
            .Append("Batch_ID from stocktakingDocument where stocktakingDocument_ID = @sdid");
            SqlParameter sqlpara = new SqlParameter("@sdid",SqlDbType.VarChar);
            sqlpara.Value = stocktakingDocuID;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            int n = dt.Rows.Count;
            if (n < 1)
                return false;
            for (int i = 0; i < n; i++)
            {


                SqlParameter[] sqlpara1 = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType .VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
            };
                sqlpara1[0].Value = dt.Rows[i][1].ToString();
                sqlpara1[1].Value = dt.Rows[i][2].ToString();
                sqlpara1[2].Value = dt.Rows[i][0].ToString();
            }

            StringBuilder strbld2 = new StringBuilder("update stocktakingDocument set Freezingpost=1,Freezingsum=1 where stocktakingDocument_ID = @sdid");
            SqlParameter sqlpara2 = new SqlParameter("@sdid", SqlDbType.VarChar);
            sqlpara2.Value = stocktakingDocuID;
            DBHelper.ExecuteNonQuery(strbld2.ToString(),sqlpara2);

            return true;
        }

        /// <summary>
        /// 盘点解冻
        /// </summary>
        /// <param name="stocktakingDocuID"></param>
        /// <returns></returns>
        public bool defreezingstock(string stocktakingDocuID)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID,Factory_ID,Stock_ID,")
           .Append("Batch_ID from stocktakingDocument where stocktakingDocument_ID = @sdid");
            SqlParameter sqlpara = new SqlParameter("@sdid", SqlDbType.VarChar);
            sqlpara.Value = stocktakingDocuID;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            int n = dt.Rows.Count;
            for (int i = 0; i < n; i++)
            {


                SqlParameter[] sqlpara1 = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType .VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),

            };
                sqlpara1[0].Value = dt.Rows[i][1].ToString();
                sqlpara1[1].Value = dt.Rows[i][2].ToString();
                sqlpara1[2].Value = dt.Rows[i][0].ToString();
            }

            StringBuilder strbld2 = new StringBuilder("update stocktakingDocument set Freezingpost=0,Freezingsum=0 where stocktakingDocument_ID = @sdid");
            SqlParameter sqlpara2 = new SqlParameter("@sdid", SqlDbType.VarChar);
            sqlpara2.Value = stocktakingDocuID;
            DBHelper.ExecuteNonQuery(strbld2.ToString(), sqlpara2);
            return true;
        }

        /// <summary>
        /// 选择物料编号（单个创建）
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        public DataTable selectMaterial(string factory, string stock)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID from Inventory where   Factory_ID = @fid and Stock_ID=@sid group by Material_ID");
            SqlParameter[] sqlpara = new SqlParameter[] {
            new SqlParameter("@fid",SqlDbType.VarChar),
            new SqlParameter("@sid",SqlDbType.VarChar)
            };
            sqlpara[0].Value = factory;
            sqlpara[1].Value = stock;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return dt;
        }
    }
}
