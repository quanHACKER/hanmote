﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class CheckResultDAL:CheckResultIDAL
    {
       /// <summary>
       /// 根据凭证ID读出Material_Name,batchID,Inventorystate,stocktakingcount,SKU,ZC 
       /// </summary>
       /// <param name="documentID"></param>
       /// <returns></returns>
        public DataTable loadTable(string documentID)
        {

            StringBuilder strbld = new StringBuilder("select Material_ID,Batch_ID,Inventory_State, ")
                .Append("Stocktaking_count,SKU,Evaluate_Type, Material_Group,Inventory_Type from stocktakingDocument where stocktakingDocument_ID=")
                .Append("@sdid and Stocktaking_count is null");
            SqlParameter sqlpara = new SqlParameter("@sdid", SqlDbType.VarChar);
            sqlpara.Value = documentID;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            int n = dt.Rows.Count;
            DataTable dt2 = new DataTable("dt2");
            if (n > 0)
            {
                dt2.Columns.Add("物料编码", typeof(string));
                dt2.Columns.Add("物料名", typeof(string));
                dt2.Columns.Add("批次号", typeof(string));
                dt2.Columns.Add("库存状态", typeof(string));
                dt2.Columns.Add("评估类型", typeof(string));
                dt2.Columns.Add("物料组", typeof(string));
                dt2.Columns.Add("库存类型", typeof(string));
                dt2.Columns.Add("盘点数量", typeof(double));
                dt2.Columns.Add("SKU", typeof(string));
                dt2.Columns.Add("ZC", typeof(bool));

                for (int i = 0; i < n; i++)
                {
                    StringBuilder strbld1 = new StringBuilder("select Material_Name from Material where Material_ID=")
                        .Append("@mid");
                    SqlParameter sqlpara1 = new SqlParameter("@mid", SqlDbType.VarChar);
                    sqlpara1.Value = dt.Rows[i][0];
                    DataTable dt1 = DBHelper.ExecuteQueryDT(strbld1.ToString(), sqlpara1);

                    DataRow dr = dt2.NewRow();
                    dr["物料编码"] = dt.Rows[i][0].ToString();
                    dr["物料名"] = dt1.Rows[0][0].ToString();
                    dr["批次号"] = dt.Rows[i][1].ToString();
                    dr["库存状态"] = dt.Rows[i][2].ToString();
                    dr["盘点数量"] = DBNull.Value;
                    dr["SKU"] = dt.Rows[i][4].ToString(); ;
                    dr["ZC"] = false;
                    dr["评估类型"] = dt.Rows[i][5].ToString();
                    dr["物料组"] = dt.Rows[i][6].ToString();
                    dr["库存类型"] = dt.Rows[i][7].ToString();
                    
                    dt2.Rows.Add(dr);

                }
            }
            return dt2;
        }

        /// <summary>
        /// 根据凭证ID读出工厂、库存地ID
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public DataTable selectbasicInfo(string documentID)
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID,Stock_ID from stocktakingDocument where stocktakingDocument_ID=")
                .Append("@sdid");
            SqlParameter sqlpara = new SqlParameter("@sdid",SqlDbType.VarChar);
            sqlpara.Value = documentID;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);
            return dt;
 
        }
        /// <summary>
        /// 判断凭证状态
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public string judgeState(string documentid)
        {
            StringBuilder strbld = new StringBuilder("select count(stocktakingDocument_ID) from stocktakingDocument where")
                .Append(" stocktakingDocument_ID = @stdid");
            SqlParameter sqlpara = new SqlParameter("@stdid", SqlDbType.VarChar);
            sqlpara.Value = documentid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);

            StringBuilder strbld1 = new StringBuilder("select count(stocktakingDocument_ID) from stocktakingDocument where")
                .Append(" stocktakingDocument_ID = @stdid and Item_State = '仅记值' or Item_State = '已过账'");
            SqlParameter sqlpara1 = new SqlParameter("@stdid", SqlDbType.VarChar);
            sqlpara1.Value = documentid;
            DataTable dt1 = DBHelper.ExecuteQueryDT(strbld1.ToString(), sqlpara1);

            if (Convert.ToUInt32(dt.Rows[0][0].ToString()) == Convert.ToUInt32(dt1.Rows[0][0].ToString()))
            {
                return "完成盘点";
            }
            else
                return "部分盘点";

        }


        /// <summary>
        /// 用户输入盘点结果，点击确定后保存于数据库中
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool inputcount(DataTable dt)
        {
            int n = dt.Rows.Count;
            if (n > 0)
            {
                for (int i = 0; i < n; i++)
                {
                    if (Convert.ToBoolean(dt.Rows[i][7].ToString()) == true || dt.Rows[i][5].ToString() != "")
                    {
                        StringBuilder strbld0 = new StringBuilder("select Stocktaking_count from stocktakingDocument")
                             .Append(" where stocktakingDocument_ID=@sdid and Material_ID=@mid and batch_ID=@bid and Inventory_State=@is")
                            .Append(" and SKU= @sku ");

                        SqlParameter[] sqlpara0 = new SqlParameter[]
                        {
                            new SqlParameter("@sdid",SqlDbType.VarChar),
                            new SqlParameter("@mid",SqlDbType.VarChar),
                            new SqlParameter("@bid",SqlDbType.VarChar),
                            new SqlParameter("@is",SqlDbType.VarChar),
                            new SqlParameter("@sku",SqlDbType.VarChar),
                        };

                        sqlpara0[0].Value = dt.Rows[i][0].ToString();
                        sqlpara0[1].Value = dt.Rows[i][2].ToString();
                        sqlpara0[2].Value = dt.Rows[i][3].ToString();
                        sqlpara0[3].Value = dt.Rows[i][4].ToString();
                        sqlpara0[4].Value = dt.Rows[i][6].ToString();

                        bool rechecked;
                        DataTable dt0 = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara0);
                        if (dt0.Rows[0][0].ToString() == "")
                        {
                            rechecked = false;
                        }
                        else
                            rechecked = true;

                       

                        StringBuilder strbld = new StringBuilder("update stocktakingDocument set Stocktaking_count = @stc,")
                            .Append(" Stocktaking_Date = @std, CountDifference = StockCount-@difr,State='部分盘点',Account_Year = @ay,rechecked=@rck")
                            .Append(" ,Item_State = @ist")
                            .Append(" where stocktakingDocument_ID=@sdid and Material_ID=@mid and batch_ID=@bid and Inventory_State=@is")
                            .Append(" and SKU= @sku and Evaluate_Type =@et and Material_Group=@mg and Inventory_Type=@it");
                        SqlParameter[] sqlpara = new SqlParameter[]
                        {
                            new SqlParameter("@stc",SqlDbType.Float),
                            new SqlParameter("@std",SqlDbType.Date),
                            new SqlParameter("@difr",SqlDbType.Float),
                            new SqlParameter("@sdid",SqlDbType.VarChar),
                            new SqlParameter("@mid",SqlDbType.VarChar),
                            new SqlParameter("@bid",SqlDbType.VarChar),
                            new SqlParameter("@is",SqlDbType.VarChar),
                            new SqlParameter("@sku",SqlDbType.VarChar),
                            new SqlParameter("@ay",SqlDbType.VarChar),
                            new SqlParameter("@rck",SqlDbType.Bit),
                            new SqlParameter("@ist",SqlDbType.VarChar),
                            new SqlParameter("@et",SqlDbType.VarChar),
                            new SqlParameter("@mg",SqlDbType.VarChar),
                            new SqlParameter("@it",SqlDbType.VarChar),
                        };

                        if (Convert.ToBoolean(dt.Rows[i][7].ToString()) == true)
                        {
                            sqlpara[0].Value = 0;
                        }
                        else
                        {
                            sqlpara[0].Value = Convert.ToDouble(dt.Rows[i][5].ToString());
                        }
                        sqlpara[1].Value = Convert.ToDateTime(dt.Rows[i][1].ToString());
                        sqlpara[2].Value = Convert.ToDouble(dt.Rows[i][5].ToString());
                        sqlpara[3].Value = dt.Rows[i][0].ToString();
                        sqlpara[4].Value = dt.Rows[i][2].ToString();
                        sqlpara[5].Value = dt.Rows[i][3].ToString();
                        sqlpara[6].Value = dt.Rows[i][4].ToString();
                        sqlpara[7].Value = dt.Rows[i][6].ToString();
                        sqlpara[8].Value = dt.Rows[i][8].ToString();
                        sqlpara[9].Value = rechecked;
                        sqlpara[10].Value = "仅记值";
                        sqlpara[11].Value = dt.Rows[i][9].ToString();
                        sqlpara[12].Value = dt.Rows[i][10].ToString();
                        sqlpara[13].Value = dt.Rows[i][11].ToString();

                        DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);


                        StringBuilder strbld2 = new StringBuilder("update stocktakingDocument set State= @s")
                            .Append(" where stocktakingDocument_ID=@sdid");

                        SqlParameter[] sqlpara2 = new SqlParameter[]{
                            new SqlParameter("@s",SqlDbType.VarChar),
                            new SqlParameter("@sdid",SqlDbType.VarChar),
                        };

                        string state = this.judgeState(dt.Rows[i][0].ToString());
                        sqlpara2[0].Value = state;
                        sqlpara2[1].Value = dt.Rows[i][0].ToString();

                        DBHelper.ExecuteNonQuery(strbld2.ToString(), sqlpara2);
                    }
                }
                return true;
            }
            else
                return false;
            
        }
    }
}
