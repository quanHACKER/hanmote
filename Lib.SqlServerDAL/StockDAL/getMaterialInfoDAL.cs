﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class getMaterialInfoDAL:getMaterialInfoIDAL
    {
        /// <summary>
        /// 由物料编码获得物料名
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialName(string materialid)
        {
            StringBuilder strbld = new StringBuilder("select  Material_Name from Material where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);
            string materialname = dt.Rows[0][0].ToString();
            return materialname;
        }

       /// <summary>
       /// 由物料编号获得物料类型
       /// </summary>
       /// <param name="materialid"></param>
       /// <returns></returns>
        public string getMaterialType(string materialid)
        {
            StringBuilder strbld = new StringBuilder("select  Material_Type from Material where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            string materialtype = dt.Rows[0][0].ToString();
            return materialtype;
        }

        /// <summary>
        /// 由物料编号获得物料单位
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMeasurement(string materialid)
        {
            StringBuilder strbld = new StringBuilder("select  Measurement from Material where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            string measurement = dt.Rows[0][0].ToString();
            return measurement;
        }

        /// <summary>
        /// 由物料编号获得物料组
        /// </summary>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public string getMaterialGroup(string materialid)
        {
            StringBuilder strbld = new StringBuilder("select  Material_Group from Material where Material_ID = @mid");
            SqlParameter sqlpara = new SqlParameter("@mid", SqlDbType.VarChar);
            sqlpara.Value = materialid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            string materialgroup = dt.Rows[0][0].ToString();
            return materialgroup;
        }


        /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable getMaterialID()
        {
            StringBuilder strbld = new StringBuilder("select Material_ID from Material group by Material_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;
        }

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        public DataTable getFactoryID()
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID from Factory group by Factory_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;
 
        }

        /// <summary>
        /// 获取所有库存地ID
        /// </summary>
        /// <returns></returns>
        public DataTable getStockID()
        {
            StringBuilder strbld = new StringBuilder("select Stock_ID from Stock group by Stock_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT; 
        }

        /// <summary>
        /// 获取所有库存盘点凭证号  LoopCheckDocument
        /// </summary>
        /// <returns></returns>
        public DataTable getStocktakingDocuid()
        {
            StringBuilder strbld = new StringBuilder("select stocktakingDocument_ID from stocktakingDocument group by stocktakingDocument_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT; 
        }
        /// <summary>
        /// 获取所有库存盘点凭证号(循环盘点)  
        /// </summary>
        /// <returns></returns>
        public DataTable getloopStocktakingDocuid()
        {
            StringBuilder strbld = new StringBuilder("select  Document_ID from LoopCheckDocument group by  Document_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;
        }

        /// <summary>
        /// 获取全部批次号
        /// </summary>
        /// <returns></returns>
        public DataTable getbatchid()
        {
            StringBuilder strbld = new StringBuilder("select  Batch_ID from Batch group by  Batch_ID");
            DataTable DT = DBHelper.ExecuteQueryDT(strbld.ToString());
            return DT;
        }
        /// <summary>
        /// 获取质检结果
        /// </summary>
        /// <returns></returns>
        public DataTable getCheckResult()
        {
            StringBuilder strbld = new StringBuilder("select Check_Result from Check_Result ");

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;
        }

        /// <summary>
        /// 获取质检分数
        /// </summary>
        /// <returns></returns>
        public DataTable getCheckScore(string checkresult)
        {
            StringBuilder strbld = new StringBuilder("select Check_Score from Check_Result where Check_Result = @cr");
            SqlParameter sqlpara = new SqlParameter("@cr", SqlDbType.VarChar);
            sqlpara.Value = checkresult;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return dt;
        }
    }
}
