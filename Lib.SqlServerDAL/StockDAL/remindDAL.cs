﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;


namespace Lib.SqlServerDAL.StockDAL
{
    public class remindDAL:RemindIDAL
    {
        
        /// <summary>
        /// 将待设置的物料选取出来
        /// </summary>
        /// <returns></returns>
        public DataTable genTable()
        {
            DataTable dt;
            StringBuilder strbld = new StringBuilder("select Factory_ID as 工厂号, Stock_ID as 仓库号,")
                .Append(" Material_ID as 物料编号, batch_ID as 批次号, Maturity_Date as 到期日, Preremindday as 提前天数 from remind where Preremindday is NULL");
            dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;
        }

        //写入用户设置的提前提醒天数，并对到期提醒时间进行计算
        public void fillPreDay(string factoryID, string stockID,string materialID,string batchID,int days)
        {
            StringBuilder strbld1 = new StringBuilder("select Maturity_Date from remind where  Factory_ID=@fid")
              .Append(" and Stock_ID=@sid and Material_ID=@mid and batch_ID=@bid");
            SqlParameter[] sqlpara1 = new SqlParameter[]
            {
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@rd",SqlDbType.DateTime),
                
            };

            sqlpara1[0].Value = factoryID;
            sqlpara1[1].Value = stockID;
            sqlpara1[2].Value = materialID;
            sqlpara1[3].Value = batchID;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld1.ToString(), sqlpara1);

            StringBuilder strbld = new StringBuilder("update remind set Preremindday=@d,Handled=0, Remind_Date=@rd where  Factory_ID=@fid")
                .Append(" and Stock_ID=@sid and Material_ID=@mid and batch_ID=@bid");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@d",SqlDbType.Int),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@rd",SqlDbType.DateTime),
                
            };

            sqlpara[0].Value = days;
            sqlpara[1].Value = factoryID;
            sqlpara[2].Value = stockID;
            sqlpara[3].Value = materialID;
            sqlpara[4].Value = batchID;
            sqlpara[5].Value = Convert.ToDateTime(dt.Rows[0][0].ToString()).AddDays(-days);
            

            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);
          
        }

        //检查是否有要提醒的物料，若有，写入提醒表中
        public DataTable reminder()
        {
            DataTable dt = null;
            StringBuilder strbld = new StringBuilder("select Handled as 选定, Factory_ID as 工厂号, Stock_ID as 仓库号,")
                .Append(" Material_ID as 物料编号, batch_ID as 批次号, Maturity_Date as 到期日,ProlongDays as 延长天数 from remind")
                .Append(" where Remind_Date <= @rd and Handled = 0");

            SqlParameter sqlpara = new SqlParameter("@rd", SqlDbType.Date);
            sqlpara.Value = DateTime.Now.Date;

            dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);


                if (dt != null)
                    return dt;
                else
                    return null;
        }


        /// <summary>
        /// 处理到期物料
        /// </summary>
        /// <param name="factoryID"></param>
        /// <param name="stockID"></param>
        /// <param name="materialID"></param>
        /// <param name="batchID"></param>
        public void handle(string factoryID, string stockID, string materialID, string batchID,string prolongdays)
        {
            if (prolongdays == "")
            {
                StringBuilder strbld = new StringBuilder("update remind set Handled=1 where Factory_ID=@fid")
                  .Append(" and Stock_ID=@sid and Material_ID=@mid and batch_ID=@bid and Handled=0");
                SqlParameter[] sqlpara1 = new SqlParameter[]
            {
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                
            };
                sqlpara1[0].Value = factoryID;
                sqlpara1[1].Value = stockID;
                sqlpara1[2].Value = materialID;
                sqlpara1[3].Value = batchID;

                DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara1);
            }
            else
            {
                StringBuilder strbld = new StringBuilder("update remind set Remind_Date= dateadd(DD,@d,Remind_Date), ProlongDays=@d where Factory_ID=@fid")
                  .Append(" and Stock_ID=@sid and Material_ID=@mid and batch_ID=@bid and Handled=0");
                SqlParameter[] sqlpara1 = new SqlParameter[]
            {
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@d",SqlDbType.Int),
                
            };
                sqlpara1[0].Value = factoryID;
                sqlpara1[1].Value = stockID;
                sqlpara1[2].Value = materialID;
                sqlpara1[3].Value = batchID;
                sqlpara1[4].Value = Convert.ToInt32(prolongdays);

                DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara1);
            }
        }

        /// <summary>
        ///选出当前物料及入库日期
        /// </summary>
        /// <returns></returns>
        public DataTable accountManage()
        {
            StringBuilder strbld = new StringBuilder("select AccountTimeManagement.Factory_ID,AccountTimeManagement.Stock_ID,AccountTimeManagement.Material_ID,StockInDate ")
                .Append(" from AccountTimeManagement,Inventory")
                .Append(" where AccountTimeManagement.Material_ID=Inventory.Material_ID AND AccountTimeManagement.Factory_ID=Inventory.Factory_ID")
                .Append(" and AccountTimeManagement.Stock_ID=Inventory.Stock_ID AND Inventory.Count >=0")
                .Append(" group by  AccountTimeManagement.Factory_ID,AccountTimeManagement.Stock_ID,AccountTimeManagement.Material_ID,AccountTimeManagement.StockInDate");

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;
        }
    }
}
