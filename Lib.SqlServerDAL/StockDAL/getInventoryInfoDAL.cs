﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class getInventoryInfoDAL
    {

        public DataTable selectStockInfo(string factoryid,string stockid,string materialid)
        {
            DataTable dt = null;

            if (factoryid != "" && stockid !="" && materialid =="")
            {
                StringBuilder strbld0 = new StringBuilder("select Factory_ID as 工厂号,Stock_ID as 库存地,  Material_ID as 物料编码,sum(curCount) as 当前数量,SUM(curSumprice) as 当前金额,SUM(History_Count) as 历史数量,SUM(History_SumPrice) as 历史金额 from ")
    .Append(" (select * from (select * ,ROW_NUMBER() over(partition by Factory_ID,Stock_ID, Material_ID,Inventory_Type ,")
    .Append(" Batch_ID ,Inventory_State ,Evaluate_Type, Material_Group,SKU order by Move_Date desc) as rowNum from InventoryInfo where Factory_ID = @fid and Stock_ID = @sid)")
    .Append(" ranked where ranked.rowNum <= 1 ) t   group by Factory_ID,Stock_ID, Material_ID ");

                SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                };

                sqlpara[0].Value = factoryid;
                sqlpara[1].Value = stockid;
                dt = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara);
                return dt;
 
            }


            else if (factoryid != "" && stockid == "" && materialid == "")
            {
                StringBuilder strbld0 = new StringBuilder("select Factory_ID as 工厂号,Stock_ID as 库存地,  Material_ID as 物料编码,sum(curCount) as 当前数量,SUM(curSumprice) as 当前金额,SUM(History_Count) as 历史数量,SUM(History_SumPrice) as 历史金额 from ")
    .Append(" (select * from (select * ,ROW_NUMBER() over(partition by Factory_ID,Stock_ID, Material_ID,Inventory_Type ,")
    .Append(" Batch_ID ,Inventory_State ,Evaluate_Type, Material_Group,SKU order by Move_Date desc) as rowNum from InventoryInfo where Factory_ID = @fid )")
    .Append(" ranked where ranked.rowNum <= 1 ) t   group by Factory_ID,Stock_ID, Material_ID ");

                SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                
                };

                sqlpara[0].Value = factoryid;
                dt = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara);
                return dt;
               
            }


            else if (factoryid == "" && stockid == "" && materialid != "")
            {
                StringBuilder strbld0 = new StringBuilder("select Factory_ID as 工厂号,Stock_ID as 库存地,  Material_ID as 物料编码,sum(curCount) as 当前数量,SUM(curSumprice) as 当前金额,SUM(History_Count) as 历史数量,SUM(History_SumPrice) as 历史金额 from ")
    .Append(" (select * from (select * ,ROW_NUMBER() over(partition by Factory_ID,Stock_ID, Material_ID,Inventory_Type ,")
    .Append(" Batch_ID ,Inventory_State ,Evaluate_Type, Material_Group,SKU order by Move_Date desc) as rowNum from InventoryInfo where Material_ID = @fid )")
    .Append(" ranked where ranked.rowNum <= 1 ) t   group by Factory_ID,Stock_ID, Material_ID ");

                SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@fid",SqlDbType.VarChar),
                
                };

                sqlpara[0].Value = materialid;

                dt = DBHelper.ExecuteQueryDT(strbld0.ToString(), sqlpara);
                return dt;

            }

            return dt;
        }

    }
}
