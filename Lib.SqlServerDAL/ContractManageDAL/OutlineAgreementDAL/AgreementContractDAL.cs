﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.OutlineAgreement;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL
{
    public class AgreementContractDAL : AgreementContractIDAL
    {
        /// <summary>
        /// 根据ID和Version获取Agreement_Contract，为空返回null
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public Agreement_Contract getAgreementContractByID(string agreementContractID,
            string agreementContractVersion) {
                
            Agreement_Contract agreementContract = new Agreement_Contract();
            StringBuilder strBui = new StringBuilder("select * from Agreement_Contract where ID = ")
                .Append("@ID and Version = @Version");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@ID", SqlDbType.VarChar),
                new SqlParameter("@Version", SqlDbType.VarChar)
            };
            sqlParas[0].Value = agreementContractID;
            sqlParas[1].Value = agreementContractVersion;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            if (dt.Rows.Count <= 0)
                return null;

            DataRow dr = dt.Rows[0];
            agreementContract.Agreement_Contract_ID = dr["ID"].ToString();
            agreementContract.Agreement_Contract_Version = dr["Version"].ToString();
            agreementContract.Supplier_ID = dr["Supplier_ID"].ToString();
            agreementContract.Agreement_Contract_Type = dr["Type"].ToString();
            agreementContract.Purchase_Group = dr["Purchase_Group"].ToString();
            agreementContract.Purchase_Organization = dr["Purchase_Org"].ToString();
            agreementContract.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());
            agreementContract.Creater_ID = dr["Creater_ID"].ToString();
            agreementContract.Status = dr["Status"].ToString();
            agreementContract.Factory = dr["Factory_ID"].ToString();
            agreementContract.Storage_Location = dr["Stock_ID"].ToString();
            agreementContract.Begin_Time = Convert.ToDateTime(dr["Begin_Time"].ToString());
            agreementContract.End_Time = Convert.ToDateTime(dr["End_Time"].ToString());
            agreementContract.IT_Term_Code = dr["Trade_Clause"].ToString();
            agreementContract.IT_Term_Text = dr["Trade_Text"].ToString();
            agreementContract.Payment_Type = dr["Payment_Clause"].ToString();
            agreementContract.Days1 = Convert.ToInt32(dr["Days1"].ToString());
            agreementContract.Discount_Rate1 = Convert.ToDouble(dr["Discount_Rate1"].ToString());
            agreementContract.Days2 = Convert.ToInt32(dr["Days2"].ToString());
            agreementContract.Discount_Rate2 = Convert.ToDouble(dr["Discount_Rate2"].ToString());
            agreementContract.Days3 = Convert.ToInt32(dr["Days3"].ToString());
            agreementContract.Target_Value = Convert.ToDouble(dr["Target_Value"].ToString());
            agreementContract.Currency_Type = dr["Currency_Type"].ToString();
            agreementContract.Exchange_Rate = Convert.ToDouble(dr["Exchange_Rate"].ToString());
            agreementContract.IsValid = Convert.IsDBNull(dr["Valid"])?1:Convert.ToInt32(dr["Valid"].ToString());
            agreementContract.Finished = Convert.IsDBNull(dr["Finished"])?0.00:Convert.ToDouble(dr["Finished"].ToString());

            return agreementContract;
        }

        /// <summary>
        /// 根据指定的条件获取Agreement_Contract
        /// </summary>
        /// <param name="conditions">指定的条件</param>
        /// <returns>符合条件的列表</returns>
        public List<Agreement_Contract> getAgreementContractByCondition(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime)
        {
            List<Agreement_Contract> contractList = new List<Agreement_Contract>();

            StringBuilder strBui = new StringBuilder("select * from Agreement_Contract where Create_Time >= @Begin_Time and ")
                .Append("Create_Time <= @End_Time ");
            
            //因为变量数目不确定，采用list
            //List<SqlParameter> sqlParasList = new List<SqlParameter>();
            //SqlParameter sqlPara = new SqlParameter("@Begin_Time", SqlDbType.DateTime);
            //sqlPara.Value = beginTime;
            //sqlParasList.Add(sqlPara);
            //sqlPara = new SqlParameter("@End_Time", SqlDbType.DateTime);
            //sqlPara.Value = endTime;
            //sqlParasList.Add(sqlPara);
            
            foreach (string key in conditions.Keys) {
                string value = (string)conditions[key];
                if (value != null && !value.Equals("")) {
                    strBui.Append(" and ").Append(key).Append(" = @")
                        .Append(key);
                    //sqlPara = new SqlParameter("@" + key, SqlDbType.VarChar);
                    //sqlPara.Value = value;
                    //sqlParasList.Add(sqlPara);
                }
            }

            //这里只是定义变量，可以定义前面的sql语句用不到的变量
            //所以这里声明所有的变量
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Type", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar)
            };

            sqlParas[0].Value = beginTime;
            sqlParas[1].Value = endTime;
            sqlParas[2].Value = (string)conditions["Agreement_Contract_ID"];
            sqlParas[3].Value = (string)conditions["Supplier_ID"];
            sqlParas[4].Value = (string)conditions["Agreement_Contract_Type"];
            sqlParas[5].Value = (string)conditions["Status"];
            
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            foreach (DataRow dr in dt.Rows) {
                Agreement_Contract agreementContract = new Agreement_Contract();
                agreementContract.Agreement_Contract_ID = dr["ID"].ToString();
                agreementContract.Agreement_Contract_Version = dr["Version"].ToString();
                agreementContract.Supplier_ID = dr["Supplier_ID"].ToString();
                agreementContract.Agreement_Contract_Type = dr["Type"].ToString();
                agreementContract.Purchase_Group = dr["Purchase_Group"].ToString();
                agreementContract.Purchase_Organization = dr["Purchase_Org"].ToString();
                agreementContract.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());
                agreementContract.Creater_ID = dr["Creater_ID"].ToString();
                agreementContract.Status = dr["Status"].ToString();
                agreementContract.Factory = dr["Factory_ID"].ToString();
                agreementContract.Storage_Location = dr["Stock_ID"].ToString();
                agreementContract.Begin_Time = Convert.ToDateTime(dr["Begin_Time"].ToString());
                agreementContract.End_Time = Convert.ToDateTime(dr["End_Time"].ToString());
                agreementContract.IT_Term_Code = dr["Trade_Clause"].ToString();
                agreementContract.IT_Term_Text = dr["Trade_Text"].ToString();
                agreementContract.Payment_Type = dr["Payment_Clause"].ToString();
                agreementContract.Days1 = Convert.ToInt32(dr["Days1"].ToString());
                agreementContract.Discount_Rate1 = Convert.ToDouble(dr["Discount_Rate1"].ToString());
                agreementContract.Days2 = Convert.ToInt32(dr["Days2"].ToString());
                agreementContract.Discount_Rate2 = Convert.ToDouble(dr["Discount_Rate2"].ToString());
                agreementContract.Days3 = Convert.ToInt32(dr["Days3"].ToString());
                agreementContract.Target_Value = Convert.ToDouble(dr["Target_Value"].ToString());
                agreementContract.Currency_Type = dr["Currency_Type"].ToString();
                agreementContract.Exchange_Rate = Convert.ToDouble(dr["Exchange_Rate"].ToString());
                agreementContract.IsValid = Convert.IsDBNull(dr["Valid"]) ? 1 : Convert.ToInt32(dr["Valid"].ToString());
                agreementContract.Finished = Convert.IsDBNull(dr["Finished"]) ? 0.00 : Convert.ToDouble(dr["Finished"].ToString());

                contractList.Add(agreementContract);
            }           

            return contractList;
        }

        /// <summary>
        /// 添加协议合同
        /// </summary>
        /// <param name="agreementContract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addAgreementContract(Agreement_Contract agreementContract, List<Agreement_Contract_Item> itemList) {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            #region 添加协议合同基本数据

            StringBuilder strBui = new StringBuilder("insert into Agreement_Contract (")
                    .Append("ID, Version, Supplier_ID, ")
                    .Append("Type, Purchase_Group, Purchase_Org, Create_Time, ")
                    .Append("Creater_ID, Status, Factory_ID, Stock_ID, Begin_Time, End_Time, ")
                    .Append("Trade_Clause, Trade_Text, Payment_Clause, Days1, Discount_Rate1, ")
                    .Append("Days2, Discount_Rate2, Days3, Target_Value, Currency_Type, Exchange_Rate,")
                    .Append("Valid,Finished")
                    .Append(") values(")
                    .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Supplier_ID, ")
                    .Append("@Agreement_Contract_Type, @Purchase_Group, @Purchase_Organization, @Create_Time, ")
                    .Append("@Creater_ID, @Status, @Factory, @Storage_Location, @Begin_Time, @End_Time, ")
                    .Append("@IT_Term_Code, @IT_Term_Text, @Payment_Type, @Days1, @Discount_Rate1, ")
                    .Append("@Days2, @Discount_Rate2, @Days3, @Target_Value, @Currency_Type, @Exchange_Rate,")
                    .Append("@IsValid,@Finished")
                    .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                    new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar),
                    new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Type", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                    new SqlParameter("@Create_Time", SqlDbType.DateTime),
                    new SqlParameter("@Creater_ID", SqlDbType.VarChar),
                    new SqlParameter("@Status", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                    new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                    new SqlParameter("@End_Time", SqlDbType.DateTime),
                    new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                    new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                    new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                    new SqlParameter("@Days1", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                    new SqlParameter("@Days2", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                    new SqlParameter("@Days3", SqlDbType.Int),
                    new SqlParameter("@Target_Value", SqlDbType.Decimal),
                    new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                    new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                    new SqlParameter("@IsValid",SqlDbType.Int),
                    new SqlParameter("@Finished",SqlDbType.Decimal)
            };

            sqlParas[0].Value = agreementContract.Agreement_Contract_ID;
            sqlParas[1].Value = agreementContract.Agreement_Contract_Version;
            sqlParas[2].Value = agreementContract.Supplier_ID;
            sqlParas[3].Value = agreementContract.Agreement_Contract_Type;
            sqlParas[4].Value = agreementContract.Purchase_Group;
            sqlParas[5].Value = agreementContract.Purchase_Organization;
            sqlParas[6].Value = agreementContract.Create_Time;
            sqlParas[7].Value = agreementContract.Creater_ID;
            sqlParas[8].Value = agreementContract.Status;
            sqlParas[9].Value = agreementContract.Factory;
            sqlParas[10].Value = agreementContract.Storage_Location;
            sqlParas[11].Value = agreementContract.Begin_Time;
            sqlParas[12].Value = agreementContract.End_Time;
            sqlParas[13].Value = agreementContract.IT_Term_Code;
            sqlParas[14].Value = agreementContract.IT_Term_Text;
            sqlParas[15].Value = agreementContract.Payment_Type;
            sqlParas[16].Value = agreementContract.Days1;
            sqlParas[17].Value = agreementContract.Discount_Rate1;
            sqlParas[18].Value = agreementContract.Days2;
            sqlParas[19].Value = agreementContract.Discount_Rate2;
            sqlParas[20].Value = agreementContract.Days3;
            sqlParas[21].Value = agreementContract.Target_Value;
            sqlParas[22].Value = agreementContract.Currency_Type;
            sqlParas[23].Value = agreementContract.Exchange_Rate;
            sqlParas[24].Value = 1;
            sqlParas[25].Value = 0.00;
            
            //加入到List中
            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            #endregion

            #region 协议合同项

            foreach (Agreement_Contract_Item item in itemList) {
                StringBuilder strBui2 = new StringBuilder("insert into Agreement_Contract_Item ( ")
                .Append("Agreement_Contract_ID, Agreement_Contract_Version, Item_Num, I, A, ")
                .Append("Material_ID, Material_Name, Demand_Count, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory_ID, Finished_Quantity, Stock_ID, Price_Determin_ID ")
                .Append(") values( ")
                .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory, @Finish, @Stock, @Price_Determin)");

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                    new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar),
                    new SqlParameter("@Item_Num", SqlDbType.Int),
                    new SqlParameter("@I", SqlDbType.VarChar),
                    new SqlParameter("@A", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Short_Text", SqlDbType.VarChar),
                    new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                    new SqlParameter("@OUn", SqlDbType.VarChar),
                    new SqlParameter("@Net_Price", SqlDbType.Decimal),
                    new SqlParameter("@Every", SqlDbType.Decimal),
                    new SqlParameter("@OPU", SqlDbType.VarChar),
                    new SqlParameter("@Material_Group", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Finish",SqlDbType.Decimal),
                    new SqlParameter("@Stock",SqlDbType.VarChar),
                    new SqlParameter("@Price_Determin",SqlDbType.VarChar)
                };

                sqlParas2[0].Value = item.Agreement_Contract_ID;
                sqlParas2[1].Value = item.Agreement_Contract_Version;
                sqlParas2[2].Value = item.Item_Num;
                sqlParas2[3].Value = item.I;
                sqlParas2[4].Value = item.A;
                sqlParas2[5].Value = item.Material_ID;
                sqlParas2[6].Value = item.Short_Text;
                sqlParas2[7].Value = item.Target_Quantity;
                sqlParas2[8].Value = item.OUn;
                sqlParas2[9].Value = item.Net_Price;
                sqlParas2[10].Value = item.Every;
                sqlParas2[11].Value = item.OPU;
                sqlParas2[12].Value = item.Material_Group;
                sqlParas2[13].Value = item.Factory;
                sqlParas2[14].Value = item.FinishedQuantity;
                sqlParas2[15].Value = item.Stock_ID;
                sqlParas2[16].Value = item.Price_Determin_ID;

                //加入到List
                sqlList.Add(strBui2.ToString());
                sqlParamsList.Add(sqlParas2);
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 更新全部协议合同信息
        /// </summary>
        /// <param name="agreementContract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateAgreementContract(Agreement_Contract agreementContract, List<Agreement_Contract_Item> itemList)
        {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            # region 更新协议合同基本数据

            StringBuilder strBui = new StringBuilder("update Agreement_Contract set ")
                .Append("Purchase_Group = @Purchase_Group, Purchase_Org = @Purchase_Organization, ")
                .Append("Status = @Status, Factory_ID = @Factory, Stock_ID = @Storage_Location, ")
                .Append("Begin_Time = @Begin_Time, End_Time = @End_Time, Trade_Clause = @IT_Term_Code, ")
                .Append("Trade_Text = @IT_Term_Text, Payment_Clause = @Payment_Type, Days1 = @Days1, ")
                .Append("Discount_Rate1 = @Discount_Rate1, Days2 = @Days2, Discount_Rate2 = @Discount_Rate2, ")
                .Append("Days3 = @Days3, Target_Value = @Target_Value, Currency_Type = @Currency_Type, ")
                .Append("Exchange_Rate = @Exchange_Rate ")
                .Append("where ID = @Agreement_Contract_ID and Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Days1", SqlDbType.Int),
                new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                new SqlParameter("@Days2", SqlDbType.Int),
                new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                new SqlParameter("@Days3", SqlDbType.Int),
                new SqlParameter("@Target_Value", SqlDbType.Decimal),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas[0].Value = agreementContract.Purchase_Group;
            sqlParas[1].Value = agreementContract.Purchase_Organization;
            sqlParas[2].Value = agreementContract.Status;
            sqlParas[3].Value = agreementContract.Factory;
            sqlParas[4].Value = agreementContract.Storage_Location;
            sqlParas[5].Value = agreementContract.Begin_Time;
            sqlParas[6].Value = agreementContract.End_Time;
            sqlParas[7].Value = agreementContract.IT_Term_Code;
            sqlParas[8].Value = agreementContract.IT_Term_Text;
            sqlParas[9].Value = agreementContract.Payment_Type;
            sqlParas[10].Value = agreementContract.Days1;
            sqlParas[11].Value = agreementContract.Discount_Rate1;
            sqlParas[12].Value = agreementContract.Days2;
            sqlParas[13].Value = agreementContract.Discount_Rate2;
            sqlParas[14].Value = agreementContract.Days3;
            sqlParas[15].Value = agreementContract.Target_Value;
            sqlParas[16].Value = agreementContract.Currency_Type;
            sqlParas[17].Value = agreementContract.Exchange_Rate;
            sqlParas[18].Value = agreementContract.Agreement_Contract_ID;
            sqlParas[19].Value = agreementContract.Agreement_Contract_Version;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            # endregion

            # region 删除协议合同项

            StringBuilder strBui2 = new StringBuilder("delete from Agreement_Contract_Item ")
                .Append("where Agreement_Contract_ID = @Agreement_Contract_ID ")
                .Append("and Agreement_Contract_Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas2[0].Value = agreementContract.Agreement_Contract_ID;
            sqlParas2[1].Value = agreementContract.Agreement_Contract_Version;

            sqlList.Add(strBui2.ToString());
            sqlParamsList.Add(sqlParas2);

            # endregion

            # region 添加新的协议合同项

            foreach (Agreement_Contract_Item item in itemList) {
                StringBuilder strBui3 = new StringBuilder("insert into Agreement_Contract_Item ( ")
                    .Append("Agreement_Contract_ID, Agreement_Contract_Version, Item_Num, I, A, ")
                    .Append("Material_ID, Material_Name, Demand_Count, OUn, Net_Price, ")
                    .Append("Every, OPU, Material_Group, Factory_ID, Finished_Quantity,  Stock_ID, Price_Determin_ID")
                    .Append(") values( ")
                    .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Item_Num, @I, @A, ")
                    .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                    .Append("@Every, @OPU, @Material_Group, @Factory, @Finish, @Stock, @Price_Determin )");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                    new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar),
                    new SqlParameter("@Item_Num", SqlDbType.Int),
                    new SqlParameter("@I", SqlDbType.VarChar),
                    new SqlParameter("@A", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Short_Text", SqlDbType.VarChar),
                    new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                    new SqlParameter("@OUn", SqlDbType.VarChar),
                    new SqlParameter("@Net_Price", SqlDbType.Decimal),
                    new SqlParameter("@Every", SqlDbType.Decimal),
                    new SqlParameter("@OPU", SqlDbType.VarChar),
                    new SqlParameter("@Material_Group", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Finish",SqlDbType.Decimal),
                    new SqlParameter("@Stock",SqlDbType.VarChar),
                    new SqlParameter("@Price_Determin",SqlDbType.VarChar)
                };

                sqlParas3[0].Value = item.Agreement_Contract_ID;
                sqlParas3[1].Value = item.Agreement_Contract_Version;
                sqlParas3[2].Value = item.Item_Num;
                sqlParas3[3].Value = item.I;
                sqlParas3[4].Value = item.A;
                sqlParas3[5].Value = item.Material_ID;
                sqlParas3[6].Value = item.Short_Text;
                sqlParas3[7].Value = item.Target_Quantity;
                sqlParas3[8].Value = item.OUn;
                sqlParas3[9].Value = item.Net_Price;
                sqlParas3[10].Value = item.Every;
                sqlParas3[11].Value = item.OPU;
                sqlParas3[12].Value = item.Material_Group;
                sqlParas3[13].Value = item.Factory;
                sqlParas3[14].Value = item.FinishedQuantity;
                sqlParas3[15].Value = item.Stock_ID;
                sqlParas3[16].Value = item.Price_Determin_ID;

                sqlList.Add(strBui3.ToString());
                sqlParamsList.Add(sqlParas3);
            }

            # endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 更新合同抬头信息
        /// </summary>
        /// <param name="agreementContactID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <param name="agreementContract"></param>
        /// <returns></returns>
        public int updateAgreementContract(string agreementContactID,
            string agreementContractVersion, Agreement_Contract agreementContract)
        {

            StringBuilder strBui = new StringBuilder("update Agreement_Contract set ")
                .Append("Purchase_Group = @Purchase_Group, Purchase_Org = @Purchase_Organization, ")
                .Append("Status = @Status, Factory_ID = @Factory, Stock_ID = @Storage_Location, ")
                .Append("Begin_Time = @Begin_Time, End_Time = @End_Time, Trade_Clause = @IT_Term_Code, ")
                .Append("Trade_Text = @IT_Term_Text, Payment_Clause = @Payment_Type, Days1 = @Days1, ")
                .Append("Discount_Rate1 = @Discount_Rate1, Days2 = @Days2, Discount_Rate2 = @Discount_Rate2, ")
                .Append("Days3 = @Days3, Target_Value = @Target_Value, Currency_Type = @Currency_Type, ")
                .Append("Exchange_Rate = @Exchange_Rate ")
                .Append("where ID = @Agreement_Contract_ID and Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Days1", SqlDbType.Int),
                new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                new SqlParameter("@Days2", SqlDbType.Int),
                new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                new SqlParameter("@Days3", SqlDbType.Int),
                new SqlParameter("@Target_Value", SqlDbType.Decimal),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas[0].Value = agreementContract.Purchase_Group;
            sqlParas[1].Value = agreementContract.Purchase_Organization;
            sqlParas[2].Value = agreementContract.Status;
            sqlParas[3].Value = agreementContract.Factory;
            sqlParas[4].Value = agreementContract.Storage_Location;
            sqlParas[5].Value = agreementContract.Begin_Time;
            sqlParas[6].Value = agreementContract.End_Time;
            sqlParas[7].Value = agreementContract.IT_Term_Code;
            sqlParas[8].Value = agreementContract.IT_Term_Text;
            sqlParas[9].Value = agreementContract.Payment_Type;
            sqlParas[10].Value = agreementContract.Days1;
            sqlParas[11].Value = agreementContract.Discount_Rate1;
            sqlParas[12].Value = agreementContract.Days2;
            sqlParas[13].Value = agreementContract.Discount_Rate2;
            sqlParas[14].Value = agreementContract.Days3;
            sqlParas[15].Value = agreementContract.Target_Value;
            sqlParas[16].Value = agreementContract.Currency_Type;
            sqlParas[17].Value = agreementContract.Exchange_Rate;
            sqlParas[18].Value = agreementContract.Agreement_Contract_ID;
            sqlParas[19].Value = agreementContract.Agreement_Contract_Version;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 删除协议合同
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public int deleteAgreementContractByID(string agreementContractID,
            string agreementContractVersion) {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            # region 删除协议合同项

            StringBuilder strBui2 = new StringBuilder("delete from Agreement_Contract_Item ")
                .Append("where Agreement_Contract_ID = @Agreement_Contract_ID ")
                .Append("and Agreement_Contract_Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas2[0].Value = agreementContractID;
            sqlParas2[1].Value = agreementContractVersion;

            sqlList.Add(strBui2.ToString());
            sqlParamsList.Add(sqlParas2);

            # endregion

            # region 删除协议合同基本数据

            StringBuilder strBui = new StringBuilder("delete from Agreement_Contract ")
                .Append("where ID = @Agreement_Contract_ID and ")
                .Append("Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas[0].Value = agreementContractID;
            sqlParas[1].Value = agreementContractVersion;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            # endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 变更协议合同
        /// </summary>
        /// <param name="oldAgreementContract"></param>
        /// <param name="newAgreementContract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int changeAgreementContract(Agreement_Contract oldAgreementContract,
            Agreement_Contract newAgreementContract, List<Agreement_Contract_Item> itemList) {
                
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            # region 更新合同抬头信息

            StringBuilder strBui = new StringBuilder("update Agreement_Contract set ")
                .Append("Purchase_Group = @Purchase_Group, Purchase_Org = @Purchase_Organization, ")
                .Append("Status = @Status, Factory = @Factory, Stock_ID = @Storage_Location, ")
                .Append("Begin_Time = @Begin_Time, End_Time = @End_Time, Trade_Clause = @IT_Term_Code, ")
                .Append("Trade_Text = @IT_Term_Text, Payment_Clause = @Payment_Type, Days1 = @Days1, ")
                .Append("Discount_Rate1 = @Discount_Rate1, Days2 = @Days2, Discount_Rate2 = @Discount_Rate2, ")
                .Append("Days3 = @Days3, Target_Value = @Target_Value, Currency_Type = @Currency_Type, ")
                .Append("Exchange_Rate = @Exchange_Rate ")
                .Append("where ID = @Agreement_Contract_ID and Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Days1", SqlDbType.Int),
                new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                new SqlParameter("@Days2", SqlDbType.Int),
                new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                new SqlParameter("@Days3", SqlDbType.Int),
                new SqlParameter("@Target_Value", SqlDbType.Decimal),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.Int)
            };

            sqlParas[0].Value = oldAgreementContract.Purchase_Group;
            sqlParas[1].Value = oldAgreementContract.Purchase_Organization;
            sqlParas[2].Value = oldAgreementContract.Status;
            sqlParas[3].Value = oldAgreementContract.Factory;
            sqlParas[4].Value = oldAgreementContract.Storage_Location;
            sqlParas[5].Value = oldAgreementContract.Begin_Time;
            sqlParas[6].Value = oldAgreementContract.End_Time;
            sqlParas[7].Value = oldAgreementContract.IT_Term_Code;
            sqlParas[8].Value = oldAgreementContract.IT_Term_Text;
            sqlParas[9].Value = oldAgreementContract.Payment_Type;
            sqlParas[10].Value = oldAgreementContract.Days1;
            sqlParas[11].Value = oldAgreementContract.Discount_Rate1;
            sqlParas[12].Value = oldAgreementContract.Days2;
            sqlParas[13].Value = oldAgreementContract.Discount_Rate2;
            sqlParas[14].Value = oldAgreementContract.Days3;
            sqlParas[15].Value = oldAgreementContract.Target_Value;
            sqlParas[16].Value = oldAgreementContract.Currency_Type;
            sqlParas[17].Value = oldAgreementContract.Exchange_Rate;
            sqlParas[18].Value = oldAgreementContract.Agreement_Contract_ID;
            sqlParas[19].Value = oldAgreementContract.Agreement_Contract_Version;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            # endregion

            #region 添加协议合同基本数据

            StringBuilder strBui3 = new StringBuilder("insert into Agreement_Contract (")
                    .Append("ID, Version, Supplier_ID, ")
                    .Append("Type, Purchase_Group, Purchase_Org, Create_Time, ")
                    .Append("Creater_ID, Status, Factory_ID, Stock_ID, Begin_Time, End_Time, ")
                    .Append("Trade_Clause, Trade_Text, Payment_Clause, Days1, Discount_Rate1, ")
                    .Append("Days2, Discount_Rate2, Days3, Target_Value, Currency_Type, Exchange_Rate, Valid, Finished")
                    .Append(") values(")
                    .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Supplier_ID, ")
                    .Append("@Agreement_Contract_Type, @Purchase_Group, @Purchase_Organization, @Create_Time, ")
                    .Append("@Creater_ID, @Status, @Factory, @Storage_Location, @Begin_Time, @End_Time, ")
                    .Append("@IT_Term_Code, @IT_Term_Text, @Payment_Type, @Days1, @Discount_Rate1, ")
                    .Append("@Days2, @Discount_Rate2, @Days3, @Target_Value, @Currency_Type, @Exchange_Rate, @IsValid, @Finished")
                    .Append(")");
            SqlParameter[] sqlParas3 = new SqlParameter[]{
                    new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Version", SqlDbType.Int),
                    new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Type", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                    new SqlParameter("@Create_Time", SqlDbType.DateTime),
                    new SqlParameter("@Creater_ID", SqlDbType.VarChar),
                    new SqlParameter("@Status", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                    new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                    new SqlParameter("@End_Time", SqlDbType.DateTime),
                    new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                    new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                    new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                    new SqlParameter("@Days1", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                    new SqlParameter("@Days2", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                    new SqlParameter("@Days3", SqlDbType.Int),
                    new SqlParameter("@Target_Value", SqlDbType.Decimal),
                    new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                    new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                    new SqlParameter("@IsValid",SqlDbType.Int),
                    new SqlParameter("@Finished",SqlDbType.Decimal)
            };

            sqlParas3[0].Value = newAgreementContract.Agreement_Contract_ID;
            sqlParas3[1].Value = newAgreementContract.Agreement_Contract_Version;
            sqlParas3[2].Value = newAgreementContract.Supplier_ID;
            sqlParas3[3].Value = newAgreementContract.Agreement_Contract_Type;
            sqlParas3[4].Value = newAgreementContract.Purchase_Group;
            sqlParas3[5].Value = newAgreementContract.Purchase_Organization;
            sqlParas3[6].Value = newAgreementContract.Create_Time;
            sqlParas3[7].Value = newAgreementContract.Creater_ID;
            sqlParas3[8].Value = newAgreementContract.Status;
            sqlParas3[9].Value = newAgreementContract.Factory;
            sqlParas3[10].Value = newAgreementContract.Storage_Location;
            sqlParas3[11].Value = newAgreementContract.Begin_Time;
            sqlParas3[12].Value = newAgreementContract.End_Time;
            sqlParas3[13].Value = newAgreementContract.IT_Term_Code;
            sqlParas3[14].Value = newAgreementContract.IT_Term_Text;
            sqlParas3[15].Value = newAgreementContract.Payment_Type;
            sqlParas3[16].Value = newAgreementContract.Days1;
            sqlParas3[17].Value = newAgreementContract.Discount_Rate1;
            sqlParas3[18].Value = newAgreementContract.Days2;
            sqlParas3[19].Value = newAgreementContract.Discount_Rate2;
            sqlParas3[20].Value = newAgreementContract.Days3;
            sqlParas3[21].Value = newAgreementContract.Target_Value;
            sqlParas3[22].Value = newAgreementContract.Currency_Type;
            sqlParas3[23].Value = newAgreementContract.Exchange_Rate;
            sqlParas3[24].Value = 1;
            sqlParas3[25].Value = 0.00;

            //加入到List中
            sqlList.Add(strBui3.ToString());
            sqlParamsList.Add(sqlParas3);

            #endregion

            #region 协议合同项

            foreach (Agreement_Contract_Item item in itemList)
            {
                StringBuilder strBui2 = new StringBuilder("insert into Agreement_Contract_Item ( ")
                .Append("Agreement_Contract_ID, Agreement_Contract_Version, Item_Num, I, A, ")
                .Append("Material_ID, Material_Name, Demand_Count, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory_ID, Finished_Quantity, Stock_ID, Price_Determin_ID ")
                .Append(") values( ")
                .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory, @Finish, @Stock, @Price_Determin )");

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                    new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                    new SqlParameter("@Agreement_Contract_Version", SqlDbType.Int),
                    new SqlParameter("@Item_Num", SqlDbType.Int),
                    new SqlParameter("@I", SqlDbType.VarChar),
                    new SqlParameter("@A", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Short_Text", SqlDbType.VarChar),
                    new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                    new SqlParameter("@OUn", SqlDbType.VarChar),
                    new SqlParameter("@Net_Price", SqlDbType.Decimal),
                    new SqlParameter("@Every", SqlDbType.Decimal),
                    new SqlParameter("@OPU", SqlDbType.VarChar),
                    new SqlParameter("@Material_Group", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Finish",SqlDbType.Decimal),
                    new SqlParameter("@Stock",SqlDbType.VarChar),
                    new SqlParameter("@Price_Determin",SqlDbType.VarChar)
                };

                sqlParas2[0].Value = item.Agreement_Contract_ID;
                sqlParas2[1].Value = item.Agreement_Contract_Version;
                sqlParas2[2].Value = item.Item_Num;
                sqlParas2[3].Value = item.I;
                sqlParas2[4].Value = item.A;
                sqlParas2[5].Value = item.Material_ID;
                sqlParas2[6].Value = item.Short_Text;
                sqlParas2[7].Value = item.Target_Quantity;
                sqlParas2[8].Value = item.OUn;
                sqlParas2[9].Value = item.Net_Price;
                sqlParas2[10].Value = item.Every;
                sqlParas2[11].Value = item.OPU;
                sqlParas2[12].Value = item.Material_Group;
                sqlParas2[13].Value = item.Factory;
                sqlParas2[14].Value = item.FinishedQuantity;
                sqlParas2[15].Value = item.Stock_ID;
                sqlParas2[16].Value = item.Price_Determin_ID;

                //加入到List
                sqlList.Add(strBui2.ToString());
                sqlParamsList.Add(sqlParas2);
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }
    }
}