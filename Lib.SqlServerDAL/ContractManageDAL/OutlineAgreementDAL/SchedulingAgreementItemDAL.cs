﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.OutlineAgreement;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL
{
    public class SchedulingAgreementItemDAL : SchedulingAgreementItemIDAL
    {
        /// <summary>
        /// 插入计划协议的框架协议项
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int addSchedulingAgreementItem(Scheduling_Agreement_Item item){
            StringBuilder strBui = new StringBuilder("insert into Scheduling_Agreement_Item ( ")
                .Append("Scheduling_Agreement_ID, Item_Num, I, A, ")
                .Append("Material_ID, Short_Text, Target_Quantity, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory ")
                .Append(") values( ")
                .Append("@Scheduling_Agreement_ID, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory )");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                new SqlParameter("@Item_Num", SqlDbType.Int),
                new SqlParameter("@I", SqlDbType.VarChar),
                new SqlParameter("@A", SqlDbType.VarChar),
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Short_Text", SqlDbType.VarChar),
                new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                new SqlParameter("@OUn", SqlDbType.VarChar),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Every", SqlDbType.Decimal),
                new SqlParameter("@OPU", SqlDbType.VarChar),
                new SqlParameter("@Material_Group", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar)
            };

            sqlParas[0].Value = item.Scheduling_Agreement_ID;
            sqlParas[1].Value = item.Item_Num;
            sqlParas[2].Value = item.I;
            sqlParas[3].Value = item.A;
            sqlParas[4].Value = item.Material_ID;
            sqlParas[5].Value = item.Short_Text;
            sqlParas[6].Value = item.Target_Quantity;
            sqlParas[7].Value = item.OUn;
            sqlParas[8].Value = item.Net_Price;
            sqlParas[9].Value = item.Every;
            sqlParas[10].Value = item.OPU;
            sqlParas[11].Value = item.Material_Group;
            sqlParas[12].Value = item.Factory;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据计划协议编号获取框架协议项
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <returns></returns>
        public List<Scheduling_Agreement_Item> getSchedulingAgreementItemListByID(
            string schedulingAgreementID) {

            List<Scheduling_Agreement_Item> itemList = new List<Scheduling_Agreement_Item>();
            StringBuilder strBui = new StringBuilder("select * from Scheduling_Agreement_Item ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID ")
                .Append("order by Item_Num");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };
            sqlParas[0].Value = schedulingAgreementID;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            foreach (DataRow dr in dt.Rows)
            {
                Scheduling_Agreement_Item tmpItem = new Scheduling_Agreement_Item();
                tmpItem.Scheduling_Agreement_ID = schedulingAgreementID;
                tmpItem.Item_Num = Convert.ToInt32(dr["Item_Num"].ToString());
                tmpItem.I = dr["I"].ToString();
                tmpItem.A = dr["A"].ToString();
                tmpItem.Material_ID = dr["Material_ID"].ToString();
                tmpItem.Short_Text = dr["Short_Text"].ToString();
                tmpItem.Target_Quantity = Convert.ToDouble(dr["Target_Quantity"].ToString());
                tmpItem.OUn = dr["OUn"].ToString();
                tmpItem.Net_Price = Convert.ToDouble(dr["Net_Price"].ToString());
                tmpItem.Every = Convert.ToDouble(dr["Every"].ToString());
                tmpItem.OPU = dr["OPU"].ToString();
                tmpItem.Material_Group = dr["Material_Group"].ToString();
                tmpItem.Factory = dr["Factory"].ToString();

                itemList.Add(tmpItem);
            }

            return itemList;
        }

        /// <summary>
        /// 更新计划协议的框架协议项
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public int updateSchedulingAgreementItem(string schedulingAgreementID,
            Scheduling_Agreement_Item item) {

            StringBuilder strBui = new StringBuilder("insert into Scheduling_Agreement_Item ( ")
                .Append("Scheduling_Agreement_ID, Item_Num, I, A, ")
                .Append("Material_ID, Short_Text, Target_Quantity, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory ")
                .Append(") values( ")
                .Append("@Scheduling_Agreement_ID, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory )");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                new SqlParameter("@Item_Num", SqlDbType.Int),
                new SqlParameter("@I", SqlDbType.VarChar),
                new SqlParameter("@A", SqlDbType.VarChar),
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Short_Text", SqlDbType.DateTime),
                new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                new SqlParameter("@OUn", SqlDbType.VarChar),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Every", SqlDbType.VarChar),
                new SqlParameter("@OPU", SqlDbType.DateTime),
                new SqlParameter("@Material_Group", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar)
            };

            sqlParas[0].Value = item.Scheduling_Agreement_ID;
            sqlParas[1].Value = item.Item_Num;
            sqlParas[2].Value = item.I;
            sqlParas[3].Value = item.A;
            sqlParas[4].Value = item.Material_ID;
            sqlParas[5].Value = item.Short_Text;
            sqlParas[6].Value = item.Target_Quantity;
            sqlParas[7].Value = item.OUn;
            sqlParas[8].Value = item.Net_Price;
            sqlParas[9].Value = item.Every;
            sqlParas[10].Value = item.OPU;
            sqlParas[11].Value = item.Material_Group;
            sqlParas[12].Value = item.Factory;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 删除指定计划协议的框架协议项
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <returns></returns>
        public int deleteSchedulingAgreementItem(string schedulingAgreementID) {
            StringBuilder strBui = new StringBuilder("delete from Scheduling_Agreement_Item ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID ");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = schedulingAgreementID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }
    }
}
