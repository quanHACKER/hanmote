﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.OutlineAgreement;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL
{
    public class DeliveryItemDAL : DeliveryItemIDAL
    {
        /// <summary>
        /// 根据具体的框架协议项查询相应的交货计划
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="materialItemNum"></param>
        /// <param name="itemNum"></param>
        /// <returns>返回交货计划链表</returns>
        public List<Delivery_Item> getDeliveryItemList(string schedulingAgreementID,
            int materialItemNum) {

            StringBuilder strBui = new StringBuilder("select * from Delivery_Item where Scheduling_Agreement_ID = ")
                .Append("@Scheduling_Agreement_ID and Material_Item_Num = @Material_Item_Num ")
                .Append(" order by Item_Num");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Item_Num", SqlDbType.Int)
            };

            sqlParams[0].Value = schedulingAgreementID;
            sqlParams[1].Value = materialItemNum;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParams);
            List<Delivery_Item> resultList = new List<Delivery_Item>();
            foreach (DataRow dr in dt.Rows) {
                Delivery_Item item = new Delivery_Item();
                item.Scheduling_Agreement_ID = schedulingAgreementID;
                item.Material_Item_Num = materialItemNum;
                item.Item_Num = Convert.ToInt32(dr["Item_Num"].ToString());
                item.Delivery_Time = Convert.ToDateTime(dr["Delivery_Time"].ToString());
                item.Delivery_Amount = Convert.ToDouble(dr["Delivery_Amount"].ToString());
                item.Record_Delivery_Time = Convert.ToDateTime(dr["Record_Delivery_Time"].ToString());
                item.Accumulate_Delivery_Amount = Convert.ToDouble(dr["Accumulate_Delivery_Amount"].ToString());
                item.LastTime_Accumulate_Amount = Convert.ToDouble(dr["LastTime_Accumulate_Amount"].ToString());
                item.Not_Delivery_Amount = Convert.ToDouble(dr["Not_Delivery_Amount"].ToString());
                item.Status = dr["Status"].ToString();

                resultList.Add(item);
            }

            return resultList;
        }

        /// <summary>
        /// 更新全部交货计划行
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="materialItemNum"></param>
        /// <param name="deliveryItemList"></param>
        /// <returns></returns>
        public int updateDeliveryItemList(string schedulingAgreementID, int materialItemNum,
            List<Delivery_Item> deliveryItemList) {
                
            LinkedList<string> sqlList = new LinkedList<string>();
            //先删除
            StringBuilder strBui = new StringBuilder("delete from Delivery_Item where ")
                .Append("Scheduling_Agreement_ID = '")
                .Append(schedulingAgreementID)
                .Append("' and Material_Item_Num = ")
                .Append(materialItemNum);

            sqlList.AddLast(strBui.ToString());
            
            //再插入
            foreach (Delivery_Item item in deliveryItemList) {
                strBui = new StringBuilder("insert into Delivery_Item ( Scheduling_Agreement_ID, ")
                    .Append("Material_Item_Num, Item_Num, Delivery_Time, Delivery_Amount, ")
                    .Append("Record_Delivery_Time, Accumulate_Delivery_Amount, LastTime_Accumulate_Amount")
                    .Append(", Not_Delivery_Amount, Status ) values ( '")
                    .Append(item.Scheduling_Agreement_ID).Append("', ")
                    .Append(item.Material_Item_Num).Append(", ")
                    .Append(item.Item_Num).Append(", '")
                    .Append(item.Delivery_Time.ToString()).Append("', ")
                    .Append(item.Delivery_Amount).Append(", '")
                    .Append(item.Record_Delivery_Time.ToString()).Append("', ")
                    .Append(item.Accumulate_Delivery_Amount).Append(", ")
                    .Append(item.LastTime_Accumulate_Amount).Append(", ")
                    .Append(item.Not_Delivery_Amount).Append(", '")
                    .Append(item.Status).Append("' )");

                sqlList.AddLast(strBui.ToString());
            }

            return DBHelper.ExecuteNonQuery(sqlList);
        }
    }
}
