﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.OutlineAgreement;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL
{
    public class AgreementContractItemDAL : AgreementContractItemIDAL
    {
        /// <summary>
        /// 插入新的框架协议项
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int addAgreementContractItem(Agreement_Contract_Item item) {
            StringBuilder strBui = new StringBuilder("insert into Agreement_Contract_Item ( ")
                .Append("Agreement_Contract_ID, Agreement_Contract_Version, Item_Num, I, A, ")
                .Append("Material_ID, Material_Name, Demand_Count, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory_ID, Finished_Quantity, Stock_ID, Price_Determin_ID")
                .Append(") values( ")
                .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory, @Finish, @Stock, @Price_Determin)");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.Int),
                new SqlParameter("@Item_Num", SqlDbType.Int),
                new SqlParameter("@I", SqlDbType.VarChar),
                new SqlParameter("@A", SqlDbType.VarChar),
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Short_Text", SqlDbType.VarChar),
                new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                new SqlParameter("@OUn", SqlDbType.VarChar),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Every", SqlDbType.Decimal),
                new SqlParameter("@OPU", SqlDbType.VarChar),
                new SqlParameter("@Material_Group", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Finish",SqlDbType.Decimal),
                new SqlParameter("@Stock",SqlDbType.VarChar),
                new SqlParameter("@Price_Determin",SqlDbType.VarChar)
            };

            sqlParas[0].Value = item.Agreement_Contract_ID;
            sqlParas[1].Value = item.Agreement_Contract_Version;
            sqlParas[2].Value = item.Item_Num;
            sqlParas[3].Value = item.I;
            sqlParas[4].Value = item.A;
            sqlParas[5].Value = item.Material_ID;
            sqlParas[6].Value = item.Short_Text;
            sqlParas[7].Value = item.Target_Quantity;
            sqlParas[8].Value = item.OUn;
            sqlParas[9].Value = item.Net_Price;
            sqlParas[10].Value = item.Every;
            sqlParas[11].Value = item.OPU;
            sqlParas[12].Value = item.Material_Group;
            sqlParas[13].Value = item.Factory;
            sqlParas[14].Value = item.FinishedQuantity;
            sqlParas[15].Value = item.Stock_ID;
            sqlParas[16].Value = item.Price_Determin_ID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据合同ID和版本查询框架协议项
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public List<Agreement_Contract_Item> getAgreementContractItemListByID(
            string agreementContractID, string agreementContractVersion) {
                
            List<Agreement_Contract_Item> itemList = new List<Agreement_Contract_Item>();
            StringBuilder strBui = new StringBuilder("select * from Agreement_Contract_Item ")
                .Append("where Agreement_Contract_ID = @Agreement_Contract_ID ")
                .Append("and Agreement_Contract_Version = @Agreement_Contract_Version ")
                .Append("order by Item_Num");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };
            sqlParas[0].Value = agreementContractID;
            sqlParas[1].Value = agreementContractVersion;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            foreach (DataRow dr in dt.Rows) {
                Agreement_Contract_Item tmpItem = new Agreement_Contract_Item();
                tmpItem.Agreement_Contract_ID = agreementContractID;
                tmpItem.Agreement_Contract_Version = agreementContractVersion;
                tmpItem.Item_Num = Convert.ToInt32(dr["Item_Num"].ToString());
                tmpItem.I = dr["I"].ToString();
                tmpItem.A = dr["A"].ToString();
                tmpItem.Material_ID = dr["Material_ID"].ToString();
                tmpItem.Short_Text = dr["Material_Name"].ToString();
                tmpItem.Target_Quantity = Convert.ToDouble(dr["Demand_Count"].ToString());
                tmpItem.OUn = dr["OUn"].ToString();
                tmpItem.Net_Price = Convert.ToDouble(dr["Net_Price"].ToString());
                tmpItem.Every = Convert.ToDouble(dr["Every"].ToString());
                tmpItem.OPU = dr["OPU"].ToString();
                tmpItem.Material_Group = dr["Material_Group"].ToString();
                tmpItem.Factory = dr["Factory_ID"].ToString();
                tmpItem.FinishedQuantity = Convert.ToDouble(dr["Finished_Quantity"].ToString());
                tmpItem.Stock_ID = dr["Stock_ID"].ToString();
                tmpItem.Price_Determin_ID = dr["Price_Determin_ID"].ToString();

                itemList.Add(tmpItem);
            }

            return itemList;
        }

        /// <summary>
        /// 更新合同框架协议项
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public int updateAgreementContractItem(string agreementContractID,
            string agreementContractVersion, Agreement_Contract_Item item) {

            StringBuilder strBui = new StringBuilder("insert into Agreement_Contract_Item ( ")
                .Append("Agreement_Contract_ID, Agreement_Contract_Version, Item_Num, I, A, ")
                .Append("Material_ID, Material_Name, Demand_Count, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory_ID, Finished_Quantity, Stock_ID, Price_Determin_ID")
                .Append(") values( ")
                .Append("@Agreement_Contract_ID, @Agreement_Contract_Version, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory, @Finish, @Stock, @Price_Determin)");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.Int),
                new SqlParameter("@Item_Num", SqlDbType.Int),
                new SqlParameter("@I", SqlDbType.VarChar),
                new SqlParameter("@A", SqlDbType.VarChar),
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Short_Text", SqlDbType.DateTime),
                new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                new SqlParameter("@OUn", SqlDbType.VarChar),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Every", SqlDbType.VarChar),
                new SqlParameter("@OPU", SqlDbType.DateTime),
                new SqlParameter("@Material_Group", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Finish",SqlDbType.Decimal),
                new SqlParameter("@Stock",SqlDbType.VarChar),
                new SqlParameter("@Price_Determin",SqlDbType.VarChar)
            };

            sqlParas[0].Value = item.Agreement_Contract_ID;
            sqlParas[1].Value = item.Agreement_Contract_Version;
            sqlParas[2].Value = item.Item_Num;
            sqlParas[3].Value = item.I;
            sqlParas[4].Value = item.A;
            sqlParas[5].Value = item.Material_ID;
            sqlParas[6].Value = item.Short_Text;
            sqlParas[7].Value = item.Target_Quantity;
            sqlParas[8].Value = item.OUn;
            sqlParas[9].Value = item.Net_Price;
            sqlParas[10].Value = item.Every;
            sqlParas[11].Value = item.OPU;
            sqlParas[12].Value = item.Material_Group;
            sqlParas[13].Value = item.Factory;
            sqlParas[14].Value = item.FinishedQuantity;
            sqlParas[15].Value = item.Stock_ID;
            sqlParas[16].Value = item.Price_Determin_ID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 删除指定合同ID和Version的框架协议项
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public int deleteAgreementContractItem(string agreementContractID,
            string agreementContractVersion) {

            StringBuilder strBui = new StringBuilder("delete from Agreement_Contract_Item ")
                .Append("where Agreement_Contract_ID = @Agreement_Contract_ID ")
                .Append("and Agreement_Contract_Version = @Agreement_Contract_Version");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Agreement_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Agreement_Contract_Version", SqlDbType.VarChar)
            };

            sqlParas[0].Value = agreementContractID;
            sqlParas[1].Value = agreementContractVersion;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }
    }
}
