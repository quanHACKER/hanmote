﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractTemplateTermSampleDAL : ContractTemplateTermSampleIDAL
    {
        /// <summary>
        /// 根据termSampleName获得Contract_Template_Term_Sample
        /// </summary>
        /// <param name="termSampleName"></param>
        /// <returns></returns>
        public Contract_Template_Term_Sample getContractTemplateTermSampleByTermSampleName(
            string termSampleName) {
            
            Contract_Template_Term_Sample result = null;
            if (termSampleName == null || termSampleName.Equals(""))
                return result;
            StringBuilder sb = new StringBuilder("select * from Contract_Template_Term_Sample where Name = '");
            sb.Append(termSampleName)
                .Append("'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            for (int i = 0; i < dt.Rows.Count; i++) {
                DataRow dr = dt.Rows[i];
                Contract_Template_Term_Sample sample = new Contract_Template_Term_Sample();
                sample.Name = dr["Name"].ToString();
                sample.Class = dr["Class"].ToString();
                sample.FileName = dr["FileName"].ToString();
                sample.Detail = dr["Detail"].ToString();

                result = sample;
            }

            return result;
        }

        /// <summary>
        /// 增加新的合同模板条款
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns>受影响的行数</returns>
        public int addNewContractTemplateTermSample(Contract_Template_Term_Sample newTermSample)
        {
            if (newTermSample == null)
                return 0;
            StringBuilder stringBuilder = new StringBuilder("insert into Contract_Template_Term_Sample (Name, Class, FileName, Detail) values(")
                .Append("@Name, @Class, @FileName, @Detail")
                .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Name", SqlDbType.VarChar),
                new SqlParameter("@Class", SqlDbType.VarChar),
                new SqlParameter("@FileName", SqlDbType.VarChar),
                new SqlParameter("@Detail", SqlDbType.VarChar)
            };
            sqlParas[0].Value = newTermSample.Name;
            sqlParas[1].Value = newTermSample.Class;
            sqlParas[2].Value = newTermSample.FileName;
            sqlParas[3].Value = newTermSample.Detail;

            return DBHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParas);
        }

        /// <summary>
        /// 执行sql语句，返回DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable getDataTableByExecSql(string sql) {
            if (sql == null || sql.Equals(""))
                return null;

            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 通过类型来查询
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataTable getContractTemplateTermSampleByType(string type) {
            if (type == null)
                return null;

            StringBuilder strBui = new StringBuilder("SELECT * FROM Contract_Template_Term_Sample");
            if (!type.Equals("全部类型")) {
                strBui.Append(" WHERE Class = '")
                    .Append(type)
                    .Append("'");
            }

            return DBHelper.ExecuteQueryDT(strBui.ToString());
        }

        /// <summary>
        /// 根据名称和类型删除指定的合同模板条款
        /// 为了便于主键的修改，此处用名称和类型组合来唯一确定一个合同模板条款
        /// </summary>
        /// <param name="name"></param>
        /// <param name="clazz"></param>
        /// <returns>受影响的行数</returns>
        public int deleteContractTemplateTermSampleByPK(string name,string clazz)
        {
            if (name == null || clazz == null || name.Equals("") || clazz.Equals(""))
                return 0;
            StringBuilder strBui = new StringBuilder("DELETE FROM Contract_Template_Term_Sample");
            strBui.Append(" WHERE Name = '");
            strBui.Append(name);
            strBui.Append("'");
            strBui.Append(" AND Class = '");
            strBui.Append(clazz);
            strBui.Append("'");
            return DBHelper.ExecuteNonQuery(strBui.ToString());
        }

    }
}
