﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractTemplateVariableDAL : ContractTemplateVariableIDAL
    {
        /// <summary>
        /// 根据Contact_Template_Term_Sample_ID获取所有的variable
        /// </summary>
        /// <param name="contractTemplateID"></param>
        /// <returns></returns>
        public LinkedList<Contract_Template_Variable> getContractTemplateVariableByContractTemplateTermSampleID(string contractTemplateTermSampleID)
        {
            LinkedList<Contract_Template_Variable> result = new LinkedList<Contract_Template_Variable>();
            if (contractTemplateTermSampleID == null || contractTemplateTermSampleID.Equals(""))
                return result;

            StringBuilder sb = new StringBuilder("select * from Contract_Template_Variable where Contract_Template_Term_Sample_ID = '");
            sb.Append(contractTemplateTermSampleID)
                .Append("' order by Order_Number");

            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                Contract_Template_Variable variable = new Contract_Template_Variable();
                variable.Name = dr["Name"].ToString();
                variable.Type = dr["Type"].ToString();
                variable.Order_Number = Convert.ToInt32(dr["Order_Number"].ToString());
                variable.Contract_Template_Term_Sample_ID = dr["Contract_Template_Term_Sample_ID"].ToString();
                variable.Detail = dr["Detail"].ToString();
                variable.Token_Text = dr["Token_Text"].ToString();

                result.AddLast(variable);
            }

            return result;
        }

        /// <summary>
        /// 增加新的合同模板条款变量
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns>受影响的行数</returns>
        public int addNewContractTemplateVariable(Contract_Template_Variable newVariable)
        {
            if (newVariable == null)
                return 0;
            StringBuilder stringBuilder = new StringBuilder("insert into Contract_Template_Variable (Name, Type, Detail, Token_Text, Contract_Template_Term_Sample_ID, Order_Number) values(")
                .Append("@Name, @Type, @Detail, @Token_Text, @Contract_Template_Term_Sample_ID, @Order_Number")
                .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Name", SqlDbType.VarChar),
                new SqlParameter("@Type", SqlDbType.VarChar),
                new SqlParameter("@Detail", SqlDbType.VarChar),
                new SqlParameter("@Token_Text", SqlDbType.VarChar),
                new SqlParameter("@Contract_Template_Term_Sample_ID", SqlDbType.VarChar),
                new SqlParameter("@Order_Number", SqlDbType.Int)
            };
            sqlParas[0].Value = newVariable.Name;
            sqlParas[1].Value = newVariable.Type;
            sqlParas[2].Value = newVariable.Detail;
            sqlParas[3].Value = newVariable.Token_Text;
            sqlParas[4].Value = newVariable.Contract_Template_Term_Sample_ID;
            sqlParas[5].Value = newVariable.Order_Number;

            return DBHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParas);
        }
    }
}
