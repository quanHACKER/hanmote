﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractTemplateDAL : ContractTemplateIDAL
    {
        /// <summary>
        /// 根据ID获取Contract_Template，如果不存在，则返回null
        /// </summary>
        /// <param name="ContractTemplateID"></param>
        /// <returns></returns>
        public Contract_Template getContractTemplateByID(string ContractTemplateID) {
            StringBuilder sb = new StringBuilder();
            sb.Append("Select * from Contract_Template where ID = '")
                .Append(ContractTemplateID)
                .Append("'");

            DataTable result = DBHelper.ExecuteQueryDT(sb.ToString());

            if (result != null && result.Rows.Count > 0) {
                Contract_Template template = new Contract_Template();
                DataRow dr = result.Rows[0];
                template.Name = dr["Name"].ToString();
                template.ID = dr["ID"].ToString();
                template.Status = dr["Status"].ToString();
                template.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());

                return template;
            }

            return null;
        }

        /// <summary>
        /// 增加新的合同模板
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns>受影响的行数</returns>
        public int addNewContractTemplate(Contract_Template contractTemplate) {
            if (contractTemplate == null)
                return 0;
            StringBuilder stringBuilder = new StringBuilder("insert into Contract_Template (ID, Name, Status, Create_Time) values(")
                .Append("@ID, @Name, @Status, @Create_Time")
                .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@ID", SqlDbType.VarChar),
                new SqlParameter("@Name", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Create_Time", SqlDbType.DateTime)
            };
            sqlParas[0].Value = contractTemplate.ID;
            sqlParas[1].Value = contractTemplate.Name;
            sqlParas[2].Value = contractTemplate.Status;
            sqlParas[3].Value = contractTemplate.Create_Time;

            return DBHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据SQL语句返回具体结果, LinkedList<Contract_Template>
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>ContractTemplateList</returns>
        public LinkedList<Contract_Template> getContractTemplateBySQL(string sql)
        {
            LinkedList<Contract_Template> contractTemplateList = new LinkedList<Contract_Template>();
            DataTable result = DBHelper.ExecuteQueryDT(sql);
            Contract_Template template;
            for (int i = 0; i < result.Rows.Count; i++)
            {
                DataRow dr = result.Rows[i];
                template = new Contract_Template();
                template.ID = dr["ID"].ToString();
                template.Name = dr["Name"].ToString();
                template.Status = dr["Status"].ToString();
                template.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());

                contractTemplateList.AddLast(template);
            }

            return contractTemplateList;
        }

        /// <summary>
        /// 返回ContractTemplate查询的DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable getContractTemplateDataTableBySQL(string sql) {
            if (sql == null || sql.Equals(""))
                return null;

            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 根据ID来更新合同模板的状态
        /// </summary>
        /// <param name="IDList">包含合同ID的list</param>
        /// <param name="changeToState">改变后的状态</param>>
        /// <returns></returns>
        public int UpdateContractTemplateState(LinkedList<string> IDList, string changeToState)
        {
            if (IDList == null || IDList.Count == 0)
            {
                return 0;
            }

            StringBuilder strBui = new StringBuilder("update Contract_Template set Status = '" + changeToState);
            strBui.Append("' where ID in (");
            foreach (string id in IDList)
            {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = 0;
            string sql = strBui.ToString();
            result = DBHelper.ExecuteNonQuery(sql);

            return result;
        }

        /// <summary>
        /// 删除指定ID的合同模板
        /// </summary>
        /// <param name="IDList"></param>
        /// <returns></returns>
        public int DeleteContractTemplate(LinkedList<string> IDList)
        {
            StringBuilder strBui = new StringBuilder("delete Contract_Template ");
            strBui.Append(" where ID in (");
            foreach (string id in IDList)
            {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = DBHelper.ExecuteNonQuery(strBui.ToString());

            return result;
        }
    }
}
