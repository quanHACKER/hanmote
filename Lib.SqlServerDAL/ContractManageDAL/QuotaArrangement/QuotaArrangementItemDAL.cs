﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.QuotaArrangement;
using Lib.Model.ContractManage.QuotaArrangement;

namespace Lib.SqlServerDAL.ContractManageDAL.QuotaArrangement
{
    public class QuotaArrangementItemDAL : QuotaArrangementItemIDAL
    {
        /// <summary>
        /// 获取指定配额协议下所有的配额协议项
        /// </summary>
        /// <param name="quotaArrangementID"></param>
        /// <returns></returns>
        public List<Quota_Arrangement_Item> getQuotaArrangementItemList(
            string quotaArrangementID) {
            StringBuilder strBui = new StringBuilder("select * from Quota_Arrangement_Item ")
                .Append("where Quota_Arrangement_ID = '").Append(quotaArrangementID)
                .Append("' order by Item_Num");
            
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            List<Quota_Arrangement_Item> itemList = new List<Quota_Arrangement_Item>();
            foreach (DataRow row in dt.Rows) {
                Quota_Arrangement_Item item = new Quota_Arrangement_Item();
                item.Quota_Arrangement_ID = row["Quota_Arrangement_ID"].ToString();
                item.Material_ID = row["Material_ID"].ToString();
                item.Item_Num = Convert.ToInt32(row["Item_Num"].ToString());
                item.Factory_ID = row["Factory_ID"].ToString();
                item.P = row["P"].ToString();
                item.S = row["S"].ToString();
                item.Supplier_ID = row["Supplier_ID"].ToString();
                item.PP1 = row["PP1"].ToString();
                item.Quota_Value = Convert.ToDouble(row["Quota_Value"].ToString());
                item.Quota_Percentage = Convert.ToDouble(row["Quota_Percentage"].ToString());
                item.Allocate_Amount = Convert.ToDouble(row["Allocate_Amount"].ToString());
                item.Max_Amount = Convert.ToDouble(row["Max_Amount"].ToString());
                item.Quota_Base_Amount = Convert.ToDouble(row["Quota_Base_Amount"].ToString());
                item.Max_Batch_Size = Convert.ToDouble(row["Max_Batch_Size"].ToString());
                item.Min_Batch_Size = Convert.ToDouble(row["Min_Batch_Size"].ToString());
                item.RPro = Convert.ToDouble(row["RPro"].ToString());
                item.One_Time = row["One_Time"].ToString();
                item.Max_Delivery_Amount = Convert.ToDouble(row["Max_Delivery_Amount"].ToString());
                item.Period = row["Period"].ToString();
                item.Pr = Convert.ToInt32(row["Pr"].ToString());

                itemList.Add(item);
            }

            return itemList;
        }

        /// <summary>
        /// 添加或刷新配额协议项
        /// </summary>
        /// <param name="quotaArrangementID"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addQuotaArrangementItemList(string quotaArrangementID,
            List<Quota_Arrangement_Item> itemList)
        {
            # region 加入新的ItemList

            LinkedList<string> sqlList = new LinkedList<string>();
            //删除旧的
            StringBuilder strBui = new StringBuilder("delete from Quota_Arrangement_Item ")
                .Append("where Quota_Arrangement_ID = '").Append(quotaArrangementID)
                .Append("'");
            sqlList.AddLast(strBui.ToString());

            //加入新的
            foreach (Quota_Arrangement_Item item in itemList) {
                strBui = new StringBuilder("insert into Quota_Arrangement_Item (")
                    .Append("Quota_Arrangement_ID, Item_Num, Material_ID, ")
                    .Append("Factory_ID, P, S, Supplier_ID, PP1, ")
                    .Append("Quota_Value, Quota_Percentage, Allocate_Amount, ")
                    .Append("Max_Amount, Quota_Base_Amount, Max_Batch_Size, ")
                    .Append("Min_Batch_Size, RPro, One_Time, Max_Delivery_Amount, ")
                    .Append("Period, Pr) values ( '")
                    .Append(item.Quota_Arrangement_ID).Append("', ")
                    .Append(item.Item_Num).Append(", '")
                    .Append(item.Material_ID).Append("', '")
                    .Append(item.Factory_ID).Append("', '")
                    .Append(item.P).Append("', '")
                    .Append(item.S).Append("', '")
                    .Append(item.Supplier_ID).Append("', '")
                    .Append(item.PP1).Append("', ")
                    .Append(item.Quota_Value).Append(", ")
                    .Append(item.Quota_Percentage).Append(", ")
                    .Append(item.Allocate_Amount).Append(", ")
                    .Append(item.Max_Amount).Append(", ")
                    .Append(item.Quota_Base_Amount).Append(", ")
                    .Append(item.Max_Batch_Size).Append(", ")
                    .Append(item.Min_Batch_Size).Append(", ")
                    .Append(item.RPro).Append(", '")
                    .Append(item.One_Time).Append("', ")
                    .Append(item.Max_Delivery_Amount).Append(", '")
                    .Append(item.Period).Append("', ")
                    .Append(item.Pr).Append(")");

                sqlList.AddLast(strBui.ToString());
            }

            # endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }
    }
}
