﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using Lib.IDAL;

namespace Lib.SqlServerDAL
{
    public class OrganizationInfoDAL : BaseDAL<OrganizationInfo>, OrganizationInfoIDAL
    {
        #region 对象实例及构造函数

		public static OrganizationInfoDAL Instance
		{
			get
			{
				return new OrganizationInfoDAL();
			}
		}
        public OrganizationInfoDAL()
            : base("Org_company_baseinfo", "id","supplierChineseName")
		{
		}

		#endregion

    }
}
