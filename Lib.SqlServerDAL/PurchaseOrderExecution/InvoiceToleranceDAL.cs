﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Model.PurchaseOrderExecution;
using Lib.IDAL.PurchaseOrderExecution;

namespace Lib.SqlServerDAL.PurchaseOrderExecution
{
    public class InvoiceToleranceDAL : InvoiceToleranceIDAL
    {
        /// <summary>
        /// 更新容差设置
        /// </summary>
        /// <param name="toleranceList"></param>
        /// <returns></returns>
        public int updateInvoiceTolerance(List<Invoice_Tolerance> toleranceList) {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            foreach (Invoice_Tolerance tolerance in toleranceList) {
                StringBuilder strBui = new StringBuilder("update Invoice_Tolerance set AbValue_Is_Check = @AbValue_Is_Check, ")
                    .Append("AbValue = @AbValue, Per_Is_Check = @Per_Is_Check, Per = @Per ")
                    .Append("where Param_Name = @Param_Name and Border = @Border");
                SqlParameter[] sqlParams = new SqlParameter[]{
                    new SqlParameter("@AbValue_Is_Check", SqlDbType.Bit),
                    new SqlParameter("@AbValue", SqlDbType.Decimal),
                    new SqlParameter("@Per_Is_Check", SqlDbType.Bit),
                    new SqlParameter("@Per", SqlDbType.Decimal),
                    new SqlParameter("@Param_Name", SqlDbType.VarChar),
                    new SqlParameter("@Border", SqlDbType.Int)
                };
                sqlParams[0].Value = tolerance.AbValue_Is_Check;
                sqlParams[1].Value = tolerance.AbValue;
                sqlParams[2].Value = tolerance.Per_Is_Check;
                sqlParams[3].Value = tolerance.Per;
                sqlParams[4].Value = tolerance.Param_Name;
                sqlParams[5].Value = tolerance.Border;

                sqlList.Add(strBui.ToString());
                sqlParamsList.Add(sqlParams);
            }

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 查询容差设置
        /// </summary>
        /// <returns></returns>
        public List<Invoice_Tolerance> getInvoiceTolerance() {
            List<Invoice_Tolerance> toleranceList = new List<Invoice_Tolerance>();
            
            string sqlText = "select * from Invoice_Tolerance";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            foreach (DataRow row in dt.Rows) {
                Invoice_Tolerance tolerance = new Invoice_Tolerance();
                tolerance.Param_Name = row["Param_Name"].ToString();
                tolerance.Border = Convert.ToInt32(row["Border"].ToString());
                tolerance.AbValue_Is_Check = Convert.ToBoolean(row["AbValue_Is_Check"].ToString());
                tolerance.AbValue = Convert.ToDouble(row["AbValue"].ToString());
                tolerance.Per_Is_Check = Convert.ToBoolean(row["Per_Is_Check"].ToString());
                tolerance.Per = Convert.ToDouble(row["Per"].ToString());
                
                toleranceList.Add(tolerance);
            }

            return toleranceList;
        }
    }
}
