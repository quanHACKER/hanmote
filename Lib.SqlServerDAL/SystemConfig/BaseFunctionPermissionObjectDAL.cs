﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class BaseFunctionPermissionObjectDAL : BaseFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询某个菜单对应的窗口的操作项
        /// </summary>
        /// <param name="baseFunctionPermissionModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionByMenuName(BaseFunctionPermissionObjectModel baseFunctionPermissionModel)
        {
            String sqlText = @"select Permission_ID,Permission_Name,Description from Base_Function_Permission_Object where Menu_Name=@Menu_Name";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Menu_Name",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseFunctionPermissionModel.Menu_Name;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }
    }
}
