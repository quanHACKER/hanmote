﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinUserOrganizationDAL : JoinUserOrganizationIDAL
    {
        /// <summary>
        /// 插入用户组织机构联系
        /// </summary>
        /// <param name="userOrgModel"></param>
        public void insertUserOrganizationLink(JoinUserOrganizationModel userOrgModel)
        {
            String sql = "insert into Join_User_Organization(User_ID,Organization_ID) values(@User_ID,@Organization_ID)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userOrgModel.User_ID;
            sqlParameter[1].Value = userOrgModel.Organization_ID;

            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

        /// <summary>
        /// 根据组织机构ID删除所有关联用户
        /// </summary>
        /// <param name="userOrgModel"></param>
        public void deleteUserOrganizationLinkByOrgId(JoinUserOrganizationModel userOrgModel)
        {
            String sql = "delete from Join_User_Organization where Organization_ID=@Organization_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userOrgModel.Organization_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

        /// <summary>
        /// 删除组织机构和用户指定的关联
        /// </summary>
        /// <param name="userOrgModel"></param>
        public void deleteUserOrganizationLinkByOrgIdAndUserId(JoinUserOrganizationModel userOrgModel)
        {
            String sql = "delete from Join_User_Organization where Organization_ID=@Organization_ID and User_ID=@User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar),
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userOrgModel.Organization_ID;
            sqlParameter[1].Value = userOrgModel.User_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

        /// <summary>
        /// 根据组织机构编号查询其关联的员工编号
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public DataTable queryUserInforByOrganizationID(String organizationID)
        {
            String sql = @"SELECT
	                            A.User_ID,
	                            A.Username,
	                            CASE
                            WHEN A.Gender = 0 THEN
	                            '男'
                            WHEN A.Gender = 1 THEN
	                            '女'
                            END AS Gender,
                             A.Birthday,
                             A.Mobile
                            FROM
	                            Hanmote_Base_User A,
	                            Join_User_Organization B
                            WHERE
	                            B.Organization_ID =@Organization_ID
                            AND A.User_ID = B.User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = organizationID;
            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }

    }
}
