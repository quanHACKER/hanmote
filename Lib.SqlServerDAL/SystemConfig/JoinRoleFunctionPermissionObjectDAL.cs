﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinRoleFunctionPermissionObjectDAL : JoinRoleFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户所有角色具有的权限（对应Menu_Name）
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromRoleOfMenuName(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select C.Permission_ID,C.Permission_Name,C.Description
                               from (select Role_ID from Join_User_Role where User_ID=@User_ID) A
                               inner join Join_Role_Function_Permission_Object B on A.Role_ID=B.Role_ID
                               inner join Base_Function_Permission_Object C on B.Permission_ID=C.Permission_ID where C.Menu_Name=@Menu_Name";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Menu_Name",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = basePermissionModel.Menu_Name;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 查询一个用户所有角色是否具有某Permission_ID对应的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromRoleOfPermissionID(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select C.Permission_ID,C.Permission_Name,C.Description
                                                from (select Role_ID from Join_User_Role where User_ID=@User_ID) A
                                                inner join Join_Role_Function_Permission_Object B on A.Role_ID=B.Role_ID
                                                inner join Base_Function_Permission_Object C on B.Permission_ID=C.Permission_ID
                                                where C.Permission_ID=@Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = basePermissionModel.Permission_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 查询一个角色所具有的权限（对应Menu_Name）
        /// </summary>
        /// <param name="baseRoleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromSingleRoleOfMenuName(BaseRoleModel baseRoleModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select A.Permission_ID,A.Permission_Name,A.Description
                               from Base_Function_Permission_Object A,Join_Role_Function_Permission_Object B
                               where A.Menu_Name=@Menu_Name and B.Role_ID=@Role_ID and A.Permission_ID=B.Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Menu_Name",SqlDbType.VarChar),
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = basePermissionModel.Menu_Name;
            sqlParameter[1].Value = baseRoleModel.Role_ID;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 查询一个角色所具有的权限（对应Permission_ID）
        /// </summary>
        /// <param name="baseRoleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromSingleRoleOfPermissionID(BaseRoleModel baseRoleModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select A.Permission_ID,A.Permission_Name,A.Description
                               from Base_Function_Permission_Object A,Join_Role_Function_Permission_Object B
                               where A.Permission_ID=@Permission_ID and B.Role_ID=@Role_ID and A.Permission_ID=B.Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Permission_ID",SqlDbType.Int),
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = basePermissionModel.Permission_ID;
            sqlParameter[1].Value = baseRoleModel.Role_ID;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 收回角色权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeRolePermission(JoinRoleFunctionPermissionObjectModel objModel)
        {
            String sqlText = "delete from Join_Role_Function_Permission_Object where Role_ID=@Role_ID and Permission_ID=@Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = objModel.Role_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

        /// <summary>
        /// 授予角色权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizeRolePermission(JoinRoleFunctionPermissionObjectModel objModel)
        {
            String sqlText = "insert into Join_Role_Function_Permission_Object(Role_ID,Permission_ID) values(@Role_ID,@Permission_ID)";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = objModel.Role_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }
    }
}
