﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class BaseGroupDAL : BaseGroupIDAL
    {
        /// <summary>
        /// 查询系统用户组、组下面用户信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryGroupAndUserName()
        {
            String sqlText = @"select A.Group_ID,A.Group_Name,B.User_ID,B.Username
                               from Base_Group A left join Join_User_Group B
                               on A.Group_ID=B.Group_ID";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 获取最大的组编号
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public DataTable queryBiggestGroupID(String today)
        {
            String sqlText = "select MAX(Group_ID) from Base_Group where Group_ID LIKE @Group_ID";
            SqlParameter[] sqlParamter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParamter[0].Value = today + "%";

            return DBHelper.ExecuteQueryDT(sqlText,sqlParamter);
        }

        /// <summary>
        /// 插入一个新的用户组
        /// </summary>
        /// <param name="groupModel"></param>
        public void insertNewGroup(BaseGroupModel groupModel)
        {
            String sqlText = @"insert into Base_Group(Group_ID,Group_Name,Description,Generate_Time,Generate_User_ID,Generate_Username)
                               values(@Group_ID,@Group_Name,@Description,@Generate_Time,@Generate_User_ID,@Generate_Username)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar),
                new SqlParameter("@Group_Name",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@Generate_Time",SqlDbType.DateTime),
                new SqlParameter("@Generate_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Generate_Username",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = groupModel.Group_ID;
            sqlParameter[1].Value = groupModel.Group_Name;
            sqlParameter[2].Value = groupModel.Description;
            sqlParameter[3].Value = groupModel.Generate_Time;
            sqlParameter[4].Value = groupModel.Generate_User_ID;
            sqlParameter[5].Value = groupModel.Generate_Username;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 根据组编号查询用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        /// <returns></returns>
        public DataTable queryGroupInforById(BaseGroupModel groupModel)
        {
            String sqlText = "select * from Base_Group where Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = groupModel.Group_ID;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 根据用户组编号更新用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        public void updateGroupInforById(BaseGroupModel groupModel)
        {
            String sqlText = @"update Base_Group set Group_Name=@Group_Name,Description=@Description,Modify_Time=@Modify_Time,Modify_User_ID=@Modify_User_ID,Modify_Username=@Modify_Username
                               where Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_Name",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@Modify_Time",SqlDbType.DateTime),
                new SqlParameter("@Modify_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Modify_Username",SqlDbType.VarChar),
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = groupModel.Group_Name;
            sqlParameter[1].Value = groupModel.Description;
            sqlParameter[2].Value = groupModel.Modify_Time;
            sqlParameter[3].Value = groupModel.Modify_User_ID;
            sqlParameter[4].Value = groupModel.Modify_Username;
            sqlParameter[5].Value = groupModel.Group_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 删除用户组
        /// </summary>
        /// <param name="groupModel"></param>
        public void deleteGroup(BaseGroupModel groupModel)
        {
            String sql = "delete from Base_Group where Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = groupModel.Group_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

    }
}
