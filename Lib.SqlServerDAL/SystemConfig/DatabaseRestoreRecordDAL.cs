﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class DatabaseRestoreRecordDAL : DatabaseRestoreRecordIDAL
    {
        /// <summary>
        /// 查询所有恢复记录
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllRestoreRecord()
        {
            String sqlText = "select * from DatabaseRestoreRecord order by Restore_Time DESC";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 根据记录ID查询恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        /// <returns></returns>
        public DataTable queryRestoreRecordById(DatabaseRestoreRecordModel recordModel)
        {
            String sqlText = "select * from DatabaseRestoreRecord where ID=@ID order by Restore_Time DESC";
            SqlParameter []sqlParameter = new SqlParameter[]{
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 插入新恢复记录到DB
        /// </summary>
        /// <param name="recordModel"></param>
        public void insertRestoreRecord(DatabaseRestoreRecordModel recordModel)
        {
            String sqlText = @"insert into DatabaseRestoreRecord(DB_Name,Restore_Time,Restore_Operator,Restore_Type,Restore_Source)
                               values(@DB_Name,@Restore_Time,@Restore_Operator,@Restore_Type,@Restore_Source)";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@DB_Name",SqlDbType.VarChar),
                new SqlParameter("@Restore_Time",SqlDbType.DateTime),
                new SqlParameter("@Restore_Operator",SqlDbType.VarChar),
                new SqlParameter("@Restore_Type",SqlDbType.Int),
                new SqlParameter("@Restore_Source",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.DB_Name;
            sqlParameter[1].Value = recordModel.Restore_Time;
            sqlParameter[2].Value = recordModel.Restore_Operator;
            sqlParameter[3].Value = recordModel.Restore_Type;
            sqlParameter[4].Value = recordModel.Restore_Source;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 更新恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        public void updateRestoreRecord(DatabaseRestoreRecordModel recordModel)
        {
            String sqlText = @"update DatabaseRestoreRecord set DB_Name=@DB_Name,
                                                                Restore_Time=@Restore_Time,
                                                                Restore_Operator=@Restore_Operator,
                                                                Restore_Type=@Restore_Type,
                                                                Restore_Source=@Restore_Source where ID=@ID";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@DB_Name",SqlDbType.VarChar),
                new SqlParameter("@Restore_Time",SqlDbType.DateTime),
                new SqlParameter("@Restore_Operator",SqlDbType.VarChar),
                new SqlParameter("@Restore_Type",SqlDbType.Int),
                new SqlParameter("@Restore_Source",SqlDbType.Int),
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.DB_Name;
            sqlParameter[1].Value = recordModel.Restore_Time;
            sqlParameter[2].Value = recordModel.Restore_Operator;
            sqlParameter[3].Value = recordModel.Restore_Type;
            sqlParameter[4].Value = recordModel.Restore_Source;
            sqlParameter[5].Value = recordModel.ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 删除恢复记录
        /// </summary>
        /// <param name="recordModel"></param>
        public void deleteRestoreRecord(DatabaseRestoreRecordModel recordModel)
        {
            String sqlText = "delete from DatabaseRestoreRecord where ID=@ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }
    }
}
