﻿using Lib.IDAL.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.SystemConfig
{
    class SystemUserOrganicRelationshipDAL : SystemUserOrganicRelationshipIDAL
    {
        public DataTable calculateRecordNumber()
        {
            String sqlText = "select count(User_ID) from Hanmote_Base_User";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 删除所有有关该用户的信息
        /// </summary>
        /// <param name="userId"></param>
        public void deleteSysUserInfoByUserId(string userId)
        {

            string sqlstr1 = @"delete from  Join_User_Group where User_ID = '" + userId + "'";
            string sqlstr2 = @"delete from  Join_User_Organization where User_ID = '" + userId + "'";
            string sqlstr3 = @"delete from  Join_User_Role where User_ID = '" + userId + "'";
            string sqlstr4 = @"delete from  Join_User_Function_Permission_Object where User_ID = '" + userId + "'";
            string sqlstr5 = @"delete from  Hanmote_Base_User where User_ID= '" + userId + "'";
            string sqlstr6 = @"delete from  Hanmote_User_MtGroupName where User_ID ='" + userId + "'";
            string sqlstr7 = @"delete from  HanmoteBaseUser_EvalCode where UserID ='" + userId + "'";
            DBHelper.ExecuteNonQuery(sqlstr7);
            DBHelper.ExecuteNonQuery(sqlstr1);
            DBHelper.ExecuteNonQuery(sqlstr2);
            DBHelper.ExecuteNonQuery(sqlstr3);
            DBHelper.ExecuteNonQuery(sqlstr4);
            DBHelper.ExecuteNonQuery(sqlstr5);
            DBHelper.ExecuteNonQuery(sqlstr6);
        }


        /// <summary>
        /// 删除/激活
        /// </summary>
        /// <param name="userID"></param>
        public void delOrActivitiUser(string userID, int a)
        {
            string sqlstr = String.Format(@"update Hanmote_Base_User set Enabled = '" + a + "' where User_ID= '" + userID + "'");
            DBHelper.ExecuteNonQuery(sqlstr);
        }
        /// <summary>
        /// 查找所有信息
        /// </summary>
        /// <returns></returns>
        public DataTable findAllSystemUserOrganicRelationshipInfo(int pageNum, int front,int type)
        {
            string sqlstr = "";
            if(type!=1)
            {
                sqlstr = String.Format( @"SELECT
	                                        hu.User_ID AS 员工编号,
	                                        hu.Username AS 姓名,
	                                        hu.Mobile AS 联系方式,
	                                        CASE
                                        WHEN hu.Enabled = 0 THEN
	                                        '在籍'
                                        WHEN hu.Enabled = 1 THEN
	                                        '已注销'
                                        END AS 状态,
                                         b.Organization_Name AS 所属公司,
                                         r.Role_Name AS 职务,
                                        hu.Depart_Name as 所属部门,
                                        hu.email as 邮箱
                                        FROM
	                                        Hanmote_Base_User hu,
	                                        Base_Organization b,
	                                        Base_Role r
                                        WHERE
	                                        b.Organization_ID = hu.companyID
                                        AND r.Role_ID = hu.Role_ID and r.Company_ID = b.Organization_ID and hu.Manager_Flag !=1", pageNum, front);
            }
            else
            {
                sqlstr = String.Format(@"SELECT
	                                        hu.User_ID AS 员工编号,
	                                        hu.Username AS 姓名,
	                                        hu.Mobile AS 联系方式,
	                                        CASE
                                        WHEN hu.Enabled = 0 THEN
	                                        '在籍'
                                        WHEN hu.Enabled = 1 THEN
	                                        '已注销'
                                        END AS 状态,
                                         b.Organization_Name AS 所属公司,
                                         r.Role_Name AS 职务,
                                        hu.Depart_Name as 所属部门,
                                        hu.email as 邮箱
                                        FROM
	                                        Hanmote_Base_User hu,
	                                        Base_Organization b,
	                                        Base_Role r
                                        WHERE
	                                        b.Organization_ID = hu.companyID
                                        AND r.Role_ID = hu.Role_ID and r.Role_ID = '2018012200001' and r.Company_ID = b.Organization_ID and hu.Manager_Flag !=1", pageNum, front);
            }
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable findAllSystemUserOrganicRelationshipInfoByCondition(SystemUserOrganicRelationshipModel model, int pageNum, int front)
        {
            StringBuilder sqlText = new StringBuilder(@"SELECT
	                                        hu.User_ID AS 员工编号,
	                                        hu.Username AS 姓名,
	                                        hu.Mobile AS 联系方式,
	                                        CASE
                                        WHEN hu.Enabled = 0 THEN
	                                        '在籍'
                                        WHEN hu.Enabled = 1 THEN
	                                        '已注销'
                                        END AS 状态,
                                         b.Organization_Name AS 所属公司,
                                         r.Role_Name AS 职务,
                                         hu.Depart_Name as 所属部门,
                                         hu.email as 邮箱
                                        FROM
	                                        Hanmote_Base_User hu,
	                                        Base_Organization b,
	                                        Base_Role r
                                        WHERE
	                                        b.Organization_ID = hu.companyID
                                        AND r.Role_ID = hu.Role_ID  and r.Company_ID = b.Organization_ID and hu.Manager_Flag !=1");
            StringBuilder tmpText = new StringBuilder("");

            if (!String.IsNullOrEmpty(model.compangClass))
            {
                tmpText.Append(" and b.Organization_ID = ");
                tmpText.Append("'" + model.compangClass + "'");
            }

            if (!String.IsNullOrEmpty(model.jobPosition))
            {
                tmpText.Append(" and r.Role_ID  =  ");
                tmpText.Append("'" + model.jobPosition + "'");
            }

            if (!String.IsNullOrEmpty(model.departName))
            {
                tmpText.Append(" and hu.Depart_Name like ");
                tmpText.Append("'%" + model.departName + "%'");
            }
            if (!String.IsNullOrEmpty(model.sysUserName))
            {
                tmpText.Append(" and hu.Username like ");
                tmpText.Append("'%" + model.sysUserName + "%'");
            }
            sqlText.Append(tmpText);

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        public DataTable getCompanyNameSQL()
        {
            String sql = "SELECT Organization_ID as id ,Organization_Name as name  FROM Base_Organization where (level=0 or level=1)";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getOrganicNameSQL(string comanyID)
        {
            //先通过ID找到值left_value、rightValue 再找出部门
            String sqlv = "SELECT Left_Value,Right_Value from Base_Organization  where Organization_ID='" + comanyID + "'";
            DataTable table = DBHelper.ExecuteQueryDT(sqlv);
            if (table != null && table.Rows.Count > 0)
            {
                String leftValue = table.Rows[0][0].ToString();
                if (leftValue == "1")
                {
                    String rightValue = table.Rows[0][1].ToString();
                    String sql = "SELECT Organization_ID as id ,Organization_Name as name  FROM Base_Organization  where  Left_Value > '" + leftValue + "'  and  Right_Value < '" + rightValue + "' and level = 1";
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    return dt;
                }
                else
                {
                    String rightValue = table.Rows[0][1].ToString();
                    String sql = "SELECT Organization_ID as id ,Organization_Name as name  FROM Base_Organization  where  Left_Value > '" + leftValue + "'  and  Right_Value < '" + rightValue + "' and level >1";
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    return dt;
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable getRoleNameSQL(string comanyID)
        {
            String sql = "SELECT Role_ID as id ,Role_Name as name  FROM Base_Role where Company_ID = '" + comanyID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        public DataTable getInfoByRoleUserGroup()
        {
            String sql = "select Role_Name as name from Base_Role GROUP BY Role_Name";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        public DataTable findAllMtGroupName()
        {
            //别人已经选过了的处在显示刷出
            String sql = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
                                Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public void insertNewUser(SystemUserOrganicRelationshipModel model)
        {
            String sqlText = @"INSERT INTO [Hanmote_Base_User](
                                [User_ID],
	                            [Login_Name],
	                            [Login_Password],
	                            [Username],
	                            [Gender],
	                            [Birthday],
	                            [Birth_Place],
	                            [Mobile],
	                            [Email],
	                            [User_Description],
	                            [Enabled],
	                            [Manager_Flag],
	                            [Role_ID],
	                            [Depart_Name],
	                            [companyID],[pid])
                                VALUES(@User_ID,@Login_Name,@Login_Password,@Username,@Gender,
                                       @Birthday,@Birth_Place,
                                       @Mobile,@Email,@User_Description,
                                       @Enabled,@Manager_Flag,@Role_ID,@Depart_Name,
                                       @companyID,@pid )";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.Date),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Depart_Name",SqlDbType.VarChar),
                new SqlParameter("@companyID",SqlDbType.VarChar),
                new SqlParameter("@pid",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = model.sysUser_ID;
            sqlParameter[1].Value = model.Login_Name;
            sqlParameter[2].Value = model.Login_Password;
            sqlParameter[3].Value = model.sysUserName;
            sqlParameter[4].Value = model.Gender;
            sqlParameter[5].Value = model.Birthday;
            sqlParameter[6].Value = model.Birth_Place;
            sqlParameter[7].Value = model.Mobile;
            sqlParameter[8].Value = model.Email;
            sqlParameter[9].Value = model.User_Description;
            sqlParameter[10].Value = model.status;
            sqlParameter[11].Value = 0;
            sqlParameter[12].Value = model.jobPosition;
            sqlParameter[13].Value = model.departName;
            sqlParameter[14].Value = model.compangClass;
            sqlParameter[15].Value = model.pid;
            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

        public DataTable queryUserInforById(String userId)
        {
            string sqlstr = @"select 
	                            [User_ID],
	                            [Login_Name],
	                            [Login_Password],
	                            [Username],
	                            [Gender],
	                            [Birthday],
	                            [Birth_Place],
	                            [ID_Number],
	                            [Mobile],
	                            [Email],
	                            [User_Description],
	                            [Question],
	                            [Answer_Question],
	                            [Enabled],
	                            [Manager_Flag],
	                            [Role_ID],
	                            [Depart_Name],
	                            [companyID]
                            FROM
                            Hanmote_Base_User  where User_ID='" + userId + "'";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }

        public void updateUserInfor(SystemUserOrganicRelationshipModel model)
        {
            String sqlText = @"Update [Hanmote_Base_User] set
	                            [Login_Name] = @Login_Name,
	                            [Login_Password] = @Login_Password,
	                            [Username] = @Username,
	                            [Gender] = @Gender,
	                            [Birthday] = @Birthday,
	                            [Birth_Place] = @Birth_Place,
	                            [Mobile] = @Mobile,
	                            [Email] = @Email,
	                            [User_Description] = @User_Description,
	                            [Enabled] = @Enabled,
	                            [Manager_Flag] = @Manager_Flag,
	                            [Role_ID] = @Role_ID,
	                            [Depart_Name] = @Depart_Name,
	                            [companyID] = @companyID ,[pid] = @pid where [User_ID]=@User_ID ";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.Date),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Depart_Name",SqlDbType.VarChar),
                new SqlParameter("@companyID",SqlDbType.VarChar),
                new SqlParameter("@pid",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = model.sysUser_ID;
            sqlParameter[1].Value = model.Login_Name;
            sqlParameter[2].Value = model.Login_Password;
            sqlParameter[3].Value = model.Username;
            sqlParameter[4].Value = model.Gender;
            sqlParameter[5].Value = model.Birthday;
            sqlParameter[6].Value = model.Birth_Place;
            sqlParameter[7].Value = model.Mobile;
            sqlParameter[8].Value = model.Email;
            sqlParameter[9].Value = model.User_Description;
            sqlParameter[10].Value = model.status;
            sqlParameter[11].Value = 0;
            sqlParameter[12].Value = model.jobPosition;
            sqlParameter[13].Value = model.departName;
            sqlParameter[14].Value = model.compangClass;
            sqlParameter[15].Value = model.pid;
            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }
        public DataTable getUserInfoByRoleName(String role)
        {
            String sql = @"SELECT
	                        hbu.User_ID as ID,
	                        hbu.Username as name,
	                        rol.Role_Name as RoleName,
	                        bor.Organization_Name as CompanyClass
                        FROM
	                        Hanmote_Base_User hbu,
	                        Base_Role rol,
	                        Base_Organization bor
                        WHERE
	                        hbu.companyID = bor.Organization_ID
                        AND rol.Role_ID = hbu.Role_ID and rol.Role_Name like '%" + role + "%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        //得到物料组名字
        public DataTable getMtGroupName()
        {
            string sqlstr = "select Description from Material_Group";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }

        public void insertHanmote_User_MtGroupName(string userID, string name, string purName, int type)
        {
            if (type != 2)
            {
                string sqlstr = "insert into Hanmote_User_MtGroupName(User_ID,Mt_Group_Name,Description,Pur_Group_Name,Type) values('" + userID + "','" + name + "','关系说明','" + purName + "',1)";
                string sql = "update Porg_MtGroup_Relationship set Flag = 1 where MtGroup_Name='" + name + "' and Porg_Name='" + purName + "' ";
                DBHelper.ExecuteNonQuery(sql);
                DBHelper.ExecuteNonQuery(sqlstr);
            }
            else//评估员
            {
                string sqlstr = "insert into Hanmote_User_MtGroupName(User_ID,Mt_Group_Name,Description,Pur_Group_Name,Type) values('" + userID + "','" + name + "','关系说明','" + purName + "',0)";
                string sql = "update Porg_MtGroup_Relationship set mFlag = 0 where MtGroup_Name='" + name + "' and Porg_Name='" + purName + "' ";
                DBHelper.ExecuteNonQuery(sql);
                DBHelper.ExecuteNonQuery(sqlstr);
            }
        }
        public DataTable findAllSystemUserOrganicRelationshipInfoByCondition(string userName, string departName,int type)
        {
            StringBuilder sqlText = null;
            if(type!=1)
            {
                sqlText = new StringBuilder(
                                     @"SELECT
	                                        hu.User_ID AS 员工编号,
	                                        hu.Username AS 姓名,
	                                        hu.Mobile AS 联系方式,
	                                        CASE
                                        WHEN hu.Enabled = 0 THEN
	                                        '在籍'
                                        WHEN hu.Enabled = 1 THEN
	                                        '已注销'
                                        END AS 状态,
                                         b.Organization_Name AS 所属公司,
                                         r.Role_Name AS 职务,
                                        hu.Depart_Name as 所属部门,
                                        hu.email as 邮箱
                                        FROM
	                                        Hanmote_Base_User hu,
	                                        Base_Organization b,
	                                        Base_Role r
                                        WHERE
	                                        b.Organization_ID = hu.companyID
                                        AND r.Role_ID = hu.Role_ID and r.Company_ID = b.Organization_ID and hu.Manager_Flag !=1  ");
            }
            else
            {
                sqlText = new StringBuilder(
                                    @"SELECT
	                                        hu.User_ID AS 员工编号,
	                                        hu.Username AS 姓名,
	                                        hu.Mobile AS 联系方式,
	                                        CASE
                                        WHEN hu.Enabled = 0 THEN
	                                        '在籍'
                                        WHEN hu.Enabled = 1 THEN
	                                        '已注销'
                                        END AS 状态,
                                         b.Organization_Name AS 所属公司,
                                         r.Role_Name AS 职务,
                                        hu.Depart_Name as 所属部门,
                                        hu.email as 邮箱
                                        FROM
	                                        Hanmote_Base_User hu,
	                                        Base_Organization b,
	                                        Base_Role r
                                        WHERE
	                                        b.Organization_ID = hu.companyID
                                        AND r.Role_ID = hu.Role_ID and r.Role_ID = '2018012200001' and r.Company_ID = b.Organization_ID and hu.Manager_Flag !=1  ");
            }
            if (!String.IsNullOrEmpty(userName))
            {
                sqlText.Append(" and hu.Username like '%" + userName + "%'");
            }
            if (!String.IsNullOrEmpty(departName))
            {
                sqlText.Append(" and hu.Depart_Name like '%" + departName + "%'");
            }
            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }
        public DataTable findAllPurGroupName()
        {
            string sqlstr = "select Buyer_org as id,Buyer_Org_Name as name from Buyer_Org";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        public DataTable findAllMtGroupNameByPurGroupName(string name, string UserID, int type)
        {
            string sqlstr = "";
            string isHave = "";
            if (type == 1)
            {
                isHave = "select count(*) from Hanmote_User_MtGroupName where User_ID='" + UserID + "' and Type = 1";
            }
            else
            {
                isHave = "select count(*) from Hanmote_User_MtGroupName where User_ID='" + UserID + "' and Type = 0";
            }

            DataTable count = DBHelper.ExecuteQueryDT(isHave);
            //表示修改
            if (count != null && count.Rows.Count > 0 && int.Parse(count.Rows[0][0].ToString()) > 0)
            {
                if (name.Equals("全部") || name.Equals("all"))
                {
                    if (type == 1)//主审关系
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship where Flag = 0  
                            UNION
                            SELECT 
                                pr.MtGroup_ID AS Material_Group,
	                            pr.MtGroup_Name AS Description,
	                            pr.Porg_Name AS MtPorGroup,
	                            pr.Porg_ID AS PurGroupID
	                        FROM  Porg_MtGroup_Relationship pr,Hanmote_User_MtGroupName hm where hm.User_ID='" + UserID + "' and hm.Mt_Group_Name = pr.MtGroup_Name and pr.Porg_Name = Pur_Group_Name and Type = 1";
                    }
                    else//评估员
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship ";
                    }
                    /**
                     * 
                     *  where mFlag = 0  
                            UNION
                            SELECT 
                                pr.MtGroup_ID AS Material_Group,
	                            pr.MtGroup_Name AS Description,
	                            pr.Porg_Name AS MtPorGroup,
	                            pr.Porg_ID AS PurGroupID
	                        FROM  Porg_MtGroup_Relationship pr,Hanmote_User_MtGroupName hm where hm.User_ID='" + UserID + "' and hm.Mt_Group_Name = pr.MtGroup_Name and pr.Porg_Name = Pur_Group_Name  and hm.type = 0";
                     * **/
                }
                else
                {
                    if (type == 1)//主审
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship where Flag = 0  and Porg_Name = '" + name + @"' 
                            UNION
                            SELECT 
                                pr.MtGroup_ID AS Material_Group,
	                            pr.MtGroup_Name AS Description,
	                            pr.Porg_Name AS MtPorGroup,
	                            pr.Porg_ID AS PurGroupID
	                        FROM  Porg_MtGroup_Relationship pr,Hanmote_User_MtGroupName hm where hm.User_ID='" + UserID + "' and hm.Mt_Group_Name = pr.MtGroup_Name and pr.Porg_Name = Pur_Group_Name and pr.Porg_Name='" + name + "' and hm.Type = 1";
                    }
                    else//评估员
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship where Porg_Name = '" + name + @"'";
                        /**
                         * where mFlag = 0 and Porg_Name = '" + name + @"' 
                            UNION
                            SELECT 
                                pr.MtGroup_ID AS Material_Group,
	                            pr.MtGroup_Name AS Description,
	                            pr.Porg_Name AS MtPorGroup,
	                            pr.Porg_ID AS PurGroupID
	                        FROM  Porg_MtGroup_Relationship pr,Hanmote_User_MtGroupName hm where hm.User_ID='" + UserID + "' and hm.Mt_Group_Name = pr.MtGroup_Name and pr.Porg_Name = Pur_Group_Name and pr.Porg_Name='" + name + "' and hm.type = 0";
                         * **/
                    }
                }

            }
            //表示新建
            else
            {
                if (type == 1)//主审
                {
                    if (name.Equals("全部") || name.Equals("all"))
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship where Flag = 0 ";
                    }
                    else
                    {
                        sqlstr = @"SELECT
	                                MtGroup_ID AS Material_Group,
	                                MtGroup_Name AS Description,
	                                Porg_Name AS MtPorGroup,
	                                Porg_ID AS PurGroupID
                                FROM
	                                Porg_MtGroup_Relationship
                                WHERE Flag = 0 and  Porg_Name = '" + name + "'";
                    }
                }
                else
                {
                    //评估员
                    if (name.Equals("全部") || name.Equals("all"))
                    {
                        sqlstr = @"SELECT
	                            MtGroup_ID AS Material_Group,
	                            MtGroup_Name AS Description,
	                            Porg_Name AS MtPorGroup,
	                            Porg_ID AS PurGroupID
                            FROM
	                            Porg_MtGroup_Relationship where mFlag = 0 ";
                    }
                    else
                    {
                        sqlstr = @"SELECT
	                                MtGroup_ID AS Material_Group,
	                                MtGroup_Name AS Description,
	                                Porg_Name AS MtPorGroup,
	                                Porg_ID AS PurGroupID
                                FROM
	                                Porg_MtGroup_Relationship
                                WHERE mFlag = 0 and  Porg_Name = '" + name + "'";
                    }
                }
            }
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        /// <summary>
        /// 删除关系
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <param name="a"> 1 删除前有的评估员身份 2 删除前有的主审身份</param>
        public void deleteHamote_User_MtGroup(string sysUser_ID, int a)
        {
            if (a == 1)
            {
                //先查出来置为0
                string isHave = "select * from Hanmote_User_MtGroupName where User_ID='" + sysUser_ID + "' and Type = 0";
                DataTable count = DBHelper.ExecuteQueryDT(isHave);
                if (count != null && count.Rows.Count > 0)
                {
                    for (int i = 0; i < count.Rows.Count; i++)
                    {
                        string sql = "update Porg_MtGroup_Relationship set mFlag = 0 where MtGroup_Name='" + count.Rows[i][1].ToString() + "' and Porg_Name='" + count.Rows[i][2].ToString() + "'";
                        DBHelper.ExecuteNonQuery(sql);
                    }

                }
                string delstr1 = "delete from Hanmote_User_MtGroupName where User_ID='" + sysUser_ID + "'";
                DBHelper.ExecuteNonQuery(delstr1);
                string delstr = " delete from HanmoteBaseUser_EvalCode where UserID='" + sysUser_ID + "'";
                DBHelper.ExecuteNonQuery(delstr);
            }
            if (a == 2)
            {
                //先查出来置为0
                string isHave = "select * from Hanmote_User_MtGroupName where User_ID='" + sysUser_ID + "' and Type  = 1";
                DataTable count = DBHelper.ExecuteQueryDT(isHave);
                if (count != null && count.Rows.Count > 0)
                {
                    for (int i = 0; i < count.Rows.Count; i++)
                    {
                        string sql = "update Porg_MtGroup_Relationship set Flag = 0 where MtGroup_Name='" + count.Rows[i][1].ToString() + "' and Porg_Name='" + count.Rows[i][2].ToString() + "'";
                        DBHelper.ExecuteNonQuery(sql);
                    }

                }
                string delstr1 = "delete from Hanmote_User_MtGroupName where User_ID='" + sysUser_ID + "'";
                DBHelper.ExecuteNonQuery(delstr1);
                string delstr = " delete from HanmoteBaseUser_EvalCode where UserID='" + sysUser_ID + "'";
                DBHelper.ExecuteNonQuery(delstr);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysUser_ID"></param>
        /// <param name="v"></param>
        /// <param name="purName"></param>
        /// <param name="type"></param>
        public void updateHanmote_User_MtGroupName(string sysUser_ID, string v, string purName, int type)
        {
            if (type != 2)//主审
            {
                string sqlstr = "INSERT INTO  Hanmote_User_MtGroupName ([User_ID], [Mt_Group_Name], [Pur_Group_Name], [Description],[Type]) VALUES ('" + sysUser_ID + "','" + v + "','" + purName + "','关系说明',1)";
                string sql = "update Porg_MtGroup_Relationship set Flag = 1 where MtGroup_Name='" + v + "' and Porg_Name='" + purName + "'";
                DBHelper.ExecuteNonQuery(sql);
                DBHelper.ExecuteNonQuery(sqlstr);
            }
            else
            {
                //评估员
                string sqlstr = "INSERT INTO  Hanmote_User_MtGroupName ([User_ID], [Mt_Group_Name], [Pur_Group_Name], [Description],[Type]) VALUES ('" + sysUser_ID + "','" + v + "','" + purName + "','关系说明',0)";
                string sql = "update Porg_MtGroup_Relationship set mFlag = 0 where MtGroup_Name='" + v + "' and Porg_Name='" + purName + "'";
                DBHelper.ExecuteNonQuery(sql);
                DBHelper.ExecuteNonQuery(sqlstr);
            }
        }
        public string getUpUserNameById(string sysUser_ID)
        {
            string sqlstr = "select  Username from Hanmote_Base_User where User_ID in (select pid from Hanmote_Base_User where User_ID= '" + sysUser_ID + "')";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlstr);
            if (dt.Rows.Count > 0)
            {
                return DBHelper.ExecuteQueryDT(sqlstr).Rows[0][0].ToString();
            }
            return "无直接上级";
        }
        public string getMtGroupName(string sysUser_ID)
        {
            string sqlstr = "select  Mt_Group_Name from Hanmote_User_MtGroupName  where User_ID='" + sysUser_ID + "' and Type = 1";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlstr);
            string name = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                name += dt.Rows[i][0].ToString() + ",";
            }
            return name;
        }
        public void deleteHanmoteBaseUser_EvalCode(string v, string code)
        {
            //先检查有无记录有责先删再插 同时删除关系转变
            string sqlupdate = "delete from  HanmoteBaseUser_EvalCode where UserID='" + v + "'";
            DBHelper.ExecuteNonQuery(sqlupdate);
            string delstr = "delete from Hanmote_User_MtGroupName  where User_ID='" + v + "'";
            DBHelper.ExecuteNonQuery(delstr);
        }
        public void insertHanmoteBaseUser_EvalCode(string v, string code,string codeDesc)
        {
            string sqlstr = "insert into HanmoteBaseUser_EvalCode (UserID,codeID,Description) values ('" + v + "','" + code + "','"+codeDesc+"')";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
        public string getEvalCodeByUserId(string sysUser_ID)
        {
            string sqlstr = "select codeID from  HanmoteBaseUser_EvalCode where UserID = '" + sysUser_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlstr);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0] != null && !dt.Rows[0][0].ToString().Equals(""))
                {
                    return dt.Rows[0][0].ToString();
                }
                else return "无";
            }

            return "无";
        }

        public string getPIDByUserId(string sysUser_ID)
        {
            string sql = "select pid from Hanmote_Base_User where User_ID='" + sysUser_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count < 0) return "null";
            else
            {
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable getMainEvaulatorSelectdMtGroupAndPurGroupByUserID(string userID, string purGroupName)
        {
            string sql = @"select   Mt_Group_Name,Pur_Group_Name  from  Hanmote_User_MtGroupName where User_ID='" + userID + "'";

            return DBHelper.ExecuteQueryDT(sql);
        }

        public string getMtMethodGroupName(string sysUser_ID)
        {
            string sqlstr = "select  Mt_Group_Name from Hanmote_User_MtGroupName  where User_ID='" + sysUser_ID + "' and Type = 0";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlstr);
            string name = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                name += dt.Rows[i][0].ToString() + ",";
            }
            return name;
        }
        
    }
}
