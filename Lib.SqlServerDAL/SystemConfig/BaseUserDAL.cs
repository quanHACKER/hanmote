﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{  
    public class BaseUserDAL : BaseUserIDAL
    {
        /// <summary>
        /// 验证用户登录信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        public DataTable validateUserInformation(BaseUserModel baseUserModel)
        {
            String sqlText = @"select * from Hanmote_Base_user  where Login_Name=@Login_Name and Login_Password=@Login_Password and Manager_Flag=@Manager_Flag";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Manager_Flag",SqlDbType.Int)
            };
            sqlParameter[0].Value = baseUserModel.Login_Name;
            sqlParameter[1].Value = baseUserModel.Login_Password;
            sqlParameter[2].Value = baseUserModel.Manager_Flag;
            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 查询当前最大的用户编号
        /// </summary>
        /// <returns></returns>
        public DataTable queryBiggestUserID(String today)
        {
            String sqlText = "select max(User_ID) from Base_User where User_ID LIKE @UserID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@UserID",SqlDbType.NChar)
            };
            sqlParameter[0].Value = today + "%";

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 计算记录条数
        /// </summary>
        /// <returns></returns>
        public DataTable calculateRecordNumber()
        {
            String sqlText = "select count(User_ID) from Base_User";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 查询用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        public DataTable queryUserInformationById(BaseUserModel baseUserModel)
        {
            String sqlText = "select * from Base_User where User_ID=@User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter) ;
        }

        /// <summary>
        /// 根据查询条件查询用户信息
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public DataTable queryUserInformationBySearchCondition(BaseUserModel userModel)
        {
            StringBuilder sqlText = new StringBuilder(@"SELECT
	                                                        User_ID,
	                                                        Birth_Place,
	                                                        Login_Name,
	                                                        Username,
	                                                        CASE Gender
                                                        WHEN 0 THEN
	                                                        '男'
                                                        WHEN 1 THEN
	                                                        '女'
                                                        END AS Gender,
                                                         Mobile,
                                                         Email,
                                                         CASE Enabled
                                                        WHEN 0 THEN
	                                                        '禁用'
                                                        WHEN 1 THEN
	                                                        '活动'
                                                        END AS Enabled,
                                                         CASE Manager_Flag
                                                        WHEN 0 THEN
	                                                        '否'
                                                        WHEN 1 THEN
	                                                        '是'
                                                        END AS Manager_Flag
                                                        FROM
	                                                        Base_User
");
            StringBuilder tmpText = new StringBuilder("");
            
            if (!String.IsNullOrEmpty(userModel.User_ID))
            {
                tmpText.Append(" and User_ID=");
                tmpText.Append("'" + userModel.User_ID + "'");
            }

            if (!String.IsNullOrEmpty(userModel.Username))
            {
                tmpText.Append(" and Username like ");
                tmpText.Append("'%" + userModel.Username + "%'");
            }

            if (userModel.Gender == 0 || userModel.Gender == 1)
            {
                tmpText.Append(" and Gender=");
                tmpText.Append(Convert.ToString(userModel.Gender));
            }

            if (!String.IsNullOrEmpty(userModel.Birth_Place))
            {
                tmpText.Append(" and Birth_Place like ");
                tmpText.Append("'%" + userModel.Birth_Place + "%'");
            }

            if (!String.IsNullOrEmpty(userModel.ID_Number))
            {
                tmpText.Append(" and ID_Number like ");
                tmpText.Append("'%" + userModel.ID_Number + "%'");
            }

            if (!String.IsNullOrEmpty(userModel.Mobile))
            {
                tmpText.Append(" and Mobile like ");
                tmpText.Append("'%" + userModel.Mobile + "%'");
            }
            tmpText.Append("and companyID like ");
            tmpText.Append("'%"+BaseUserModel.Supplier_Id+"%'");
            if (tmpText.Length > 0)
            {
                tmpText.Remove(0, 4);
                sqlText.Append(" where ");
                sqlText.Append(tmpText);
            }

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        /// <summary>
        /// 查询所有用户的信息
        /// </summary>
        /// <param name="pageNum">每页要显示的数目</param>
        /// <param name="frontNum">前面已经显示过的数目</param>
        /// <returns></returns>
        public DataTable queryAllUserInformation(int pageNum,int frontNum)
        {
            String sqlText = String.Format(@" SELECT
	                                                Supplier_Id AS Supplier_Id,
	                                                Supplier_Name AS Supplier_Name,
	                                                email AS Email,
	                                                ManagerName AS Username,
	                                                mobile AS Mobile,
	                                                CASE
                                                WHEN Enable = '0' THEN
	                                                '在籍'
                                                WHEN Enable = '1' THEN
	                                                '已注销'
                                                END AS Enabled
                                                FROM
	                                                Supplier_MainAccount", pageNum,frontNum);
            
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 插入一个新用户
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        public void insertUserInformation(BaseUserModel baseUserModel)
        {
            String sqlText = @"INSERT INTO [Base_User]([User_ID],[Login_Name],[Login_Password],[Username]
                                                      ,[Gender],[Birthday],[Birth_Place],[ID_Number]
                                                      ,[Mobile],[Email],[User_Description]
                                                      ,[Question],[Answer_Question],[Enabled],[Manager_Flag]
                                                      ,[Generate_Time],[Generate_User_ID],[Generate_Username],[Modify_Time]
                                                      ,[Modify_User_ID],[Modify_Username],[companyID],[companyName])
                               VALUES(@User_ID,@Login_Name,@Login_Password,@Username,
                                      @Gender,@Birthday,@Birth_Place,@ID_Number,
                                      @Mobile,@Email,@User_Description,
                                      @Question,@Answer_Question,@Enabled,@Manager_Flag,
                                      @Generate_Time,@Generate_User_ID,@Generate_Username,@Modify_Time,
                                      @Modify_User_ID,@Modify_Username,@companyID,@companyName)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.Date),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@ID_Number",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Question",SqlDbType.VarChar),
                new SqlParameter("@Answer_Question",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Generate_Time",SqlDbType.DateTime),
                new SqlParameter("@Generate_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Generate_Username",SqlDbType.VarChar),
                new SqlParameter("@Modify_Time",SqlDbType.DateTime),
                new SqlParameter("@Modify_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Modify_Username",SqlDbType.VarChar),
                new SqlParameter("@companyuID",SqlDbType.VarChar),
                new SqlParameter("@companyName",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = baseUserModel.Login_Name;
            sqlParameter[2].Value = baseUserModel.Login_Password;
            sqlParameter[3].Value = baseUserModel.Username;
            sqlParameter[4].Value = baseUserModel.Gender;
            sqlParameter[5].Value = baseUserModel.Birthday;
            sqlParameter[6].Value = baseUserModel.Birth_Place;
            sqlParameter[7].Value = baseUserModel.ID_Number;
            sqlParameter[8].Value = baseUserModel.Mobile;
            sqlParameter[9].Value = baseUserModel.Email;
            sqlParameter[10].Value = baseUserModel.User_Description;
            sqlParameter[11].Value = baseUserModel.Question;
            sqlParameter[12].Value = baseUserModel.Answer_Question;
            sqlParameter[13].Value = baseUserModel.Enabled;
            sqlParameter[14].Value = baseUserModel.Manager_Flag;
            sqlParameter[15].Value = baseUserModel.Generate_Time;
            sqlParameter[16].Value = baseUserModel.Generate_User_ID;
            sqlParameter[17].Value = baseUserModel.Generate_Username;
            sqlParameter[18].Value = baseUserModel.Modify_Time;
            sqlParameter[19].Value = baseUserModel.Modify_User_ID;
            sqlParameter[20].Value = baseUserModel.Modify_Username;
            sqlParameter[19].Value = BaseUserModel.Supplier_Id;
            sqlParameter[20].Value = BaseUserModel.Supplier_Name;
            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 更新用户基本信息
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        public void updateUserInformation(BaseUserModel baseUserModel)
        {
            String sqlText = @"update Base_User set [Login_Name]=@Login_Name,[Login_Password]=@Login_Password,[Username]=@Username,[Gender]=@Gender,
                                                    [Birthday]=@Birthday,[Birth_Place]=@Birth_Place,[ID_Number]=@ID_Number,[Mobile]=@Mobile,
                                                    [Email]=@Email,[User_Description]=@User_Description,[Question]=@Question,[Answer_Question]=@Answer_Question,
                                                    [Enabled]=@Enabled,[Manager_Flag]=@Manager_Flag,[Modify_Time]=@Modify_Time,[Modify_User_ID]=@Modify_User_ID,[Modify_Username]=@Modify_Username
                               where [User_ID]=@User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.VarChar),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@ID_Number",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Question",SqlDbType.VarChar),
                new SqlParameter("@Answer_Question",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Modify_Time",SqlDbType.DateTime),
                new SqlParameter("@Modify_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Modify_Username",SqlDbType.VarChar),
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseUserModel.Login_Name;
            sqlParameter[1].Value = baseUserModel.Login_Password;
            sqlParameter[2].Value = baseUserModel.Username;
            sqlParameter[3].Value = baseUserModel.Gender;
            sqlParameter[4].Value = baseUserModel.Birthday;
            sqlParameter[5].Value = baseUserModel.Birth_Place;
            sqlParameter[6].Value = baseUserModel.ID_Number;
            sqlParameter[7].Value = baseUserModel.Mobile;
            sqlParameter[8].Value = baseUserModel.Email;
            sqlParameter[9].Value = baseUserModel.User_Description;
            sqlParameter[10].Value = baseUserModel.Question;
            sqlParameter[11].Value = baseUserModel.Answer_Question;
            sqlParameter[12].Value = baseUserModel.Enabled;
            sqlParameter[13].Value = baseUserModel.Manager_Flag;
            sqlParameter[14].Value = baseUserModel.Modify_Time;
            sqlParameter[15].Value = baseUserModel.Modify_User_ID;
            sqlParameter[16].Value = baseUserModel.Modify_Username;
            sqlParameter[17].Value = baseUserModel.User_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 设置用户是否可用
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="adminModel"></param>
        /// <param name="enable"></param>
        public void setUserEnable(BaseUserModel baseUserModel)
        {
            String sqlText = "update Base_User set Enabled=@Enabled where User_ID=@User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = baseUserModel.Enabled;
            sqlParameter[1].Value = baseUserModel.User_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }
        /// <summary>
        /// 企业人员信息选择
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public DataTable getSysUserList(int v1, int v2)
        {
            string sqlstr = @"SELECT
	                            User_ID,
	                            Username,
                            CASE
                            WHEN Gender = '0' THEN
	                            '男'
                            WHEN Gender = '1' THEN
	                            '女'
                            END AS Gender,
	                            Mobile,
	                            Email,
	                            CASE
                            WHEN Enabled = '0' THEN
	                            '在籍'
                            WHEN Enabled = '1' THEN
	                            '已注销'
                            END AS Enabled,
                             r.Role_Name AS Manager_Flag,
                             b.Organization_Name AS companyName
                            FROM
	                            Hanmote_Base_User hub,
	                            Base_Role r,
	                            Base_Organization b
                            WHERE
	                            hub.Role_ID = r.Role_ID
                            AND b.Organization_ID = hub.companyID
                            AND r.Company_ID = b.Organization_ID";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }

        public DataTable getAuthorNodes(string user_ID)
        {
            string sqlstr = @"SELECT
	                            bfpo.Menu_Name,
                                bfpo.Window_Name
                            FROM
	                            Join_User_Function_Permission_Object jufpo,
	                            Base_Function_Permission_Object bfpo
                            WHERE
	                            User_ID = '" + user_ID+@"'
                            AND bfpo.Permission_ID = jufpo.Permission_ID";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
    }
}

