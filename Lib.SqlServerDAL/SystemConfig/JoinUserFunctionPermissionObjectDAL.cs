﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinUserFunctionPermissionObjectDAL : JoinUserFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户的直接授权权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        public DataTable queryUserFunctionByID(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select A.Permission_ID,A.Permission_Name,A.Description
                               from (select * from Base_Function_Permission_Object where Menu_Name=@Menu_Name) A
                               inner join (select Permission_ID from Join_User_Function_Permission_Object where User_ID = @User_ID) B
                               on A.Permission_ID = B.Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Menu_Name",SqlDbType.VarChar),
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = basePermissionModel.Menu_Name;
            sqlParameter[1].Value = baseUserModel.User_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 查询用户是否直接具有某个权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        public DataTable queryUserFunctionPermissionById(BaseUserModel baseUserModel,int permissionId)
        {
            String sqlText = @"SELECT Permission_ID 
                               FROM Join_User_Function_Permission_Object
                               WHERE User_ID=@User_ID AND Permission_ID=@Permission_ID;";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = permissionId;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 收回用户被授予的权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokePermission(JoinUserFunctionPermissionObjectModel objModel)
        {
            String sqlText = "delete from Join_User_Function_Permission_Object where User_ID=@User_ID and Permission_ID=@Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = objModel.User_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 授予用户新的权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizePermission(JoinUserFunctionPermissionObjectModel objModel)
        {
            String sqlText = "insert into Join_User_Function_Permission_Object(User_ID,Permission_ID) values(@User_ID,@Permission_ID)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = objModel.User_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

    }
}
