﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinGroupFunctionPermissionObjectDAL : JoinGroupFunctionPermissionObjectIDAL
    {
        /// <summary>
        /// 查询一个用户所有组具有的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromGroup(BaseUserModel baseUserModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select C.Permission_ID,C.Permission_Name,C.Description
                               from (select Group_ID from Join_User_Group where User_ID=@User_ID) A
                               inner join Join_Group_Function_Permission_Object B on A.Group_ID=B.Group_ID
                               inner join Base_Function_Permission_Object C on B.Permission_ID=C.Permission_ID where C.Menu_Name=@Menu_Name";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Menu_Name",SqlDbType.VarChar),
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = basePermissionModel.Menu_Name;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 查询某用户所在的组是否有permissionId对应的权限
        /// </summary>
        /// <param name="baseUserModel"></param>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        public DataTable queryPermissionByIdFormGroup(BaseUserModel baseUserModel, int permissionId)
        {
            String sqlText = @"SELECT B.Permission_ID 
                               FROM Join_User_Group A,Join_Group_Function_Permission_Object B
                               WHERE A.User_ID=@User_ID AND B.Permission_ID=@Permission_ID AND  A.Group_ID=B.Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = baseUserModel.User_ID;
            sqlParameter[1].Value = permissionId;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 查询一个组所具有的权限
        /// </summary>
        /// <param name="baseGroupModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryPermissionFromSingleGroup(BaseGroupModel baseGroupModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            String sqlText = @"select A.Permission_ID,A.Permission_Name,A.Description
                               from Base_Function_Permission_Object A,Join_Group_Function_Permission_Object B
                               where A.Menu_Name=@Menu_Name and B.Group_ID=@Group_ID and A.Permission_ID=B.Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Menu_Name",SqlDbType.VarChar),
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = basePermissionModel.Menu_Name;
            sqlParameter[1].Value = baseGroupModel.Group_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 收回组权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            String sqlText = "delete from Join_Group_Function_Permission_Object where Group_ID=@Group_ID and Permission_ID=@Permission_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = objModel.Group_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 授予组权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            String sqlText = "insert into Join_Group_Function_Permission_Object(Group_ID,Permission_ID) values(@Group_ID,@Permission_ID)";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@Group_ID",SqlDbType.VarChar),
                new SqlParameter("@Permission_ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = objModel.Group_ID;
            sqlParameter[1].Value = objModel.Permission_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 收回一个组对应的所有权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeGroupAllPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            String sql = "delete from Join_Group_Function_Permission_Object where Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = objModel.Group_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

    }
}
