﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class BaseOrganizationDAL : BaseOrganizationIDAL
    {
        /// <summary>
        /// 查询最大的可用组织机构编号
        /// </summary>
        /// <returns></returns>
        public DataTable queryBiggestOrganizationID(String today)
        {
            String sql = "select max(Organization_ID) from Base_Organization where Organization_ID LIKE @Organization_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = today + "%";

            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }

        /// <summary>
        /// 查询父级节点的Right_Value
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public DataTable queryParentRightValue(String organizationID)
        {
            String sql = "select Right_Value from Base_Organization where Organization_ID=@Organization_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = organizationID;
            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }
        
        /// <summary>
        /// 插入新的组织机构
        /// </summary>
        /// <param name="organiztionModel"></param>
        public void insertOrganization(BaseOrganizationModel organiztionModel)
        {
            String sql1 = @"update Base_Organization set Right_Value=Right_Value+2 where Right_Value>=@Right_Value";
            String sql2 = @"update Base_Organization set Left_Value=Left_Value+2 where Left_Value>=@Left_Value";
            String sql3 = @"insert into Base_Organization(Organization_ID,Organization_Name,Left_Value,Right_Value,Level,Description)
                                         values(@Organization_ID,@Organization_Name,@Left_Value,@Right_Value,@Level,@Description)";

            SqlParameter[] sqlParameter1 = new SqlParameter[] {
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter1[0].Value = organiztionModel.Left_Value;

            SqlParameter[] sqlParameter2 = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int)
            };
            sqlParameter2[0].Value = organiztionModel.Left_Value;

            SqlParameter[] sqlParameter3 = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar),
                new SqlParameter("@Organization_Name",SqlDbType.VarChar),
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int),
                new SqlParameter("@Level",SqlDbType.Int),
                new SqlParameter("@Description",SqlDbType.VarChar)
            };
            sqlParameter3[0].Value = organiztionModel.Organization_ID;
            sqlParameter3[1].Value = organiztionModel.Organization_Name;
            sqlParameter3[2].Value = organiztionModel.Left_Value;
            sqlParameter3[3].Value = organiztionModel.Right_Value;
            sqlParameter3[4].Value = organiztionModel.Level;
            sqlParameter3[5].Value = organiztionModel.Description;

            List<String> sqlList = new List<String> {sql1,sql2,sql3 };
            List<SqlParameter[]> sqlParameterList = new List<SqlParameter[]> { sqlParameter1, sqlParameter2, sqlParameter3,};

            DBHelper.ExecuteNonQuery(sqlList, sqlParameterList);
        }

        /// <summary>
        /// 查询组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        public DataTable queryOrganizationInfor(BaseOrganizationModel orgModel)
        {
            String sql = "select * from Base_Organization where Organization_ID=@Organization_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = orgModel.Organization_ID;
            return DBHelper.ExecuteQueryDT(sql, sqlParameter);
        }

        /// <summary>
        /// 更新组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        public void updateOrganizationInfor(BaseOrganizationModel orgModel)
        {
            String sql = @"update Base_Organization
                                       set Organization_Name=@Organization_Name,Description=@Description
                                       where Organization_ID=@Organization_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@Organization_Name",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@Organization_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = orgModel.Organization_Name;
            sqlParameter[1].Value = orgModel.Description;
            sqlParameter[2].Value = orgModel.Organization_ID;
            DBHelper.ExecuteNonQuery(sql, sqlParameter);
        }

        /// <summary>
        /// 查询所有组织机构信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllOrganizationInfor()
        {
            String sql = "select Organization_ID,Organization_Name,Left_Value,Right_Value,Level from Base_Organization order by Level ASC";
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 查询当前组织机构的所有下属机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        /// <returns></returns>
        public DataTable queryOffspringOrganization(BaseOrganizationModel orgModel)
        {
            String sql = "select Organization_ID from Base_Organization where Left_Value>=@Left_Value and Right_Value<=@Right_Value";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter[0].Value = orgModel.Left_Value;
            sqlParameter[1].Value = orgModel.Right_Value;
            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }

        /// <summary>
        /// 删除当前组织机构及所有下属机构，并更新相应节点的左右值
        /// </summary>
        /// <param name="orgModel"></param>
        public void deleteOffspringOrganizationAndUpdateNodes(BaseOrganizationModel orgModel)
        {
            String sql1 = "delete from Base_Organization where Left_Value>=@Left_Value and Right_Value<=@Right_Value";
            SqlParameter[] sqlParameter1 = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter1[0].Value = orgModel.Left_Value;
            sqlParameter1[1].Value = orgModel.Right_Value;

            String sql2 = "update Base_Organization set Right_Value=Right_Value-@width where Right_Value>@Right_Value";
            SqlParameter[] sqlParameter2 = new SqlParameter[] { 
                new SqlParameter("@width",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter2[0].Value = orgModel.Right_Value - orgModel.Left_Value + 1;
            sqlParameter2[1].Value = orgModel.Right_Value;

            String sql3 = "update Base_Organization set Left_Value=Left_Value-@width where Left_Value>@Right_Value";
            SqlParameter[] sqlParameter3 = new SqlParameter[] { 
                new SqlParameter("@width",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter3[0].Value = orgModel.Right_Value - orgModel.Left_Value + 1;
            sqlParameter3[1].Value = orgModel.Right_Value;

            List<String> sqlList = new List<String> { sql1,sql2,sql3};
            List<SqlParameter[]> sqlParameterList = new List<SqlParameter[]> { sqlParameter1, sqlParameter2, sqlParameter3};
            
            DBHelper.ExecuteNonQuery(sqlList, sqlParameterList);
        }
    }
}
