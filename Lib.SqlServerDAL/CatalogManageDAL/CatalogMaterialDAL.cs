﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Lib.Model.CatalogPurchase;
using System.Drawing;
using Lib.IDAL.CatalogManageIDAL;

namespace Lib.SqlServerDAL.CatalogManageDAL
{
    public class CatalogMaterialDAL : CatalogMaterialIDAL
    {
        public int addNewCatalogMaterial(CatalogMaterial catalogMaterial) {
            StringBuilder strBui = new StringBuilder("insert into Material_Catalog (ID, Material_ID,")
                .Append("Supplier_ID, Material_Name, Supplier_Name, Material_Type, Material_Standard,")
                .Append("Short_Text, Price, Material_Unit, Currency_Type, Catalog_Purchase_Contract_ID, ")
                .Append("Detail, Image, Create_Time, End_Time, Status")
                .Append(")")
                .Append("values(@ID, @Material_ID,")
                .Append("@Supplier_ID, @Material_Name, @Supplier_Name, @Material_Type, @Material_Standard,")
                .Append("@Short_Text, @Price, @Material_Unit, @Currency_Type, @Catalog_Purchase_Contract_ID, ")
                .Append("@Detail, @Image, @Create_Time, @End_Time, @Status")
                .Append(")");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Name", SqlDbType.VarChar),
                new SqlParameter("@Supplier_Name", SqlDbType.VarChar),
                new SqlParameter("@Material_Type", SqlDbType.VarChar),
                new SqlParameter("@Material_Standard", SqlDbType.VarChar),
                new SqlParameter("@Short_Text", SqlDbType.VarChar),
                new SqlParameter("@Price", SqlDbType.Decimal),
                new SqlParameter("@Material_Unit", SqlDbType.VarChar),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Catalog_Purchase_Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Detail", SqlDbType.VarChar),
                new SqlParameter("@Image", SqlDbType.Image),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = catalogMaterial.Material_ID;
            sqlParas[1].Value = catalogMaterial.Supplier_ID;
            sqlParas[2].Value = catalogMaterial.Material_Name;
            sqlParas[3].Value = catalogMaterial.Supplier_Name;
            sqlParas[4].Value = catalogMaterial.Material_Type;
            sqlParas[5].Value = catalogMaterial.Material_Standard;
            sqlParas[6].Value = catalogMaterial.Short_Text;
            sqlParas[7].Value = catalogMaterial.Price;
            sqlParas[8].Value = catalogMaterial.Material_Unit;
            sqlParas[9].Value = catalogMaterial.Currency_Type;
            sqlParas[10].Value = catalogMaterial.Catalog_Purchase_Contract_ID;
            sqlParas[11].Value = catalogMaterial.Detail;
            //图像的转换
            if (catalogMaterial.Image == null)
            {
                sqlParas[12].Value = null;
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                catalogMaterial.Image.Save(ms, catalogMaterial.Image.RawFormat);
                byte[] byteImage = new Byte[ms.Length];
                byteImage = ms.ToArray();
                sqlParas[12].Value = byteImage;
            }
            sqlParas[13].Value = catalogMaterial.Create_Time;
            sqlParas[14].Value = catalogMaterial.End_Time;
            sqlParas[15].Value = catalogMaterial.Status;
            sqlParas[16].Value = catalogMaterial.ID;

            int result = DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);

            return result;
        }

        /// <summary>
        /// 查询
        /// 此方法有问题，如果数据库中值为null，则取出的是dbnull对象，除了tostring外不能进行其他任何转换，
        /// 因此，在强制转换之前必须判断是不是dbnull对象。（字段create_time,end_time,image,price）
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public CatalogMaterial getCatalogMaterial(string materialID, string supplierID) {
            StringBuilder strBui = new StringBuilder("select * from Material_Catalog where ");
            bool isFirst = true;
            if (!materialID.Equals("")) {
                strBui.Append(" Material_ID = '").Append(materialID).Append("'");
                isFirst = false;
            }
            if (!supplierID.Equals("")) {
                if (!isFirst) {
                    strBui.Append(" and ");
                }
                else
                    isFirst = false;
                strBui.Append("Supplier_ID = '").Append(supplierID).Append("'");
            }
            

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            if(dt.Rows.Count == 0)
                return null;

            DataRow row = dt.Rows[0];
            CatalogMaterial catalogMaterial = new CatalogMaterial();
            catalogMaterial.ID = row["ID"].ToString();
            catalogMaterial.Material_ID = row["Material_ID"].ToString();
            catalogMaterial.Material_Name = row["Material_Name"].ToString();
            catalogMaterial.Material_Standard = row["Material_Standard"].ToString();
            catalogMaterial.Supplier_ID = row["Supplier_ID"].ToString();
            catalogMaterial.Catalog_Purchase_Contract_ID = row["Catalog_Purchase_Contract_ID"].ToString();
            catalogMaterial.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
            catalogMaterial.Currency_Type = row["Currency_Type"].ToString();
            catalogMaterial.Detail = row["Detail"].ToString();
            catalogMaterial.End_Time = Convert.ToDateTime(row["End_Time"].ToString());
            //Image转换
            if (Convert.IsDBNull(row["Image"]))
            {
                catalogMaterial.Image = null;
            }
            else
            {
               byte[] imageByte = (byte[])row["Image"];
               MemoryStream ms = new MemoryStream(imageByte);
               catalogMaterial.Image = new Bitmap(Image.FromStream(ms));
            }

            catalogMaterial.Material_Type = row["Material_Type"].ToString();
            catalogMaterial.Material_Unit = row["Material_Unit"].ToString();
            catalogMaterial.Price = Convert.ToDouble(row["Price"].ToString());
            catalogMaterial.Short_Text = row["Short_Text"].ToString();
            catalogMaterial.Status = row["Status"].ToString();
            catalogMaterial.Supplier_Name = row["Supplier_Name"].ToString();

            return catalogMaterial;
        }

        /// <summary>
        /// 查询
        /// 问题同上
        /// </summary>
        /// <param name="materialCatalogID"></param>
        /// <returns></returns>
        public CatalogMaterial getCatalogMaterial(string materialCatalogID)
        {
            StringBuilder strBui = new StringBuilder("select * from Material_Catalog where ")
                .Append("ID = '").Append(materialCatalogID).Append("'");     

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            if (dt.Rows.Count == 0)
                return null;

            DataRow row = dt.Rows[0];
            CatalogMaterial catalogMaterial = new CatalogMaterial();
            catalogMaterial.ID = row["ID"].ToString();
            catalogMaterial.Material_ID = row["Material_ID"].ToString();
            catalogMaterial.Material_Name = row["Material_Name"].ToString();
            catalogMaterial.Material_Standard = row["Material_Standard"].ToString();
            catalogMaterial.Supplier_ID = row["Supplier_ID"].ToString();
            catalogMaterial.Catalog_Purchase_Contract_ID = row["Catalog_Purchase_Contract_ID"].ToString();
            catalogMaterial.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
            catalogMaterial.Currency_Type = row["Currency_Type"].ToString();
            catalogMaterial.Detail = row["Detail"].ToString();
            catalogMaterial.End_Time = Convert.ToDateTime(row["End_Time"].ToString());
            //Image转换
            if (Convert.IsDBNull(row["Image"]))
            {
                catalogMaterial.Image = null;
            }
            else
            {
                byte[] imageByte = (byte[])row["Image"];
                MemoryStream ms = new MemoryStream(imageByte);
                catalogMaterial.Image = new Bitmap(Image.FromStream(ms));
            }
            catalogMaterial.Material_Type = row["Material_Type"].ToString();
            catalogMaterial.Material_Unit = row["Material_Unit"].ToString();
            catalogMaterial.Price = Convert.ToDouble(row["Price"].ToString());
            catalogMaterial.Short_Text = row["Short_Text"].ToString();
            catalogMaterial.Status = row["Status"].ToString();
            catalogMaterial.Supplier_Name = row["Supplier_Name"].ToString();

            return catalogMaterial;
        }

        /// <summary>
        /// 查询商品信息
        /// </summary>
        /// <param name="materialType"></param>
        /// <param name="supplierID"></param>
        /// <param name="keywords"></param>
        /// <returns>符合要求的商品信息</returns>
        public DataTable getCatalogMaterialDataTable(string materialType, string supplierID, string[] keywords) {
            StringBuilder strBui = new StringBuilder("select * from Material_Catalog ");
            bool isFirst = true;
            if (!materialType.Equals(""))
            {
                isFirst = false;
                strBui.Append(" where Material_Type = '").Append(materialType).Append("'");
            }

            if (!supplierID.Equals(""))
            {
                if (!isFirst)
                {
                    strBui.Append(" and ");
                }
                else
                {
                    isFirst = false;
                    strBui.Append(" where ");
                }
                strBui.Append("Supplier_ID = '").Append(supplierID).Append("'");
            }
            foreach (string key in keywords)
            {
                if (key.Equals(""))
                    continue;

                if (!isFirst)
                {
                    strBui.Append(" and ");
                }
                else
                {
                    isFirst = false;
                    strBui.Append(" where ");
                }
                strBui.Append("Contains(Detail, '").Append(key).Append("')");
            }

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            //if()
            return dt;
        }

        public List<string> getAllMaterialTypes()
        {
            List<string> materialTypes = new List<string>();
            string sql = "select distinct(MATERIAL_TYPE) from Material_Catalog";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                materialTypes.Add(dt.Rows[i][0].ToString());
            }
            return materialTypes;
        }

        public List<string> getAllSuppliers()
        {
            List<string> suppliersList = new List<string>();
            string sql = "select distinct(Supplier_ID) from Material_Catalog";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                suppliersList.Add(dt.Rows[i][0].ToString());
            }
            return suppliersList;
        }
    }
}
