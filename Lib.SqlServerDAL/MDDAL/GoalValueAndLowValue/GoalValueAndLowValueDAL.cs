﻿using Lib.IDAL.MDIDAL.GoalValueAndLowValue;
using Lib.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.MDDAL.GoalValueAndLowValue
{
    class GoalValueAndLowValueDAL : GoalValueAndLowValueIDAL
    {
        public static string sqlText = "";
        /// <summary>
        /// //查询目标值和最低值
        /// </summary>
        /// <returns>返回值目标值和最低值</returns>
        DataTable GoalValueAndLowValueIDAL.getGoalAndLow()
        {
            sqlText = "SELECT * FROM [dbo].[TabGoalAndLowVal]";
            DataTable dt= DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count == 0)
                dt = null ;
            return dt;
        }
        /// <summary>
        /// 新建目标值和最低值信息
        /// </summary>
        /// <param name="goalValueAndLowValueModel">目标值和最低值对象</param>
        /// <returns>flase,true</returns>
        bool GoalValueAndLowValueIDAL.newGoalAndLowValue(GoalValueAndLowValueModel goalValueAndLowValueModel)
        {
            sqlText = @"IF EXISTS ( SELECT * FROM [dbo].[TabGoalAndLowVal] ) UPDATE [dbo].[TabGoalAndLowVal] 
                            SET CGoalValue = @CGoalValue,
                            CLowValue = @CLowValue,
                            PGoalValue = @PGoalValue,
                            LowValue = @LowValue ELSE INSERT INTO [dbo].[TabGoalAndLowVal]( CGoalValue, CLowValue, PGoalValue, LowValue )
                            VALUES
	                            ( @CGoalValue, @CLowValue, @PGoalValue, @LowValue )";

            SqlParameter[] sqlParameters = new SqlParameter[]
           {
                new SqlParameter("@CGoalValue",goalValueAndLowValueModel.CGreenValue),
                new SqlParameter("@CLowValue",goalValueAndLowValueModel.CLowValue),
                new SqlParameter("@PGoalValue",goalValueAndLowValueModel.PGreenValue),
                new SqlParameter("@LowValue",goalValueAndLowValueModel.PLowValue),
           };
            if (DBHelper.ExecuteNonQuery(sqlText,sqlParameters) == 1)
                return true;
            else
                return false;
        }
    }
}
