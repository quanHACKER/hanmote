﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using System.Data;

namespace Lib.SqlServerDAL.MDDAL.SP
{
   public class SupplierCompanyDAL:SupplierCompanyIDAL
    {

        public Model.MD.SP.SupplierCompanyCode GetSupplierCompanyInformation(string supplierid, string companyid)
        {
            string sql = "SELECT * FROM [Supplier_CompanyCode] WHERE Supplier_ID = '" + supplierid + "' AND Company_ID='" + companyid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            SupplierCompanyCode spcpy = new SupplierCompanyCode();
            if (dt.Rows.Count == 0)
                return spcpy;
            else
            {
                DataRow row = dt.Rows[0];
                spcpy.Supplier_ID = row["Supplier_ID"].ToString();
                spcpy.Company_ID = row["Company_ID"].ToString();
                spcpy.Supplier_Name = row["Supplier_Name"].ToString();
                spcpy.Company_Name = row["Company_Name"].ToString();
                spcpy.Controlling_Accounts = row["Controlling_Accounts"].ToString();
                spcpy.Head_Office = row["Head_Office"].ToString();
                spcpy.Permissions_Set = row["Permissions_Set"].ToString();
                spcpy.Few_Marks = row["Few_Marks"].ToString();
                spcpy.Sort_Code = row["Sort_Code"].ToString();
                spcpy.CashManagement_Group = row["CashManagement_Group"].ToString();
                spcpy.Approval_Group = row["Approval_Group"].ToString();
                spcpy.Certificate_Date =Convert.ToDateTime( row["Certificate_Date"].ToString());
                spcpy.InterestCalculated_Logo = row["InterestCalculated_Logo"].ToString();
                spcpy.Interest_Cycle = row["Interest_Cycle"].ToString();
                spcpy.LastCalculation_Date =Convert.ToDateTime( row["LastCalculation_Date"].ToString());
                spcpy.Last_Runtime = Convert.ToDateTime(row["Last_Runtime"].ToString());
                spcpy.Withholding_TaxCode = row["Withholding_TaxCode"].ToString();
                spcpy.Income_Country = row["Income_Country"].ToString();
                spcpy.Receiptor_Type = row["Receiptor_Type"].ToString();
                spcpy.Duty_Free = row["Duty_Free"].ToString();
                spcpy.Effective_EndDate =Convert.ToDateTime( row["Effective_EndDate"].ToString());
                spcpy.DutyFree_Access = row["DutyFree_Access"].ToString();
                spcpy.Previous_Account = row["Previous_Account"].ToString();
                spcpy.Personnel_Number = row["Personnel_Number"].ToString();
                return spcpy;

            }

        }

        public bool UpdateSupplierCompanyInformation(Model.MD.SP.SupplierCompanyCode spcpy)
        {
            string sql1 = "SELECT * FROM [Supplier_CompanyCode] WHERE Supplier_ID = '" + spcpy.Supplier_ID + "' AND Company_ID='" + spcpy.Company_ID + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count == 0)
            {
                string sql2 = "INSERT INTO [Supplier_CompanyCode] (Supplier_ID,Supplier_Name,Company_ID,";
                sql2 += "Company_Name,Controlling_Accounts,Head_Office,Permissions_Set,Few_Marks,";
                sql2 += "Sort_Code,CashManagement_Group,Approval_Group,Certificate_Date,InterestCalculated_Logo,";
                sql2 += "Interest_Cycle,LastCalculation_Date,Last_Runtime,Withholding_TaxCode,Income_Country,";
                sql2 += "Receiptor_Type,Duty_Free,Effective_EndDate,DutyFree_Access,Previous_Account,Personnel_Number)";
                sql2 += "VALUES ('" + spcpy.Supplier_ID + "','" + spcpy.Supplier_Name + "','" + spcpy.Company_ID + "',";
                sql2 += "'" + spcpy.Company_Name + "','" + spcpy.Controlling_Accounts + "','" + spcpy.Head_Office + "',";
                sql2 += "'" + spcpy.Permissions_Set + "','" + spcpy.Few_Marks + "','" + spcpy.Sort_Code + "',";
                sql2 += "'" + spcpy.CashManagement_Group + "','" + spcpy.Approval_Group + "','" + spcpy.Certificate_Date + "',";
                sql2 += "'" + spcpy.InterestCalculated_Logo + "','" + spcpy.Interest_Cycle + "','" + spcpy.LastCalculation_Date + "',";
                sql2 += "'" + spcpy.Last_Runtime + "','" + spcpy.Withholding_TaxCode + "','" + spcpy.Income_Country + "',";
                sql2 += "'" + spcpy.Receiptor_Type + "','" + spcpy.Duty_Free + "','" + spcpy.Effective_EndDate + "',";
                sql2 += "'" + spcpy.DutyFree_Access + "','" + spcpy.Previous_Account + "','" + spcpy.Personnel_Number + "')";
                DBHelper.ExecuteNonQuery(sql2);
                return true;
            }
            else
            {
                string sql3 = " Update [Supplier_CompanyCode] SET Controlling_Accounts='"+spcpy.Controlling_Accounts+"',";
                sql3 += "Head_Office='" + spcpy.Head_Office + "',Permissions_Set='" + spcpy.Permissions_Set + "',";
                sql3 += "Few_Marks='" + spcpy.Few_Marks + "',Sort_Code='" + spcpy.Sort_Code + "',CashManagement_Group='" + spcpy.CashManagement_Group + "',";
                sql3 += "Approval_Group='" + spcpy.Approval_Group + "',Certificate_Date='" + spcpy.Certificate_Date + "',";
                sql3 += "InterestCalculated_Logo='" + spcpy.InterestCalculated_Logo + "',Interest_Cycle='" + spcpy.Interest_Cycle + "',";
                sql3 += "LastCalculation_Date='" + spcpy.LastCalculation_Date + "',Last_Runtime='" + spcpy.Last_Runtime + "',Withholding_TaxCode='" + spcpy.Withholding_TaxCode + "',";
                sql3 += "Income_Country='" + spcpy.Income_Country + "',Receiptor_Type='" + spcpy.Receiptor_Type + "',Duty_Free='" + spcpy.Duty_Free + "',";
                sql3 += "Effective_EndDate='" + spcpy.Effective_EndDate + "',DutyFree_Access='" + spcpy.DutyFree_Access + "',";
                sql3 += "Previous_Account='" + spcpy.Previous_Account + "',Personnel_Number='" + spcpy.Personnel_Number + "'";
                sql3 += "WHERE Supplier_ID='" + spcpy.Supplier_ID + "' AND Company_ID='" + spcpy.Company_ID + "'";
                DBHelper.ExecuteNonQuery(sql3);
                return true;
            }
        }
    }
}
