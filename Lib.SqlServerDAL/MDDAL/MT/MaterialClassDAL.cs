﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Common.CommonUtils;
using Lib.Model.MD.MT;
using Lib.Model.MD.SP;
using Lib.IDAL.MDIDAL.MT;

namespace Lib.SqlServerDAL.MDDAL.MT
{
  public  class MaterialClassDAL:MaterialClassIDAL
    {



        public List<Model.MD.MT.Class_Feature> GetFeatureInfoByClassID(string classid)
        {
            List<Class_Feature> listcf = new List<Class_Feature>();
            string sql = "SELECT * FROM [Class_Feature] WHERE Class_ID = '" + classid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有任何行");
                return listcf;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Class_Feature clfe = new Class_Feature();
                    clfe.Class_ID = classid;
                    clfe.Class_Name = dt.Rows[i]["Class_Name"].ToString();
                    clfe.Feature_ID = dt.Rows[i]["Feature_ID"].ToString();
                    clfe.Feature_Name = dt.Rows[i]["Feature_Name"].ToString();
                    listcf.Add(clfe);
                }
                return listcf;
            }
            
        }

        public List<Model.MD.MT.Material_Feature> GetFeatureInfoByClassIDMtID(string classid, string mtid)
        {
            List<Material_Feature> listmtfe = new List<Material_Feature>();
            string sql = "SELECT * FROM [Material_Feature] WHERE Class_ID = '" + classid + "' AND Material_ID = '" + mtid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有任何行");
                return listmtfe;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Material_Feature mtfe = new Material_Feature();
                    mtfe.Class_ID = classid;
                    mtfe.Feature_ID = dt.Rows[i]["Feature_ID"].ToString();
                    mtfe.Feature_Name = dt.Rows[i]["Feature_Name"].ToString();
                    mtfe.Feature_Value = dt.Rows[i]["Feature_Value"].ToString();
                    mtfe.Material_ID = dt.Rows[i]["Material_ID"].ToString();
                    listmtfe.Add(mtfe);

                }
                return listmtfe;
            }
        }


        public bool InsertFeatureInfo(List<Model.MD.MT.Material_Feature> listmtf)
        {

            string sql1 = "SELECT DISTINCT Class_Name FROM [Class_Feature] WHERE Class_ID='"+listmtf[0].Class_ID+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql1);
            string classname = dt.Rows[0]["Class_Name"].ToString();
            string sql = "UPDATE [Material] SET Material_Class='" + classname + "'WHERE Material_ID='"+listmtf[0].Material_ID+"';";
            for (int i = 0; i < listmtf.Count; i++)
            {
                sql += "INSERT INTO [Material_Feature] VALUES ('" + listmtf[i].Material_ID + "','" + listmtf[i].Class_ID + "','" + listmtf[i].Feature_ID + "','" + listmtf[i].Feature_Name + "','" + listmtf[i].Feature_Value + "');";

            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool InsertClassFeature(List<Model.MD.MT.Class_Feature> listfea)
        {
            string sql = "";
            for (int i = 0; i < listfea.Count; i++)
            {
                sql += "INSERT INTO [Class_Feature] VALUES ('" + listfea[i].Class_ID + "','" + listfea[i].Class_Name + "','" + listfea[i].Feature_ID + "','" + listfea[i].Feature_Name + "');";
            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public bool UpdateFeatureInfo(List<Material_Feature> listmtf)
        {
            string sql = "";
            for (int i = 0; i < listmtf.Count; i++)
            {
                sql += "UPDATE [Material_Feature] SET Feature_Value='" + listmtf[i].Feature_Value + "' WHERE Material_ID='" + listmtf[i].Material_ID + "' AND Class_ID='" + listmtf[i].Class_ID + "' AND Feature_ID='" + listmtf[i].Feature_ID + "';";
            }
            DBHelper.ExecuteNonQuery(sql);
            return true;

        }

        public bool UpdateClassFeature(List<Class_Feature> listfea)
        {
            string sql = "";
            for (int i = 0; i < listfea.Count; i++)
            {
                sql += "UPDATE [Class_Feature] SET Class_Name='" + listfea[i].Class_Name + "',Feature_Name='" + listfea[i].Feature_Name + "' WHERE Class_ID = '" + listfea[i].Class_ID + "' AND Feature_ID = '" + listfea[i].Feature_ID + "';";
            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public DataTable GetDistinctClassInfo()
        {
            string sql = "select distinct Class_ID,Class_Name from [Class_Feature] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }




        public List<Supplier_Feature> GetFeatureInfoByClassIDSPID(string classid, string spid)
        {
            List<Supplier_Feature> listspfe = new List<Supplier_Feature>();
            string sql = "SELECT * FROM [Supplier_Feature] WHERE Class_ID = '" + classid + "' AND Supplier_ID = '" + spid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有任何行");
                return listspfe;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Supplier_Feature spfe = new Supplier_Feature();
                    spfe.Class_ID = classid;
                    spfe.Class_Name = dt.Rows[i]["Class_Name"].ToString();
                    spfe.Feature_ID = dt.Rows[i]["Feature_ID"].ToString();
                    spfe.Feature_Name = dt.Rows[i]["Feature_Name"].ToString();
                    spfe.Feature_Value = dt.Rows[i]["Feature_Value"].ToString();
                    spfe.Supplier_ID = dt.Rows[i]["Supplier_ID"].ToString();
                    listspfe.Add(spfe);

                }
                return listspfe;
            }
        }

        public bool InsertFeatureInfosp(List<Supplier_Feature> listspfe)
        {
            string sql1 = "SELECT DISTINCT Class_Name FROM [Class_Feature] WHERE Class_ID='" + listspfe[0].Class_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql1);
            string classname = dt.Rows[0]["Class_Name"].ToString();
            string sql = "UPDATE [Supplier_Base] SET Supplier_Class='" + classname + "'WHERE Supplier_ID='"+listspfe[0].Supplier_ID+"';";
            for (int i = 0; i < listspfe.Count; i++)
            {
                sql += "INSERT INTO [Supplier_Feature] VALUES ('" + listspfe[i].Supplier_ID + "','" + listspfe[i].Class_ID + "','" + listspfe[i].Class_Name + "','" + listspfe[i].Feature_ID + "','" + listspfe[i].Feature_Name + "','" + listspfe[i].Feature_Value + "');";

            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool UpdateFeatureInfosp(List<Supplier_Feature> listspfe)
        {
            string sql = "";
            for (int i = 0; i < listspfe.Count; i++)
            {
                sql += "UPDATE [Supplier_Feature] SET Feature_Value='" + listspfe[i].Feature_Value + "' WHERE Supplier_ID='" + listspfe[i].Supplier_ID + "' AND Class_ID='" + listspfe[i].Class_ID + "' AND Feature_ID='" + listspfe[i].Feature_ID + "';";
            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
    }
}
