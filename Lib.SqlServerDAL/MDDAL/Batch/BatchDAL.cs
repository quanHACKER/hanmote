﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using System.Data.SqlClient;
using System.Data;
using Lib.Model;
using Lib.IDAL;
using Lib.Common.CommonUtils;

namespace Lib.SqlServerDAL.MDDAL.Batch
{
   public class BatchDAL:BatchFeatureIDAL
    {
    //获取所有固定特性
     public  List<string> GetAllFixedFeatures()
       {
           string sql = " SELECT * FROM [Feature] ";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           if (dt == null || dt.Rows.Count == 0)
           {
               return null;
           }
       List<string> list = new List<string>();
           string obj;
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               obj = dt.Rows[i][1].ToString();
               list.Add(obj);
               obj = null;
           }
           return list;
       }
       //向数据库中写入批次信息
     public  bool InsertBatchFeatures(string BatchID, string name, string value)
       {
           string sql = "INSERT INTO [Batch_Feature](Batch_ID,Feature_Name,Feature_Value)Values('" + BatchID + "','" + name + "','" + value + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
       //获取特性名称对应的默认值信息
     public List<string> GetAllValue(string name)
     {
         string sql = "SELECT * FROM [Feature_Value] WHERE Feature_Name='" + name + "'";
         DataTable dt = DBHelper.ExecuteQueryDT(sql);
         List<string> list = new List<string>();
         if (dt == null || dt.Rows.Count == 0)
         {
             return list;
         }
         
         string obj;
         for (int i = 0; i < dt.Rows.Count; i++)
         {
             obj = dt.Rows[i][2].ToString();
             list.Add(obj);
             obj = null;
         }
         return list;
     }
        //通过批次编码获取批次特性
     public DataTable GetFeaturesByID(string ID)
     {
         string sql = " SELECT Batch_ID ,Feature_Name ,Feature_Value  FROM [Batch_Feature] WHERE Batch_ID='" + ID + "'";
         DataTable dt = DBHelper.ExecuteQueryDT(sql);
         return dt;
     }


     public bool RepeatedBatchFeatureInformation(string batchid, string featurename)
     {
         string sql = "SELECT * FROM [Batch_Feature] ";
         DataTable dt = DBHelper.ExecuteQueryDT(sql);
        int count = 0;
        count = dt.Rows.Count;
        if (count == 0) 
        return true;
         else{
        int i;
        for (i = 0; i < dt.Rows.Count; i++)
        {
            if (batchid == dt.Rows[i][1].ToString() && featurename == dt.Rows[i][2].ToString())
            {
                
                return false;
               
            }                 
        }
        return true;
         }
     }

     public bool RepeatedBatch(string BatchID)
     {
         throw new NotImplementedException();
     }
    }
}
