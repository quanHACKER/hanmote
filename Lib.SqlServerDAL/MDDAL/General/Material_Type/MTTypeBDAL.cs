﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using Lib.IDAL.MDIDAL.Gerneral.Material_Type;
using System.Data;

namespace Lib.SqlServerDAL.MDDAL.General.Material_Type
{
    class MTTypeBDAL:MTTypeBIDAL
    {
        DataTable MTTypeBIDAL.GetAllLittleClassfy()
        {
            string sql = "SELECT Littleclassfy_Name as '小分类名称',Littleclassfy_ID as '小分类ID',Bigclassfy_Name as '大分类名称',Bigclassfy_ID as '大分类ID' FROM [Littleclassfy]";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
        }   

        List<string> MTTypeBIDAL.GetAllBigClassfyName()
        {
            string sql = "SELECT * FROM [Bigclassfy]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            if (dt == null || dt.Rows.Count == 0)
            {
                return list;
            }

            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        bool MTTypeBIDAL.NewLittleClassfy(string lID, string lname, string bname)
        {
            string bid1 = "SELECT ClassfyID FROM [Bigclassfy] WHERE Bigclassfy_Name = '"+bname+"'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(bid1);
            string bid = dt1.Rows[0][0].ToString();
            string sql = "INSERT INTO [Littleclassfy](Littleclassfy_ID,Littleclassfy_Name,Bigclassfy_Name,Bigclassfy_ID) VALUES ('" + lID + "','" + lname + "','" + bname + "','" + bid + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        bool MTTypeBIDAL.DeleteLittleClassfy(string ID)
        {
            throw new NotImplementedException();
        }


        public List<string> GetLittleClassfyByBig(string name)
        {
            string sql = "SELECT * FROM [Littleclassfy] WHERE Bigclassfy_Name='" + name + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           List<string> list = new List<string>();
           if (dt == null || dt.Rows.Count == 0)
           {
               return list;
           }

           string obj;
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               obj = dt.Rows[i][0].ToString();
               list.Add(obj);
               obj = null;
           }
           return list;
        }

        public List<string> GetAllMtGroupName()
        {
            string sql = "SELECT DISTINCT Material_Group FROM Material_Group ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            if (dt == null || dt.Rows.Count == 0)
            {
                return list;
            }

            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        public List<string> GetAllfiled()
        {
            string sql = "SELECT DISTINCT Industry_Category FROM Industry_Category";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            if (dt == null || dt.Rows.Count == 0)
            {
                return list;
            }

            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

    }
}
