﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using Lib.IDAL.MDIDAL.Gerneral.Material_Type;

namespace Lib.SqlServerDAL.MDDAL.General.Material_Type
{
    class MTTypeCDAL:MTTypeCIDAL
    {
        DataTable MTTypeCIDAL.GetAllLittleClassfy()
        {
            string sql = "SELECT Type_Name as '物料类型类名称',Type_ID as '物料类型ID',Littleclassfy_Name as '大分类名称',Littleclassfy_ID as '大分类ID' FROM [Typeclassfy]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        List<string> MTTypeCIDAL.GetAllBigClassfyName()
        {
            string sql = "SELECT * FROM [Littleclassfy]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            if (dt == null || dt.Rows.Count == 0)
            {
                return list;
            }

            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        bool MTTypeCIDAL.NewLittleClassfy(string lID, string lname, string bname)
        {
            string bid1 = "SELECT Littleclassfy_ID FROM [Littleclassfy] WHERE Littleclassfy_Name = '" + bname + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(bid1);
            string bid = dt1.Rows[0][0].ToString();
            string sql = "INSERT INTO [Typeclassfy](Type_ID,Type_Name,Littleclassfy_Name,Littleclassfy_ID) VALUES ('" + lID + "','" + lname + "','" + bname + "','" + bid + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        bool MTTypeCIDAL.DeleteLittleClassfy(string ID)
        {
            throw new NotImplementedException();
        }


        public List<string> GetAllTypeName()
        {
            string sql = "SELECT * FROM [Typeclassfy]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            if (dt == null || dt.Rows.Count == 0)
            {
                return list;
            }

            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
    }
}
