﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using System.Data.SqlClient;
using System.Data;
using Lib.Model;

namespace Lib.SqlServerDAL
{
    public class AccountDAL : AccumentIDAL
    {
        //获取公司代码
        public List<string> GetCompanyCode()
        {
            string sql = " SELECT * FROM [Company] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有货币类型
        public List<string> GetCurrencyType()
        {
            string sql = " SELECT * FROM [Currency] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][2].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有评估类
        public List<string> GetEvaluationClass()
        {
            return null;
        }
         //获取所有供应商
        public List<string> GetAllSupplier()
        {
            string sql = " SELECT * FROM [Supplier_Base] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //根据供应商ID获取供应商描述
        public string GetSuppplierDescription(string SupplierID)
        {
            string sql = " SELECT * FROM [Supplier_Base] WHERE Supplier_ID='" + SupplierID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
             if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
             string obj = dt.Rows[0][3].ToString();
             return obj;
        }
        //获取所有税码
        public List<string> GetTaxCode()
        {
            string sql = " SELECT * FROM [Tax_Code] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //通过所选税码获取税码描述
        public string  GetTaxDescription(string TaxID)
        {
            string sql = " SELECT * FROM [Tax_Code] WHERE Tax_Code='" + TaxID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            string obj = dt.Rows[0][1].ToString();
            return obj;
        }
        //获取所有工厂
        public List<string> GetAllFactory()
        {
            string sql = " SELECT * FROM [Factory] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //通过所选工厂获取其所属库存地
        public List<string> GetAllStock(string FactoryID)
        {
            string sql = " SELECT * FROM [Stock] WHERE Factory_ID='" + FactoryID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有科目
        public List<string> GetAllAccount()
        {
            string sql = " SELECT * FROM [Account] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有科目名称
        public List<string> GetAllAccountName()
        {
            string sql = " SELECT * FROM [Account] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //借记原材料科目
       public bool DebitRawMaterial(DateTime time,string number,float dmoney,float cmoney,string factory,string stock,string type)
    {
        string sql = " INSERT INTO [Raw_Material_Account](Account_Date,Certificate_Number,Debit,Credit,factory,stock,Material_Type)values('" + time + "','" + number + "','" + dmoney + "','" + cmoney + "','" + factory + "','" + stock + "','" + type + "')";
        DBHelper.ExecuteNonQuery(sql);
        return true;
    }
        //借记服务类
       public bool DebitServiceClass(DateTime time, string number, float money, string type)
       {
           string sql = " INSERT INTO [Service_Class_Account](Account_Date,Certificate_Number,Debit,Material_Type)values('" + time + "','" + number + "','" + money + "','" + type + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
       public bool DebitFinishedProduct(DateTime time, string number, float money, string factory, string stock, string type)
       {
           string sql = " INSERT INTO [Finished_Product_Account](Account_Date,Certificate_Number,Debit,factory,stock,Material_Type)values('" + time + "','" + number + "','" + money + "','" + factory + "','" + stock + "','" + type + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记应付账款科目
       public bool DebitAccountsPayable(DateTime time, string number, float money)
       {
           string sql = " INSERT INTO [Accounts_Payable_Account](Account_Date,Certificate_Number,Debit)values('" + time + "','" + number + "','" + money + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
         //借记应交税费科目
       public bool DebitTaxPayable(DateTime time, string number, float money)
       {
           string sql = " INSERT INTO [Tax_Payable_Account](Account_Date,Certificate_Number,Debit)values('" + time + "','" + number + "','" + money + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记银行存款科目
       public bool DebitBankDeposit(DateTime time, string number, float money)
       {
           string sql = " INSERT INTO [Bank_Deposit_Account](Account_Date,Certificate_Number,Debit)values('" + time + "','" + number + "','" + money + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记在途物资科目
       public bool DebitAfloatMaterials(DateTime time, string number, float money)
       {
           string sql = " INSERT INTO [Afloat_Materials_Account](Account_Date,Certificate_Number,Debit)values('" + time + "','" + number + "','" + money + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记材料成本差异科目
       public bool DebitMaterialCostVariances(DateTime time, string number, float money, string type)
       {
           string sql = " INSERT INTO [Material_Cost_Variance_Account](Account_Date,Certificate_Number,Debit，Material_Type)values('" + time + "','" + number + "','" + money + "','"+type+"')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记GR/IR科目
       public bool DebitGRIR(DateTime time, string number, float money, string type)
       {
           string sql = " INSERT INTO [GR_IR_Account](Account_Date,Certificate_Number,Debit，Material_Type)values('" + time + "','" + number + "','" + money + "','" + type + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //借记质检库存科目
       public bool DebitCheckInventory(DateTime time, string number, float money, string type)
       {
           string sql = " INSERT INTO [Check_Inventory_Account](Account_Date,Certificate_Number,Debit，Material_Type)values('" + time + "','" + number + "','" + money + "','" + type + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
        //贷记应付账款科目
       public bool CreditAccountsPayable(DateTime time, string number, float money)
       {
           string sql = "INSERT INTO [Accounts_Payable_Account](Account_Date,Certificate_Number,Credit)values('" + time + "','" + number + "','" + money + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
       // //将会计凭证信息写入会计凭证查询数据表
       //public bool AccountQuery(DateTime time, string number, string type, string code, string classa, string supliera, string accounta, string debita, string credita, string factorya, string stocka, string classb, string suplierb, string accountb, string debitb, string creditb, string factoryb, string stockb)
       //{
       //    string sql = "INSERT INTO [Account_Query](Voucher_Code,Accounting_Date,Voucher_Type,Company_Code,Evaluation_Class_A,Supplier_ID_A,Account_A,Debit_A,Credit_A,Factory_A,Stock_A,Evaluation_Class_B,Supplier_ID_B,Account_B,Debit_B,Credit_B,Factory_B,Stock_B)values('"+number+"','"+time+"','"+type+"','"+code+"','"+classa+"','"+supliera+"','"+accounta+"',debita, string credita, string factorya, string stocka, string classb, string suplierb, string accountb, string debitb, s
       //}
       //将会计凭证项目信息写入数据表
       public bool WriteAccountQuery(string code, int rownumber, string supplierID, string AccountID, float debit, float credit, string taxcode)
       {
           string sql = "INSERT INTO [Account_Document_Inner](Voucher_Code,Row_Number,Supplier_ID,Account_ID,Debit,Credit,Tax_Code)values('" + code + "','" + rownumber + "','" + supplierID + "','" + AccountID + "','" + debit + "','" +credit + "','" + taxcode + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
       public bool WriteAccountFrount(string code, DateTime time, string type, string ccode)
       {
           string sql = "INSERT INTO [Account_Document_Frount](Voucher_Code,Accounting_Date,Voucher_Type,Movement_Type)values('" + code + "','" + time + "','" + type + "','" + ccode + "')";
           DBHelper.ExecuteNonQuery(sql);
           return true;
       }
       public DataTable ReadAccountFrount(string code)
       {
           string sql = "SELECT * FROM [Account_Document_Frount] WHERE Voucher_Code = '" + code + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
       }
       public DataTable ReadAccountInner(string code)
       {
           string sql = "SELECT * FROM [Account_Document_Inner] WHERE Voucher_Code = '" + code + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
       }



       public List<string> GetAllVocherID()
       {
           string sql = "SELECT * FROM [Account_Document_Frount] ";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           if (dt == null || dt.Rows.Count == 0)
           {
               return null;
           }
           List<string> list = new List<string>();
           string obj;
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               obj = dt.Rows[i][0].ToString();
               list.Add(obj);
               obj = null;
           }
           return list;
       }


       public DataTable GetALLAccountInformation()
       {
           string sql = " SELECT * FROM [Account] ";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
       }


       public DataTable GetAllTaxCodeInformation()
       {
           string sql = "SELECT * FROM [Tax_Code]";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
       }


       public DataTable GetTaxByTaxCode(string TaxCode)
       {
           string sql = "SELECT Tax FROM [Tax_Code] WHERE Tax_Code='" + TaxCode + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
       }



       public List<string> GetAllOrderID()
       {
           throw new NotImplementedException();
       }

       public DataTable GetAllOrderInformation()
       {
           throw new NotImplementedException();
       }

       public double GetOrderAmount(string orderid)
       {
           throw new NotImplementedException();
       }
    }
        
}

