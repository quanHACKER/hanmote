﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Lib.IDAL.MT_GroupPositionIDAL;
using Lib.Model.MT_GroupModel;

namespace Lib.SqlServerDAL.MT_GroupDAL
{
    public class N_RiskAssDAL : N_RiskAssessmentIDAL
    {
        DataTable dt = null;
        String sql = "";
        /// <summary>
        /// 获取已经定义好的物料组数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetDefinedGroup(String purOrgName)
        {

            sql = "select distinct id as 物料组编号,MTGName as 物料组名称,Category as 类别 from MTGAttributeDefinition where PurOrgName='"+ purOrgName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;


        }
        //获取支出情况
        public DataTable getPurCostAndItem(string purName)
        {
            sql = @"select AddProportionCount as cost from  Hanmote_MtGroupPositionSheet where  PurGroupName='"+purName+"' order by AddProportionCount;";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;


        }

        public DataTable getMtGroupImportedData(string purOrgName)
        {
            sql = @"select sheet.MtGroupName, sheet.AddProportionCount as 累计支出,sheet.Proportion as 支出金额 ,info.mtScore as '评分',info.mtLevel as 等级 from Hanmote_MtGroupPositionSheet sheet,Han_MtPosInfo info where PurGroupName=@purOrgName and  sheet.PurGroupName=info.PurOrgName and sheet.MtGroupCode =info.MtGroupId;";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@purOrgName",purOrgName),
                
            };

            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter);
            return dt;
        }

        /// <summary>
        /// 物料组评估子项
        /// </summary>
        /// <param name="mtItemScoreInfoModle"></param>
        /// <returns></returns>
        public Boolean updateMtItemScoreInfo(MtItemScoreInfoModle mtItemScoreInfoModle,string PurOrgName)
        {
            Boolean flag = false;
            sql = @"update MTGAttributeDefinition set AimsScore=@aimScore,RisKScore=@RiskScore,aimLevel=@aimLevel,RiskLevel=@RiskLevel,riskText =@riskText where id=@id and Aims=@Aim and PurOrgName=@PurOrgName";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@aimScore",mtItemScoreInfoModle.InfluenScore),
                new SqlParameter("@RiskScore",mtItemScoreInfoModle.RiskScore),
                new SqlParameter("@aimLevel",mtItemScoreInfoModle.InfluenLevel),
                new SqlParameter("@RiskLevel",mtItemScoreInfoModle.RiskLevel),
                new SqlParameter("@id",mtItemScoreInfoModle.Id),
                new SqlParameter("@Aim",mtItemScoreInfoModle.Aim),
                new SqlParameter("@PurOrgName",PurOrgName),
                new SqlParameter("@riskText",mtItemScoreInfoModle.RiskText)


            };

            int n = DBHelper.ExecuteNonQuery(sql,sqlParameter);
            if (n > 0) {
                flag = true;

            }
            return flag;
        }

        public DataTable getMtGroupFinished(string purOrgName)
        {
            sql = @"select riskScore,influenceScore,mtName  from Han_MtPosInfo  where PurOrgName='"+purOrgName+"' and  riskScore is not null and influenceScore is not null";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;

        }

        public DataTable mtGroupSearch(string mtName, string purOrgName)
        {
            sql = @"select distinct id as 物料组编号,MTGName as 物料组名称,Category as 类别 from MTGAttributeDefinition where PurOrgName='" + purOrgName + "' and MTGName like'%"+mtName+"%'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable SearchMtGroupByName(string mtName,string purOrgName)
        {
            sql = @"select MtGroupName as 物料组,InfluAndRisk as 影响风险评分,Cost as 支出金额,PosLevel as 定位结果 from Han_MtPosResult  where  PurOrgName='" + purOrgName + "' and MtGroupName like'%" + mtName + "%' ORDER BY InfluAndRisk desc";
           
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public int insertLastPosResultInfo(PosResultInfoModel posResultInfoModel)
        {

            int flag = 0;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@mtName",posResultInfoModel.MtName),
                new SqlParameter("@PurOrgName",posResultInfoModel.PurOrgName),
                new SqlParameter("@Cost",posResultInfoModel.Cost),
                new SqlParameter("@InfluAndRiskScore",posResultInfoModel.InfluAndRiskScore),
                new SqlParameter("@PosResult",posResultInfoModel.PosResult)
            };
            //判读是否已经存在数据
            sql = @"select * from Han_MtPosResult where  PurOrgName='"+ posResultInfoModel.PurOrgName+ "' and MtGroupName='"+ posResultInfoModel.MtName+ "'";
            dt = DBHelper.ExecuteQueryDT(sql);
           
            if (dt.Rows.Count > 0)
            {
                sql = "update Han_MtPosResult set InfluAndRisk=@InfluAndRiskScore,Cost=@Cost,PosLevel=@PosResult where PurOrgName=@PurOrgName and MtGroupName=@mtName";
            }
            else {
                sql = @"insert into Han_MtPosResult values(@PurOrgName,@mtName,@InfluAndRiskScore,@Cost,@PosResult)";
            }

            flag = DBHelper.ExecuteNonQuery(sql, sqlParameter);

            return flag;

        }

        public DataTable getMtGroupResultData(string id, string PurOrgName)
        {
            sql = @"select * from(select distinct aimLevel,AimsScore from MTGAttributeDefinition where AimsScore in (select MAX(AimsScore) from MTGAttributeDefinition where id=@id and PurOrgName=@PurOrgName) and id=@id and PurOrgName=@PurOrgName ) t1,
                    (select distinct RiskLevel,RisKScore from MTGAttributeDefinition where RisKScore in (select MAX(RisKScore) from MTGAttributeDefinition where id=@id and PurOrgName=@PurOrgName) and id=@id and PurOrgName=@PurOrgName ) t2";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@id",id),
                new SqlParameter("@PurOrgName",PurOrgName)
            };

            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter);
            return dt;
        }

        public int insertIntoPosResInfo(double resultPosScore, string resultLevel, string id, string purOrgName,string riskScore, string influenceScore,string mtName)
        {
            SqlParameter[] sqlParameter1 = new SqlParameter[] {
                new SqlParameter("@id",id),
                new SqlParameter("@PurOrgName",purOrgName),
                new SqlParameter("@resultPosScore",resultPosScore),
                new SqlParameter("@resultLevel",resultLevel),
                new SqlParameter("@riskScore",riskScore),
                new SqlParameter("@influenceScore",influenceScore),
                new SqlParameter("@mtName",mtName)
            };
            SqlParameter[] sqlParameter2 = new SqlParameter[] {
                new SqlParameter("@id",id),
                new SqlParameter("@PurOrgName",purOrgName),
                new SqlParameter("@resultPosScore",resultPosScore),
                new SqlParameter("@resultLevel",resultLevel),
                new SqlParameter("@riskScore",riskScore),
                new SqlParameter("@influenceScore",influenceScore),
                new SqlParameter("@mtName",mtName)
            };
            sql = @"select * from Han_MtPosInfo where PurOrgName=@PurOrgName and MtGroupId=@id";
            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter1);
            if ( dt.Rows.Count== 0)
            {
                sql = @"insert into Han_MtPosInfo values(@PurOrgName,@id,@resultPosScore,@resultLevel,@riskScore,@influenceScore,@mtName)";
            }
            else {
                sql = @"update Han_MtPosInfo set mtScore=@resultPosScore,mtLevel=@resultLevel,riskScore=@riskScore,influenceScore=@influenceScore where PurOrgName=@purOrgName and MtGroupId=@id";

            }

            return DBHelper.ExecuteNonQuery(sql,sqlParameter2);

        }

        /// <summary>
        /// 获取评估子项
        /// </summary>
        /// <param name="id">物料组id</param>
        /// <param name="purOrgName">采购组名称</param>
        /// <returns></returns>
        public DataTable getMtGroupData(String id,String purOrgName)
        {
            sql = @"SELECT Area as 领域, Aims AS 供应目标, Goals AS 指标,riskText as 风险,
                        (CASE WHEN AimsScore IS NULL THEN '待评估' else AimsScore END) AS 影响力得分, 
                        (CASE WHEN RiskScore IS NULL THEN '待评估' else RiskScore END) AS 供应风险得分,
                        (CASE WHEN aimLevel IS NULL THEN '待评估' else aimLevel END) AS 影响力PIP等级,
                        (CASE WHEN RiskLevel IS NULL THEN '待评估' else RiskLevel END) AS 供应风险等级
                    FROM      MTGAttributeDefinition
                    WHERE   (id = '" + id+"')  and PurOrgName='"+purOrgName+"'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getMtGroupFinishData(String id,String purOrgName)
        {
            // sql = @"select * from MTGAttributeDefinition where (id = '" + id + "') and PurOrgName='"+ purOrgName + "' and AimsScore is not null and RisKScore is not null";
            sql = @"select * from MTGAttributeDefinition where (id = '" + id + "') and PurOrgName='" + purOrgName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }



    }
}
