﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class ConstractManagementDAL : ConstractManagementIDAL
    {
        public DataTable GetConstractInfo(string purchaseOrg, string supplierNameOrID, string constractNameOrID)
        {
            /*
            StringBuilder sqlText = new StringBuilder("SELECT b.Material_Group,a.Material_ID,b.Material_Name,a.Price,b.Measurement,c.Supplier_ID,c.Supplier_Name,CONVERT(varchar(50),a.Begin_Time,23) AS StartTime,CONVERT(varchar(50),a.End_Time,23) AS EndTime FROM [Order] a JOIN Material b ON a.Material_ID=b.Material_ID JOIN Supplier_Base c ON a.Supplier_ID=c.Supplier_ID WHERE c.Supplier_Name=@SupplierName OR c.Supplier_ID=@SupplierID");
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@SupplierName", SqlDbType.VarChar),
                new SqlParameter("@SupplierID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = supplierNameOrID;
            sqlParameter[1].Value = supplierNameOrID;
             * */
            string sqlText = @"SELECT b.Supplier_Name,a.ConstractID, a.OrderNum,a.RecieveNum,a.OnTimeRecieveNum * 1.0 / a.OrderNum AS OnTimeRecieve,a.RelayNum * 1.0 /a.OrderNum AS RelayRecieve,a.AdvanceNum  * 1.0 /a.OrderNum AS AdvanceRecieve FROM ConstractManagement a JOIN Supplier_Base b ON a.SupplierID=b.Supplier_ID";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            else
            {
                return dt;
            }
        }
    }
}