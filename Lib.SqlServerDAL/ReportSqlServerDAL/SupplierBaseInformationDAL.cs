﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using Lib.Model;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public class SupplierBaseInformationDAL:SupplierBaseInformationIDAL
    {
        /// <summary>
        /// 创建一个模板对象
        /// </summary>
        SupplierBaseInformation supplierBaseInfo = new SupplierBaseInformation();

        /// <summary>
        /// 根据供应商名称查询供应商信息，
        /// 若参数为空或者在数据库中没有查询到数据则返回null,否则返回一个SupplierBaseInformation实例
        /// </summary>
        /// <param name="SupplierName">供应商名称</param>
        /// <returns></returns>
        public SupplierBaseInformation FindBySupplierName(string SupplierName)
        {
            if (SupplierName == null || SupplierName.Equals(""))
            {
                return null;
            }
            //查询语句
            StringBuilder sqlText = new StringBuilder("SELECT Supplier_ID,Supplier_LoginName,Supplier_Name,Enterprise,Nation,[Address],Register_time, Representative,Bank_Name,Bank_Account,Islistedcompany,Stock_code,Payer_NO,Fax,URL,Zip_Code FROM Supplier_Base WHERE Supplier_Name=@SupplierName");
            SqlParameter[] sqlParameter = new SqlParameter[]{
                 new SqlParameter("@SupplierName", SqlDbType.VarChar)
            };
            sqlParameter[0].Value = SupplierName;
            //查询数据库返回一个DataTable
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(),sqlParameter);
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            return BindingToSupplierBaseInformation(dt);
        }

        /// <summary>
        /// 根据供应商的ID来查询供应商的信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public SupplierBaseInformation FindBySupplierID(string SupplierID)
        {
            if (SupplierID == null || SupplierID.Equals(""))
            {
                return null;
            }
            //查询语句
            StringBuilder sqlText = new StringBuilder("SELECT Supplier_ID,Supplier_LoginName,Supplier_Name,Enterprise,Nation,[Address],Register_time, Representative,Bank_Name,Bank_Account,Islistedcompany,Stock_code,Payer_NO,Fax,URL,Zip_Code FROM Supplier_Base WHERE Supplier_ID=@SupplierID");
            SqlParameter[] sqlParameter = new SqlParameter[]{
                 new SqlParameter("@SupplierID", SqlDbType.VarChar)
            };
            sqlParameter[0].Value = SupplierID;
            //查询数据库返回一个DataTable
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParameter);
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            return BindingToSupplierBaseInformation(dt);   
        }

        /// <summary>
        /// 讲查询到的数据绑定到模板对象上
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private SupplierBaseInformation BindingToSupplierBaseInformation(DataTable dt)
        {
            DataRow dr = dt.Rows[0];
            //给实例的各属性赋值
            supplierBaseInfo.Supplier_ID = Convert.ToString(dr["Supplier_ID"]);
            supplierBaseInfo.Supplier_LoginName = Convert.ToString(dr["Supplier_LoginName"]);
            supplierBaseInfo.Supplier_Name = Convert.ToString(dr["Supplier_Name"]);
            supplierBaseInfo.Enterprise = Convert.ToString(dr["Enterprise"]);
            supplierBaseInfo.Nation = Convert.ToString(dr["Nation"]);
            supplierBaseInfo.Address = Convert.ToString(dr["Address"]);
            //现将数据库的时间类型转化为字符串形式，然后再次用ToDataTime方法转化为DataTime类型数据
            //supplierBaseInfo.Register_Time=Convert.ToDateTime(dr["Register_Time"]);
            supplierBaseInfo.Register_Time = dr["Register_Time"].ToString();
            supplierBaseInfo.Representative = Convert.ToString(dr["Representative"]);
            supplierBaseInfo.Bank_Name = Convert.ToString(dr["Bank_Name"]);
            supplierBaseInfo.Bank_Account = Convert.ToString(dr["Bank_Account"]);
            supplierBaseInfo.IsListedCompany = dr["IsListedCompany"].ToString();
            supplierBaseInfo.Stock_code = Convert.ToString(dr["Stock_code"]);
            supplierBaseInfo.Payer_No = Convert.ToString(dr["Payer_No"]);
            supplierBaseInfo.Fax = Convert.ToString(dr["Fax"]);
            supplierBaseInfo.URL = Convert.ToString(dr["URL"]);
            supplierBaseInfo.Zip_Code = Convert.ToString(dr["Zip_Code"]);
            return supplierBaseInfo;
        }
    }
}
