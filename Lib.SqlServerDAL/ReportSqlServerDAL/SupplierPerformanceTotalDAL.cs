﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class SupplierPerformanceTotalDAL : SupplierPerformanceTotalIDAL
    {
        public DataTable GetSupplierPerformanceTotal(string purchaseOrg, string materialGroup)
        {
            if (purchaseOrg == null || purchaseOrg.Equals(""))
            {
                return null;
            }
            StringBuilder sqlText = new StringBuilder("SELECT [Supplier_Name],[Total_Score],[Price_Score],[PriceLevel_Score],[PriceHistory_Score],[Quality_Score],[GoodReceipt_Score],[QualityAudit_Score],[ComplaintAndReject_Score],[Delivery_Score],[OnTimeDelivery_Score],[ConfirmDate_Score],[QuantityReliability_Score],[Shipment_Score],[GeneralServiceAndSupport_Score],[ExternalService_Score] FROM [Supplier_Performance] a JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID");
            /*
            SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@Buyer_GroupName", SqlDbType.VarChar)
            };
            sqlParameter[0].Value = purchaseOrg;
             * */
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            else
            {
                return dt;
            }
        }
    }
}
