﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class BuyerOrganizationDAL:BuyerOrganizationIDAL
    {
        public DataTable GetAllBuyerOrganizationName()
        {
            StringBuilder sqlText = new StringBuilder("SELECT Buyer_Org_Name FROM Buyer_Org");
           
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            else
            {
                return dt;            
            }
        }
    }
}
