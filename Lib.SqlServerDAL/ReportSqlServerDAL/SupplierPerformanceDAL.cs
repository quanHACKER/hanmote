﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class SupplierPerformanceDAL:SupplierPerformanceIDAL
    {
        public DataTable GetSupplierPerformance(string purchaseOrg, string materialGroup)
        {
            if (purchaseOrg == null || purchaseOrg.Equals(""))
            {
                return null;
            }
            StringBuilder sqlText = new StringBuilder("SELECT Supplier_Base.Supplier_Name,AVG(Total_Score) AS Total_Score,AVG(Price_Score) AS Price_Score,AVG(Quality_Score) AS Quality_Score,AVG(Delivery_Score) AS Delivery_Score, AVG(GeneralServiceAndSupport_Score) AS GeneralServiceAndSupport_Score FROM Supplier_Performance JOIN Material ON Supplier_Performance.Material_ID=Material.Material_ID JOIN Supplier_Base ON Supplier_Performance.Supplier_ID=Supplier_Base.Supplier_ID WHERE Material.Buyer_Group=@Buyer_GroupName GROUP BY Supplier_Base.Supplier_Name");
             SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@Buyer_GroupName", SqlDbType.VarChar)
            };
             sqlParameter[0].Value = purchaseOrg;
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParameter);
            if (dt == null || dt.Rows.Count <= 0)
            {
                return null;
            }
            else
            {
                return dt;
            }
        }
    }
}
