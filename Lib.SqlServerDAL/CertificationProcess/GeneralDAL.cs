﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.CertificationProcess;
using System.Data;
using Lib.Model.CertificationProcess;
using Lib.Common.CommonControls;
using Lib.Model.CertificationProcess.GN;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.CertificationProcess
{
    public class GeneralDAL:GeneralIDAL
    {
        DataTable GeneralIDAL.GetAllInfo(string id)
        {
            string sql = @"SELECT
	                            SupplierId as 供应商编号,
	                            companyName as 供应商名称,
	                            Country as 区域,
	                            ContactName as 联系人,
	                            PhoneNumber as 联系方式,
	                            ContactMail as 电子邮件,
                            RegisterStatus as '状态' 
                            FROM
	                            SR_Info,
	                            Hanmote_User_MtGroupName hum,
	                            Material_Group mg
                            WHERE
                             mg.Description = SR_Info.SelectedMType
                            AND hum.Mt_Group_Name = SR_Info.SelectedMType
                            AND hum.Pur_Group_Name = SR_Info.SelectedDep
                            AND TagFirst = 	1
                            AND TagSecond = 0
                            AND TagThird = 0
                            AND TagLocalEval = 0
                            AND TagRefuse = 0
                            AND hum.Type=1
                            AND hum.User_ID ='" + id + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.page(String UserId, int pagesize,int currentpage, out int totalSize)//筛选加载界面时刷数据
        {
            
            //调用存储过程
           string sql = @"EXECUTE Proc_SR_InfoAcessFilter @pageSize,@pageCount,@UserId,'',null";
            SqlParameter[] sqlParameters = {
                new SqlParameter("@pageSize",pagesize),
                new SqlParameter("@pageCount",currentpage),
                new SqlParameter("@UserId",UserId)
            };
            DataTable dt = DBHelper.ExecuteQueryDT(sql,sqlParameters);
            totalSize = (int)dt.Rows[0]["pageCount"];
            dt.Columns.Remove("pageCount");
            return dt;
        }
        DataTable GeneralIDAL.GetAllInfoe(string userId)//评审员初始刷数据 <shi>
        {
            string sql = "SELECT CASE WHEN info1.Eval_stat = '1' THEN '已完成' WHEN info1.Eval_stat = '0' THEN '未完成' END AS '状态',info1.supplierID AS '供应商编号',sr.CompanyName AS '供应商名称',sr.Country AS '国家',sr.ContactName AS '联系人姓名' ,sr.PhoneNumber AS '联系方式',sr.ContactMail AS '邮箱',info1.EvaluateID as '评估员编号',CASE WHEN ile.status_self_supplier = '1' THEN '已上传' WHEN ile.status_self_supplier = '0' THEN '未上传' END AS '上传状态' FROM Eval_file_Info info1,SR_Info sr,File_status ile WHERE	info1.supplierID = sr.SupplierId and info1.EvaluateID = '" + userId + "' and info1.supplierID=ile.ID";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
       /* DataTable GeneralIDAL.GetAllInfo1(string userId)//预评加载所有信息<shi>
        {
            string sql = " SELECT  distinct sr.SupplierId as '供应商编号',sr.CompanyName as '供应商名称',sr.Country as '区域',sr.ContactName as '联系人',sr.PhoneNumber as '联系方式',sr.ContactMail as '电子邮件',CASE WHEN ile.status_self_supplier = '1' THEN '已上传' WHEN ile.status_self_supplier = '0' THEN '未上传' END AS '上传状态' FROM SR_Info sr,File_status ile, Hanmote_User_MtGroupName hum where sr.Id!=''and sr.TagSecond = '1'and sr.TagRefuse = '0'and sr.SupplierId=ile.ID and hum.Type=1 and hum.User_ID = '" + userId + "' and hum.Mt_Group_Name = sr.SelectedMType  AND sr.TagSecond='0'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }*/


        DataTable GeneralIDAL.GetAllInfo1(string userId,int pageSize,int pageIndex,out int totalSize)//预评加载所有信息<shi>
        {
            string sql = @"EXECUTE Proc_SR_InfoPre @pageSize,@pageCount,@UserId,null";
            SqlParameter[] sqlParameters = {
                new SqlParameter("@pageSize",pageSize),
                new SqlParameter("@pageCount",pageIndex),
                new SqlParameter("@UserId",userId)
            };
            DataTable dt = DBHelper.ExecuteQueryDT(sql, sqlParameters);
            if (dt.Rows.Count == 0)
            {
                totalSize = 0;
            }
            else {
                totalSize = (int)dt.Rows[0]["pageCount"];
            }
           
            dt.Columns.Remove("pageCount");
            return dt;
        }

        DataTable GeneralIDAL.checkdetail(string id)//查看评估详情
        {
            string sql = "select info.EvaluateID as '评估员编号',han.Username as '评估员名字',info.Eval_score as '评估分数',info.EvalSumScore as '总分',info.IsNo as '否决项',info.Eval_ReasonInfo as '评估理由',CASE WHEN info.Eval_stat = '1' THEN '已完成' WHEN info.Eval_stat = '0' THEN '未完成' END AS '状态' from Eval_file_Info info, Hanmote_Base_User han where info.EvaluateID=han.User_ID and supplierID='" + id+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.getid(string id)//
        {
            string sql = "select pid from Hanmote_Base_User where User_ID='" + id + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.getscore(string id)//
        {
            string sql = "select Eval_score from Eval_file_Info where supplierID='" + id + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.getsum(string id)//
        {
            string sql = "select EvalSumScore from Eval_file_Info where supplierID='" + id + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.getthing(string thing)//
        {
            string sql = "select Evallevel from Material_Group where Description='" + thing + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        void GeneralIDAL.back(string id, string eid)//查看评估详情
        {
            string sql = " update Eval_file_Info set Eval_stat='0' where supplierID='" + id + "' and EvaluateID='"+eid+"'";
            DBHelper.ExecuteQueryDT(sql);
            
        }
        DataTable GeneralIDAL.GetAllInfo2(string id)//建立评估员小组加载所有信息
        {
            string sql = String.Format(@" SELECT
	                                              u.User_ID AS '评审员编号',
	                                            u.Username AS '评审员姓名',
                                                 sr.SelectedDep AS '采购组织',
                                                e.Description as '评估方面',
                                                u.Birth_Place AS '地址',
                                                 u.Mobile AS '手机',
	                                            u.Email AS '邮编'
                                            FROM
	                                            Hanmote_User_MtGroupName mt,
	                                            Hanmote_Base_User u,
	                                            SR_Info sr,
                                            HanmoteBaseUser_EvalCode e
                                            WHERE
	                                            mt.Type = 0
                                            AND u.User_ID = mt.User_ID
                                            and e.UserID = mt.User_ID
                                            AND mt.Mt_Group_Name = sr.SelectedMType
                                            AND mt.Pur_Group_Name = sr.SelectedDep
                                            AND sr.SupplierId ='" + id + "'");
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.GetAllInfo3(string id)//查看评估信息加载信息
        {
            string sql = String.Format(@"SELECT
                                        info.Username AS '评估员姓名',
	                                    sr.SelectedDep AS '采购组织',
	                                    lo.Description AS '评估方面',
	                                    info.mobile as '手机',
	                                    info.email as '邮箱',
	                                    info.User_ID AS '评估员编号',
	                                    CASE
                                    WHEN lol.Eval_stat = '0' THEN
	                                    '未完成'
                                    WHEN lol.Eval_stat = '1' THEN
	                                    '已完成'
                                    END AS '状态'
                                    FROM
	                                    Hanmote_Base_User info,
	                                    SR_Info sr,
	                                    HanmoteBaseUser_EvalCode lo,
	                                    Eval_file_Info lol
                                    WHERE
	                                    lol.EvaluateID = info.User_ID
                                    AND lo.UserID = info.User_ID
                                    AND sr.SupplierId = lol.supplierID
                                    AND sr.SupplierId = '" + id + "'");
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        void GeneralIDAL.updateEvaluate(string obj1, string obj2, string obj3, string obj4, string obj5)
        {
            string sql = "insert into Eval_file_Info (supplierID,MT_GroupID,EvaluateID,Eval_stat,Eval_Item)values('"+obj1+"','"+obj2 + "','" + obj3 + "','" + obj4 + "','" + obj5 + "'" +"); " ;
            DBHelper.ExecuteQueryDT(sql);
            
        }
        void GeneralIDAL.deal1(string obj1, string obj2, string obj3, string obj4, string obj5)
        {
            string sql = "insert into Eval_file_Info (supplierID,MT_GroupID,EvaluateID,Eval_score,EvalSumScore,Eval_stat)values('" + obj1 + "','" + obj2 + "','" + obj3 + "','" + obj4 + "','" + obj5 + "','1'" + "); "; 
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.insert(string id, string thing)
        {
            string sql = "insert into File_status (ID,Mt_GroupName,status_self_supplier,status_eval,LocalStatus)values('" + id + "','" + thing +  "'" + ",'0','0','0'); ";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.inserttask(string UserID, string state)
        {
            //string sql = "insert into File_status (ID,Mt_GroupName,status_self_supplier,status_eval,LocalStatus)values('" + id + "','" + thing + "'" + ",'0','0','0'); ";
            string sql = "insert into Hanmote_TaskSpecification(UserID,TaskSpecification,isFinished,DateTime) values('" + UserID + "','" + state + "','0',getdate())";

            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.Updatetask(string zhuid, string sname)
        {
            //string sql = "insert into File_status (ID,Mt_GroupName,status_self_supplier,status_eval,LocalStatus)values('" + id + "','" + thing + "'" + ",'0','0','0'); ";
            //string sql = "insert into Hanmote_TaskSpecification(UserID,TaskSpecification,isFinished,DateTime) values('" + UserID + "','" + state + "','0',getdate())";
            string sql = "update Hanmote_TaskSpecification set isFinished = '1', DateTime=getdate()  where UserID='" + zhuid + "' and TaskSpecification like '%" + sname + "%'";

            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.Updatetask1(string zhuid, string sname)
        {
            string sql = "update Hanmote_TaskSpecification set isFinished = '0', DateTime=getdate()  where UserID='" + zhuid + "' and TaskSpecification like '%" + sname + "%'";

            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.updateTask(string UserID, string state)
        {
            string sql = "update Hanmote_TaskSpecification set isFinished = '1' where UserID='" + UserID + "' and TaskSpecification='" + state + "'";

            DBHelper.ExecuteQueryDT(sql);

        }
        /* DataTable GeneralIDAL.GetAllInfo1()//加载所有信息
         {
             string sql = "SELECT SupplierId as '供应商编号',CompanyName as '供应商名称',Country as '区域',ContactName as '联系人',PhoneNumber as '联系方式',ContactMail as '电子邮件',RegisterStatus as '状态',SelectedMType as '物料组' FROM [SR_info]";
             DataTable dt = DBHelper.ExecuteQueryDT(sql);
             return dt;
         }*/
        void GeneralIDAL.RestartInfo(string id)//预评重置
        {
            string sql = "update SR_Info set RegisterStatus='新的',TagSecond ='0',TagThird='0',TagPermission='0',Agree='0',TagFour='0',status_Super='0',TagLocalEval='0',TagRefuse ='0'where SupplierId='" + id + "'";
            string sqlstrDel = @"delete from Eval_file_Info where supplierID='" + id + @"'; 
                                delete from File_status where ID='" + id + @"'; 
                                delete from Hanmote_Decision_Eval where Supplier_ID='" + id + @"'; 
                                delete from LocalEval_Info where supplierID='" + id + @"'; 
                                delete from Supplier_MainAccount where Supplier_Id='" + id + "';";
            DBHelper.ExecuteNonQuery(sqlstrDel);
            DBHelper.ExecuteQueryDT(sql);
          
        }
        void GeneralIDAL.adelete(string id)
        {
            string sql = "delete from File_status where ID='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.adeletetask(string zid, string zzid)
        {
            string sql = "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + zzid + "%' and UserID='"+zid+"'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.adeletetask1(string zzid)
        {
            string sql = "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + zzid + "%'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.adeletetask2(string zzid)
        {
            string sql = "delete from Hanmote_TaskSpecification where TaskSpecification <> '" + zzid + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.deal(string id, string reason, string score, string sum, string isno, string eid)
        {
            string sql = "update Eval_file_Info set Eval_stat='1',Eval_score ='"+score+ "',EvalSumScore ='"+sum+ "',IsNo ='"+isno+ "',Eval_ReasonInfo ='" + reason + "'where SupplierId='" + id + "'and EvaluateID='" + eid + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.deal1(string reason,string id)
        {
            string sql = "update Eval_file_Info set MainEvalAdvice ='" + reason + "'where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInf(string email)//预评处理状态置1
        {
            string sql = "update SR_Info set RegisterStatus='旧的',TagSecond ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInf1(string email)//预评处理状态置1
        {
            string sql = "update SR_Info set TagThird ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInf2(string email)//预评处理状态置1
        {
            string sql = "update SR_Info set status_Super ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInf3(string id, string gid, string User_ID, string level, string reason)//提交高层插入数据
        {
            string sql = "insert into Hanmote_Decision_Eval (Supplier_ID,User_ID,SID,PositionDescription,stepTag,backTag,statusEnd,suggestion)values('" + id + "','" + gid + "','" +User_ID + "','高层领导','1','1','" + level + "','" + reason  + "'); ";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.pdelete(string id)//提交高层插入数据
        {
            string sql = "delete from Hanmote_Decision_Eval  where Supplier_ID='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.prdelete(string id,string eid)//提交高层插入数据
        {
            string sql = "delete from Eval_file_Info  where supplierID='" + id + "' and EvaluateID='"+eid+"'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RejectInf(string email)//预评拒绝状态置1
        {
            string sql = "update SR_Info set RegisterStatus='旧的',TagSecond ='1',TagRefuse ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RejectInf1(string email)//预评拒绝状态置1
        {
            string sql = "update SR_Info set TagThird ='1',TagRefuse ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.restart(string email)//预评拒绝状态置1
        {
            string sql = "update SR_Info set TagThird ='0' where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.deleteInfo(string id)//删除已建立的评估员小组信息
        {
            string sql = "delete from Eval_file_Info where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.deleteInfo1(string id, string eid)//删除已建立的评估员小组信息
        {
            string sql = "update Eval_file_Info set Eval_stat='0',Eval_score =NULL,EvalSumScore =NULL,IsNo =NULL,Eval_ReasonInfo =NULL where SupplierId='" + id + "'and EvaluateID='" + eid + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.Delete(string id)//删除已建立的评估员小组信息
        {
            string sql = "DELETE FROM File_status WHERE ID='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInfo0(string id)//筛选重置
        {
            string sql = "update SR_Info set RegisterStatus='新的',TagSecond ='0',TagRefuse ='0'where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RestartInf0(string email)//筛选处理状态置1
        {
            string sql = "update SR_Info set RegisterStatus='旧的',TagSecond ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.RejectInf0(string email)//筛选拒绝状态置1
        {
            string sql = "update SR_Info set RegisterStatus='旧的',TagSecond ='1',TagRefuse ='1'where  SupplierId='" + email + "'";
            DBHelper.ExecuteQueryDT(sql);

        }
        bool GeneralIDAL.Get(string id)//筛选判断状态
        {
            string sql = "SELECT TagSecond FROM [SR_info] where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            
            if (dt.Rows[0][0].ToString().Equals("1"))
                    return false;
            else
                    return true;
           
        }
        bool GeneralIDAL.Get1(string id)//筛选判断状态
        {
            string sql = "SELECT TagThird FROM [SR_info] where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            if (dt.Rows[0][0].ToString().Equals("1"))
                return false;
            else
                return true;

        }
        bool GeneralIDAL.GetStatus(string id)//筛选判断状态
        {
            string sql = "SELECT Eval_stat FROM [Eval_file_Info] where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString().Equals("0"))
                    return false;
                
            }
            return true;

        }
        bool GeneralIDAL.GetTaskStatus(string id)//筛选判断状态
        {
            string sql = "SELECT isFinished FROM [Hanmote_TaskSpecification] where TaskSpecification='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString().Equals("0"))
                    return false;

            }
            return true;

        }
        bool GeneralIDAL.GetStatus1(string id)//筛选判断状态
        {
            string sql = "SELECT Eval_stat FROM [Eval_file_Info] where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if( dt.Rows.Count>0)
               return false;
            else
               return true;

        }
        bool GeneralIDAL.GetStatus2(string id,string eid)//筛选判断状态
        {
            string sql = "SELECT Eval_stat FROM [Eval_file_Info] where SupplierId='" + id + "'and EvaluateID='"+eid+"'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows[0][0].ToString().Equals("0"))
                return true;
            else
                return false;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="eid"></param>
        /// <returns></returns>
        bool GeneralIDAL.fGetStatus(string id)//
        {
            string sql = "SELECT status_self_supplier FROM File_status where ID='" + id +  "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows[0][0].ToString().Equals("1"))
                return true;
            else
                return false;

        }
        bool GeneralIDAL.ffGetStatus(string id)//
        {
            string sql = "SELECT EvaluateID FROM Eval_file_Info where SupplierId='" + id + "'";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        void GeneralIDAL.GetStatus3(string id, string eid)//重置
        {
            string sql = "update [Eval_file_Info] set  Eval_stat='0' where SupplierId='" + id + "'and EvaluateID='" + eid + "'";
            DBHelper.ExecuteQueryDT(sql);
        }
        DataTable GeneralIDAL.GetAll(string id)//加载所有信息
        {
            string sql = "SELECT * FROM [SR_info] where SupplierId='" + id+"';";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            /*if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
               
            }*/
            return dt;
        }
        DataTable GeneralIDAL.GetThingLevel(string thing)//加载所有信息
        {
            string sql = "SELECT MtGroupClass FROM Material_Group where Description='" + thing+ "';";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            
            return dt;
        }
        DataTable GeneralIDAL.Getzhuid(string thing)//加载所有信息
        {
            string sql = "SELECT UserID FROM Hanmote_TaskSpecification where TaskSpecification like '%" + thing + "%';";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }

            return dt;
        }
        DataTable GeneralIDAL.GetInfo(string UserId, string purchase, string thingGroup, string status, string country, string name)//筛选查询
        {

            string sql = @"SELECT
	                            SupplierId as 供应商编号,
	                            companyName as 供应商名称,
	                            Country as 区域,
	                            ContactName as 联系人,
	                            PhoneNumber as 联系方式,
	                            ContactMail as 电子邮件,
                            RegisterStatus as '状态' 
                            FROM
	                            SR_Info,
	                            Hanmote_User_MtGroupName hum,
	                            Material_Group mg
                            WHERE
                             mg.Description = SR_Info.SelectedMType
                            AND hum.Mt_Group_Name = SR_Info.SelectedMType
                            AND hum.Pur_Group_Name = SR_Info.SelectedDep
                            AND TagFirst = 	1
                           

                            AND hum.Type=1
                            AND hum.User_ID ='" + UserId + "'";
                            /*AND TagSecond = 0
                            AND TagThird = 0
                            AND TagLocalEval = 0
                            AND TagRefuse = 0*/

            if (status != null && status != "")
                sql += "and RegisterStatus = '" + status + "'";
            if (country != null && country != "")
                sql += "and Country = '" + country + "'";
            if (name != null && name != "")
                sql += "and CompanyName like '%" + name + "%'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            
            return dt;
        }
        DataTable GeneralIDAL.GetInfo1(string userId, string purchase, string thingGroup, string status, string country, string name)//预评查询相应信息<shi>
        {
            string sql = " SELECT distinct sr.SupplierId as '供应商编号',sr.CompanyName as '供应商名称',sr.Country as '区域',sr.ContactName as '联系人',sr.PhoneNumber as '联系方式',sr.ContactMail as '电子邮件',CASE WHEN ile.status_self_supplier = '1' THEN '已上传' WHEN ile.status_self_supplier = '0' THEN '未上传' END AS '上传状态' FROM SR_Info sr,File_status ile, Hanmote_User_MtGroupName hum where sr.Id!=''and sr.TagSecond = '1'and sr.TagRefuse = '0' and hum.Type=1 and sr.SupplierId=ile.ID and hum.User_ID = '" + userId + "' and hum.Mt_Group_Name = sr.SelectedMType ";
            string i = "";
            if (status != null && status != "")
            {
                if (status.Equals("已上传"))
                    i = "1";
                else
                    i = "0";
                sql += "  and ile.status_self_supplier = '" + i + "'";
            }
            if (country != null && country != "")
                sql += "  and Country = '" + country + "'";
            if (name != null && name != "")
                sql += "  and CompanyName like '%" + name + "%'";


            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        /// <summary>
        /// cahxun
        /// </summary>
        /// <param name="purchase"></param>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DataTable GeneralIDAL.GetInfo2(string purchase, string name,string id)//建立评估小组查询相应信息
        {
            string sql = @" SELECT
	                                            u.User_ID AS '评审员编号',
	                                            u.Username AS '评审员姓名',
                                                 sr.SelectedDep AS '采购组织',
                                                c.Name as '评估方面',
                                                u.Birth_Place AS '地址',
                                                 u.Mobile AS '手机',
	                                            u.Email AS '邮编'
                                            FROM
	                                            Hanmote_User_MtGroupName mt,
	                                            Hanmote_Base_User u,
	                                            SR_Info sr,
                                            HanmoteBaseUser_EvalCode e,
                                            LocalEvalClass_1 c
                                            WHERE
	                                            Type = 0
                                            AND u.User_ID = mt.User_ID
                                            and e.UserID = mt.User_ID
                                            and c.Id = e.codeID
                                            AND mt.Mt_Group_Name = sr.SelectedMType
                                            AND mt.Pur_Group_Name = sr.SelectedDep
                                            AND sr.SupplierId ='" + id+"'";

            if (purchase != null && purchase != "")
                sql += " and c.Name = '" + purchase + "'";
            
            if (name != null && name != "")
                sql += " and u.Username like '%" + name + "%'";


            DataTable dt = DBHelper.ExecuteQueryDT(sql);
             return dt;
        }
        DataTable GeneralIDAL.eGetInfo(string userId,string purchase, string thingGroup, string status, string country, string name)//评估员查询相应信息
        {

            // string sql = "SELECT CASE WHEN info1.Eval_stat = '1' THEN '已完成' WHEN info1.Eval_stat = '0' THEN '未完成' END AS '状态',info1.supplierID AS '供应商编号',sr.CompanyName AS '供应商名称',sr.Country AS '国家',sr.ContactName AS '联系人姓名' ,sr.PhoneNumber AS '联系方式',sr.ContactMail AS '邮箱',info1.EvaluateID as '评估员编号',CASE WHEN ile.status_self_supplier = '1' THEN '已上传' WHEN ile.status_self_supplier = '0' THEN '未上传' END AS '上传状态' FROM Eval_file_Info info1,SR_Info sr,File_status ile WHERE	info1.supplierID = sr.SupplierId and info1.supplierID=ile.ID";
            string sql = "SELECT CASE WHEN info1.Eval_stat = '1' THEN '已完成' WHEN info1.Eval_stat = '0' THEN '未完成' END AS '状态',info1.supplierID AS '供应商编号',sr.CompanyName AS '供应商名称',sr.Country AS '国家',sr.ContactName AS '联系人姓名' ,sr.PhoneNumber AS '联系方式',sr.ContactMail AS '邮箱',info1.EvaluateID as '评估员编号',CASE WHEN ile.status_self_supplier = '1' THEN '已上传' WHEN ile.status_self_supplier = '0' THEN '未上传' END AS '上传状态' FROM Eval_file_Info info1,SR_Info sr,File_status ile WHERE	info1.supplierID = sr.SupplierId and info1.EvaluateID = '" + userId + "' and info1.supplierID=ile.ID ";
            string status1;
            if (status != null && status != "")
            {
                if (status.Equals( "已完成"))
                    status1 = "1";
                else
                    status1 = "0";

                sql += " and info1.Eval_stat = '" + status1 + "'";
            }
            if (country != null && country != "")
                sql += " and sr.Country = '" + country + "'";
            if (name != null && name != "")
                sql += " and sr.CompanyName like '%" + name + "%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
       public  void updateAndDeleteTask(string user_ID, string supplierName)
        {
            string sqlstr = "delete from Hanmote_TaskSpecification where TaskSpecification   like '%" + supplierName + "%'";
            string sqlins = @"INSERT INTO  Hanmote_TaskSpecification (
	                                             UserID ,
	                                             TaskSpecification ,
	                                             isFinished 
                                            )
                                            VALUES
	                                            (
		                                             '"+ user_ID + @"',
		                                             '请对供应商：" + supplierName +@"待筛选',
		                                            '0'
	                                            );";
            DBHelper.ExecuteNonQuery(sqlstr);
            DBHelper.ExecuteNonQuery(sqlins);
        }

        public DataTable getEClassMtName()
        {
            string sql = "select Description from Material_Group where MtGroupClass='E'";
            return DBHelper.ExecuteQueryDT(sql);
        }
    }
}
