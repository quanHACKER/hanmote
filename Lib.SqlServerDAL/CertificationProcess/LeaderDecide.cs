﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.CertificationProcess
{
    /// <summary>
    /// 作者：聂雄超
    /// </summary>
    public class LeaderDecide
    {
        public DataTable GetSupplier(string ID)
        {
            string sql = @"SELECT DISTINCT SR_Info.SupplierId as '供应商编号',SR_Info.CompanyName as '供应商名称',SR_Info.Country as '区域',SR_Info.ContactName as '联系人',SR_Info.PhoneNumber as '联系方式',SR_Info.ContactMail as '电子邮件',SR_Info.Tagfour FROM [SR_info]INNER  JOIN  Hanmote_Decision_Eval ON Hanmote_Decision_Eval.Supplier_ID=SR_Info.SupplierId where Hanmote_Decision_Eval.User_ID='" + ID + "' and status_Super=1 and TagFour=0";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable GetAgree(string supplierId)
        {
            string sql = @"select Agree from SR_Info where SupplierId=@SupplierId";
            SqlParameter parameter = new SqlParameter("@SupplierId", supplierId);
            return DBHelper.ExecuteQueryDT(sql, parameter);
        }

        public DataTable selectDecideStaus(string supplierId)
        {
            string sql = @"SELECT
	                            hde.stepTag AS 审批层级,
	                            hde.statusEnd AS 审批层数,
	                            hde.IsAgree AS 审批状态,
	                            hbu.Username AS 审批人
                            FROM
	                            Hanmote_Decision_Eval hde,
	                            Hanmote_Base_User hbu
                            WHERE
	                            hbu.User_ID = hde.User_ID
                            AND hde.supplier_ID =@SupplierId";
            SqlParameter parameter = new SqlParameter("@SupplierId", supplierId);
            return DBHelper.ExecuteQueryDT(sql, parameter);
        }

        /// <summary>
        /// 决绝供应商
        /// </summary>
        /// <param name="supplierId">供应商编号</param>
        /// <param name="user_ID">审批ID</param>
        /// <param name="suggession">审批意见</param>
        /// <returns></returns>
        public int refuseSupplier(string supplierId,string  user_ID,string suggession)
        {
            //更新Hanmote_Decision_Eval表
            string sql = @"update Hanmote_Decision_Eval set IsAgree='不同意',super_Suggstion = @suggession,IsProcess=1 where User_ID =@user_ID and Supplier_ID=@supplierId";
            //更新SR_Info
            string sqlSR_Info = @"update SR_Info set TagFour=1,Agree =0 where  SupplierId='"+ supplierId + "' ";
            //插入任务
            string sqlTask = @"insert  into Hanmote_TaskSpecification values((select SID from Hanmote_Decision_Eval where Supplier_ID ='" + supplierId + "' and stepTag =1),(select CompanyName from SR_Info where SupplierId='" + supplierId + "')+'待准入',0,GETDATE())";
            SqlParameter[] parameter = {
                new SqlParameter("@user_ID",user_ID),
                new SqlParameter("@supplierId",supplierId),
                new SqlParameter("@suggession",suggession)
            };

            //判断是否两个都执行成功
            int flag = DBHelper.ExecuteNonQuery(sqlSR_Info)+ DBHelper.ExecuteNonQuery(sql, parameter)+ DBHelper.ExecuteNonQuery(sqlTask);
            if (flag == 3)
                flag = 1;
            else
                flag=0;
   
            return flag;
        }
        /// <summary>
        /// 获取物料组的绿色目标和目标
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public DataTable getCGreenValueAndGLowValue()
        {
            string sql = @"select CGoalValue as CGreenValue,CLowValue from TabGoalAndLowVal";
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 获取最终得分
        /// </summary>
        /// <param name="id">供应商id</param>
        /// <returns></returns>
        public DataTable getScore(string id, string flag)
        {
            //预评得分
            string sql = "select sum(Eval_score),SUM(EvalSumScore) from Eval_file_Info where supplierID='" + id + "'";
            if (flag.Equals("1"))
            {

                sql = "select sum(localEvalScore),SUM(localEvalSum) from LocalEval_Info where supplierID='" + id + "'";

            }
            //现场评估得分

            return DBHelper.ExecuteQueryDT(sql);
        }

        public void updateConditionToPermission(string supplierID)
        {
            string sql = "update SR_Info set TagThird = 1 ,TagRefuse = 0,TagFour = 1,TagLocalEval = 1,TagPermission = 0,status_Super = 1 where SupplierId='"+supplierID+"'";
            DBHelper.ExecuteNonQuery(sql);
        }

        /// <summary>
        /// 查询审批状态
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="supplierId"></param>
        /// <returns>stepTag，backTag,statusEnd</returns>
        public DataTable getDecideInfoBySupplierIdAndUserId(string user_ID, string supplierId)
        {
            string sql = "select * from Hanmote_Decision_Eval where Supplier_ID=@supplierId and User_ID=@user_ID";
            SqlParameter[] parameter = {
                new SqlParameter("@user_ID",user_ID),
                new SqlParameter("@supplierId",supplierId)
            };
            return DBHelper.ExecuteQueryDT(sql, parameter);
        }

        public void updateLevelInfo(string level, string supplierId)
        {
            string sql = @"update SR_Info set SupLevel =@level where SupplierId=@supplierId";
            SqlParameter[] parameter = {
                new SqlParameter("@level",level),
                new SqlParameter("@supplierId",supplierId)
            };
            DBHelper.ExecuteQueryDT(sql, parameter);
        }

        //获取终审决策信息
        public DataTable getIsAgree(string user_ID, string supplierID)
        {
            string sql = @"select distinct IsAgree from Hanmote_Decision_Eval where User_ID ='" + user_ID + "'  and Supplier_ID='" + supplierID + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getSuggesstion(string user_ID, string supplierID)
        {
            string sql = @"select distinct suggestion from Hanmote_Decision_Eval where User_ID ='" + user_ID + "'  and Supplier_ID='" + supplierID + "'";
            return DBHelper.ExecuteQueryDT(sql);

        }

        //插入高层评审信息
        public int insertDecideInfo(string supplierId, string suggession, string user_ID, int step, int statuEnd,string isAgree)
        {

            string sql = @"insert into Hanmote_Decision_Eval(Supplier_ID,User_ID,SID,PositionDescription,stepTag,backTag,statusEnd,suggestion,IsProcess) 
                            values(@supplierId,(select PID from Hanmote_Base_User where User_ID='" + user_ID + "'),@user_ID,'高层领导',@step,1,@statuEnd,@suggession,0) ";
            string sql_IsProcess = @"update Hanmote_Decision_Eval set IsProcess=1,super_Suggstion='"+ suggession + "',IsAgree='"+ isAgree + "'  where User_ID='" + user_ID + "' and Supplier_ID='"+ supplierId + "'";
           //插入任务
            string sqlTask = @"insert  into Hanmote_TaskSpecification values((select PID from Hanmote_Base_User where User_ID='" + user_ID + "'),(select CompanyName from SR_Info where SupplierId ='"+ supplierId + "')+'待审核',0,GETDATE())";
            
            SqlParameter[] parameter = {
                new SqlParameter("@user_ID",user_ID),
                new SqlParameter("@supplierId",supplierId),
                new SqlParameter("@step",step),
                new SqlParameter("@statuEnd",statuEnd),
                new SqlParameter("@suggession",suggession)
            };
            //修改处理状态IsProcess=1：
            DBHelper.ExecuteNonQuery(sql_IsProcess);
            DBHelper.ExecuteNonQuery(sqlTask);
            return DBHelper.ExecuteNonQuery(sql, parameter);
        }

        /// <summary>
        /// 主审直接领导反馈意见，更新SR_Info
        /// </summary>
        /// <param name="supplierId">供应商编号</param>
        /// <param name="user_ID">登陆高层编号</param>
        /// <param name="isAgree">决策意见</param>
        /// <returns></returns>
        public int upDataSupplierTag(string supplierId, string user_ID, string isAgree)
        {
            int agree = 0;
            if (isAgree.Equals("同意")) agree = 1;

            //更新处理标志
            string sql = @"update SR_Info set TagFour =1,Agree='" + agree + "' where SupplierId='" + supplierId + "'";

            DBHelper.ExecuteNonQuery(sql);
            //更新SR_Info

            sql = @"update Hanmote_Decision_Eval set IsProcess=2 where User_ID='" + user_ID + "'";
            return DBHelper.ExecuteNonQuery(sql);

        }

        /// <summary>
        /// 终审，之后向下反馈
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="suggession"></param>
        /// <param name="user_ID"></param>
        /// <param name="step"></param>
        /// <param name="backTag"></param>
        /// <param name="isAgree"></param>
        /// <param name="statuEnd"></param>
        /// <returns></returns>
        public int insertDecideInfoSubmitEnd(string user_ID, int backTag, string isAgree, string SupplierId,string suggession)
        {   //更新Hanmote_Decision_Eval意见，决策
            string sqlUpdate = @"update Hanmote_Decision_Eval set IsAgree=@isAgree,IsProcess=1,backTag=@backTag, super_Suggstion=@suggession where User_ID=@user_ID  and Supplier_ID=@supplierId";
            string sqlTask = @"insert  into Hanmote_TaskSpecification values((select SID from Hanmote_Decision_Eval where Supplier_ID ='"+ SupplierId + "' and stepTag =1),(select CompanyName from SR_Info where SupplierId='"+ SupplierId + "')+'待准入',0,GETDATE())";
            string sql;
            int agree = 0;
            if (isAgree.Equals("同意")) agree = 1;

            //string sql_IsProcess = @"update Hanmote_Decision_Eval set IsProcess=2,IsAgree ='" + isAgree + "'  where User_ID='" + user_ID + "' and Supplier_ID='" + SupplierId + "'";
            SqlParameter[] parameter = {
                new SqlParameter("@user_ID",user_ID),
                new SqlParameter("@backTag",backTag),
                new SqlParameter("@isAgree",isAgree),
                new SqlParameter("@supplierId",SupplierId),
                new SqlParameter("@suggession",suggession)

            };
            
           // string sqlStatusEnd = @"select statusEnd from Hanmote_Decision_Eval where Supplier_ID='" + SupplierId + "' and User_ID='" + user_ID + "'";
            //判断一级物料
            //string statusEnd = DBHelper.ExecuteQueryDT(sqlStatusEnd).Rows[0][0].ToString();


           // if (statusEnd.Equals("1"))
          //  {

                //更新处理标志
                sql = @"update SR_Info set TagFour =1,Agree='" + agree + "' where SupplierId='" + SupplierId + "'";


           // }
            DBHelper.ExecuteNonQuery(sqlUpdate, parameter);
            DBHelper.ExecuteNonQuery(sqlTask);

            return DBHelper.ExecuteNonQuery(sql);
        }


        /// <summary>
        /// 根据条件查询决策供应商信息
        /// </summary>
        /// <param name="purchase"></param>
        /// <param name="thingGroup"></param>
        /// <param name="status"></param>
        /// <param name="country"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataTable QueryCondition(string purchase, string thingGroup, string status, string country, string name, int flag, string ID)
        {
            SqlParameter parameter = new SqlParameter("@User_ID", ID);
            string sql = "";
            string sql1 = "";

            if (flag == 1)
            {

                sql1 = @"SELECT DISTINCT SR_Info.SupplierId as '供应商编号',SR_Info.CompanyName as '供应商名称',SR_Info.Country as '区域',SR_Info.ContactName as '联系人',SR_Info.PhoneNumber as '联系方式',SR_Info.ContactMail as '电子邮件',SR_Info.Tagfour,SupLevel  FROM[SR_info]INNER JOIN  Hanmote_Decision_Eval ON Hanmote_Decision_Eval.Supplier_ID = SR_Info.SupplierId where Hanmote_Decision_Eval.User_ID = '" + ID + "' and status_Super = 1 and TagFour = 0 ";

            }
            if (flag == 2)
            {
                sql = @"SELECT
	DISTINCT SupplierId as '供应商编号',CompanyName as '供应商名称',Country as '区域',ContactName as '联系人',PhoneNumber as '联系方式',ContactMail as '电子邮件',TagLocalEval,SupLevel 
FROM
	SR_Info sr,
  Hanmote_Decision_Eval hde
WHERE
hde.Supplier_ID=sr.SupplierId and hde.SID=@User_ID";
            }


            string sql2 = @"  and sr.SelectedMType IN (

        SELECT
            Mt_Group_Name

        FROM
            Hanmote_User_MtGroupName

        WHERE
            User_ID = @User_ID
	)union all

    SELECT DISTINCT SupplierId as '供应商编号',CompanyName as '供应商名称',Country as '区域',ContactName as '联系人',PhoneNumber as '联系方式',ContactMail as '电子邮件',TagLocalEval,SupLevel FROM SR_Info,Hanmote_Decision_Eval where Hanmote_Decision_Eval.Supplier_ID = SR_Info.SupplierId and Hanmote_Decision_Eval.User_ID = @User_ID ";

            //已准入供应商
            if (status != null && status != "" && status.Equals("已拒绝"))
            {
                sql1 += "  and  TagRefuse=1 and TagPermission=0";
                sql += "  and  TagRefuse=1 and TagPermission=0";
                sql2 += "  and  TagRefuse=1 and TagPermission=0";
            }


            if (status != null && status != "" && status.Equals("已准入"))
            {
                sql += "  and  TagRefuse=0  and TagPermission=1";
                sql1 += "  and  TagRefuse=0  and TagPermission=1";
                sql2 += "  and  TagRefuse=0  and TagPermission=1";
            }

            //领导决策
            if (status != null && status != "" && status.Equals("已处理"))
                sql += " and TagFour =1 ";
            if (status != null && status != "" && status.Equals("未处理"))
                sql += "  and TagFour =0";

            //物料组
            if (thingGroup != null && thingGroup != "")
            {
                sql1 += "  and SelectedMType = '" + thingGroup + "'";
                sql += "  and SelectedMType = '" + thingGroup + "'";
                sql2 += "  and SelectedMType = '" + thingGroup + "'";

            }

            //采购组织
            if (purchase != null && purchase != "")
            {
                sql1 += "  and SelectedDep = '" + purchase + "'";
                sql += "  and SelectedDep = '" + purchase + "'";
                sql2 += "  and SelectedDep = '" + purchase + "'";
            }

            //国家
            if (country != null && country != "")
            {
                sql1 += "  and Country = '" + country + "'";
                sql += "  and Country = '" + country + "'";
                sql2 += "  and Country = '" + country + "'";
            }

            //供应商名称
            if (name != null && name != "")
            {
                sql1 += "  and CompanyName like '%" + name + "%'";
                sql += "  and CompanyName like '%" + name + "%'";
                sql2 += "  and CompanyName like '%" + name + "%'";
            }

            if (flag == 1)
            {
                return DBHelper.ExecuteQueryDT(sql1, parameter);

            }

            return DBHelper.ExecuteQueryDT(sql + sql2, parameter);
        }

        //查询主审意见
        public DataTable queryMainAdvice(int tagFour, string supplierID)
        {
            string tableName = "LocalEval_Info";
            if (tagFour == 0)
            {
                tableName = "Eval_file_Info";
            }
            string sql = @"select DISTINCT MainEvalAdvice from " + tableName + " where supplierID='" + supplierID + "'";


            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable SelectSuppById(string id)
        {


            string sql = @"select DbsCode,CompanyName,ContactName,ContactPosition,PhoneNumber,CompanyFox,ContactMail,CompanyWebsite,Address,PostalCode,Agree,TagThird,TagLocalEval ,SelectedMType from SR_Info where SupplierId=@id ";
            SqlParameter[] parameter = {
                new SqlParameter("@id",id)
            };
            return DBHelper.ExecuteQueryDT(sql, parameter);

        }

        /// <summary>
        /// 上层领导提交决策意见
        /// </summary>
        /// 
        /// <param name="SupplierId"></param>
        /// <param name="Agree"></param>
        /// <returns></returns>
        public int InsertagreeInfo(String SupplierId, int Agree)
        {
            string sql = @"update SR_Info set Agree=@Agree,TagFour='1' where SupplierId=@SupplierId;";
            SqlParameter[] parameter = {
                new SqlParameter("@Agree",Agree),
                new SqlParameter("@SupplierId",SupplierId)
            };


            return DBHelper.ExecuteNonQuery(sql, parameter);

        }
        /// <summary>
        /// 查看准入供应商和拒绝供应商
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DataTable SelectPerimissionedSup(string ID,int pageSize, int pageIndex, out int total)
        {
            string sql = @"EXECUTE Proc_SR_InfoPermiss @pageSize, @pageIndex, @ID,null";
            SqlParameter[] parameter = {
                new SqlParameter("@ID",ID),
                new SqlParameter("@pageSize",pageSize),
                new SqlParameter("@pageIndex",pageIndex)

            };
            DataTable dt = DBHelper.ExecuteQueryDT(sql, parameter);
            if (dt.Rows.Count == 0)
            {
                total = 0;
            }
            else {
                total = (int)dt.Rows[0]["pageCount"];
            }
            dt.Columns.Remove("pageCount");
                return dt;
            
        }

        /// <summary>
        /// 查询被拒绝的供应商
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DataTable SelectRefusedSup(string ID)
        {

            string sql = @"SELECT  DISTINCT SupplierId as '供应商编号',CompanyName as '供应商名称',Country as '区域',ContactName as '联系人',PhoneNumber as '联系方式',ContactMail as '电子邮件' FROM SR_Info where TagRefuse=1";

            return DBHelper.ExecuteQueryDT(sql);
        }




    }
}
