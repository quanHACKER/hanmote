﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Lib.IDAL.CertificationProcess.LocationalEvaluation;
using Lib.Model.CertificationProcess.LocationEvaluation;

namespace Lib.SqlServerDAL.CertificationProcess.LocationEvaluation
{
    /// <summary>  
    /// author：hezl
    /// </summary>  
    class LocationalEvaluationDAL : LocationalEvaluationIDAL
    {
        public DataTable calculateRecordNumber()
        {
            String sqlText = "select count(Id) from SR_Info where TagRefuse='0'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public int calculateUserNumber()
        {
            DataTable userInforTable = calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        } 

        public DataTable findAllCompanyList(string userId,int pageNum, int pageIndex,out int totalSize)
        {
            String sqlText = String.Format(@"SELECT
	                                                SupplierId,
	                                                companyName,
	                                                Country,
	                                                ContactName,
	                                                PhoneNumber,
	                                                ContactMail,
	                                                SelectedMType AS MtGroupName,
	                                                SelectedDep AS 采购组织
                                                FROM
	                                                SR_Info,
	                                                Hanmote_User_MtGroupName hum,
	                                                Material_Group mg
                                                WHERE
	                                                mg.MtGroupClass != 'E'
                                                AND mg.Description = hum.Mt_Group_Name
                                                AND hum.Mt_Group_Name = SR_Info.SelectedMType
                                                AND TagFirst = 1
                                                AND TagSecond = 1
                                                AND TagThird = 1
                                                AND TagLocalEval = 0
                                                AND TagRefuse = 0
                                                AND hum.User_ID = '" + userId+"'", pageNum, pageIndex);


            string sql = @"EXECUTE Proc_SR_InfoMainPerLocation @pageSize, @pageIndex, @userId,null";
            SqlParameter[] parameter = {
                new SqlParameter("@userId",userId),
                new SqlParameter("@pageSize",pageNum),
                new SqlParameter("@pageIndex",pageIndex)
            };
            DataTable dt = DBHelper.ExecuteQueryDT(sql, parameter);
            
            if (dt.Rows.Count == 0)
            {
                totalSize = 0;
            }
            else
            {
                totalSize = (int)dt.Rows[0]["pageCount"];
            }
            dt.Columns.Remove("pageCount");
            return dt;
        }

        public DataTable findAllCompanyListByCondition(string userId,LocationalEvaluationSupperModel locationalEvaluationModel, int pageNum, int frontNum)
        {
            StringBuilder sqlText = new StringBuilder(@"SELECT
	                                                            SupplierId,
	                                                            companyName,
	                                                            Country,
	                                                            ContactName,
	                                                            PhoneNumber,
	                                                            ContactMail,
	                                                            SelectedMType AS MtGroupName,
	                                                            SelectedDep AS 采购组织
                                                            FROM
	                                                            SR_Info,
	                                                            Hanmote_User_MtGroupName hum,
	                                                            Material_Group mg
                                                            WHERE
	                                                            mg.MtGroupClass != 'E'
                                                            AND mg.Description = SR_Info.SelectedMType
                                                            AND hum.Mt_Group_Name = SR_Info.SelectedMType
                                                            AND hum.Pur_Group_Name = SR_Info.SelectedDep
                                                            AND TagFirst = 1
                                                            AND TagSecond = 1
                                                            AND TagThird = 1
                                                            AND TagLocalEval = 0
                                                            AND TagRefuse = 0
                                                            AND hum.User_ID = '" + userId+"'");
                                                                    StringBuilder tmpText = new StringBuilder("");

            if (!String.IsNullOrEmpty(locationalEvaluationModel.companyName))
            {
                tmpText.Append(" and companyName like ");
                tmpText.Append("'%" + locationalEvaluationModel.companyName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.MtGroupName))
            {
                tmpText.Append(" and SelectedMType like ");
                tmpText.Append("'%" + locationalEvaluationModel.MtGroupName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.SelectedDep))
            {
                tmpText.Append(" and SelectedDep like ");
                tmpText.Append("'%" + locationalEvaluationModel.SelectedDep + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.Country))
            {
                tmpText.Append(" and Country like ");
                tmpText.Append("'%" + locationalEvaluationModel.Country + "%'");
            }
            sqlText.Append(tmpText);

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        public DataTable findSimplePersonEvalListByCondition(string userId,LocationalEvaluationSupperModel locationalEvaluationModel, int pageSize, int v)
        {
            StringBuilder sqlText = new StringBuilder(@"SELECT
                                  case
	                              when info.localEvalstat='1' then '已完成'
                                  when info.localEvalstat='0' then '未完成'
                                  END as status,
                                  info.supplierID ,
                                  sr.CompanyName,
                                  sr.Country,
                                  sr.ContactName,
                                  sr.PhoneNumber,
                                  sr.ContactMail,
                                  sr.SelectedMType as Mt_GroupName,
                                  sr.SelectedDep  as Pur_GroupName,
                                  info.evaluateID FROM
	                                LocalEval_Info info,
	                                SR_Info sr
                                WHERE TagRefuse=0 and 
	                                info.supplierID = sr.SupplierId and info.evaluateID='" + userId+"'");
            StringBuilder tmpText = new StringBuilder("");

            if (!String.IsNullOrEmpty(locationalEvaluationModel.companyName))
            {
                tmpText.Append(" and sr.CompanyName like ");
                tmpText.Append("'%" + locationalEvaluationModel.companyName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.MtGroupName))
            {
                tmpText.Append(" and sr.SelectedMType like ");
                tmpText.Append("'%" + locationalEvaluationModel.MtGroupName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.SelectedDep))
            {
                tmpText.Append(" and sr.SelectedDep like ");
                tmpText.Append("'%" + locationalEvaluationModel.SelectedDep + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.Country))
            {
                tmpText.Append(" and sr.Country like ");
                tmpText.Append("'%" + locationalEvaluationModel.Country + "%'");
            }
            if (!String.IsNullOrEmpty(locationalEvaluationModel.evalID))
            {
                tmpText.Append(" and info.evaluateID = ");
                tmpText.Append("'" + locationalEvaluationModel.evalID + "'");
            }
            sqlText.Append(tmpText);

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }
        /// <summary>
        /// 现场评估完结修改标志位
        /// </summary>
        /// <param name="companyID"></param>
        public void updateTagSuperSRInfoByCompanyID(string companyID)
        {
            String sqlText = String.Format(@"update SR_Info set status_Super = 1,TagLocalEval = 1 where TagRefuse = 0 and SupplierId = '" + companyID+"'");
            DBHelper.ExecuteNonQuery(sqlText);
        }
        
        /// <summary>
        /// 准入
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public DataTable findAllCertificatedLocalCompanyList(string userId,int pageSize, int v)
        {
            String sqlText = String.Format(@"SELECT
	                                                CASE
                                                WHEN Agree = '0' THEN
	                                                '不同意'
                                                WHEN Agree = '1' THEN
	                                                '同意'
                                                END AS Agree,
                                                 SupplierId,
                                                 companyName,
                                                 Country,
                                                 ContactName,
                                                 PhoneNumber,
                                                 ContactMail,
                                                 SelectedMType AS MtGroupName,
                                                 SelectedDep AS 采购组织
                                                FROM
	                                                SR_Info,
	                                                Hanmote_User_MtGroupName hum
                                                WHERE
	                                                TagFirst = 1
                                                AND TagSecond = 1
                                                AND TagThird = 1
                                                AND TagRefuse = 0
                                                AND TagFour = 1
                                                AND TagLocalEval = 1
                                                and TagPermission = 0 
                                                AND status_Super = 1
                                                AND hum.Mt_Group_Name = SR_Info.SelectedMType
                                                AND hum.User_ID = '" + userId+"'", pageSize, v);
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 准入列表
        /// </summary>
        /// <param name="searchCondition"></param>
        /// <returns></returns>
        public DataTable findCompanyCertificatedListByCondition(String userId,LocationalEvaluationSupperModel locationalEvaluationModel)
        {
            StringBuilder sqlText = new StringBuilder(@"SELECT
	                                                CASE
                                                WHEN Agree = '0' THEN
	                                                '不同意'
                                                WHEN Agree = '1' THEN
	                                                '同意'
                                                END AS Agree,
                                                 SupplierId,
                                                 companyName,
                                                 Country,
                                                 ContactName,
                                                 PhoneNumber,
                                                 ContactMail,
                                                 SelectedMType AS MtGroupName,
                                                 SelectedDep AS 采购组织
                                                FROM
	                                                SR_Info,
	                                                Hanmote_User_MtGroupName hum
                                                WHERE
	                                                TagFirst = 1
                                                AND TagSecond = 1
                                                AND TagThird = 1
                                                AND TagRefuse = 0
                                                AND TagFour = 1
                                                AND TagLocalEval = 1
                                                AND TagPermission = 0
                                                AND status_Super = 1
                                                AND hum.Mt_Group_Name = SR_Info.SelectedMType
                                                AND hum.User_ID = '" + userId + "'");
            StringBuilder tmpText = new StringBuilder("");

            if (!String.IsNullOrEmpty(locationalEvaluationModel.companyName))
            {
                tmpText.Append(" and companyName like ");
                tmpText.Append("'%" + locationalEvaluationModel.companyName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.MtGroupName))
            {
                tmpText.Append(" and SelectedMType like ");
                tmpText.Append("'%" + locationalEvaluationModel.MtGroupName + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.SelectedDep))
            {
                tmpText.Append(" and SelectedDep like ");
                tmpText.Append("'%" + locationalEvaluationModel.SelectedDep + "%'");
            }

            if (!String.IsNullOrEmpty(locationalEvaluationModel.Country))
            {
                tmpText.Append(" and Country like ");
                tmpText.Append("'%" + locationalEvaluationModel.Country + "%'");
            }
            sqlText.Append(tmpText);

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        public void insertHanmoteDecisionTable(string id, string companyID, string v,string suggestion)
        {
            string pid = DBHelper.ExecuteQueryDT("select pid from Hanmote_Base_User where User_ID='" + id + "'").Rows[0][0].ToString();
            string sql1 = @"SELECT
	                                mg.Evallevel
                                FROM
	                                SR_Info sr,
	                                Material_Group mg
                                WHERE TagRefuse = 0 and 
	                                sr.SupplierId = '" + companyID+@"'
                                AND SelectedMType IN (
	                                SELECT
		                                Mt_Group_Name
	                                FROM
		                                Hanmote_User_MtGroupName
	                                WHERE
		                                USER_ID = '"+id+@"'
                                )
                                AND sr.SelectedMType = mg.Description";
            string endstep = DBHelper.ExecuteQueryDT(sql1).Rows[0][0].ToString();
            string sqlstr = @"INSERT into Hanmote_Decision_Eval ( 
                                 [Supplier_ID],
                                 [User_ID],
                                 [SID],
                                 [PositionDescription],
                                 [stepTag],
                                 [backTag],
                                 [statusEnd],
                                 [suggestion],
                                 [IsAgree],
                                 [IsProcess]) VALUES('" + companyID+"', '"+pid+"', '"+id+"','"+v+"',1,1,'"+endstep+"','"+ suggestion + "','待审核',0)";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public DataTable findAllImprovementReportSupplier(string user_ID, int pageSize, int pageIndex,out int pageCount)
        {
            string sqlstr = @"SELECT
                                DISTINCT
	                            sr.SupplierId,
	                            mg.MtGroupClass,
	                            sr.CompanyName,
	                            sr.ContactMail,
	                            sr.ContactName,
	                            sr.PhoneNumber,
	                            CASE
                            WHEN sr.isSubmitImprovementReport = 1 THEN
	                            '已提交'
                            WHEN sr.isSubmitImprovementReport = 0 THEN
	                            '未提交'
                            WHEN sr.isSubmitImprovementReport = 2 THEN
	                            '已改善'
                            END AS isSubmitImprovementReport,
                             sr.Country,
                             SUBSTRING(CONVERT(nvarchar,li.localEvalScore/li.localEvalSum), 1, 4) AS rate,
                             mg.Description AS MtGroupName,
                             CASE
                            WHEN (li.localEvalScore/li.localEvalSum )< 0.9 THEN
	                            '需要改善'
                            WHEN mg.MtGroupClass = 'A'
                            AND (li.localEvalScore/li.localEvalSum )>= 0.9 THEN
	                            '不需要改善'
                            END AS status
                            FROM
	                            SR_Info sr,
	                            Material_Group mg,
	                            LocalEval_Info li
                            WHERE
	                            li.supplierID = sr.SupplierId
                            AND mg.Description = sr.SelectedMType
                            AND TagFirst = 1
                            AND TagSecond = 1
                            AND TagRefuse = 0
                            AND TagThird = 1
                            AND TagPermission = 1
                            AND TagLocalEval = 1
                            AND li.evaluateID = '" + user_ID+ @"'
                            AND sr.SelectedMType IN (
	                            SELECT
		                            Mt_Group_Name
	                            FROM
		                            Hanmote_User_MtGroupName
	                            WHERE
		                            User_ID = '" + user_ID + @"'
                            )";

            string sql = @"SELECT
	                            sr.SupplierId,
	                            mg.MtGroupClass,
	                            sr.CompanyName,
	                            sr.ContactMail,
	                            sr.ContactName,
	                            sr.PhoneNumber,
	                            CASE
                            WHEN sr.isSubmitImprovementReport = 1 THEN
	                            '已提交'
                            WHEN sr.isSubmitImprovementReport = 0 THEN
	                            '未提交'
                            WHEN sr.isSubmitImprovementReport = 2 THEN
	                            '已改善'
                            END AS isSubmitImprovementReport,
                             sr.Country,
                            SUBSTRING(CONVERT(nvarchar,li.Eval_score/li.EvalSumScore), 1, 6) AS rate,
                             mg.Description AS MtGroupName,
                             CASE
                            WHEN (li.Eval_score/li.EvalSumScore)< 0.9 THEN
	                            '需要改善'
                            WHEN mg.MtGroupClass = 'E'
                            AND (li.Eval_score/li.EvalSumScore)>= 0.9 THEN
	                            '不需要改善'
                            END AS status
                            FROM
	                            SR_Info sr,
	                            Material_Group mg,
	                            Eval_file_Info li
                            WHERE
	                            li.supplierID = sr.SupplierId
                            AND mg.MtGroupClass = 'E'
                            AND mg.Description = sr.SelectedMType
                            AND TagFirst = 1
                            AND TagSecond = 1
                            AND TagThird = 1
                            AND TagPermission = 1
                            AND li.evaluateID = '" + user_ID + @"'
                            AND sr.SelectedMType IN (
	                            SELECT
		                            Mt_Group_Name
	                            FROM
		                            Hanmote_User_MtGroupName
	                            WHERE
		                            User_ID = '" + user_ID + @"'
                            )";
            DBHelper.ExecuteQueryDT(sql);
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql);
            sqlstr = @"EXECUTE Proc_SR_InfoImproved @pageSize,@pageIndex,@user_ID,null";
            SqlParameter[] sqlParas = new SqlParameter[] {
                new SqlParameter("@pageSize",pageSize),
                new SqlParameter("@pageIndex",pageIndex),
                new SqlParameter("@user_ID",user_ID),
            };
            DataTable dt2 = DBHelper.ExecuteQueryDT(sqlstr, sqlParas);
            if(dt1.Rows.Count>0)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    dt2.Rows.Add(new Object[] { dt1.Rows[i][0], dt1.Rows[i][1], dt1.Rows[i][2], dt1.Rows[i][3], dt1.Rows[i][4], dt1.Rows[i][5], dt1.Rows[i][6], dt1.Rows[i][7], dt1.Rows[i][8], dt1.Rows[i][9], dt1.Rows[i][10] });
                }
            }
            if (dt2.Rows.Count == 0)
            {
                pageCount = 0;
            }
            else
            {
                pageCount = (int)dt2.Rows[0]["pageCount"];
            }
            dt2.Columns.Remove("pageCount");
            return dt2;
        }
        public DataTable findAllImprovementReportSupplierByCondition(string user_ID, int pageSize, int v, string nationalName, string companyNam,string isProvement)
        {
            StringBuilder sqlLocal = new StringBuilder(@"SELECT
                                DISTINCT
	                            sr.SupplierId,
	                            mg.MtGroupClass,
	                            sr.CompanyName,
	                            sr.ContactMail,
	                            sr.ContactName,
	                            sr.PhoneNumber,
	                            CASE
                            WHEN sr.isSubmitImprovementReport = 1 THEN
	                            '已提交'
                            WHEN sr.isSubmitImprovementReport = 0 THEN
	                            '未提交'
                            WHEN sr.isSubmitImprovementReport = 2 THEN
	                            '已改善'
                            END AS isSubmitImprovementReport,
                             sr.Country,
                             SUBSTRING(CONVERT(nvarchar,li.localEvalScore/li.localEvalSum), 1, 6) AS rate,
                             mg.Description AS MtGroupName,
                             CASE
                            WHEN (li.localEvalScore/li.localEvalSum )< 0.9 THEN
	                            '需要改善'
                            WHEN mg.MtGroupClass = 'A'
                            AND (li.localEvalScore/li.localEvalSum )>= 0.9 THEN
	                            '不需要改善'
                            END AS status
                            FROM
	                            SR_Info sr,
	                            Material_Group mg,
	                            LocalEval_Info li
                            WHERE
	                            li.supplierID = sr.SupplierId
                            AND mg.Description = sr.SelectedMType
                            AND TagFirst = 1
                            AND TagSecond = 1
                            AND TagThird = 1
                            AND TagRefuse=0
                            AND TagPermission = 1
                            AND TagLocalEval = 1
                            AND li.evaluateID = '" + user_ID + @"'
                            AND sr.SelectedMType IN (
	                            SELECT
		                            Mt_Group_Name
	                            FROM
		                            Hanmote_User_MtGroupName
	                            WHERE
		                            User_ID = '" + user_ID + @"'
                            )");

            StringBuilder sqlPer = new StringBuilder(@"SELECT
                                 DISTINCT
	                            sr.SupplierId,
	                            mg.MtGroupClass,
	                            sr.CompanyName,
	                            sr.ContactMail,
	                            sr.ContactName,
	                            sr.PhoneNumber,
	                            CASE
                            WHEN sr.isSubmitImprovementReport = 1 THEN
	                            '已提交'
                            WHEN sr.isSubmitImprovementReport = 0 THEN
	                            '未提交'
                            WHEN sr.isSubmitImprovementReport = 2 THEN
	                            '已改善'
                            END AS isSubmitImprovementReport,
                             sr.Country,
                            SUBSTRING(CONVERT(nvarchar,li.Eval_score/li.EvalSumScore), 1, 6) AS rate,
                             mg.Description AS MtGroupName,
                             CASE
                            WHEN (li.Eval_score/li.EvalSumScore)< 0.9 THEN
	                            '需要改善'
                            WHEN mg.MtGroupClass = 'E'
                            AND (li.Eval_score/li.EvalSumScore)>= 0.9 THEN
	                            '不需要改善'
                            END AS status
                            FROM
	                            SR_Info sr,
	                            Material_Group mg,
	                            Eval_file_Info li
                            WHERE
	                            li.supplierID = sr.SupplierId
                            AND mg.MtGroupClass = 'E'
                            AND mg.Description = sr.SelectedMType
                            AND TagFirst = 1
                            AND TagSecond = 1
                            AND TagThird = 1
                            AND TagRefuse = 0
                            AND TagPermission = 1
                            AND li.evaluateID = '" + user_ID + @"'
                            AND sr.SelectedMType IN (
	                            SELECT
		                            Mt_Group_Name
	                            FROM
		                            Hanmote_User_MtGroupName
	                            WHERE
		                            User_ID = '" + user_ID + @"'
                            )");
            StringBuilder tmpText1 = new StringBuilder("");
            StringBuilder tmpText2 = new StringBuilder("");
            if (!String.IsNullOrEmpty(nationalName))
            {
                tmpText1.Append(" and sr.Country like ");
                tmpText1.Append("'%" + nationalName + "%'");
                tmpText2.Append(" and sr.Country like ");
                tmpText2.Append("'%" + nationalName + "%'");
            }

            if (!String.IsNullOrEmpty(companyNam))
            {
                tmpText1.Append(" and sr.CompanyName like ");
                tmpText1.Append("'%" + companyNam  + "%'");
                tmpText2.Append(" and sr.CompanyName like ");
                tmpText2.Append("'%" + companyNam + "%'");
            }
            if (!String.IsNullOrEmpty(isProvement) && isProvement.Equals("不需要改善"))
            {
                tmpText1.Append(" and (sr.SupLevel = ");
                tmpText1.Append("'A' or sr.isSubmitImprovementReport = 2) ");
                tmpText2.Append(" AND (sr.isSubmitImprovementReport = 2 or (li.Eval_score/li.EvalSumScore)>0.9) ");
            }
            if (!String.IsNullOrEmpty(isProvement) && isProvement.Equals("需要改善"))
            {
                tmpText1.Append(" and sr.SupLevel != ");
                tmpText1.Append("'A' and (li.localEvalScore / li.localEvalSum)<0.9 ");
                tmpText2.Append(" and sr.SupLevel != ");
                tmpText2.Append("'A' and (li.Eval_score/li.EvalSumScore)<0.9 ");
            }
            sqlLocal.Append(tmpText1);
            DataTable dt1 = DBHelper.ExecuteQueryDT(sqlPer.Append(tmpText2).ToString());
            DataTable dt2 = DBHelper.ExecuteQueryDT(sqlLocal.ToString());
            if (dt1.Rows.Count > 0)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    dt2.Rows.Add(new Object[] { dt1.Rows[i][0], dt1.Rows[i][1], dt1.Rows[i][2], dt1.Rows[i][3], dt1.Rows[i][4], dt1.Rows[i][5], dt1.Rows[i][6], dt1.Rows[i][7], dt1.Rows[i][8], dt1.Rows[i][9], dt1.Rows[i][10] });
                }
            }
            return dt2;
        }

        public string getISSubmitImprovementReportBySupplierId(string companyID)
        {
            string sql = "select isSubmitImprovementReport from SR_Info where TagRefuse=0 and SupplierId='" + companyID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql); 
            if(dt.Rows.Count>0)
            {
                return dt.Rows[0][0].ToString();
            }
            return "false";
        }

        public void setSupplierLevelValueABySupplierID(string companyID)
        {
            string sqlstr = "update SR_Info set SupLevel='A',isSubmitImprovementReport = 2  where SupplierId = '" + companyID+"'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public string getPidByCurUserID(string user_ID)
        {
            string sql = "select pid from Hanmote_Base_User where User_ID='"+user_ID+"'";
            return DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
        }

        public void updateTagRefuseSRInfoByCompanyID(string iD)
        {
            string sqlstr = "update SR_Info set TagRefuse= '1'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
    }
}
