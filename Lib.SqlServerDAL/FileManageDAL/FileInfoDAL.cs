﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.FileManage;
using Lib.Model.FileManage;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.FileManageDAL
{
    public class FileInfoDAL : FileInfoIDAL
    {
        /// <summary>
        /// 获取文件信息
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public File_Info getFileInfo(string fileID) {
            StringBuilder strBui = new StringBuilder("select * from File_Info where File_ID = '")
                .Append(fileID).Append("'");
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            if (dt.Rows.Count == 0)
                return null;
            
            DataRow row = dt.Rows[0];
            File_Info fileInfo = new File_Info();
            fileInfo.File_ID = row["File_ID"].ToString();
            fileInfo.File_Type = row["File_Type"].ToString();
            fileInfo.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
            fileInfo.Creator_ID = row["Creator_ID"].ToString();
            fileInfo.Creator_Type = row["Creator_Type"].ToString();
            fileInfo.Modify_Time = Convert.ToDateTime(row["Modify_Time"].ToString());
            fileInfo.Supplier_Visible_Level = row["Supplier_Visible_Level"].ToString();
            fileInfo.Description = row["Description"].ToString();

            return fileInfo;
        }

        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<File_Info> getFileInfoListByConditions(Dictionary<string, string> conditions,
            DateTime beginTime, DateTime endTime) {

            List<File_Info> result = new List<File_Info>();
            StringBuilder strBui = new StringBuilder("select * from File_Info where Create_Time >= '")
                .Append(beginTime.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("'")
                .Append(" and Create_Time <= '").Append(endTime.ToString("yyyy-MM-dd hh:mm:ss.fff"))
                .Append("'");
            foreach (KeyValuePair<string, string> kvPair in conditions) { 
                string key = kvPair.Key;
                string value = kvPair.Value;
                strBui.Append(" and ").Append(key).Append(" = '")
                    .Append(value).Append("'");
            }
            strBui.Append(" order by File_Type, Create_Time");

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            foreach (DataRow row in dt.Rows) {
                File_Info file = new File_Info();
                file.File_ID = row["File_ID"].ToString();
                file.File_Type = row["File_Type"].ToString();
                file.Supplier_Visible_Level = row["Supplier_Visible_Level"].ToString();
                file.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
                file.Modify_Time = Convert.ToDateTime(row["Modify_Time"].ToString());
                file.Creator_ID = row["Creator_ID"].ToString();
                file.Creator_Type = row["Creator_Type"].ToString();
                file.Description = row["Description"].ToString();

                result.Add(file);
            }

            return result;
        }

        /// <summary>
        /// 添加文件信息
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public int addFileInfo(File_Info fileInfo, 
            List<File_Attachment_Info> fileAttachementList) {
            LinkedList<string> sqlTextList = new LinkedList<string>();
            # region 添加文件主体信息
            
            StringBuilder strBui = new StringBuilder("insert into File_Info (File_ID, Description, File_Type,")
                .Append("Creator_ID, Creator_Type, Create_Time, Modify_Time, Supplier_Visible_Level) values( '")
                .Append(fileInfo.File_ID).Append("', '").Append(fileInfo.Description).Append("', '")
                .Append(fileInfo.File_Type).Append("', '").Append(fileInfo.Creator_ID).Append("', '")
                .Append(fileInfo.Creator_Type).Append("', '").Append(fileInfo.Create_Time.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', '")
                .Append(fileInfo.Modify_Time.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', '").Append(fileInfo.Supplier_Visible_Level).Append("')");
            sqlTextList.AddLast(strBui.ToString());
            
            #endregion
            
            #region 添加文件附件信息

            foreach (File_Attachment_Info fileAttachment in fileAttachementList) {
                strBui = new StringBuilder("insert into File_Attachment_Info (File_ID, ")
                    .Append("Attachment_Name, Attachment_Location, Upload_Time, Attachment_Size, ")
                    .Append("Attachment_Download_Times) values('").Append(fileAttachment.File_ID)
                    .Append("', '").Append(fileAttachment.Attachment_Name).Append("', '")
                    .Append(fileAttachment.Attachment_Location).Append("', '")
                    .Append(fileAttachment.Upload_Time.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', ")
                    .Append(fileAttachment.Attachment_Size).Append(", ")
                    .Append(fileAttachment.Attachment_Download_Times).Append(")");
                sqlTextList.AddLast(strBui.ToString());
            }

            #endregion

            #region 添加文件权限信息(暂时未添加)

            //暂时没添加

            #endregion

            return DBHelper.ExecuteNonQuery(sqlTextList);
        }

        /// <summary>
        /// 删除整个文件的数据库信息
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public int deleteFileInfo(string fileID) {
            LinkedList<string> sqlTextList = new LinkedList<string>();
            #region 删除文件主体信息

            StringBuilder strBui = new StringBuilder("delete from File_Info where File_ID = '")
                .Append(fileID).Append("'");
            sqlTextList.AddLast(strBui.ToString());

            #endregion

            #region 删除文件附件信息

            strBui = new StringBuilder("delete from File_Attachment_Info where File_ID = '")
                .Append(fileID).Append("'");
            sqlTextList.AddLast(strBui.ToString());

            #endregion

            #region 删除文件权限信息

            strBui = new StringBuilder("delete from File_Permission where File_ID = '")
                .Append(fileID).Append("'");
            sqlTextList.AddLast(strBui.ToString());

            #endregion

            return DBHelper.ExecuteNonQuery(sqlTextList);
        }

        /// <summary>
        /// 更新文件信息
        /// </summary>
        /// <param name="fileInfo">更新后的文件信息</param>
        /// <param name="fileAttachmentList">更新后的所有附件信息</param>
        /// <returns></returns>
        public int updateFileInfo(File_Info fileInfo, List<File_Attachment_Info> fileAttachmentList)
        {
            LinkedList<string> sqlList = new LinkedList<string>();
            #region 更新文件主体信息
            
            StringBuilder strBui = new StringBuilder("update File_Info set Description = '")
                .Append(fileInfo.Description).Append("', File_Type = '")
                .Append(fileInfo.File_Type).Append("', Modify_Time = '")
                .Append(fileInfo.Modify_Time.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', Supplier_Visible_Level = '")
                .Append(fileInfo.Supplier_Visible_Level).Append("' where File_ID = '")
                .Append(fileInfo.File_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 删除旧的附件信息

            strBui = new StringBuilder("delete from File_Attachment_Info where File_ID = '")
                .Append(fileInfo.File_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 插入新的附件信息

            foreach (File_Attachment_Info fileAttachment in fileAttachmentList) {
                strBui = new StringBuilder("insert into File_Attachment_Info (")
                    .Append("File_ID, Attachment_Name, Attachment_Location, Upload_Time, ")
                    .Append("Attachment_Size, Attachment_Download_Times ) values ('")
                    .Append(fileAttachment.File_ID).Append("', '")
                    .Append(fileAttachment.Attachment_Name).Append("', '")
                    .Append(fileAttachment.Attachment_Location).Append("', '")
                    .Append(fileAttachment.Upload_Time.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', ")
                    .Append(fileAttachment.Attachment_Size).Append(", ")
                    .Append(fileAttachment.Attachment_Download_Times).Append(" )");

                sqlList.AddLast(strBui.ToString());
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }
    }
}
