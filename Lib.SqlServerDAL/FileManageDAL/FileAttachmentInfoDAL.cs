﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.FileManage;
using Lib.IDAL.FileManage;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.FileManageDAL
{
    public class FileAttachmentInfoDAL : FileAttachmentInfoIDAL
    {
        /// <summary>
        /// 获取文件的附件信息
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public List<File_Attachment_Info> getFileAttachmentList(string fileID) {
            StringBuilder strBui = new StringBuilder("select * from File_Attachment_Info ")
                .Append("where File_ID = '").Append(fileID).Append("'");
            List<File_Attachment_Info> fileAttachmentList = new List<File_Attachment_Info>();
            
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            foreach (DataRow row in dt.Rows) {
                File_Attachment_Info fileAttachment = new File_Attachment_Info();
                fileAttachment.File_ID = row["File_ID"].ToString();
                fileAttachment.Attachment_Name = row["Attachment_Name"].ToString();
                fileAttachment.Attachment_Location = row["Attachment_Location"].ToString();
                fileAttachment.Attachment_Size = Convert.ToDouble(row["Attachment_Size"].ToString());
                fileAttachment.Attachment_Download_Times = Convert.ToInt32(row["Attachment_Download_Times"].ToString());
                fileAttachment.Upload_Time = Convert.ToDateTime(row["Upload_Time"].ToString());

                fileAttachmentList.Add(fileAttachment);
            }

            return fileAttachmentList;
        }

        /// <summary>
        /// 删除一个附件数据库信息
        /// </summary>
        /// <param name="fileID">文件编号</param>
        /// <param name="attachmentName">附件名称</param>
        /// <returns>受影响行数</returns>
        public int deleteFileAttachment(string fileID, string attachmentName) {
            StringBuilder strBui = new StringBuilder("delete File_Attachment_Info where ")
                .Append("File_ID = '").Append(fileID).Append("'and Attachment_Name = '")
                .Append(attachmentName).Append("'");

            return DBHelper.ExecuteNonQuery(strBui.ToString());
        }
    }
}
