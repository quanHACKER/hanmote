﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.SqlServerDAL
{
    public class SPR_SPCompareRadarCharDAL : SPR_BaseDAL,SPR_SPCompareRadarChartIDAL
    {

        /// <summary>
        /// 查询供应商列表
        /// 条件
        /// 采购组织ID、年度、时段
        /// </summary>
        /// <param name="selectSupplierConditionSettings"></param>
        /// <returns></returns>
        public DataTable getAllSupplierInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            //编写SQL语句文本
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT DISTINCT a.Supplier_ID,b.Supplier_Name,c.SupplierIndustry_Id,b.Address ");
            sqlText.Append("    FROM Supplier_Performance a INNER JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID INNER JOIN Supplier_Industry c ON b.Industry =c.SupplierIndustry_Id");
            sqlText.Append("    WHERE  a.PurchasingORG_ID=@PurchasingORG_ID AND a.Evaluation_Period=@Evaluation_Period ");
            
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = selectSupplierConditionSettings.PurchaseId;
            string evaluationPeriod = selectSupplierConditionSettings.Year + "年" +selectSupplierConditionSettings.Month;
            sqlParas[1].Value = evaluationPeriod;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 查询供应商评分结果
        /// </summary>
        /// <param name="conditionvalue"></param>
        /// <returns></returns>
        public DataTable getSupplierEvaluationScore(SPCompareRadarChartConditionValue conditionvalue)
        {
            //得到保存的标准键值对
            Dictionary<string, string> temp = conditionvalue.SaveSelectedStandardName;
            
            //进行SQL语句拼接
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("    SELECT ");
            //只遍历其键值
            string selecteStandard = "";
            foreach (string sStandard in temp.Keys)
            {
                selecteStandard += sStandard + ",";
            }
            sqlText.Append(selecteStandard.Substring(0,selecteStandard.Length - 1));   //去掉最后一个多余的","号

            //设置表名
            sqlText.Append("    FROM Supplier_Performance");
            //设置条件
            sqlText.Append("    WHERE PurchasingORG_ID='" + conditionvalue.SelectSupplierConditionSettings.PurchaseId + "'");
            string evaluationPeriod = conditionvalue.SelectSupplierConditionSettings.Year + "年" + conditionvalue.SelectSupplierConditionSettings.Month;
            sqlText.Append("    AND Evaluation_Period='" + evaluationPeriod + "'");
            sqlText.Append("    AND Supplier_ID IN (");
            //只取出选中的几个供应商
            string selectedSupplierId = "";
            temp = conditionvalue.SavaSelectedSupplierMap;
            foreach(string sSupplierId in temp.Keys)
            {
                selectedSupplierId += "'" + sSupplierId + "',";
            }
            selectedSupplierId = selectedSupplierId.Substring(0,selectedSupplierId.Length - 1) + ")";
            sqlText.Append(selectedSupplierId);

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

    }
}
