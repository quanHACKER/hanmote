﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class SPR_BaseDAL
    {
        /// <summary>
        /// 获取所有采购组织信息
        /// </summary>
        /// <returns></returns>
        public DataTable getAllBuyerOrgInfo()
        {
            string sqlText = @"SELECT Buyer_Org,Buyer_Org_Name FROM Buyer_Org";

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 根据采购组织编码查找该采购组织下的供应商名称
        /// </summary>
        /// <param name="purchaseOrgId">采购组织编码</param>
        /// <returns></returns>
        public DataTable getAllSupplierInfoByPurchaseOrgId(string purchaseOrgId)
        {
            //编写SQL语句文本
            string sqlText = @"SELECT Supplier_ID,Supplier_Name FROM Supplier_Purchasing_Org WHERE PurchasingORG_ID=@PurchasingORG_ID";
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = purchaseOrgId;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText,sqlParas);
        }


    }
}
