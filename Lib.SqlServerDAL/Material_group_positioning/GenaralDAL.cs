﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.Material_group_positioning;
using System.Data;
using Lib.Model.CertificationProcess;
using Lib.Common.CommonControls;
using Lib.Model.CertificationProcess.GN;
using System.Windows.Forms;

namespace Lib.SqlServerDAL.Material_group_positioning
{
    public class GeneralDAL : GeneralIDAL
    {
        DataTable GeneralIDAL.loaddata(string id)//加载和刷新数据
        {
            string sql = @"select 
                                    M.PurOrgID AS '采购组织编号',
                                    M.PurOrgName as '采购组织名称',
                                    M.id AS '物料组编号',
                                    M.MTGName as '物料组名称',
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性物料'";
            /* Material_Group AS MG 
             WHERE M.id=MG.Material_Group ";*/
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }
        DataTable GeneralIDAL.loaditemdata(string id)//加载和刷新数据
        {
            string sql = @"select 
                                    M.PurOrgID AS '采购组织编号',
                                    M.PurOrgName as '采购组织名称',
                                    M.id AS '项目编号',
                                    M.MTGName as '项目名称',
                                    M.Aims as '项目领域', 
                                    M.Goals as '项目目标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性资本性物料'";
            /* Material_Group AS MG 
             WHERE M.id=MG.Material_Group ";*/
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }
        DataTable GeneralIDAL.loadgeneralitemdata(string id)//加载和刷新数据
        {
            string sql = @"select 
                                    M.PurOrgID AS '采购组织编号',
                                    M.PurOrgName as '采购组织名称',
                                    M.id AS '项目编号',
                                    M.MTGName as '项目名称',
                                    M.Aims as '项目领域', 
                                    M.Goals as '项目目标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='维持公司运营物料'";
            /* Material_Group AS MG 
             WHERE M.id=MG.Material_Group ";*/
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }






        DataTable GeneralIDAL.searchdata(string id, string ORGID, string ORGName, string MGTID, string MGTName)//一般物料主界面搜索
        {
            string sql = @"select 
                                   
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    
                                    WHERE M.Category='生产性物料' ";
            if (ORGID != null && ORGID != "")
                sql += " and M.PurOrgID= '" + ORGID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.PurOrgName like '%" + ORGName + "%'";
            if (MGTName != null && MGTName != "")
                sql += " and M.id = '" + MGTID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.MTGName like '%" + MGTName + "%'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.searchitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName)//核心竞争力主界面搜索
        {
            string sql = @"select 
                                   
                                    M.Aims as '项目领域', 
                                    M.Goals as '项目目标' 
                                    from MTGAttributeDefinition as M
                                    
                                    WHERE M.Category='生产性资本性物料' ";
            if (ORGID != null && ORGID != "")
                sql += " and M.PurOrgID= '" + ORGID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.PurOrgName like '%" + ORGName + "%'";
            if (MGTName != null && MGTName != "")
                sql += " and M.id = '" + MGTID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.MTGName like '%" + MGTName + "%'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.searchgeneralitemdata(string id, string ORGID, string ORGName, string MGTID, string MGTName)//核心竞争力主界面搜索
        {
            string sql = @"select 
                                   
                                    M.Aims as '项目领域', 
                                    M.Goals as '项目目标' 
                                    from MTGAttributeDefinition as M
                                    
                                    WHERE M.Category='维持公司运营物料' ";
            if (ORGID != null && ORGID != "")
                sql += " and M.PurOrgID= '" + ORGID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.PurOrgName like '%" + ORGName + "%'";
            if (MGTName != null && MGTName != "")
                sql += " and M.id = '" + MGTID + "'";
            if (MGTName != null && MGTName != "")
                sql += " and M.MTGName like '%" + MGTName + "%'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }




        List<String> GeneralIDAL.getALLMtGroup()//获取所有物料组的名字
        {
            /*string sql = @"select Description
                                    
                                    from
                                    Material_Group 
                                   ";
            
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
            */
            List<String> MtGroup_ID = new List<string>();

            String sql = " select Description  from  Material_Group ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }
        /// <summary>
        /// 获取所有采购组织
        /// </summary>
        /// <returns></returns>
        List<String> GeneralIDAL.getALLorg()
        {
            
            List<String> MtGroup_ID = new List<string>();

            String sql = " select Buyer_Org  from  Buyer_Org ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }
        /// <summary>
        /// 获取属性模板
        /// </summary>
        /// <returns></returns>
        List<String> GeneralIDAL.getModel()
        {

            List<String> MtGroup_ID = new List<string>();

            String sql = " select Type  from  MTGroupAttributeModel ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }
        /// <summary>
        /// 获取所有采购组织的ID
        /// </summary>
        /// <returns></returns>
        List<String> GeneralIDAL.getALLorgID()
        {
            List<String> MtGroup_ID = new List<string>();

            String sql = " select distinct PurOrgID  from  MTGAttributeDefinition WHERE Category='生产性物料' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }
        List<String> GeneralIDAL.getALLitemorgID()
        {
            List<String> MtGroup_ID = new List<string>();

            String sql = " select distinct PurOrgID  from  MTGAttributeDefinition WHERE Category='生产性资本性物料' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }
        List<String> GeneralIDAL.getALLgeneralitemorgID()
        {
            List<String> MtGroup_ID = new List<string>();

            String sql = " select distinct PurOrgID  from  MTGAttributeDefinition WHERE Category='维持公司运营物料' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }

        void GeneralIDAL.SaveGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1)//添加数据
        {
            string sql = @"
                                IF NOT EXISTS(SELECT * FROM MTGAttributeDefinition WHERE Category='生产性物料' and Aims='" + G1 + "' and Goals= '"+ Z1 + "') BEGIN insert into   MTGAttributeDefinition (PurOrgID,PurOrgName,id,MTGName,Category,Aims,Goals)  values ('" + orgID + "','" + orgName + "','" + name_MtGroup + "','" + code_MtGroup + "','生产性物料','" + G1 + "','" + Z1 + "');  END";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.SaveitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1)//添加数据
        {
            string sql = @"
                                IF NOT EXISTS(SELECT * FROM MTGAttributeDefinition WHERE Category='生产性资本性物料' and Aims='" + G1 + "' and Goals= '" + Z1 + "') BEGIN insert into   MTGAttributeDefinition (PurOrgID,PurOrgName,id,MTGName,Category,Aims,Goals)  values ('" + orgID + "','" + orgName + "','" + name_MtGroup + "','" + code_MtGroup + "','生产性资本性物料','" + G1 + "','" + Z1 + "');  END";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.SavegeneralitemGoal(string orgID, string orgName, string name_MtGroup, string code_MtGroup, string G1, string Z1)//添加数据
        {
            string sql = @"
                                IF NOT EXISTS(SELECT * FROM MTGAttributeDefinition WHERE Category='维持公司运营物料' and Aims='" + G1 + "' and Goals= '" + Z1 + "') BEGIN insert into   MTGAttributeDefinition (PurOrgID,PurOrgName,id,MTGName,Category,Aims,Goals)  values ('" + orgID + "','" + orgName + "','" + name_MtGroup + "','" + code_MtGroup + "','维持公司运营物料','" + G1 + "','" + Z1 + "');  END";
            DBHelper.ExecuteQueryDT(sql);

        }


        /// <summary>
        /// 保存模板
        /// </summary>
        /// <param name="orgID"></param>
        /// <param name="orgName"></param>
        /// <param name="name_MtGroup"></param>
        /// <param name="code_MtGroup"></param>
        /// <param name="G1"></param>
        /// <param name="Z1"></param>
        void GeneralIDAL.SaveModel(string name, string area, string aim, string goal)//保存为模板
        {
            string sql = @"insert into 
                            MTGroupAttributeModel (Type,AreaModel,AimModel,GoalModel) 
                            values ('" + name + "','" + area + "','" + aim + "','" + goal + "'); ";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.UpdateMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)//修改数据
        {
            string sql = @"UPDATE MTGAttributeDefinition
        SET	Aims='" + G1 + "',Goals='" + Z1 + "'WHERE Category='生产性物料' and Aims='" + name_MtGroup + "' AND Goals='" + code_MtGroup + "' ";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.UpdateitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)//修改核心竞争力数据
        {
            string sql = @"UPDATE MTGAttributeDefinition
        SET	Aims='" + G1 + "',Goals='" + Z1 + "'WHERE Category='生产性资本性物料' and Aims='" + name_MtGroup + "' AND Goals='" + code_MtGroup + "' ";
            DBHelper.ExecuteQueryDT(sql);

        }
        void GeneralIDAL.UpdategeneralitemMtGroup(string name_MtGroup, string code_MtGroup, string G1, string Z1)//修改维持公司运营数据
        {
            string sql = @"UPDATE MTGAttributeDefinition
        SET	Aims='" + G1 + "',Goals='" + Z1 + "'WHERE Category='维持公司运营物料' and Aims='" + name_MtGroup + "' AND Goals='" + code_MtGroup + "' ";
            DBHelper.ExecuteQueryDT(sql);

        }




        void GeneralIDAL.deletedata(string b, string G1, string Z1)//删除数据
        {
            string sql = @"delete from MTGAttributeDefinition WHERE Category='生产性物料' and Aims='" + G1 + "' AND Goals='" + Z1 + "' AND Area='" + b + "'";
            DBHelper.ExecuteQueryDT(sql);
        }
        void GeneralIDAL.deleteitemdata(string G1, string Z1)//删除数据
        {
            string sql = @"delete from MTGAttributeDefinition WHERE Category='生产性资本性物料' and Aims='" + G1 + "' AND Goals='" + Z1 + "' ";
            DBHelper.ExecuteQueryDT(sql);
        }
        void GeneralIDAL.deletegeneralitemdata(string G1, string Z1)//删除数据
        {
            string sql = @"delete from MTGAttributeDefinition WHERE Category='维持公司运营物料' and Aims='" + G1 + "' AND Goals='" + Z1 + "' ";
            DBHelper.ExecuteQueryDT(sql);
        }
    }
}
