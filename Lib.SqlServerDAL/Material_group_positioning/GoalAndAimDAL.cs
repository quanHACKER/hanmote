﻿using Lib.IDAL.Material_group_positioning;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.Material_group_positioning
{
    public class GoalAndAimDAL : GoalAndAimIDAL
    {
        //获取组成产品的物料信息
        //text:产品编号
        public DataTable getMaterialInfo(string text)
        {
            string sqlText = @"select Material_ID,Material_Name from TabPrdAndMaterial pm,Material m where prdId='"+text+"'  and pm.materialId =m.Material_ID";
            return DBHelper.ExecuteQueryDT(sqlText);

        }

        //获取产品组编号
        public DataTable GetPrdId()
        {
            string sqlText = @"select prdId from TabPrdInfo";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        //获取产品组编号
        public DataTable GetPrdName(string text)
        {
            string sqlText = @"select prdName from TabPrdInfo where prdId='"+text+"'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public int insertGoalInfo(prdGoalModel prdGoalModel)
        {
            string sqlText = @"if EXISTS(select * from TabprdGoalAndPrd where goalId=@goalId)
	                    UPDATE TabprdGoalAndPrd set goalText=@goalText,goalSocre=@goalSocre,prdId=@prdId where  goalId=@goalId
                    else
		                insert into TabprdGoalAndPrd(goalId,goalText,goalSocre,prdId) values(@goalId,@goalText,@goalSocre,@prdId)";

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@goalId",prdGoalModel.GoalId),
                new SqlParameter("@goalText",prdGoalModel.GoalText),
                new SqlParameter("@goalSocre",prdGoalModel.GoalSocre),
                new SqlParameter("@prdId",prdGoalModel.PrdId),
            };
            return DBHelper.ExecuteNonQuery(sqlText,sqlParams);
        }
    }
}
