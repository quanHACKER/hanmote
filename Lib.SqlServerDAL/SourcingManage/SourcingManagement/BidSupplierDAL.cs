﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class BidSupplierDAL : BidSupplierIDAL
    {

        private string tableName = "dbo.BidSupplier";
        //添加BidSupplier
        public int addBidSupplier(BidSupplier bs)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                .Append("(bidId,supplierId,supplierName,contact,email,address,state) values(")
                .Append("@BidId,@SupplierId,@SupplierName,@Contact,@Email,@Address,@State)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@SupplierName",SqlDbType.VarChar),
                new SqlParameter("@Contact",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@Address",SqlDbType.VarChar),
                new SqlParameter("@State",SqlDbType.Int)
            };
            sqlParas[0].Value = bs.BidId;
            sqlParas[1].Value = bs.SupplierId;
            sqlParas[2].Value = bs.SupplierName;
            sqlParas[3].Value = bs.Contact;
            sqlParas[4].Value = bs.Email;
            sqlParas[5].Value = bs.Address;
            sqlParas[6].Value = bs.State;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }
        //获取合格供应商
        public List<BidSupplier> getBidSupplier()
        {
            string table = "dbo.Supplier_Classification";
            List<BidSupplier> List = new List<BidSupplier>();
            StringBuilder sb = new StringBuilder("select * from "+table);
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if(dt.Rows.Count>0){
                foreach(DataRow row in dt.Rows){
                    BidSupplier bs = new BidSupplier();
                    bs.SupplierId = row["Supplier_Id"].ToString();
                    bs.ClassificationResult = row["Classification_Result"].ToString();
                    StringBuilder tsb = new StringBuilder("select * from Supplier_Base where Supplier_ID='"+bs.SupplierId+"'");
                    DataTable tdt = DBHelper.ExecuteQueryDT(tsb.ToString());
                    DataRow trw = tdt.Rows[0];
                    bs.SupplierName = trw["Supplier_Name"].ToString();
                    bs.Contact = "";
                    bs.Email = trw["Email"].ToString();
                    bs.Address = trw["Address"].ToString();
                    bs.State = 0;
                    List.Add(bs);
                 }
            }
            return List;

        }


    }
}
