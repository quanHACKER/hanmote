﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    class PRSupplierDAL : PRSupplierIDAL
    {
        private string tableName = "dbo.PR_Supplier";

        public int addPRSupplier(PRSupplier prs)
        {
            StringBuilder sb = new StringBuilder("insert into "+tableName+" (PR_ID,Material_ID,Supplier_ID,Number,Finished_Mark) values(")
                .Append("@PR_ID,@Material_ID,@Supplier_ID,@Number,@Finished_Mark)");
            SqlParameter[] sqlParas = new SqlParameter[]
            {
                new SqlParameter("@PR_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Number",SqlDbType.Int),
                new SqlParameter("@Finished_Mark",SqlDbType.Bit)
            };
            sqlParas[0].Value = prs.PR_ID;
            sqlParas[1].Value = prs.Material_ID;
            sqlParas[2].Value = prs.Supplier_ID;
            sqlParas[3].Value = prs.Number;
            sqlParas[4].Value = prs.Finished_Mark;
            return DBHelper.ExecuteNonQuery(sb.ToString(),sqlParas);
        }
    }
}
