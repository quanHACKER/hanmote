﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.Model.SourcingManage;
using System.Text.RegularExpressions;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class Source_ListDAL
    {
        private string tableName = "";

        /// <summary>
        /// 构造函数，设为私有
        /// </summary>
        public Source_ListDAL()
        {
            tableName = "dbo.Source_List";
        }

        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

        /// <summary>
        /// 保存新的货源清单
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewSource_List(List<Source_List> sourceList)
        {

            LinkedList<string> sqlList = new LinkedList<string>();

            StringBuilder strBui = null;

            #region 删除旧的附件信息

            strBui = new StringBuilder("delete from ")
                .Append(tableName).Append(" where Material_ID = '")
                .Append(sourceList.ElementAt(0).Material_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 插入新的货源信息

            foreach (Source_List source in sourceList)
            {
                strBui = new StringBuilder("insert into ")
                    .Append(tableName + "([Material_ID],[Factory_ID],[Supplier_ID],[Effective_Date],[Expiration_Date],[Create_Date],[Department],[Buyer_Name],[Agreement_ID],[State],[Spare1],[Spare2],[Spare3])VALUES('")
                    .Append(source.Material_ID).Append("', '")
                    .Append(source.Factory_ID).Append("', '")
                    .Append(source.Supplier_ID).Append("', '")
                    .Append(source.Effective_Date.ToString()).Append("', '")
                    .Append(source.Expiration_Date.ToString()).Append("', '")
                    .Append(source.Create_Date.ToString()).Append("', '")
                    .Append(source.Department).Append("', '")
                    .Append(source.Buyer_Name).Append("', '")
                    .Append(source.Agreement_ID).Append("', '")
                    .Append(source.State).Append("', '")
                    .Append(source.Spare1).Append("', '")
                    .Append(source.Spare2).Append("', '")
                    .Append(source.Spare3).Append("') ");
                sqlList.AddLast(strBui.ToString());
            }
            
            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public Inquiry_Table findInquiryTable(Dictionary<string, string> conditions, String table)
        {
            Inquiry_Table result = new Inquiry_Table();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table).Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if(isfirst)
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("'");
                else
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
            }
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                result.Inquiry_ID = row["Inquiry_ID"].ToString();
                result.Inquiry_Name = row["Purchase_ID"].ToString();
                result.Transaction_Type = row["Department"].ToString();
                result.Buyer_Name = row["Buyer_Name"].ToString();
                result.Inquiry_Time = DateTime.Parse(row["Inquiry_Time"].ToString());
                result.Offer_Time = DateTime.Parse(row["Offer_Time"].ToString());
                result.Create_Time = DateTime.Parse(row["Create_Time"].ToString());
                result.State = row["State"].ToString();
                result.Inquiry_Path = row["Inquiry_Path"].ToString();
                result.Example3 = row["Example3"].ToString();

                return result;
            }
            else
                return null;
        }

        /// <summary>
        /// 根据询价单号查询已存在的询价单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Source_List> findSource(Dictionary<string, string> conditions)
        {
            List<Source_List> result = new List<Source_List>();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            if (conditions.Count!=0)
                stringBuilder.Append(tableName).Append(" where ");
            else
                stringBuilder.Append(tableName);
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if (isfirst && key.Length>1)
                {
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("' ");
                    isfirst = false;
                }
                else if (isfirst && key.Length <= 1)
                {
                    stringBuilder.Append(value);
                    isfirst = false;
                }
                else if (key.Length > 1)
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
                else 
                    stringBuilder.Append(" and ").Append(value);
            }
            stringBuilder.Append(" order by Create_Date desc");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Source_List source = new Source_List();
                    source.Material_ID = row["Material_ID"].ToString();
                    source.Factory_ID = row["Factory_ID"].ToString();
                    source.Supplier_ID = row["Supplier_ID"].ToString();
                    source.Effective_Date = DateTime.Parse(row["Effective_Date"].ToString());
                    source.Expiration_Date = DateTime.Parse(row["Expiration_Date"].ToString());
                    source.Create_Date = DateTime.Parse(row["Create_Date"].ToString());
                    source.Department = row["Department"].ToString();
                    source.Buyer_Name = row["Buyer_Name"].ToString();
                    source.Agreement_ID = row["Agreement_ID"].ToString();
                    source.State = row["State"].ToString();
                    source.Spare1 = row["Spare1"].ToString();
                    source.Spare2 = row["Spare2"].ToString();
                    source.Spare3 = row["Spare3"].ToString();

                    result.Add(source);
                }
                return result;
            }
            else
                return result;
        }

        /// <summary>
        /// 更改报价信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateEnquiryOffer(List<Enquiry> enquiryList, Inquiry_Table inquiry)
        {

            LinkedList<string> sqlList = new LinkedList<string>();
            #region 更新询价单表头状态信息

            StringBuilder strBui = new StringBuilder("update Inquiry_Table set State = '")
                .Append(inquiry.State).Append("' ")
                .Append("where Inquiry_ID = '").Append(inquiry.Inquiry_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 更改报价信息

            foreach (Enquiry enquiry in enquiryList)
            {
                strBui = new StringBuilder("update Enquiry set Price = '")
                    .Append(enquiry.Price).Append("', Delivery_Time ='")
                    .Append(DBNull.Value).Append("', Clause_ID='")
                    .Append(enquiry.Clause_ID).Append("', Condition_ID='")
                    .Append(enquiry.Condition_ID).Append("', State='")
                    .Append(enquiry.State).Append("', Update_Time='")
                    .Append(enquiry.Update_Time.ToString()).Append("' where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and Supplier_ID='")
                    .Append(enquiry.Supplier_ID).Append("'and Material_ID='")
                    .Append(enquiry.Material_ID).Append("'");
                sqlList.AddLast(strBui.ToString());
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 查询报价单某状态数量
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int enquiryCount(Enquiry enquiry)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from Enquiry where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and State ='")
                    .Append("待报价'");
            DataTable dt = DBHelper.ExecuteQueryDT(stringBuilder.ToString());

            return dt.Rows.Count;
        }
    }
}
