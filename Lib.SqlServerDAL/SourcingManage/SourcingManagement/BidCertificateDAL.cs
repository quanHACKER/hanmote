﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
   public  class BidCertificateDAL : BidCertificateIDAL
    {
       private string tableName = "dbo.BidCertificate";

       //添加招标凭证
       public int addBidCertificate(BidCertificate bc)
       {
           StringBuilder sb = new StringBuilder("insert into " + tableName)
               .Append("(bidId,shortBidText,approvalComment,longBidText) values(")
               .Append("@BidId,@ShortBidText,@ApprovalComment,@LongBidText)");
           SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@ShortBidText",SqlDbType.VarChar),
                new SqlParameter("@ApprovalComment",SqlDbType.VarChar),
                new SqlParameter("@LongBidText",SqlDbType.VarChar)
           };
           sqlParas[0].Value = bc.BidId;
           sqlParas[1].Value = bc.ShortBidText;
           sqlParas[2].Value = bc.ApprovalComment;
           sqlParas[3].Value = bc.LongBidText;
           string sqlText = sb.ToString();
           return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
       }

        public BidCertificate getBidCertificateByBidId(string bidId)
        {
            BidCertificate bidCer = new BidCertificate();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where  bidId='"+bidId+"'" );
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    
                    bidCer.ShortBidText = dr["shortBidText"].ToString();
                    bidCer.ApprovalComment = dr["approvalComment"].ToString();
                    bidCer.LongBidText = dr["longBidText"].ToString();
                }
            }
            return bidCer;
        }
    }
}
