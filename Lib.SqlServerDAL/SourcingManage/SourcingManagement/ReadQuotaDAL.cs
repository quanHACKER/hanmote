﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using System.Data;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class ReadQuotaDAL : ReadQuotaIDAL
    {
        public string tableName = "dbo.Quota_Arrangement";

        public List<Quota_Arrangement_Item> getInfoByMaterialId(string mid)
        {
            List<Quota_Arrangement_Item> list = new List<Quota_Arrangement_Item>();
            StringBuilder sb = new StringBuilder("select top 1 * from " + tableName + " where Material_ID='" + mid + "' and Status !='失效' order by End_Time desc");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt!=null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                Quota_Arrangement qa = new Quota_Arrangement();
                qa.Quota_Arrangement_ID = row["Quota_Arrangement_ID"].ToString();
                StringBuilder str = new StringBuilder("select * from Quota_Arrangement_Item where Quota_Arrangement_ID='" + qa.Quota_Arrangement_ID + "'");
                DataTable dtemp = DBHelper.ExecuteQueryDT(str.ToString());
                if (dtemp != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtemp.Rows)
                    {
                        Quota_Arrangement_Item qai = new Quota_Arrangement_Item();
                        qai.Supplier_ID = dr["Supplier_ID"].ToString();
                        qai.Quota_Percentage = double.Parse(dr["Quota_Percentage"].ToString());
                        list.Add(qai);
                    }
                }
                    
            }
            return list;
        }
    }
}
