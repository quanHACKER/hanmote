﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceSupplierDAL:SourceSupplierIDAL
    {
        private string tableName = "dbo.Source_Supplier";

        /// <summary>
        /// 添加寻源对应的供应商信息
        /// </summary>
        /// <param name="ss"></param>
        /// <returns></returns>
        public int addSourceSupplier(SourceSupplier ss)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + "(")
                   .Append(" Source_ID,Supplier_ID,Supplier_Name,Contact,Email,Address,State,IsTrans) values(")
                   .Append("@Source_ID,@Supplier_ID,@Supplier_Name,@Contact,@Email,@Address,@State,@IsTrans)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Source_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_Name",SqlDbType.VarChar),
                new SqlParameter("@Contact",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@Address",SqlDbType.VarChar),
                new SqlParameter("@State",SqlDbType.Int),
                new SqlParameter("@IsTrans",SqlDbType.Int)
            };
            sqlParas[0].Value = ss.Source_ID;
            sqlParas[1].Value = ss.Supplier_ID;
            sqlParas[2].Value = ss.Supplier_Name;
            sqlParas[3].Value = ss.Contact;
            sqlParas[4].Value = ss.Email;
            sqlParas[5].Value = ss.Address;
            sqlParas[6].Value = ss.State;
            sqlParas[7].Value = ss.IsTrans;
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText,sqlParas);
            return result;
        }

        /// <summary>
        /// 根据寻源单号查找对应的供应商信息
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public List<SourceSupplier> findSuppliersBySId(string sourceId)
        {
            List<SourceSupplier> ssList = new List<SourceSupplier>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + sourceId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceSupplier ss = new SourceSupplier();
                    ss.Supplier_ID = dr["Supplier_ID"].ToString();
                    ssList.Add(ss);
                }
            }
            return ssList;
        }


        //获取合格供应商
        public List<SourceSupplier> getBidSupplier()
        {
            string table = "dbo.Supplier_Classification";
            List<SourceSupplier> List = new List<SourceSupplier>();
            StringBuilder sb = new StringBuilder("select * from " + table + " sc1 where Create_Time=(select max(Create_Time) from " + table + " sc2 where sc1.Supplier_ID=sc2.Supplier_ID)");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceSupplier ss = new SourceSupplier();
                    ss.Supplier_ID = row["Supplier_Id"].ToString();
                    ss.ClassificationResult = row["Classification_Result"].ToString();
                    StringBuilder tsb = new StringBuilder("select * from Supplier_Base where Supplier_ID='" + ss.Supplier_ID + "'");
                    DataTable tdt = DBHelper.ExecuteQueryDT(tsb.ToString());
                    DataRow trw = tdt.Rows[0];
                    ss.Supplier_Name = trw["Supplier_Name"].ToString();
                    ss.Contact = "";
                    ss.Email = trw["Email"].ToString();
                    ss.Address = trw["Address"].ToString();
                    ss.State = 0;
                    List.Add(ss);
                }
            }
            return List;

        }

        public List<SourceSupplier> getBidSuppliersByBidId(string bidId)
        {
            List<SourceSupplier> list = new List<SourceSupplier>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID = '" + bidId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceSupplier bs = new SourceSupplier();
                    bs.Supplier_ID = row["Supplier_ID"].ToString();
                    bs.Supplier_Name = row["Supplier_Name"].ToString();
                    bs.Contact = "";
                    bs.Email = row["Email"].ToString();
                    bs.Address = row["Address"].ToString();
                    bs.State = Convert.ToInt32(row["State"].ToString());
                    list.Add(bs);
                }
            }
            return list;
        }

        public List<SourceSupplier> getBidsBySupplierId(string sid)
        {
            List<SourceSupplier> list = new List<SourceSupplier>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Supplier_ID = '" + sid + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceSupplier bs = new SourceSupplier();
                    bs.Source_ID = row["Source_ID"].ToString();
                    bs.State = int.Parse(row["State"].ToString());
                    list.Add(bs);
                }
            }
            return list;
        }

        public int updateTransState(string bid,string sid,int res)
        {
            StringBuilder sb = new StringBuilder("update " + tableName + " set IsTrans="+res+" where Source_ID='"+bid+"' and Supplier_ID='"+sid+"'");
            return DBHelper.ExecuteNonQuery(sb.ToString());
        }

        /// <summary>
        /// 获取正在谈判的供应商
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public SourceSupplier getSupplierByBidState(string bidId)
        {
            SourceSupplier ss = null;
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + bidId + "' and IsTrans=2");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count == 1)
            {
                ss = new SourceSupplier();
                ss.Supplier_ID = dt.Rows[0]["Supplier_ID"].ToString();
            }
            return ss;
        }

        public List<SourceSupplier> getAvailableSuppliersByBId(string bidId)
        {
            List<SourceSupplier> list = new List<SourceSupplier>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Source_ID='"+bidId+"' and IsTrans=1");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceSupplier ss = new SourceSupplier();
                    ss.Supplier_ID = dr["Supplier_ID"].ToString();
                    list.Add(ss);
                }
            }
            
            return list;
        }

        public string countTaskNumberByUserId(string user_ID)
        {
            string sql = @"SELECT
	                            COUNT (*)
                            FROM
	                            Hanmote_TaskSpecification
                            WHERE
	                            UserID ='"+user_ID+"' and  isFinished =0";
            return DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
        }
    }
}
