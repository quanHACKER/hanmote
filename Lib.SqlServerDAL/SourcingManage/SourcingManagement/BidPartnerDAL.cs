﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class BidPartnerDAL : BidPartnerIDAL
    {
        private string tableName = "dbo.BidPartner";
        //添加BidPartner
        public int addBidPartner(BidPartner bp)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                .Append("(bidId,requesterId,requesterName,receiverId,receiverName,dropPointId,dropPointName,reviewerId,reviewerName,callerId,callerName) values(")
                .Append("@BidId,@RequesterId,@RequesterName,@ReceiverId,@ReceiverName,@DropPointId,@DropPointName,@ReviewerId,@ReviewerName,@CallerId,@CallerName)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@RequesterId",SqlDbType.VarChar),
                new SqlParameter("@RequesterName",SqlDbType.VarChar),
                new SqlParameter("@ReceiverId",SqlDbType.VarChar),
                new SqlParameter("@ReceiverName",SqlDbType.VarChar),
                new SqlParameter("@DropPointId",SqlDbType.VarChar),
                new SqlParameter("@DropPointName",SqlDbType.VarChar),
                new SqlParameter("@ReviewerId",SqlDbType.VarChar),
                new SqlParameter("@ReviewerName",SqlDbType.VarChar),
                new SqlParameter("@CallerId",SqlDbType.VarChar),
                new SqlParameter("@CallerName",SqlDbType.VarChar)
            };
            sqlParas[0].Value = bp.BidId;
            sqlParas[1].Value = bp.RequesterId;
            sqlParas[2].Value = bp.RequesterName;
            sqlParas[3].Value = bp.ReceiverId;
            sqlParas[4].Value = bp.ReceiverName;
            sqlParas[5].Value = bp.DropPointId;
            sqlParas[6].Value = bp.DropPointName;
            sqlParas[7].Value = bp.ReviewerId;
            sqlParas[8].Value = bp.ReviewerName;
            sqlParas[9].Value = bp.CallerId;
            sqlParas[10].Value = bp.CallerName;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public BidPartner findBidPartnerByBidId(string bidId)
        {
            BidPartner bp = new BidPartner();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where bidId = '" + bidId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0]; 
                bp.RequesterId  = dr["requesterId"].ToString();
                bp.RequesterName = dr["requesterName"].ToString();
                bp.ReceiverId = dr["receiverId"].ToString();
                bp.ReceiverName = dr["receiverName"].ToString();
                bp.DropPointId = dr["dropPointId"].ToString();
                bp.DropPointName = dr["dropPointName"].ToString();
                bp.ReviewerId = dr["reviewerId"].ToString();
                bp.ReviewerName = dr["reviewerName"].ToString();
                bp.CallerId = dr["callerId"].ToString();
                bp.CallerName = dr["callerName"].ToString();
            }
            return bp;
        }
    }
}
