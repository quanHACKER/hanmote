﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class BidMaterialDAL : BidMaterialIDAL
    {
        private string tableName = "dbo.BidMaterial";
        //添加项目数据
        public  int addBidMaterial(BidMaterial bm)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                .Append("(bidId,materialId,materialName,number,unit) values(")
                .Append("@BidId,@MaterialId,@MaterialName,@Number,@Unit)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@MaterialId",SqlDbType.VarChar),
                new SqlParameter("@MaterialName",SqlDbType.VarChar),
                new SqlParameter("@Number",SqlDbType.Int),
                new SqlParameter("@Unit",SqlDbType.VarChar)
            };
            sqlParas[0].Value = bm.BidId;
            sqlParas[1].Value = bm.MaterialId;
            sqlParas[2].Value = bm.MaterialName;
            sqlParas[3].Value = bm.Number;
            sqlParas[4].Value = bm.Unit;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }
    }
}
