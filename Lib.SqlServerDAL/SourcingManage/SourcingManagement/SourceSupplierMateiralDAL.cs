﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceSupplierMaterialDAL : SourceSupplierMaterialIDAL
    {
        private string tableName = "dbo.Source_Supplier_Material";

        public int addSupplierMaterial(SourceSupplierMaterial ssm)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + "(")
                    .Append(" Source_ID,Supplier_ID,Material_ID,Price) values(")
                    .Append("'" + ssm.Source_ID + "',")
                    .Append("'" + ssm.Supplier_ID + "',")
                    .Append("'" + ssm.Material_ID + "',")
                    .Append(ssm.Price + ")");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        public List<SourceSupplierMaterial> findSSMBySId(string sourceId)
        {
            List<SourceSupplierMaterial> ssmList = new List<SourceSupplierMaterial>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + sourceId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceSupplierMaterial ssm = new SourceSupplierMaterial();
                    ssm.Supplier_ID = dr["Supplier_ID"].ToString();
                    ssm.Material_ID = dr["Material_ID"].ToString();
                    ssm.Price = float.Parse(dr["Price"].ToString());
                    ssmList.Add(ssm);
                }
            }
            return ssmList;
        }

        public List<SourceSupplierMaterial> getSSMBySuIdMId(string bidId, string materialId)
        {
            List<SourceSupplierMaterial> ssmList = new List<SourceSupplierMaterial>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + bidId + "' and Material_ID='"+materialId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceSupplierMaterial ssm = new SourceSupplierMaterial();
                    ssm.Supplier_ID = dr["Supplier_ID"].ToString();
                    ssm.Price = float.Parse(dr["Price"].ToString());
                    ssmList.Add(ssm);
                }
            }
            return ssmList;
        }

        public List<SourceSupplierMaterial> getSSMBySIdSuId(string sourceId, string supplierId)
        {
            List<SourceSupplierMaterial> ssmList = new List<SourceSupplierMaterial>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + sourceId + "' and Supplier_ID='" + supplierId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceSupplierMaterial ssm = new SourceSupplierMaterial();
                    ssm.Supplier_ID = dr["Supplier_ID"].ToString();
                    ssm.Material_ID = dr["Material_ID"].ToString();
                    ssm.Price = float.Parse(dr["Price"].ToString());
                    ssmList.Add(ssm);
                }
            }
            return ssmList;
        }

        public SourceSupplierMaterial getSSMBySIdSIdMId(string bidId, string supplierId, string materialId)
        {
            SourceSupplierMaterial ssmt = new SourceSupplierMaterial();
            StringBuilder sb = new StringBuilder("select * from " +tableName+ " where Source_ID='" + bidId + "' and Supplier_ID='" + supplierId + "' and Material_ID='"+materialId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ssmt.Price = float.Parse(dr["Price"].ToString());
                }
            }
            return ssmt;
        }

    }
}
