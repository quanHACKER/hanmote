﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceTimeSectionDAL:SourceTimeSectionIDAL
    {
        private string tableName = "dbo.SourceTimeSection";

        public List<SourceTimeSection> getSouTmsBySouSupMaId(string sourceId, string supplierId, string materialId)
        {
            List<SourceTimeSection> list = new List<SourceTimeSection>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where SourceId='"+sourceId+"' and SupplierId='"+supplierId+"' and MaterialId='"+materialId+"'" );
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceTimeSection sts = new SourceTimeSection();
                    sts.StartTime = DateTime.Parse(dr["StartTime"].ToString());
                    sts.EndTime = DateTime.Parse(dr["EndTime"].ToString());
                    sts.Num = float.Parse(dr["Num"].ToString());
                    list.Add(sts);
                }
            }
            return list;
        }
    }
}
