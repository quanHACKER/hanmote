﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.Model.SourcingManage;
using System.Text.RegularExpressions;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceListDAL:SourceListIDAL
    {
        private string tableName = "dbo.SourceList";

        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

        //添加货源清单
        public int addSourceList(SourceList sl)
        {
            StringBuilder sb = new StringBuilder("insert into "+tableName+"(")
                .Append("materialId,factoryId,supplierId,createTime,startTime,endTime,fix,blk,mrp,ppl,sourceListId) values(")
                .Append("@MaterialId,@FactoryId,@SupplierId,@CreateTime,@StartTime,@EndTime,@Fix,@Blk,@Mrp,@Ppl,@SourceListId)");
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@MaterialId",SqlDbType.VarChar),
                new SqlParameter("@FactoryId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@CreateTime",SqlDbType.DateTime),
                new SqlParameter("@StartTime",SqlDbType.DateTime),
                new SqlParameter("@EndTime",SqlDbType.DateTime),
                new SqlParameter("@Fix",SqlDbType.Int),
                new SqlParameter("@Blk",SqlDbType.Int),
                new SqlParameter("@Mrp",SqlDbType.Int),
                new SqlParameter("@Ppl",SqlDbType.VarChar),
                new SqlParameter("@SourceListId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = sl.Material_ID;
            sqlParas[1].Value = sl.Factory_ID;
            sqlParas[2].Value = sl.Supplier_ID;
            sqlParas[3].Value = DateTime.Now;
            sqlParas[4].Value = sl.StartTime;
            sqlParas[5].Value = sl.EndTime;
            sqlParas[6].Value = sl.Fix;
            sqlParas[7].Value = sl.Blk;
            sqlParas[8].Value = sl.MRP;
            sqlParas[9].Value = sl.PPL;
            sqlParas[10].Value = sl.SourceListId;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        //根据物料编号获取货源清单
        public List<SourceList> getSourceList(string materialId,string factoryId)
        {
            List<SourceList> list = new List<SourceList>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where materialId='" + materialId + "' and factoryId='" + factoryId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceList sl = new SourceList();
                    sl.SourceListId = row["sourceListId"].ToString();
                    sl.Supplier_ID = row["supplierId"].ToString();
                    sl.StartTime = DateTime.Parse(row["startTime"].ToString());
                    sl.EndTime = DateTime.Parse(row["endTime"].ToString());
                    sl.CreateTime = DateTime.Parse(row["createTime"].ToString());
                    sl.Fix = Convert.ToInt32(row["fix"].ToString());
                    sl.Blk = Convert.ToInt32(row["blk"].ToString());
                    sl.MRP = Convert.ToInt32(row["mrp"].ToString());
                    list.Add(sl);
                }
            }
            return list;
        }

        /// <summary>
        /// 该方法任有BUG
        /// </summary>
        /// <param name="materialId"></param>
        /// <param name="factoryId"></param>
        /// <param name="StartTime"></param>
        /// <param name="EndTime"></param>
        /// <returns></returns>
        public List<SourceList> getSourceList(string materialId, string factoryId,string StartTime,string EndTime)
        {
            List<SourceList> list = new List<SourceList>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where materialId='" + materialId + "' and factoryId='"+factoryId+"' and ("+StartTime+" between and");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceList sl = new SourceList();
                    sl.SourceListId = dr["sourceListId"].ToString();
                    sl.Supplier_ID = dr["supplierId"].ToString();
                    sl.StartTime = DateTime.Parse(dr["startTime"].ToString());
                    sl.EndTime = DateTime.Parse(dr["endTime"].ToString());
                    sl.PurchaseOrg = dr["purchaseOrg"].ToString();
                    sl.Fix = int.Parse(dr["fix"].ToString());
                    sl.Blk = int.Parse(dr["blk"].ToString());
                    sl.MRP = int.Parse(dr["mrp"].ToString());
                    sl.PPL = dr["ppl"].ToString();
                    list.Add(sl);
                }
            }
            return list;
        }

        /// <summary>
        /// 更新货源清单
        /// </summary>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        public int updateSourceList(SourceList sourceList)
        {
            string startTime = sourceList.StartTime.ToString("yyyy-MM-dd hh:mm:ss");
            string endTime = sourceList.EndTime.ToString("yyyy-MM-dd hh:mm:ss");
            StringBuilder sb = new StringBuilder("update " + tableName + " set ")
                .Append(" ppl='" + sourceList.PPL + "',")
                .Append(" startTime='" + startTime + "',")
                .Append(" endTime='" + endTime + "', ")
                .Append(" Fix=" + sourceList.Fix + " ,")
                .Append(" Blk=" + sourceList.Blk + " ,")
                .Append(" MRP=" + sourceList.MRP + " ")
                .Append( " where sourceListId='"+sourceList.SourceListId+"'");
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText);
        }

        /// <summary>
        /// 根据货源清单编号查找货源清单
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public SourceList getSourceListBySId(string sid)
        {
            SourceList sourceList = new SourceList();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where sourceListId='"+sid+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sourceList.SourceListId = dr["sourceListId"].ToString();
                    sourceList.Material_ID = dr["materialId"].ToString();
                    sourceList.Factory_ID = dr["factoryId"].ToString();
                    sourceList.Supplier_ID = dr["supplierId"].ToString();
                    sourceList.StartTime = DateTime.Parse(dr["startTime"].ToString());
                    sourceList.EndTime = DateTime.Parse(dr["endTime"].ToString());
                    sourceList.PurchaseOrg = dr["purchaseOrg"].ToString();
                    sourceList.Fix = int.Parse(dr["fix"].ToString());
                    sourceList.Blk = int.Parse(dr["blk"].ToString());
                    sourceList.MRP = int.Parse(dr["mrp"].ToString());
                    sourceList.PPL = dr["ppl"].ToString();

                }
            }
            return sourceList;

        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public SourceInquiry findInquiryTable(Dictionary<string, string> conditions, String table)
        {
            SourceInquiry result = new SourceInquiry();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table).Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if(isfirst)
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("'");
                else
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
            }
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                result.Source_ID = row["Inquiry_ID"].ToString();

                result.Transaction_Type = row["Department"].ToString();
                result.Offer_Time = DateTime.Parse(row["Offer_Time"].ToString());
                result.State = int.Parse(row["State"].ToString());
                result.Inquiry_Path = row["Inquiry_Path"].ToString();

                return result;
            }
            else
                return null;
        }

        
        /// <summary>
        /// 更改报价信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateEnquiryOffer(List<Model.SourcingManage.Enquiry> enquiryList, SourceInquiry inquiry)
        {

            LinkedList<string> sqlList = new LinkedList<string>();
            #region 更新询价单表头状态信息

            StringBuilder strBui = new StringBuilder("update Inquiry_Table set State = '")
                .Append(inquiry.State).Append("' ")
                .Append("where Inquiry_ID = '").Append(inquiry.Source_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 更改报价信息

            foreach (Model.SourcingManage.Enquiry enquiry in enquiryList)
            {
                strBui = new StringBuilder("update Enquiry set Price = '")
                    .Append(enquiry.Price).Append("', Delivery_Time ='")
                    .Append(DBNull.Value).Append("', Clause_ID='")
                    .Append(enquiry.Clause_ID).Append("', Condition_ID='")
                    .Append(enquiry.Condition_ID).Append("', State='")
                    .Append(enquiry.State).Append("', Update_Time='")
                    .Append(enquiry.Update_Time.ToString()).Append("' where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and Supplier_ID='")
                    .Append(enquiry.Supplier_ID).Append("'and Material_ID='")
                    .Append(enquiry.Material_ID).Append("'");
                sqlList.AddLast(strBui.ToString());
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 查询报价单某状态数量
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int enquiryCount(Model.SourcingManage.Enquiry enquiry)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from Enquiry where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and State ='")
                    .Append("待报价'");
            DataTable dt = DBHelper.ExecuteQueryDT(stringBuilder.ToString());

            return dt.Rows.Count;
        }
    }
}
