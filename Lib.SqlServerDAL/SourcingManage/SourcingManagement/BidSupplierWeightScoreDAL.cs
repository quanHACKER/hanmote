﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    class BidSupplierWeightScoreDAL : BidSupplierWeightScoreIDAL
    {
        private string tableName = "dbo.BidSupplierWeightScore";

        public int addBidSupplierWeightScore(BidSupplierWeightScore bsws)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                .Append(" (bidId,supplierId,weightName,score,Cause) values(")
                .Append("@BidId,@SupplierId,@WeightName,@Score,@Cause)");
            SqlParameter[] sqlParas = new SqlParameter[] 
            {
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@WeightName",SqlDbType.VarChar),
                new SqlParameter("@Score",SqlDbType.Int),
                new SqlParameter("@Cause",SqlDbType.VarChar)
            };
            sqlParas[0].Value = bsws.BidId;
            sqlParas[1].Value = bsws.SupplierId;
            sqlParas[2].Value = bsws.WeightName;
            sqlParas[3].Value = bsws.Score;
            sqlParas[4].Value = bsws.Cause;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public int getResultByBidSIdWeightName(string bid, string sid,string wn)
        {
            int result = 0;
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where bidId='"+bid+"' and supplierId='"+sid+"' and weightName='"+wn+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                result = Convert.ToInt32(dr["score"].ToString());
            }
            return result;
        }

        public List<BidSupplierWeightScore> getBidSWSByBIdSId(string bidId, string supplierId)
        {
            List<BidSupplierWeightScore> list = new List<BidSupplierWeightScore>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where bidId='" + bidId + "' and supplierId='"+supplierId+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    BidSupplierWeightScore bsws = new BidSupplierWeightScore();
                    bsws.WeightName = dr["weightName"].ToString();
                    bsws.SupplierAnswer = dr["supplierAnswer"].ToString();
                    bsws.WeightNum = 1;
                    bsws.Score = int.Parse(dr["score"].ToString());
                    list.Add(bsws);
                }
            }
            return list;

        }

        public BidSupplierWeightScore getBSWSByBIdSIdWN(string bidId, string supplierId, string wn)
        {
            BidSupplierWeightScore bsws = new BidSupplierWeightScore();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where bidId='"+bidId+"' and supplierId='"+supplierId+"' and weightName='"+wn+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                bsws.SupplierAnswer = dr["supplierAnswer"].ToString();
            }
            return bsws;
        }
    }
}
