﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceBidDAL : SourceBidIDAL
    {
        private string tableName = "dbo.Source_Bid";
        //添加招标
        public int addBid(SourceBid bid)
        {
            StringBuilder sb = new StringBuilder(" insert into " + tableName)
                .Append("(bidId,serviceType,catalogue,displayType,transType,startTime, endTime,startBeginTime,limitTime,timeZone,currency,commBidStartTime,commBidStartDate,techBidStartTime,techBidStartDate,buyEndTime,buyEndDate,enrollEndTime,enrollEndDate,enrollStartTime,enrollStartDate,bidCost,bidState,evalMethId,isShow) values(")
                .Append("@BidId,@ServiceType,@Catalogue,@DisplayType,@TransType,@StartTime,@EndTime,@StartBeginTime,@LimitTime,@TimeZone,@Currency,@CommBidStartTime,@CommBidStartDate,@TechBidStartTime,@TechBidStartDate,@BuyEndTime,@BuyEndDate,@EnrollEndTime,@EnrollEndDate,@EnrollStartTime,@EnrollStartDate,@BidCost,@BidState,@EvalMethId,@IsShow)");
            SqlParameter[] sqlParas = new SqlParameter []{ 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@ServiceType",SqlDbType.VarChar),
                new SqlParameter("@Catalogue",SqlDbType.VarChar),
                new SqlParameter("@DisplayType",SqlDbType.VarChar),
                new SqlParameter("@TransType",SqlDbType.VarChar),
                new SqlParameter("@StartTime",SqlDbType.DateTime),
                new SqlParameter("@EndTime",SqlDbType.DateTime),
                new SqlParameter("@StartBeginTime",SqlDbType.DateTime),
                new SqlParameter("@LimitTime",SqlDbType.DateTime),
                new SqlParameter("@TimeZone",SqlDbType.VarChar),
                new SqlParameter("@Currency",SqlDbType.VarChar),
                new SqlParameter("@CommBidStartTime",SqlDbType.DateTime),
                new SqlParameter("@CommBidStartDate",SqlDbType.DateTime),
                new SqlParameter("@TechBidStartTime",SqlDbType.DateTime),
                new SqlParameter("@TechBidStartDate",SqlDbType.DateTime),
                new SqlParameter("@BuyEndTime",SqlDbType.DateTime),
                new SqlParameter("@BuyEndDate",SqlDbType.DateTime),
                new SqlParameter("@EnrollEndTime",SqlDbType.DateTime),
                new SqlParameter("@EnrollEndDate",SqlDbType.DateTime),
                new SqlParameter("@EnrollStartTime",SqlDbType.DateTime),
                new SqlParameter("@EnrollStartDate",SqlDbType.DateTime),
                new SqlParameter("@BidCost",SqlDbType.Float),
                new SqlParameter("@BidState",SqlDbType.Int),
                new SqlParameter("@EvalMethId",SqlDbType.Int),
                new SqlParameter("@IsShow",SqlDbType.Int)
            };

            sqlParas[0].Value = bid.BidId;
            sqlParas[1].Value = bid.ServiceType;
            sqlParas[2].Value = bid.Catalogue;
            sqlParas[3].Value = bid.DisplayType;
            sqlParas[4].Value = bid.TransType;
            sqlParas[5].Value = bid.StartTime;
            sqlParas[6].Value = bid.EndTime;
            sqlParas[7].Value = bid.StartBeginTime;
            sqlParas[8].Value = bid.LimitTime;
            sqlParas[9].Value = bid.TimeZone;
            sqlParas[10].Value = bid.Currency;
            sqlParas[11].Value = bid.CommBidStartTime;
            sqlParas[12].Value = bid.CommBidStartDate;
            sqlParas[13].Value = bid.TechBidStartTime;
            sqlParas[14].Value = bid.TechBidStartDate;
            sqlParas[15].Value = bid.BuyEndTime;
            sqlParas[16].Value = bid.BuyEndDate;
            sqlParas[17].Value = bid.EnrollEndTime;
            sqlParas[18].Value = bid.EnrollEndDate;
            sqlParas[19].Value = bid.EnrollStartTime;
            sqlParas[20].Value = bid.EnrollStartDate;
            sqlParas[21].Value = bid.BidCost;
            sqlParas[22].Value = bid.BidState;
            sqlParas[23].Value = bid.EvalMethId;
            //sqlParas[24].Value = bid.IsShow;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public List<SourceBid> getBids()
        {
            List<SourceBid> result = new List<SourceBid>();
            StringBuilder sb = new StringBuilder("select * from " + tableName);
            string sqlText = sb.ToString();
            DataTable dt =  DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceBid bid = new SourceBid();
                    bid.BidId = row["bidId"].ToString();
                    bid.BidState = Convert.ToInt32(row["bidState"].ToString());
                    result.Add(bid); 
                }
            }
            return result;
        }

        public SourceBid findBidByBidId(string bidId)
        {
            SourceBid bid = new SourceBid();
            StringBuilder sb = new StringBuilder("select * from " + tableName)
                .Append(" where bidId = '" + bidId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                bid.BidId = bidId;
                bid.ServiceType = dr["ServiceType"].ToString();
                bid.Catalogue = dr["catalogue"].ToString();
                bid.DisplayType = dr["displayType"].ToString();
                bid.TransType = dr["transType"].ToString();
                bid.StartTime = DateTime.Parse(dr["startTime"].ToString());
                bid.EndTime = DateTime.Parse(dr["endTime"].ToString());
                bid.StartBeginTime = DateTime.Parse(dr["startBeginTime"].ToString());
                bid.LimitTime = DateTime.Parse(dr["limitTime"].ToString());
                bid.TimeZone = dr["timeZone"].ToString();
                bid.Currency = dr["currency"].ToString();
                bid.CommBidStartDate = DateTime.Parse(dr["commBidStartDate"].ToString());
                bid.CommBidStartTime = DateTime.Parse(dr["commBidStartTime"].ToString());
                bid.TechBidStartDate = DateTime.Parse(dr["techBidStartDate"].ToString());
                bid.TechBidStartTime = DateTime.Parse(dr["techBidStartTime"].ToString());
                bid.BuyEndTime = DateTime.Parse(dr["buyEndTime"].ToString());
                bid.BuyEndDate = DateTime.Parse(dr["buyEndDate"].ToString());
                bid.EnrollEndDate = DateTime.Parse(dr["enrollEndDate"].ToString());
                bid.EnrollEndTime = DateTime.Parse(dr["enrollEndTime"].ToString());
                bid.EnrollStartDate = DateTime.Parse(dr["enrollStartDate"].ToString());
                bid.EnrollStartTime = DateTime.Parse(dr["enrollStartTime"].ToString());
                bid.BidCost = Convert.ToInt32(dr["bidCost"].ToString());
                bid.BidState = int.Parse(dr["bidState"].ToString());
                bid.EvalMethId = Convert.ToInt32(dr["evalMethId"].ToString());
            }
            return bid;
        }

        public int updateBidState(SourceBid bid)
        {
            StringBuilder sb = new StringBuilder("update "+tableName+" set BidState="+bid.BidState+" where bidId='"+bid.BidId+"'");
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText);
        }

        /// <summary>
        /// 根据招标状态筛选招标
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public List<SourceBid> getSourceBidsByState(int state)
        {
            List<SourceBid> list = new List<SourceBid>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where bidState="+state);
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceBid sourceBid = new SourceBid();
                    sourceBid.BidId = dr["bidId"].ToString();
                    sourceBid.BidState = int.Parse(dr["bidState"].ToString());
                    list.Add(sourceBid);
                }
            }
            return list;
        }

        public int updateBidReleaseTime(string bidId)
        {
            StringBuilder sb = new StringBuilder("update " + tableName + " set releaseTime=" + DateTime.Now.ToString("yyyy-MM-dd") + " where bidId='" + bidId+ "'");
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText);
        }
    }
}
