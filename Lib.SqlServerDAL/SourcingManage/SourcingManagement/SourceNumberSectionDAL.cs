﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceNumberSectionDAL : SourceNumberSectionIDAL
    {
        private string tableName = "dbo.SourceNumberSection";
        public List<SourceNumberSection> getSouNumSecsBySouSupMaId(string sourceId, string supplierId, string materialId)
        {
            List<SourceNumberSection> list = new List<SourceNumberSection>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where SourceId='"+sourceId+"' and SupplierId='"+supplierId+"' and MaterialId='"+materialId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceNumberSection sns = new SourceNumberSection();
                    sns.StartNumber = Convert.ToInt32(dr["StartNumber"].ToString());
                    sns.EndNumber = Convert.ToInt32(dr["EndNumber"].ToString());
                    sns.Num = float.Parse(dr["Num"].ToString());
                    list.Add(sns);
                }
            }
            return list;
        }
    }
}
