﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    class ConditionDAL : ConditionIDAL
    {
        private string tableName = "dbo.ConditionType";

        public int addCondition(ConditionType condition)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + "(")
                .Append("conditionId,conditionName,conditionCategory,calType,roundType,pn,description) values(")
                .Append("@ConditionId,@ConditionName,@ConditionCategory,@CalType,@RoundType,@Pn,@Description)");
            SqlParameter[] sqlParas = new SqlParameter[]
            {
                new SqlParameter("@ConditionId",SqlDbType.VarChar),
                new SqlParameter("@ConditionName",SqlDbType.VarChar),
                new SqlParameter("@ConditionCategory",SqlDbType.VarChar),
                new SqlParameter("@CalType",SqlDbType.VarChar),
                new SqlParameter("@RoundType",SqlDbType.VarChar),
                new SqlParameter("@Pn",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar)
            };
            sqlParas[0].Value = condition.ConditionId;
            sqlParas[1].Value = condition.ConditionName;
            sqlParas[2].Value = condition.ConditionCategory;
            sqlParas[3].Value = condition.CalType;
            sqlParas[4].Value = condition.RoundType;
            sqlParas[5].Value = condition.Pn;
            sqlParas[6].Value = condition.Description;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public int updateCondition(ConditionType condition)
        {
            StringBuilder sb = new StringBuilder("update " + tableName + "set")
                .Append("conditionName = @ConditionName,conditionCategory = @ConditionCategory,roundType = @RoundType,pn = @Pn,description=@Description")
                .Append(" where conditionId="+"@ConditionId");
            SqlParameter[] sqlParas = new SqlParameter[] 
            {  
                new SqlParameter("@ConditionName",SqlDbType.VarChar),
                new SqlParameter("@ConditionCategory",SqlDbType.VarChar),
                new SqlParameter("@RoundType",SqlDbType.VarChar),
                new SqlParameter("@Pn",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@ConditionId",SqlDbType.VarChar)
            };
            string sqlText = sb.ToString();
            sqlParas[0].Value = condition.ConditionName;
            sqlParas[1].Value = condition.ConditionCategory;
            sqlParas[2].Value = condition.RoundType;
            sqlParas[3].Value = condition.Pn;
            sqlParas[4].Value = condition.Description;
            sqlParas[5].Value = condition.ConditionId;
            return DBHelper.ExecuteNonQuery(sqlText,sqlParas);
        }

        public List<ConditionType> getConditions()
        {
            List<ConditionType> list = new List<ConditionType>();
            StringBuilder sb = new StringBuilder("select * from " + tableName);
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null&&dt.Rows.Count>0)
            {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ConditionType condition = new ConditionType();
                        condition.ConditionId = dr["conditionId"].ToString();
                        condition.ConditionName = dr["conditionName"].ToString();
                        condition.ConditionCategory = dr["conditionCategory"].ToString();
                        condition.CalType = dr["calType"].ToString();
                        condition.RoundType = dr["roundType"].ToString();
                        condition.Pn = dr["pn"].ToString();
                        list.Add(condition);
                    }
            }
            return list;
        }

        //根据条件编号查找条件类型
        public ConditionType findConditionTypeByConditionID(string conditionId)
        {
            ConditionType condition = new ConditionType();
            StringBuilder sb = new StringBuilder("select * from " +tableName+" where conditionId='"+conditionId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                condition.ConditionId = dr["conditionId"].ToString();
                condition.ConditionName = dr["conditionName"].ToString();
                condition.ConditionCategory = dr["conditionCategory"].ToString();
                condition.RoundType = dr["roundType"].ToString();
                condition.Pn = dr["pn"].ToString();
                condition.Description = dr["description"].ToString();
            } 
            return condition;
        }
    }
}
