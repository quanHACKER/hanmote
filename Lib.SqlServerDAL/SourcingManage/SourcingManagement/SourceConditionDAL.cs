﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    class SourceConditionDAL : SourceConditionIDAL
    {
        private string tableName = "dbo.SourceCondition";

        public List<SourceCondition> getSourceConByBidSpMaId(string sourceId, string supplierId, string materialId)
        {
            List<SourceCondition> list = new List<SourceCondition>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where SourceId='"+sourceId+"' and SupplierId='"+supplierId+"' and MaterialId='"+materialId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceCondition sc = new SourceCondition();
                    sc.ConditionId = dr["ConditionId"].ToString();
                    sc.Num = float.Parse(dr["Num"].ToString());
                    list.Add(sc);
                }
            }
            return list;
        }
    }
}
