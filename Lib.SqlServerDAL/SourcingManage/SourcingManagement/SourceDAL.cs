﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceDAL:SourceIDAL
    {
        private string tableName = "dbo.Source";

        /// <summary>
        /// 添加寻源记录
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int addSource(Source source)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                    .Append(" (Source_ID,Source_Name,Source_Type,PurchaseOrg,PurchaseGroup,Create_Time,Delivery_Time) values(")
                    .Append("@Source_ID,@Source_Name,@Source_Type,@PurchaseOrg,@PurchaseGroup,@Create_Time,@Delivery_Time)");
            
            SqlParameter[] sqlParas = new SqlParameter[]
            {
                new SqlParameter("@Source_ID",SqlDbType.VarChar),
                new SqlParameter("@Source_Name",SqlDbType.VarChar),
                new SqlParameter("@Source_Type",SqlDbType.VarChar),
                new SqlParameter("@PurchaseOrg",SqlDbType.VarChar),
                new SqlParameter("@PurchaseGroup",SqlDbType.VarChar),
                new SqlParameter("@Create_Time",SqlDbType.DateTime),
                new SqlParameter("@Delivery_Time",SqlDbType.DateTime)
            };
            sqlParas[0].Value = source.Source_ID;
            sqlParas[1].Value = source.Source_Name;
            sqlParas[2].Value = source.Source_Type;
            sqlParas[3].Value = source.PurchaseOrg;
            sqlParas[4].Value = source.PurchaseGroup;
            sqlParas[5].Value = DateTime.Now;
            //sqlParas[6].Value = source.Delivery_Time;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 查找所有的寻源记录
        /// </summary>
        /// <returns></returns>
        public List<Source> findAllSources(string item)
        {
            List<Source> sourceList = new List<Source>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_Type='" + item+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Source source = new Source();
                    source.Source_ID = dr["Source_ID"].ToString();
                    source.Source_Name = dr["Source_Name"].ToString();
                    source.Create_Time = DateTime.Parse(dr["Create_Time"].ToString());
                    sourceList.Add(source);
                }
            }
            return sourceList;
        }

        /// <summary>
        /// 更改指定寻源编号的寻源路径
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public int updateSourcePath(string sourceId, string path)
        {
            StringBuilder sb = new StringBuilder("update " + tableName + " set ")
                    .Append("Source_Path='" + path + "'")
                    .Append(" where Source_ID='" + sourceId + "'");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        /// <summary>
        /// 根据寻源编号查找寻源记录
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public Source findSourceById(string sourceId)
        {
            Source source = new Source();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Source_ID = '"+sourceId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];               
                source.Source_ID = sourceId;
                source.Source_Type = dr["Source_Type"].ToString();
                source.Source_Name = dr["Source_Name"].ToString();
                source.PurchaseOrg = dr["PurchaseOrg"].ToString();
                source.PurchaseGroup = dr["PurchaseGroup"].ToString();
                source.Create_Time = DateTime.Parse(dr["Create_Time"].ToString());
            }
            return source;
        }

    }
}
