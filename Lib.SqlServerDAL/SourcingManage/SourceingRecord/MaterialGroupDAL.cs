﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourceingRecord
{
    public class MaterialGroupDAL:MaterialGroupIDAL
    {
        private string tableName = "dbo.Material_Group";

        public List<MaterialGroup> getAllMaterialGroup()
        {
            List<MaterialGroup> list = new List<MaterialGroup>();
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    MaterialGroup mg = new MaterialGroup();
                    mg.MaterialGroupId = dr["Material_Group"].ToString();
                    mg.MaterialGroupName = dr["Description"].ToString();
                    list.Add(mg);
                }
            }
            return list;
        }

    }
}
